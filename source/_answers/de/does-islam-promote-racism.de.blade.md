---
extends: _layouts.answer
section: content
title: Befürwortet der Islam Rassismus?
date: 2024-11-17
description: Der Islam achtet nicht auf Unterschiede hinsichtlich Hautfarbe, Rasse
  oder Abstammung.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 587746
---

Alle Menschen stammen von einem Mann und einer Frau ab – Gläubige und Ungläubige, Schwarze und Weiße, Araber und Nicht-Araber, Reiche und Arme, Edle und Geringe.

Der Islam schenkt Unterschieden in Hautfarbe, Herkunft oder Abstammung keine Beachtung. Alle Menschen stammen von Adam ab, und Adam wurde aus Erde erschaffen. Im Islam basiert die Unterscheidung zwischen Menschen auf ihrem Glauben (Iman) und ihrer Frömmigkeit (Taqwa) – darauf, ob sie das tun, was Allah geboten hat, und das meiden, was Allah verboten hat. Allah sagt:

{{ $page->verse('49:13') }}

Der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:2564b') }}

Im Islam werden alle Menschen in Bezug auf Rechte und Pflichten als gleich angesehen. Vor dem Gesetz (der Schari’a) sind alle Menschen gleich, wie Allah sagt:

{{ $page->verse('16:97') }}

Glaube, Wahrhaftigkeit und Frömmigkeit führen allesamt ins Paradies, das demjenigen zusteht, der diese Eigenschaften erlangt – selbst wenn er zu den Schwächsten oder Geringsten der Menschen gehört. Allah sagt:

{{ $page->verse('..65:11') }}

Kufr (Unglaube), Arroganz und Unterdrückung führen alle zur Hölle, selbst wenn derjenige, der diese Dinge tut, einer der reichsten oder edelsten Menschen ist. Allah sagt:

{{ $page->verse('64:10') }}

Zu den Beratern des Propheten Muhammad {{ $page->pbuh() }} gehörten Muslime aller Stämme, Rassen und Farben. Ihre Herzen waren erfüllt vom Tauhid (Monotheismus) und ihr Glaube und ihre Frömmigkeit verbanden sie – wie etwa Abu Bakr von den Kuraisch, Ali ibn Abi Talib von den Bani Haashim, Bilal der Äthiopier, Suhayb der Römer, Salman der Perser, reiche Männer wie Uthman und arme Männer wie Ammar, wohlhabende und arme Leute wie die Ahl al-Suffah und andere.

Sie glaubten an Allah und strebten um Seinetwillen, bis Allah und Sein Gesandter mit ihnen zufrieden waren. Sie waren die wahren Gläubigen.

{{ $page->verse('98:8') }}
