---
extends: _layouts.answer
section: content
title: Zašto islam ne dozvoljava svinjetinu?
date: 2024-08-27
description: Muslimani se pokoravaju svemu što im Allah naredi, i uzdržavaju se od
  svega što im On zabranjuje, bez obzira da li je razlog za to jasan ili ne.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 584497
---

Osnovni princip za muslimana je da se pokorava svemu što mu Allah naredi, i uzdržava se od onoga što mu zabranjuje, bez obzira da li je razlog za to jasan ili ne.

Nije dozvoljeno da musliman odbaci bilo koju šerijatsku odluku ili da okleva da je sledi ako razlog iza toga nije jasan. Umesto toga, on mora da prihvati presude o halalu i haramu kada su dokazane u tekstu, bez obzira da li on razume razlog za to ili ne. Allah kaže:

{{ $page->verse('33:36') }}

### Zašto je svinjetina haram?

Svinjetina je haram (zabranjena) u islamu prema tekstu u Kur'anu, gde Allah kaže:

{{ $page->verse('2:173..') }}

Nije dozvoljeno muslimanina da konzumiraju svinjetinu ni pod kojim okolnostima, osim u slučajevima nužde kada život osobe zavisi od toga, kao što je slučaj gladi kada se osoba boji da će umreti, a ne može naći nikakvu drugu vrstu hrane.

U šerijatskim tekstovima se ne pominje određeni razlog za zabranu svinjetine, osim ajeta u kojem Allah kaže:

{{ $page->verse('..6:145..') }}

Reč rijs (prevedena ovde kao 'pogano') se koristi za označavanje svega što se u islamu i prema zdravoj ljudskoj prirodi (fitrah) smatra odvratnim. Sam ovaj razlog je dovoljan.

I postoji opšti razlog koji je dat u vezi sa zabranom haram hrane i pića i slično, koji ukazuje na razlog koji stoji iza zabrane svinjetine. Ovaj opšti razlog se može naći u ajetu u kojem Allah kaže:

{{ $page->verse('..7:157..') }}

### Naučni i medicinski razlozi za zabranu svinjetine

Naučna i medicinska istraživanja su takođe dokazala da se svinja, među svim ostalim životinjama, smatra nosiocem klica koje su štetne za ljudski organizam. Detaljno objašnjavanje svih ovih štetnih bolesti trajalo bi predugo, ali ukratko ih možemo navesti kao: parazitske bolesti, bakterijske bolesti, viruse i tako dalje.

Ovi i drugi štetni efekti ukazuju na to da je Allah zabranio svinjetinu samo sa razlogom, a to je očuvanje života i zdravlja, koji su među pet osnovnih potreba koje su zaštićene šerijatom.
