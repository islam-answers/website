---
extends: _layouts.answer
section: content
title: O Que os Muçulmanos Acreditam Sobre Jesus?
date: 2024-06-02
description: Os muçulmanos respeitam e reverenciam Jesus (que a paz esteja sobre ele).
  Eles o consideram um dos grandes mensageiros de Deus para a humanidade.
sources:
- href: https://www.islam-guide.com/pt/ch3-10.htm
  title: islam-guide.com
---

Os muçulmanos respeitam e reverenciam Jesus (que a paz esteja sobre ele). Eles o consideram um dos grandes mensageiros de Deus para a humanidade. O Alcorão confirma seu nascimento virginal, e um capítulo do Alcorão é intitulado ‘Mariam’ (Maria). O Alcorão descreve o nascimento de Jesus como se segue:

{{ $page->verse('3:45-47') }}

Jesus nasceu miraculosamente pelo comando de Deus, o mesmo comando que trouxe Adão à existência sem pai ou mãe. Deus disse:

{{ $page->verse('3:59') }}

Durante sua missão profética, Jesus realizou muitos milagres. Deus nos diz que Jesus disse:

{{ $page->verse('3:49..') }}

Os muçulmanos não acreditam que Jesus foi crucificado. Foi um plano dos inimigos de Jesus crucificá-lo, mas Deus o salvou e o elevou para Ele. E a aparência de Jesus foi colocada sobre outro homem. Os inimigos de Jesus pegaram esse homem e o crucificaram, pensando que ele fosse Jesus. Deus disse:

{{ $page->verse('..4:157..') }}

Nem Muhammad {{ $page->pbuh() }} e nem Jesus vieram para mudar a doutrina básica da crença em um Deus único, trazida pelos profetas anteriores, mas sim confirmá-la e renová-la.
