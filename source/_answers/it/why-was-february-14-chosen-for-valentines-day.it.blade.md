---
extends: _layouts.answer
section: content
title: Perché il 14 febbraio si festeggia San Valentino?
date: 2025-02-12
description: La festa di San Valentino, il 14 febbraio, sostituì la festa pagana di
  Lupercalia che in seguito fu associata a San Valentino.
sources:
- href: https://muslimskeptic.com/2022/02/14/the-dark-origins-of-valentines-day-muslims-might-be-unaware-of/
  title: muslimskeptic.com
- href: https://muslimskeptic.com/2022/02/15/its-not-just-christmas-how-christianity-adopts-paganism-for-valentines-day/
  title: muslimskeptic.com
- href: https://islamqa.info/en/answers/73007/
  title: islamqa.info
- href: https://islamqa.info/en/articles/65/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145893/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21694/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1130/
  title: islamqa.info
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 573107
---

### Qual è l'origine della festa di San Valentino?

Per comprendere le origini della festa di San Valentino, dobbiamo prima conoscerne la storia. Gli studiosi concordano sul fatto che essa deriva dalla combinazione di due eventi storici separati, uno che coinvolge un sacerdote cristiano (San Valentino) e l'altro riguarda la festa pagana di Lupercalia.

Lupercalia era una celebrazione pagana sanguinaria, violenta e a sfondo sessuale, con moltissimi sacrifici di animali, incontri casuali ed accoppiamenti con estranei nella speranza di allontanare gli spiriti maligni e l'infertilità. Questa festa spregevole era celebrata ogni anno il 15 febbraio, appena un giorno dopo l'attuale San Valentino. In origine era nota come Februa ("Purificazioni" o "Epurazioni") che è la radice da cui prende il nome il mese di febbraio.

Il tutto iniziava con un gruppo di sacerdoti romani, chiamati Luperci, che si riunivano in un luogo specifico, ad esempio nella grotta di Lupercal. Questi Luperci sacrificavano quindi una capra e un cane, ovviamente invocando le loro false divinità. Il sacrificio della capra doveva simboleggiare la fertilità. I Luperci correvano poi selvaggiamente per la città, frustando tutte le donne che incontravano. Anche questo era considerato un simbolo di fertilità. Si pensa che queste donne accogliessero con favore le frustate nella speranza di diventare fertili. Tuttavia, secondo alcune testimonianze storiche, questa parte del rituale non sarebbe stata sempre così consensuale. Il rituale "romantico" si concludeva con l'accoppiamento di giovani uomini e donne. In altre parole, si trattava di una notte di fornicazione.

L'unione di questa festa con la storia di San Valentino, che oggigiorno molti pensano sia l'origine principale della ricorrenza, fu molto probabilmente un'iniziativa della Chiesa cattolica per attrarre i pagani verso il cristianesimo. San Valentino è il nome dato a due degli antichi "martiri" della Chiesa cristiana. Si diceva che fossero due ma anche che ce ne fosse soltanto uno. Quando i Romani si convertirono al cristianesimo, continuarono a celebrare questo rito definendolo Festa dell'amore, ma lo trasformarono, passando dal concetto pagano di "amore spirituale" al nuovo concetto di "martire dell'amore", rappresentato da San Valentino che aveva sostenuto l'amore e la pace, e per questo motivo fu martirizzato, secondo quanto da loro affermato. Fu anche chiamata Festa degli innamorati e San Valentino venne considerato il santo protettore degli innamorati.

Una delle loro false credenze legate a questa festa era che i nomi delle ragazze in età da matrimonio venivano scritti su piccoli rotoli di carta e messi in un piatto, sopra un tavolo. Poi si convocavano i giovani che desideravano sposarsi e ognuno di loro sceglieva un rotolo di carta. Ogni ragazzo, quindi, si metteva a disposizione della ragazza della quale aveva estratto il nome, per un anno, in modo che potessero conoscersi meglio. Poi si sposavano, oppure ripetevano nuovamente lo stesso procedimento l'anno successivo, nel giorno della festa. Il clero cristiano reagì negativamente contro questa usanza, che riteneva avesse un'influenza corruttrice sulla morale dei giovani, sia uomini che donne.

Sulle origini di questa festa si diceva anche che, quando i Romani divennero Cristiani, a seguito della diffusione del Cristianesimo, nel III secolo d.C. l'imperatore romano Claudio II decretò che i soldati non potevano più sposarsi perché il matrimonio li avrebbe distratti dalle guerre che erano soliti combattere all'epoca. Questo decreto fu osteggiato da San Valentino che iniziò a celebrare i matrimoni dei soldati in segreto. Quando l'imperatore ne venne a conoscenza, lo fece imprigionare e lo condannò alla pena di morte. In prigione, San Valentino si innamorò della figlia del carceriere, ma rimase un segreto perché, secondo le leggi cristiane, ai sacerdoti e ai monaci era proibito sposarsi o innamorarsi. San Valentino è ancora molto stimato dai Cristiani per la fermezza che ebbe nell'aderire al Cristianesimo quando l'imperatore gli offrì il perdono se avesse abbandonato la fede e adorato gli dei romani; se avesse accettato, sarebbe stato uno dei suoi più stretti confidenti e lo avrebbe fatto diventare suo genero. Ma Valentino rifiutò l'offerta e preferì il cristianesimo, pertanto fu giustiziato il 14 febbraio 270 d.C., alla vigilia del 15 febbraio, festa dei Lupercali. Perciò questo giorno è stato intitolato a questo santo.

### Perché i musulmani non festeggiano San Valentino?

Qualcuno potrebbe chiedere: perché i musulmani non celebrano questa festa? Innanzitutto, le feste fanno parte delle cerimonie religiose di cui Allah dice nel Corano:

{{ $page->verse('..5:48..') }}

Allah dice anche:

{{ $page->verse('22:67..') }}

Poiché il giorno di San Valentino risale all'epoca romana e non a quella islamica, ciò significa che si tratta di un evento che appartiene esclusivamente al cristianesimo, non all'Islam, e pertanto i musulmani non vi partecipano. Il profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:892') }}

Questo vuol dire che ogni popolo deve distinguersi per le proprie celebrazioni. Se i cristiani hanno una festa e gli ebrei ne hanno un'altra, che appartengono esclusivamente al loro culto, allora nessun musulmano deve unirsi ai loro festeggiamenti, allo stesso modo in cui non condivide la loro religione o la loro direzione nella preghiera.

In secondo luogo, celebrare San Valentino significa assomigliare o imitare i pagani romani. Non è permesso ai musulmani imitare i non musulmani nelle cose che non fanno parte dell'Islam. Il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('abudawud:4031') }}

In terzo luogo, è un errore confondere il nome che viene dato a questa giornata con le reali intenzioni che si celano dietro di essa. L'amore a cui si fa riferimento in questo giorno è l'amore romantico, con donne e uomini che si amano, fidanzati e fidanzate. È noto per essere un giorno dedicato alla promiscuità e al sesso, senza vincoli o restrizioni. Non si tratta dell'amore puro tra un marito e sua moglie, o comunque non vi è una netta distinzione tra l'amore legittimo (rapporto tra marito e moglie) e l'amore proibito (tra amanti uomini e amanti donne).

Nell'Islam, il marito ama sua moglie tutto l'anno ed esprime questo amore nei suoi confronti con regali, in versi e in prosa, nelle lettere e in altri modi, col passare degli anni, non solo in un giorno dell'anno. Il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:1468') }}

Non esiste religione che incoraggi i credenti ad amarsi e a prendersi cura gli uni degli altri più di quanto faccia l'Islam. Ciò vale in ogni momento e in ogni circostanza. Il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:54') }}

### Chi trae vantaggio da San Valentino?

San Valentino è divenuta una festa estremamente commerciale con molti usi e costumi. La gente si scambia biglietti e cartoline di auguri con poesie d'amore, organizza feste e si scambia regali con le persone amate. L'impatto commerciale è notevole: in Gran Bretagna, ad esempio, le vendite di fiori raggiungono i 22 milioni di sterline. Il consumo di cioccolato cresce vertiginosamente, mentre i prezzi delle rose possono aumentare anche di dieci volte. I negozi di articoli da regalo e che vendono biglietti augurali gareggiano alacremente tra di loro per vendere i gadget e tutto il merchandise di San Valentino, e alcune famiglie decorano persino le loro case con rose rosse per l'occasione. La rilevanza economica di San Valentino è enorme: nel 2020, gli americani hanno speso 27,4 miliardi di dollari per i regali di San Valentino, secondo la National Retail Federation (NRT), la più importante associazione di commercio al dettaglio al mondo. Sostanzialmente, sono i produttori e i rivenditori che traggono i maggiori benefici da questa celebrazione mercificata dell'amore.
