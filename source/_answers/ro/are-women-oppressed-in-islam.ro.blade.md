---
extends: _layouts.answer
section: content
title: Sunt femeile oprimate în Islam?
date: 2024-10-29
description: Islamul promovează egalitatea pentru femei, condamnând opresiunea. Interpretările
  greșite provin adesea din practicile culturale, nu din învățăturile islamice.
sources:
- href: https://www.islamweb.net/en/article/109366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 554943
---

Printre cele mai importante subiecte de interes pentru non-musulmani se numără statutul femeilor musulmane și tema drepturilor lor sau, mai degrabă, lipsa percepută a acestora. Portretul pe care mass-media îl face femeilor musulmane, subliniind de obicei "opresiunea și misterul" lor, pare să contribuie la această percepție negativă.

Principalul motiv pentru care se întâmplă acest lucru este că oamenii nu reușesc adesea să facă distincția între cultură și religie - două lucruri complet diferite. De fapt, Islamul condamnă opresiunea de orice fel, fie că este împotriva unei femei sau a omenirii în general.

Coranul este cartea sacră după care trăiesc musulmanii. Această carte a fost revelată în urmă cu peste 1400 de ani unui om pe nume Muhammad {{ $page->pbuh() }}, care va deveni mai târziu Profetul, fie ca Allah să îi înalțe pomenirea. Au trecut 14 secole și această carte nu a fost modificată de atunci; nicio literă nu a fost alterată.

În Coran, Allah Cel Atotputernic spune:

{{ $page->verse('33:59') }}

Acest verset arată că Islamul face necesară purtarea unui Hijab. Hijab este cuvântul folosit pentru a acoperi, nu doar vălul de pe cap (așa cum ar putea crede unii oameni), ci și purtarea de haine largi care nu sunt în culori prea aprinse sau strălucitoare.

Uneori, oamenii văd femei musulmane acoperite și se gândesc la asta ca la o opresiune. Acest lucru este greșit. O femeie musulmană nu este oprimată, de fapt, ea este eliberată. Acest lucru se datorează faptului că ea nu mai este apreciată pentru ceva material, cum ar fi aspectul său frumos sau forma corpului său. Ea îi obligă pe ceilalți să o judece pentru inteligența, bunătatea, onestitatea și personalitatea ei. Prin urmare, oamenii o judecă pentru ceea ce este ea de fapt.

Atunci când femeile musulmane își acoperă părul și poartă haine lejere, ele se supun ordinului Domnului lor de a fi modeste, și nu obiceiurilor culturale sau sociale. De fapt, călugărițele creștine își acoperă părul din motive de modestie, dar nimeni nu le consideră "oprimate". Urmând porunca lui Allah, femeile musulmane fac exact același lucru.

Viețile oamenilor care au acceptat Coranul drept cuvânt al lui Dumnezeu s-au schimbat radical. Acesta a avut un impact extraordinar asupra multor oameni, în special asupra femeilor, deoarece a fost pentru prima dată când sufletele bărbaților și femeilor au fost declarate egale - cu aceleași obligații, precum și cu aceleași recompense.

Islamul este o religie care tratează femeile cu mare considerație. Cu mult timp în urmă, când se nășteau băieți, aceștia aduceau o mare bucurie familiei. Nașterea unei fete era întâmpinată cu mult mai puțină bucurie și entuziasm. Uneori, fetele erau urâte atât de mult încât erau îngropate de vii. Islamul a fost întotdeauna împotriva acestei discriminări iraționale împotriva fetelor și a infanticidului feminin.

## Au fost neglijate drepturile femeilor în Islam?

În ceea ce privește modificările acestor drepturi de-a lungul timpului, principiile de bază nu s-au schimbat, dar în ceea ce privește aplicarea acestor principii, nu există nicio îndoială că, în timpul epocii de aur a islamului, musulmanii au aplicat mai mult shari'ah (legea islamică) a Domnului lor, iar hotărârile acestei shari'ah includ onorarea mamei și tratarea cu bunăvoință a soției, fiicei, surorii și femeilor în general. Cu cât angajamentul religios era mai slab, cu atât aceste drepturi erau mai neglijate, dar până în Ziua Judecății va continua să existe un grup care aderă la religia lor și aplică shari'ah (legile) Domnului lor. Aceștia sunt oamenii care onorează cel mai mult femeile și le acordă drepturile lor.

În ciuda slăbiciunii angajamentului religios în rândul multor musulmani din zilele noastre, femeile încă se bucură de un statut înalt, fie ca fiice, soții sau surori; totodată, recunoaștem că există deficiențe, greșeli și neglijarea drepturilor femeilor în rândul anumitor persoane, dar fiecare va răspunde pentru sine.

Islamul este o religie care tratează femeile în mod corect. Femeia musulmană a primit acum peste 1400 de ani un rol, îndatoriri și drepturi de care majoritatea femeilor nu se bucură nici măcar astăzi în Occident. Aceste drepturi vin de la Dumnezeu și sunt concepute pentru a menține un echilibru în societate; ceea ce poate părea "nedrept" sau "lipsă" într-un loc este compensat sau explicat în alt loc.
