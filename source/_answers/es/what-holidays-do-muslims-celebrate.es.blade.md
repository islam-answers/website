---
extends: _layouts.answer
section: content
title: ¿Qué fiestas celebran los musulmanes?
date: 2024-07-28
description: Los musulmanes sólo celebran dos Eids (festivales), Eid al-Fitr (al final
  del mes de Ramadán) y Eid al-Adha, al final del Hajj (peregrinación anual).
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 573109
---

Los musulmanes sólo celebran dos Eids (festivales), Eid al-Fitr (al final del mes de Ramadán) y Eid al-Adha, al final del Hajj (peregrinación anual) el día 10 del mes de Dhul-Hijjah.

Durante estas dos fiestas, los musulmanes se felicitan unos a otros, reparten alegría en sus comunidades y lo celebran con sus familias extensas. Pero lo más importante es que recuerdan las bendiciones de Allah sobre ellos, celebran Su nombre y ofrecen la oración del Eid en la mezquita. Aparte de estas dos ocasiones, los musulmanes no reconocen ni celebran ningún otro día del año.

Por supuesto, hay otras ocasiones alegres para las cuales el Islam dicta una celebración apropiada, como las celebraciones de bodas (walima) o el nacimiento de un niño (aqeeqah). Sin embargo, estos días no se especifican como días particulares del año; más bien, se celebran tal como suceden en el curso de la vida de un musulmán.

En la mañana de Eid al-Fitr y Eid al-Adha, los musulmanes asisten en congregación a las oraciones del Eid en la mezquita, seguidas de un sermón (khutbah) que recuerda a los musulmanes sus deberes y responsabilidades. Después de la oración, los musulmanes se saludan con la frase "Eid Mubarak" (Bendito Eid) e intercambian regalos y dulces.

## Eid al-Fitr

Eid al-Fitr marca el final del Ramadán, que tiene lugar durante el noveno mes del calendario islámico basado en la luna.

Eid al-Fitr es importante porque sigue a uno de los meses más sagrados de todos: Ramadán. El Ramadán es un tiempo para que los musulmanes fortalezcan su vínculo con Allah, reciten el Corán y aumenten sus buenas acciones. Al final del Ramadán, Allah concede a los musulmanes el día de Eid al-Fitr como recompensa por haber completado con éxito el ayuno y por haber aumentado los actos de adoración durante el mes de Ramadán. En Eid al-Fitr, los musulmanes agradecen a Allah la oportunidad de presenciar otro Ramadán, de acercarse más a Él, de convertirse en mejores personas y de tener otra oportunidad de salvarse del Fuego del Infierno.

{{ $page->hadith('ibnmajah:3925') }}

## Eid al-Adha

Eid al-Adha marca la finalización del periodo anual del Hajj (peregrinación a La Meca), que tiene lugar en el mes de Dhul-Hijjah, el duodécimo y último mes del calendario islámico basado en la luna.

La celebración del Eid al-Adha conmemora la devoción del profeta Ibrahim (Abraham) a Allah y su disposición a sacrificar a su hijo Ismail (Ismael). En el momento mismo del sacrificio, Allah sustituyó a Ismail por un carnero, que debía ser sacrificado en lugar de su hijo. Esta orden de Allah era una prueba de la voluntad y el compromiso del profeta Ibrahim de obedecer las órdenes de su Señor, sin cuestionarlas. Por eso, Eid al-Adha significa la fiesta del sacrificio.

Una de las mejores acciones que los musulmanes pueden realizar en Eid al-Adha es el acto de Qurbani/Uhdiya (sacrificio), que se lleva a cabo tras la oración de Eid. Los musulmanes sacrifican un animal para recordar el sacrificio del Profeta Abraham por Allah. El animal sacrificado debe ser una oveja, un cordero, una cabra, una vaca, un toro o un camello. El animal debe estar en buen estado de salud y tener más de cierta edad para poder ser sacrificado, de forma halal y islámica. La carne del animal sacrificado se reparte entre la persona que realiza el sacrificio, sus amigos y familiares y los pobres y necesitados.

Sacrificar un animal en el Eid al-Adha refleja la disposición del profeta Abraham a sacrificar a su propio hijo y es también un acto de adoración que se encuentra en el Corán y una tradición confirmada del profeta Muhammad {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

En el Corán, Dios ha dicho:

{{ $page->verse('2:196..') }}
