<?php

return [
    // Christianity is europe + americas + africa

    "hinduism" => [
        "hi", // Hindi (500 million)
        "mr", // Marathi (83 million)
        "ta", // Tamil (75 million)
        "te", // Telugu (81 million)
        "gu", // Gujarati (55 million)
        "kn", // Kannada (44 million)
        "or", // Odia (35 million)
        "ml", // Malayalam (38 million)
        "ne", // Nepali (16 million)
    ],

    "buddhism" => [
        "zh", // Chinese (1.2 billion)
        "ja", // Japanese (125 million)
        "th", // Thai (69 million)
        "my", // Burmese (33 million)
        "ko", // Korean (77 million)
        "vi", // Vietnamese (85 million)
        "km", // Khmer (16 million)
        "si", // Sinhala (16 million)
        "bo", // Tibetan (6 million)
        "yue", // Cantonese (73 million)
    ],

    "europe" => [
        "en", // English (1.5 billion)
        "ru", // Russian (150 million)
        "fr", // French (77 million)
        "de", // German (76 million)
        "es", // Spanish (460 million)
        "it", // Italian (63 million)
        "pt", // Portuguese (220 million)
        "uk", // Ukrainian (30 million)
        "nl", // Dutch (23 million)
        "pl", // Polish (38 million)
        "el", // Greek (10 million)
        "sv", // Swedish (10 million)
        "da", // Danish (5.5 million)
        "fi", // Finnish (5 million)
        "cs", // Czech (10 million)
        "hu", // Hungarian (13 million)
        "ro", // Romanian (19 million)
        "bg", // Bulgarian (7 million)
        "hr", // Croatian (5 million)
        "sk", // Slovak (5 million)
        "lt", // Lithuanian (3 million)
        "lv", // Latvian (2 million)
        "et", // Estonian (1 million)
        "sl", // Slovenian (2 million)
        "mt", // Maltese (0.5 million)
        "cy", // Welsh (0.7 million)
        "ga", // Irish (1.8 million)
    ],

    "americas" => [
        "en", // English (300 million)
        "es", // Spanish (460 million)
        "pt", // Portuguese (220 million)
        "fr", // French (77 million)
        "ht", // Haitian Creole (10 million)
    ],

    "africa" => [
        "sw", // Swahili (16 million)
        "en", // English (130 million)
        "fr", // French (120 million)
        "zu", // Zulu (12 million)
        "am", // Amharic (32 million)
        "ig", // Igbo (24 million)
        "af", // Afrikaans (7 million)
        "sn", // Shona (10 million)
        "rw", // Kinyarwanda (12 million)
    ],
];
