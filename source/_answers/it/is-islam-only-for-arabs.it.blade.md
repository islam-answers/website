---
extends: _layouts.answer
section: content
title: L’Islam è solo per gli arabi?
date: 2024-05-25
description: L’Islam non è solo per gli arabi. La maggioranza dei musulmani non sono
  arabi e l’Islam è una religione universale, per tutti i popoli.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=MSC01&articleID=MSC010003&articlePages=1
  title: spubs.com
translator_id: 573107
---

Il modo più immediato per dimostrare che ciò è completamente falso, è ribadire e sottolineare che solo circa il 15-20% dei musulmani nel mondo sono arabi. Vi sono più musulmani indiani che arabi e più musulmani indonesiani che indiani! Credere che l’Islam sia una religione solo per gli arabi è una leggenda diffusa dai nemici dell’Islam fin dagli inizi della sua storia. Questo presupposto errato è probabilmente basato sul fatto che la maggior parte dei musulmani delle prime generazioni erano arabi, il Corano è in arabo e il profeta Mohammed {{ $page->pbuh() }} era arabo. Tuttavia, sia gli insegnamenti dell’Islam che la storia della sua diffusione dimostrano che i primi musulmani si sono impegnati al massimo per divulgare il messaggio di Verità a tutte le nazioni, razze e popoli.

Inoltre, va chiarito che non tutti gli arabi sono musulmani e non tutti i musulmani sono arabi. Un arabo può essere musulmano, cristiano, ebreo, ateo o di qualsiasi altra religione o ideologia. Inoltre, molti Paesi che alcuni considerano "arabi" non lo sono affatto, come ad esempio la Turchia e l'Iran (Persia). Le persone che vivono in questi Paesi parlano una lingua madre diversa dall'arabo ed hanno un patrimonio etnico differente da quello degli arabi.

È importante capire che fin dall'inizio della missione del Profeta Mohammed {{ $page->pbuh() }}, i suoi seguaci provenivano da diverse tipologie di individui: c'era Bilal, lo schiavo africano; Suhaib, il romano bizantino; Ibn Sailam, il rabbino ebreo; e Salman, il persiano. Poiché la verità religiosa è eterna e immutabile e l'umanità è una fratellanza universale, l'Islam insegna che le rivelazioni di Dio Onnipotente all'umanità sono sempre state coerenti, chiare e universali.

La verità dell'Islam è destinata a tutte le persone, indipendentemente dalla razza, dalla nazionalità o dalla provenienza linguistica. Uno sguardo al mondo musulmano, dalla Nigeria alla Bosnia, dalla Malesia all'Afghanistan, è sufficiente a dimostrare che l'Islam è un messaggio universale, destinato a tutta l'umanità - infatti, non dobbiamo dimenticare che un numero sempre più significativo di europei e americani, di tutte le razze e provenienze etniche, si sta avvicinando all'Islam.
