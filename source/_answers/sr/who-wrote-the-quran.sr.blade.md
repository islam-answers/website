---
extends: _layouts.answer
section: content
title: Ko je napisao Kuran?
date: 2024-07-14
description: Kuran je bukvalna Božija reč, koju je otkrio svom proroku Muhamedu (mir
  Božiji na njega ) preko Anđela Džibrila.
sources:
- href: https://www.islam-guide.com/ch1-1.htm
  title: islam-guide.com
translator_id: 584497
---

Bog je pomogao njegovog poslednjeg proroka Muhameda {{ $page->pbuh() }} sa mnogo čuda i mnogo dokaza koji su dokazali da je on pravi Prorok koga je poslao Bog. Takođe, Bog je podržao Njegovu poslednju objavljenju knjigu Sveti Kuran, sa mnogim čudima koja dokazuju da je ovaj Kuran doslovna Božija reč, koju je otkrio On, i da autor nije nijedno ljudsko biće.

Kuran je doslovna Božija reč, koju je Bog otkrio svom proroku Muhamedu {{ $page->pbuh() }} preko Anđela Džibrila. Prorok Muhamed {{ $page->pbuh() }} ga je naucio napamet, a potom diktirao svojim Pratiocima. Oni su ga, zauzvrat, zapamtili, zapisali i pregledali sa prorokom Muhamedom {{ $page->pbuh() }}. Štaviše, prorok Muhamed {{ $page->pbuh() }} je pregledao Kuran sa Anđelom Džibrilom jednom svake godine i dva puta u poslednjoj godini svog života. Od vremena kada je Kuran otkriven, pa sve do danas, uvek je postojao ogroman broj muslimana koji su zapamtili ceo Kuran, slovo po slovo. Neki od njih su čak uspeli da zapamte ceo Kuran do desete godine. Ni jedno slovo Kurana nije promenjeno tokom vekova.

Kuran, koji je otkriven pre četrnaest vekova, pomenuo je činjenice koje su naučnici tek nedavno otkrili ili dokazali. To bez sumnje dokazuje da Kuran mora da bude doslovna Božija reč, koju je On otkrio proroku Muhamedu {{ $page->pbuh() }}, i da Kuran nije autor proroka Muhameda {{ $page->pbuh() }} ili bilo kojeg drugog ljudskog bića. To takođe dokazuje da je prorok Muhamed [mir Božiji na njega] zaista prorok kojeg je poslao Bog. Van razuma je da bi bilo ko pre 1400 godina znao da su ove činjenice koje su otkrivene ili dokazane tek nedavno sa naprednom opremom i sofisticiranim naučnim metodama.
