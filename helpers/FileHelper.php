<?php

namespace App\Helpers;

class FileHelper
{
    public static function read(string $path): string
    {
        $ret = file_get_contents($path);

        if ($ret !== false) {
            return $ret;
        }

        throw new \Exception("Failed to read file: $path");
    }

    public static function write(string $path, string $content): bool
    {
        if (file_put_contents($path, $content) !== false) {
            return true;
        }

        throw new \Exception("Failed to write file: $path");
    }
}