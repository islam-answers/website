---
extends: _layouts.answer
section: content
title: O que é comida Halal?
date: 2024-09-04
description: Alimentos halal são aqueles permitidos por Deus para os muçulmanos consumirem,
  com as principais exceções sendo carne de porco e álcool.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 586449
---

Os alimentos Halal, ou lícitos, são aqueles que Deus permite que os muçulmanos consumam. De un modo geral, a maioria dos alimentos e bebidas são considerados Halal, sendo as principais excepções o porco e o álcool. As carnes e as aves de criação devem ser abatidas de forma humana e correta, que inclui a menção do nome de Deus antes do abate e a minimização do sofrimento dos animais.
