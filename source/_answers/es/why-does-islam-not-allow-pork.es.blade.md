---
extends: _layouts.answer
section: content
title: ¿Por qué el Islam no permite la carne de cerdo?
date: 2024-10-08
description: Los musulmanes obedecen todo lo que Allah les ordena y se abstienen de
  todo lo que Él les prohíbe, sea clara o no la razón detrás de eso.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 573109
---

El principio básico para el musulmán es obedecer todo lo que Allah le ordena y abstenerse de todo lo que Él le prohíbe, sea clara o no la razón detrás de eso.

No está permitido que un musulmán rechace una norma de la Shari'ah o dude en seguirla si la razón detrás no está clara. Por el contrario, debe aceptar las reglas sobre lo halal y lo haram cuando están probadas en el texto, ya sea que comprenda o no la razón detrás de eso. Allah dice:

{{ $page->verse('33:36') }}

### ¿Por qué es haram la carne de cerdo?

La carne de cerdo es haram (prohibida) en el Islam según el texto del Corán, donde Allah dice:

{{ $page->verse('2:173..') }}

No es permisible para un musulmán consumir la carne de cerdo bajo ninguna circunstancia, excepto en casos de necesidad en los que la vida de una persona depende de comerlo, como en el caso de inanición en el que una persona teme que va a morir, y no puede encontrar ningún otro tipo de comida.

En los textos de la Shari'ah no se menciona ninguna razón específica para la prohibición del cerdo, aparte del versículo en el que Allah dice:

{{ $page->verse('..6:145..') }}

La palabra rijs (traducida aquí como 'impuro') se utiliza para referirse a todo lo que se considera aborrecible en el Islam y de acuerdo con la sana naturaleza humana (fitrah). Esta sola razón es suficiente.

Y hay una razón general que se da con respecto a la prohibición de alimentos y bebidas haram y similares, que apunta a la razón detrás de la prohibición de la carne de cerdo. Esta razón general se encuentra en el versículo en el que Allah dice:

{{ $page->verse('..7:157..') }} 

### Razones científicas y médicas para prohibir la carne de cerdo

Las investigaciones científicas y médicas también han demostrado que el cerdo, entre todos los demás animales, se considera portador de gérmenes nocivos para el cuerpo humano. Explicar en detalle todas estas enfermedades nocivas llevaría demasiado tiempo, pero en resumen podemos enumerarlas como: enfermedades parasitarias, enfermedades bacterianas, virus, etc.

Estos y otros efectos nocivos indican que Allah solo ha prohibido la carne de cerdo por una razón, que es preservar la vida y la salud, que se encuentran entre las cinco necesidades básicas que están protegidas por la Shari'ah.
