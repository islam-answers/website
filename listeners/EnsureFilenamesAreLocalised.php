<?php

namespace App\Listeners;

use TightenCo\Jigsaw\Jigsaw;

class EnsureFilenamesAreLocalised
{
    public function handle(Jigsaw $jigsaw)
    {
        $answerFiles = collect(glob("source/_answers/*/*"));
        $filesFixed = collect();

        $answerFiles->each(function ($filepath) use (&$filesFixed) {
            $filepathParts = explode('/', $filepath);
            $langDirectory = $filepathParts[2];
            $filename = $filepathParts[3];
            $filenameParts = explode('.', $filename);
            $langSuffix = $filenameParts[1];

            if ($langSuffix !== $langDirectory) {
                $fixedFilepath = str_replace(".$langSuffix.", ".$langDirectory.", $filepath);
                rename($filepath, $fixedFilepath);
                $filesFixed->add(["old" => $filepath, "new" => $fixedFilepath]);
            }
        });

        if ($filesFixed->isNotEmpty() && $jigsaw->getEnvironment() === 'production') {
            printf("Error: %s file(s) had incorrect language code.", $filesFixed->count());

            $filesFixed->each(function ($file) {
                printf(" - %s should be renamed to %s\n", $file["old"], $file["new"]);
            });

            throw new \Exception("Incorrect language code in file name");
        }

        if ($filesFixed->isNotEmpty()) {
            printf("%s file(s) had incorrect language code, and were renamed to match their parent directory.\n", $filesFixed->count());

            $filesFixed->each(function ($file) {
                printf(" - %s renamed to %s\n", $file["old"], $file["new"]);
            });
        }
    }
}
