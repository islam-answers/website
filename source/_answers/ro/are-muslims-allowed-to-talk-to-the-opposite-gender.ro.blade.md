---
extends: _layouts.answer
section: content
title: Este permis musulmanilor să vorbească cu sexul opus?
date: 2024-11-28
description: Interacțiunea cu sexul opus este permisă în caz de necesitate, dar ea trebuie
  să fie modestă, scurtă și să evite tentațiile.
sources:
- href: https://islamqa.info/en/answers/27304/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1497/
  title: islamqa.info
- href: https://islamqa.info/en/answers/34841/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21183/
  title: islamqa.info
translator_id: 554943
---

În islam, interacțiunea cu sexul opus este permisă în caz de necesitate, dar ea trebuie să fie modestă, scurtă și să evite tentațiile.

### Principiul prevenirii (evitarea riscurilor)

În islam, tot ceea ce ar putea determina o persoană să cadă în lucruri nepermise (haram) este, de asemenea, nepermis, chiar dacă în principiu este permis inițial. Aceasta este ceea ce savanții numesc principiul prevenirii prejudiciului. Allah spune în Coran:

{{ $page->verse('24:21..') }}

Conversația - fie în persoană, verbală sau în scris - între bărbați și femei este permisă în sine, dar poate fi o modalitate de a cădea în încercarea (sau tentația, în arabǎ "fitnah") Diavolului (Shaytaan).

### Pericolele interacțiunii cu sexul opus

Nu există nicio îndoială că fitnah (tentația) femeilor este mare. Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:2741') }}

Prin urmare, musulmanul trebuie să fie precaut cu privire la această fitnah și să se țină departe de orice lucru care l-ar putea face să cadă pradă ei. Unele dintre cele mai mari cauze ale acestei fitnah sunt privitul femeilor și socializarea cu ele.

Allah spune în Coran:

{{ $page->verse('24:30-31') }}

Aici Allah îi poruncește Profetului Său {{ $page->pbuh() }} să le spună bărbaților și femeilor credincioase să își coboare privirea și să își păzească castitatea, apoi explică faptul că acest lucru este mai curat pentru ei. Păstrarea castității și evitarea acțiunilor imorale se realizează numai prin evitarea mijloacelor care conduc la astfel de acțiuni. Fără îndoială, rătăcirea privirii și socializarea dintre bărbați și femei la locul de muncă și în alte locuri sunt printre cele mai comune mijloace care duc la imoralitate.

De câte ori aceste conversații au condus la consecințe nefavorabile și chiar au făcut ca oamenii să se îndrăgostească, iar pe unii i-au determinat să facă lucruri chiar mai grave decât atât. Satana (Shaytaan) îi face să își imagineze calități atractive la celălalt, ceea ce îi determină să dezvolte un atașament care este în detrimentul bunăstării lor spirituale și al afacerilor lumești.

### Normele interacțiunii cu sexul opus

Mahramul unei persoane (tradus în românǎ ca "rudǎ necăsătoribilă") este o persoană cu care nu i se permite niciodată să se căsătorească din cauza relației apropiate de sânge (pentru o femeie, aceștia sunt strămoșii, descendenții, frații, unchii și nepoții ei) sau din cauza alăptării sau pentru că sunt rude prin căsătorie (pentru o femeie, aceștia sunt strămoșii soțului ei, tații vitregi ai acestuia, descendenții lui și soții fiicelor ei).

Conversația cu o persoană de sex opus care nu îi este mahram ar trebui să fie doar pentru o nevoie specifică, cum ar fi adresarea unei întrebări, cumpărarea sau vânzarea și așa mai departe. Astfel de conversații ar trebui să fie scurte și la obiect, fără nimic îndoielnic nici în ceea ce se spune, nici în modul în care se spune. Conversația nu trebuie lăsată să se îndepărteze prea mult de subiectul discutat; nu ar trebui să întrebe despre chestiuni personale care nu au nicio legătură cu problema discutată, cum ar fi câți ani are o persoană, cât de înaltă este sau unde locuiește etc.

Islamul blochează toate căile care pot duce la fitnah (ispitǎ), prin urmare interzice blândețea vorbirii și nu permite unui bărbat să fie singur cu o femeie non-mahram. Nu ar trebui să-și lase vocile să fie dulci sau să folosească expresii care exprimǎ blândețe; mai degrabă, ar trebui să vorbească pe același ton obișnuit de voce ca și cu oricine altcineva. Allah, înălțat, spune, adresându-se soțiilor profetului {{ $page->pbuh() }}:

{{ $page->verse('..33:32') }}

De asemenea, nu este permis ca un bărbat să fie singur cu o femeie care nu este mahramul său, deoarece Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:1341') }}

Conversația sau corespondența trebuie întreruptă imediat dacă inima începe să se agite cu sentimente de dorință. De asemenea, glumele, râsetele sau flirtul trebuiesc evitate. O altă normǎ a interacțiunii cu sexul opus este evitarea privirii și încercarea de a coborî privirea cât mai mult posibil. Jarir bin 'Abdullah a raportat:

{{ $page->hadith('muslim:2159') }}

De asemenea, merită menționat faptul că nu este permis unui bărbat care crede în Allah și în Trimisul Său {{ $page->pbuh() }} să aibă vreun contact fizic cu un membru al sexului opus care nu este mahram pentru el.
