---
extends: _layouts.answer
section: content
title: Czy islam jest tylko dla Arabów?
date: 2024-07-16
description: 'Islam nie jest wyłącznie dla Arabów. Większość muzułmanów nie jest Arabami,
  a islam jest religią uniwersalną dla wszystkich ludzi. '
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=MSC01&articleID=MSC010003&articlePages=1
  title: spubs.com
translator_id: 584496
---

Najszybszym sposobem udowodnienia, że jest to całkowita nieprawda, jest stwierdzenie faktu, iż wyłącznie około 15% do 20% muzułmanów na świecie to Arabowie. Jest więcej muzułmanów indyjskich niż muzułmanów arabskich, a także więcej muzułmanów indonezyjskich niż muzułmanów indyjskich! Przekonanie, że Islam jest religią wyłącznie dla Arabów, jest mitem szerzonym przez wrogów Islamu na początku jego historii. To błędne założenie wynika prawdopodobnie z faktu, ze większość pierwszego pokolenia Muzułmanów była Arabami, Koran jest w języku arabskim, a Prorok Muhammad {{ $page->pbuh() }} był Arabem. Niemniej jednak, zarówno nauki Islamu, jak i historia jego rozprzestrzeniania się pokazują, że pierwsi Muzułmanie dokładali wszelkich starań, aby szerzyć swoje przesłanie Prawdy pośród wszystkich narodów, ras i ludów.

Ponadto należy wyjaśnić, że nie wszyscy Arabowie są muzułmanami i nie wszyscy muzułmanie są Arabami. Arab może być muzułmaninem, chrześcijaninem, żydem, ateistą- lub wyznawcą jakiejkolwiek innej religii czy ideologii. Ponadto wiele krajów, które niektórzy uważają za „arabskie”, wcale nie jest „arabskimi” - jak Turcja i Iran (Persja). Mieszkańcy tych krajów posługują się językami innymi niż arabski i mają inne pochodzenie etniczne niż Arabowie.

Ważne jest, aby zdać sobie sprawę, że od samego początku misji Proroka Muhammada {{ $page->pbuh() }}, jego naśladowcy pochodzili z szerokiego spektrum osób- był Bilal, afrykański niewolnik; Suhaib, bizantyjski Rzymianin; Ibn Sailam, żydowski rabin; i Salman, Pers. Ponieważ prawda religijna jest wieczna i niezmienna, a ludzkość jest jednym powszechnym braterstwem, Islam naucza, że objawienia Boga Wszechmogącego dla ludzkości zawsze były spójne, jasne i uniwersalne.

Prawda Islamu jest przeznaczona dla wszystkich ludzi bez względu na rasę, narodowość czy pochodzenie językowe. Wystarczy spojrzeć na świat muzułmański, od Nigerii po Bośnię i od Malezji po Afganistan, aby udowodnić, ze Islam jest uniwersalnym przesłaniem dla całej ludzkości- nie wspominając już o tym, ze znaczna liczba Europejczyków i Amerykanów wszystkich ras i środowisk etnicznych przechodzi na Islam.
