---
extends: _layouts.answer
section: content
title: Da li je islam protiv pasa?
date: 2024-09-06
description: Islam nas uči da se prema životinjama odnosimo dostojanstveno i milosrdno. Psi se ne mogu držati kao kućni ljubimci, ali se mogu držati u određene svrhe.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 584497
---

Životinje su Allahava stvorenja i mi ih ne smatramo lošim; mi smo dužni, kao muslimani, da tretiramo životinjena dostojanstven, human i milosrdan način. Allahov Poslanik {{ $page->pbuh() }} je rekao:

{{ $page->hadith('bukhari:3321') }}

## Ubijanje pasa

Abdallah ibn Umar je rekao:

{{ $page->hadith('muslim:1570b') }}

Važno je razjasniti o kakvim psima se govori o ovoj naraciji, kao i o opštem kontekstu. Psi o kojima se ovde govori su čopori divljih pasa koji lutaju po gradovima i predstavljaju pretjnu zajednici ali i zdravstveni rizik. Oni prenose i šire bolesti, baš kao i pacovi, i prouzrokoju mnogo neprijatanosti ljudima. Ubijanje opasnih životinja ili životinja prenosnika bolesti je prihvaćeno širom sveta.

Na osnovu svih dokaza, pravnici su spomenuli da je naredba za ubijanje pasa dozvoljena samo ako je pas opasan i štetan. Međutim, ukoliko je pas bezopasan, bez obzira na boju, taj pas ne smije biti ubijen.

## Čistoća

Pljuvačka psa je nečista prema većini muslimanskih učenjaka, kao i njohovo krzno. Jedno od najistaknutijih učenja islama je čistoća na svim nivoima, uzimajući ovo u obzir, kao i činjenicu da anđeli ne posećuju mesta gde su psi. To je zbog reči poslanika {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:3322') }}

## Držanje pasa kao kućnih ljubimaca

Islam zabranjuje držanje pasa kao kućnih ljubimaca, Allahov Poslanik {{ $page->pbuh() }} je rekao:

{{ $page->hadith('muslim:1574g') }}
