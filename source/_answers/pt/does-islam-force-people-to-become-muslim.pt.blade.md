---
extends: _layouts.answer
section: content
title: O Islam força as pessoas a tornarem-se muçulmanas?
date: 2024-11-18
description: Ninguém pode ser obrigado a aceitar o Islam. Para aceitar o Islam, uma
  pessoa tem de acreditar e obedecer a Deus de forma sincera e voluntária.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 594172
---

Deus diz no Alcorão:

{{ $page->verse('2:256..') }}

Embora seja um dever dos muçulmanos transmitir e partilhar a bela mensagem do Islam com os outros, ninguém pode ser obrigado a aceitar o Islam. Para aceitar o Islam, uma pessoa tem de acreditar e obedecer a Deus de forma sincera e voluntária, pelo que, por definição, ninguém pode (ou deve) ser forçado a aceitar o Islam.

Considere o seguinte:

- A Indonésia tem a maior população muçulmana, mas não foram travadas batalhas para levar o Islam até lá.
- Há cerca de 14 milhões de cristãos coptas árabes que vivem no coração da Arábia há várias gerações.
- O Islam é atualmente uma das religiões com maior crescimento no mundo ocidental.
- Embora a luta contra a opressão e a promoção da justiça sejam razões válidas para a realização da jihad, forçar as pessoas a aceitar o Islam não é uma delas.
- Os muçulmanos governaram Espanha durante cerca de 800 anos, mas nunca coagiram as pessoas a converterem-se.
