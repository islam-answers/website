---
extends: _layouts.answer
section: content
title: What is Halal food?
date: 2024-03-23
description: Halal foods are those permitted by God for Muslims to consume, with the
  main exceptions being pig and alcohol.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

Halal, or lawful, foods are those permitted by God for Muslims to consume. Generally, most foods and beverages are considered Halal, with the main exceptions being pig and alcohol. Meats and poultry must be slaughtered humanely and correctly, which includes mentioning God’s name before slaughter and minimising the suffering of animals.
