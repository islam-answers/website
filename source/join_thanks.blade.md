---
title: Join the team
description: Help us translate our answers into more languages.
extends: _layouts.main
permalink: join/thanks.html
section: body
---

# Join the team

Thank you for your interest in helping us translate our answers into more languages. We will get back to you soon with more information on how you can get involved.
