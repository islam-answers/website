---
extends: _layouts.answer
section: content
title: Šta islam kaže o terorizmu?
date: 2024-08-05
description: Islam, religija milosrđa, ne dozvoljava terorizam.
sources:
- href: https://www.islam-guide.com/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 584497
---

Islam, religija milosrđa, ne dozvoljava terorizam. U Kur'anu, Bog je rekao:

{{ $page->verse('60:8') }}

Poslanik Muhamed {{ $page->pbuh() }} je zabranjivao vojnicima da ubijaju žene i decu, i savetovao ih je:

{{ $page->hadith('tirmidhi:1408') }}

I on je takođe rekao:

{{ $page->hadith('bukhari:3166') }}

Takođe, Poslanik Muhamed {{ $page->pbuh() }} je zabranio kažnjavanje vatrom.

On je jednom naveo ubistvo kao drugi od najvećih grehova, i on je čak upozorio da na Sudnjem danu,

{{ $page->hadith('bukhari:6533') }}

Muslimani su čak ohrabreni da budu ljubazni prema životinjama i zabranjeno im je da ih povrede. Jednom je Poslanik Muhamed {{ $page->pbuh() }} rekao:

{{ $page->hadith('bukhari:3318') }}

On je takođe rekao da je čovek dao veoma žednom psu piće, tako da mu je Bog oprostio njegove grehe zbog ovog čina. Poslanik, s.a.v.s., je upitan: "Božiji Poslaniče, da li smo nagrađeni za dobrotu prema životinjama?" On je rekao:

{{ $page->hadith('bukhari:2466') }}

Pored toga, dok uzimaju život životinji za hranu, muslimanima je naređeno da to učine na način koji izaziva najmanju moguću količinu straha i patnje. Poslanik Muhamed {{ $page->pbuh() }} je rekao:

{{ $page->hadith('tirmidhi:1409') }}

U svetlu ovih i drugih islamskih tekstova, čin podsticanja terora u srcima bespomoćnih civila, masovno uništavanje zgrada i imovine, bombardovanje i sakaćenje nevinih muškaraca, žena i dece su sve zabranjena i odvratna dela prema islamu i muslimanima.

Muslimani slede religiju mira, milosti i opraštanja, a velika većina nema nikakve veze sa nasilnim događajima koje su neki povezali sa muslimanima. Ako bi pojedinačni musliman počinio teroristički čin, ta osoba bi bila kriva za kršenje zakona islama.

Međutim, važno je razlikovati terorizam i legitimni otpor okupaciji, jer su to dvoje veoma različiti.
