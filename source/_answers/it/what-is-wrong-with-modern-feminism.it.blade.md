---
extends: _layouts.answer
section: content
title: Cosa c'è di sbagliato nel femminismo moderno?
date: 2025-02-12
description: Il femminismo nega le naturali differenze di genere. L'Islam invece onora
  le donne, assicurando giustizia, equilibrio e protezione dei loro diritti.
sources:
- href: https://islamqa.info/en/answers/40405/
  title: islamqa.info
- href: https://islamqa.info/en/answers/258254/
  title: islamqa.info
- href: https://islamqa.info/en/answers/43252/
  title: islamqa.info
- href: https://islamqa.info/en/answers/930/
  title: islamqa.info
- href: https://islamqa.info/en/answers/175863
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/127550/
  title: islamqa.org (AskImam.org)
- href: https://muslimskeptic.com/2021/03/02/feminism-harms-women/
  title: muslimskeptic.com
translator_id: 573107
---

### Cos'è il femminismo e perché è un tema controverso?

Il termine femminismo è molto ambiguo. Wikipedia lo definisce come "una serie di movimenti sociali, movimenti politici e ideologie accomunati da un obiettivo condiviso, ovvero definire, stabilire e raggiungere l'uguaglianza politica, economica, personale e sociale dei sessi". Il femminismo sembra mirare alla liberazione, ma in realtà mira al privilegio di genere mascherato da uguaglianza. In sostanza, il femminismo nega la realtà umana e non conduce alla giustizia.

Le femministe sostengono che le donne sono state tradizionalmente disumanizzate da una società dominata dagli uomini, che chiamano patriarcato; e che il fatto di essere uomini è sempre stato un vantaggio. Ma questa affermazione settaria disconosce i privilegi di cui le donne hanno spesso goduto per il semplice fatto di essere donne. Questo privilegio non viene apprezzato dalle femministe poiché di solito il privilegio è nascosto a chi lo possiede.

Pressoché tutte le specie, dalle api ai primati, hanno ruoli diversi in base al loro genere, con idoneità biologiche differenti nei due sessi. Eppure le femministe insistono sul fatto che ogni differenza di genere tra gli esseri umani è solo un'invenzione e che non c'è nulla di biologico negli uomini o nelle donne che determini le loro funzioni all'interno della società. Studi scientifici hanno chiaramente dimostrato, tuttavia, il ruolo che il testosterone ha nello sviluppo muscolare, nell'incremento della competitività, della fiducia e dell'assunzione di rischi. Tutti questi fattori rendono sicuramente gli uomini più adatti a svolgere i compiti più rischiosi e competitivi nella società. Grazie al testosterone, gli uomini tendono per natura ad essere più veloci, più pesanti, ad avere più resistenza e sono fisicamente più forti. Quindi, insegnare a una ragazza che per natura può competere alla pari con gli uomini in ogni cosa è fuorviante.

### In che modo il femminismo moderno delude le donne

Un'analisi corretta e obiettiva delle statistiche rivela che questo argomento è più complesso e profondo:

1. Secondo l'Ufficio per il Censimento degli USA, 10.4 milioni di famiglie in America sono gestite da madri single che costituiscono l'unico sostentamento per le loro famiglie. 
1. Ogni anno in America, oltre un milione di bambini vengono uccisi a causa degli aborti, secondo i Centri per la Lotta contro le Malattie. 
1. Ogni anno, 683.000 donne vengono stuprate in America (78 donne ogni ora), come riportato dal Dipartimento di Giustizia degli Stati Uniti. 
1. In America, 1.320 donne vengono uccise ogni anno (4 donne al giorno) dai loro mariti o fidanzati. 
1. Ogni anno in America, circa 3 milioni di donne sono soggette ad abusi fisici da parte del marito o del fidanzato, secondo il sito web ufficiale del governo dello Stato del New Jersey. 
1. In America, il 22,1% delle donne subisce abusi fisici da parte del marito o del fidanzato (attuale o ex), secondo il Dipartimento di Giustizia degli Stati Uniti. 
1. Un rapporto pubblicato dall'Ufficio di Statistica del Lavoro degli Stati Uniti conferma che, in occidente, la maggior parte delle donne svolge lavori mal pagati e di basso livello, e che, nonostante le pressioni esercitate dal governo per migliorare i posti di lavoro delle donne, il 97% delle posizioni dirigenziali più elevate, nella maggior parte delle aziende, è occupato da uomini. 
1. Il 78% delle donne nelle forze armate subisce molestie sessuali da parte degli altri militari. 
1. Secondo il New York Times, ogni anno circa 50.000 donne e ragazze cadono vittime della tratta in America dove vengono ridotte in schiavitù e costrette a prostituirsi. 
1. Lo sfruttamento del corpo delle donne nei modi più svariati rappresenta un'industria che frutta dodici miliardi di dollari all'anno solo in America, secondo un rapporto pubblicato dalla Reuters. 
1. I risultati di una ricerca condotta in quattordici paesi hanno dimostrato che il 42% dei britannici ammette di avere relazioni illecite con più di una persona contemporaneamente, mentre, per quanto riguarda gli americani, la percentuale sale al 50%. In Italia, secondo la BBC, l'indice delle relazioni illecite scende al 38% e in Francia al 36%.

Esiste un elenco pressoché infinito di pagine oscure e nascoste che rivelano la reale e triste situazione delle donne nella civiltà materialista occidentale, tutte documentate da siti web ufficiali occidentali, che costituiscono fonti affidabili e attendibili e che confermano tali statistiche.

### La perversa agenda femminista dei governi laici

Il sistema che esiste nei paesi laici occidentali è di tipo liberale. Incoraggia e promuove il liberalismo esaltando la libertà personale, l'indipendenza e l'uguaglianza. Ovvero, in altre parole, l'adorazione di se stessi, il fatto di essere totalmente assorbiti dall'individuo a scapito di tutto il resto. In una società laica e liberale ogni cosa ruota attorno all'individuo e solo all'individuo. Il sistema è progettato per costringere tutte le persone a seguire un percorso specifico e predeterminato: la schiavitù salariale, la famiglia a doppio reddito, in cui sia il marito che la moglie devono lavorare per arrivare a fine mese. In passato, il sistema era strutturato in modo tale che una famiglia potesse sopravvivere e vivere bene solo col reddito del padre, mentre la madre restava a casa coi figli. Col passare degli anni, il sistema si è spostato sempre di più verso la famiglia a doppio reddito, costringendo sia il padre che la madre a uscire di casa per lavorare.

Ma che ne è stato dei bambini? Istituti scolastici per tutti. E per i bambini ancora troppo piccoli per andare a scuola? Istituti di assistenza all'infanzia. E che farne degli anziani troppo vecchi per continuare a lavorare come schiavi salariati? Metterli in istituti di assistenza agli anziani. Tutti vengono istituzionalizzati, internati. Gli uomini sono separati dalle loro donne e i genitori dai loro figli. I legami naturali tra le persone vengono recisi. I legami biologici sono spezzati. Le normali relazioni che dovrebbero esistere tra gli esseri umani sono distrutte.

Un esempio di legame spezzato è la separazione della madre dal suo bambino appena nato. In natura, nel mondo animale e in tutte le società umane tradizionali, attraverso il tempo e lo spazio, la madre e il bambino sono sempre rimasti insieme. Stare lontani dalla madre non è naturale, soprattutto in così tenera età. Questa separazione tra la madre e il suo bambino è incredibilmente difficile ed è estremamente faticosa, sia per la mamma che per il bambino. È in netto contrasto con la natura di entrambi, mamma e bambino, e quindi sconvolge i bisogni più profondi di entrambi.

Spesso non è colpa né della madre né di una persona specifica. La povera madre è spesso (anche se non sempre) costretta, per necessità, a lasciare il suo bambino in un asilo nido, con degli estranei, mentre lei va al lavoro. Spesso non ha altra scelta. Le madri single sono frequenti, soprattutto in una società in cui la Zina (fornicazione) è dilagante, normalizzata, senza vergogna e, di fatto, attivamente incoraggiata dalla società e persino dai genitori. L'individualismo è incentivato come qualcosa di estremamente importante.

Qual è il risultato di tutti questi "diritti", iper-focalizzati solo sull'individuo e sui suoi piaceri momentanei? I diritti di tutti gli altri vengono ignorati e violati. I bambini soffrono e vengono abbandonati. I matrimoni soffrono e quindi falliscono. Le famiglie nucleari soffrono e si disgregano. La famiglia allargata soffre e si dissolve. I legami di parentela si spezzano. La comunità è disconnessa. La società si indebolisce. Le civiltà collassano.

I governi incentivano il femminismo attraverso i mass media e attraverso i loro organismi, come ad esempio la CIA (Central Intelligence Agency). Utilizzano persone come Gloria Steinem (giornalista e attivista americana) per promuovere i loro programmi, con strategie a lungo termine. In particolare, dato che le madri sono troppo impegnate al lavoro, i loro figli iniziano l'asilo nido il prima possibile (anche prima dei 6 mesi). In tal modo, questi bambini vengono istituzionalizzati precocemente per essere indottrinati con le ideologie che il governo vuole imporre loro, rendendoli più facili da controllare nella società man mano che crescono.

### Islam: una soluzione naturale per l'uguaglianza di genere

Allah ha onorato enormemente le donne. Le onora come figlie, madri e mogli, e conferisce loro diritti e pregi, imponendo un buon trattamento nei loro confronti che in molti casi non è condiviso dagli uomini. L'Islam non nega l'umanità della donna. Anzi, le riconosce i suoi diritti e la tiene in grande considerazione. Ad esempio, prima dell'Islam alle donne non era permesso ereditare, o addirittura a volte venivano sepolte vive, e altre cose ancora peggiori. Poi arrivò l'Islam che proibì di seppellire le bambine vive, considerandolo un omicidio, che è uno dei peccati più gravi. L'Islam ha dato alle donne la loro quota di eredità, ha permesso alle donne di acquistare, vendere e possedere proprietà, e le ha incoraggiate a cercare la conoscenza e chiamare le persone ad Allah. Ha comandato che le donne siano onorate come mogli e come madri, e ha reso i diritti della madre tre volte superiori a quelli del padre. Ed esistono molti altri modi con cui l'Islam ha onorato le donne.

L'Islam ha un sistema completamente diverso, un sistema eccellente e perfetto, che prevede un delicato equilibrio tra i diritti di tutte le persone: l'individuo, la famiglia, i bambini, i genitori, la comunità e la società. I diritti più importanti e fondamentali di ciascuna parte sono garantiti in modo sicuro, affinché nessuno soffra; e per fare in modo che la natura umana sia sostenuta e la fitrah (disposizione naturale innata) sia mantenuta. La dignità dei bambini, delle madri, dei padri e degli anziani è assicurata. I loro bisogni vengono rispettati.

Allah ci dice nel Corano:

{{ $page->verse('4:1') }}

L'Islam è il sistema che risponde a tutti i bisogni umani; soddisfa tutti i diritti naturali di tutte le persone e organizza la società in un modo che si allinea magnificamente con la nostra natura umana intrinseca e non la distrugge. L'Islam regola il modo in cui interagiamo con i nostri figli, i nostri genitori, i nostri fratelli, i nostri vicini, amici e persino gli estranei. L'Islam rafforza il nostro amore istintivo per i nostri cari e alimenta le nostre relazioni umane biologiche. Come?

Grazie agli strumenti che integrano la nostra religione, come i ruoli di genere (in cui il marito ha obblighi finanziari mentre la moglie è libera di essere madre e stare a casa coi figli); con "birr al-walidayn" (eccellenza verso i genitori) che assicura il rispetto per i genitori da parte dei loro figli; con "silat al-rahim" (mantenimento dei legami di parentela) che assicura la rete di protezione e il supporto della famiglia allargata e dei parenti e mantiene anche l'ascendenza. In un sistema islamico, è rarissimo che una madre venga separata dal proprio bambino sei settimane dopo il parto. Sia la madre che il bambino vengono risparmiati dalla sofferenza di essere separati l'uno dall'altro in modo così innaturale. Nel sistema laico occidentale, invece, questa separazione della madre dal bambino è praticamente la norma.

L'Islam riconosce e accoglie le lievi differenze tra uomini e donne perché Colui Che ha inviato l'Islam è anche Colui Che ha creato uomini e donne con la loro ampia varietà di affinità e differenze. Date queste diversità tra i sessi, in realtà, l'uguaglianza speculare sarebbe oppressiva perché costringerebbe un sesso a piegarsi ai criteri che si applicano all'altro sesso.

Se le femministe riconoscono che i sessi sono diversi e quindi le donne dovrebbero essere trattate "equamente" anziché "allo stesso modo" e c'è spazio per emendamenti nelle leggi per tener conto di tali differenze, allora dove tracciamo il confine? Ogni caso di trattamento diverso potrebbe essere attribuito alla diversa natura/creazione dei sessi. Ogni differenza nel ruolo di genere potrebbe essere attribuita e spiegata facendo riferimento alla differenza nella natura/creazione tra i sessi.

Perché le donne dovrebbero essere utili alla società solamente per il loro ruolo di bambinaie rispetto agli uomini che dovrebbero avere invece ruoli di sostegno economico? Differenza di natura. Perché le donne dovrebbero vestirsi in un certo modo, a differenza degli uomini? Differenza di natura. Perché alle donne dovrebbe essere impedito di diventare imam (leader religiosi), mentre gli uomini ricoprono quel ruolo? Differenza di natura. Perché il ruolo di califfo e di guida dello Stato dovrebbe essere riservato agli uomini e non alle donne? Differenza di natura.

Tutte le differenze di genere possono essere spiegate razionalmente in tal modo, ma questo fatto rappresenta un incubo per la filosofia femminista. Quindi, per evitarlo, le femministe devono insistere sull'uguaglianza speculare, ma ciò causa anche conflitti e incongruenze, come abbiamo visto. Questo è uno dei tanti problemi concettuali che affliggono il pensiero femminista, ma l'Islam e la Sharia sono completamente esenti da tali problemi. Allah ha dato a ciascuno di noi una posizione e un posto diversi nella società e nelle nostre famiglie, e ci giudicherà in base alla nostra taqwa (coscienza di Dio), non in base alla nostra ricchezza, al nostro status, alla nostra leadership, alla nostra posizione o a qualsiasi altro elemento da cui il femminismo è ossessionato.

### L'incomprensione dell'Islam e dei diritti delle donne

I malintesi relativi all'Islam derivano prima di tutto da una mancanza di conoscenza (ignoranza) dello status delle donne negli insegnamenti islamici. Infatti, nell'Islam alla donna è garantito un enorme rispetto, le vengono riconosciuti tutti i suoi diritti, l'Islam protegge la sua dignità e ordina agli uomini di trattare le donne con gentilezza e di non maltrattarle. Alcune persone osservano che in qualche società islamica vi sono regole che vanno contro gli insegnamenti dell'Islam, dove gli uomini maltrattano e opprimono le donne. L'Islam non ha nulla a che fare con tutto ciò; questi sono errori e la responsabilità per essi ricade su chi li commette ed è lui che sarà chiamato a renderne conto. Questi sbagli non devono essere attribuiti all'Islam.

In secondo luogo, i malintesi emergono perché non si osserva la reale situazione presente nella società, alla luce dei numeri ufficiali e delle statistiche che descrivono dolorosamente ciò che le donne stanno attraversando nelle società secolari, con l'illusione e l'inganno delle apparenti libertà a favore delle donne. Dobbiamo richiamare l'attenzione a livello mondiale sugli innumerevoli episodi di molestie sessuali, stupri e il terribile sfruttamento sessuale, notizie che sono ormai ovunque e di cui le persone ovunque ne parlano. Secondo la BBC, ad esempio, il 50% delle donne britanniche ha subito molestie sessuali di vario tipo. Analogamente in Francia, il 100% delle utenti dei trasporti pubblici ha subito molestie o aggressioni sessuali, secondo l'Alto Consiglio per l'Uguaglianza tra Uomini e Donne (HCEfh).

Con l'arrivo dell'Islam e dei suoi insegnamenti specifici, la vita delle donne è entrata in una nuova fase, una fase che è cambiata notevolmente da quella precedente. Grazie all'Islam, le donne hanno potuto godere di tutti i loro diritti individuali, sociali e umani. La base degli insegnamenti islamici riguardo alle donne è esattamente ciò che si legge nel Nobile Corano. Allah dice nel Corano:

{{ $page->verse('16:97') }}

L'Islam considera la donna, come l'uomo, completamente libera e indipendente. Dichiara che questa libertà è per tutte le persone, uomini e donne. Allah dice nel Corano:

{{ $page->verse('41:46') }}

Il matrimonio è il fondamento della vita familiare e l'unità fondamentale della società islamica, che ci conferisce diritti e doveri reciproci. Allah dice nel Corano:

{{ $page->verse('30:21') }}

Nel matrimonio, agli uomini è affidato il compito di essere responsabili delle donne, di prendersi cura di loro, di guidarle nel miglior modo possibile e di impartire ordini e divieti. Allah dice nel Corano:

{{ $page->verse('4:34..') }}

In quanto madre, la donna è preferita al padre per quanto riguarda il rispetto dei figli. E questo è dovuto al ruolo onorevole che svolge come cuore e anima della famiglia e, per estensione, della società. È stato narrato che Abu Hurayrah (che Allah sia soddisfatto di lui) disse:

{{ $page->hadith('muslim:2548') }}

In breve, l'Islam considera la donna un elemento fondamentale della società e, pertanto, non deve essere trattata come un'entità priva di volontà.

Il sistema islamico non si basa sull'individualismo egoistico. Nell'Islam, le donne non servono gli uomini, né gli uomini servono le donne. Piuttosto, serviamo Allah aiutandoci a vicenda e donandoci a vicenda in base ai bisogni umani, con la consapevolezza che gli esseri umani non sono tutti uguali. L'Islam fornisce una soluzione chiara, naturale e giusta per garantire giustizia a tutti gli esseri umani, e non ha bisogno dei vani tentativi del femminismo di reinventare la ruota che l'Islam ha messo in moto oltre 1400 anni fa.
