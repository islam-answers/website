---
extends: _layouts.answer
section: content
title: Qual é a Condição das Mulheres no Islã?
date: 2024-06-02
description: As mulheres muçulmanas têm o direito de possuir propriedades, ganhar
  dinheiro e gastá-lo como quiserem. O Islã encoraja os homens a tratarem bem mulheres.
sources:
- href: https://www.islam-guide.com/pt/ch3-13.htm
  title: islam-guide.com
---

O Islã vê a mulher, solteira ou casada, como um indivíduo independente, com o direito de ter e dispor de sua propriedade e ganhos sem qualquer custódia legal sobre ela (seja do pai, marido ou qualquer outra pessoa). Ela tem direito de comprar e vender, dar presentes e caridade, e pode gastar seu dinheiro como quiser. Um dote de casamento é dado pelo noivo para a noiva para seu uso pessoal, e ela mantém seu nome de família ao invés de tomar o nome da família do marido.

O Islã encoraja o marido a tratar sua esposa bem, como o Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('ibnmajah:1977') }}

As mães no Islã são altamente honradas. O Islã recomenda tratá-las da melhor maneira.

{{ $page->hadith('bukhari:5971') }}
