---
extends: _layouts.answer
section: content
title: Что ислам говорит об эволюции?
date: 2024-09-16
description: Аллах создал первого человека, Адама, в его окончательной форме. Исламские источники не дают подробностей о других живых существах.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 593513
---

Аллах создал первого человека, Адама, в его окончательной форме, а не постепенно эволюционировал из продвинутых обезьян. Это не может быть напрямую подтверждено или опровергнуто наукой, потому что это было уникальное и единичное историческое событие - чудо.

Что касается живых существ, кроме людей, исламские источники ничего не говорят по этому поводу, и требуют от нас лишь верить в то, что Аллах создал их так, как по его воля. используя эволюцию или каким-либо другим образом.
