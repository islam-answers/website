---
extends: _layouts.answer
section: content
title: L'Islam è contro i cani?
date: 2024-08-13
description: L’Islam insegna a rispettare gli animali. I cani sono ammessi solo per scopi specifici, non come animali domestici.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 573107
---

Gli animali sono una creazione di Allah e noi non li consideriamo negativamente; anzi, come musulmani abbiamo il dovere di trattarli in modo dignitoso, umano e misericordioso. Il Messaggero di Allah {{ $page->pbuh() }} ha detto,

{{ $page->hadith('bukhari:3321') }}

## Uccidere i cani

Abdallah ibn 'Umar disse:

{{ $page->hadith('muslim:1570b') }}

È importante chiarire a quale tipo di cani si fa riferimento in questa narrazione, oltre al contesto generale. I cani a cui si fa riferimento in questo caso sono branchi di cani selvatici che vagano per villaggi e città e costituiscono un disturbo per la comunità e un rischio per la salute. Portano e diffondono malattie, proprio come i ratti, e causano molti disagi alle persone. L'uccisione di animali pericolosi o che diffondono malattie è frequente e comunemente accettata in tutto il mondo.

Sulla base delle valutazioni complessive, i giuristi hanno affermato che l'ordine di uccidere i cani è lecito solo se si tratta di cani predatori e dannosi. Pertanto, un cane innocuo, indipendentemente dal colore, non può essere ucciso.

## Purezza

Secondo la maggior parte dei sapienti musulmani, la saliva dei cani è impura, così come la loro pelliccia secondo alcuni. Uno degli insegnamenti più importanti nell'Islam è la purezza a tutti i livelli, quindi questo aspetto è stato preso in considerazione, così come il fatto che gli angeli non frequentano un luogo dove ci sono cani. Ciò è dovuto alle parole del Profeta {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3322') }}

## Tenere i cani come animali domestici

L'Islam proibisce ai musulmani di tenere i cani come animali domestici. Il Messaggero di Allah {{ $page->pbuh() }} ha detto:

{{ $page->hadith('muslim:1574g') }}
