---
extends: _layouts.answer
section: content
title: Czy Jezus został ukrzyżowany?
date: 2024-10-13
description: Według Koranu Jezus nie został zabity ani ukrzyżowany. Jezus został wskrzeszony
  przez Allaha, a w jego miejsce ukrzyżowano innego człowieka.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 584496
---

Kiedy Żydzi i cesarz rzymski spiskowali, aby zabić Jezusa {{ $page->pbuh() }}, Allah uczynił jednego z obecnych ludzi podobnym pod każdym względem do Jezusa. Więc zabili człowieka, który wyglądał jak Jezus. Nie zabili Jezusa. Jezus {{ $page->pbuh() }} został wskrzeszony żywy do Niebios. Dowodem na to są słowa Allaha dotyczące fałszerstwa Żydów i jego obalenia:

{{ $page->verse('4:157-158') }}

Tak więc Allah uznał za fałszywe słowa Żydów, gdy twierdzili, że go zabili i ukrzyżowali, i oświadczył, że zabrał go do Siebie. To było miłosierdzie od Allaha dla Jezusa (Isa) {{ $page->pbuh() }} i był to zaszczyt dla niego, aby mógł być jednym z Jego znaków, zaszczyt, który Allah obdarza kogokolwiek zechce ze Swoich posłańców.

Implikacją słów "Ale Bóg wskrzesił go (Jezusa) (z jego ciałem i duszą) dla siebie" jest to, że Bóg, niech będzie uwielbiony, zabrał Jezusa w ciele i duszy, aby obalić twierdzenie Żydów, że ukrzyżowali go i zabili, ponieważ zabijanie i krzyżowanie mają związek z ciałem fizycznym. Co więcej, gdyby zabrał tylko duszę, nie wykluczyłoby to twierdzenia o zabiciu i ukrzyżowaniu, ponieważ zabranie tylko duszy nie byłoby obaleniem ich twierdzenia. Również imię Jezus {{ $page->pbuh() }}, w rzeczywistości odnosi się do Niego zarówno w duszy, jak i w ciele; nie może odnosić się tylko do jednego z nich, chyba że istnieje ku temu wskazówka. Co więcej, wzięcie zarówno jego duszy, jak i ciała razem wskazuje na doskonałą potęgę i mądrość Allaha oraz na Jego honor i wsparcie dla kogokolwiek zechce spośród Jego Posłańców, zgodnie z tym, co On, niech będzie wywyższony, mówi na końcu wersetu: "A Allah jest wszechmocny, wszechmądry"
