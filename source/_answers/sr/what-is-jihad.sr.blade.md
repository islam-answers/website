---
extends: _layouts.answer
section: content
title: Što je džihad?
date: 2024-11-20
description: Suština džihada je borba i žrtvovanje za svoju veru na način koji je
  ugodan Bogu.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 584497
---

Suština džihada je borba i žrtvovanje za svoju veru na način koji je ugodan Bogu. Jezično, to znači "boriti se" i može se odnositi na nastojanje da se čine dobra dela, davanje milostinje ili borba za dobrobit Boga.

Najčešći poznati oblik je vojni džihad koji je dopušten kako bi se očuvalo blagostanje društva, sprečilo širenje ugnjetavanja i promovisala pravda.
