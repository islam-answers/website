---
extends: _layouts.answer
section: content
title: Kako neko postaje musliman?
date: 2024-10-18
description: 'Jednostavnim izgovaranjem s uvjerenjem: “La ilahe illa Allah, Muhammadu
  rrasoolu Allah,” prelazi se na islam i postaje musliman.'
sources:
- href: https://www.islam-guide.com/ch3-6.htm
  title: islam-guide.com
translator_id: 584497
---

Jednostavnim izgovaranjem s uvjerenjem: “La ilahe illa Allah, Muhammadu rrasoolu Allah,” prelazi se na islam i postaje musliman. Ova izreka znači "Nema istinskog boga (božanstva) osim Boga (Allaha), a Muhammed je Božiji poslanik (prorok)." Prvi deo, "Nema pravog boga osim Boga", znači da niko nema pravo biti obožavan osim samoga Boga, te da Bog nema ni partnera ni sina. Da bi neko bio musliman, također treba:

- Vjerujte da je Časni Kur'an doslovna Božija riječ koju je On objavio.
- Vjerujte da je Sudnji dan (Dan proživljenja) istina i da će doći, kao što je Bog obećao u Kur'anu.
- Prihvatiti islam kao svoju vjeru.
- Ne obožavati ništa i nikoga osim Boga.

Poslanik Muhamed {{ $page->pbuh() }} je rekao

{{ $page->hadith('muslim:2747') }}
