---
extends: _layouts.answer
section: content
title: ¿Necesita testigos para decir Shahadah?
date: 2024-07-14
description: No es necesario declarar nuestro Islam ante testigos para que sea válido.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 554943
---

Para que una persona se convierta en musulmana, no es necesario que declare su Islam ante nadie. El Islam es una cuestión entre la persona y su Señor, bendito y alabado sea.

Si esta persona pide que alguien testifique su Islam para que pueda registrarse entre sus documentos personales, no hay nada de malo en esto, pero debe hacerse sin que sea una condición para que el Islam sea válido.
