---
extends: _layouts.answer
section: content
title: Por que os muçulmanos não podem beber álcool?
date: 2024-10-23
description: O consumo de álcool é um pecado grave e é a chave de todos os males.
  Enevoa a mente e desperdiça dinheiro. Allah proibiu tudo o que prejudica o corpo
  e a mente.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
translator_id: 594172
---

O Islam veio para trazer e aumentar as coisas boas e para afastar e reduzir as coisas más. Tudo o que é benéfico ou maioritariamente benéfico é permitido (Halal) e tudo o que é prejudicial ou maioritariamente prejudicial é proibido (Haram). O álcool insere-se, sem dúvida, na segunda categoria. Allah diz:

{{ $page->verse('2:219') }}

Os efeitos nocivos e nefastos do álcool são bem conhecidos de todos. Entre os efeitos nocivos do álcool está o que foi mencionado por Allah:

{{ $page->verse('5:90-91') }}

O álcool conduz a muitas coisas prejudiciais e merece ser chamado "a mãe de todos os males" - como foi descrito pelo Profeta Muhammad {{ $page->pbuh() }}, que disse:

{{ $page->hadith('ibnmajah:3371') }}

Cria inimizade e ódio entre as pessoas, impede-as de se lembrarem de Allah e de rezarem, incita-as à zina [relações sexuais ilícitas] e pode mesmo incitá-las a cometer incesto com as suas filhas, irmãs ou outras parentes do sexo feminino. Tira o orgulho e o ciúme protetor, gera vergonha, arrependimento e desgraça. Leva à revelação de segredos e à exposição de faltas. Incentiva as pessoas a cometerem pecados e más acções.

Além disso, Ibn Umar narrou que o Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2003a') }}

O Profeta {{ $page->pbuh() }} também disse:

{{ $page->hadith('ibnmajah:3381') }}
