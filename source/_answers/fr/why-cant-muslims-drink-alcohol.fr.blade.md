---
extends: _layouts.answer
section: content
title: Pourquoi les musulmans ne peuvent-ils pas boire d'alcool ?
date: 2024-10-15
description: Boire de l'alcool est un péché majeur qui trouble l'esprit et gaspille l'argent. Allah a interdit tout ce qui nuit au corps et à l'esprit.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
translator_id: 586055
---

L'islam est venu pour apporter et accroître les bonnes choses, et pour écarter et réduire les mauvaises choses. Tout ce qui est bénéfique ou plutôt bénéfique est permis (halal) et tout ce qui est nuisible ou plutôt nuisible est interdit (haram). L'alcool appartient sans aucun doute à la deuxième catégorie. Allah dit :

{{ $page->verse('2:219') }}

Les effets nocifs et néfastes de l’alcool sont bien connus de tous. Parmi les effets nocifs de l’alcool, il y a celui qu’Allah a mentionné :

{{ $page->verse('5:90-91') }}

L'alcool entraîne de nombreuses conséquences néfastes et mérite d'être appelé "la clé de tout mal", comme l'a décrit le prophète Muhammad {{ $page->pbuh() }}, qui a déclaré : "L'alcool est la clé de tous les maux" :

{{ $page->hadith('ibnmajah:3371') }}

Elle crée de l’inimitié et de la haine entre les gens, les empêche d’invoquer Allah et d’accomplir la prière, les incite à la zina [relations sexuelles illicites] et peut même les inciter à commettre l’inceste avec leurs filles, leurs sœurs ou d’autres femmes de leur famille. Elle enlève l’orgueil et la jalousie protectrice, engendre la honte, le regret et la disgrâce. Elle conduit à la divulgation des secrets et à la révélation des fautes. Elle encourage les gens à commettre des péchés et des actes mauvais.

De plus, Ibn 'Umar a rapporté que le Prophète {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:2003a') }}

Le Prophète {{ $page->pbuh() }} a également dit :

{{ $page->hadith('ibnmajah:3381') }}
