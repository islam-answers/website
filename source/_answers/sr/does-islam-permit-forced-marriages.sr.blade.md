---
extends: _layouts.answer
section: content
title: Da li islam dozvoljava prisilne brakove?
date: 2024-07-29
description: Oboje, i muškarci i žene imaju pravo da izaberu svog supružnika, a brak
  se smatra ništavnim bez istinskog odobrenja žene
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 584497
---

Ugovoreni brakovi su dominantna kulturološka praksa u određenim zemljama svijeta. Iako nisu ograničeni na muslimane, prisilni brakovi su postali pogrešno povezani sa islamom.

U islamu, i muškarci i žene imaju pravo da izaberu ili odbace svog potencijalnog supružnika, a brak se smatra ništavnim i nevažećim ukoliko žena pre braka nije dala istinsko odobrenje i pristala na brak.
