---
extends: _layouts.answer
section: content
title: Le Coran a-t-il été copié sur la Bible ?
date: 2024-12-26
description: Le Coran n'a pas été copié sur la Bible. Le prophète Muhammad était analphabète, il n’y avait pas de Bible en arabe, et les révélations venaient d’Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 573111
---

Allah a créé tout ce qui nous entoure dans un but précis. L'homme, lui aussi, a été créé dans un but bien défini : adorer Allah. Allah dit dans le Coran :

{{ $page->verse('51:56') }}

Pour guider l'humanité, Allah a envoyé des prophètes à différentes époques. Ces prophètes ont reçu la révélation d'Allah, et chaque religion possède son propre recueil d'écritures divines, qui constituent le fondement de ses croyances. Ces écritures sont la transcription de la révélation divine, soit directement, comme dans le cas du prophète Moïse (Musa), qui a reçu les commandements d'Allah, soit indirectement, comme dans le cas de Jésus (Isa) et de Muhammad - {{ $page->pbuh() }} - qui ont transmis au peuple la révélation communiquée par l'ange Gabriel (Jibreel).

Le Coran demande à tous les musulmans de croire aux écritures qui l'ont précédé. Cependant, en tant que musulmans, nous croyons que la Torah (Tawrah), le Livre des Psaumes (Zaboor) et l'Évangile (Injeel) sont la parole d'Allah dans leur forme originale, telle qu'elle a été révélée.

## Le prophète Muhammad aurait-il pu copier le Coran à partir de la Bible ?

La discussion sur la manière dont le Prophète Muhammad,{{ $page->pbuh() }}, aurait pu apprendre les histoires des Gens du Livre, puis les transmettre dans le Coran – ce qui impliquerait qu'il ne s'agirait pas d'une révélation – porte sur diverses allégations. Ces allégations suggèrent qu'il, {{ $page->pbuh() }}, aurait pu apprendre ces histoires directement à partir de leurs livres saints, ou verbalement, de la part des Gens du Livre, concernant les récits, les commandements et les interdictions contenus dans leurs écritures, s'il lui était impossible d'étudier ces livres lui-même.

Ces allégations se résument en trois points fondamentaux :

1. L'allégation selon laquelle le Prophète Muhammad, {{ $page->pbuh() }}, n'était ni inculte ni analphabète
1. L'allégation que les écritures chrétiennes étaient accessibles au Prophète, {{ $page->pbuh() }}, et qu'il pouvait les citer ou les copier
1. L'allégation que La Mecque était un centre majeur d'enseignement pour l'étude des écritures

Premièrement, le texte du Coran et de la Sunna indique clairement en de nombreux endroits, sans laisser de place au doute, que le Prophète Muhammad, {{ $page->pbuh() }}, était effectivement analphabète et qu'il n'a jamais raconté aucune des histoires des Gens du Livre avant que la révélation ne lui parvienne, ni écrit quoi que ce soit de sa propre main. Il ne connaissait rien de ces récits avant le début de sa mission et n'en a jamais parlé auparavant.

Allah, exalté soit-Il, dit :

{{ $page->verse('29:48') }}

Les livres sur la biographie et l'histoire du Prophète ne mentionnent pas que le Prophète écrivait la révélation ou qu'il rédigeait lui-même ses lettres aux rois. C'est plutôt l'inverse qui est mentionné : le Prophète Muhammad, {{ $page->pbuh() }}, avait des scribes qui écrivaient la révélation et d'autres documents. Dans un texte brillant d'Ibn Khaldoun, on trouve une description du niveau d'éducation dans la péninsule arabique juste avant le début de la mission du Prophète. Il écrit :

> Chez les Arabes, l'écriture était plus rare que les œufs de chamelle. La plupart des gens étaient analphabètes, en particulier les habitants du désert, car cette compétence était généralement réservée aux citadins.

C'est pourquoi les Arabes ne qualifiaient pas une personne d'analphabète ; ils disaient plutôt d'une personne qui savait lire et écrire qu'elle était savante, car savoir lire et écrire était l'exception, et non la norme, parmi les gens.

Deuxièmement, il n'y a aucune preuve, indication ni suggestion qu'une traduction arabe de la Bible ait existé à cette époque. Al-Bukhari a rapporté d'Abu Hurayrah (qu'Allah soit satisfait de lui) qu'il a dit :

{{ $page->hadith('bukhari:7362') }}

Ce hadith indique que les Juifs avaient le monopole du texte et de son explication. Si leurs textes avaient été connus en arabe, les Juifs n'auraient pas eu besoin de lire le texte ni de l'expliquer [aux musulmans].

Cette idée est étayée par le verset dans lequel Allah, exalté soit-Il, dit :

{{ $page->verse('3:78') }}

Le livre le plus important écrit sur ce sujet est peut-être celui de Bruce Metzger, professeur de langue et de littérature du Nouveau Testament, The Bible in Translation, dans lequel il dit :

> Il est très probable que la plus ancienne traduction arabe de la Bible date du huitième siècle.

L'orientaliste Thomas Patrick Hughes a écrit :

> Il n'y a aucune preuve que Muhammad ait lu les Écritures chrétiennes... Il convient de noter qu'il n'existe aucune preuve évidente suggérant qu'une traduction arabe de l'Ancien et du Nouveau Testament ait existé avant l'époque de Muhammad.

Troisièmement, l'idée que le Prophète aurait reçu la révélation des gens est une vieille accusation. Les polythéistes l'ont accusé de cela, et le Coran les a réfutés, comme le dit Allah, exalté soit-Il :

{{ $page->verse('25:5-6') }}

En outre, l'affirmation selon laquelle le Prophète Muhammad, {{ $page->pbuh() }}, aurait acquis le savoir des Gens du Livre lorsqu'il se trouvait à La Mecque est réfutée par le statut académique et culturel de La Mecque et par la véritable nature du lien que cette société arabe entretenait avec le savoir des Gens du Livre.

Par conséquent, après avoir prouvé que le Prophète Muhammad, {{ $page->pbuh() }}, n'a pas appris le savoir des Gens du Livre directement ou par un intermédiaire, nous n'avons d'autre choix que d'affirmer qu'il s'agit d'une révélation d'Allah à Son Prophète élu.
