---
extends: _layouts.answer
section: content
title: Islam nói gì về Ngày Phán quyết?
date: 2024-06-30
description: Đến một ngày kia khi cả vũ trụ bị hủy diệt và người chết sẽ hồi sinh
  để nghe phán quyết của Thượng Đế.
sources:
- href: https://www.islam-guide.com/vi/ch3-5.htm
  title: islam-guide.com
---

Cũng giống như người Cơ Đốc giáo, người Islam tin rằng cuộc sống hiện tại chỉ là sự chuẩn bị mang tính thử nghiệm cho kiếp sau. Cuộc sống hiện tại là một kỳ sát hạch đối với mỗi cá nhân cho cuộc sống tiếp theo của họ. Đến một ngày kia khi cả vũ trụ bị hủy diệt và người chết sẽ hồi sinh để nghe phán quyết của Thượng Đế. Ngày đó sẽ là sự khởi đầu của một cuộc sống vĩnh hằng. Ngày đó là Ngày Phán quyết. Vào ngày đó, tất cả mọi người sẽ được Thượng Đế tưởng thưởng tùy theo đức tin và hành vi của họ. Những ai chết đi mà vẫn tin rằng “không có thần linh nào khác ngoại trừ Thượng Đế, và Muhammad là Người đưa tin (Thiên Sứ) của Thượng Đế” và những ai là người Islam sẽ được tưởng thưởng trong ngày phán quyết rồi sẽ được sống ở Thiên đàng mãi mãi, như Thượng Đế nói:

{{ $page->verse('2:82') }}

Còn những ai chết đi mà không tin rằng “không có thần linh nào khác ngoại trừ Thượng Đế, và Muhammad là Người đưa tin (Thiên Sứ) của Thượng Đế” và những ai không phải là người Islam sẽ không bao giờ tới được Thiên đàng, ngược lại họ sẽ bị đày tới Hỏa ngục, như Thượng Đế nói:

{{ $page->verse('3:85') }}

Và như Ngài đã nói:

{{ $page->verse('3:91') }}

Ai đó có thể hỏi, ‘Tôi biết Islam là tôn giáo tốt, nhưng nếu tôi chuyển sang theo Islam thi gia đình tôi, bạn bè tôi và những người khác sẽ làm khổ hoặc cười nhạo tôi. Vậy nếu tôi không chuyển sang theo Islam, liệu tôi có được đến Thiên đàng và thoát khởi Hỏa ngục?’

Câu trả lời là những gì mà Thượng Đế đã nói trong câu trước “Và những ai tìm kiếm tôn giáo khác không phải Islam, thì sự tìm kím đó sẽ không được (Thượng Đế) chấp nhận và người đó ở Đời sau sẽ là một trong những người thua cuộc.”

Sau khi đã phái Thiên Sứ Muhammad {{ $page->pbuh() }} đến để kêu gọi mọi người đi theo Islam, Thượng Đế không chấp nhận việc ai đó đi theo bất cứ tôn giáo nào khác ngoài Islam. Thượng Đế là Đấng tạo hóa và nâng đỡ chúng ta. Ngài đã tạo cho chúng tai mọi thứ trên trái đất. Mọi phúc lành và những điều tốt đẹp mà chúng ta có được đều do Ngài ban tặng. Do đó, nếu có ai đó từ chối tin vào Thượng Đế, tin vào Thiên Sứ Muhammad {{ $page->pbuh() }}, và tôn giáo Islam của Ngài, thì người đó sau này sẽ bị trừng phạt. Trên thực tế, mục đích chính của việc tạo ra chúng ta là thờ phụng duy nhất Thượng Đế và tuân theo lời của Ngài, như Thượng Đế nói trong Thánh Kinh Qur'an (51:56).

Cuộc sống mà chúng ta đang sống đây rất ngắn ngủi. Những người không có lòng tin vào Ngày Phán quyết sẽ nghĩ rằng cuộc sống mà họ đã sống trên trái đất chỉ là một ngày hoặc một phần của ngày, như Thượng Đế hỏi:

{{ $page->verse('23:112-113..') }}

Và Ngài hỏi tiếp:

{{ $page->verse('23:115-116..') }}

Quả thật, cuộc sống ở kiếp sau là một cuộc sống thực sự. Nó không chỉ là một cuộc sống bằng linh hồn mà còn là một cuộc sống bằng thể xác. Chúng ta sẽ sống ở đó với tâm hồn lẩn thể xác của mình.

Khi so sánh cuộc sống hiện tại với cuộc sống kiếp sau, Thiên Sứ Muhammad {{ $page->pbuh() }} nói:

{{ $page->hadith('muslim:2858') }}

Câu này có nghĩa là giá trí của thế giới này nếu đem so với giá trị của thế giới Tương lai cũng chỉ giống như vài giọt nước so với biển cả.
