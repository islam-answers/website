---
extends: _layouts.answer
section: content
title: Was ist der Islam?
date: 2024-12-01
description: Islam bedeutet, sich Allah zu unterwerfen, Ihm zu gehorchen und Seine Gebote zu befolgen. Es bedeutet, nur Allah zu verehren und an Ihn zu glauben.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 587746
---

Wenn man in arabische Wörterbücher nachschlägt, wird man feststellen, dass die Bedeutung des Wortes Islam Folgendes ist: Unterwerfung, sich demütigen, den Befehlen zu gehorchen und den Verboten ohne Einwände zu folgen, Allah allein aufrichtig zu verehren, das zu glauben, was Er uns sagt, und an Ihn zu glauben.

Das Wort Islam ist der Name der Religion, die vom Propheten Muhammad {{ $page->pbuh() }} eingeführt wurde.

### Warum diese Religion Islam genannt wird

Alle Religionen der Erde haben unterschiedliche Namen, entweder den Namen einer bestimmten Person oder einer bestimmten Nation. Das Christentum hat seinen Namen von Christus; der Buddhismus hat seinen Namen von seinem Gründer, dem Buddha; ebenso hat das Judentum seinen Namen von einem Stamm namens Yehudah (Juda) und wurde daher als Judentum bekannt. Und so weiter.

Mit Ausnahme des Islam. Denn dieser wird nicht mit einem bestimmten Mann oder einer bestimmten Nation in Verbindung gebracht, sondern sein Name bezieht sich auf die Bedeutung des Wortes „Islam“. Was dieser Name bedeutet, ist, dass die Etablierung und Gründung dieser Religion nicht das Werk eines bestimmten Mannes war und dass sie nicht nur für eine bestimmte Nation gilt, zum Ausschluss aller anderen. Vielmehr ist es ihr Ziel, allen Völkern der Erde die Eigenschaft zu verleihen, die im Wort „Islam“ impliziert ist. Daher ist jeder, der diese Eigenschaft erlangt, ob er aus der Vergangenheit oder der Gegenwart stammt, ein Muslim, und jeder, der diese Eigenschaft in der Zukunft erlangt, wird ebenfalls ein Muslim sein.

Der Islam ist die Religion aller Propheten. Er erschien zu Beginn des Prophetentums, seit der Zeit unseres Vaters Adam, und alle Botschaften riefen in Bezug auf Glauben und Grundregeln zum Islam (Unterwerfung unter Allah) auf. Die Gläubigen, die den früheren Propheten folgten, waren allgemein gesehen Muslime, da sie den Lehren und Geboten Allahs folgten. Unabhängig von der spezifischen Zeit oder den Umständen war der Kern ihres Glaubens die Unterwerfung unter den einzig wahren Gott, und sie werden aufgrund ihrer Hingabe zum Islam ins Paradies eintreten.
