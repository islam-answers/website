---
extends: _layouts.answer
section: content
title: Czym jest Dżihad?
date: 2024-08-04
description: Istotą Dżihadu jest walka i poświęcenie dla swojej religii w sposób,
  który jest miły Bogu.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 584496
---

Podstawą Dżihadu jest walka i poświęcenie dla swojej religii w sposób, który jest miły dla Boga. Lingwistycznie oznacza to „walkę” i może odnosić się do dążenia do dobrych uczynków, dawania jałmużny, czy walki w imię Boga.

Najbardziej znaną formą jest dżihad wojskowy, który jest dozwolony w celu zachowania dobrobytu społeczeństwa, zapobiegania rozprzestrzenianiu się ucisku i promowania sprawiedliwości.
