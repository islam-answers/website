---
extends: _layouts.answer
section: content
title: Despre ce este Coranul?
date: 2024-02-16
description: Sursa principală a credinței și practicii fiecărui musulman.
sources:
- href: https://www.islam-guide.com/ch3-7.htm
  title: islam-guide.com
translator_id: 554943
---

Coranul, ultimul cuvânt revelat al lui Dumnezeu, este sursa principală a credinței și practicii fiecărui musulman. Ea tratează toate subiectele care privesc ființele umane: înțelepciunea, doctrină, cult, tranzacții, legea etc., dar tema sa de bază este relația dintre Dumnezeu și creaturile Sale. În același timp, oferă linii directoare și învățături detaliate pentru o societate justă, o conduită umană adecvată și un sistem economic echitabil.

Coranul i-a fost revelat Profetului Muhammad {{ $page->pbuh() }} numai în arabă. Deci, orice traducere a Coranului, fie în engleză, fie în orice altă limbă, nu este nici un Coran, nici o versiune a Coranului, ci este doar o traducere a sensului Coranului. Coranul există doar în limba arabă în care a fost revelat.
