---
extends: _layouts.answer
section: content
title: Musulmanii venerează Kaaba?
date: 2024-12-15
description: Musulmanii îl venerează doar pe Allah, respectându-I poruncile, cum ar fi rugăciunea către Kaaba - un simbol al unității și monoteismului.
sources:
- href: https://islamqa.info/en/answers/82349/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/85437/
  title: islamweb.net
- href: https://www.islamweb.net/en/article/141834/
  title: islamweb.net
translator_id: 554943
---

Kaaba este Qiblah (direcția) spre care toți musulmanii își întorc fața în timpul rugăciunilor. Este o datorie să ne întoarcem fețele spre Kaaba în timpul rugăciunii. Allah spune în Coran:

{{ $page->verse('2:144..') }}

În fiecare an, peste 2,5 milioane de pelerini musulmani din întreaga lume îndeplinesc ritualurile de Hajj (pelerinaj) la Mecca, unde se află Kaaba. Din punct de vedere lingvistic, Kaaba înseamnă pur și simplu "cub" în arabă, dar este mult mai mult decât o clădire în formă de cub drapată în negru. Este simbolul unității islamice, în inima islamului, și se bazează pe principiul monoteismului.

Făcând referire la Kaaba, Allah ne spune în Coran că:

{{ $page->verse('3:96') }}

La acea vreme, bazele Kaabei nu fuseseră încă ridicate de Ibrahim (Avraam), care mai târziu avea să construiască Kaaba împreună cu fiul său Ismail (Ismael), așa cum a poruncit Allah:

{{ $page->verse('22:26') }}

Aici, vedem că scopul acestei case era adorarea exclusivă a lui Allah, fără parteneri. Avraam și-a petrecut întreaga viață respingând politeismul, fiind împotriva credințelor și obiceiurilor întregii sale comunități, ale liderilor acesteia, și chiar împotriva celor ale tatălui său, care obișnuia să făurească idoli cu propriile mâini.

Ca musulmani, principiul nostru fundamental este că îl adorăm doar pe Allah. Acțiuni precum rugăciunea către Kaaba și Tawaf (înconjurarea acesteia) reprezintă îndeplinirea poruncilor lui Allah, nu venerarea obiectului în sine. Există diferențe majore între actul de a te ruga spre Kaaba și venerarea idolilor.
