---
extends: _layouts.answer
section: content
title: Jésus a-t-il été crucifié ?
date: 2024-09-21
description: Selon le Coran, Jésus n’a pas été tué ou crucifié. Jésus a été élevé
  vivant au ciel par Allah, et un autre homme a été crucifié à sa place.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 586055
---

Lorsque les Juifs et l'empereur romain ont comploté pour tuer Jésus {{ $page->pbuh() }}, Allah a fait en sorte que l'une des personnes présentes ressemble en tout point à Jésus. Ils ont donc tué l'homme qui ressemblait à Jésus. Ils n'ont pas tué Jésus. Jésus {{ $page->pbuh() }} a été ressuscité vivant dans les cieux. La preuve en est la parole d'Allah sur l'affabulation des Juifs et sa réfutation :

{{ $page->verse('4:157-158') }}

C'est ainsi qu'Allah a déclaré mensongères les paroles des Juifs qui prétendaient l'avoir tué et crucifié, et qu'Il a déclaré l'avoir fait monter vers Lui. C'était une miséricorde d'Allah pour Jésus (Isa) {{ $page->pbuh() }}, et un honneur pour lui, afin qu'il soit l'un de Ses signes, honneur qu'Allah accorde à qui Il veut parmi Ses messagers.

L'implication des mots "Mais Allah l'a élevé (Jésus) avec son corps et son âme" est qu'Allah, qu'Il soit glorifié, a élevé Jésus avec son corps et son âme, afin de réfuter l'affirmation des Juifs selon laquelle ils l'ont crucifié et tué, parce que tuer et crucifier ont à voir avec le corps physique. De plus, s'il n'avait repris que son âme, cela n'exclurait pas l'affirmation de l'avoir tué et crucifié, car reprendre seulement l'âme ne serait pas une réfutation de leur affirmation. De même, le nom de Jésus {{ $page->pbuh() }}, en réalité, le désigne à la fois dans son âme et dans son corps ; il ne peut se référer à l'un d'eux seulement, à moins qu'il n'y ait une indication en ce sens. De plus, le fait de prendre en charge à la fois son âme et son corps témoigne de la puissance et de la sagesse parfaites d'Allah, ainsi que de Son honneur et de Son soutien à qui Il veut parmi Ses messagers, conformément à ce qu'Il dit, qu'Il soit exalté, à la fin du verset : "Et Allah est toujours tout-puissant : "Et Allah est toujours Puissant et Sage"
