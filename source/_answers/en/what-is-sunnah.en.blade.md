---
extends: _layouts.answer
section: content
title: What is Sunnah?
date: 2024-12-09
description: The Sunnah, divinely inspired, complements the Quran by clarifying and
  expanding its rulings for Islamic practice.
sources:
- href: https://islamqa.info/en/answers/77243/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145520/
  title: islamqa.info
---

The Sunnah – which means the words and deeds that are attributed to the Prophet Muhammad {{ $page->pbuh() }} as well as the actions that he approved – is one of the two parts of divine revelation (Wahy) that were revealed to the Messenger of Allah {{ $page->pbuh() }}. The other part of the revelation is the Holy Quran. Allah says:

{{ $page->verse('53:3-4') }}

It was narrated that the Messenger of Allah {{ $page->pbuh() }} said:

{{ $page->hadith('ibnmajah:12') }}

Hassaan ibn ‘Atiyah said:

> Jibreel (angel Gabriel) used to bring the sunnah down to the Prophet {{ $page->pbuh() }} as he used to bring the Quran down to him.

The importance of the Sunnah is first of all that it explains the Quran and is a commentary on it, then it adds some rulings to those in the Quran. Allah says:

{{ $page->verse('..16:44') }}

Ibn ‘Abd al-Barr said:

> The commentary of the Prophet {{ $page->pbuh() }} on the Quran is of two types:
>
> 1. Explaining things that are mentioned in general terms in the Holy Quran, such as the five daily prayers, their times, prostration, bowing and all other rulings.
> 1. Adding rulings to the rulings of the Quran, such as the prohibition on being married to a woman and to her paternal or maternal aunt at the same time.

The Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('bukhari:3461') }}
