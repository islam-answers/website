---
extends: _layouts.answer
section: content
title: Quem escreveu o Alcorão?
date: 2024-06-02
description: O Alcorão é a palavra literal de Deus, que Ele revelou ao Seu Profeta
  Muhammad através do Anjo Gabriel.
sources:
- href: https://www.islam-guide.com/pt/ch1-1.htm
  title: islam-guide.com
---

Deus apoiou Seu último Profeta Muhammad com muitos milagres e muitas evidências que provaram que ele é um verdadeiro Profeta enviado por Deus. Também, Deus apoiou seu último livro revelado, o Alcorão Sagrado, com muitos milagres que provam que o Alcorão é a palavra literal de Deus, revelado por Ele, e não de autoria de nenhum ser humano.

O Alcorão é a palavra literal de Deus, que Ele revelou ao Seu Profeta Muhammad {{ $page->pbuh() }} através do Anjo Gabriel. Ele foi memorizado por Muhammad {{ $page->pbuh() }}, que então o ditou aos seus Companheiros. Eles, por sua vez, o memorizaram, registraram por escrito, e o revisaram com o Profeta Muhammad {{ $page->pbuh() }}. Além disso, o Profeta Muhammad {{ $page->pbuh() }} revisava o Alcorão com o Anjo Gabriel uma vez por ano e o revisou duas vezes no último ano de sua vida. Da época em que o Alcorão foi revelado, até esse dia, houve um enorme número de muçulmanos que memorizaram todo o Alcorão, palavra por palavra. Alguns deles foram capazes de memorizar todo o Alcorão por volta dos dez anos de idade. Nem uma letra do Alcorão foi mudada ao longo dos séculos.

O Alcorão, que foi revelado há quatorze séculos atrás, mencionou fatos apenas recentemente descobertos ou provados pelos cientistas. Isso prova sem dúvida que o Alcorão deve ser a palavra literal de Deus, revelada por Ele ao Profeta Muhammad {{ $page->pbuh() }}, e que o Alcorão não é de autoria de Muhammad {{ $page->pbuh() }} ou de qualquer outro ser humano. Também prova que Muhammad {{ $page->pbuh() }} é verdadeiramente um profeta enviado por Deus. Está além da razão que alguém há quatorze séculos atrás pudesse conhecer fatos descobertos ou provados apenas recentemente com equipamentos avançados e métodos científicos sofisticados.
