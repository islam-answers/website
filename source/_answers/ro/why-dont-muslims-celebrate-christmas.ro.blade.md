---
extends: _layouts.answer
section: content
title: De ce musulmanii nu sărbătoresc Crăciunul?
date: 2024-12-22
description: Musulmanii îl consideră pe Iisus un mare profet, dar îi resping divinitatea.
  Crăciunul, înrădăcinat în credințe non-islamice, nu este permis în Islam.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 554943
---

Musulmanii îl respectă pe Iisus {{ $page->pbuh() }}, fiul lui Maryam (Maria) și așteaptă a doua sa venire. Ei îl consideră unul dintre cei mai mari mesageri ai lui Dumnezeu pentru omenire. Credința unui musulman nu este valabilă decât dacă acesta crede în toți mesagerii lui Allah, inclusiv în Iisus {{ $page->pbuh() }}.

Dar musulmanii nu cred că Iisus a fost Dumnezeu sau Fiul lui Dumnezeu. Musulmanii nu cred nici că a fost răstignit. Allah l-a trimis pe Iisus la israeliți pentru a-i chema să creadă doar în Allah și să se închine numai Lui. Allah l-a sprijinit pe Iisus cu miracole care au dovedit că el spunea adevărul.

Allah spune în Coran:

{{ $page->verse('19:88-92') }}

### Este permisă sărbătorirea Crăciunului?

Având în vedere aceste diferențe teologice, musulmanilor nu le este permis să sărbătorească festivalurile altor religii sau să participe la aceste ritualuri sau obiceiuri. Mai degrabă, aceste zile de sărbătoare sunt zile obișnuite pentru musulmani.

Sărbătoarea creștină a Crăciunului este o tradiție care încorporează credințe și practici care nu sunt înrădăcinate în învățăturile islamice. Ca atare, musulmanilor nu le este permis să ia parte la aceste sărbători, deoarece ele nu se aliniază cu înțelegerea islamică a lui Iisus {{ $page->pbuh() }} sau cu învățăturile lui.

Pe lângă faptul că este o inovație, se încadrează în imitarea necredincioșilor în chestiuni care sunt unice pentru ei și religia lor. Profetul Muhammad {{ $page->pbuh() }} spus:

{{ $page->hadith('abudawud:4031') }}

Ibn `Uthaymin (Allah să aibă milă de el) a spus:

> Felicitarea creștinilor cu ocazia Crăciunului sau a oricăruia dintre celelalte festivaluri religioase ale acestora este interzisă, deoarece implică aprobarea a ceea ce ei urmează din necredință. Chiar dacă nu aprobă această necredință pentru el însuși, este interzis pentru un musulman să aprobe ritualurile legate de aceasta sau să felicite pe cel care participă la ele. Nu este permis musulmanilor să îi imite în niciun fel care este unic pentru sărbătorile lor, fie că este vorba de mâncare, haine, baie, aprinderea focului sau abținerea de la munca sau cultul obișnuite și așa mai departe. Și nu este permis să dai un ospăț sau să faci schimb de cadouri sau să vinzi lucruri care îi ajută să își sărbătorească festivalurile (...) sau să te împodobești sau să pui decorațiuni.

Enciclopedia Britannica afirmă următoarele:

> Cuvântul "Crăciun" este derivat din engleza veche "Cristes maesse", "Masa lui Hristos". Nu există o narațiune autentică cu privire la data nașterii lui Iisus. Cronografii creștini din secolul al III-lea credeau că creația lumii a avut loc la echinocțiul de primăvară, calculat atunci la 25 martie; prin urmare, noua creație prin întrupare (adică concepția) și moartea lui Hristos trebuie să fi avut loc în aceeași zi, nașterea sa urmând nouă luni mai târziu, la solstițiul de iarnă, 25 decembrie.

> Motivul pentru care Crăciunul a ajuns să fie sărbătorit pe 25 decembrie rămâne incert, dar cel mai probabil motivul este că primii creștini au dorit ca data să coincidă cu festivalul roman păgân care marchează "ziua de naștere a soarelui necucerit" ) (natalis solis invicti); acest festival sărbătorea solstițiul de iarnă, când zilele încep din nou să se lungească și soarele începe să urce mai sus pe cer. Prin urmare, obiceiurile tradiționale legate de Crăciun s-au dezvoltat din mai multe surse, ca urmare a coincidenței dintre sărbătoarea nașterii lui Hristos și sărbătorile păgâne agricole și solare din mijlocul iernii. În lumea romană, Saturnaliile (17 decembrie) erau o perioadă de veselie și de schimb de cadouri.  De asemenea, 25 decembrie era considerată data de naștere a zeului misterios iranian Mithra, Soarele Dreptății. De Anul Nou roman (1 ianuarie), casele erau decorate cu verdeață și lumini, iar copiilor și săracilor li se dădeau cadouri.

Așadar, după cum poate vedea orice persoană rațională, nu există nicio bază solidă pentru Crăciun; nici Iisus {{ $page->pbuh() }} sau adevărații săi urmași nu au sărbătorit Crăciunul sau nu au cerut nimănui să sărbătorească Crăciunul, nici nu există vreo înregistrare a celor care se numesc creștini sărbătorind Crăciunul până la câteva sute de ani după Iisus. Așadar, au fost însoțitorii lui Iisus îndrumați mai corect (i.e. să nu sărbătorească Crăciunul), sau  oamenii de astăzi?

Așadar, dacă dorești să îl respecți pe Iisus {{ $page->pbuh() }}, așa cum fac musulmanii, nu sărbători un eveniment inventat care a fost ales să coincidă cu festivaluri păgâne și să copieze obiceiuri păgâne. Crezi că Dumnezeu, sau chiar Iisus însuși, ar aproba, sau ar condamna un astfel de lucru?
