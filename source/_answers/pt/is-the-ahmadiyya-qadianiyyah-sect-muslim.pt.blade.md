---
extends: _layouts.answer
section: content
title: A seita Ahmadiyya (Qadianiyyah) é muçulmana?
date: 2025-01-30
description: A Ahmadiyya é um grupo mal orientado, que não faz parte do Islam. As
  suas crenças são completamente contraditórias com o Islam.
sources:
- href: https://islamqa.info/en/answers/170092/
  title: islamqa.info
- href: https://islamqa.info/en/answers/4060/
  title: islamqa.info
- href: https://islamqa.info/en/answers/45525/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/103015/
  title: islamqa.org (AskImam.org)
translator_id: 594172
---

A Ahmadiyya (Qadianiyyah) é um grupo mal orientado, que não faz parte do Islam. As suas crenças são completamente contraditórias com o Islam, pelo que os muçulmanos devem ter cuidado com as suas actividades, uma vez que os "Ulama" (estudiosos) do Islam declararam que eles são Kaafirs (não crentes).

Qadianiyyah é um movimento que começou em 1900 d.C. como uma conspiração dos colonialistas britânicos no subcontinente indiano, com o objetivo de desviar os muçulmanos da sua religião e da obrigação da Jihad em particular, para que não se opusessem ao colonialismo em nome do Islam. O porta-voz deste movimento é a revista Majallat Al-Adyaan (Revista das Religiões), publicada em inglês.

Mirza Ghulam Ahmad al-Qadiani foi o principal instrumento através do qual a Qadianiyyah foi fundada. Nasceu na aldeia de Qadian, no Punjab, na Índia, em 1839 d.C.. Oriundo de uma família conhecida por ter traído a sua religião e o seu país, Ghulam Ahmad cresceu leal e obediente aos colonialistas em todos os sentidos. Assim, foi escolhido para o papel de pretenso profeta, para que os muçulmanos se juntassem à sua volta e ele os distraísse da Jihad contra os colonialistas ingleses. O governo britânico prestava-lhes muitos favores, pelo que eram leais aos britânicos. Ghulam Ahmad era conhecido entre os seus seguidores por ser instável, com muitos problemas de saúde e dependente de drogas.

Ghulam Ahmad iniciou as suas actividades como daa'iyah islâmico (chamador do Islam) para poder reunir seguidores à sua volta, tendo depois afirmado ser um mujaddid (renovador) inspirado por Allah. Depois, deu mais um passo e afirmou ser o Mahdi esperado e o Messias prometido. Depois, afirmou ser um Profeta e que a sua profecia era superior à de Muhammad {{ $page->pbuh() }}.

Os Qadianis acreditam que Allah jejua, reza, dorme, acorda, escreve, comete erros e tem relações sexuais - exaltado seja Allah muito acima de tudo o que eles dizem. Acreditam também que o seu Deus é inglês, porque fala com eles em inglês. Acreditam que o seu livro foi revelado. O seu nome é al-Kitaab al-Mubeen e é diferente do Alcorão Sagrado. Apelam à abolição da Jihad e à obediência cega ao governo britânico porque, segundo afirmam, os britânicos são "aqueles que detêm a autoridade", tal como está escrito no Alcorão. Também autorizam o álcool, o ópio, as drogas e os tóxicos.

Os estudiosos contemporâneos concordaram unanimemente que os Qadianis estão fora dos limites do Islam, porque as suas crenças incluem coisas que constituem descrença e são contrárias aos ensinamentos fundamentais do Islam. Esta seita contrariou o consenso definitivo dos muçulmanos de que não há Profeta depois do nosso Profeta Muhammad {{ $page->pbuh() }}; este facto é indicado por uma série de textos do Alcorão e do saheeh Sunnah.

Atualmente, a maioria dos Qadianis vive na Índia e no Paquistão, com alguns na Palestina ocupada "Israel" e no mundo árabe. Estão a tentar, com a ajuda dos colonialistas, obter posições sensíveis em todos os locais onde vivem. O governo britânico também está a apoiar este movimento e a facilitar aos seus seguidores a obtenção de cargos nos governos mundiais, na administração de empresas e nos consulados. Alguns deles são também oficiais de alta patente nos serviços secretos. Para chamar as pessoas para as suas crenças, os Qadianis usam todos os tipos de métodos, especialmente meios educacionais, porque são altamente educados e há muitos cientistas, engenheiros e médicos nas suas fileiras.

É inadmissível que um muçulmano faça Salaah (oração) com a Jama'ah (congregação) atrás de um Imam (líder da oração) pertencente à seita Ahmedi, uma vez que são classificados como não muçulmanos. Os muçulmanos devem também abster-se de frequentar os seus locais de culto e reuniões, uma vez que são descrentes. Também não é permitido a um muçulmano casar com um deles ou dar a sua filha em casamento a eles, porque são descrentes e apóstatas.
