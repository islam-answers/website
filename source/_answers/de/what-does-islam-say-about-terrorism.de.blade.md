---
extends: _layouts.answer
section: content
title: Was sagt der Islam über Terrorismus?
date: 2024-04-11
description: Der Islam, eine Religion der Barmherzigkeit, erlaubt keinen Terrorismus.
sources:
- href: https://www.islam-guide.com/de/ch3-11.htm
  title: islam-guide.com
---

Der Islam, eine Religion der Barmherzigkeit, erlaubt keinen Terrorismus. Im Quran sagt Gott:

{{ $page->verse('60:8') }}

Der Prophet Muhammad {{ $page->pbuh() }} verbot den Soldaten, Frauen und Kinder zu töten, und er wies sie an:

{{ $page->hadith('tirmidhi:1408') }}

Und er sagte auch:

{{ $page->hadith('bukhari:3166') }}

Der Prophet Muhammad {{ $page->pbuh() }} hat auch die Folterung mit Feuer verboten.

Einst zählte er den Mord als zweite der großen Sünden, und erwähnte sogar, dass am Tag des Gerichts,

{{ $page->hadith('bukhari:6533') }}

Muslime werden sogar aufgefordert freundlich zu den Tieren zu sein, und ihnen wird verboten sie zu verletzen. Der Prophet Muhammad {{ $page->pbuh() }} sagte einst:

{{ $page->hadith('bukhari:3318') }}

Er sprach auch von einem Mann, der einem sehr durstigen Hund etwas zu trinken gab; da vergab ihm Gott seine Sünden. Der Prophet {{ $page->pbuh() }} wurde gefragt: „Gesandter Gottes, werden wir für die Freundlichkeit zu den Tieren belohnt?“ Er antwortete:

{{ $page->hadith('bukhari:2466') }}

Wenn man einem Tier für Nahrung das Leben nehmen muss, wurde den Muslimen darüber hinaus befohlen dies so zu tun, dass das Tier so wenig wie möglich Angst haben soll oder gar leidet. Der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('tirmidhi:1409') }}

In light of these and other Islamic texts, the act of inciting terror in the hearts of defenseless civilians, the wholesale destruction of buildings and properties, the bombing and maiming of innocent men, women, and children are all forbidden and detestable acts according to Islam and the Muslims.

Angesichts dieser und anderer islamischer Berichte sind das Anstacheln zum Terror in den Herzen wehrloser Bürger, die vollständige Zerstörung von Gebäuden und Besitztümern, das Bombardieren und Verstümmeln unschuldiger Männer, Frauen und Kinder als vom Islam und den Muslimen verbotene und verabscheuungswürdige Handlungen anzusehen. Die Muslime verfolgen eine Religion des Friedens, der Gnade und Vergebung, und der Großteil hat mit den Gewaltverbrechen nichts zu tun, die manche mit den Muslimen assoziieren. Wenn ein einzelner Muslim eine terroristische Handlung begeht, macht sich diese Person im Sinne der islamischen Gesetze strafbar.
