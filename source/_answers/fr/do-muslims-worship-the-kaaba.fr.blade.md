---
extends: _layouts.answer
section: content
title: Les musulmans adorent-ils la Kaaba ?
date: 2024-12-23
description: Les musulmans adorent Allah seul, obéissant à Ses commandements tels
  que prier vers la Kaaba, symbole d'unité et du monothéisme.
sources:
- href: https://islamqa.info/en/answers/82349/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/85437/
  title: islamweb.net
- href: https://www.islamweb.net/en/article/141834/
  title: islamweb.net
translator_id: 586055
---

La Kaaba est la Qiblah (direction) vers laquelle tous les musulmans tournent leur visage lorsqu'ils prient. Il est de notre devoir de tourner notre visage vers la Kaaba lorsque nous prions. Allah dit dans le Coran :

{{ $page->verse('2:144..') }}

Chaque année, plus de 2,5 millions de pèlerins musulmans venus du monde entier accomplissent les rites du Hajj (pèlerinage) à La Mecque, où se trouve la Kaaba. Linguistiquement, Kaaba signifie simplement « cube » en arabe, mais c'est bien plus qu'un bâtiment cubique drapé de noir. C'est le symbole de l'unité islamique, au cœur de l'islam, et basé sur le principe du monothéisme.

En référence à la Kaaba, Allah nous dit dans le Coran que :

{{ $page->verse('3:96') }}

À cette époque, les fondations de la Kaaba n'avaient pas encore été érigées par Ibrahim (Abraham), qui construira plus tard la Kaaba avec son fils Ismail (Ismaël), comme l'avait ordonné Allah :

{{ $page->verse('22:26') }}

Nous voyons ici que le but de cette maison était l'adoration exclusive d'Allah Seul, sans associé. Ibrahim avait passé toute sa vie à rejeter le polythéisme, allant à l'encontre des croyances et des coutumes de toute sa communauté et de ses dirigeants, et même à l'encontre de celles de son père, qui avait l'habitude de fabriquer des idoles de ses propres mains.

En tant que musulmans, notre principe fondamental est de n'adorer qu'Allah. Des actions telles que prier en direction de la Kaaba et faire le Tawaf (faire le tour de la Kaaba) sont l'accomplissement des commandements d'Allah, et non l'adoration de l'objet lui-même. Il existe des différences majeures entre le fait de se tourner vers la Kaaba et l'adoration d'idoles.
