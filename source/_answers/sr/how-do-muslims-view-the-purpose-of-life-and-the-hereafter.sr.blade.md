---
extends: _layouts.answer
section: content
title: Kako muslimani gledaju na svrhu života i ahiret?
old_titles:
- Kako muslimani gledaju na svrhu života i na onaj svijet (ahiret)?
date: 2024-11-26
description: Islam uči da je svrha života obožavanje Boga i da će ljudima suditi Bog
  na onom svijetu.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=NMM01&articleID=NMM010003&articlePages=4
  title: spubs.com
translator_id: 584497
---

U Časnom Kur'anu Bog uči ljude da su stvoreni da Ga obožavaju i da je osnova svakog pravog ibadeta svest o Bogu. Budući da učenja islama obuhvaćaju sve aspekte života i etike, svest o Bogu se podstiče u svim ljudskim poslovima.

Islam jasno kaže da su sva ljudska dela ibadet ako su učinjena samo za Boga iu skladu s Njegovim božanskim zakonom. Kao takvo, obožavanje u islamu nije ograničeno na verske obrede. Učenja islama deluju kao milost i lek za ljudsku dušu, a osobine kao što su poniznost, iskrenost, strpljivost i milosrđe snažno se potiču. Osim toga, islam osuđuje ponos i samopravednost, budući da je Svemogući Bog jedini sudac ljudske pravednosti.

Islamski pogled na prirodu čoveka je također realističan i uravnotežen. Ne veruje se da su ljudska bića inherentno grešna, ali se smatraju podjednako sposobnima i za dobro i za zlo. Islam također uči da vera i delovanje idu ruku pod ruku. Bog je ljudima dao slobodnu volju, a merilo vere su njegova dela i postupci. Međutim, ljudska bića su također stvorena slaba i redovito padaju u greh.

To je priroda ljudskog bića kakvog je stvorio Bog u svojoj mudrosti, i ono nije samo po sebi "pokvareno" niti treba popravke. To je zato što je put pokajanja uvek otvoren za sva ljudska bića, a Svemogući Bog više voli grešnika koji se kaje nego onoga koji uopće ne griješi.

Prava ravnoteža islamskog života uspostavlja se zdravim strahom od Boga kao i iskrenim vjerovanjem u Njegovu beskrajnu milost. Život bez straha od Boga vodi u grijeh i neposluh, a uvjerenje da smo toliko zgriješili da nam Bog to neće oprostiti vodi samo u očaj. U svjetlu ovoga, islam uči da: samo oni u zabludi su uskračeni za milost svoga Gospodara.

{{ $page->verse('39:53') }}

Uz to, Časni Kur'an, koji je objavljen Poslaniku Muhammedu {{ $page->pbuh() }}, sadrži mnogo učenja o budućem životu i Sudnjem danu. Zbog toga muslimani veruju da će svim ljudskim bićima na kraju suditi Bog za njihova verovanja i postupke u njihovim zemaljskim životima.

U suđenju ljudskim bićima, Svemogući Bog će biti i Milostiv i Pravedan, a ljudima će se suditi samo za ono za što su bili sposobni. Dovoljno je reći da islam uči da je život ispit i da će sva ljudska bića biti odgovorna pred Bogom. Iskreno verovanje u budući život ključno je za vođenje dobro uravnoteženog života i morala. U suprotnom, život se promatra kao cilj sam po sebi, što uzrokuje da ljudska bića postanu sebičnija, materijalistička i nemoralnija.
