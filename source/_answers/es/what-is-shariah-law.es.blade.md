---
extends: _layouts.answer
section: content
title: ¿Qué es la Sharia?
date: 2024-12-14
description: La Sharia se refiere a toda la religión del Islam, que Allah eligió para
  Sus siervos, para conducirlos desde las profundidades de la oscuridad hacia la luz.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 573109
---

La palabra Shari'ah se refiere a toda la religión del Islam, que Allah eligió para Sus siervos, para conducirlos de las profundidades de la oscuridad a la luz. Es lo que Él les ha prescrito y explicado de mandamientos y prohibiciones, halal y haram.

Quien sigue la Shari'ah de Allah, considerando permisible lo que Él ha permitido y considerando prohibido lo que Él ha prohibido, alcanzará el triunfo.

Quien va en contra de la Shari'ah de Allah se expone a la ira, la cólera y el castigo de Allah.

Allah, exaltado sea, dice:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (que Allah tenga piedad de él) dijo:

> La palabra shari'ah (pl. sharai') se refiere a lo que Allah ha prescrito (shara'a) a la gente en materia de religión, y a lo que les ha ordenado que cumplan en cuanto a la oración, el ayuno, el Hayy, etcétera. Es el shir'ah (el lugar del río donde se puede beber).

Ibn Hazm (que Allah tenga piedad de él) dijo:

> La shari'ah es lo que Allah, exaltado sea, prescribió (shara'a) en boca de Su Profeta {{ $page->pbuh() }} con respecto a la religión, y en boca de los Profetas (la paz sea con ellos) que le precedieron. La norma del texto abrogatorio debe considerarse la norma definitiva.

### El origen lingüístico del término Shari'ah

El origen lingüístico del término Shari'ah se refiere al lugar en el que un jinete puede ir a beber agua, y al lugar en el río donde uno puede beber. Allah, exaltado sea, dice:

{{ $page->verse('42:13') }}
