---
extends: _layouts.answer
section: content
title: Quali festività celebrano i musulmani?
date: 2024-06-18
description: 'I musulmani celebrano solo due Eid (feste): Eid al-Fitr (alla fine del
  mese di Ramadan) e Eid al-Adha alla fine del Hajj (pellegrinaggio annuale).'
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 573107
---

I musulmani celebrano solo due Eid (feste): Eid al-Fitr (alla fine del mese di Ramadan) e Eid al-Adha, che segna il completamento del Hajj (periodo di pellegrinaggio annuale), il 10° giorno del mese di Dhul-Hijjah.

Durante queste due festività, i musulmani si scambiano gli auguri, esprimono e infondono gioia nelle loro comunità e festeggiano con le loro famiglie allargate. Ma, ancora più importante, ricordano le benedizioni che hanno ricevuto da Allah, celebrano il Suo nome e offrono la preghiera del Eid in moschea. A parte queste due occasioni, i musulmani non riconoscono né celebrano altri giorni festivi nell'anno.

Ovviamente, ci sono altre occasioni gioiose per le quali l'Islam stabilisce che vengano festeggiate in modo adeguato, come la celebrazione del matrimonio (walima) o in occasione della nascita di un bambino (aqeeqah). Tuttavia, questi giorni non sono indicati come festività particolari dell'anno, ma vengono celebrati man mano che si verificano nel corso della vita di un musulmano.

Sia la mattina di Eid al-Fitr che di Eid al-Adha, i musulmani partecipano alle preghiere della festa in congregazione nella moschea, seguite da un sermone (khutbah) che ricorda ai musulmani i loro doveri e le loro responsabilità. Dopo la preghiera, i musulmani si salutano con l'augurio "Eid Mubarak" (Festa Benedetta) e si scambiano doni e dolci.

## Eid al-Fitr

Eid al-Fitr segna la fine del Ramadan, il periodo di digiuno che si effettua durante il nono mese del calendario islamico lunare.

L'Eid al-Fitr è importante perché fa seguito ad uno dei mesi più sacri in assoluto: il Ramadan. Il Ramadan è un periodo in cui i musulmani rafforzano il loro legame con Allah, recitano il Corano e aumentano le loro buone azioni. Alla fine del Ramadan, Allah dona ai musulmani il giorno del Eid al-Fitr come ricompensa per aver completato con successo il mese di digiuno e per aver incrementato gli atti di culto durante il Ramadan. Nella giornata del Eid al-Fitr, i musulmani ringraziano Allah per l'opportunità di aver potuto essere presenti ad un altro Ramadan, per essersi avvicinati a Lui, per essere diventate persone migliori e per aver ottenuto un'altra possibilità di essere salvati dal fuoco dell'inferno.

{{ $page->hadith('ibnmajah:3925') }}

##Eid al-Adha

Eid al-Adha segna il completamento del periodo annuale del Hajj (pellegrinaggio alla Mecca), che si svolge nel mese di Dhul-Hijjah, il dodicesimo e ultimo mese del calendario islamico lunare.

La celebrazione di Eid al-Adha ha lo scopo di commemorare la devozione del profeta Ibrahim (Abramo) ad Allah e la sua disponibilità a sacrificare il proprio figlio Ismail (Ismaele). Al momento del sacrificio, Allah sostituì Ismail con un montone che sarebbe stato macellato al posto di suo figlio. Questo comando di Allah fu una prova della volontà e dell'impegno del profeta Ibrahim ad obbedire al comando del suo Signore, senza fare domande. Pertanto, Eid al-Adha significa la festa del sacrificio.

Una delle migliori azioni da compiere per i musulmani durante l'Eid al-Adha è l'atto del Qurbani/Uhdiya (sacrificio), che viene eseguito dopo la preghiera del Eid. I musulmani macellano un animale per ricordare il sacrificio del profeta Abramo per Allah. L'animale sacrificato deve essere una pecora, un agnello, una capra, una mucca, un toro o un cammello. L'animale deve essere in buona salute e avere più di una certa età per poter essere macellato in modo halal, cioè islamico. La carne dell'animale sacrificato viene condivisa tra la persona che effettua il sacrificio, i suoi amici, la sua famiglia e i poveri e bisognosi.

Il sacrificio di un animale durante l'Eid al-Adha rappresenta la disponibilità del profeta Abramo a sacrificare il proprio figlio ed è anche un atto di adorazione che si trova nel Corano e una tradizione confermata del profeta Mohammed {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

Nel Corano, Dio ha detto:

{{ $page->verse('2:196..') }}
