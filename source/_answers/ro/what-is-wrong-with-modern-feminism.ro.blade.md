---
extends: _layouts.answer
section: content
title: Ce este în neregulă cu feminismul modern?
date: 2025-02-08
description: Feminismul favorizează inegalitatea prin negarea diferențelor naturale
  de gen. Islamul onorează femeile, asigurând echilibrul și protecția drepturilor
  lor.
sources:
- href: https://islamqa.info/en/answers/40405/
  title: islamqa.info
- href: https://islamqa.info/en/answers/258254/
  title: islamqa.info
- href: https://islamqa.info/en/answers/43252/
  title: islamqa.info
- href: https://islamqa.info/en/answers/930/
  title: islamqa.info
- href: https://islamqa.info/en/answers/175863
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/127550/
  title: islamqa.org (AskImam.org)
- href: https://muslimskeptic.com/2021/03/02/feminism-harms-women/
  title: muslimskeptic.com
translator_id: 554943
---

### Ce este feminismul și de ce este controversat?

Feminismul este un termen extrem de ambiguu. Wikipedia îl definește ca fiind "o serie de mișcări sociale, mișcări politice și ideologii care împărtășesc un scop comun, și anume definirea, stabilirea și realizarea egalității politice, economice, personale și sociale a sexelor". Feminismul pare să facă apel la eliberare, dar, de fapt, face apel la privilegii de gen mascate drept egalitate. În esență, feminismul neagă realitatea umană și nu rezultă în dreptate între sexe.

Feministele susțin că femeile au fost în mod tradițional dezumanizate de o societate dominată de bărbați, pe care o numesc patriarhat, și că întotdeauna a fost mai bine să fii bărbat. Dar această afirmație unilaterală respinge privilegiile de care s-au bucurat adesea femeile pentru simplul fapt că sunt femei. Acest privilegiu nu este apreciat de feminiști, deoarece privilegiul este invizibil pentru cei care îl au.

Practic, toate speciile, de la albine la primate, au roluri de gen diferite, cu abilități biologice diferite între sexe. Cu toate acestea, feminiștii insistă că orice diferență de gen între oameni este inventată și că nu există nimic din punct de vedere biologic la bărbați sau femei care ar trebui să influențeze rolurile lor sociale. Cu toate acestea, studiile științifice au demonstrat în mod clar rolul testosteronului în dezvoltarea musculaturii, în creșterea competitivității, a încrederii și a asumării de riscuri - ceea ce îi face pe bărbați mai potriviți pentru rolurile mai periculoase și competitive din societate. Datorită testosteronului, bărbații tind în mod natural să fie mai rapizi, mai mari, mai rezistenți și mai puternici fizic. Prin urmare, a învăța o fată că poate concura în mod natural în mod egal cu bărbații în toate domeniile este înșelător.

### Cum feminismul modern le dezamăgește pe femei

O privire obiectivă asupra statisticilor dezvăluie o complexitate mai profundă a subiectului:

1. Conform Biroului de Recensământ al SUA, 10,4 milioane de familii din America sunt conduse de mame singure, care sunt singurele întreținătoare ale familiilor lor.
1. Peste un milion de copii sunt uciși prin avort în fiecare an în America, conform Centrului pentru Controlul Bolilor.
1. 683,000 de femei sunt supuse anual violului în America, la o rată de șaptezeci și opt de femei pe oră, după cum afirmă Departamentul Justiției al SUA.
1. 1,320 de femei sunt ucise anual, ceea ce înseamnă că aproximativ patru femei sunt ucise în fiecare zi, de soții sau iubiții lor în America.
1. Aproximativ trei milioane de femei din America sunt supuse în fiecare an abuzurilor fizice din partea soțului sau iubitului, potrivit site-ului oficial al guvernului statului New Jersey.
1. 22,1% dintre femeile din America sunt supuse abuzurilor fizice din partea unui soț sau prieten (actual sau fost), potrivit Departamentului Justiției al SUA.
1. Un raport emis de Biroul de Statistică a Muncii din SUA confirmă faptul că majoritatea femeilor din Occident lucrează job-uri cu salarii mici și statut scăzut și, în ciuda presiunilor pe care guvernul le exercită pentru a îmbunătăți locurile de muncă ale femeilor, 97% din pozițiile de conducere din majoritatea companiilor sunt ocupate de bărbați.
1. 78% dintre femeile din forțele armate sunt supuse hărțuirii sexuale din partea angajaților militari.
1. Aproximativ 50,000 de femei și fete sunt traficate în America în fiecare an, unde sunt înrobite și forțate să se prostitueze, potrivit New York Times.
1. Exploatarea corpului femeilor în diverse moduri permisive este o industrie care aduce anual 12 miliarde de dolari numai în America, potrivit unui raport Reuters.
1. Conform unui studiu realizat în paisprezece țări, s-a arătat că 42% dintre britanici recunosc că au relații ilicite cu mai multe persoane în același timp, în timp ce jumătate dintre americani au relații ilicite. Rata în Italia este de 38%, iar în Franța este de 36%, potrivit BBC.

Există o listă aproape nesfârșită de pagini web întunecate care vorbesc despre situația femeilor în civilizația occidentală materialistă, toate fiind documentate pe site-uri oficiale occidentale care constituie surse demne de încredere pentru astfel de statistici.

### Planul feminist întortocheat al guvernelor seculare

Sistemul care există în țările seculare occidentale este un sistem liberal. Acesta promovează liberalismul, care constă în maximizarea libertății personale și a egalității. Cu alte cuvinte, venerarea sinelui; a fi complet absorbit de propria persoană în detrimentul tuturor celor din jur. Într-o societate seculară, liberală, totul se învârte în jurul individului și numai al individului. Sistemul este conceput pentru a-i forța pe toți oamenii să urmeze o cale specifică, prestabilită: sclavia salarială - familia cu două venituri, în care atât soțul, cât și soția trebuie să muncească pentru a se descurca financiar. În trecut, sistemul era structurat în așa fel încât o familie putea supraviețui și trăi bine doar cu veniturile tatălui, în timp ce mama stătea acasă cu copiii. Sistemul a evoluat în direcția unei familii cu două venituri, forțând atât tatăl, cât și mama să iasă din casă pentru a munci.

Dar cum rămâne cu copiii? Instituții de școlarizare în masă. Dar cum rămâne cu copiii care sunt prea mici pentru a merge la școală? Instituții de îngrijire în masă a copiilor (precum creșe sau grădinițe). Dar cum rămâne cu cei care sunt prea bătrâni pentru a continua să lucreze? Instituții de îngrijire a bătrânilor (i.e. aziluri). Cu toții sunt instituționalizați. Bărbații sunt separați de femeile lor, iar părinții sunt separați de copiii lor. Legăturile naturale dintre oameni sunt rupte. Relațiile normale care ar trebui să existe între ființele umane sunt distruse.

Un exemplu de rupere a acestei legături este separarea unei mame de bebelușul ei. În natură, în lumea animală și în toate societățile umane tradiționale de oricând și oriunde, mama și copilul au rămas întotdeauna împreună. A fi departe de mamă nu este ceva natural, mai ales la o vârstă fragedă. Această separare dintre mamă și copil este incredibil de dură și este extrem de solicitantă atât pentru mamă, cât și pentru copil. Aceasta intră în conflict cu natura atât a mamei, cât și a copilului și, prin urmare, perturbă cele mai profunde nevoi ale amândurora.

Adesea, acest lucru nu este nici vina mamei, nici a vreunei persoane anume. Biata mamă este adesea (deși nu întotdeauna) forțată, din necesitate, să își lase copilul la o creșă cu oameni străini în timp ce ea pleacă la muncă. Adesea nu are de ales. Mamele singure sunt deseori întâlnite, mai ales într-o societate în care Zina (fornicația) este răspândită, normalizată și încurajată activ de societate și chiar de părinți. Individualismul este promovat ca ceva extrem de important.

Care este rezultatul tuturor acestor "drepturi", hiper-concentrate doar pe individ și pe propriile plăceri de moment? Drepturile tuturor celor din jur sunt ignorate și încălcate. Copiii suferă și sunt abandonați. Familiile nucleare suferă și se destramă. Familia extinsă suferă și se dizolvă. Legăturile de rudenie se rup. Comunitatea este dezbinată. Societatea slăbește. Civilizațiile se prăbușesc.

Guvernele promovează feminismul prin mass-media și prin agențiile lor, cum ar fi CIA (Agenția Centrală de Informații). Acestea folosesc persoane precum Gloria Steinem (jurnalistă și activistă socială americană) pentru a-și promova ideologiile, jucând jocul pe termen lung. Mai precis, deoarece mamele sunt prea ocupate cu munca, copiii lor vor merge la creșă sau grădiniță cât mai devreme posibil (chiar înainte de vârsta de 6 luni). Acești copii sunt instituționalizați devreme, astfel încât să poată fi îndoctrinați cu ideologiile pe care guvernul dorește să le impună, ceea ce îi face ușor de controlat în societate mai târziu în viață.

### Islamul: o soluție naturală pentru justiția dintre sexe

Allah a onorat foarte mult femeile. El le onorează în calitate de fiice, mame și soții, le conferă drepturi și virtuți și le impune un tratament bun în moduri care nu sunt împărtășite de bărbați în multe cazuri. Islamul nu neagă umanitatea femeilor. Dimpotrivă, le acordă drepturi și o înaltă considerație. De exemplu, înainte de Islam, femeilor nu li se permitea să moștenească, ca să nu mai vorbim de faptul că fetele erau îngropate de vii la naștere și multe alte lucruri. Apoi a venit Islamul și a interzis îngroparea de vii a fetițelor nou-născute; a considerat acest lucru drept crimă (omucidere), care este un păcat major. Islamul a oferit femeilor partea lor de moștenire, le-a permis să cumpere, să vândă și să dețină proprietăți și le-a încurajat să învețe și să cheme oamenii către Allah. A poruncit ca femeile să fie onorate ca soții și ca mame și a făcut ca drepturile mamei să fie de trei ori mai mari decât cele ale tatălui. Și mai există multe alte moduri în care Islamul a onorat femeile.

Islamul are un sistem complet diferit - un sistem elegant, rafinat, cu un echilibru delicat între drepturile tuturor oamenilor: individul, familia, copiii, părinții, comunitatea și societatea. Cele mai importante, cele mai fundamentale drepturi ale fiecărei părți sunt asigurate, astfel încât nimeni să nu sufere; și astfel încât natura umană să fie susținută și fitrah (dispoziția naturală înnăscută) să fie menținută. Demnitatea copiilor, a mamelor, a taților și a persoanelor în vârstă sunt asigurate. Nevoile lor sunt respectate.

Allah ne spune în Coran:

{{ $page->verse('4:1') }}

Islamul este sistemul care satisface toate nevoile umane, îndeplinește toate drepturile naturale ale oamenilor și organizează societatea într-un mod care se aliniază frumos naturii noastre umane inerente și nu o distruge. Islamul guvernează modul în care interacționăm cu copiii, părinții, frații, vecinii, prietenii și chiar cu străinii. Islamul ne adâncește dragostea intuitivă pentru cei dragi și ne hrănește relațiile umane organice. Cum anume?

Cu mecanisme integrate în religia noastră, cum ar fi rolurile de gen (în care soțul are obligații financiare, iar soția este liberă să fie mamă și sǎ fie acasă cu copiii ei); cu "birr al-walidayn" (excelența față de părinți), care asigură respectul față de părinți din partea copiilor lor; cu "silat al-rahim" (menținerea legăturilor de rudenie), care asigură plasa de siguranță și rețeaua de sprijin a familiei extinse și a rudelor și menține, de asemenea, descendența. Într-un sistem Islamic, ar fi extrem de rar ca o mamă să fie separată de copilul ei la șase săptămâni după ce a născut. Atât mama, cât și copilul sunt scutiți de agonia de a fi la o distanță nefirească unul de celălalt. Cu toate acestea, în sistemul secular occidental, această separare la vârste fragede dintre mamă și copil este ceva normal.

Islamul recunoaște și ia în considerare diferențele subtile dintre bărbați și femei, deoarece Cel care a trimis Islamul este, de asemenea, Cel care a creat bărbații și femeile cu vasta lor gamă de asemănări și diferențe. Având în vedere aceste diferențe între sexe, în realitate, egalitatea în oglindă ar fi opresivă, deoarece ar forța un sex să se supună standardelor care se aplică celuilalt sex.

Dacă feminiștii recunosc că sexele sunt diferite și, prin urmare, femeile ar trebui să fie tratate "echitabil", spre deosebire de "egal", și că există loc pentru variații în legislație pentru a lua în considerare aceste diferențe, atunci unde tragem linia? Fiecare caz de tratament diferit ar putea fi atribuit naturii/creării diferite a sexelor. Fiecare diferență de rol între sexe ar putea fi atribuită și explicată prin diferența de natură/creație dintre sexe.

De ce ar trebui ca femeile să fie socializate pentru a-și asuma rolul de îngrijitoare maternă, spre deosebire de bărbați, care ar trebui socializați pentru rolul de întreținători ai familiei? Diferență de natură. De ce ar trebui femeile să se îmbrace într-un anumit fel, spre deosebire de bărbați? Diferență de natură. De ce ar trebui să li se interzică femeilor să devină imami (lideri religioși), în timp ce bărbații îndeplinesc acest rol? Diferență de natură. De ce ar trebui ca rolul de khalifa (calif) și conducerea statului să fie rezervate bărbaților și nu femeilor? Diferență de natură.

Toate diferențele de gen pot fi raționalizate în acest fel - lucru care este un coșmar pentru filozofia feministă. Deci, pentru a evita acest lucru, feminiștii trebuie să insiste asupra egalității în oglindă, dar acest lucru provoacă, de asemenea, conflicte și incoerență, după cum am văzut. Aceasta este una dintre multele probleme conceptuale care afectează gândirea feministă, însă Islamul și sharia sunt lipsite de aceste probleme. Allah ne-a dat fiecăruia dintre noi o poziție și un loc diferit în societate și în familiile noastre, iar El ne va judeca în funcție de taqwa (conștiința și frica lui Dumnezeu), nu în funcție de bogăția noastră, de statutul nostru, de poziția noastră sau de oricare dintre celelalte criterii pe care feminismul le promovează.

### Înțelegerea greșită a islamului și a drepturilor femeilor

Neînțelegerile legate de Islam par să provină în primul rând dintr-o lipsă de cunoaștere (ignoranță) a statutului femeii în învățăturile Islamice, a stimei înalte în care este ținută femeia în Islam, a recunoașterii drepturilor sale, a modului în care Islamul îi protejează demnitatea și le cere bărbaților să trateze femeile cu bunătate. Poate că oamenii se uită la unele societăți musulmane care contravin învățăturilor Islamului și maltratează și oprimă femeile. Islamul nu are nimic de-a face cu acest lucru; mai degrabă, acestea sunt greșeli, iar responsabilitatea pentru ele revine celui care face acest lucru și el este cel care va fi tras la răspundere pentru aceasta. Astfel de lucruri nu ar trebui să fie atribuite Islamului.

În al doilea rând, neînțelegerile apar, de asemenea, din faptul că nu analizăm situația reală din societate în lumina cifrelor oficiale și a statisticilor precise care descriu prin ce trec femeile în acele societăți seculare și din faptul că suntem înșelați de unele aparente "libertăți" pentru femei. Ar trebui să ne îndreptăm atenția asupra recunoașterii internaționale a incidentelor de hărțuire sexuală, viol și exploatare sexuală îngrozitoare, ale căror știri au ajuns peste tot și oamenii de pretutindeni vorbesc despre ele, în măsura în care există rapoarte potrivit cărora jumătate dintre femeile britanice s-au confruntat cu hărțuire sexuală de diferite tipuri, conform BBC. În mod similar, în Franța, 100% dintre femeile care folosesc transportul în comun au fost hărțuite sau agresate sexual la un moment dat, conform Înaltului Consiliu pentru egalitatea între femei și bărbați (HCEfh).

Odată cu apariția Islamului și a învățăturilor sale speciale, viața femeilor a intrat într-o nouă fază - o fază care diferă foarte mult de cea anterioară, în care femeile beneficiază de tot felul de drepturi individuale, sociale și umane. Baza învățăturilor Islamice cu privire la femei este exact ceea ce citim în Nobilul Coran. Allah spune în Coran:

{{ $page->verse('16:97') }}

Islamul consideră că femeia, ca și bărbatul, este complet liberă și independentă. Această libertate este pentru toți oamenii, bărbați și femei. Allah menționează în Coran:

{{ $page->verse('41:46') }}

Căsătoria este piatra de temelie a vieții de familie și unitatea fundamentală a societății islamice, conferindu-ne drepturi și îndatoriri unii față de alții. Allah spune în Coran:

{{ $page->verse('30:21') }}

În cadrul căsătoriei, bărbaților li se încredințează sarcina de a fi responsabili pentru femei, de a avea grijă de ele din toate punctele de vedere și de a le îndruma în cel mai bun mod posibil, inclusiv prin sfaturi, porunci și interdicții conforme religiei. Allah spune în Coran:

{{ $page->verse('4:34..') }}

Ca mamă, femeia se bucură de un nivel și mai înalt al purtării excelente a copiilor ei față de tatăl lor (datorită greutăților pe care aceasta le are în timpul sarcinii, nașterii, alăptării și așa mai departe). Acesta este rolul pe care îl joacă ca inimă și suflet al familiei și, prin extensie, al societății. S-a relatat că Abu Hurayrah (Allah să fie mulțumit de el) a spus:

{{ $page->hadith('muslim:2548') }}

Pe scurt, Islamul consideră femeia un element fundamental al societății și, prin urmare, ea nu ar trebui tratată ca o entitate lipsită de voință.

Sistemul islamic nu se bazează pe individualismul egoist. În Islam, femeile nu servesc bărbații, nici bărbații nu servesc femeile. Mai degrabă, îl slujim pe Allah ajutându-ne reciproc și dăruind unul altuia în funcție de nevoile umane, înțelegând că oamenii nu sunt toți la fel. Islamul oferă o soluție clară, naturală și justă pentru a asigura dreptatea pentru toți oamenii și nu are nevoie de încercările zadarnice ale feminismului de a reinventa roata pe care Islamul a pus-o în mișcare acum peste 1400 de ani.
