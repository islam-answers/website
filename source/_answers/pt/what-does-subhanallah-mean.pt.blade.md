---
extends: _layouts.answer
section: content
title: O que significa Subhanallah?
date: 2024-10-28
description: Geralmente traduzida como "Glorificado seja Allah", aparece no Alcorão
  para negar as deficiências e falhas atribuídas a Allah e afirmar a Sua perfeição.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 594172
---

O termo árabe "tasbih" refere-se geralmente à frase "SubhanAllah". É uma frase que Allah aprovou para Si próprio, inspirou os Seus anjos a dizê-la e guiou os mais excelentes da Sua criação a declará-la. "SubhanAllah" é comummente traduzido como "Glorificado seja Allah", mas esta tradução não consegue transmitir o significado completo do tasbih.

O tasbih é composto por duas palavras: Subhana e Allah. Linguisticamente, a palavra "subhana" vem da palavra "sabh", que significa distância, afastamento. Por conseguinte, Ibn 'Abbas explicou a frase como sendo a pureza e a absolvição de Allah acima de qualquer mal ou assunto impróprio. Por outras palavras, Allah está para sempre longe e eternamente acima de todas as transgressões, deficiências, falhas e inadequações.

Esta frase aparece no Alcorão para negar descrições inapropriadas e incompatíveis atribuídas a Allah:

{{ $page->verse('23:91') }}

Um único tasbih é suficiente para negar todas as falhas ou mentiras atribuídas a Allah em todos os lugares e em todos os momentos por qualquer ou toda a criação. Assim, Allah refuta inúmeras mentiras que lhe são atribuídas com um único tasbih:

{{ $page->verse('37:149-159') }}

O objetivo da negação é que o seu oposto seja afirmado. Por exemplo, quando o Alcorão nega a ignorância de Allah, então o muçulmano nega a ignorância e, por sua vez, afirma o oposto de Allah, que é o conhecimento totalizante. O tasbih é fundamentalmente uma negação (de deficiências, actos ilícitos e afirmações falsas) e, por conseguinte, de acordo com este princípio, implica a afirmação do seu oposto, que é a existência bela, a conduta perfeita e a verdade suprema.

Sa’d Ibn Abi Waqqas (que Allah esteja satisfeito com ele) narrou:

{{ $page->hadith('muslim:2698') }}
