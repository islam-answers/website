---
extends: _layouts.answer
section: content
title: Ce înseamnă Assalamu Alaykum?
date: 2024-07-29
description: Assalamu alaykum înseamnă "pacea fie asupra ta" și este un salut folosit
  printre musulmani.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
translator_id: 554943
---

Când Islamul a apărut, Allah a prescris ca modul de salut printre musulmani să fie "Al-salamu 'alaykum" și ca acest salut să fie folosit numai în rândul musulmanilor.

Sensul cuvântului "salam" (literalmente însemnând "pace") este inofensivitatea, siguranța și protecția împotriva răului și a greșelilor. Numele al-Salam este un nume al lui Allah, fie ca El să fie înălțat, astfel încât semnificația salutului de salam care este necesar printre musulmani este: "Fie ca binecuvântarea Numelui Său să coboare asupra ta." Utilizarea prepoziției "'ala" in "'alaykum" (asupra ta) indică faptul că salutul este inclusiv.

Cea mai completă formă de salut este de a spune "As-Salamu `alaykum wa rahmatu-Allahi wa barakatuhu", ceea ce înseamnă "Pacea fie asupra ta și mila lui Allah și binecuvântările Sale)".

Profetul {{ $page->pbuh() }} a făcut din răspândirea salamului o parte a credinței.

{{ $page->hadith('bukhari:12') }}

Prin urmare, Profetul {{ $page->pbuh() }} a explicat că oferirea salamului răspândește dragoste și fraternitate. Trimisul lui Allah {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:54') }}

Profetul {{ $page->pbuh() }} ne-a poruncit să întoarcem saluturile (salamurile) și a făcut din aceasta un drept și o datorie. El a spus:

{{ $page->hadith('muslim:2162a') }}

Este clar că este obligatoriu să întorci salutul, deoarece, făcând acest lucru, un musulman îți oferă siguranță, iar tu trebuie să îi oferi siguranță în schimb. Este ca și cum el ți-ar spune: "Eu îți ofer siguranță și securitate", așa că tu trebuie să îi oferi același lucru, astfel încât să nu devină suspicios sau să creadă că cel căruia i-a oferit salamul îl trădează sau îl ignoră în vreun fel.
