---
extends: _layouts.answer
section: content
title: What is Karma?
date: 2025-01-18
description: Karma - a belief in consequences from actions - is a false concept from Indian religions. Islam teaches that Allah alone governs reward and punishment.
sources:
- href: https://www.islamweb.net/en/fatwa/343198/
  title: islamweb.net
- href: https://islamqa.info/ar/answers/183131/
  title: islamqa.info
---

"Karma" is a common term in Indian religions (Hinduism, Jainism, Sikhism, and Buddhism). The word "karma" refers to the actions of a living being and the moral consequences resulting from them. Any act of good or evil, whether it is a word, an action, or just a thought, must have consequences (reward or punishment), as long as it is the result of prior awareness and perception.

For those religions, the Karma system works according to a natural moral law, and is not under the authority of divine judgments. According to them, Karma determines things such as external appearance, beauty, intelligence, age, wealth, and social status. They also claim that the law of Karma governs everything that is created, and it is an unchangeable law. They claim that this law governs and monitors every moment, and therefore each of our good and bad actions has its consequences.

It was stated in "The Facilitated Encyclopedia Of Contemporary Religions Doctrines And Parties":

> Karma - according to Hindus - is the law of retribution, meaning that the system of the universe is divine and based on pure justice, this justice that will inevitably occur either in the present life or in the next life, and the retribution of one life will be in another life (...)

It also stated:

> And man continues to be born and die as long as karma is attached to his soul, and his soul is not purified until it is rid of karma, where his desires end, and then he remains alive and immortal in the bliss of salvation, which is the stage of "Nirvana" or salvation that may be obtained in this world through training and exercise or through death.

### Is Karma a true concept?

There is no doubt that these Indian religions are pagan religions, formed according to false beliefs and impossible, imaginary perceptions. The belief in "karma" is among those false beliefs that these people believe and adhere to.

We can summarize the reasons for saying that this corrupt belief is false as follows:

1. It is a fabricated belief, not based on an infallible divine revelation, but rather its source is an invented pagan religion.
1. It is a system that operates according to a natural moral law that is self-sufficient, independent of divine law and heavenly religious beliefs.
1. They claim that it is a dominant law that controls every creature, monitors actions, manages destinies, and rewards for actions. This is clear blasphemy; for God is the dominant one and the one who conducts all affairs and the one who holds people accountable for their actions.
1. This belief is part of their system of false beliefs that they want to reach the stage of eternal salvation, as they claim, which is the highest goal for them, since karma is the consequences of the actions that people do, there is no salvation as long as karma exists.

And we, praise be to God, are independent of these false beliefs and invented sects through the religion of God and the grace of God. It is sufficient for us to say:

{{ $page->verse('99:7-8') }}

It is sufficient for us to know that Allah is the Guardian over all things, and that Allah has encompassed all things in His knowledge. Allah says in the Quran:

{{ $page->verse('53:31') }}

Whoever knows this and believes in it and is certain that Allah will resurrect those in the graves to hold them accountable for the weight of an atom of deeds, and He has established witnesses and scribes who will record those deeds, will not need this invented falsehood and corrupt, misguided belief in order to stop sinning and refrain from bad words, deeds and morals.
