---
extends: _layouts.answer
section: content
title: What holidays do Muslims celebrate?
date: 2024-06-15
description: Muslims celebrate only two Eids (festivals), Eid al-Fitr (at the end of the month of Ramadan), and Eid al-Adha, at the end of the Hajj (annual pilgrimage).
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
---

Muslims celebrate only two Eids (festivals): Eid al-Fitr (at the end of the month of Ramadan), and Eid al-Adha, which marks the completion of the Hajj (annual pilgrimage period) on the 10th day of the month of Dhul-Hijjah.

During these two festivals, Muslims congratulate one another, spread joy in their communities, and celebrate with their extended families. But more importantly, they remember Allah's blessings on them, celebrate His name and offer the Eid prayer in the mosque. Other than these two occasions, Muslims do not recognize or celebrate any other days in the year.

Of course, there are other joyous occasions for which Islam dictates appropriate celebration, such as wedding celebrations (walima) or on the occasion of the birth of a child (aqeeqah). However, these days are not specified as particular days in the year; rather, they are celebrated as they happen in the course of a Muslim's life.

On the morning of both Eid al-Fitr and Eid al-Adha, Muslims attend Eid prayers in congregation at the mosque, followed by a sermon (khutbah) that reminds Muslims of their duties and responsibilities. After the prayer, Muslims greet each other with the phrase "Eid Mubarak" (Blessed Eid) and exchange gifts and sweets.

## Eid al-Fitr

Eid al-Fitr marks the end of Ramadan, which takes place during the ninth month of the lunar-based Islamic calendar.

Eid al-Fitr is important because it follows one of the most sacred months of all: Ramadan. Ramadan is a time for Muslims to strengthen their bond with Allah, recite the Qur’an and increase in good deeds. At the end of Ramadan, Allah gives Muslims the day of Eid al-Fitr as a reward for successfully completing the fast and for increasing in acts of worship during the month of Ramadan. On Eid al-Fitr, Muslims thank Allah for the opportunity to witness another Ramadan, to come closer to Him, to become better people and to have another chance to be saved from Hellfire.

{{ $page->hadith('ibnmajah:3925') }}

## Eid al-Adha

Eid al-Adha marks the completion of the annual Hajj period (pilgrimage to Mecca), which takes place in the month of Dhul-Hijjah, the twelfth and final month of the lunar-based Islamic calendar.

The celebration of Eid al-Adha is to commemorate prophet Ibrahim’s (Abraham) devotion to Allah and his readiness to sacrifice his son, Ismail (Ishmael). At the very point of sacrifice, Allah replaced Ismail with a ram, which was to be slaughtered in place of his son. This command from Allah was a test of prophet Ibrahim’s willingness and commitment to obey his Lord’s command, without question. Therefore, Eid al-Adha means the festival of sacrifice.

One of the best deeds for Muslims to perform on Eid al-Adha is the act of Qurbani/Uhdiya (sacrifice), which is carried out following the Eid Prayer. Muslims sacrifice an animal in order to remember Prophet Abraham's sacrifice for Allah. The sacrificial animal must be a sheep, lamb, goat, cow, bull or a camel. The animal must be in good health and over a certain age in order to be slaughtered, in a halal, Islamic way. The meat of the sacrificed animal is shared among the person making the sacrifice, their friends and family and the poor and needy.

Sacrificing an animal on Eid al-Adha reflects prophet Abraham's readiness to sacrifice his own son and it is also an act of worship found in the Quran and a confirmed tradition of prophet Muhammad {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

In the Quran, God has said:

{{ $page->verse('2:196..') }}
