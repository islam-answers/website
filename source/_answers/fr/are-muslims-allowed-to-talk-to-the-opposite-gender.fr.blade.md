---
extends: _layouts.answer
section: content
title: Les musulmans sont-ils autorisés à parler au sexe opposé ?
date: 2024-12-12
description: Dans l'Islam, l'interaction avec le sexe opposé est autorisée par nécessité,
  mais elle doit être pudique, brève et éviter la tentation.
sources:
- href: https://islamqa.info/en/answers/27304/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1497/
  title: islamqa.info
- href: https://islamqa.info/en/answers/34841/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21183/
  title: islamqa.info
translator_id: 586055
---

Dans l’Islam, l’interaction avec le sexe opposé est autorisée en cas de nécessité, mais doit être modeste, brève et éviter la tentation.

### Le principe de prévention (éviter les dommages)

En Islam, tout ce qui peut conduire une personne à tomber dans des choses interdites (haram) est également interdit, même si en principe c'est permis. C'est ce que les savants appellent le principe de protection contre le mal. Allah dit dans le Coran :

{{ $page->verse('24:21..') }}

La conversation - qu'elle soit personnelle, verbale ou écrite - entre hommes et femmes est permise en soi, mais elle peut être un moyen de tomber dans le piège (fitnah) tendu par Satan (Shaytaan).

### Les dangers des interactions avec le sexe opposé

Il ne fait aucun doute que la fitnah (tentation) des femmes est grande. Le prophète {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:2741') }}

Le musulman doit donc se méfier de cette fitnah et s’éloigner de tout ce qui pourrait l’en faire tomber victime. L’une des principales causes de cette fitnah est le fait de regarder les femmes et de les fréquenter.

Allah dit dans le Coran :

{{ $page->verse('24:30-31') }}

Ici, Allah ordonne à son prophète {{ $page->pbuh() }} de dire aux croyants et aux croyantes de baisser leur regard et de préserver leur chasteté, puis il explique que c'est plus pur pour eux. Garder sa chasteté et éviter les actions immorales n'est possible qu'en évitant les moyens qui mènent à de telles actions. Il ne fait aucun doute que le fait de laisser errer son regard et de mélanger les hommes et les femmes sur le lieu de travail et ailleurs sont parmi les plus grands moyens qui mènent à l'immoralité.

Combien de fois ces conversations ont-elles abouti à de mauvais résultats, et même fait tomber les gens amoureux, et ont conduit certains à faire des choses encore plus graves que cela. Satan (Shaytaan) fait imaginer à chacun d'eux des qualités attrayantes chez l'autre, ce qui les conduit à développer un attachement préjudiciable à leur bien-être spirituel et à leurs affaires mondaines.

### Etiquettes pour parler au sexe opposé

Le Mahram d'une personne (traduit en français par « parent non mariable ») est une personne qu'elle n'est jamais autorisée à épouser en raison de son lien de sang étroit (pour une femme, ce sont ses ancêtres, ses descendants, ses frères, ses oncles et ses neveux) ou en raison de l'allaitement ou parce qu'ils sont liés par mariage (pour une femme, ce sont les ancêtres de son mari, ses beaux-pères, ses descendants et les maris de ses filles).

Parler avec une personne du sexe opposé à laquelle on n'est pas lié (c'est-à-dire qui n'est pas mahram) ne doit se faire que pour un besoin spécifique, comme poser une question, acheter ou vendre, demander des nouvelles du chef de famille, etc. Ces conversations doivent être brèves et aller droit au but, sans que rien ne soit douteux, ni dans ce qui est dit ni dans la manière dont c'est dit. La conversation ne doit pas s'éloigner du sujet abordé ; elle ne doit pas porter sur des questions personnelles qui n'ont aucun rapport avec le sujet abordé, comme l'âge, la taille, l'endroit où l'on vit, etc.

L’islam interdit toutes les voies qui peuvent conduire à la fitnah (tentation), c’est pourquoi il interdit la douceur de langage et ne permet pas à un homme de rester seul avec une femme non-mahram. Ils ne doivent pas avoir une voix douce ni utiliser des expressions douces et gentilles. Ils doivent plutôt parler sur le même ton de voix ordinaire qu’ils parleraient à n’importe qui d’autre. Allah, qu’Il soit exalté, dit en s’adressant aux Mères des Croyants :

{{ $page->verse('..33:32') }}

Il n'est pas non plus permis à un homme d'être seul avec une femme qui n'est pas sa mahram, car le prophète {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:1341') }}

Il faut immédiatement mettre un terme à la conversation ou à la correspondance si le cœur commence à s'agiter de désir. Il faut également éviter de plaisanter, de rire ou de flirter. Une autre règle de bienséance dans les interactions avec le sexe opposé consiste à éviter de fixer du regard et à toujours essayer de baisser le regard autant que possible. Jarir bin 'Abdullah a rapporté :

{{ $page->hadith('muslim:2159') }}

Il convient également de mentionner qu'il n'est pas permis à un homme qui croit en Allah et en son messager {{ $page->pbuh() }} d'avoir un contact physique avec un membre du sexe opposé qui n'est pas Mahram pour lui.
