<?php

namespace App\Listeners;

use App\CollectionItem\Answer;
use TightenCo\Jigsaw\IterableObject;
use TightenCo\Jigsaw\Jigsaw;

class BuildRedirects
{
    private function getTitleRedirects(IterableObject $answers)
    {
        return $answers
            ->filter(fn (Answer $answer, string $key) => $answer->old_titles)
            ->mapWithKeys(fn ($answer) => collect($answer->old_titles)
                ->mapWithKeys(function ($oldTitle) use ($answer) {
                    $currentSlug = $answer->getSlug();
                    $currentPath = $answer->getRealUrl();
                    $clone = clone $answer;
                    $clone['title'] = $oldTitle;
                    $oldTitleSlug = $clone->getSlug();
                    $oldTitlePath = str_replace($currentSlug, $oldTitleSlug, $currentPath);

                    return [$oldTitlePath => $currentPath];
                })
            );
    }

    private function writeNetlifyStyleRedirects(IterableObject $redirects)
    {
        $redirectsFile = collect();

        $redirects->each(function ($newPath, $oldPath) use ($redirectsFile) {
            $redirectsFile->push("{$oldPath} {$newPath} 301");
        });

        return $redirectsFile->implode("\n");
    }

    private function updateFirebaseJson(IterableObject $redirects)
    {
        $firebaseJson = json_decode(file_get_contents('firebase.json'), true);
        $existingRedirects = collect($firebaseJson['hosting']['redirects']);

        $redirects->each(function ($newPath, $oldPath) use (&$firebaseJson, $existingRedirects) {
            $exists = $existingRedirects->contains(fn ($redirect) => $redirect['source'] === $oldPath && $redirect['destination'] === $newPath);

            if (!$exists) {
                $firebaseJson['hosting']['redirects'][] = [
                    "source" => $oldPath,
                    "destination" => $newPath,
                    "type" => 301,
                ];
            }
        });

        file_put_contents('firebase.json', json_encode($firebaseJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }

    public function handle(Jigsaw $jigsaw)
    {
        $answers = $jigsaw->getCollection('answers');
        $redirects = $this->getTitleRedirects($answers);
        $redirectsFile = $this->writeNetlifyStyleRedirects($redirects);

        $this->updateFirebaseJson($redirects);

        file_put_contents($jigsaw->getDestinationPath() . '/_redirects', $redirectsFile);
    }
}