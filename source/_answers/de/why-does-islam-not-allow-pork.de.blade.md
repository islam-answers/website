---
extends: _layouts.answer
section: content
title: Warum ist Schweinefleisch im Islam nicht erlaubt?
date: 2024-09-14
description: Muslime befolgen alles, was Allah ihnen aufträgt, und unterlassen alles,
  was Er ihnen verbietet, unabhängig davon, ob der Grund dafür klar ist oder nicht.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 587746
---

Das grundlegende Prinzip für den Muslim ist, dass er alles befolgt, was Allah ihm auferlegt, und sich von allem fernhält, was Er ihm verbietet, unabhängig davon, ob der Grund dafür klar ist oder nicht.

Es ist für einen Muslim nicht erlaubt, eine Entscheidung der Scharia abzulehnen oder bei deren Befolgung zu zögern, nur weil der Grund dafür nicht klar ist. Vielmehr muss er die Vorschriften über halal und haram akzeptieren, wenn sie im Text belegt sind, unabhängig davon, ob er den Grund dafür versteht oder nicht. Allah sagt:

{{ $page->verse('33:36') }}

### Warum ist Schweinefleisch haram?

Schweinefleisch ist im Islam haram (verboten), gemäß dem Text des Qurans, wo Allah sagt:

{{ $page->verse('2:173..') }}

Es ist einem Muslim grundsätzlich untersagt, Schweinefleisch zu essen, außer in echten Notfällen, in denen das Überleben davon abhängt. Solche Situationen könnten etwa bei extremer Hungersnot auftreten, wenn keine andere Nahrung verfügbar ist und die Gefahr des Todes besteht.

In den Texten der Scharia wird kein spezifischer Grund für das Verbot von Schweinefleisch erwähnt, außer in dem Vers, in dem Allah sagt:

{{ $page->verse('..6:145..') }}

Das Wort rijs (hier als „unrein“ übersetzt) bezeichnet alles, was im Islam und nach der natürlichen menschlichen Veranlagung (fitrah) als abscheulich angesehen wird. Dieser Grund allein ist bereits ausreichend.

Es gibt einen allgemeinen Grund für das Verbot von haram Lebensmitteln und Getränken, der auch den Grund für das Verbot von Schweinefleisch erklärt. Dieser allgemeine Grund ist in dem Vers zu finden, in dem Allah sagt:

{{ $page->verse('..7:157..') }}

### Wissenschaftliche und medizinische Gründe für das Verbot von Schweinefleisch

Wissenschaftliche und medizinische Forschungen haben ebenfalls bestätigt, dass das Schwein im Vergleich zu anderen Tieren als Überträger von Keimen gilt, die dem menschlichen Körper schädlich sind. Eine detaillierte Erklärung aller dieser schädlichen Krankheiten würde zu lange dauern, aber kurz zusammengefasst lassen sich folgende Hauptkategorien nennen: parasitäre Erkrankungen, bakterielle Infektionen, Viruserkrankungen und ähnliches.

Diese und andere schädliche Auswirkungen zeigen, dass Allah Schweinefleisch aus einem wichtigen Grund verboten hat: um Leben und Gesundheit zu bewahren, die zu den fünf grundlegenden Bedürfnissen gehören, die von der Scharia geschützt werden.
