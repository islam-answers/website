<blockquote class="border-green-600">
    <p class="mb-2">
        {!! $text !!}
    </p>

    <cite class="text-xs text-gray-600">
        <a href="https://sunnah.com/{{ $reference }}" title="{{ $reference_label }}" class="text-gray-600" target="_blank">{{ $reference_label }}</a>
    </cite>
</blockquote>
