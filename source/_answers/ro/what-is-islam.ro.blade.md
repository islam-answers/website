---
extends: _layouts.answer
section: content
title: Ce este Islamul?
date: 2024-07-16
description: Islamul înseamnă supunere, ascultarea poruncilor și respectarea interdicțiilor
  fără obiecții, închinarea numai lui Allah și credința în El.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 554943
---

Consultând dicționarele de limbă arabă, veți descoperi că sensul cuvântului Islam este: supunere, umilință, ascultarea de porunci și respectarea interdicțiilor fără obiecții, adorarea sinceră doar a lui Allah, credința în ceea ce El ne spune și credința în El.

Cuvântul Islam a devenit numele religiei care a fost introdusă de profetul Muhammad {{ $page->pbuh() }}.

### De ce această religie se numește Islam

Toate religiile de pe pământ poartă diverse nume - fie numele unui anumit om, fie numele unei anumite națiuni. Astfel, creștinismul își ia numele de la Hristos; budismul își ia numele de la fondatorul său, Buddha. În mod similar, iudaismul și-a luat numele de la un trib numit Yehudah (Iuda), așa că a devenit cunoscut sub numele de iudaism. Și așa mai departe.

Cu excepția Islamului, care are un nume ce nu este atribuit unui anumit om sau unei anumite națiuni, ci se referă la semnificația cuvântului "islam" descrisă anterior. Acest nume indică faptul că fondarea acestei religii nu a fost opera unui om anume și că nu este doar pentru o națiune anume, excluzându-le pe toate celelalte. Scopul său este mai degrabă de a oferi atributul implicat de cuvântul "islam" tuturor popoarelor de pe pământ. Astfel, oricine dobândește acest atribut, indiferent dacă este din trecut sau din prezent, este musulman, iar oricine dobândește acest atribut în viitor va fi, de asemenea, musulman.

Islamul este religia tuturor profeților. A apărut la începutul Profeției, din timpul tatălui nostru Adam, și toate mesajele au chemat către islam (supunerea față de Allah) în ceea ce privește credința și regulile de bază. Credincioșii care i-au urmat pe profeții anteriori au fost cu toții musulmani în sens general și vor intra în Paradis în virtutea islamului lor.
