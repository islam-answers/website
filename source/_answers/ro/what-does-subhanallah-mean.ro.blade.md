---
extends: _layouts.answer
section: content
title: Ce înseamnă Subhanallah?
date: 2024-10-07
description: Tradus deseori ca "Slavă lui Allah", "SubhanAllah" apare în Coran pentru
  a nega deficiențele atribuite nedrept lui Allah și pentru a afirma perfecțiunea
  Sa.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 554943
---

Termenul din limba Arabǎ "tasbih" se referă de obicei la expresia "SubhanAllah". Este o expresie pe care Allah a aprobat-o pentru Sine, i-a inspirat pe îngerii Săi să o rostească și i-a îndrumat pe cei mai buni din creația Sa să o declare. "SubhanAllah" se traduce în mod obișnuit prin "Slavă lui Allah", însă această traducere nu reușește să transmită semnificația completă a tasbih-ului.

Tasbih-ul este compus din două cuvinte: Subhana și Allah. Lingvistic, cuvântul "subhana" provine din cuvântul "sabh", care înseamnă distanță, depărtare. În consecință, Ibn 'Abbas a explicat fraza ca fiind puritatea și absolvirea lui Allah deasupra oricărei chestiuni rele sau nepotrivite. Cu alte cuvinte, Allah este veșnic departe și veșnic înalt deasupra oricărei fapte rele, deficiențe, neajunsuri și nepotriviri.

Această expresie apare în Coran pentru a nega descrierile nepotrivite și incompatibile atribuite lui Allah:

{{ $page->verse('23:91') }}

Un singur tasbih este suficient pentru a nega orice defect sau minciună atribuită lui Allah în orice loc și în orice moment de către oricare sau de către întreaga creație. Astfel, Allah respinge numeroasele minciuni care Îi sunt atribuite cu un singur tasbih:

{{ $page->verse('37:149-159') }}

Scopul negării este ca opusul său să fie afirmat. De exemplu, atunci când Coranul neagă ignoranța din partea lui Allah, atunci musulmanul neagă ignoranța și, la rândul său, afirmă opusul lui Allah, care este cunoașterea atotcuprinzătoare. Tasbih-ul este în mod fundamental o negare (a deficiențelor, a faptelor greșite și a afirmațiilor false) și, prin urmare, în conformitate cu acest principiu, implică afirmarea opusului, care este existența frumoasă, comportamentul perfect și adevărul suprem.

Sa'd bin Abu Waqqas (Allah să fie mulțumit de el) a raportat:

{{ $page->hadith('muslim:2698') }}
