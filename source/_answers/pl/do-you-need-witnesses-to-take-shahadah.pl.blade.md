---
extends: _layouts.answer
section: content
title: Czy potrzebni są świadkowie, aby wypowiedzieć Szahadę?
date: 2024-07-24
description: Nie jest konieczne składanie szahady (świadectwa wiary) przed świadkami.
  Islam jest sprawą między człowiekiem a jego Panem.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 584496
---

Aby ktoś stał się muzułmaninem, nie jest konieczne, aby deklarował swoją przynależność do Islamu przed kimkolwiek. Islam jest sprawą między człowiekiem a jego Panem, niech będzie błogosławiony i wywyższony.

Jeśli ktokolwiek zwróci się do ludzi, by byli świadkami o jego przynależności do Islamu, w celu uwierzytelnienia tego faktu w jego osobistych dokumentach, nie ma w tym nic złego, ale nie powinno to być warunkiem ważności jego muzułmańskiej wiary.
