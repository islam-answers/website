---
extends: _layouts.answer
section: content
title: Cosa dice l'Islam sul terrorismo?
date: 2024-05-26
description: L'Islam è una religione di misericordia e non consente il terrorismo.
sources:
- href: https://www.islam-guide.com/it/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

L'Islam è una religione di misericordia e non consente il terrorismo. Dio disse nel Corano:

{{ $page->verse('60:8') }}

Il profeta Mohammed proibiva {{ $page->pbuh() }} ai soldati di uccidere donne e bambini e li avvisava:

{{ $page->hadith('tirmidhi:1408') }}

Disse anche:

{{ $page->hadith('bukhari:3166') }}

Il profeta Mohammed {{ $page->pbuh() }} aveva anche proibito la punizione con il fuoco.

Egli elencò l'omicidio come il secondo di tutti i peccati e avvisava anche che nel giorno del giudizio,

{{ $page->hadith('bukhari:6533') }}

I musulmani sono anche incoraggiati a essere gentili con gli animali ed è proibito maltrattarli. Una volta il profeta Mohammed {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:3318') }}

Disse anche che un uomo diede da bere a un cane, così Dio dimenticò i suoi peccati a causa di questa buona azione. Fu chiesto al profeta: “Messaggero di Dio, saremo ricompensati per la gentilezza verso gli animali?” Egli disse:

{{ $page->hadith('bukhari:2466') }}

In aggiunta, quando i musulmani macellano gli animali è loro imposto di farli spaventare e soffrire il meno possibile. Il profeta Mohammed {{ $page->pbuh() }} disse:

{{ $page->hadith('tirmidhi:1409') }}

Alla luce di questi e altri testi islamici, l'atto di incitare al terrore nei cuori dei civili senza difese, la distruzione completa di edifici e proprietà, il bombardamento e lo storpiare uomini innocenti, donne e bambini sono atti proibiti e detestabili secondo l'Islam e i musulamni.

I musulmani seguono una religione di pace, misericordia e perdono e la maggior parte non ha nulla a che vedere con i violenti eventi che sono associati ai musulmani. Se un musulmano commette un atto di terrorismo, questa persona sarà colpevole di violare le leggi dell'Islam.

Tuttavia, è importante distinguere tra terrorismo e legittima resistenza all’occupazione, poiché i due sono molto diversi.
