---
extends: _layouts.answer
section: content
title: Koje su glavne prakse islama?
date: 2024-09-24
description: Glavne prakse islama se nazivaju pet stubova islama. Oni uključuju svedočanstvo
  vere (šehadet), molitve, propisano dobročinstvo, post i hodočašće.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 584497
---

Glavne prakse islama se nazivaju pet stubova islama.

### Prvi stub: svedočanstvo vere

Da izjavi da nema Boga dostojnog obožavanja osim Allaha, a Muhamed je Njegov Posljednji poslanik.

###Drugi stub: Molitva

Da obavlja molitvu pet puta dnevno, jednom u zoru, podne, sredinom podneva, posle zlaska sunca, i noću.

### Treći stub: propisano dobročinstvo "Zekat"

Ovo je obavezna godišnja pomoć koja se isplaćuje onima koji su u potrebi i izračunava se kao mali dio nečije ukupne godišnje ušteđevine, koja uključuje 2.5% novčanog bogatstva a može uključivati i drugu imovinu. Uplaćuju ga oni koji ima višak bogatstva.

### Četvrti stub: post u mesecu Ramazanu

Tokom ovog meseca, muslimani se moraju uzdržavati od svake hrane, pića i seksualnih odnosa sa svojim supružnicima, od zore do zalaska sunca. Promoviše samokontrolu, svest o Bogu i empatiju prema siromašnima.

### Peti stub: Hodočašće

Svaki sposobni musliman je dužan da hodočašasti u Meku tokom svog života. To uključuje klanjanje, molitvu, milostinju i putovanje, i veoma je ponizno i duhovno iskustvo.
