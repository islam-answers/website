---
extends: _layouts.answer
section: content
title: Hồi giáo nói gì về cách đối xử với cha mẹ?
date: 2024-09-17
description: Đối xử tốt với cha mẹ là một trong những hành động được đền đáp cao nhất trước mắt Chúa, theo lời dạy của Kinh Qur'an.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 587785
---

Thể hiện lòng tốt với cha mẹ là một trong những hành động được đền đáp cao nhất trước mắt Allah. Mệnh lệnh phải đối xử tốt với cha mẹ là lời dạy rõ ràng của Qur'an. Allah phán:

{{ $page->verse('4:36..') }}

Việc đề cập đến sự phục tùng cha mẹ diễn ra ngay sau sự phục tùng Chúa. Điều này được lặp lại trong toàn bộ Kinh Qur'an.

{{ $page->verse('17:23') }}

Trong câu này, Thượng đế ra lệnh cho chúng ta chỉ thờ phụng Ngài và không ai khác, và ngay lập tức chuyển sự chú ý sang việc cư xử tốt với cha mẹ, đặc biệt là khi họ già đi và phụ thuộc vào người khác để có nơi trú ẩn và nhu cầu của họ. Người Hồi giáo được dạy không được thô lỗ với cha mẹ hoặc la mắng họ, mà thay vào đó hãy nói chuyện với họ bằng những lời tử tế và thể hiện tình yêu thương và sự dịu dàng.

Trong kinh Qur'an, Allah còn yêu cầu con người cầu xin Ngài cho cha mẹ mình:

{{ $page->verse('17:24') }}

Allah nhấn mạnh rằng vâng lời cha mẹ chính là vâng lời Ngài, ngoại trừ việc đặt Ngài ngang hàng với những thánh thần khác.

{{ $page->verse('31:14-15..') }}

Câu thơ này nhấn mạnh rằng tất cả cha mẹ đều xứng đáng được đối xử tốt. Cần phải chăm sóc cho các bà mẹ nhiều hơn, vì họ phải chịu nhiều khó khăn hơn.

Việc chăm sóc cha mẹ được coi là một trong những việc làm tốt nhất:

{{ $page->hadith('muslim:85e') }}

Tóm lại, trong đạo Hồi, tôn kính cha mẹ có nghĩa là vâng lời họ, tôn trọng họ, cầu nguyện cho họ, hạ giọng khi ở trước mặt họ, mỉm cười với họ, không tỏ ra khó chịu với họ, cố gắng phục vụ họ, thực hiện mong muốn của họ, hỏi ý kiến họ và lắng nghe những gì họ nói.
