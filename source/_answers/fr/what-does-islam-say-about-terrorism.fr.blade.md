---
extends: _layouts.answer
section: content
title: Que dit l'islam à propos du terrorisme ?
date: 2024-06-08
description: L'islam, qui est une religion de miséricorde, ne permet pas le terrorisme.
sources:
- href: https://www.islam-guide.com/fr/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

L'islam, qui est une religion de miséricorde, ne permet pas le terrorisme. Dans le Coran, Dieu a dit:

{{ $page->verse('60:8') }}

Le prophète Mohammed {{ $page->pbuh() }} interdisait aux soldats de tuer des femmes et des enfants, et il leur conseillait:

{{ $page->hadith('tirmidhi:1408') }}

Et il a aussi dit:

{{ $page->hadith('bukhari:3166') }}

Le prophète Mohammed {{ $page->pbuh() }} a aussi interdit de punir les gens par le feu.

Une fois, il a classé le meurtre comme deuxième péché majeur, et il a même averti les gens que:

{{ $page->hadith('bukhari:6533') }}

Les musulmans sont même encouragés à être bons envers les animaux et il leur est interdit de leur faire du mal. Une fois, le prophète Mohammed {{ $page->pbuh() }} a raconté:

{{ $page->hadith('bukhari:3318') }}

Il a aussi raconté qu'un homme ayant donné à boire à un chien assoiffé, Dieu lui pardonna ses péchés pour cette action. On demanda au prophète: "Ô messager de Dieu, sommes-nous récompensés pour le bien que nous faisons aux animaux?" Il répondit:

{{ $page->hadith('bukhari:2466') }}

Par ailleurs, lorsque les musulmans tuent un animal pour se nourrir, ils doivent le faire de la façon qui cause le moins de frayeur et de douleur possible. Le prophète Mohammed {{ $page->pbuh() }} a dit:

{{ $page->hadith('tirmidhi:1409') }}

À la lumière de ces textes et d'autres textes islamiques, le fait de provoquer la terreur dans les coeurs de civils sans défense, la destruction massive d'édifices et de propriétés, le bombardement et la mutilation d'hommes, de femmes et d'enfants innocents sont tous des actes interdits et détestables aux yeux de l'islam et des musulmans.

Les musulmans pratiquent une religion basée sur la paix, la miséricorde et le pardon, et la vaste majorité d'entre eux n'ont rien à voir avec les violents événements que certains associent aux musulmans. Si un musulman commettait un acte de terrorisme, il serait coupable d'avoir violé les lois de l'islam.

Il est toutefois important de faire la distinction entre le terrorisme et la résistance légitime à l’occupation, car les deux sont très différents.
