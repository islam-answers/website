---
extends: _layouts.answer
section: content
title: Czy Islam jest przeciwny psom?
date: 2024-08-30
description: Islam uczy nas traktować zwierzęta godnie i miłosiernie. Psów nie można trzymać jako zwierząt domowych, ale można je trzymać w określonych celach.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 584496
---

Zwierzęta są dziełem Allaha i nie patrzymy na nie źle; raczej jesteśmy zobowiązani, jako muzułmanie, by traktować zwierzęta w sposób godny, humanitarny i miłosierny. Posłaniec Allaha {{ $page->pbuh() }} powiedział:

{{ $page->hadith('bukhari:3321') }}

## Zabijanie psów

Abdallah ibn Umar powiedział:

{{ $page->hadith('muslim:1570b') }}

Ważne jest wyjaśnienie, o jakich psach mowa w tej narracji, a także o ogólnym kontekście. Psy, o których mowa w tym miejscu, to stada dzikich psów, które wędrują po miasteczkach i miastach, są utrapieniem dla społeczności i zagrożeniem dla zdrowia. Przenoszą i rozprzestrzeniają choroby, tak jak szczury, i powodują wiele niedogodności dla ludzi. Zabijanie niebezpiecznych zwierząt lub zwierząt rozprzestrzeniających choroby jest powszechne i akceptowane na całym świecie.

Na podstawie wszystkich ogólnych dowodów prawnicy stwierdzili, że polecenie zabicia psa jest dopuszczalne tylko wtedy, gdy jest drapieżny i szkodliwy. Jednakże nieszkodliwy pies, niezależnie od koloru, nie może zostać zabity.

## Czystość

Ślina psów jest nieczysta według większości muzułmańskich uczonych, podobnie jak ich futro według niektórych. Jedną z najważniejszych nauk islamu jest czystość na wszystkich poziomach, więc to było brane pod uwagę, podobnie jak fakt, że aniołowie nie odwiedzają miejsc, w których są psy. Dzieje się tak ze względu na słowa Proroka {{ $page->pbuh() }} :

{{ $page->hadith('bukhari:3322') }}

## Trzymanie psów jako zwierząt domowych

Islam zabrania muzułmanom trzymania psów jako zwierząt domowych. Posłaniec Allaha {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:1574g') }}
