---
extends: _layouts.answer
section: content
title: Da li islam promoviše rasizam?
date: 2024-08-27
description: Islam ne obraća pažnju na razlike u boji, rasi ili lozi.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 584497
---

Svi ljudi su potomci jednog muškarca i jedne žene, vernika i kaafira (nevernika), crnog i belog, Arapa i ne-Arapa, bogatih i siromašnih, plemenitih i poniznih.

Islam ne obraća pažnju na razlike u boji, rasi ili lozi. Svi ljudi potiču od Adema (Adam), a Adem je stvoren od prašine. U islamu, razlika između ljudi se zasniva na veri (iman) i pobožnosti (takva), čineći ono što je Allah naredio i izbegavajući ono što je Allah zabranio. Allah kaže:

{{ $page->verse('49:13') }}

Poslanik Muhamed {{ $page->pbuh() }} je rekao:

{{ $page->hadith('muslim:2564b') }}

Islam smatra da su svi ljudi jednaki što se tiče prava i dužnosti. Ljudi su jednaki pred zakonom (šerijatom), kao što Allah kaže:

{{ $page->verse('16:97') }}

Vera, istinitost i pobožnost vode u Džennet (raj), koji je pravo onoga ko stekne ove osobine, čak i ako je jedan od najslabijih ili najnižih ljudi. Allah kaže:

{{ $page->verse('..65:11') }}

Kufr (neverovanje), arogancija i ugnjetavanje vode u pakao, čak i ako je onaj koji čini ove stvari jedan od najbogatijih ili najplemenitijih ljudi. Allah kaže:

{{ $page->verse('64:10') }}

Savetnici proroka Muhameda {{ $page->pbuh() }} bilii su muslimani svih plemena, rasa i boja. Njihova srca su bila ispunjena tevhidom (monoteizmom) i bili su okupljeni svojom verom i pobožnošću – kao što su Ebu Bekr iz Kurejša, 'Ali ibn Ebi Taalib iz Bani Haašima, Bilal Etiopljanin, Suhejb Rimljanin, Salman Persijanac, bogataši poput Osmana i siromašni ljudi poput Ammara, ljudi sa imovinom i siromašni ljudi kao što je Ehl-Suffah, i drugi.

Oni su vjerovali u Allaha i borili se za Njega dobro, sve dok Allah i Njegov Poslanik nisu bili zadovoljni njima. Oni su bili pravi vernici.

{{ $page->verse('98:8') }}
