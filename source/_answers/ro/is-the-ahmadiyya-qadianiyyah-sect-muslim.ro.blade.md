---
extends: _layouts.answer
section: content
title: Este secta Ahmadiyya (Qadianiyyah) musulmană?
date: 2025-01-19
description: Ahmadiyya este un grup rătăcit, care nu face deloc parte din Islam. Credințele
  lor sunt complet contradictorii cu Islamul.
sources:
- href: https://islamqa.info/en/answers/170092/
  title: islamqa.info
- href: https://islamqa.info/en/answers/4060/
  title: islamqa.info
- href: https://islamqa.info/en/answers/45525/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/103015/
  title: islamqa.org (AskImam.org)
translator_id: 554943
---

Ahmadiyya (Qadianiyyah) este un grup rătăcit, care nu face deloc parte din Islam. Credințele lor sunt complet contradictorii cu Islamul. Musulmanii trebuie să știe despre ei și activitățile lor, deoarece savanții Islamului au declarat că aceștia sunt necredincioși.

Qadianiyyah este o mișcare care a început în 1900 ca un complot al colonialiștilor britanici de pe subcontinentul indian, cu scopul de a-i abate pe musulmani de la religia lor și de la obligația Jihadului în special, astfel încât aceștia să nu se opună colonialismului în numele Islamului. Revista Majallat Al-Adyaan (Revista religiilor), care a fost publicată în limba engleză, a răspândit ideile acestei mișcări.

Mirza Ghulam Ahmad al-Qadiani a fost principalul instrument prin intermediul căruia a fost fondată secta Qadianiyyah. El s-a născut în satul Qadian, în Punjab, India, în 1839. Provenea dintr-o familie cunoscută pentru faptul că și-a trădat religia și țara, astfel că Ghulam Ahmad a devenit loial și ascultător față de coloniști în toate sensurile. Astfel, el a fost ales pentru rolul unui așa-zis profet, pentru ca musulmanii să se adune în jurul său și el să le distragă atenția de la nevoia de a face Jihad (a se lupta) împotriva coloniștilor englezi. Guvernul britanic le-a făcut o mulțime de favoruri, așa că au fost loiali britanicilor. Ghulam Ahmad era cunoscut printre adepții săi ca fiind instabil, cu multe probleme de sănătate și dependent de droguri.

Ghulam Ahmad și-a început activitățile ca un predicator (care cheamă oamenii către Islam), astfel încât să poată aduna adepți în jurul său, apoi a pretins că este un mujaddid (neînnoitor, inovator) inspirat de Allah. Apoi a făcut un pas mai departe și a pretins că este Mahdi cel Așteptat și Mesia cel Promis. Apoi a pretins că este un profet și că profeția sa este mai înaltă decât cea a lui Muhammad {{ $page->pbuh() }}.

Qadianii cred că Allah postește, se roagă, doarme, se trezește, scrie, face greșeli și are relații sexuale - înălțat să fie Allah cu mult peste tot ceea ce spun ei! De asemenea, ei cred că Dumnezeul lor este englez, deoarece le vorbește în engleză. Ei cred că cartea lor, al-Kitaab al-Mubeen, a fost revelată și este diferită de Sfântul Coran. Ei au cerut abolirea Jihadului și supunerea oarbă față de guvernul britanic pentru că, după cum au susținut, britanicii erau "cei cu autoritate", așa cum se menționează în Coran. De asemenea, ei permit consumul de alcool, opiu, droguri și substanțe toxice.

Savanții contemporani au fost unanim de acord că Qadianii sunt în afara Islamului, deoarece credințele lor includ lucruri care constituie necredință și sunt contrare învățăturilor fundamentale ale Islamului. Această sectă a încălcat consensul definitiv al musulmanilor conform căruia nu există niciun Profet după Profetul nostru Muhammad {{ $page->pbuh() }}; acest lucru este indicat de o serie de texte din Coran și narațiuni profetice.

În prezent, majoritatea qadianilor locuiesc în India și Pakistan, cu câțiva în Palestina ocupată "Israel" și în lumea arabă. Ei încearcă, cu ajutorul coloniștilor, să obțină poziții importante în toate locurile în care trăiesc. Guvernul britanic sprijină, de asemenea, această mișcare și le facilitează adepților lor obținerea de funcții în guvernele lumii, în administrația corporațiilor și în consulate. Unii dintre ei sunt și ofițeri de rang înalt în serviciile secrete. Pentru a-i atrage pe oameni la credințele lor, qadianii folosesc tot felul de metode, în special mijloace educaționale, deoarece sunt foarte educați și există mulți oameni de știință, ingineri și medici în rândurile lor.

Nu este permis unui musulman să se roage în congregație în spatele unui imam (conducătorul rugăciunii) aparținând sectei Ahmedi, deoarece aceștia sunt clasificați drept nemusulmani. Musulmanii trebuie să nu frecventeze lăcașurile de cult și adunările acestora, deoarece sunt necredincioși. De asemenea, nu este permis unui musulman să se căsătorească cu unul dintre ei sau să își dea fiica în căsătorie unuia dintre ei, deoarece sunt necredincioși și apostați.
