---
extends: _layouts.answer
section: content
title: Chi ha scritto il Corano?
date: 2024-05-25
description: Il Corano è la parola letterale di Dio, che Egli rivelò al Suo Profeta
  Mohammed (la pace sia su di lui) attraverso l'Angelo Gabriele.
sources:
- href: https://www.islam-guide.com/it/ch1-1.htm
  title: islam-guide.com
---

Dio ha sostenuto il Suo ultimo profeta Mohammed {{ $page->pbuh() }} con molti miracoli e molte prove che ne confermano la veste di Profeta inviato da Dio. Inoltre, Dio ha sostenuto il Suo ultimo libro rivelato, il Sacro Corano, con molti miracoli che testimoniano che esso è parola di Dio, rivelato da Dio e che nessun essere umano ne è l'autore.

Il Corano è il verbo di Dio, che Egli ha rivelato al Suo profeta Mohammed {{ $page->pbuh() }} attraverso l'Angelo Gabriele. Mohammed {{ $page->pbuh() }} lo imparò a memoria e quindi lo dettò ai suoi Compagni. Questi, a loro volta, lo impararono a memoria, lo scrissero e lo rividero con il profeta Mohammed {{ $page->pbuh() }}. Inoltre, il profeta Mohammed {{ $page->pbuh() }} rivide il Corano con l'Angelo Gabriele una volta ogni anno e due volte nel suo ultimo anno di vita. Dal tempo in cui il Corano fu rivelato a tutt'oggi, un numero enorme di musulmani ha continuato a memorizzarlo tutto, lettera per lettera. Alcuni di loro sono stati persino in grado di memorizzare tutto il Corano all'età di dieci anni. Nel corso dei secoli, non una lettera del Corano è mai stata cambiata.

Il Corano, che fu rivelato quattordici secoli fa, menziona fatti che solo recentemente sono stati scoperti o provati dagli scienziati. Questo prova senz'ombra di dubbio che il Corano deve essere il Verbo di Dio, rivelato da Dio al profeta Mohammed {{ $page->pbuh() }} e che il Corano non è frutto di Mohammed {{ $page->pbuh() }} o di altro essere umano. Ciò prova anche che Mohammed {{ $page->pbuh() }} è veramente un profeta inviato da Dio. È fuori discussione che qualcuno quattordici secoli fa conoscesse questi fatti scoperti o provati solo recentemente con attrezzature avanzate e metodi sofisticati scientifici.
