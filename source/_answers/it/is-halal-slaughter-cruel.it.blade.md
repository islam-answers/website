---
extends: _layouts.answer
section: content
title: La macellazione Halal è cruenta?
date: 2024-12-16
description: La macellazione Halal è più umana e igienica dei metodi occidentali, che causano dolore e lasciano sangue nella carne.
sources:
- href: https://islamqa.org/hanafi/qibla-hanafi/35739/
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/136275/
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 573107
---

La pratica islamica che consiste nel macellare gli animali con un taglio netto nella parte anteriore del collo è stata spesso attaccata da alcuni attivisti per i diritti degli animali come una forma di crudeltà, sostenendo che si tratti di un metodo doloroso e disumano per uccidere gli animali. In Occidente, è obbligatorio per legge stordire gli animali con un colpo alla testa prima della macellazione, presumibilmente per rendere l'animale incosciente e per impedirgli di rianimarsi prima di essere ucciso, in modo da non rallentare in alcun modo la linea di lavorazione. Viene anche usato per evitare che l'animale provi dolore prima di morire.

### Uno studio tedesco sul dolore

Coloro che sono convinti di ciò, saranno quindi sorpresi nell'apprendere i risultati di uno studio condotto dal professor Wilhelm Schulze e dal suo collega dott. Hazim presso la facoltà di medicina veterinaria dell'Università di Hannover, in Germania. Lo studio - "Prove oggettive di dolore e di coscienza nei metodi convenzionali (CBP - stordimento con pistola a proiettile captivo) e rituali (halal, coltello) di macellazione di pecore e vitelli" - ha concluso che:

> La macellazione islamica è il metodo più umano, mentre la macellazione CBP, praticata in Occidente, provoca notevoli sofferenze all'animale.

Nello studio, diversi elettrodi sono stati impiantati chirurgicamente in vari punti del cranio di tutti gli animali, a contatto con la superficie del cervello. Quindi, agli animali è stato permesso di guarire e riprendersi per diverse settimane. Alcuni animali sono stati poi macellati praticando un'incisione rapida e profonda con un coltello affilato sul collo, tagliando le vene giugulari e le arterie carotidi, nonché la trachea e l'esofago (metodo islamico). Gli altri animali sono stati storditi utilizzando una pistola. Durante l'esperimento, un elettroencefalografo (EEG) e un elettrocardiogramma (ECG) hanno registrato le condizioni del cervello e del cuore di tutti gli animali durante il processo di macellazione e di stordimento.

I risultati sono stati i seguenti:

##### Metodo islamico

1. I primi tre secondi dal momento della macellazione islamica, registrati sull'EEG, non hanno mostrato alcun cambiamento rispetto al grafico precedente alla macellazione, dimostrando che l'animale non ha provato alcun dolore durante o immediatamente dopo l'incisione.
1. Nei 3 secondi successivi, l'EEG ha registrato una condizione di sonno profondo o perdita di coscienza. Ciò è dovuto alla grande quantità di sangue che sgorga dal corpo.
1. Dopo i suddetti 6 secondi, l'EEG ha registrato un livello pari a zero che dimostra l'assenza di qualsiasi sensazione di dolore.
1. Mentre il messaggio cerebrale (EEG) scendeva a livello zero, il cuore continuava a battere forte e il corpo era in preda a forti convulsioni (un'azione riflessa del midollo spinale) facendo uscire la massima quantità di sangue, ottenendo quindi una carne più sana per il consumo umano.

##### Metodo occidentale con stordimento CBP

1. Gli animali erano apparentemente incoscienti subito dopo lo stordimento.
1. L'EEG ha mostrato un forte dolore subito dopo lo stordimento.
1. I cuori degli animali storditi con CBP hanno smesso di battere prima rispetto a quelli degli animali macellati secondo il metodo islamico, con conseguente ritenzione di più sangue nella carne, rendendola meno sana per il consumatore.

### Regole islamiche per la macellazione

Come si evince da questo studio, la macellazione islamica degli animali è una benedizione sia per l'animale che per gli esseri umani. Affinché la macellazione sia lecita, chi la esegue deve sottostare a diverse regole. Questo per garantire il massimo beneficio sia all'animale che al consumatore. A questo proposito, il Profeta Mohammad {{ $page->pbuh() }} ha detto:

{{ $page->hadith('muslim:1955') }}

L'oggetto usato per macellare l'animale deve essere affilato e usato con rapidità. Il taglio veloce dei vasi del collo interrompe l'afflusso di sangue ai nervi del cervello responsabili del dolore. In questo modo l'animale non sente dolore. I movimenti e il cedimento che si verificano nell'animale dopo il taglio non sono causati dal dolore, ma dalla contrazione e dal rilassamento dei muscoli non più irrorati di sangue. Il Profeta {{ $page->pbuh() }} insegnò addirittura ai musulmani di non affilare la lama del coltello di fronte all'animale, né di macellare un animale davanti ad altri della sua stessa specie.

Il taglio deve includere la trachea, l'esofago e le due vene giugulari, senza tagliare il midollo spinale. Questo metodo consente di far defluire rapidamente il sangue dal corpo dell'animale. Se il midollo spinale venisse tagliato, le fibre nervose del cuore potrebbero essere danneggiate, causando un arresto cardiaco con conseguente ristagno di sangue nei vasi sanguigni. Il sangue deve essere drenato completamente prima di rimuovere la testa. In questo modo si purifica la carne, eliminando la maggior parte del sangue che funge da terreno di coltura per i microrganismi. Inoltre, la carne rimane fresca più a lungo rispetto ad altri metodi di macellazione.

Pertanto, le accuse di crudeltà sugli animali dovrebbero essere rivolte a coloro che non utilizzano il metodo di macellazione islamica, ma che invece preferiscono ricorrere a metodi che provocano dolore e agonia all'animale, e che potrebbero anche causare danni a chi ne consuma la carne.
