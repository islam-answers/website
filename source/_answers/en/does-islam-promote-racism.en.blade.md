---
extends: _layouts.answer
section: content
title: Does Islam promote racism?
date: 2024-08-21
description: Islam does not pay attention to differences in colour, race or lineage.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
---

All people are the descendants of one man and one woman, believer and kaafir (non-believer), black and white, Arab and non-Arab, rich and poor, noble and lowly.

Islam does not pay attention to differences in colour, race or lineage. All people come from Adam, and Adam was created from dust. In Islam, differentiation between people is based on faith (Iman) and piety (Taqwa), by doing that which Allah has enjoined and avoiding that which Allah has prohibited. Allah says:

{{ $page->verse('49:13') }}

The Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:2564b') }}

Islam regards all people as equal as far as rights and duties are concerned. People are equal before the law (Shari'ah), as Allah says:

{{ $page->verse('16:97') }}

Faith, truthfulness and piety all lead to Paradise, which is the right of the one who acquires these attributes, even if he is one of the weakest or lowliest of people. Allah says:

{{ $page->verse('..65:11') }}

Kufr (disbelief), arrogance and oppression all lead to Hell, even if the one who does these things is one of the richest or noblest of people. Allah says:

{{ $page->verse('64:10') }}

The advisors of Prophet Muhammad {{ $page->pbuh() }} included Muslim men of all tribes, races and colours. Their hearts were filled with Tawhid (monotheism) and they were brought together by their faith and piety – such as Abu Bakr from Quraysh, ‘Ali ibn Abi Taalib from Bani Haashim, Bilal the Ethiopian, Suhayb the Roman, Salman the Persian, rich men like ‘Uthman and poor men like ‘Ammar, people of means and poor people like Ahl al-Suffah, and others.

They believed in Allah and strove for His sake, until Allah and His Messenger were pleased with them. They were the true believers.

{{ $page->verse('98:8') }}
