---
extends: _layouts.answer
section: content
title: Care sunt sărbătorile musulmanilor?
date: 2024-06-15
description: 'Musulmanii sărbătoresc doar două Eid (sărbători): Eid al-Fitr (la sfârșitul
  lunii Ramadan) și Eid al-Adha, la sfârșitul pelerinajului anual (Hajj).'
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 554943
---

Musulmanii sărbătoresc doar două Eid (festivaluri): Eid al-Fitr (la sfârșitul lunii Ramadan) și Eid al-Adha, care marchează finalul perioadei anuale de pelerinaj (Hajj), în a zecea zi a lunii Dhul-Hijjah.

În timpul acestor două sǎrbǎtori, musulmanii se felicită unii pe alții, răspândesc bucurie în comunitățile lor și sărbătoresc cu familiile lor extinse. Dar, cel mai important, ei își amintesc de binecuvântările lui Allah asupra lor, Îi celebrează numele și oferă rugăciunea cu ocazia zilei de Eid în moschee. În afară de aceste două ocazii, musulmanii nu recunosc și nu sărbătoresc alte zile din an.

Desigur, există și alte ocazii de bucurie pentru care Islamul dictează sărbătorirea corespunzătoare, cum ar fi sărbătorirea nunții (walima) sau nașterea unui copil (aqeeqah). Cu toate acestea, aceste zile nu sunt specificate ca zile speciale ale anului; ci ele sunt sărbătorite așa cum se întâmplă în cursul vieții unui musulman.

În dimineața de Eid al-Fitr și Eid al-Adha, musulmanii participă la rugăciunile pentru Eid la moschee, urmate de o predică (khutbah) care le amintește musulmanilor de îndatoririle și responsabilitățile lor. După rugăciune, se felicitǎ reciproc cu expresia "Eid Mubarak!" (Eid binecuvântat!) și împart cadouri și dulciuri.

## Eid al-Fitr

Eid al-Fitr marchează sfârșitul Ramadanului, care are loc în cea de-a noua lună a calendarului islamic lunar.

Eid al-Fitr este important pentru că urmează dupǎ una dintre cele mai sacre luni dintre toate: Ramadanul. Ramadanul este o perioadă în care musulmanii își întăresc legătura cu Allah, recită Coranul și își sporesc numărul de fapte bune. La sfârșitul Ramadanului, Allah le oferă musulmanilor ziua de Eid al-Fitr ca o recompensă pentru că au reușit să încheie cu succes postul și pentru că au crescut în acte de adorare în timpul lunii Ramadanului. De Eid al-Fitr, musulmanii îi mulțumesc lui Allah pentru oportunitatea de a asista la un alt Ramadan, de a se apropia de El, de a deveni oameni mai buni și de a avea o nouă șansă de a fi salvați de Focul Iadului.

{{ $page->hadith('ibnmajah:3925') }}

## Eid al-Adha

Eid al-Adha marchează încheierea perioadei anuale de Hajj (pelerinaj la Mecca), care are loc în luna Dhul-Hijjah, a douăsprezecea și ultima lună a calendarului islamic lunar.

Sărbătoarea Eid al-Adha comemorează devotamentul profetului Ibrahim (Avraam) față de Allah și promptitudinea acestuia de a-și sacrifica fiul, Ismail (Ismael). Chiar în momentul sacrificiului, Allah l-a înlocuit pe Ismail cu un berbec, care urma să fie sacrificat în locul fiului său. Această poruncă de la Allah a fost un test al voinței și angajamentului profetului Ibrahim de a se supune poruncii Domnului său, fără să pună întrebări. Prin urmare, Eid al-Adha înseamnă sărbătoarea sacrificiului.

Una dintre cele mai bune fapte pe care musulmanii le pot face de Eid al-Adha este Qurbani/Uhdiya (sacrificiu), care se face după rugăciunea de Eid. Musulmanii sacrifică un animal pentru a-și aminti sacrificiul profetului Ibrahim (Avraam) pentru Allah. Animalul sacrificial trebuie să fie o oaie, miel, capră, vacă, taur sau cămilă. Animalul trebuie să fie în stare bună de sănătate și peste o anumită vârstă pentru a fi sacrificat, într-un mod halal, islamic. Carnea animalului sacrificat este împărțită între persoana care face sacrificiul, prietenii și familia sa, precum şi săracii și nevoiașii.

Sacrificarea unui animal în ziua de Eid al-Adha reflectă disponibilitatea profetului Avraam de a-și sacrifica propriul fiu și este, de asemenea, un act de închinare care se regăsește în Coran și o tradiție confirmată a profetului Muhammad {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

În Coran, Dumnezeu a spus:

{{ $page->verse('2:196..') }}
