---
extends: _layouts.answer
section: content
title: Zašto muškarci muslimani imaju bradu?
date: 2025-02-03
description: Muslimani puštaju bradu kako bi sledili primer poslanika Muhameda a.s,
  podržavali svoje urođene sklonosti i razlikovali se od nevernika.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 584497
---

Posljednji i najbolji od poslanika, Muhammed {{ $page->pbuh() }}, pustio je da mu raste brada, kao i halife koji su došli posle njega, i njegovi drugovi i vođe i obični ljudi muslimani. Ovo je put Poslanika i njihovih sljedbenika, i to je dio fitre (prvobitne predispozicije) kojom je Allah stvorio ljude. Aiša, radijallahu anhu, prenosi da je Allahov Poslanik {{ $page->pbuh() }} rekao:

{{ $page->hadith('muslim:261a') }}

Ono što se traži od vernika, ako su Allah i Njegov Poslanik nešto naredili, jeste da kaže: Mi slušamo i pokoravamo se, kao što Allah kaže:

{{ $page->verse('24:51') }}

Muslimani puštaju brade u pokornosti Allahu i Njegovom Poslaniku. Poslanik {{ $page->pbuh() }} naredio je muslimanima da slobodno puste brade i da podrežu brkove. Od Ibn Omera se prenosi da je Poslanik Muhammed {{ $page->pbuh() }} rekao:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn 'Abd al-Barr je rekao: "Zabranjeno je brijati bradu, i to niko ne radi osim muškaraca koji su ženstveni", tj. onih koji oponašaju žene. Džabir prenosi sljedeće o Poslaniku Muhammed {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2344b') }}

Šejh al-Islam Ibn Taymiyah (da mu se Allah smiluje) je rekao:

> Kur'an, sunnet i idžma' (konsenzus učenjaka) svi ukazuju na to da se moramo razlikovati od nevernika u svim aspektima i ne oponašati ih, jer će nas oponašanje izvana natjerati da ih oponašamo u njihovim lošim delima i navikama, pa čak i u verovanju. (...)
