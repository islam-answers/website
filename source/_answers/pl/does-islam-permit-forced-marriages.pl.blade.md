---
extends: _layouts.answer
section: content
title: Czy Islam zezwala na przymusowe małżeństwa?
date: 2024-07-24
description: Zarówno mężczyźni, jak i kobiety mają prawo do wyboru małżonka, a małżeństwo
  uznaje się za nieważne, jeśli kobieta nie wyrazi na to zgody.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 584496
---

Aranżowane małżeństwa to praktyki kulturowe, które dominują w niektórych krajach na całym świecie. Chociaż nie ograniczają się do muzułmanów, przymusowe małżeństwa stały się błędnie kojarzone z islamem.

W islamie zarówno mężczyźni, jak i kobiety mają prawo do wyboru lub odrzucenia potencjalnego małżonka, a małżeństwo uznaje się za nieważne, jeśli kobieta nie wyrazi na nie zgody przed zawarciem małżeństwa.
