---
extends: _layouts.answer
section: content
title: What does Islam say about terrorism?
date: 2024-02-12
description: Islam, a religion of mercy, does not permit terrorism.
sources:
- href: https://www.islam-guide.com/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

Islam, a religion of mercy, does not permit terrorism. In the Quran, God has said:

{{ $page->verse('60:8') }}

The Prophet Muhammad {{ $page->pbuh() }} used to prohibit soldiers from killing women and children, and he would advise them:

{{ $page->hadith('tirmidhi:1408') }}

And he also said:

{{ $page->hadith('bukhari:3166') }}

Also, the Prophet Muhammad {{ $page->pbuh() }} has forbidden punishment with fire.

He once listed murder as the second of the major sins, and he even warned that on the Day of Judgment,

{{ $page->hadith('bukhari:6533') }}

Muslims are even encouraged to be kind to animals and are forbidden to hurt them. Once the Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('bukhari:3318') }}

He also said that a man gave a very thirsty dog a drink, so God forgave his sins for this action. The Prophet was asked, “Messenger of God, are we rewarded for kindness towards animals?” He said:

{{ $page->hadith('bukhari:2466') }}

Additionally, while taking the life of an animal for food, Muslims are commanded to do so in a manner that causes the least amount of fright and suffering possible. The Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('tirmidhi:1409') }}

In light of these and other Islamic texts, the act of inciting terror in the hearts of defenseless civilians, the wholesale destruction of buildings and properties, the bombing and maiming of innocent men, women, and children are all forbidden and detestable acts according to Islam and the Muslims.

Muslims follow a religion of peace, mercy, and forgiveness, and the vast majority have nothing to do with the violent events some have associated with Muslims. If an individual Muslim were to commit an act of terrorism, this person would be guilty of violating the laws of Islam.

However, it is important to distinguish between terrorism and legitimate resistance to occupation, as the two are very different.
