---
extends: _layouts.answer
section: content
title: L'islam encourage-t-il le racisme ?
date: 2024-09-21
description: L’Islam ne prête pas attention aux différences de couleur, de race ou
  de lignée.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 586055
---

Tous les hommes sont les descendants d'un seul homme et d'une seule femme : croyants ou kaafir (non-croyants), noirs ou blancs, arabes ou non arabes, riches ou pauvres, nobles ou humbles.

L'islam ne fait pas de distinction entre les couleurs, les races ou les lignées. Tous les êtres humains descendent d'Adam, et Adam a été créé à partir de la poussière. En Islam, la distinction entre les êtres humains est fondée sur la foi (Iman) et la piété (Taqwa), en faisant ce qu'Allah a ordonné et en évitant ce qu'Allah a interdit. Allah dit:

{{ $page->verse('49:13') }}

Le Prophète Muhammad {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:2564b') }}

L'islam considère que tous les individus sont égaux en termes de droits et de devoirs. Les gens sont égaux devant la loi (Shari'ah), comme le dit Allah :

{{ $page->verse('16:97') }}

La foi, la véracité et la piété mènent au Paradis, ce qui est le droit de celui qui acquiert ces attributs, même s'il fait partie des plus faibles ou des plus humbles des gens. Allah dit :

{{ $page->verse('..65:11') }}

Le kufr (mécréance), l'arrogance et l'oppression mènent à l'enfer, même si l'auteur de ces actes est l'une des personnes les plus riches ou les plus nobles. Allah dit :

{{ $page->verse('64:10') }}

Les compagnons du prophète Muhammad {{ $page->pbuh() }} comprenaient des hommes musulmans de toutes tribus, races et couleurs. Leurs cœurs étaient remplis de tawhid (monothéisme) et ils étaient unis par leur foi et leur piété - comme Abu Bakr deu peuple Quraysh, Ali ibn Abi Taalib de la tribus Bani Haashim, Bilal l'Éthiopien, Suhayb le Romain, Salman le Persan, des hommes riches comme 'Uthman et des hommes pauvres comme 'Ammar, des gens aisés et des gens pauvres comme Ahl al-Suffah, et bien d'autres encore.

Ils crurent en Allah et luttèrent pour Lui jusqu'à ce qu'Allah et Son Messager les aient agréés. Ceux-là étaient les vrais croyants.

{{ $page->verse('98:8') }}
