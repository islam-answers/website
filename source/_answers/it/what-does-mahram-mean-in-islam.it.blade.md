---
extends: _layouts.answer
section: content
title: Qual è il significato di Mahram nell'Islam?
date: 2024-12-10
description: Il Mahram è una persona con la quale è sempre proibito sposarsi, come
  ad esempio i familiari consanguinei, i genitori adottivi o i parenti acquisiti.
sources:
- href: https://islamqa.org/hanafi/daruliftaa/8495/
  title: islamqa.org (Daruliftaa.com)
- href: https://islamqa.info/en/answers/5538/
  title: islamqa.info
- href: https://islamqa.info/en/answers/130002/
  title: islamqa.info
translator_id: 573107
---

Come principio generale, un Mahram è una persona con cui il matrimonio è sempre illecito. Questo è il motivo per cui "Mahram" viene tradotto con "parente non sposabile". Alla donna è consentito togliere l'hijab davanti ai suoi Mahram. È inoltre consentito stringere la mano o baciare un Mahram sulla testa, sul naso o sulla guancia.

Questo divieto permanente di matrimonio viene stabilito in tre modi: per parentela (legami di sangue), per relazione adottiva (allattamento) e per relazione matrimoniale.

Quindi, l'illiceità permanente del matrimonio è stabilita con i tre tipi di relazione sopra menzionati, e un Mahram è colui col quale il matrimonio è sempre illecito. In altre parole, si diventa Mahram a causa di questi tre tipi di relazione.

## Rapporto di parentela/discendenza

È sempre illecito per un uomo sposare le seguenti persone (sarà quindi considerato un Mahram per loro):

- Madre, nonna e così via;
- Nonna paterna e così via;
- Figlie, nipoti e così via;
- Tutti i tipi di sorelle (con un solo genitore o entrambi i genitori in comune);
- Zie materne e paterne;
- Nipoti (figlie del fratello o della sorella).

Allah dice nel Corano:

{{ $page->verse('4:23') }}

Allo stesso modo, i Mahram di una donna in base alle relazioni familiari sono:

- Padre, nonno e così via;
- Nonno materno e così via;
- Figli, nipoti e così via;
- Tutti i tipi di fratelli (con un solo genitore o entrambi i genitori in comune);
- Zii materni e paterni,;
- Nipoti (figli del fratello o della sorella).

Allah dice nel Corano:

{{ $page->verse('..24:31..') }}

## Rapporto di adozione

Chiunque sia un Mahram in virtù della relazione di parentela, sarà considerato un Mahram anche in virtù dell'adozione. Pertanto, un padre adottivo (marito della madre adottiva), un fratello adottivo, uno zio adottivo, un nipote adottivo, ecc., saranno tutti considerati Mahram di una donna, e un uomo sarà un Mahram per la madre adottiva, la sorella adottiva, la nipote adottiva, ecc.

Tuttavia, bisogna ricordare che questo accade solo quando l'allattamento avviene nel periodo previsto, che è di due anni. Bisogna fare attenzione nel determinare chi è un Mahram attraverso le relazioni di adozione, perché determinare ciò, a volte, può essere complesso e complicato. Occorre rivolgersi a un sapiente prima di arrivare ad una decisione definitiva.

## Relazione di matrimonio

Il terzo tipo di relazione con cui il matrimonio diventa permanentemente illecito, e di conseguenza si instaura il rapporto di Mahram, è quella del matrimonio.

Esistono quattro tipi di persone con le quali il matrimonio diventa sempre illecito a causa della relazione matrimoniale:

- La madre della propria moglie (suocera), la nonna, e così via: il matrimonio con lei diventa illecito semplicemente contraendo matrimonio con la figlia, indipendentemente dal fatto che il matrimonio sia stato consumato o meno.
- La figlia della moglie (da un precedente matrimonio), la nipote e così via: il matrimonio con lei diventa illecito (in modo permanente) se il matrimonio con la moglie è stato consumato.
- La moglie di un figlio, di un nipote e così via: indipendentemente dal fatto che il figlio abbia consumato o meno il matrimonio.
- La matrigna, la nonna acquisita e così via: ovvero le donne che sono state sposate col padre o con il nonno paterno o materno.

Riassumendo, un Mahram è la persona con la quale il matrimonio è sempre illecito, e questa illiceità/proibizione permanente del matrimonio si stabilisce in tre modi: col rapporto di parentela, col rapporto di adozione e col rapporto di matrimonio.
