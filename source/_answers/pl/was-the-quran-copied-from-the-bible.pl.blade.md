---
extends: _layouts.answer
section: content
title: Czy Koran został skopiowany z Biblii?
date: 2024-10-28
description: Koran nie został skopiowany z Biblii. Prorok Mahomet był analfabetą, nie było arabskiej Biblii, a objawienia pochodziły bezpośrednio od Allaha.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 584496
---

Allah stworzył wszystko wokół nas z celem. Człowiek również został stworzony z celem, którym jest oddawanie czci Allahowi. Allah mówi w Koranie:

{{ $page->verse('51:56') }}

W celu prowadzenia ludzkości, Allah od czasu do czasu wysyłał proroków. Prorocy ci otrzymali objawienie od Boga, a każda religia posiada własny zbiór boskich pism świętych, które stanowią podstawę ich wierzeń. Pisma te są materialną transkrypcją boskiego objawienia, bezpośrednio, jak w przypadku proroka Mojżesza (Musa), który otrzymał przykazania od Allaha, lub pośrednio, jak w przypadku Jezusa (Isa) i Muhammada {{ $page->pbuh() }}, którzy przekazali ludziom objawienie przekazane im przez anioła Gabriela (Jibreel).

Koran nakazuje wszystkim muzułmanom wierzyć w pisma, które go poprzedzają. Jednak jako muzułmanie wierzymy w Torę (Tawrah), Księgę Psalmów (Zaboor), Ewangelię (Injeel) w ich oryginalnej formie, tj. w sposób, w jaki zostały objawione, jako słowo Allaha.

## Czy prorok Muhammad mógł skopiować Koran z Biblii?

Dyskusja na temat tego, w jaki sposób Prorok Muhammad {{ $page->pbuh() }} mógł poznać historie Ludu Księgi, a następnie przekazać je w Koranie — co oznacza, że nie było to objawienie — jest dyskusją na temat różnych twierdzeń, w których Prorok Muhammad {{ $page->pbuh() }} mógł poznać historie bezpośrednio z ich świętych ksiąg lub dowiedzieć się, co pisma święte zawierały w opowieściach, nakazach i zakazach, ustnie od Ludu Księgi, jeśli nie był w stanie sam przestudiować ich ksiąg.

Twierdzenia te można podsumować w trzech podstawowych stwierdzeniach:

1. Twierdzenie, że Prorok Mahomet {{ $page->pbuh() }} nie był niepiśmienny ani analfabetą.
1. Twierdzenie, że Pisma Chrześcijańskie były dostępne Prorokowi {{ $page->pbuh() }} i mógł je cytować lub przepisywać.
1. Twierdzenie, że Mekka była głównym ośrodkiem edukacyjnym studiów nad pismami świętymi.

Po pierwsze, tekst Koranu i Sunnah w wielu miejscach wyraźnie wskazują, nie pozostawiając miejsca na wątpliwości, że Prorok Muhammad {{ $page->pbuh() }} był rzeczywiście niepiśmienny i że nigdy nie opowiedział żadnej z historii Ludzi Księgi przed otrzymaniem objawienia, i nigdy nie napisał niczego własną ręką. Nie wiedział nic o historiach przed rozpoczęciem swojej misji i nigdy o nich nie mówił.

Allah, niech będzie wywyższony, mówi:

{{ $page->verse('29:48') }}

Księgi biografii i historii Proroka nie wspominają o tym, że Prorok spisał objawienie lub że sam pisał listy do królów. Wręcz przeciwnie, Prorok Muhammad {{ $page->pbuh() }} miał skrybów, którzy spisywali objawienie i inne rzeczy. W genialnym tekście Ibn Chalduna znajduje się opis poziomu edukacji na Półwyspie Arabskim tuż przed rozpoczęciem misji Proroka. Mówi on:

> Wśród Arabów umiejętność pisania była rzadsza niż jaja wielbłądzic. Większość ludzi była analfabetami, zwłaszcza mieszkańcy pustyni, ponieważ jest to umiejętność, którą zazwyczaj posiadają mieszkańcy miast.

Dlatego Arabowie nie nazywali analfabetów analfabetami; raczej tych, którzy potrafili czytać i pisać, opisywali jako osoby wykształcone, ponieważ umiejętność czytania i pisania była wśród ludzi wyjątkiem, a nie normą.

Po drugie, nie ma dowodów, wskazówek ani sugestii, że w tamtym czasie istniało jakiekolwiek arabskie tłumaczenie Biblii. Al-Bukhari przekazał od Abu Hurajry (niech Allah będzie z niego zadowolony), że powiedział:

{{ $page->hadith('bukhari:7362') }}

Ten hadis wskazuje, że Żydzi mieli pełny monopol na tekst i jego wyjaśnienie. Gdyby ich teksty były znane w języku arabskim, nie byłoby potrzeby, aby Żydzi czytali tekst lub wyjaśniali go [muzułmanom].

Idea ta jest poparta wersetem, w którym Allah, niech będzie wywyższony, mówi:

{{ $page->verse('3:78') }}

Być może najważniejszą książką napisaną na ten temat jest książka Bruce'a Metzgera, profesora języka i literatury Nowego Testamentu, Biblia w tłumaczeniu, w której powiedział:

> Najbardziej prawdopodobne jest, że najstarsze arabskie tłumaczenie Biblii pochodzi z VIII wieku.

Orientalista Thomas Patrick Hughes napisał:

> Nie ma żadnego dowodu na to, że Mahomet czytał Pisma Chrześcijańskie... Należy zauważyć, że nie ma żadnego wyraźnego dowodu sugerującego, że jakiekolwiek arabskie tłumaczenie Starego i Nowego Testamentu istniało przed czasami Muhammada.

Po trzecie, idea, że Prorok otrzymał objawienie od ludzi, jest starą ideą. Politeiści oskarżyli go o to, a Koran ich obalił, jak mówi Allah, niech będzie wywyższony:

{{ $page->verse('25:5-6') }}

Co więcej, twierdzenie, że Prorok Mahomet {{ $page->pbuh() }} zdobył wiedzę od Ludzi Księgi podczas pobytu w Mekce, jest twierdzeniem obalonym w świetle akademickiego i kulturalnego statusu Mekki oraz prawdziwej natury związku, jaki to arabskie społeczeństwo miało z wiedzą Ludzi Księgi.

Dlatego też, po udowodnieniu, że Prorok Muhammad {{ $page->pbuh() }} nie uzyskał wiedzy Ludzi Księgi bezpośrednio od nich lub za pośrednictwem pośrednika, nie pozostaje nam nic innego, jak stwierdzić, że było to objawienie od Allaha dla Jego wybranego Proroka.
