---
extends: _layouts.answer
section: content
title: Perché il profeta Mohammad sposò una ragazza giovane?
date: 2024-12-16
description: Il matrimonio del Profeta Mohammad con Aisha seguiva le norme del VII
  secolo, senza obiezioni all'epoca. Le critiche odierne applicano standard moderni.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 573107
---

Aisha (che Allah sia soddisfatto di lei) aveva 6 anni quando fu data in sposa al Profeta Mohammad {{ $page->pbuh() }} e ne aveva 9 quando il matrimonio fu consumato.

Purtroppo, oggi vediamo che gli sforzi di altre religioni si focalizzano principalmente sulla distorsione e sulla manipolazione dei fatti storici ed etimologici. Uno di questi argomenti è stata l'accusa del matrimonio della giovane Aisha con il Profeta Mohammad {{ $page->pbuh() }}. Alcuni predicatori cercano di accusare il Profeta di essere un molestatore di bambini, anche se in termini politicamente corretti, a causa del fatto che Aisha fu promessa in sposa all'età di 6 anni e il matrimonio fu consumato 3 anni dopo - a 9 anni - quando era già in piena pubertà. Il periodo di tempo intercorso tra il fidanzamento e la consumazione del matrimonio, dimostra chiaramente che i suoi genitori stavano aspettando che Aisha raggiungesse la pubertà, prima che il suo matrimonio fosse consumato. Questa risposta cerca di confutare l'ingiusta accusa contro il Profeta Mohammad {{ $page->pbuh() }}.

### Pubertà e matrimoni precoci nella cultura semitica

L'accusa di molestie su minori contraddice il dato fondamentale secondo cui una ragazza diventa donna quando inizia il ciclo mestruale. Il significato delle mestruazioni, che chiunque abbia un minimo di familiarità con la fisiologia vi spiegherà, è che si tratta del segnale che la ragazza si sta preparando a diventare madre.

Le donne raggiungono la pubertà in età differenti, dagli 8 ai 12 anni, a seconda della genetica, della razza e dell'ambiente. Fino ai dieci anni di età circa, le differenze tra ragazzi e ragazze sono minime. L'aumento rapido di crescita della pubertà inizia prima nelle ragazze, ma dura più a lungo nei ragazzi. I primi segni della pubertà si verificano intorno ai 9 o 10 anni nelle ragazze, più spesso verso i 12 anni nei ragazzi. Inoltre, le donne nate in zone più calde raggiungono la pubertà ad un'età più precoce rispetto a quelle nate in ambienti freddi. In questo caso, la temperatura media del paese o dell'area viene considerata il fattore più importante, non solo per quanto riguarda le mestruazioni, ma anche per l'intero sviluppo sessuale durante la pubertà.

Nell'Arabia del VII secolo, il matrimonio durante i primi anni della pubertà era accettato poiché si trattava di una norma sociale presente in tutte le culture semitiche, dagli Israeliti agli Arabi e in tutte le nazioni che si trovavano nel mezzo. Secondo il Talmud, che gli ebrei considerano la loro "Torah orale", in Sanhedrin 76b afferma chiaramente che è preferibile che una donna si sposi quando ha le prime mestruazioni, e in Ketuvot 6a ci sono regole riguardanti i rapporti sessuali con ragazze che non hanno ancora avuto le mestruazioni. Jim West, un ministro battista, commenta la seguente tradizione degli Israeliti:

- La moglie doveva essere scelta all'interno della cerchia familiare più ampia (solitamente all'inizio della pubertà o intorno ai 13 anni) per preservare la purezza della linea familiare.
- Nel corso della storia, la pubertà è sempre stata un simbolo dell'età adulta.
- La pubertà è definita come l'età o il periodo in cui una persona è in grado, per la prima volta, di riprodursi sessualmente; in altre epoche storiche, un rito o una celebrazione di questo evento fondamentale faceva parte della cultura.

Gli illustri sessuologi R.E.L. Masters e Allan Edwards, nel loro studio sull'espressione sessuale afro-asiatica, affermano quanto segue:

> Oggigiorno, in molte parti del Nord Africa e del Medio Oriente, le bambine si sposano e consumano il matrimonio tra i cinque e i nove anni; e nessuna donna che si rispetti resta nubile oltre l'età della pubertà.

### Perché Mohammad ha sposato Aisha

Il Profeta {{ $page->pbuh() }}, addolorato per la morte della sua prima moglie Khadijah che lo aveva sempre sostenuto e gli era stata accanto per molto tempo, aveva definito l'anno in cui morì "Anno del Dolore". Quindi sposò Sawdah, una donna anziana e non molto bella; la sposò principalmente per consolarla dopo la morte del marito. Quattro anni dopo, il Profeta {{ $page->pbuh() }} sposò Aisha. I motivi del matrimonio furono i seguenti:

1. Fece un sogno in cui la sposava. Dalla narrazione di Aisha, risulta che il Profeta {{ $page->pbuh() }} le disse:

{{ $page->hadith('bukhari:3895') }}

2. Le caratteristiche di intelligenza e arguzia che il Profeta {{ $page->pbuh() }} aveva notato in lei fin da bambina. Per questo motivo volle sposarla in modo che fosse in grado, più di altri, di trasmettere i resoconti di ciò che egli faceva e diceva. Infatti, come già detto, Aisha era un punto di riferimento per i compagni del Profeta {{ $page->pbuh() }} per quanto riguarda i loro affari e le loro decisioni.

3. L'amore che il Profeta {{ $page->pbuh() }} provava per il padre di Aisha, Abu Bakr, e la persecuzione che questi aveva subito per amore dell'invito all'Islam, che sopportò con pazienza. Era il più forte e il più sincero degli uomini di fede dopo i Profeti.

### Vi furono obiezioni al loro matrimonio?

La risposta a questa domanda è no. Non esiste nessun documento, tra le fonti islamiche, laiche o di qualsiasi altra fonte storica, che mostri anche solo implicitamente qualcosa di diverso dalla gioia assoluta di tutte le parti coinvolte in questo matrimonio. Nabia Abbott descrive il matrimonio di Aisha con il Profeta {{ $page->pbuh() }} nel modo seguente:

> In nessuna versione compare qualche commento sulla differenza di età tra Mohammad e Aisha o sulla tenera età della sposa che, al massimo, non poteva avere più di dieci anni e che era ancora molto interessata ai suoi giochi.

Anche il noto critico orientalista W. Montgomery Watt ha affermato quanto segue a proposito della moralità del Profeta {{ $page->pbuh() }}:

> Dal punto di vista dell'epoca di Mohammad, quindi, le accuse di tradimento e bramosia sessuale non possono essere sostenute. I suoi contemporanei non lo ritenevano in alcun modo moralmente corrotto o carente. Al contrario, alcune delle azioni criticate dall'occidentale moderno dimostrano che gli standard di Mohammad erano più elevati di quelli del suo tempo.

Oltre al fatto che nessuno si lamentava di lui o delle sue azioni, egli era anche un esempio assoluto di moralità, nella sua società e nel suo tempo. Pertanto, giudicare la sua moralità in base agli standard della nostra società e cultura di oggi non solo è assurdo, ma anche ingiusto.

### Il matrimonio durante la pubertà ai giorni nostri

I contemporanei del Profeta {{ $page->pbuh() }} (sia nemici che amici) accettarono tranquillamente il suo matrimonio con Aisha, senza alcun tipo di problema. In effetti, è soltanto nelle epoche più recenti che questa unione ha ricevuto pesanti critiche.

Ciò è dovuto principalmente al fatto che oggigiorno è cambiata la cultura, anche per quel che riguarda l'età in cui le persone solitamente si sposano. Tuttavia, fino ai giorni nostri, nel XXI secolo, in molti paesi l'età del consenso (sessualmente parlando) è ancora piuttosto bassa. Ad esempio, in Germania, Italia e Austria le persone possono legalmente fare sesso a 14 anni, nelle Filippine e in Angola a 12 anni. La California, nel 1889, fu il primo stato ad innalzare l'età del consenso a 14 anni. In seguito, altri stati si sono adeguati alla California innalzando anch'essi l'età del consenso.

### L'Islam e l'età della pubertà

L'Islam insegna, senza alcun dubbio, che l'età adulta inizia quando una persona ha raggiunto la pubertà. Allah dice nel Corano:

{{ $page->verse('24:59..') }}

Il raggiungimento della pubertà per le donne avviene con l'inizio delle mestruazioni, come Allah dice nel Corano:

{{ $page->verse('65:4..') }}

Nell'Islam, pertanto, è fondamentale riconoscere che con l'arrivo della pubertà inizia l'età adulta. È il momento in cui la persona è già matura ed è pronta per le responsabilità di un adulto. Su quali basi si potrebbe quindi criticare il matrimonio di Aisha, dal momento che esso è stato consumato dopo il raggiungimento della pubertà?

Se volessimo accusare il Profeta {{ $page->pbuh() }} di “molestie su minori”, allora dovremmo accusare anche tutti i popoli semiti che consideravano il matrimonio durante la pubertà come un evento normale e naturale.

Inoltre, sapendo che il Profeta {{ $page->pbuh() }} non sposò nessun'altra vergine oltre ad Aisha e che tutte le altre sue mogli erano già state sposate (molte di loro erano anziane), ciò confuta l'idea, diffusa da molte fonti ostili, secondo cui i motivi principali dei matrimoni del Profeta fossero il desiderio fisico e il godimento delle donne, perché se queste fossero state le sue intenzioni avrebbe scelto solo ragazze giovani.

### Conclusioni

Abbiamo quindi visto che:

- Nella società semitica dell'Arabia del VII secolo era consuetudine consentire i matrimoni in età puberale.
- Non ci sono notizie di opposizioni al matrimonio del Profeta con Aisha, né da parte dei suoi amici né dei suoi nemici.
- Ancora oggi, esistono culture che consentono il matrimonio in età puberale alle giovani donne.

Nonostante ci si trovi di fronte a queste verità, alcune persone continuano ad accusare il Profeta {{ $page->pbuh() }} di immoralità. Eppure, fu lui a portare giustizia alle donne d'Arabia, elevandole a un livello mai visto prima nella loro società, un livello che le civiltà antiche non avevano mai concesso alle loro donne.

Ad esempio, quando divenne Profeta per la prima volta, i pagani dell'Arabia avevano ereditato il disprezzo per le donne, tramandato dai loro vicini ebrei e cristiani. Per loro era talmente disdicevole essere benedetti con una figlia femmina, che arrivavano al punto di seppellirla viva per evitare il disonore associato alle bambine. Allah dice nel Corano:

{{ $page->verse('16:58-59') }}

Non solo Mohammad {{ $page->pbuh() }} scoraggiò e condannò severamente questo atto, ma insegnò anche alle persone a rispettare e ad amare le proprie figlie e madri come compagne e fonti di salvezza per gli uomini della loro famiglia. Disse:

{{ $page->hadith('ibnmajah:3669') }}

In un hadith di Anas Ibn Malik è stato anche narrato che:

{{ $page->hadith('muslim:2631') }}

In altre parole, se si ama il Messaggero di Allah {{ $page->pbuh() }} e si desidera essere con lui nel Giorno della Resurrezione in Paradiso, è necessario trattare bene le proprie figlie. Questo non è certamente l'atto di un "molestatore di bambini", come alcuni vorrebbero farci credere.
