---
extends: _layouts.answer
section: content
title: What is the Quran about?
date: 2024-02-11
description: The primary source of every Muslim’s faith and practice.
sources:
- href: https://www.islam-guide.com/ch3-7.htm
  title: islam-guide.com
---

The Quran, the last revealed word of God, is the primary source of every Muslim’s faith and practice. It deals with all the subjects which concern human beings: wisdom, doctrine, worship, transactions, law, etc., but its basic theme is the relationship between God and His creatures. At the same time, it provides guidelines and detailed teachings for a just society, proper human conduct, and an equitable economic system.

Note that the Quran was revealed to Prophet Muhammad {{ $page->pbuh() }} in Arabic only. So, any Quranic translation, either in English or any other language, is neither a Quran, nor a version of the Quran, but rather it is only a translation of the meaning of the Quran. The Quran exists only in the Arabic in which it was revealed.
