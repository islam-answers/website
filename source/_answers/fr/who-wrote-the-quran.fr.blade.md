---
extends: _layouts.answer
section: content
title: Qui a écrit le Coran ?
date: 2024-06-08
description: Le Coran est la parole de Dieu, qu'Il a révélée à Son prophète Mohammed
  par l'intermédiaire de l'ange Gabriel.
sources:
- href: https://www.islam-guide.com/fr/ch1-1.htm
  title: islam-guide.com
---

Dieu a envoyé Son dernier prophète (Mohammed {{ $page->pbuh() }}) avec plusieurs miracles et signes prouvant qu'il est un véritable prophète envoyé par Dieu. De la même façon, Dieu a fortifié Sa dernière révélation, le Coran, avec de nombreux miracles qui prouvent que ce Coran est la parole de Dieu, révélée par Lui, et qu'il n'a pas été rédigé par des hommes.

Le Coran est la parole de Dieu, qu'Il a révélée à Son prophète Mohammed {{ $page->pbuh() }} par l'intermédiaire de l'ange Gabriel. Il a été mémorisé par Mohammed {{ $page->pbuh() }}, qui l'a ensuite dicté à ses compagnons. Ces derniers l'ont à leur tour mémorisé, mis par écrit et révisé en compagnie du prophète Mohammed {{ $page->pbuh() }}. De plus, une fois par année, le prophète Mohammed {{ $page->pbuh() }} révisait tout le Coran avec l'ange Gabriel; la dernière année de sa vie, il l'a révisé deux fois. Depuis le moment où le Coran a été révélé jusqu'à nos jours, il y a toujours eu un très grand nombre de musulmans l'ayant mémorisé en entier, lettre par lettre. Certains d'entre eux ont même réussi à mémoriser tout le Coran avant l'âge de dix ans. Pas une seule lettre du Coran n'a été modifiée à travers les siècles.

Le Coran, qui a été révélé il y a plus de quatorze siècles, fait mention de faits qui n'ont été que récemment découverts ou prouvés par les scientifiques. Cela démontre, sans l'ombre d'un doute, que le Coran est la parole de Dieu, révélée par Lui au prophète Mohammed {{ $page->pbuh() }}, et qu'il n'a pas été écrit par Mohammed {{ $page->pbuh() }}, ni par aucun autre être humain. Cela prouve également que Mohammed {{ $page->pbuh() }} est un véritable prophète envoyé par Dieu. Il serait déraisonnable de croire qu'une personne aurait pu avoir connaissance, il y a quatorze siècles, de faits qui n'ont été découverts ou prouvés que récemment à l'aide d'équipements de pointe et de méthodes scientifiques hautement perfectionnées.
