---
extends: _layouts.answer
section: content
title: O que o Islã diz sobre a evolução?
date: 2024-09-16
description: Deus criou o primeiro ser humano, Adão na sua forma final. Quanto a criaturas
  vivas, além dos seres humanos, as fontes islâmicas não falam sobre o assunto.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 594172
---

Deus criou o primeiro ser humano, Adão, na sua forma final - ao contrário de evoluir gradualmente a partir de macacos avançados. Este facto não pode ser diretamente confirmado ou negado pela ciência, porque se tratou de um acontecimento histórico único e singular - um milagre.

Quanto às outras criaturas vivas, além dos seres humanos, as fontes islâmicas não falam sobre o assunto e apenas nos levam a acreditar que Deus as criou da forma que quis, através da evolução ou de outra forma.
