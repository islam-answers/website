---
extends: _layouts.answer
section: content
title: Did Jesus claim to be God?
date: 2024-10-28
description: Muslims believe Jesus was a prophet who called people to worship Allah
  alone, never claiming to be God.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
---

Muslims believe that Jesus {{ $page->pbuh() }} was one of the slaves of Allah, and one of His noble Messengers. Allah sent Jesus to the Children of Israel to call them to believe in Allah alone and worship Him alone.

Allah supported Jesus {{ $page->pbuh() }} with miracles that proved he was speaking the truth; Jesus was born to the Virgin Maryam (Mary) with no father. He permitted to the Jews some of the things that had been forbidden to them, he did not die and his enemies the Jews did not kill him, rather Allah saved him from them and raised him up to heaven alive. Jesus told his followers of the coming of our Prophet Muhammad {{ $page->pbuh() }}. Jesus will come back down at the end of time. He will disavow himself on the Day of Resurrection of the claims that he was a God.

On the Day of Resurrection, Jesus will stand before the Lord of the Worlds, Who will ask him before the witnesses what he said to the Children of Israel, as Allah says:

{{ $page->verse('5:116-118') }}

An honest examination shows that Jesus {{ $page->pbuh() }} never explicitly and unequivocally said he was God. It was the authors of the New Testament, none of whom met Jesus, that claimed he was God and propagated this. There is no evidence that the actual apostles of Jesus claimed this, or that they authored the Bible. Rather, Jesus is shown to be constantly worshiping, and calling others to worship, God alone.

The claim that Jesus {{ $page->pbuh() }} is God cannot be ascribed to Jesus, if it so starkly contradicts the First Commandment not to take any gods besides God, nor to make an idol of anything in heaven and earth. Also, it’s logically absurd for God to become His creation.

Thus, only Islam’s claim is plausible, and consistent with early revelation: Jesus {{ $page->pbuh() }} was a prophet, not God.
