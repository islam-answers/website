---
extends: _layouts.answer
section: content
title: Péchés pardonnés après sa conversion à l’Islam ?
date: 2025-01-26
description: Lorsqu'un mécréant devient musulman, Allah lui pardonne tout ce qu'il
  a fait lorsqu'il était non-musulman et le purifie de ses péchés.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
translator_id: 586055
---

Par Sa grâce et Sa miséricorde, Allah a fait de l’adhésion à l’Islam une cause d’effacement des péchés commis avant l’Islam. Lorsqu’un non croyants devient musulman, Allah lui pardonne tout ce qu’il a fait lorsqu’il était non-musulman, et il est purifié de ses péchés.

'Amr ibn al-'As (qu'Allah l'agrée) a dit :

{{ $page->hadith('muslim:121') }}

L'imam Nawawi a dit : "L'islam détruit ce qui l'a précédé", ce qui signifie qu'il l'efface et le fait disparaître.

On a posé une question similaire à Shaykh Ibn `Uthaymin (qu'Allah lui fasse miséricorde) au sujet d'une personne qui gagnait de l'argent en vendant de la drogue avant de devenir musulman. Il a répondu : Nous disons à ce frère qu'Allah a béni par l'Islam après qu'il ait gagné des biens illicites : réjouis-toi, car ces biens lui sont permis et il n'y a pas de péché pour lui, qu'il les garde, qu'il les donne en charité ou qu'il les utilise pour se marier, parce qu'Allah dit dans Son Livre sacré : "Il n'y a pas de péché :

{{ $page->verse('8:38') }}

Cela signifie que tout ce qui est passé, de manière générale, est pardonné. Mais l'argent qui a été pris par la force à son propriétaire doit lui être rendu. Mais l'argent gagné par des accords entre personnes, même s'il est illicite, comme celui gagné par le Riba (intérêt), ou en vendant de la drogue, etc. lui est permis lorsqu'il devient musulman.

De nombreux non-musulmans de l'époque du prophète Muhammad {{ $page->pbuh() }} sont devenus musulmans après avoir tué des musulmans, mais ils n'ont pas été punis pour ce qu'ils avaient fait.
