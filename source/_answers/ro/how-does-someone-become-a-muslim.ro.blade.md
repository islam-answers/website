---
extends: _layouts.answer
section: content
title: Cum devine cineva musulman?
date: 2024-03-12
description: Ca o persoană să se convertească la Islam și să devină musulman, ea trebuie
  să spună cu convingere „La ilaha illa Allah, Muhammadur rasoolu Allah”.
sources:
- href: https://www.islam-guide.com/ch3-6.htm
  title: islam-guide.com
translator_id: 554943
---

Ca o persoană să se convertească la Islam și să devină musulman, ea trebuie să spună cu convingere: „La ilaha illa Allah, Muhammadur rasoolu Allah”. Această zicală înseamnă „Nu există nicio zeitate în afară de Dumnezeu (Allah), iar Muhammad este Mesagerul (Profetul) lui Dumnezeu”. Prima parte, „Nu există nicio zeitate în afară de Dumnezeu”, înseamnă că nimeni altcineva nu are dreptul de a fi venerat și că Dumnezeu nu are nici partener, nici fiu. Pentru a fi musulman, trebuie să:

- Crezi că Sfântul Coran este cuvântul literal al lui Dumnezeu, revelat de El.
- Crezi că Ziua Judecății (Ziua Învierii) este adevărată și va veni, așa cum a promis Dumnezeu în Coran.
- Accepți Islamul ca religie.
- Nu te închini la nimic și nimănui în afară de Dumnezeu.

Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:2747') }}
