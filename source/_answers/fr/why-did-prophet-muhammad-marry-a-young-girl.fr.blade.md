---
extends: _layouts.answer
section: content
title: Pourquoi le prophète Mahomet a-t-il épousé une jeune fille ?
date: 2025-02-03
description: Le mariage du prophète Mahomet avec Aïcha était conforme aux normes du
  VIIe siècle, sans objection à l'époque. La critique moderne ignore ce contexte historique
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 573111
---

Aïcha (qu'Allah soit satisfait d'elle) avait 6 ans lorsqu'elle s'est fiancée au prophète Muhammad ({{ $page->pbuh() }}) et 9 ans lorsque leur mariage a été consommé.

Malheureusement, nous constatons aujourd’hui que les efforts de certaines autres religions visent principalement à déformer et à manipuler des faits historiques et étymologiques. L'un des sujets fréquemment abordés est l'allégation du mariage précoce d'Aïcha avec le prophète Muhammad {{ $page->pbuh() }}. Certains missionnaires tentent de l’accuser, de manière implicite, de pédophilie en affirmant qu'Aïcha s'est fiancée à l'âge de 6 ans et que le mariage a été consommé trois ans plus tard, à 9 ans, alors qu'elle était en pleine puberté. Toutefois, le laps de temps entre les fiançailles et la consommation du mariage démontre clairement que ses parents ont attendu qu'elle atteigne la puberté avant de passer à cette étape. Cet article vise à réfuter ces accusations infondées portées à l'encontre du prophète Muhammad {{ $page->pbuh() }}.

### La puberté et le mariage précoce dans la culture sémitique

L'allégation de maltraitance envers les enfants contredit un fait fondamental : une fille devient une femme lorsqu'elle commence son cycle menstruel. Toute personne un tant soit peu familiarisée avec la physiologie vous dira que la menstruation est le signe que la jeune fille est biologiquement prête à devenir mère.

Les femmes atteignent la puberté à des âges variés, allant de 8 à 12 ans, selon la génétique, la race et l'environnement. Jusqu'à l'âge de dix ans, la différence de taille entre les garçons et les filles est minime. Cependant, la poussée de croissance liée à la puberté commence plus tôt chez les filles, mais dure plus longtemps chez les garçons. Les premiers signes de la puberté apparaissent vers 9 ou 10 ans chez les filles, mais plutôt vers 12 ans chez les garçons. De plus, les filles vivant dans des environnements plus chauds atteignent la puberté beaucoup plus tôt que celles vivant dans des environnements plus froids. La température moyenne d'un pays ou d'une région est considérée comme le facteur clé, non seulement pour les menstruations, mais aussi pour l'ensemble du développement sexuel pendant la puberté.

Le mariage durant les premières années de la puberté était accepté dans l'Arabie du VIIe siècle, comme il l'était dans toutes les cultures sémitiques, des Israélites aux Arabes, ainsi que dans les nations intermédiaires. Selon le Talmud, que les Juifs considèrent comme leur « Torah orale », Sanhedrin 76b indique clairement qu'il est préférable qu'une femme se marie lorsqu'elle a ses premières règles. De plus, dans Ketuvot 6a, il existe des règles concernant les rapports sexuels avec des filles qui n'ont pas encore eu leurs règles. Jim West, pasteur baptiste, mentionne la tradition suivante chez les Israélites :

- L'épouse devait être choisie au sein du cercle familial élargi (généralement au début de la puberté ou vers l'âge de 13 ans) afin de préserver la pureté de la lignée familiale.
- La puberté a toujours été un symbole de l'âge adulte à travers l'histoire.
- La puberté est définie comme l'âge ou la période à laquelle une personne devient capable de se reproduire sexuellement ; à d'autres époques de l'histoire, un rite ou une célébration marquait cet événement important dans la culture.

Les sexologues renommés R.E.L. Masters et Allan Edwards, dans leur étude sur l'expression sexuelle afro-asiatique, déclarent ce qui suit :

> Aujourd'hui, dans de nombreuses régions d'Afrique du Nord et du Moyen-Orient, les filles sont mariées et ont des relations conjugales entre cinq et neuf ans, et aucune femme qui se respecte ne reste célibataire au-delà de l'âge de la puberté.

### Les raisons du mariage du prophète Muhammad avec Aïcha

Après la mort de sa première femme, Khadeejah, qui l'avait soutenu et était restée à ses côtés, le Prophète {{ $page->pbuh() }} avait été profondément affligé. Il appela l'année de sa mort « l'année du chagrin ». Par la suite, il épousa Sawdah, une femme plus âgée et non particulièrement belle, mais il l'épousa principalement pour la consoler après la perte de son mari. Quatre ans plus tard, le Prophète {{ $page->pbuh() }}, épousa Aïcha. Les raisons de ce mariage étaient les suivantes :

1. Il a rêvé de l'épouser. Il est prouvé, d'après le récit d'Aïcha, que le Prophète, {{ $page->pbuh() }}, lui a dit :

{{ $page->hadith('bukhari:3895') }}

2. Les qualités d'intelligence et de sagacité que le Prophète, {{ $page->pbuh() }}, avait remarquées chez Aïcha dès son plus jeune âge ont fait qu'il a souhaité l'épouser, convaincu qu'elle serait mieux placée que les autres pour transmettre les récits de ses paroles et de ses actes. En effet, comme mentionné plus haut, elle est devenue une référence pour les compagnons du Prophète, {{ $page->pbuh() }}, en ce qui concerne leurs affaires et leurs décisions.

3. L'amour du Prophète, {{ $page->pbuh() }}, pour son père Abu Bakr, ainsi que les persécutions qu'Abu Bakr a endurées pour son soutien à l'islam, qu'il a supportées avec une grande patience. Il était le plus fort et le plus sincère des croyants après les prophètes.

### Y a-t-il eu des objections à leur mariage ?

La réponse est non. Il n'existe absolument aucun document, qu'il soit musulman, séculier ou d'une autre source historique, qui suggère, même implicitement, quoi que ce soit d'autre que la joie totale de toutes les parties concernées par ce mariage. Nabia Abbott décrit le mariage d'Aïcha avec le Prophète, {{ $page->pbuh() }}, de la manière suivante :

> Dans aucune version, il n'est fait mention de la différence d'âge entre Muhammad et Aïcha, ni de l'âge jeune de la mariée, qui, tout au plus, n'avait pas plus de dix ans et était encore très attachée à ses jeux d'enfant.

Même le célèbre orientaliste critique W. Montgomery Watt a déclaré ce qui suit au sujet du caractère moral du Prophète, {{ $page->pbuh() }} :

> Du point de vue de l'époque de Muhammad, les accusations de trahison et de sensualité ne peuvent donc pas être soutenues. Ses contemporains ne lui attribuaient aucune déficience morale. Au contraire, certains des actes critiqués par l'Occidental moderne montrent que les normes de Muhammad étaient plus élevées que celles de son époque.

Outre le fait que personne n'était mécontent de lui ou de ses actions, il était un modèle primordial de moralité dans sa société et à son époque. Par conséquent, juger sa moralité selon les normes de notre société et culture actuelles est non seulement absurde, mais aussi injuste.

### Le mariage à la puberté aujourd'hui

Les contemporains du Prophète {{ $page->pbuh() }}, qu'ils soient ennemis ou amis, ont clairement accepté son mariage avec Aïcha sans aucun problème. L'absence de critiques à l'encontre de ce mariage jusqu'à l'époque moderne en témoigne.

La cause réside dans un changement culturel de notre époque concernant l'âge habituel du mariage. Toutefois, même aujourd'hui, au XXIe siècle, l'âge du consentement sexuel reste relativement bas dans de nombreux endroits. En Allemagne, en Italie et en Autriche, il est légal d'avoir des relations sexuelles dès l'âge de 14 ans, tandis qu'aux Philippines et en Angola, cet âge est de 12 ans. La Californie a été le premier État à fixer l'âge du consentement à 14 ans, en 1889. Après la Californie, d'autres États ont suivi en augmentant également l'âge du consentement.

### L'islam et l'âge de la puberté

L'islam enseigne clairement que l'âge adulte commence lorsqu'une personne atteint la puberté. Allah dit dans le Coran :

{{ $page->verse('24:59..') }}

Les femmes atteignent la puberté avec le début des menstruations, comme le dit Allah dans le Coran :

{{ $page->verse('65:4..') }}

L'islam reconnaît donc la puberté comme le début de l'âge adulte. C'est le moment où la personne a déjà mûri et est prête à assumer les responsabilités d'un adulte. Dès lors, sur quelle base peut-on critiquer le mariage d'Aïcha, puisque ce dernier a été consommé après qu'elle ait atteint la puberté ?

Si l'allégation de « pédophilie » devait être avancée contre le Prophète, {{ $page->pbuh() }}, il faudrait également inclure tous les peuples sémites qui ont considéré le mariage à la puberté comme une norme.

De plus, sachant que le Prophète, {{ $page->pbuh() }}, n'a épousé aucune vierge autre qu'Aïcha et que toutes ses autres épouses avaient été mariées auparavant (et beaucoup d'entre elles étaient âgées), cela réfute l'idée, largement répandue par de nombreuses sources hostiles, selon laquelle le motif fondamental des mariages du Prophète serait le désir physique et la jouissance des femmes. En effet, si telle avait été son intention, il n'aurait choisi que de jeunes épouses.

### Conclusions

Nous avons ainsi pu constater que :

- Dans la société sémite de l'Arabie du VIIe siècle, les mariages entre personnes pubères étaient courants.
- Il n'y a eu aucune opposition au mariage du Prophète avec Aïcha, que ce soit de la part de ses amis ou de ses ennemis.
- Aujourd'hui encore, certaines cultures permettent le mariage de jeunes femmes ayant atteint la puberté.

Malgré ces faits bien établis, certaines personnes persistent à accuser le Prophète, {{ $page->pbuh() }}, d'immoralité. Pourtant, c'est lui qui a rendu justice aux femmes d'Arabie, les élevant à un statut qu'elles n'avaient jamais connu dans leur société, un traitement que les civilisations anciennes n'avaient pas accordé à leurs femmes.

Par exemple, lorsqu'il est devenu prophète, les païens d'Arabie avaient hérité d'un mépris pour les femmes, héritage transmis par leurs voisins juifs et chrétiens. Ils considéraient comme un déshonneur d'être bénis d'un enfant de sexe féminin, au point de tuer ces bébés en les enterrant vivants pour éviter le déshonneur associé aux filles. Allah dit dans le Coran :

{{ $page->verse('16:58-59') }}

Non seulement Muhammad, {{ $page->pbuh() }}, a sévèrement découragé et condamné cet acte, mais il a également enseigné aux gens à respecter et à chérir leurs filles et leurs mères, en tant que partenaires et sources de salut pour les hommes de leur famille. Il a dit :

{{ $page->hadith('ibnmajah:3669') }}

Il a également été rapporté dans un hadith d'Anas Ibn Malik que :

{{ $page->hadith('muslim:2631') }}

En d'autres termes, si quelqu'un aime le Messager d'Allah, {{ $page->pbuh() }}, et souhaite être en sa compagnie le jour de la résurrection au paradis, il doit traiter ses filles avec bonté. Il est évident que ce n'est pas l'acte d'un « agresseur d'enfants », comme certains voudraient nous le faire croire.
