---
extends: _layouts.answer
section: content
title: Cosa dice l'Islam su come devono essere trattati i genitori?
date: 2024-06-13
description: La gentilezza nei confronti dei genitori è uno degli atti più graditi
  e ricompensati da Dio.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 573107
---

Mostrare gentilezza verso i genitori è uno degli atti più graditi agli occhi di Allah. Il comandamento di comportarsi bene con i propri genitori è un chiaro insegnamento del Corano. Allah dice:

{{ $page->verse('4:36..') }}

La menzione della servitù ai genitori segue immediatamente la servitù a Dio. Questo è ripetuto in tutto il Corano.

{{ $page->verse('17:23') }}

In questo versetto, Dio ci ordina di adorare solo Lui e nessun altro, e subito dopo rivolge l'attenzione al comportamento da tenere con i genitori, soprattutto quando questi diventano anziani e dipendono da altri per il loro ricovero e le loro necessità. Ai musulmani viene insegnato di non essere duri con i genitori e di non rimproverarli, ma di parlare loro con parole gentili e mostrare amore e tenerezza.

Nel Corano, Allah chiede inoltre a una persona di supplicarLo in favore dei suoi genitori:

{{ $page->verse('17:24') }}

Allah sottolinea che obbedire ai genitori significa obbedire a Lui, senza però associarGli alcun partner.

{{ $page->verse('31:14-15..') }}

Questo versetto sottolinea che tutti i genitori meritano un buon trattamento. Tuttavia, alle madri dovrebbe essere mostrata particolare attenzione, a causa delle maggiori difficoltà che devono sopportare.

Prendersi cura dei genitori è considerata una delle azioni migliori:

{{ $page->hadith('muslim:85e') }}

In conclusione, nell'Islam, onorare i propri genitori significa obbedire loro, rispettarli, pregare per loro, abbassare la voce in loro presenza, sorridere loro, non mostrare disappunto nei loro confronti, impegnarsi per servirli, esaudire i loro desideri, consultarli e ascoltare ciò che dicono.
