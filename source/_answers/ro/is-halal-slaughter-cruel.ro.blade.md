---
extends: _layouts.answer
section: content
title: Provoacă sacrificarea Halal suferință animalelor?
date: 2024-12-09
description: Sacrificarea Halal nu este crudă; studiile arată că este mai umană și
  mai igienică decât metodele occidentale, care provoacă dureri și rețin sângele în carne.
sources:
- href: https://islamqa.org/hanafi/qibla-hanafi/35739/
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/136275/
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 554943
---

Metoda islamică de sacrificare a animalelor prin intermediul unei tăieturi ascuțite în partea din față a gâtului a fost adesea atacată de unii activiști pentru drepturile animalelor, ca fiind o formă de cruzime împotriva animalelor - afirmația fiind că este o metodă dureroasă, inumană de a ucide animalele. În Occident, legea impune asomarea (amețirea) animalelor cu o împușcătură în cap înainte de sacrificare. Presupunerea este că animalul trebuie să fie inconștient, de exemplu pentru a-l împiedica să reînvie înainte de a fi ucis şi pentru a nu încetini mișcarea liniei de procesare. De asemenea, metoda este folosită pentru a preveni durerea animalului înainte de a muri.

### Un studiu german de cercetare a durerii

Prin urmare, ar putea fi o surpriză pentru cei care au făcut astfel de presupuneri, să afle rezultatele unui studiu efectuat de profesorul Wilhelm Schulze și colegul său, Dr. Hazim, de la Facultatea de Medicină Veterinară a Universității din Hannover, Germania. Studiul - _'Încercări de obiectivare a durerii și a conștiinței în metodele convenționale C.B.P. (asomarea cu pistolul cu bolț captiv penetrant) și metodele-ritual (halal, cu cuțitul) de sacrificare a oilor și vițeilor'_ - concluzionează că:

> Sacrificarea islamică este cea mai umană metodă de sacrificare și că C.B.P. (asomarea cu pistolul cu bolț captiv penetrant), practicată în Occident, provoacă dureri severe animalului.

În cadrul studiului, mai mulți electrozi au fost implantați chirurgical în diferite puncte ale craniului tuturor animalelor, atingând suprafața creierului. Animalele au fost lăsate să se recupereze timp de câteva săptămâni. Unele animale au fost apoi sacrificate prin efectuarea unei incizii rapide și adânci cu un cuțit ascuțit la gât, tăind venele jugulare și arterele carotide, precum și traheea și esofagul (metoda islamică). În timpul experimentului, un electroencefalograf (EEG) și o electrocardiogramă (ECG) au înregistrat starea creierului și a inimii tuturor animalelor în timpul sacrificării și al asomării.

Rezultatele au fost următoarele:

##### Metoda islamică

1. În primele trei secunde de la momentul sacrificării islamice, înregistrarea EEG (electroencefalograma) nu a prezentat nicio modificare față de graficul dinaintea sacrificării, indicând astfel că animalul nu a simțit nicio durere în timpul sau imediat după incizie.
1. Pentru următoarele 3 secunde, EEG a înregistrat o stare de somn profund - inconștiență. Acest lucru se datorează cantității mari de sânge care țâșnește din corp.
1. După cele 6 secunde menționate mai sus, EEG a înregistrat un nivel zero, ceea ce arată că nu a simțit nicio durere.
1. În timp ce mesajul cerebral a scăzut la nivelul zero, inima continua să bată cu putere, iar corpul se convulsiona viguros (o acțiune reflexă a măduvei spinării), eliminând o cantitate maximă de sânge din corp, rezultând astfel o carne mai curată, igienică pentru consumator.

##### Metoda occidentală prin asomare

1. Animalele erau aparent inconștiente la scurt timp după asomare.
1. EEG a arătat durere severă imediat după asomare.
1. Inimile animalelor asomate prin intermediului pistolului cu bolț captiv penetrant au încetat să bată mai devreme în comparație cu cele ale animalelor sacrificate conform metodei islamice, rezultând în reținerea unei cantități mai mare de sânge în carne. Acest lucru este neigienic pentru consumator.

### Reguli islamice pentru sacrificare

După cum se poate vedea din acest studiu, sacrificarea folosind metoda islamică a animalelor este o binecuvântare atât pentru animale, cât și pentru oameni. Pentru ca sacrificarea să fie legală, trebuie luate mai multe măsuri de către cel care efectuează această acțiune. Aceasta pentru a asigura cel mai mare beneficiu atât pentru animal, cât și pentru consumator. În acest sens, Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:1955') }}

Obiectul folosit pentru sacrificarea animalului trebuie să fie ascuțit și utilizat rapid. Tăierea rapidă a vaselor de la nivelul gâtului deconectează fluxul de sânge către nervii din creier responsabili de durere. Astfel, animalul nu simte durerea. Mișcările animalului după tăiere nu se datorează durerii, ci contracției și relaxării mușchilor lipsiți de sânge. De asemenea, Profetul {{ $page->pbuh() }} i-a învățat pe musulmani să nu ascută lama cuțitului în fața animalului și nici să nu sacrifice un animal în fața altora din aceeași specie.

Tăietura trebuie să cuprindă traheea, esofagul și cele două vene jugulare, fără a tăia măduva spinării. Această metodă are ca rezultat scurgerea rapidă a sângelui din corpul animalului. În cazul în care măduva spinării este tăiată, fibrele nervoase către inimă pot fi deteriorate, ceea ce duce la stop cardiac și, prin urmare, la stagnarea sângelui în vasele de sânge. Sângele trebuie drenat complet înainte de îndepărtarea capului. Astfel, carnea este purificată prin îndepărtarea sângelui care acționează ca un mediu pentru microorganisme; de asemenea, carnea rămâne proaspătă mai mult timp în comparație cu alte metode de sacrificare.

Prin urmare, acuzațiile de cruzime față de animale ar trebui, de fapt, să se concentreze asupra celor care nu folosesc metoda islamică de sacrificare, ci preferă să folosească acele metode care provoacă durere și agonie animalului și care ar putea, de asemenea, să dăuneze celor care consumă carnea.
