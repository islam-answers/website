---
extends: _layouts.answer
section: content
title: Cred musulmanii în Fecioara Maria?
date: 2024-12-06
description: Fecioara Maria este menționată în Coran ca o persoană credincioasă. Musulmanii
  o respectă și cred în puritatea ei.
sources:
- href: https://islamqa.info/en/answers/241999/
  title: islamqa.info
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/32915/
  title: islamqa.org (SeekersGuidance.org)
translator_id: 554943
---

Maryam (Maria), fiica lui 'Imraan, fie ca Allah să fie mulțumit de ea, este una dintre credincioasele devotate și femeile drepte. Ea este menționată în Coran, unde este descrisă ca având caracteristicile oamenilor credincioși. Ea a fost o credincioasă a cărei credință, afirmare a Unicității lui Allah și supunere față de El erau perfecte.

Coranul o descrie ca pe o adevărată sclavă a lui Allah, care era umilă și ascultătoare față de El, Domnul Lumilor. Allah spune:

{{ $page->verse('3:42-43') }}

### Inocența Fecioarei Maria

Musulmanii o respectă pe Fecioara Maria și cred în ceea ce este menționat în Sfântul Coran despre ea, și anume puritatea și nevinovăția ei față de ceea ce au acuzat-o dușmanii lui Allah:

{{ $page->verse('4:156') }}

În Coran, există un întreg capitol numit Maria (Maryam), care prezintă în mod minunat povestea nașterii din fecioară. În viziunea islamică, Maria a fost o fecioară; ea nu a fost niciodată căsătorită înainte sau în timpul nașterii lui Iisus {{ $page->pbuh() }}. Maria a fost șocată când îngerul a informat-o că va naște un fiu, spunând:

{{ $page->verse('19:20') }}

Sursele islamice nu raportează o logodnă pentru Maria, nici nu îl menționează pe Iosif, nici o căsătorie ulterioară, nici frați sau surori pentru Iisus {{ $page->pbuh() }}.

Printre musulmani, ea este femeia musulmană perfectă, una dintre cele mai virtuoase femei dintre femeile Paradisului, așa cum a spus Profetul Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2431') }}

Într-o altă narațiune, Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('bukhari:3432') }}

### Nașterea miraculoasă a lui Iisus - un semn pentru întreaga omenire

Musulmanii cred că Allah a făcut din ea și din fiul ei un semn pentru întreaga omenire și o mărturie a Unicității, Domniei și Puterii lui Allah. Allah spune:

{{ $page->verse('23:50..') }}

El mai spune:

{{ $page->verse('3:45') }}

Allah îi avertizează, de asemenea, pe cei care îi iau pe Iisus și pe mama sa, Maria, drept zeități, asociindu-i cu Allah:

{{ $page->verse('5:75..') }}

Allah, fie El glorificat și înălțat, ne-a spus că, în Ziua Judecǎții, Profetul lui Allah 'Isa ibn Maryam (Iisus, fiul Mariei) {{ $page->pbuh() }} va respinge politeismul celor care îl asociază pe el cu Allah și exagerează cu privire la el și la mama sa. Să reflectăm la acest schimb sublim care va avea loc între Allah și robul și Trimisul Său 'Isa (Iisus) {{ $page->pbuh() }}. Allah, preaslăvit să fie, zice:

{{ $page->verse('5:116-117') }}
