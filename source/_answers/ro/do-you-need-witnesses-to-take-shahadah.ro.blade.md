---
extends: _layouts.answer
section: content
title: Este necesar sǎ ai martori pentru a spune Shahadah?
date: 2024-07-14
description: Nu este esențial să spui shahadah (mărturia de credință islamicǎ) în
  fața martorilor. Islamul este o chestiune care se află între o persoană și Domnul
  său.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 554943
---

Pentru ca o persoană să devină musulmană, nu este esențial ca ea să-și declare Islamul în fața nimănui. Islamul este o chestiune care este între o persoană și Domnul său (fie ca El să fie binecuvântat și înălțat).

Dacă el cere oamenilor să fie martori la momentul convertirii lui la Islam, astfel încât să poată fi documentatǎ printre documentele sale personale, nu este nimic în neregulă cu asta, dar aceasta nu este o condiție ca acceptarea Islamului să fie validǎ.
