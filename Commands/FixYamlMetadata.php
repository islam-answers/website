<?php

namespace App\Commands;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'yaml:fix',
    description: 'Fix yaml metadata',
)]
class FixYamlMetadata extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        collect(glob('source/_answers/*/*.md'))
            ->each(function ($file) use ($output) {
                $content = file_get_contents($file);
                $metadata = yaml_parse($content);

                // here we can also sort, add, update or remove metadata keys

                $date = $metadata['date'] ?? null;
                $metadataText = yaml_emit($metadata, YAML_UTF8_ENCODING, YAML_LN_BREAK);
                $metadataText = preg_replace("/\"$date\"/", $date, $metadataText);
                $metadataText = preg_replace('/\.\.\.$/', '---', $metadataText);

                $content = preg_replace('/^---.*?---\s*/s', '', $content);
                $content = $metadataText . "\n" . $content;
                file_put_contents($file, $content);
            }
        );

        $output->writeln('Done!');

        return Command::SUCCESS;
    }
}
