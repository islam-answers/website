---
extends: _layouts.answer
section: content
title: De ce permite Dumnezeu suferința și răul?
date: 2025-02-24
description: Suferința și greutățile ne pot curăța de păcate, în timp ce confortul
  și ușurința duc adesea la păcat și la neglijarea binecuvântărilor.
sources:
- href: https://islamqa.info/en/answers/2850/
  title: islamqa.info
- href: https://islamqa.info/en/answers/13610/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20785/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1239/
  title: islamqa.info
translator_id: 554943
---

Allah (Dumnezeu) Cel Atotputernic este cel mai milostiv, fără nicio îndoială. Cu toate acestea, acțiunile Sale nu pot fi pe deplin înțelese de mințile noastre necalificate.

Ceea ce simplifică această problemă este faptul că Allah este corect, drept, înțelept și cunoscător. Aceasta înseamnă că orice  Dumnezeu Atotputernic face are un scop legitim, chiar dacă noi nu suntem în măsură să îl înțelegem.

De exemplu, un medic și tată grijuliu și iubitor poate fi forțat să amputeze piciorul unicului său fiu. Nu există nicio îndoială că acest tată își iubește fiul. Totuși, acțiunea sa a fost de dragul acestui fiu iubit, deși poate părea crudă pentru cei care nu înțeleg circumstanțele.

Allah Cel Atotputernic este exemplul cel mai mare și mai înalt și nu este dreptul niciuneia dintre creaturile Sale să pună la îndoială faptele Sale, așa cum se menționează în Coran:

{{ $page->verse('21:23') }}

Musulmanul crede că suferința, foamea, accidentele tragice etc. se datorează păcatelor sale, iar Allah decretează această suferință pentru a acționa ca un mijloc de ștergere a acestor păcate. Allah spune în Coran:

{{ $page->verse('42:30') }}

De asemenea, este evident că omul în vremuri de criză se apropie de Allah și începe să se pocăiască, în timp ce în vremuri de ușurință și confort este departe de a-și aminti de binecuvântările lui Allah și folosește aceste daruri și binecuvântări pentru a comite păcat după păcat.

Allah Cel Atotputernic i-a arătat omului calea binelui și a răului și i-a dat puterea și voința de a alege. Prin urmare, omul este răspunzător de faptele sale și de pedeapsa pe care o primește pentru acestea, deoarece viața în această lume este doar un test, însă rezultatele vor fi cunoscute în Viața de Apoi.

### De ce suferă copiii?

Nu orice boală sau handicap este neapărat o pedeapsă; mai degrabă poate fi un test pentru părinții copilului, prin care Allah le va ispăși faptele lor rele sau le va ridica statutul în Paradis dacă suportă această încercare cu răbdare. Apoi, dacă copilul crește, testul îl va include și pe el, iar dacă îl suportă cu răbdare și credință, atunci Allah a pregătit pentru cel răbdător o răsplată care nu poate fi enumerată. Allah spune:

{{ $page->verse('..39:10') }}

Fără îndoială, în faptul că Allah permite copiilor să sufere există o mare înțelepciune care poate fi ascunsă de unii oameni, astfel încât aceștia se opun decretului divin, iar Shaytaan (Satana) profită de această problemă pentru a-i îndepărta de adevăr și de călăuzirea corectă.

Printre motivele pentru care Allah îi face pe copii să sufere sunt următoarele:

1. Este un mijloc de a semnala că copilul este bolnav sau suferă; dacă nu ar fi această suferință, nu s-ar ști de ce boală suferă.
1. Plânsul cauzat de durere aduce mari beneficii organismului copilului.
1. Lecții de învățat: este posibil ca familia acestui copil să comită acțiuni haraam (nepermise) sau să neglijeze îndatoririle obligatorii, dar atunci când văd suferința copilului lor, acest lucru îi determină să renunțe la acele acțiuni, cum ar fi consumul de riba (dobândă), comiterea de zina (adulter), fumatul sau neefectuarea rugăciunilor, mai ales dacă suferința copilului se datorează unei boli pe care ei au provocat-o, așa cum se întâmplă în cazul unora dintre lucrurile nepermise menționate mai sus.
1. Să se gândească la Viața de Apoi, pentru că nu există fericire și pace adevărată decât în Paradis; acolo nu există suferință și durere, ci doar sănătate, bunăstare și fericire. Și să se gândească la Iad, pentru că acesta este lăcașul durerii și suferinței eterne și fără sfârșit. Deci, el va face ceea ce îl va apropia de Paradis și îl va îndepărta de Iad.

## Care este înțelepciunea din spatele creării animalelor periculoase?

Înțelepciunea din spatele creării animalelor periculoase este de a manifesta natura perfectă a creației lui Allah și a controlului pe care îl exercită asupra tuturor lucrurilor. Chiar dacă lucrurile create sunt atât de multe, El le asigură tuturor hrană. De asemenea, El îi testează pe oameni prin intermediul acestora (creaturilor periculoase), îi răsplătește pe cei care sunt afectați de ele și manifestă curajul celor care le ucid. Prin crearea lor, El testează credința și certitudinea sclavilor Săi: credinciosul acceptă această creație și se supune Creatorului, în timp ce îndoielnicul spune: "Ce rost are ca Allah să creeze acest lucru?!" De asemenea, El demonstrează slăbiciunea și incapacitatea omului, prin faptul că acesta suferă dureri și boli din cauza unei creaturi care este mult mai mică decât el.

Unul dintre savanți a fost întrebat despre înțelepciunea din spatele creării muștelor. El a spus: „pentru ca Allah să umilească cu ele nasurile tiranilor”. Datorită existenței unor creaturi dăunătoare, devine evident cât de mare este binecuvântarea în crearea lucrurilor benefice, deoarece contrastul cu opusul demonstrează natura lucrurilor.

Studiul medicinei a demonstrat că multe medicamente benefice sunt derivate din veninul șerpilor și altele asemenea. Glorie Celui care a creat beneficii în lucruri care, prin toate aparențele exterioare, sunt dăunătoare. În plus, multe dintre aceste animale periculoase sunt hrană pentru alte creaturi care sunt benefice, iar acest lucru formează ciclul ecologic în mediile pe care Allah le-a creat.

Dar musulmanul trebuie să creadă că tot ceea ce face Allah este bun și că nu există rău pur în ceea ce creează El. În tot ceea ce creează El trebuie să existe un aspect al binelui, chiar dacă acesta ne este ascuns, cum este cazul creării lui Iblees (Satana). Dar există înțelepciune și un scop în spatele creării sale, pentru că Allah Își testează creaturile prin intermediul lui, pentru a-i deosebi pe cei ascultători de cei neascultători, pe cei care se străduiesc de cei care sunt neglijenți, pe oamenii Paradisului de oamenii Iadului.
