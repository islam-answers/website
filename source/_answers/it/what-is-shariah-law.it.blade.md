---
extends: _layouts.answer
section: content
title: Che cosa è la Shari'ah?
date: 2024-07-21
description: La Shari’ah rappresenta l’Islam intero, scelto da Allah per guidare i Suoi fedeli dalle tenebre alla luce.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 573107
---

La parola Shari'ah si riferisce alla religione dell'Islam nel suo complesso, religione che Allah ha scelto per i Suoi servi, per condurli dalla profondità delle tenebre alla luce. Egli ha prescritto per loro, spiegandoli, comandi e divieti, halal e haram.

Colui che segue la Shari'ah di Allah, considerando lecito ciò che Egli ha permesso e illecito ciò che Egli ha proibito, otterrà il trionfo.

Colui che si oppone alla Shari'ah di Allah, si espone all'ira, alla collera e alla punizione di Allah.

Allah, l'Altissimo, dice:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (che Allah abbia misericordia di lui) disse:

> La parola Shari'ah (pl. sharai') si riferisce a ciò che Allah ha prescritto (shara'a) agli uomini in materia di religione, e a ciò che ha comandato loro di osservare durante la preghiera, il digiuno, Hajj e così via. È la shir'ah, cioè il luogo del fiume in cui l'acqua si può bere.

Ibn Hazm (che Allah abbia misericordia di lui) disse:

> La Shari'ah è l'insieme delle leggi islamiche che Allah, l'Altissimo, ha prescritto (shara'a) per gli uomini attraverso il Suo Profeta [la pace sia su di lui], e tramite i Profeti (la pace sia su di loro) che sono venuti prima di lui. La sentenza fornita con un testo abrogativo deve essere considerata come sentenza definitiva.

### Origine linguistica del termine Shari'ah

L'origine linguistica del termine Shari'ah si riferisce al luogo in cui un cavaliere può giungere a bere acqua, e al luogo del fiume nel quale l'acqua si può bere. Allah, l'Altissimo, dice:

{{ $page->verse('42:13') }}
