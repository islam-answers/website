---
extends: _layouts.answer
section: content
title: ¿Qué significa Subhanallah?
date: 2024-10-25
description: Comúnmente traducido como "Gloria a Allah", aparece en el Corán con el
  fin de negar las deficiencias y defectos atribuidos a Allah, y afirmar Su perfección.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 597714
---

El término árabe "tasbih" suele referirse a la frase "SubhanAllah". Es una frase que Alá ha aprobado para Sí mismo, y ha inspirado a Sus ángeles para que la digan, y guiado a los más excelentes de Su creación para que la declaren. "SubhanAllah" suele traducirse como "Gloria a Alá", pero esta traducción se queda corta para transmitir todo el significado del tasbih.

El tasbih se compone de dos palabras: Subhana y Allah. Lingüísticamente, la palabra "subhana" proviene de la palabra "sabh", que significa distancia, lejanía. En consecuencia, Ibn 'Abbas explicó la frase como la pureza y la absolución de Allah por encima de todo mal o asunto impropio. En otras palabras, Allah está siempre lejos y eternamente por encima de toda maldad, deficiencia, defecto e inadecuación.

Esta frase aparece en el Corán con el fin de negar las descripciones inapropiadas e incompatibles atribuidas a Allah:

{{ $page->verse('23:91') }}

Una sola tasbih basta para negar todo defecto o mentira atribuido a Allah en todo lugar y en todo tiempo por cualquiera o toda la creación. Así, Allah refuta numerosas mentiras atribuidas a Él con una sola tasbih:

{{ $page->verse('37:149-159') }}

El propósito de la negación es que se afirme su opuesto. Por ejemplo, cuando el Corán niega la ignorancia de Allah, entonces el musulmán niega la ignorancia y a su vez afirma lo contrario para Allah, que es el conocimiento que todo lo abarca. La tasbih es fundamentalmente una negación (de las deficiencias, las malas acciones y las afirmaciones falsas) y, por tanto, según este principio, conlleva la afirmación de lo opuesto, que es la existencia bella, la conducta perfecta y la máxima verdad.

Sa'd bin Abu Waqqas (que Allah esté complacido con él) informó:

{{ $page->hadith('muslim:2698') }} 
