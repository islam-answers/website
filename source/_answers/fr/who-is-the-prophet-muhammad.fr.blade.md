---
extends: _layouts.answer
section: content
title: Qui est le prophète Mohammed ?
date: 2024-04-04
description: Le prophète Mohammed (que la paix soit sur lui) était un parfait exemple
  d'un homme honnête, juste, clément, compatissant, véridique et brave.
sources:
- href: https://www.islam-guide.com/fr/ch3-8.htm
  title: islam-guide.com
---

Mohammed {{ $page->pbuh() }} était très religieux et pendant longtemps, il détesta la décadence et l'idolâtrie de la société dans laquelle il vivait.

À l'âge de quarante ans, Mohammed {{ $page->pbuh() }} reçut sa première révélation de Dieu par l'intermédiaire de l'ange Gabriel. Les révélations se poursuivirent pendant vingt-trois ans, et ensemble elles formèrent ce que nous connaissons comme le Coran.

Dès qu'il commença à réciter le Coran et à prêcher la vérité que Dieu lui avait révélée, il souffrit, avec son petit groupe de disciples, de persécutions de la part des mécréants. Les persécutions devinrent si acharnées qu'en l'an 622, Dieu leur ordonna d'émigrer. Cette émigration de la Mecque à la ville de Médine, située à environ 260 milles (418 km) au nord, marque le début du calendrier musulman.

Après plusieurs années, Mohammed {{ $page->pbuh() }} et ses disciples purent enfin retourner à la Mecque, où ils pardonnèrent à leurs ennemis. Avant que Mohammed {{ $page->pbuh() }} ne meure, à l'âge de soixante-trois ans, la majeure partie de la Péninsule Arabe était devenue musulmane, et moins d'un siècle après sa mort, l'islam s'était propagé jusqu'en Espagne à l'ouest, et aussi loin qu'en Chine à l'est. Parmi les raisons qui expliquent la propagation rapide et pacifique de l'islam, il y a la vérité et la clarté de sa doctrine. L'islam appelle les gens à ne croire qu'en un seul Dieu, qui est le Seul qui mérite d'être adoré.

Le prophète Mohammed {{ $page->pbuh() }} était un parfait exemple d'un homme honnête, juste, clément, compatissant, véridique et brave. Bien qu'il fût un homme, il était très loin d'en avoir les mauvaises caractéristiques, et il luttait et faisait tous ses efforts par amour pour Dieu et pour Sa récompense dans l'au-delà. De plus, dans toutes ses actions et ses relations avec les gens, il avait toujours la crainte de Dieu et le souci de Lui plaire.
