---
extends: _layouts.answer
section: content
title: L’islam, religion des Arabes ?
date: 2024-06-04
description: L'islam n'est pas réservé aux Arabes. La majorité des musulmans ne sont
  pas arabes et l'islam est une religion universelle qui s'adresse à tous les peuples.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=MSC01&articleID=MSC010003&articlePages=1
  title: spubs.com
translator_id: 573111
---

Le moyen le plus direct de prouver que cette affirmation est totalement fausse est de préciser que seuls 15 à 20 % des musulmans dans le monde sont des Arabes. Il y a plus de musulmans indiens que de musulmans arabes, et plus de musulmans indonésiens que de musulmans indiens ! La conviction que l'islam n'est qu'une religion pour les Arabes est un mythe répandu par les ennemis de l'islam au début de son histoire. Cette hypothèse erronée est probablement fondée sur le fait que la plupart des musulmans de la première génération étaient arabes, que le Coran est en langue arabe et que le prophète Muhammad {{ $page->pbuh() }} était arabe. Cependant, les enseignements de l'islam et l'histoire de sa diffusion montrent que les premiers musulmans se sont efforcés de diffuser leur message de Vérité à toutes les nations, à toutes les ethnies et à tous les peuples.

En outre, il convient de préciser que tous les Arabes ne sont pas musulmans et que tous les musulmans ne sont pas arabes. Un Arabe peut être de religion musulmane, chrétienne, juive, athée, ou de toute autre religion ou idéologie. Par ailleurs, de nombreux pays que certains considèrent comme « arabes » ne le sont absolument pas, comme la Turquie et l'Iran (Perse). La langue maternelle des habitants de ces pays n'est pas l'arabe et leur héritage ethnique est différent de celui des Arabes.

Il est important de comprendre que dès le début de la mission du prophète Muhammad {{ $page->pbuh() }}, ses disciples étaient composés d'un large éventail d'individus : il y avait Bilal, l'esclave africain ; Suhaib, le Romain byzantin ; Ibn Sailam, le rabbin juif ; et Salman, le Persan. Puisque la vérité religieuse est éternelle et immuable, et que l'humanité constitue une fraternité universelle, l'islam enseigne que les révélations de Dieu Tout-Puissant à l'humanité ont toujours été cohérentes, claires et universelles.

La vérité de l'islam s'adresse à tous les peuples, sans distinction d'origines ethniques, de nationalités ou d'origines linguistiques. Un regard sur le monde musulman, du Nigeria à la Bosnie et de la Malaisie à l'Afghanistan, suffit à prouver que l'islam est un message universel pour toute l'humanité. Et ce, sans oublier le fait qu'un nombre important d'Européens et d'Américains de toutes origines ethniques se convertissent à l'islam.
