<?php

namespace App\Listeners;

class GenerateTable
{
    public function handle($jigsaw)
    {
        if ($jigsaw->getEnvironment() === "production") {
            return;
        }

        $answers = $jigsaw
            ->getCollection("answers")
            ->groupBy(fn($item) => $item->getSharedFilename())
            ->map(
                fn($group) => $group->mapWithKeys(
                    fn($item) => [$item->getLanguage() => $item]
                )
            );
        $languages = $jigsaw->getCollection("languages");

        $table = [];

        foreach ($answers as $slug => $answer) {
            $row = [
                "title" => sprintf(
                    "<a href='%s'>%s</a>",
                    $answer->first()->getRealUrl(),
                    $answer->first()->getSharedFilename()
                ),
                "slug" => $slug,
                "languages" => [],
            ];

            foreach ($languages as $language) {
                $row["languages"][$language->getFilename()] = $answer->has(
                    $language->getFilename()
                )
                    ? "✔"
                    : "✘";
            }

            $table[] = $row;
        }

        file_put_contents(
            $jigsaw->getDestinationPath() . "/answers_matrix.html",
            $this->generateTable($table, $languages)
        );
    }

    private function generateTable($table, $languages)
    {
        $html = "<style>
            table {
                border-collapse: collapse;
                width: 100%;
            }
            th, td {
                border: 1px solid #ddd;
                padding: 8px;
            }
            th {
                background-color: #f2f2f2;
            }
            th {
                text-align: left;
            }
            tr:hover {
                background-color: #f5f5f5;
            }
            </style>";
        $html .= "<table >\n";
        $html .= "<tr>\n";
        $html .= "<th>Question</th>\n";
        $html .= sprintf(
            "<th colspan=\"%d\">Languages</th>\n",
            count($languages)
        );
        $html .= "</tr>\n";

        $html .= "<tr>\n";
        $html .= "<td></td>\n";
        foreach ($languages as $language) {
            $html .= "<td>" . $language->title . "</td>\n";
        }
        $html .= "</tr>\n";

        foreach ($table as $row) {
            $html .= "<tr>\n";
            $checkbox = sprintf(
                "<input type='checkbox' name='%s'>",
                $row["slug"]
            );
            $html .= "<td>" . $checkbox . $row["title"] . "</td>\n";

            foreach ($row["languages"] as $language => $value) {
                $html .= "<td>" . $value . "</td>\n";
            }

            $html .= "</tr>\n";
        }

        $html .= "</table>\n";

        return $html;
    }
}
