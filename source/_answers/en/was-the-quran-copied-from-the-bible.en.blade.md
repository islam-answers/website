---
extends: _layouts.answer
section: content
title: Was the Quran copied from the Bible?
date: 2024-10-19
description: The Quran was not copied from the Bible. Prophet Muhammad was illiterate,
  no Arabic Bible existed at the time, and the revelations came directly from Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
---

Allah created everything around us with a purpose. Man has also been created with a purpose and that is to worship Allah. Allah says in the Quran:

{{ $page->verse('51:56') }}

For the guidance of mankind, Allah sent prophets from time to time. These prophets received revelation from Allah and each religion possesses its own collection of divine scriptures, which constitute the foundation of their beliefs. These scriptures are the material transcription of divine revelation, directly as in the case of prophet Moses (Musa), who received the commandments from Allah, or indirectly as in the case of Jesus (Isa) and Muhammad {{ $page->pbuh() }}, who transmitted to the people the revelation communicated to them by angel Gabriel (Jibreel).

The Quran directs all Muslims to believe in the scriptures that precede it. However, as Muslims we believe in the Torah (Tawrah), Book of Psalms (Zaboor), Gospel (Injeel) in their original form, i.e. the way they were revealed, to be the word of Allah.

## Could prophet Muhammad have copied the Quran from the Bible?

The discussion of how the Prophet Muhammad {{ $page->pbuh() }} could have learned the stories of the People of the Book, then transmitted them in the Quran – meaning that it was not revelation – is a discussion of various claims in which he {{ $page->pbuh() }} could have learned the stories directly from their holy books, or learned what the scriptures contained of stories, commands and prohibitions, verbally from the People of the Book, if it was not possible for him to study their books himself.

These claims may be summed up in three basic claims:

1. The claim that the Prophet Muhammad {{ $page->pbuh() }} was not unlettered or illiterate
1. The claim that the Christian scriptures were available to the Prophet {{ $page->pbuh() }} and he could quote from them or copy them
1. The claim that Mecca was a major educational centre for scriptural studies

Firstly, the text of the Quran and Sunnah in many places clearly indicate, leaving no room for doubt, that the Prophet Muhammad {{ $page->pbuh() }} was indeed unlettered and that he never narrated any of the stories of the People of the Book before the revelation came to him, and he never wrote anything with his own hand. He knew nothing of the stories before his mission began, and he never spoke of them before that.

Allah, may He be exalted, says:

{{ $page->verse('29:48') }}

The books of the Prophet’s biography and history make no mention of the Prophet writing down the revelation, or of him writing his letters to the kings himself. Rather the opposite is what is mentioned; the Prophet Muhammad {{ $page->pbuh() }} had scribes who wrote down the revelation and other things. In a brilliant text of Ibn Khaldun, there is a description of the level of education in the Arabian Peninsula just before the Prophet’s mission began. He says:

> Among the Arabs, the skill of writing was rarer than she-camels’ eggs. Most of the people were illiterate, especially the desert-dwellers, because this is a skill that is usually found among city-dwellers.

Hence the Arabs did not refer to the illiterate person as being illiterate; rather they would describe one who knew how to read and write as being knowledgeable, because knowing how to read and write was the exception, not the norm among people.

Secondly, there is no evidence, indication or suggestion that any Arabic translation of the Bible existed at that time. Al-Bukhari narrated from Abu Hurayrah (may Allah be pleased with him) that he said:

{{ $page->hadith('bukhari:7362') }}

This hadith indicates that the Jews had a full monopoly on the text and its explanation. If their texts were known in Arabic, there would have been no need for the Jews to read the text or explain it [to the Muslims].

This idea is supported by the verse in which Allah, may He be exalted, says:

{{ $page->verse('3:78') }}

Perhaps the most important book written on this topic is the book of Bruce Metzger, Professor of New Testament Language and Literature, The Bible in Translation, in which he said:

> It is most likely that the oldest Arabic translation of the Bible dates back to the eighth century.

The Orientalist Thomas Patrick Hughes wrote:

> There is no proof that Muhammad had read the Christian Scriptures... It must be noted that there is no clear evidence to suggest that any Arabic translation of the Old and New Testaments existed prior to the time of Muhammad.

Thirdly, the idea that the Prophet acquired the revelation from people is an old idea. The polytheists accused him of that, and the Quran refuted them, as Allah, may He be exalted, says:

{{ $page->verse('25:5-6') }}

Moreover, the claim that the Prophet Muhammad {{ $page->pbuh() }} acquired knowledge from the People of the Book when he was in Mecca is a claim that is disproven by the academic and cultural status of Mecca and the true nature of the connection this Arab society had with the knowledge of the People of the Book.

Therefore, after proving that the Prophet Muhammad {{ $page->pbuh() }} did not learn the knowledge of the People of the Book from them directly or through an intermediary, we have no option left except to affirm that it was revelation from Allah to His chosen Prophet.
