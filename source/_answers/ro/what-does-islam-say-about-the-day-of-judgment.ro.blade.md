---
extends: _layouts.answer
section: content
title: Ce spune Islamul despre Ziua Judecății?
date: 2024-03-11
description: Va veni o zi când întregul univers va fi distrus și morții vor fi înviați
  pentru judecată de către Dumnezeu.
sources:
- href: https://www.islam-guide.com/ch3-5.htm
  title: islam-guide.com
translator_id: 554943
---

La fel ca și creștinii, musulmanii cred că viața prezentă este doar o pregătire de încercare pentru următorul tărâm al existenței. Această viață este un test pentru fiecare individ, pentru viața de după moarte. Va veni o zi când întregul univers va fi distrus și morții vor fi înviați pentru judecată de către Dumnezeu. Această zi va fi începutul unei vieți care nu se va sfârși niciodată. Această zi este Ziua Judecății. În acea zi, toți oamenii vor fi răsplătiți de Dumnezeu în funcție de credințele și faptele lor. Cei care mor în timp ce cred că „Nu există nicio altă zeitate în afară de Dumnezeu, iar Muhammad este Mesagerul (Profetul) lui Dumnezeu” și sunt musulmani vor fi răsplătiți în acea zi și vor fi admiși în Paradis pentru totdeauna, așa cum a spus Dumnezeu:

{{ $page->verse('2:82') }}

Dar cei care mor fără să creadă că „Nu există nicio altă zeitate în afară de Dumnezeu, iar Muhammad este Mesagerul (Profetul) lui Dumnezeu” sau nu sunt musulmani, vor pierde Paradisul pentru totdeauna și vor fi trimiși în Focul Iadului, așa cum a spus Dumnezeu:

{{ $page->verse('3:85') }}

Și după cum El a spus:

{{ $page->verse('3:91') }}

Cineva se poate întreba: „Cred că islamul este o religie bună, dar dacă ar fi să mă convertesc la islam, familia mea, prietenii și alți oameni m-ar persecuta și și-ar bate joc de mine. Deci, dacă nu mă convertesc la islam, voi intra în Paradis și voi fi mântuit de focul iadului?’

Răspunsul este ceea ce Dumnezeu a spus în versetul precedent: "Acela care doreşte o altă religie decât Islamul, nu‑i va fi acceptată, şi el se va afla în Lumea de Apoi printre cei pierduţi."

După ce Profetul Muhammad {{ $page->pbuh() }} a fost trimis să invite oamenii la acceptarea Islamului, Dumnezeu nu acceptă aderarea la nicio altă religie decât aceasta. Dumnezeu este Creatorul și Susținătorul nostru. El a creat pentru noi tot ce este pe pământ. Toate binecuvântările și lucrurile bune pe care le avem sunt de la El. Deci, după toate acestea, când cineva respinge credința în Dumnezeu, Profetul Său Muhammad {{ $page->pbuh() }} sau religia Sa, este drept ca acea persoană să fie pedepsită în Viața de Apoi. De fapt, scopul principal al creației noastre este să ne închinăm numai lui Dumnezeu și să-L ascultăm, așa cum a spus Dumnezeu în Sfântul Coran (51:56)

Viața pe care o trăim astăzi este o viață foarte scurtă. Necredincioșii în Ziua Judecății vor crede că viața pe care au trăit-o pe pământ a fost doar o zi sau o parte dintr-o zi, așa cum a spus Dumnezeu:

{{ $page->verse('23:112-113..') }}

Și El a spus:

{{ $page->verse('23:115-116..') }}

Viața de Apoi este o viață foarte reală. Nu este doar spirituală, ci și fizică. Vom trăi acolo cu sufletele și trupurile noastre.

Comparând această lume cu Viața de Apoi, Profetul Muhammad {{ $page->pbuh() }} a exprimat faptul că valoarea acestei lumi în comparație cu cea a vieții de Apoi este ca câteva picături de apă în comparație cu marea.
