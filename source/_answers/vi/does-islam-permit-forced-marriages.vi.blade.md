---
extends: _layouts.answer
section: content
title: Hồi giáo có cho phép hôn nhân cưỡng bức không?
date: 2024-08-13
description: Cả nam và nữ đều có quyền lựa chọn người bạn đời của mình, và một cuộc
  hôn nhân sẽ được coi là vô hiệu nếu không có sự đồng ý chân thành của phụ nữ.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 587785
---

Hôn nhân sắp đặt là những phong tục văn hoá phổ biến ở một số quốc gia trên thế giới. Hôn nhân ép buộc đã bị gán ghép không đúng với Hồi giáo mặc dù không chỉ giới hạn ở người Hồi giáo.

Trong Hồi giáo, cả nam và nữ đều có quyền chọn hoặc từ chối người mình định kết hôn, và một cuộc hôn nhân sẽ được coi là vô hiệu nếu không có sự đồng ý chân thành của người phụ nữ trước khi kết hôn.
