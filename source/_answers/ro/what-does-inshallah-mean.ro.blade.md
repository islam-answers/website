---
extends: _layouts.answer
section: content
title: Ce înseamnă Inshallah?
date: 2024-06-20
description: Inshallah înseamnă că orice acțiune viitoare pe care un musulman dorește
  să o facă depinde de voința lui Allah.
sources:
- href: https://www.islamweb.net/en/fatwa/416905/
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/357538/
  title: islamweb.net
translator_id: 554943
---

Expresia "In Shaa' Allah" (dacă Allah vrea) înseamnă că orice act viitor pe care un musulman dorește să îl facă depinde de voința lui Allah.

A spune "In Shaa' Allah" face parte din eticheta Islamului. Musulmanul este instruit să spună "In Shaa' Allah" ori de câte ori își exprimă dorința de a efectua o acțiune viitoare.

{{ $page->verse('18:23-24..') }}

Atunci când persoana o spune în legătură cu acțiunile sale viitoare, aceste acțiuni pot avea loc sau nu; cu toate acestea, se speră mai mult că vor avea loc dacă el spune „în Shaa’ Allah”.

Povestea profetului Sulayman (Solomon) indică acest sens.

{{ $page->hadith('bukhari:3424') }}

Aceasta este o dovadă că atunci când cineva spune "In Shaa' Allah", este mai probabil ca dorința și nevoia sa să fie îndeplinite; totuși, acest lucru nu este neapărat valabil tot timpul. Dacă Allah vrea, aceasta se va împlini. Trebuie remarcat faptul că privarea (de a obține acel lucru) ar putea fi o binecuvântare deghizată.

{{ $page->verse('..2:216') }}
