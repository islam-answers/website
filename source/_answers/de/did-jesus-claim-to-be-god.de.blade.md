---
extends: _layouts.answer
section: content
title: Behauptete Jesus, Gott zu sein?
date: 2024-11-03
description: Muslime glauben, dass Jesus ein Prophet war, der die Menschen dazu aufrief,
  allein Allah anzubeten, und der nie behauptete, Gott zu sein.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
translator_id: 587746
---

Muslime glauben, dass Jesus {{ $page->pbuh() }} einer der Diener Allahs und einer seiner edlen Gesandten war. Allah sandte Jesus zu den Kindern Israels, um sie aufzurufen, nur an Allah zu glauben und nur Ihn anzubeten.

Allah unterstützte Jesus {{ $page->pbuh() }} mit Wundern, die bewiesen, dass er die Wahrheit sprach. Jesus wurde als Sohn der Jungfrau Maryam (Maria) ohne Vater geboren. Er erlaubte den Juden einige der Dinge, die ihnen zuvor verboten worden waren. Er starb nicht, und seine Feinde, die Juden, töteten ihn nicht; vielmehr bewahrte Allah ihn vor ihnen und erhob ihn lebendig in den Himmel. Jesus kündigte seinen Anhängern das Kommen unseres Propheten Muhammad {{ $page->pbuh() }} an. Am Ende der Zeit wird Jesus wiederkommen und sich am Tag der Auferstehung von den Behauptungen distanzieren, dass er Gott gewesen sei.

Am Tag der Auferstehung wird Jesus vor dem Herrn der Welten stehen, der ihn vor den Zeugen fragen wird, was er den Kindern Israels gesagt hat, wie Allah sagt:

{{ $page->verse('5:116-118') }}

Eine ehrliche Untersuchung zeigt, dass Jesus {{ $page->pbuh() }} nie ausdrücklich und eindeutig gesagt hat, Gott zu sein. Es waren die Autoren des Neuen Testaments, von denen keiner Jesus begegnete, die behaupteten, er sei Gott, und die dies verbreiteten. Es gibt keine Beweise dafür, dass die tatsächlichen Apostel Jesu dies behaupteten oder dass sie die Bibel verfassten. Vielmehr wird gezeigt, dass Jesus ständig Gott allein anbetet und andere dazu aufruft, ihn allein anzubeten.

Die Behauptung, dass Jesus {{ $page->pbuh() }} Gott sei, kann Jesus nicht zugeschrieben werden, wenn sie dem ersten Gebot, keine Götter neben Gott anzunehmen und nichts im Himmel und auf Erden zu einem Götzen zu machen, so stark widerspricht. Außerdem ist es logisch absurd, dass Gott seine Schöpfung wird.

Daher ist nur die Behauptung des Islam plausibel und mit der frühen Offenbarung vereinbar: Jesus {{ $page->pbuh() }} war ein Prophet, nicht Gott.
