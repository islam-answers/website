---
extends: _layouts.answer
section: content
title: イエス様は十字架にかけられましたか?
date: 2024-10-07
description: コーランによれば、イエス様は殺されたり、十字架にかけられたりはしませんでした。イエス様はアラーによって生きたまま天に召しになったのです。そして、彼の代わりに別の男が十字架にかけられました。
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 594447
---

ユダヤ教徒とローマ皇帝がイエス様　{{ $page->pbuh() }}　の殺害を企てたとき、アッラーは、その場にいた人の中にイエスとすべてが似ている人を創造しました。それで、彼らはイエスに似た男を殺しました。彼らはイエス自身を殺さなかったが、イエス{{ $page->pbuh() }}、生きて天に上げられました。
その証拠としては、アッラーはユダヤ教徒の虚構についてコーランで述べたことです：

{{ $page->verse('4:157-158') }}

このように、ユダヤ教徒がイエス様を殺し、十字架にかけたと主張した時、アッラーはその言葉は偽りだと言いました。そして、イエス様を御自分のところに連れて行ったと述べました。それはアッラーのイエス（イーサー） {{ $page->pbuh() }} に対する慈悲や、栄誉であり、それによって彼「イエス様」はアッラーのしるしの一つとなりました。アッラーが使徒の中からお望みの者に授ける栄誉です。

「しかしアッラーは、彼（イエス様)を(その肉体と魂とともに）御自身のもとにお召しになったのである」という節の意味は、アッラーがイエス様を肉体と魂の両方を上げて、ユダヤ人がイエスを十字架にかけて殺したという主張に反駁したということです。 なぜなら、殺害と十字架刑は肉体に関係するからです。さらに、もしアッラーがイエス様の魂だけを上げていたとしても、彼を殺して十字架にかけたという主張の否定になりません。 また、イエスの名を指したのは {{ $page->pbuh() }}、実際には魂と肉体の両方を指しており、そのこと以外を示す兆候がない限り、どちらか一方だけを指すことはない。さらに、魂と肉体の両方を同時に召しになったことは、アッラーの完全な力と英知、そしてアッラーが使徒の中から望む者への栄誉と支援を意味しており、アッラーは節の終わりでこう語っています。「アッラー*はもとより、偉力ならびない*お方、英知あふれる*お方。」
