<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

class AnswersHelper
{
    function getBaseAnswers(): Collection
    {
        $englishAnswers = collect(glob('source/_answers/en/*.md'));

        return $englishAnswers->mapWithKeys(function ($answer) {
            $slug = basename($answer, '.en.blade.md');
            $content = file_get_contents($answer);

            $metadata = preg_match('/---(.*?)---/s', $content, $matches);
            $metadata = $matches[1];
            $metadata = yaml_parse($metadata);

            $markdown = preg_replace('/---(.*?)---/s', '', $content);

            // TODO: make this into an object
            return [
                $answer => [
                    'slug' => $slug,
                    'metadata' => $metadata,
                    'markdown' => $markdown,
                    'url' => "https://islam-answers.com/answers/en/{$slug}",
                ],
            ];
        });
    }
}
