---
extends: _layouts.answer
section: content
title: Este OK să amâni convertirea la islam?
date: 2024-11-24
description: Amânarea convertirii la islam este descurajată, deoarece oricine moare
  urmând o altă religie decât islamul a pierdut viața aceasta și cea de Apoi.
sources:
- href: https://islamqa.info/en/answers/2585/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/260971/
  title: islamweb.net
translator_id: 554943
---

Islamul este adevărata religie a lui Allah (Dumnezeu), iar acceptarea ei ajută persoana să atingă fericirea în această viață lumească și în Viața de Apoi. Allah a încheiat revelațiile divine cu religia islamului; prin urmare, El nu acceptă ca robii Săi să aibă o altă religie decât islamul, după cum spune în Coran:

{{ $page->verse('3:85') }}

Oricine moare în timp ce urmează o altă religie decât islamul a pierdut viața lumească și cea de Apoi. Prin urmare, este obligatoriu ca persoana să se grăbească să accepte islamul, deoarece ea nu știe posibilele obstacole pe care le poate întâlni, inclusiv moartea. Prin urmare, nu ar trebui să existe nicio amânare în această privință.

Moartea este posibilă în orice moment, așa că o persoană ar trebui să se grăbească să accepte islamul, astfel încât, dacă timpul ei (i.e. moartea) ar veni în curând, să îl întâlnească pe Allah ca adept al religiei Sale, islamul, în afară de care El nu acceptă nicio altă religie.

Trebuie remarcat faptul că un musulman este obligat să adere la ritualurile și îndatoririle devoționale aparente, cum ar fi efectuarea rugăciunii la timp; cu toate acestea, poate să le efectueze în secret și chiar să combine două rugăciuni în conformitate cu sunnah (tradiția) profetului {{ $page->pbuh() }} atunci când apare o nevoie urgentă.

Prin urmare, a muri ca non-musulman nu este ca și cum ai muri ca un păcătos musulman. Un non-musulman va rămâne în focul iadului pentru veșnicie. Cu toate acestea, chiar dacă un musulman intră în focul iadului din cauza păcatelor, va fi pedepsit pentru ele, dar nu va rămâne în el permanent. Mai mult, Allah ar putea ierta păcatele acestuia din urmă, ar putea salva o astfel de persoană din focul iadului și ar putea să o admită în Paradis. Allah spune în Coran:

{{ $page->verse('4:116') }}

În cele din urmă, vă sfătuim să vă grăbiți să acceptați islamul și totul va fi mai bine, cu voia lui Allah. Allah spune în Coran:

{{ $page->verse('..65:2-3..') }}
