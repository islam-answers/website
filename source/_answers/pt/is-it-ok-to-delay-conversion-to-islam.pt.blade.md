---
extends: _layouts.answer
section: content
title: É correto adiar a conversão ao Islam?
date: 2025-01-23
description: Adiar a conversão ao Islam é desaconselhado, pois quem morre seguindo
  outra religião perde a vida neste mundo e no Além.
sources:
- href: https://islamqa.info/en/answers/2585/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/260971/
  title: islamweb.net
translator_id: 594172
---

O Islam é a verdadeira religião de Allah e a sua adoção ajuda a pessoa a alcançar a felicidade nesta vida terrena e na outra. Allah concluiu as revelações divinas com a religião do Islam; por conseguinte, não aceita que os Seus servos tenham outra religião que não o Islam, como diz no Alcorão:

{{ $page->verse('3:85') }}

Quem morre enquanto segue uma religião diferente do Islam perdeu a vida do mundo e da outra vida. Assim, é obrigatório que a pessoa se apresse a abraçar o Islam, uma vez que não sabe quais os possíveis obstáculos que pode encontrar, incluindo a morte. Por conseguinte, não deve haver procrastinação a este respeito.

A morte é possível a qualquer momento, pelo que se deve apressar a entrada no Islam imediatamente, para que, se a sua hora chegar, se encontre com Allah como um seguidor da Sua religião, o Islam, para além da qual Ele não aceita nenhuma outra religião.

Deve notar-se que um muçulmano é obrigado a aderir aos ritos e deveres de devoção aparentes, tais como fazer a oração a horas; no entanto, pode fazê-las secretamente e até combinar duas orações, de acordo com a sunnah do profeta {{ $page->pbuh() }}, quando surge uma necessidade premente.

Por conseguinte, morrer como um não-muçulmano não é como morrer como um pecador muçulmano. Um não-muçulmano permanecerá no fogo do Inferno eternamente. No entanto, mesmo que um muçulmano entre no fogo do Inferno devido aos seus pecados, será punido por eles, mas não permanecerá nele permanentemente. Além disso, Allah pode perdoar os pecados deste último, salvá-lo do fogo do Inferno e admiti-lo no Paraíso. Allah diz no Alcorão:

{{ $page->verse('4:116') }}

Por último, aconselhamo-lo a apressar-se a abraçar o Islam e tudo melhorará, se Allah quiser. Allah diz no Alcorão:

{{ $page->verse('..65:2-3..') }}
