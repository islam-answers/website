---
extends: _layouts.answer
section: content
title: 伊斯兰教对进化论有何看法？
date: 2024-11-23
description: 真主以其最终的形态创造了第一个人类，亚当。至于人类以外的生物，伊斯兰教文献对此未作具体说明。
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 603054
---

真主以其最终的形态创造了第一个人类亚当，而不是从高级猿猴逐渐进化而来。科学无法直接证实或否认这一点，因为这是一个独一无二的历史事件——一个奇迹。

至于人类以外的生物，伊斯兰教文献对此未作具体说明，只是要求我们相信真主以他所意欲的方式创造了它们，无论是通过进化还是其他方式。
