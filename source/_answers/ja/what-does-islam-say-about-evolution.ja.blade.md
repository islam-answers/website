---
extends: _layouts.answer
section: content
title: 進化論についてイスラムの見方は？
date: 2024-09-18
description: アッラー「唯一の神様」は最初の人間、アダムを最終的な形で創造した。人間以外の生物に関しては、イスラム教の文献では何も語れるていない。
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 594447
---

アッラー「唯一の神様」は最初の人間、アダムを最初から最終的な形で創造した。それは人間が進化した類人猿から徐々に進化したという進化論の内容の逆となる。アダムの創造は、唯一無二の歴史的出来事、すなわち、奇跡であるため、科学によって直接確認、または否定しかねる。

人間以外の生物については、イスラム教の文献はこの問題について沈黙しており、アッラー「唯一の神様」が望む方法で創造したと信じることだけを求めている。
