---
extends: _layouts.answer
section: content
title: Warum dürfen Muslime nicht daten?
date: 2024-09-14
description: Der Quran verbietet romantische Beziehungen wie das Haben von Freund oder Freundin. Der Islam regelt Beziehungen entsprechend der menschlichen Natur.
sources:
- href: https://islamqa.org/shafii/seekersguidance-shafii/241530
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107140
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107560
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/shafii/qibla-shafii/33045
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/askimam/17968
  title: islamqa.org (AskImam.org)
- href: https://www.islamweb.net/en/fatwa/89542
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/82217
  title: islamweb.net
- href: https://islamqa.info/en/answers/20949
  title: islamqa.info
- href: https://islamqa.info/en/answers/33702
  title: islamqa.info
- href: https://islamqa.info/en/answers/1200
  title: islamqa.info
- href: https://islamqa.info/en/answers/21933
  title: islamqa.info
- href: https://islamqa.info/en/answers/93450
  title: islamqa.info
- href: https://islamqa.info/en/answers/1114
  title: islamqa.info
- href: https://islamqa.info/en/answers/3807
  title: islamqa.info
translator_id: 587746
---

Der Islam wahrt die Beziehungen zwischen Individuen auf eine Weise, die der menschlichen Natur entspricht. Der Kontakt zwischen Männern und Frauen ist nur im Rahmen einer rechtmäßigen Ehe erlaubt. Wenn es notwendig ist, mit dem anderen Geschlecht zu sprechen, sollte dies in den Grenzen von Höflichkeit und guten Manieren geschehen.

## Versuchungen vorbeugen

Der Islam ist darauf bedacht, jede Form von Unzucht von Anfang an zu verhindern. Es ist weder für Frauen noch für Männer erlaubt, über Chats, das Internet oder andere Wege eine Freundschaft oder Liebesbeziehung mit dem anderen Geschlecht aufzubauen, da dies zu Versuchungen führen kann. Dies ist die Methode des Teufels, der die Person dazu verführt, in Sünde, Unzucht oder Ehebruch zu verfallen. Allah sagt:

{{ $page->verse('24:21..') }}

Es ist der Frau verboten, mit jemandem, der nicht ihr Ehemann ist, auf eine sanfte oder verführerische Weise zu sprechen, wie Allah sagt:

{{ $page->verse('..33:32') }}

Das Zusammenkommen, die Vermischung und das Miteinander von Männern und Frauen an einem Ort, das Gedränge und das Zur-Schau-Stellen von Frauen vor Männern sind nach dem islamischen Gesetz (Scharia) verboten. Diese Handlungen sind untersagt, da sie zu Fitnah (Versuchung oder Prüfung mit negativen Konsequenzen), zur Erregung von Begierden und zur Begehung von Unanständigkeit und Unrecht führen können. Der Prophet {{ $page->pbuh() }} sagte:

{{ $page->hadith('ahmad:1369') }}

Der Quran verbietet das Führen von Liebesbeziehungen wie Freundinnen oder Freunden. Allah sagt:

{{ $page->verse('..4:25..') }}

## Soziale Überlegungen

Eine Beziehung im Sinne von Freund/Freundin einzugehen, kann den Ruf einer Frau ernsthaft schädigen, ohne dass der männliche Partner notwendigerweise betroffen ist. Sex vor der Ehe widerspricht den religiösen Geboten und gefährdet die Abstammung und die Ehre. Es ist oft schwierig, klar zu sehen, wenn man von Liebe, Lust oder Verliebtheit überwältigt ist – deshalb gebietet uns Allah, uns von vorehelichem Sex fernzuhalten. Schon ein einziges unrechtmäßiges Mal kann enormen Schaden anrichten, wie zum Beispiel ungewollte Schwangerschaften, sexuell übertragbare Krankheiten, gebrochene Herzen und Schuldgefühle.

Abgesehen von dem ausdrücklichen Verbot durch Gott gibt es offensichtliche Weisheiten, warum voreheliche Beziehungen unzulässig sind. Zu diesen gehören:

1. Die Eindeutigkeit der Vaterschaft, falls die Frau schwanger wird. Selbst in langjährigen Beziehungen, wer kann garantieren, dass die Frau nicht mit anderen Männern schläft, um die Kompatibilität eines zukünftigen Ehepartners zu prüfen?
1. Ein uneheliches Kind wird nicht dem Vater zugeschrieben. Der Erhalt der Abstammung ist eines der Hauptziele der Scharia.
1. Solche Beziehungen geben Männern den Vorteil, mit Frauen zu tun, was sie wollen, ohne emotionale oder finanzielle Verantwortung gegenüber der Frau oder den aus der Beziehung geborenen Kindern zu tragen.
1. Es liegt eine angeborene Tugend darin, die eigene Keuschheit vor der Ehe zu bewahren, die seit vielen Jahrhunderten anerkannt und von allen Religionen bekräftigt wird.
1. Auch wenn wir keinen sichtbaren Unterschied in unseren weltlichen Angelegenheiten bemerken, haben unangemessene Praktiken eine Wirkung auf die Seele, die wir nicht wahrnehmen können. Je mehr man in Sünde verfällt, desto mehr verhärtet sich das Herz.
1. Mehrere Partner zu haben, erhöht das Risiko, sexuell übertragbare Krankheiten zu bekommen und andere anzustecken.
1. Es gibt keine Garantie, dass das Zusammenleben vor der Ehe ein sicheres Zeichen für den Zustand der Beziehung nach der Ehe ist. Viele nicht-muslimische Paare leben jahrelang zusammen, nur um sich nach der Heirat zu trennen.
1. Man sollte sich auch fragen, ob man möchte, dass die eigenen Söhne und Töchter das Gleiche vor der Ehe tun.

Abschließend lässt sich sagen, dass es keinen Sinn ergibt, langfristige Beziehungen und Sex vor der Ehe zu erlauben. Die individuellen Überlegungen sind unbedeutend im Vergleich zu den zahlreichen schwerwiegenden Problemen, die vor allem Frauen betreffen und sicher zu einem moralischen Verfall der Gesellschaft sowie der eigenen Seele führen würden.
