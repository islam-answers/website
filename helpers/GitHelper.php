<?php

namespace App\Helpers;

use Closure;
use Symfony\Component\Process\Process;

class GitHelper
{
    private array $originalConfigs;

    private array $author = [
        'name' => 'CI',
        'email' => 'ci@islam-answers.com',
    ];

    function __construct(protected bool $verbose = false) {}

    function runWithOutput(array $args): string
    {
        $command = [
            'git',
            ...$args,
        ];

        if ($this->verbose) echo '--> ' . implode(' ', $command) . PHP_EOL;

        $process = new Process($command, __DIR__);

        $process->mustRun();

        $output = trim($process->getOutput());

        if ($this->verbose) echo '<-- ' . $output . PHP_EOL . PHP_EOL;

        return $output;
    }

    function run(array $args): self
    {
        $this->runWithOutput(array_filter($args));

        return $this;
    }

    function fetch(): self
    {
        return $this->run([
            'fetch',
            '--all',
        ]);
    }

    function pull(): self
    {
        return $this->run([
            'pull',
            'origin',
            'main',
        ]);
    }

    function getBranches(): array
    {
        $branches = explode(PHP_EOL, $this->runWithOutput([
            'branch',
            '-r',
            '--format=%(refname:short)',
        ]));

        return array_map(fn ($branch) => trim(preg_replace('/^.*\//', '', $branch)), $branches);
    }

    function switch(string $branch, bool $create = false): self
    {
        return $this->run([
            'switch',
            $create ? '-c' : '',
            $branch,
        ]);
    }

    function reset(bool $hard = false, string $commit = 'HEAD'): self
    {
        return $this->run([
            'reset',
            $hard ? '--hard' : null,
            $commit,
        ]);
    }

    function add(array $files): self
    {
        return $this->run([
            'add',
            ...$files,
        ]);
    }

    function commit(string $message): self
    {
        return $this->run([
            '-c', 'user.name=' . $this->author['name'],
            '-c', 'user.email=' . $this->author['email'],
            'commit',
            '-m',
            $message,
        ]);
    }

    function push(string $mergeRequestTitle, string $branchName): self
    {
        return $this->run([
            'push',
            'origin',
            $branchName,
            '-o',
            'merge_request.create',
            '-o',
            'merge_request.remove_source_branch',
            '-o',
            'merge_request.target=main',
            '-o',
            'merge_request.title=' . $mergeRequestTitle,
        ]);
    }
}
