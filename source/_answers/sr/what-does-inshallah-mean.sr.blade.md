---
extends: _layouts.answer
section: content
title: Šta znači Inšallah?
date: 2025-02-20
description: Inšallah znači da svaki budući čin koji musliman želi učiniti zavisi
  od Allahove volje.
sources:
- href: https://www.islamweb.net/en/fatwa/416905/
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/357538/
  title: islamweb.net
translator_id: 584497
---

Izraz “In Shaa’ Allah” (ako Allah hoće) znači da svaki budući čin koji musliman želi učiniti zavisi od Allahove volje.

Reći "In Shaa' Allah" dio je bontona islama. Muslimanu je naređeno da kaže "In Shaa' Allah" kad god izrazi želju da izvrši buduću akciju.

{{ $page->verse('18:23-24..') }}

Kada osoba to kaže u vezi sa svojim budućim radnjama, ove radnje se mogu ili ne moraju dogoditi; međutim, više se nada da će se dogoditi ako on kaže "In Shaa' Allahu."

Priča o Poslaniku Sulejmanu (Solomonu) ukazuje na ovo značenje.

{{ $page->hadith('bukhari:3424') }}

Ovo je dokaz da kada neko kaže "In Shaa' Allahu", veća je verovatnoća da će mu se želja i potreba ispuniti; međutim, to nije nužno uvek slučaj. Ako Allah to želi, to će se ispuniti. Treba napomenuti da bi lišavanje moglo biti prikriveni blagoslov.

{{ $page->verse('..2:216') }}
