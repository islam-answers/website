---
extends: _layouts.answer
section: content
title: ¿Sobre qué trata el Corán?
date: 2024-04-03
description: La fuente primordial de la fe y práctica de cada musulmán.
sources:
- href: https://www.islam-guide.com/es/ch3-7.htm
  title: islam-guide.com
---

El Corán, la última revelación de Dios, es la fuente primordial de la fe, y práctica de cada musulmán. El Corán trata todos los temas que se relacionan con los seres humanos: sabiduría, doctrina, adoración, transacciones, leyes, etc., pero su tema básico es la relación entre Dios y sus criaturas. Al mismo tiempo provee líneas de Guía y detalladas enseñanzas para lograr una sociedad justa, una conducta humana correcta y un sistema económico equitativo.

Nótese que el Corán fue revelado en árabe al profeta Muhammad {{ $page->pbuh() }}, por lo tanto cualquier traducción del mismo, ya sea en castellano o en cualquier otro idioma, no es el Corán, ni es una versión del mismo; es tan solo una traducción de los significados del Corán. El Corán como tal, existe tan solo en árabe tal y como fuera revelado.
