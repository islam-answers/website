---
extends: _layouts.answer
section: content
title: Como Alguém se Torna Muçulmano?
date: 2024-06-02
description: Simplesmente dizendo com convicção, “La ilaha illa Allah, Muhammadur
  rasoolu Allah,” alguém se converte ao Islã e se torna um muçulmano.
sources:
- href: https://www.islam-guide.com/pt/ch3-6.htm
  title: islam-guide.com
---

Simplesmente dizendo com convicção, “La ilaha illa Allah, Muhammadur rasoolu Allah,” alguém se converte ao Islã e se torna um muçulmano. Essa frase significa “Não existe verdadeiro deus (divindade) exceto Deus (Allah), e Muhammad é o Mensageiro (Profeta) de Deus.” A primeira parte, “Não existe verdadeiro deus exceto Deus”, significa que ninguém tem o direito de ser adorado mas Deus apenas, e que Deus não tem nem parceiro ou filho. Para ser um muçulmano, deve-se também:

- Acreditar que o Alcorão Sagrado é a palavra literal de Deus, revelada por Ele.
- Acreditar que o Dia do Juízo (o Dia da Ressurreição) é verdade e chegará, como Deus prometeu no Alcorão.
- Aceitar o Islã como sua religião.
- Não adorar a nada nem ninguém, exceto Deus.

O Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2747') }}
