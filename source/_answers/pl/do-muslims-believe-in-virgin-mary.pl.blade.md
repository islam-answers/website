---
extends: _layouts.answer
section: content
title: Czy muzułmanie wierzą w Maryję Dziewicę?
date: 2024-12-28
description: Maryam jest wymieniona w Koranie jako osoba wierząca. Muzułmanie szanują
  ją i wierzą w jej czystość i niewinność.
sources:
- href: https://islamqa.info/en/answers/241999/
  title: islamqa.info
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/32915/
  title: islamqa.org (SeekersGuidance.org)
translator_id: 584496
---

Maryam (Maryja), córka Imraana, oby Allah był z niej zadowolony, jest jedną z oddanych czcicielek i prawych kobiet. Wspomniano o niej w Koranie, gdzie opisano ją jako posiadającą cechy ludzi wiary. Była wierzącą, której wyznanie, potwierdzenie Jedności Allaha i posłuszeństwo wobec Niego były doskonałe.

Koran opisuje ją jako prawdziwą niewolnicę Allaha, która była pokorna i posłuszna Allahowi, Panu Światów. Allah, niech będzie wywyższony, mówi:

{{ $page->verse('3:42-43') }}

### Niewinność Maryam

Muzułmanie szanują Maryam i wierzą w to, co na jej temat powiedziano w Świętym Koranie, a mianowicie w jej czystość i niewinność wobec tego, o co oskarżali ją wrogowie Allaha:

{{ $page->verse('4:156') }}

W Koranie jest cały rozdział nazwany imieniem Maryam, który pięknie przekazuje historię narodzin z dziewicy. W islamskim ujęciu Maryam była dziewicą; nigdy nie wyszła za mąż przed ani w czasie narodzin Jezusa {{ $page->pbuh() }}. Maryam była bardzo zszokowana, gdy anioł poinformował ją, że urodzi syna, mówiąc:

{{ $page->verse('19:20') }}

Źródła islamskie nie wspominają o zaręczynach Maryam, nie wspominają też o Józefie, późniejszym małżeństwie ani o rodzeństwie Jezusa {{ $page->pbuh() }}.

Wśród muzułmanów jest ona idealną muzułmanką, jedną z najcnotliwszych kobiet Raju, jak powiedział Prorok Muhammad {{ $page->pbuh() }} :

{{ $page->hadith('muslim:2431') }}

W innej narracji Prorok {{ $page->pbuh() }} powiedział:

{{ $page->hadith('bukhari:3432') }}

### Cudowne narodzenie Jezusa – znak dla całej ludzkości

Muzułmanie wierzą, że Allah uczynił ją i jej syna znakiem dla całej ludzkości i świadectwem Jedności, Panowania i Potęgi Allaha. Allah, niech będzie wywyższony, mówi:

{{ $page->verse('23:50..') }}

Mówi także:

{{ $page->verse('3:45') }}

Allah ostrzega także tych, którzy uważają Jezusa i jego matkę, Maryam, za bogów, łącząc ich z Allahem:

{{ $page->verse('5:75..') }}

Allah, niech będzie uwielbiony i wywyższony, powiedział nam, że w Dniu Zmartwychwstania Prorok Allaha 'Isa ibn Maryam (Jezus syn Marii) {{ $page->pbuh() }} wyrzeknie się politeizmu tych, którzy wiążą go z Allahem i przesadzają o nim i jego matce. Zastanówmy się nad tą wzniosłą wymianą, która będzie miała miejsce między Panem Chwały i Majestatu a Jego niewolnikiem i Posłańcem 'Isa ibn Maryam {{ $page->pbuh() }}. Allah, niech będzie wywyższony, mówi:

{{ $page->verse('5:116-117') }}
