---
extends: _layouts.answer
section: content
title: Milline on naise staatus islamis?
date: 2024-06-08
description: Mosleminaistel on õigus omada vara, teenida raha ja kulutada seda nii,
  nagu nad soovivad. Islam julgustab mehi naisi hästi kohtlema.
sources:
- href: https://www.islam-guide.com/ee/ch3-13.htm
  title: islam-guide.com
---

Islam näeb naist, olgu ta siis vallaline või abielus, kui indiviidi, kellel on oma õigused, kaasa arvatud õigus omada ja jagada oma vara ja sissetulekut, ilma kellegi eestkosteta (olgu siis tegu isa, abikaasa või kellegi kolmandaga). Tal on õigus osta ja müüa, teha kingitusi ja anda raha heategevuseks ning ta võib kulutada oma raha nagu soovib. Abielu kaasavara antakse peigmehe poolt pruudile isiklikuks kasutamiseks ja ta hoiab pigem alles oma perekonnanime, kui võtab abikaasa oma.

Islam julgustab meest oma naist hästi kohtlema, kuna Muhammed {{ $page->pbuh() }} on öelnud:

{{ $page->hadith('ibnmajah:1977') }}

Emad on islamis kõrgelt austatud. Islam soovitab kohelda neid parimal moel.

{{ $page->hadith('bukhari:5971') }}
