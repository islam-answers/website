---
extends: _layouts.answer
section: content
title: අල්ලාහ් දෙවියන් මුහම්මද් නබිතුමාව එව්වේ ඇයි?
date: 2025-02-19
description: සැබෑ ඒක දේවවාදය ප්‍රතිෂ්ඨාපනය කිරීමට, ආගමික ආරවුල් පැහැදිලි කිරීමට සහ
  මිනිසුන්ව ධර්මිෂ්ඨකමට මඟ පෙන්වීමට අල්ලාහ් නබි මුහම්මද් තුමා එවීය.
sources:
- href: https://islamqa.info/en/answers/11575/
  title: islamqa.info
- href: https://islamqa.info/en/answers/13957/
  title: islamqa.info
- href: https://islamqa.info/en/answers/10468/
  title: islamqa.info
- href: https://islamqa.info/en/answers/2114/
  title: islamqa.info
translator_id: 583845
---

නබිවරු යනු අල්ලාහ්ගේ දාසයන්ට ඔහුගේ දූතයින්ය; ඔවුන් ඔහුගේ ආඥා ප්‍රකාශ කරන අතර, අල්ලාහ් ඔහුගේ ආඥාවලට කීකරු වුවහොත් ඔවුන්ට පොරොන්දු වූ සතුට පිළිබඳ ශුභාරංචිය ඔවුන්ට ලබා දෙන අතර, ඔහුගේ තහනම් කිරීම්වලට පටහැනිව ගියහොත් ස්ථිර දඬුවමක් ගැන ඔවුන්ට අනතුරු අඟවයි. ඔවුන් අතීත ජාතීන්ගේ කථා සහ ඔවුන්ගේ ස්වාමියාගේ ආඥාවලට අකීකරු වීම නිසා මෙලොවදී ඔවුන්ට අත් වූ දඬුවම සහ පළිගැනීම් ඔවුන්ට පවසති.

මෙම දිව්‍ය ආඥා සහ තහනම් කිරීම් ස්වාධීන චින්තනයෙන් දැනගත නොහැක. එබැවින් අල්ලාහ් නීති සහ නියෝග සහ තහනම් කිරීම් නියම කර ඇත්තේ මනුෂ්‍ය වර්ගයාට ගරු කිරීමට සහ ඔවුන්ගේ අවශ්‍යතා ආරක්ෂා කිරීමට ය. මන්ද මිනිසුන් තම ආශාවන් අනුගමනය කර සීමාවන් ඉක්මවා ගොස් මිනිසුන්ට අතවර කර ඔවුන්ගේ අයිතිවාසිකම් අහිමි කළ හැකි බැවිනි.

### අල්ලාහ් නබිවරුන් සහ දූතයන් යැව්වේ ඇයි?

අල්ලාහ් තම ප්‍රඥාවෙන් මනුෂ්‍ය වර්ගයා අතරට වරින් වර පණිවිඩකරුවන් යැව්වේ අල්ලාහ්ගේ ආඥා මතක් කර දීමට සහ පාපයට වැටීමෙන් වළකින්න ඔවුන්ට අනතුරු ඇඟවීමට, ඔවුන්ට දේශනා කිරීමට සහ ඔවුන්ට පෙර සිටි අයගේ කථා පැවසීමටයි. මක්නිසාද යත් මිනිසුන් ආශ්චර්යමත් කථා අසන විට එය ඔවුන්ගේ මනස අවදි කරයි, එබැවින් ඔවුන් තේරුම් ගෙන දැනුම වැඩි කර නිවැරදිව තේරුම් ගනී. මක්නිසාද යත්, මිනිසුන් වැඩි වැඩියෙන් අසන තරමට, ඔවුන්ට වැඩි අදහස් ලැබෙනු ඇත; ඔවුන්ට වැඩි අදහස් ඇති තරමට, ඔවුන් වැඩි වැඩියෙන් සිතනු ඇත; ඔවුන් සිතන තරමට, ඔවුන් වැඩි වැඩියෙන් දැන ගනු ඇත; ඔවුන් දන්නා තරමට, ඔවුන් වැඩි වැඩියෙන් කරනු ඇත. එබැවින් සත්‍යය පැහැදිලි කිරීම සඳහා පණිවිඩකරුවන් යැවීමට විකල්පයක් නොමැත.

පෘථිවියේ ජනතාවට පණිවිඩකරුවන් සඳහා ඇති අවශ්‍යතාවය හිරු, සඳු, සුළඟ සහ වැස්ස සඳහා ඇති අවශ්‍යතාවය මෙන් නොවේ, නැතහොත් මිනිසෙකුට තම ජීවිතයට ඇති අවශ්‍යතාවය මෙන් නොවේ, නැතහොත් ඇසට ආලෝකය සඳහා ඇති අවශ්‍යතාවය මෙන් නොවේ, නැතහොත් ශරීරයට ආහාර සහ පාන වර්ග සඳහා ඇති අවශ්‍යතාවය මෙන් නොවේ. ඒ වෙනුවට එය ඊට වඩා විශාලයි, ඔබට සිතිය හැකි ඕනෑම දෙයකට ඔහුගේ අවශ්‍යතාවයට වඩා විශාලයි. පණිවිඩකරුවන් (අල්ලාහ්ගේ සාමය සහ ආශිර්වාදය ඔවුන් කෙරෙහි වේවා) අල්ලාහ් සහ ඔහුගේ මැවිල්ල අතර අතරමැදියන් වන අතර, ඔහුගේ අණපනත් සහ තහනම් කිරීම් දැනුම් දෙයි. ඔවුන් උන් වහන්සේගෙන් ඔහුගේ වහලුන්ට තානාපතිවරු වෙති. ඔවුන්ගෙන් අවසාන සහ ශ්‍රේෂ්ඨතම, ඔහුගේ ස්වාමියා ඉදිරියේ උතුම්ම තැනැත්තා මුහම්මද් {{ $page->pbuh() }} ය. අල්ලාහ් ඔහුව ලෝකයන්ට දයාවක් ලෙස, අල්ලාහ් වෙත සමීප වීමට කැමති අයට මඟ පෙන්වීමක් ලෙස එව්වේය, එය සියලු මිනිසුන්ට කිසිදු නිදහසට කරුණක් ඉතිරි නොකළ සාක්ෂියකි.

නබිවරුන් සහ පණිවිඩකරුවන් බොහෝ දෙනෙක් සිටි අතර, ඔවුන්ගේ සංඛ්‍යාව අල්ලාහ් හැර වෙන කිසිවෙකු දන්නේ නැත. ඔවුන් අතර අල්ලාහ් අපට පවසා ඇති අය සිටින අතර, ඔහු අපට පවසා නැති අයද සිටිති. අල්ලාහ් මෙසේ පවසයි:

{{ $page->verse('4:164..') }}

### අල්ලාහ් මුහම්මද් නබිතුමා ව එව්වේ ඇයි?

නබිතුමාගේ {{ $page->pbuh() }} පණිවිඩය ඔහුට පමණක් ආවේණික වූවක් නොව, ඔහුට පෙර සිටි අනෙකුත් පණිවිඩකරුවන් විසින් ගෙන එන ලද පණිවිඩයට සමාන ස්වභාවයකින් සහ අන්තර්ගතයකින් යුක්ත විය. අල්ලාහ් මූසා (මෝසෙස්), ඊසා (ජේසුස්) සහ තවත් අය වැනි අනාගතවක්තෘවරුන් සහ පණිවිඩකරුවන් ඊශ්‍රායෙල් දරුවන් වෙත යවා ඇති අතර, බොහෝ දෙනෙක් ඔවුන් කෙරෙහි විශ්වාසය තබා ඔවුන්ගේ පොත්වල සත්‍යයට සාක්ෂි දුන්හ, ඒවා සාමාන්‍යයෙන් කුර්ආනය මගින් ගෙන එන ලද පණිවිඩයට සමාන විය. විශේෂයෙන් එය ඔවුන් සාක්ෂි දුන් සත්‍යයට සමාන පණිවිඩයකට අයත් වූ බැවින්, ඔහු යවන ලද පණිවිඩයේ සත්‍යතාවයට මෙය වාචික සාක්ෂියක් විය.

අල්ලාහ් මුහම්මද් {{ $page->pbuh() }} තුමාණන්ව ඔහුට පෙර සිටි අනාගතවක්තෘවරුන් මෙන් එකම පණිවිඩය සමඟ එවූ විට, කුර්ආනය පැමිණියේ ඔවුන්ගේ පොත් සහ ඔවුන්ගේ අනාගතවක්තෘත්වය තහවුරු කිරීමට සහ ඒවා විශ්වාස කරන ලෙස මිනිසුන්ට ආරාධනා කිරීමට ය. එබැවින් ග්‍රන්ථයේ ජනයා ඔහු සහ ඔහුගේ පොත විශ්වාස නොකළ විට, එයින් අදහස් වූයේ ඔවුන් තමන්ගේම පොත් සහ පණිවිඩකරුවන් විශ්වාස නොකරන බවයි. කුර්ආනයේ ඔවුන්ගේ පොත් වලට සමාන මූලධර්ම අඩංගු වූ අතර ඒවා තහවුරු කළ බැවින්, මෙයින් අදහස් කළේ එය ගොතන ලද හෝ අල්ලාහ් හැර වෙනත් මූලාශ්‍රයකින් පැමිණීමට ඇති ඉඩකඩ අවම බවයි, මන්ද ඒ සියල්ල අල්ලාහ්ගෙන් පැමිණි බැවිනි, ඔහු උසස් වේවා.

ඊශ්‍රායෙල් දරුවන් අතර මතභේද සහ ආරවුල් ඇති විය. ඔවුන් තම විශ්වාසයන් සහ නීතිවල වෙනස්කම් සහ වෙනස්කම් හඳුන්වා දුන්හ. මේ අනුව සත්‍යය නිවී ගොස් අසත්‍යය ජය ගත්තේය, පීඩනය සහ නපුර පුළුල් විය, සත්‍යය ස්ථාපිත කරන, නපුර විනාශ කරන සහ මිනිසුන්ව සෘජු මාර්ගයට යොමු කරන ආගමක් මිනිසුන්ට අවශ්‍ය විය, එබැවින් අල්ලාහ් මුහම්මද් {{ $page->pbuh() }} තුමාව එව්වේ අල්ලාහ් පැවසූ පරිදි:

{{ $page->verse('16:64') }}

අල්ලාහ් සියලු නබිවරුන් සහ රසූල්වරුන් එව්වේ අල්ලාහ්ට පමණක් නමස්කාර කරන ලෙසත්, මිනිසුන් අන්ධකාරයෙන් ආලෝකයට ගෙන ඒම සඳහාත් ය. මෙම රසූල්වරුන්ගෙන් පළමුවැන්නා නූහ් වන අතර ඔවුන්ගෙන් අවසානයා මුහම්මද් {{ $page->pbuh() }} අල්ලාහ් පැවසූ පරිදි:

{{ $page->verse('16:36..') }}
