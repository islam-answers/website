---
extends: _layouts.answer
section: content
title: Pourquoi Dieu nous a-t-il créés ?
old_titles:
- Pourquoi Dieu nous a-t-il créés et pourquoi sommes-nous sur cette terre ?
date: 2024-11-12
description: Dieu nous a créés pour reconnaître et affirmer Son unicité en L'adorant exclusivement, sans associé ni partenaire.
sources:
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/108100/
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.info/en/answers/45529/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/127330/
  title: islamqa.org (AskImam.org)
translator_id: 573111
---

L'un des plus grands attributs d'Allah est la sagesse, et l'un de Ses noms les plus nobles est al-Hakim (Le Plus Sage). Rien de ce qu'Il a créé n'est sans but ; Allah est bien au-dessus de toute futilité. Au contraire, Il crée toutes choses pour des raisons profondes et sages, visant des objectifs sublimes. C'est ce qu'Allah a affirmé dans le Coran :

{{ $page->verse('23:115-116') }}

Allah dit également dans le Coran :

{{ $page->verse('44:38-39') }}

Tout comme la sagesse derrière la création de l'homme est établie d'un point de vue de la charia (loi islamique), elle l'est également d'un point de vue rationnel. Une personne sage reconnaît que chaque chose a été créée pour une raison, et considère absurde de faire quoi que ce soit sans but dans sa propre vie. Que dire alors d'Allah, le Plus Sage des sages ?

Allah n'a pas créé l'homme uniquement pour qu'il mange, boive et se reproduise, car cela le rendrait semblable aux animaux. Allah a honoré l'homme et l'a élevé bien au-dessus de nombreuses créatures qu'Il a créées. Cependant, beaucoup persistent dans l'incrédulité, ignorant ou rejetant la véritable sagesse de leur création, ne cherchant qu'à profiter des plaisirs de ce monde. La vie de ces personnes ressemble à celle des animaux, voire pire encore dans leur égarement. Allah dit :

{{ $page->verse('..47:12') }}

L'une des principales raisons pour lesquelles Allah a créé l'humanité — qui constitue l'une des plus grandes épreuves — est le commandement d'affirmer Son Unicité et de L'adorer Seul, sans associé ni collaborateur. Allah a énoncé cette raison pour la création de l'humanité, comme Il le dit :

{{ $page->verse('51:56') }}

Ibn Kathir (qu'Allah lui fasse miséricorde) a dit :

> Cela signifie : « Je les ai créés pour leur ordonner de M'adorer, et non parce que J'ai besoin d'eux. »

En tant que musulmans, notre but dans la vie est à la fois profond et simple. Nous nous efforçons d'harmoniser tous les aspects de notre être, tant extérieur qu'intérieur, avec ce qu'Allah a ordonné. Allah ne nous a pas créés parce qu'Il a besoin de nous. Il est au-delà de tout besoin, et c'est nous qui avons besoin d'Allah.

Allah nous a dit que la création des cieux et de la terre, ainsi que de la vie et de la mort, a aussi pour but d'éprouver l'homme. Quiconque Lui obéit, Il le récompensera, et quiconque Lui désobéit, Il le punira. Allah dit :

{{ $page->verse('67:2') }}

De ce test résulte une manifestation des noms et attributs d'Allah, tels que al-Rahman (le Tout Miséricordieux), al-Ghafur (le Pardonneur), al-Hakim (le Très Sage), al-Tawwab (Celui qui accepte le repentir), al-Rahim (le Très Miséricordieux), et d'autres noms d'Allah.
