---
extends: _layouts.answer
section: content
title: 伊斯兰教的主要教义是什么？
date: 2024-12-14
description: 五大支柱是伊斯兰教的基本信仰。 这五大支柱分别是：“信仰”、“祈祷”、“斋戒”、“天课”以及“朝觐”。
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 603054
---

伊斯兰教的主要教义被称为五大支柱。

### 第一项支柱 : 信仰

宣告除真主外，绝无应受崇拜的神，穆罕默德是真主的最后使者。

### 第二项支柱 : 礼拜

每天进行五次：黎明、中午、午后、日落之后和晚上各一次。

### 第三项支柱：规定的施舍“天课”

这是一项每年向不幸者支付的强制性慈善金，按个人年度总储蓄的一小部分计算，其中包括 2.5% 的货币财富，可能还包括其他资产。它由拥有超额财富的人支付。

### 第四项支柱：斋月的斋戒

整个斋月期间，从黎明到日落，穆斯林必须禁食、禁饮，不得与配偶发生性关系。斋月提倡自我克制、敬畏真主和同情穷人。

###  第五项支柱：朝圣

每一个有能力的穆斯林一生中都必须去麦加朝圣。朝圣包括礼拜、祈祷、施舍和旅行，是一种令人谦卑且极具精神性的体验。
