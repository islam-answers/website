---
extends: _layouts.answer
section: content
title: Ai đã viết Kinh Qur'an?
date: 2024-06-26
description: Kinh Qur'an là những lời của Thượng Đế đã mặc khải cho Thiên Sứ của Ngài
  là Thiên Sứ Muhammad thông qua Thiên Thần Jibriel (Gabriel).
sources:
- href: https://www.islam-guide.com/vi/ch1-1.htm
  title: islam-guide.com
---

Thượng Đế đã trợ giúp Thiên Sứ cuối cùng của Ngài là Thiên Sứ Muhammad {{ $page->pbuh() }} bằng cách ban cho Ông rất nhiều phép mầu và những dấu hiệu để chứng minh rằng Ông đúng là Thiên Sứ do Thượng Đế đề cử (tuyển chọn). Bên cạnh đó, Thượng Đế cũng đã mặc khải cho Ông quyển sách cuối cùng của Ngài, Thánh Kinh Qur'an, với nhiều phép mầu chứng tỏ rằng Kinh Qur'an là lời của Thượng Đế, do Ngài mặc khải, chứ không phải là tác phẩm của bất cứ con người trần thế nào.

Kinh Qur'an là những lời của Thượng Đế đã mặc khải cho Thiên Sứ của Ngài là Thiên Sứ Muhammad {{ $page->pbuh() }} thông qua Thiên Thần Jibriel (Gabriel). Những lời mặc khải của Thượng Đế được Thiên Sứ Muhammad {{ $page->pbuh() }}, ghi nhớ rồi đọc lại cho những người bạn của Ông. Sau đó, đến lượt những người này chép lại và đối chiếu lại với Thiên Sứ Muhammad {{ $page->pbuh() }}. Bên cạnh đó, Thiên Sứ Muhammad {{ $page->pbuh() }} ũng đối chiếu Kinh Qur'an với Thiên Thần Jibriel mỗi năm một lần và hai lần trong năm cuối đời của ông. Kể từ khi lúc Kinh Qur'an ra đời, cho đến ngày nay, có rất nhiều người Islam đã thuộc lòng từng chữ trong cuốn Kinh này. Thậm chí, rất nhiều người có thể nhớ toàn bộ Kinh Qur'an khi mới ở độ tuổi lên mười. Hàng thế kỷ trôi qua, Kinh Qur'an không hề thay đổi, dù chỉ là một chữ.

Kinh Qur'an được mặc khải cách đây 14 thế kỷ đã đề cập đến những sự kiện mới được các nhà khoa học khám phá ra hoặc chứng minh trong thời gian gần đây. Điều đó đã chứng tỏ rằng Kinh Qur'an phải là lời của Thượng Đế và do Ngài mặc khải cho Thiên Sứ Muhammad {{ $page->pbuh() }}, và rằng Kinh Qur'an không phải là tác phẩm của Thiên Sứ Muhammad {{ $page->pbuh() }} hay bất cứ người phàm nào khác. Điều đó cũng chứng tỏ rằng Thiên Sứ Muhammad {{ $page->pbuh() }} đúng là Thiên Sứ mà Thượng Đế đã tuyển chọn. Sẽ rất phi lý khi có ai đó cách đây 14 thế kỷ có thể biết trước được những sự kiện mới được khám phá ra hoặc chứng minh trong thời gian gần đây bằng những thiết bị tiên tiến và những phương pháp rất phức tạp.
