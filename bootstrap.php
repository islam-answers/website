<?php

// @var $container \Illuminate\Container\Container
// @var $events \TightenCo\Jigsaw\Events\EventBus

/*
 * You can run custom code at different stages of the build process by
 * listening to the 'beforeBuild', 'afterCollections', and 'afterBuild' events.
 *
 * For example:
 *
 * $events->beforeBuild(function (Jigsaw $jigsaw) {
 *     // Your code here
 * });
 */

$events->beforeBuild(App\Listeners\EnsureFilenamesAreLocalised::class);
$events->afterBuild(App\Listeners\EnforceSeoBestPractices::class);
$events->afterBuild(App\Listeners\ReportBadSubstitutions::class);
$events->afterBuild(App\Listeners\GenerateSitemap::class);
$events->afterBuild(App\Listeners\GenerateIndex::class);
$events->afterBuild(App\Listeners\GenerateTable::class);
$events->afterBuild(App\Listeners\BuildRedirects::class);
