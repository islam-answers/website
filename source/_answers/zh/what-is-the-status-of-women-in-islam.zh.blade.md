---
extends: _layouts.answer
section: content
title: 女性在伊斯兰教中的地位如何？
date: 2024-07-08
description: 穆斯林妇女有权拥有财产，赚钱，并随心所欲地花钱。伊斯兰教鼓励男人善待女人。
sources:
- href: https://www.islam-guide.com/ch3-13.htm
  title: islam-guide.com
translator_id: 554943
---

伊斯兰教将女性（不论是已婚还是未婚）视为与生俱来的独立个体， 有权选择拥有或放弃自己的财产和收入，不受任何监护人（父亲、丈夫或任何其它人）的限制。 她有权利进行买卖、送礼和施舍，可以随心所欲地花费自己的钱。 新郎要给予新娘嫁妆，供新娘本身花用，且新娘可以保留自己的姓，而不必改随夫姓。

伊斯兰教鼓励丈夫善待他的妻子，正如先知穆罕默德 {{ $page->pbuh() }} 所说：

{{ $page->hadith('ibnmajah:1977') }}

伊斯兰教的母亲均受到高度尊重。 伊斯兰教建议以最好的方式来对待她们。

{{ $page->hadith('bukhari:5971') }}
