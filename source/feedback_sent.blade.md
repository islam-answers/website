---
title: Anonymous feedback
description: Send anonymous feedback to the Islam Answers team
extends: _layouts.main
permalink: feedback/thanks.html
section: body
---

# Anonymous feedback

Thank you for sharing your feedback! We appreciate your time and effort in helping us improve our website.
