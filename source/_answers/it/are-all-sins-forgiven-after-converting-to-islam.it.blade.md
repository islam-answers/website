---
extends: _layouts.answer
section: content
title: Con la conversione, tutti i peccati vengono perdonati?
date: 2025-01-06
description: Nel momento in cui un miscredente diventa musulmano, Allah gli perdona
  tutto quel che ha fatto quando non era musulmano, purificandolo da ogni peccato.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
translator_id: 573107
---

Per Sua Grazia e Misericordia, Allah ha fatto sì che, con l'accettazione dell'Islam, vengano cancellati tutti i peccati commessi prima della testimonianza di fede. Quando un miscredente diventa musulmano, Allah perdona tutto quel che ha fatto quando non era musulmano, purificandolo dal peccato.

'Amr ibn al-'As (che Allah sia soddisfatto di lui) disse:

{{ $page->hadith('muslim:121') }}

L'Imam Nawawi ha affermato che "L'Islam distrugge ciò che è venuto prima di esso", ovvero lo cancella e lo elimina.

Allo Shaykh Ibn 'Uthaymin (che Allah abbia misericordia di lui) fu posta una domanda simile, a proposito di qualcuno che guadagnava denaro spacciando droga prima che diventasse musulmano. Egli rispose: "A questo fratello che Allah ha benedetto con l'Islam dopo che aveva guadagnato ricchezze illecite, diciamo: 'non temere' perché questa ricchezza gli è permessa e non vi è peccato in essa, sia che la conservi, che la dia in carità, o che la usi per sposarsi, perché Allah dice nel Suo Libro Sacro:

{{ $page->verse('8:38') }}

Ciò significa che tutto quel che è accaduto in passato, generalmente, è perdonato. Ma il denaro sottratto con la forza al legittimo proprietario deve essere restituito. Mentre il denaro guadagnato a seguito di accordi tra persone, anche se illecito - come quello ricavato con la Riba (interesse) o con la vendita di droga, ecc. - diviene lecito quando si diventa musulmani.

All'epoca del profeta Muhammad {{ $page->pbuh() }}, molti miscredenti si convertirono all'Islam dopo che avevano ucciso dei musulmani, e nonostante ciò non vennero puniti.
