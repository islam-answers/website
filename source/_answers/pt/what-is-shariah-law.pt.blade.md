---
extends: _layouts.answer
section: content
title: O que é sharia?
date: 2024-07-14
description: A palavra sharia se refere a toda a religião do Islam, que
  Allah escolheu para Seus servos, com o intuito de guiá-los das profundezas das trevas
  para a luz.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 554943
---

A palavra shari’ah (sharia) se refere a toda a religião do Islam, que Allah escolheu para Seus servos, com o intuito de guiá-los das profundezas das trevas para a luz. É o que Ele prescreveu para os servos e explicou a eles sobre os mandamentos e proibições, halaal e haram.

Aquele que segue a shari’ah de Allah, considerando como permissível o que Ele permitiu e considerando como proibido o que Ele proibiu, alcançará o triunfo.

Aquele que vai contra a shari'ah de Allah está se expondo à ira, irritação e punição de Allah.

Allah, Exaltado seja, diz (interpretação do significado):

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (que Allah tenha misericórdia dele) disse:

A palavra shari'ah (pl. sharaa'i') se refere ao que Allah prescreveu (shara'a) para as pessoas em relação a questões de religião, e o que Ele ordenou que elas aderissem quanto à oração, jejum, Hajj e assim por diante. É o shir'ah (o local num rio onde se pode beber).

Ibn Hazm (que Allah tenha misericórdia dele) disse:

Shari'ah é o que Allah, exaltado seja, prescreveu (shara'a) através dos lábios de Seu Profeta Muhammad {{ $page->pbuh() }} em relação à religião, e dos lábios dos Profetas (que a paz esteja com eles) que vieram anteriormente. A regra do texto revogatório deve ser considerada a decisão final.

### Origem linguística do termo Shari'ah

A origem linguística do termo shari'ah se refere ao lugar em que um cavaleiro pode vir e beber água, e o local no rio onde se pode beber. Allah, Exaltado seja, diz (interpretação do significado):

{{ $page->verse('42:13') }}
