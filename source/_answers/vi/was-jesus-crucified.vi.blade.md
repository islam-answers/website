---
extends: _layouts.answer
section: content
title: Jesus có bị đóng đinh không?
date: 2024-10-25
description: Theo Kinh Quran, Jesus không bị giết hay bị đóng đinh. Jesus đã được
  Allah đưa khi còn sống trở về Thiên đàng, và một người khác đã bị đóng đinh thay
  cho Ngài.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 587785
---

Khi người Do Thái và hoàng đế La Mã âm mưu giết Jesus {{ $page->pbuh() }}, Allah đã tạo ra một trong những người có mặt giống Jesus về mọi mặt. Vì vậy, họ đã giết người đàn ông trông giống Jesus. Họ không giết Jesus. Jesus {{ $page->pbuh() }} đã được đưa lên Thiên đàng khi còn sống. Bằng chứng cho điều đó là lời của Allah liên quan đến việc bịa đặt của người Do Thái và sự bác bỏ của họ:

{{ $page->verse('4:157-158') }}

Như vậy, Allah đã tuyên bố lời của người Do Thái là sai khi họ tuyên bố rằng họ đã giết và đóng đinh ngài ấy, và Người tuyên bố rằng Người đã đưa ngài ấy lên với Người. Đó là lòng thương xót của Allah đối với Giê-su (Isa) {{ $page->pbuh() }}, và là một vinh dự cho ngài ấy, để ngài trở thành một trong những dấu hiệu của Người, một vinh dự mà Allah ban cho bất kỳ ai Người muốn trong số các sứ giả của Người.

Ý nghĩa của cụm từ “Nhưng Allah đã đưa Ngài ấy (Jesus) lên (với thân xác và linh hồn) với chính Ngài ấy” là Allah, xin được tôn vinh Người, đã đưa Jesus lên cả thân xác và linh hồn, để bác bỏ lời tuyên bố của người Do Thái rằng họ đã đóng đinh và giết chết Ngài ấy, bởi vì giết và đóng đinh liên quan đến thể xác. Hơn nữa, nếu Người chỉ đưa linh hồn của Ngài ấy lên, điều đó sẽ không loại trừ lời tuyên bố đã giết và đóng đinh Ngài, bởi vì chỉ đưa linh hồn lên sẽ không phải là lời bác bỏ lời tuyên bố của họ. Ngoài ra, tên của Jesus {{ $page->pbuh() }}, trên thực tế, ám chỉ Ngài ấy ở cả linh hồn và thể xác; nó không thể chỉ ám chỉ một trong hai, trừ khi có dấu hiệu cho thấy điều đó. Hơn nữa, việc cả linh hồn và thể xác của Ngài ấy cùng nhau biểu thị cho quyền năng và sự khôn ngoan hoàn hảo của Allah, và cho thấy sự tôn vinh và hỗ trợ của Người đối với bất kỳ ai Người muốn trong số các Sứ giả của Người, theo như những gì Người, xin được tôn vinh Người, đã phán ở cuối câu: "Và Allah là Đấng Toàn năng, Toàn trí."
