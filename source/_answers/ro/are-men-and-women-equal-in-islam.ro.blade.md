---
extends: _layouts.answer
section: content
title: Sunt bărbații și femeile egali în islam?
date: 2024-11-26
description: În islam, bărbații și femeile sunt egali spiritual, dar au roluri și responsabilități complementare, asigurând echilibru, dreptate și respect reciproc.
sources:
- href: https://islamqa.info/en/answers/12840/
  title: islamqa.info
- href: https://islamqa.org/hanafi/fatwacentre/179051/
  title: islamqa.org (FatwaCentre.org)
- href: https://www.islamweb.net/en/article/109364/
  title: islamweb.net
translator_id: 554943
---

Islamul a venit să onoreze femeile și să le ridice statutul, să le ofere o poziție care să li se potrivească, să aibă grijă de ele și să le protejeze demnitatea. Astfel, islamul le poruncește tutorilor și soților femeilor să cheltuiască pentru ele, să le trateze bine, să aibă grijă de ele și să fie buni cu ele. Allah spune în Coran:

{{ $page->verse('..4:19..') }}

Se raportează că Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('tirmidhi:3895') }}

Islamul oferă femeilor toate drepturile lor și le permite să-și aranjeze treburile într-un mod adecvat. Aceasta include tot felul de tranzacții, cumpărare, vânzare, desemnarea altora să acționeze în numele lor, împrumut, depunere de trusturi etc. Allah spune:

{{ $page->verse('..2:228') }}

Islamul a impus femeilor actele de cult și îndatoririle care li se cuvin, aceleași îndatoriri ca și bărbaților, și anume purificarea, Zakat (caritatea prescrisă), postul, rugăciunea, Hajj (pelerinajul) și alte acte de cult.

Dar Islamul acordă femeii jumătate din partea bărbatului atunci când vine vorba de moștenire, deoarece ea nu este obligată să cheltuiască nimic pentru ea însăși, pentru casa ei sau pentru copiii ei. Mai degrabă, cel care este obligat să cheltuiască pentru ei este bărbatul.

În ceea ce privește faptul că mărturia a două femei este echivalentă cu mărturia unui bărbat în unele cazuri, aceasta se datorează faptului că femeile tind să uite mai des decât bărbații din cauza ciclurilor lor naturale de menstruație, sarcină, naștere, creșterea copiilor etc. Toate aceste lucruri le preocupă și le fac să uite. Prin urmare, legea islamică indică faptul că o altă femeie ar trebui să întărească mărturia unei femei, astfel încât aceasta să fie mai exactă. Cu toate acestea, există chestiuni care privesc numai femeile în care mărturia unei singure femei este suficientă, cum ar fi determinarea frecvenței cu care un copil a fost alăptat, defectele care pot afecta căsătoria și așa mai departe.

Femeile sunt egale cu bărbații în ceea ce privește răsplata, rămânând neclintite în credință și făcând fapte drepte, bucurându-se de o viață bună în această lume și de o mare răsplată în Viața de Apoi. Allah spune:

{{ $page->verse('16:97') }}

Femeile au drepturi și îndatoriri, la fel cum bărbații au drepturi și îndatoriri. Există chestiuni care se potrivesc bărbaților, astfel încât Allah le-a făcut responsabilitatea bărbaților, la fel cum există chestiuni care se potrivesc femeilor, astfel încât El le-a făcut responsabilitatea femeilor.
