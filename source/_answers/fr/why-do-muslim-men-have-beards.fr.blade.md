---
extends: _layouts.answer
section: content
title: Pourquoi les hommes musulmans ont-ils des barbes ?
date: 2025-02-15
description: Les hommes musulmans portent la barbe pour suivre l'exemple du prophète
  Muhammad, préserver leur disposition innée et se distinguer des non-croyants.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 618015
---

Le dernier des Messagers, Muhammad {{ $page->pbuh() }} laissa pousser sa barbe, tout comme les califes qui vinrent après lui, ainsi que ses compagnons, les dirigeants et le peuple commun des musulmans. C'est ce que préconisent les prophètes et les messagers, ainsi que leurs disciples, et cela fait partie de la fitrah (prédisposition originelle) avec laquelle Allah a créé les gens. Aisha (qu'Allah l'agrée) a rapporté que le Messager d'Allah {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:261a') }}

Ce qui est demandé au croyant, si Allah et Son Messager ont ordonné quelque chose, c'est de dire : « Nous entendons et obéissons », comme Allah dit :

{{ $page->verse('24:51') }}

Les hommes musulmans se laissent pousser la barbe par obéissance à Allah et à son Messager. Le Prophète {{ $page->pbuh() }} a ordonné aux musulmans de laisser pousser librement leurs barbes et de tailler leurs moustaches. Il a été rapporté d'Ibn 'Umar que le Prophète Muhammad {{ $page->pbuh() }} a dit :

{{ $page->hadith('bukhari:5892') }}

L'imam Ibn 'Abdil-Barr a dit : " Il est interdit de raser la barbe, et personne ne le fait à part les efféminés parmi les hommes." c'est-à-dire ceux qui imitent les femmes. Jabir rapporte ce qui sur le prophète Muhammad, {{ $page->pbuh() }} :

{{ $page->hadith('muslim:2344b') }}

Cheikh al-Islam Ibn Taymiyah (qu'Allah lui fasse miséricorde) a dit :

> Le Coran, la Sunnah et l'ijma' (consensus des savants) indiquent tous que nous devons nous différencier des mécréants dans tous les aspects et ne pas les imiter, car en les imitant en apparence, nous serons amenés à imiter leurs mauvaises pratiques et habitudes, et même leurs croyances. (...)
