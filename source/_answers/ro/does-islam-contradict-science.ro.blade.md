---
extends: _layouts.answer
section: content
title: Contrazice Islamul știința?
date: 2024-04-02
description: Coranul conține fapte științifice care au fost descoperite recent datorită
  progresului tehnologiei și cunoștințelor științifice.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 554943
---

Coranul este ultima carte sfântă revelată de Dumnezeu omenirii prin intermediul profeților.

Deși Coranul (revelat cu peste 1400 de ani în urmă) nu este o carte de știință, el conține fapte științifice care abia recent au fost descoperite datorită progresului tehnologiei. Islamul încurajează reflecția și cercetarea științifică, deoarece înțelegerea naturii creației le permite oamenilor să-și aprecieze și mai mult Creatorul și amploarea puterii și înțelepciunii Sale.

Coranul a fost revelat într-o perioadă în care știința era primitivă; nu existau telescoape, microscoape sau ceva apropiat tehnologiei actuale. Oamenii credeau că soarele orbitează în jurul pământului și că cerul era susținut de stâlpi la colțurile unui pământ plat. În acest context a fost revelat Coranul, care conține multe fapte științifice pe subiecte variind de la astronomie și biologie, până la geologie și zoologie.

Câteva dintre faptele științifice găsite în Coran includ:

### Faptul #1 - Originea vieții

{{ $page->verse('21:30') }}

Apa este indicată ca originea întregii vieți. Toate ființele vii sunt formate din celule și acum știm că celulele sunt formate în mare parte din apă. Acest lucru a fost descoperit abia după inventarea microscopului. În deșerturile Arabiei, ar fi de neconceput să credem că cineva ar fi ghicit că toată viața provine din apă.

### Faptul #2 - Dezvoltarea embrionară umană

Dumnezeu vorbește despre etapele dezvoltării embrionare a omului:

{{ $page->verse('23:12-14') }}

Cuvântul arab „alaqah” are trei semnificații: o lipitoare, un lucru suspendat și un cheag de sânge. Oamenii de știință în domeniul embriologiei au remarcat că utilizarea acestor termeni în descrierea formării embrionului este corectă și în conformitate cu înțelegerea noastră științifică actuală a procesului de dezvoltare.

Până în secolul al XX-lea se știa foarte puțin despre punerea în scenă și clasificarea embrionilor umani, ceea ce înseamnă că descrierile embrionului uman din Coran nu se pot baza pe cunoștințele științifice din secolul al VII-lea, ci pe o revelație de la Dumnezeu.

### Faptul #3 - Expansiunea Universului

Într-o perioadă în care știința astronomiei era încă primitivă, a fost dezvăluit următorul verset din Coran:

{{ $page->verse('51:47') }}

Faptul că universul se extinde (de exemplu, planetele se îndepărtează mai mult una de cealaltă) a fost descoperit în ultimul secol. Fizicianul Stephen Hawking în cartea sa „Scurta istorie a timpului” scrie: „Descoperirea că universul se extinde a fost una dintre marile revoluții intelectuale ale secolului al XX-lea”.

Coranul face aluzie la expansiunea universului chiar înainte de inventarea telescopului!

### Faptul #4 - Fierul trimis pe Pământ

Fierul nu este natural planetei Pământ, fiindcă a venit pe această planetă din spațiul cosmic. Oamenii de știință au descoperit că în urmă cu miliarde de ani, Pământul a fost lovit de meteoriți care transportau fier de la stele îndepărtate care explodaseră.

{{ $page->verse('..57:25..') }}

Dumnezeu folosește cuvântul „pogorât” - faptul că fierul a fost trimis pe pământ din spațiul cosmic este ceva ce nu putea fi cunoscut de știința primitivă a secolului al VII-lea.

### Faptul #5 - Protecția Cerului

Cerul joacă un rol crucial în protejarea pământului și a locuitorilor săi de razele letale ale soarelui, precum și de frigul înghețat al spațiului.

Dumnezeu ne cere să luăm în considerare următorul verset:

{{ $page->verse('21:32') }}

Coranul face referire la protecția cerului ca un semn al lui Dumnezeu. Proprietățile protectoare au fost descoperite de cercetările științifice efectuate abia în secolul al XX-lea.

### Faptul #6 - Munții

Dumnezeu ne atrage atenția asupra unei caracteristici importante a munților:

{{ $page->verse('78:6-7') }}

Coranul descrie cu acuratețe rădăcinile adânci ale munților folosind cuvântul „ţăruşi”. Muntele Everest, de exemplu, are o înălțime de aproximativ 9 km deasupra solului, în timp ce rădăcina lui (sub pământ) este de peste 125 km!

Faptul că munții au rădăcini adânci asemănătoare ţăruşilor nu a fost cunoscut decât după dezvoltarea teoriei plăcilor tectonice, la începutul secolului al XX-lea. Dumnezeu mai spune în Coran (16:15), că munții au un rol în stabilizarea pământului „... ca să nu se cutremure”, lucru care a fost observat de oamenii de știință.

### Faptul #7 - Orbita Soarelui

În 1512, astronomul Nicolae Copernic și-a prezentat teoria potrivit căreia Soarele este nemișcat în centrul sistemului solar și planetele se învârt în jurul lui. Această convingere a fost larg răspândită printre astronomi până în secolul al XX-lea. Acum este un fapt bine stabilit că Soarele nu este staționar, ci se mișcă pe o orbită în jurul galaxiei noastre, Calea Lactee.

{{ $page->verse('21:33') }}

### Faptul #8 - Valurile interne în ocean

Se credea în mod obișnuit că valurile apar doar la suprafața oceanului. Cu toate acestea, oceanografii au descoperit că există unde interne care au loc în adâncime, sunt invizibile pentru ochiul uman și pot fi detectate doar de echipamente specializate.

Coranul menționează:

{{ $page->verse('24:40..') }}

Această descriere este uimitoare, deoarece acum 1400 de ani nu exista niciun echipament specializat pentru a descoperi undele interne adânci din oceane.

### Faptul #9 - Minciună și mișcare

A existat un lider tribal crud și opresiv care a trăit în timpul profetului Muhammad {{ $page->pbuh() }}. Dumnezeu a dezvăluit un verset pentru a-l avertiza:

{{ $page->verse('96:15-16') }}

Dumnezeu nu numește această persoană mincinoasă, ci îi numește fruntea (partea din față a creierului) „mincinoasă” și „păcătoasă” și îl avertizează să se oprească. Numeroase studii au descoperit că partea din față a creierului nostru (lobul frontal) este responsabilă atât pentru minciună, cât și pentru mișcarea voluntară și, prin urmare, pentru păcat. Aceste funcții au fost descoperite folosind echipamente de imagistică medicală care au fost dezvoltate în secolul al XX-lea.

### Faptul #10 - Cele două mări care nu se amestecă

Cu privire la mări, Creatorul nostru spune:

{{ $page->verse('55:19-20') }}

O forță fizică numită tensiune superficială împiedică amestecarea apelor mărilor vecine, din cauza diferenței de densitate a acestor ape. Este ca și cum un zid subțire ar fi între ele. Acest lucru a fost descoperit abia recent de oceanografi.

### Nu ar fi putut Muhammad să fi fost autorul Coranului?

Profetul Muhammad {{ $page->pbuh() }} era cunoscut în istorie ca fiind analfabet; nu știa să citească și nici să scrie și nu a fost educat în niciun domeniu care ar putea explica exactitatea științifică din Coran.

Unii ar putea pretinde că a copiat-o de la oameni învățați sau oameni de știință ai timpului său. Dacă ar fi fost copiat, ne-am fi așteptat să vedem și toate ipotezele științifice incorecte ale vremii copiate. Mai degrabă, constatăm că Coranul nu conține niciun fel de erori – fie ele științifice sau de altă natură.

Unii oameni pot pretinde, de asemenea, că Coranul a fost schimbat pe măsură ce au fost descoperite noi fapte științifice. Acest lucru nu este adevărat, deoarece este documentat istoric faptul că Coranul a fost păstrat de-a lungul secolelor în limba sa originală - ceea ce este un miracol în sine.

### Doar o coincidență?

{{ $page->verse('41:53') }}

Deşi acest răspuns s-a concentrat doar pe miracolele științifice, există mai multe tipuri de miracole menționate în Coran: miracole istorice; profeții care s-au adeverit; stiluri lingvistice și literare care nu pot fi egalate; ca să nu mai vorbim de efectul pe care îl are asupra oamenilor. Toate aceste miracole nu pot fi o coincidență. Ele indică în mod clar că Coranul este de la Dumnezeu, Creatorul tuturor acestor legi ale științei. El este singurul Dumnezeu care i-a trimis pe toți profeții cu același mesaj - să se închine singurului Dumnezeu și să urmeze învățăturile Trimisului Său.

Coranul este o carte de îndrumare care demonstrează că Dumnezeu nu a creat oamenii pentru a rătăci pur și simplu fără țintă. Mai degrabă, ne învață că avem un scop semnificativ și mai înalt în viață - să recunoaștem perfecțiunea completă, măreția și unicitatea lui Dumnezeu și să ne închinăm Lui.

Depinde de fiecare persoană să-și folosească intelectul și abilitățile de raționament date de Dumnezeu pentru a contempla și a recunoaște semnele lui Dumnezeu - Coranul fiind cel mai important semn. Citiți și descoperiți frumusețea și adevărul Coranului, astfel încât să puteți avea succes!
