# Islam Answers

[![pipeline status](https://gitlab.com/islam-answers/website/badges/main/pipeline.svg)](https://gitlab.com/islam-answers/website/-/commits/main)

Islam Answers aims to answer the most common and controversial questions that people of other faiths have about Islam. After researching the questions with trending Google searches, we have created this website to provide clear answers from reliable sources, backed by evidence from the Quran and sunnah (the words, actions and teachings of prophet Muhammad, peace be upon him).

Our aim is to make these answers available in as many languages as possible, in order to reach the highest number of people interested to learn more about Islam. Given the recent events in Palestine and the increased interest that non-Muslims have to find out more about Islam, we believe that this website will be a valuable resource.

Our website is constantly growing in terms of the number of questions we answer, as well as the number of languages we translate them into.

We are not affiliated with any organisation, nor are we funded by any organisation. We are a group of volunteers who believe that the world needs to know more about Islam, and we are doing our part to make that happen.

If you have any questions or if you would like to contribute to the website (as a translator or content writer), please reach out to us at [contact at islam-answers.com](mailto:contact@islam-answers.com).

May Allah guide us all to the truth. Ameen.

## Credits

This website is built using [Jigsaw](https://jigsaw.tighten.co/), a static site generator for Laravel developers. The site is hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Localization was made possible by the help of [Lokalise](https://lokalise.com/?ref=islam-answers.com), a one-stop solution for AI-powered translations and automated localization. Lokalise have generously provided us with an Enterprise plan to help us scale our project to many languages.

GitLab is the most comprehensive AI-powered DevSecOps Platform. GitLab generously provides us with an Ultimate plan to host our project and run CI/CD pipelines.
