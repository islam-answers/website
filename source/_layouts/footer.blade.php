@php
$popularTopics = [
    ['query' => 'jesus', 'text' => 'Jesus'],
    ['query' => 'quran', 'text' => 'Quran'],
    ['query' => 'muhammad', 'text' => 'Muhammad'],
    ['query' => 'allah', 'text' => 'Allah'],
    ['query' => 'god', 'text' => 'God'],
    ['query' => 'arab', 'text' => 'Arab'],
    ['query' => 'ramadan', 'text' => 'Ramadan'],
    ['query' => 'conver', 'text' => 'Conversion'],
    ['query' => 'hijab', 'text' => 'Hijab'],
    ['query' => 'women', 'text' => 'Women'],
    ['query' => 'pray', 'text' => 'Prayer'],
    ['query' => 'fast', 'text' => 'Fasting'],
    ['query' => 'hajj', 'text' => 'Hajj'],
    ['query' => 'mary', 'text' => 'Virgin Mary'],
    ['query' => 'jihad', 'text' => 'Jihad'],
    ['query' => 'terrorism', 'text' => 'Terrorism'],
];

$popularQuestions = [
    ['slug' => 'was-jesus-crucified', 'text' => 'Was Jesus crucified?'],
    ['slug' => 'who-wrote-the-quran', 'text' => 'Who wrote the Quran?'],
    ['slug' => 'who-is-the-prophet-muhammad', 'text' => 'Who is the Prophet Muhammad?'],
    ['slug' => 'what-does-bismillah-mean', 'text' => 'What does Bismillah mean?'],
    ['slug' => 'why-do-muslims-fast', 'text' => 'Why do Muslims fast?'],
    ['slug' => 'what-is-shariah-law', 'text' => 'What is Shariah Law?'],
];
@endphp

<footer class="bg-white shadow text-sm mt-12 py-8 relative" role="contentinfo">
    <div class="container mx-auto px-4 lg:px-8 max-w-4xl mb-8">
        <div class="grid grid-cols-1 md:grid-cols-2 gap-8">
            <div class="text-center md:text-left">
                <h3 class="font-semibold text-lg mb-4">Popular Topics</h3>
                <div class="flex flex-wrap gap-2 justify-center md:justify-start">
                    @foreach($popularTopics as $topic)
                        <a href="/answers/en/?q={{ $topic['query'] }}" class="text-sm bg-blue-100 hover:bg-blue-200 text-blue-800 px-3 py-1 rounded-full">{{ $topic['text'] }}</a>
                    @endforeach
                </div>
            </div>
            <div class="text-center md:text-left">
                <h3 class="font-semibold text-lg mb-4">Popular Questions</h3>
                <ul class="space-y-2 list-none">
                    @foreach($popularQuestions as $question)
                        <li><a href="/answers/en/{{ $question['slug'] }}/" class="text-blue-600 hover:text-blue-800">{{ $question['text'] }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="absolute inset-0 overflow-hidden pointer-events-none opacity-5">
        <svg class="absolute bottom-0 left-0 animate-wave" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <defs>
                <linearGradient id="footer-gradient" x1="0%" y1="0%" x2="100%" y2="0%">
                    <stop offset="0%" style="stop-color:#1e40af;stop-opacity:1">
                        <animate attributeName="stop-color" values="#1e40af;#f1d302;#1e40af" dur="10s" repeatCount="indefinite"/>
                    </stop>
                    <stop offset="100%" style="stop-color:#f1d302;stop-opacity:1">
                        <animate attributeName="stop-color" values="#f1d302;#1e40af;#f1d302" dur="10s" repeatCount="indefinite"/>
                    </stop>
                </linearGradient>
            </defs>
            <path fill="url(#footer-gradient)" fill-opacity="1" d="M0,64L48,85.3C96,107,192,149,288,160C384,171,480,149,576,144C672,139,768,149,864,154.7C960,160,1056,160,1152,144C1248,128,1344,96,1392,80L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
                <animate attributeName="d"
                    dur="20s"
                    repeatCount="indefinite"
                    values="M0,64L48,85.3C96,107,192,149,288,160C384,171,480,149,576,144C672,139,768,149,864,154.7C960,160,1056,160,1152,144C1248,128,1344,96,1392,80L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z;
                    M0,96L48,112C96,128,192,160,288,176C384,192,480,192,576,176C672,160,768,128,864,128C960,128,1056,160,1152,176C1248,192,1344,192,1392,192L1440,192L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z;
                    M0,64L48,85.3C96,107,192,149,288,160C384,171,480,149,576,144C672,139,768,149,864,154.7C960,160,1056,160,1152,144C1248,128,1344,96,1392,80L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"/>
            </path>
        </svg>
    </div>
    <div class="container mx-auto px-4 lg:px-8 max-w-4xl">
        <div class="grid grid-cols-1 md:grid-cols-3 gap-8">
            <!-- Column 1: Logo & Website -->
            <div class="text-center md:text-left order-3 md:order-1 mx-auto">
                @include('_components.logo', ['class' => 'h-8 mb-2 mx-auto md:mx-0'])
                <h3 class="font-semibold text-lg mb-2">Islam Answers</h3>
                <p class="text-gray-600 text-sm">
                    Powered by
                    <a href="https://lokalise.com?ref=islam-answers.com" title="Lokalise" target="_blank" class="text-blue-600 hover:text-blue-800">
                        Lokalise Enterprise
                    </a>
                    <br class="hidden md:block" />
                    and
                    <a href="https://about.gitlab.com/pricing/ultimate/?ref=islam-answers.com" title="GitLab" target="_blank" class="text-blue-600 hover:text-blue-800">
                        GitLab Ultimate
                    </a>
                </p>
            </div>

            <!-- Column 2: About Us -->
            <div class="text-center md:text-left order-2 mx-auto">
                <h3 class="font-semibold text-lg mb-4">Navigate</h3>
                <ul class="space-y-2 list-none">
                    <li><a href="/about/" class="text-blue-600 hover:text-blue-800">Our mission</a></li>
                    <li><a href="/join/" class="text-blue-600 hover:text-blue-800">Join the team</a></li>
                    <li><a href="/feedback/" class="text-blue-600 hover:text-blue-800">Send feedback</a></li>
                    <li><a href="https://quran.com" class="text-blue-600 hover:text-blue-800" target="_blank">Quran.com</a></li>
                    <li><a href="https://sunnah.com" class="text-blue-600 hover:text-blue-800" target="_blank">Sunnah.com</a></li>
                </ul>
            </div>

            <!-- Column 3: Connect -->
            <div class="text-center md:text-left order-1 md:order-3 mx-auto">
                <h3 class="font-semibold text-lg mb-4">Connect</h3>
                <ul class="space-y-2 list-none">
                    <li>
                        <a href="https://medium.com/@islamanswers" class="text-blue-600 hover:text-blue-800 flex items-center justify-center md:justify-start" target="_blank">
                            <svg class="w-5 h-5 mr-2" fill="currentColor" viewBox="0 0 24 24"><path d="M13.54 12a6.8 6.8 0 01-6.77 6.82A6.8 6.8 0 010 12a6.8 6.8 0 016.77-6.82A6.8 6.8 0 0113.54 12zm7.42 0c0 3.54-1.51 6.42-3.38 6.42-1.87 0-3.39-2.88-3.39-6.42s1.52-6.42 3.39-6.42 3.38 2.88 3.38 6.42M24 12c0 3.17-.53 5.75-1.19 5.75-.66 0-1.19-2.58-1.19-5.75s.53-5.75 1.19-5.75C23.47 6.25 24 8.83 24 12z"/></svg>
                            Medium
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/islam-answers/" class="text-blue-600 hover:text-blue-800 flex items-center justify-center md:justify-start" target="_blank">
                            <svg class="w-5 h-5 mr-2" fill="currentColor" viewBox="0 0 24 24"><path d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z"/></svg>
                            LinkedIn
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/islamanswers1445" class="text-blue-600 hover:text-blue-800 flex items-center justify-center md:justify-start" target="_blank">
                            <svg class="w-5 h-5 mr-2" fill="currentColor" viewBox="0 0 24 24"><path d="M24 12.073c0-6.627-5.373-12-12-12s-12 5.373-12 12c0 5.99 4.388 10.954 10.125 11.854v-8.385H7.078v-3.47h3.047V9.43c0-3.007 1.792-4.669 4.533-4.669 1.312 0 2.686.235 2.686.235v2.953H15.83c-1.491 0-1.956.925-1.956 1.874v2.25h3.328l-.532 3.47h-2.796v8.385C19.612 23.027 24 18.062 24 12.073z"/></svg>
                            Facebook
                        </a>
                    </li>
                    <li>
                        <a href="mailto:contact@islam-answers.com" class="text-blue-600 hover:text-blue-800 flex items-center justify-center md:justify-start">
                            <svg class="w-5 h-5 mr-2" fill="currentColor" viewBox="0 0 24 24"><path d="M20 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"/></svg>
                            Email
                        </a>
                    </li>
                    <li>
                        <a href="/answers/feed.atom" class="text-blue-600 hover:text-blue-800 flex items-center justify-center md:justify-start">
                            <svg class="w-5 h-5 mr-2" fill="currentColor" viewBox="0 0 24 24"><path d="M6.503 20.752c0 1.794-1.456 3.248-3.251 3.248-1.796 0-3.252-1.454-3.252-3.248 0-1.794 1.456-3.248 3.252-3.248 1.795.001 3.251 1.454 3.251 3.248zm-6.503-12.572v4.811c6.05.062 10.96 4.966 11.022 11.009h4.817c-.062-8.71-7.118-15.758-15.839-15.82zm0-3.368c10.58.046 19.152 8.594 19.183 19.188h4.817c-.03-13.231-10.755-23.954-24-24v4.812z"/></svg>
                            RSS Feed
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="mt-8 text-xs text-gray-500 text-center">
            <a href="/legal/" class="hover:text-gray-700">Privacy Policy</a> ·
            <a href="https://gitlab.com/islam-answers" class="hover:text-gray-700" target="_blank">Source code</a>
        </div>
    </footer>
