---
extends: _layouts.answer
section: content
title: Co oznacza Subhanallah?
date: 2024-10-28
description: Zwrot ten oznacza „Chwała Allahowi” i w Koranie służy do zanegowania wad przypisywanych Allahowi oraz potwierdzenia Jego doskonałości.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 584496
---

Arabski termin „tasbih” zwykle odnosi się do zwrotu „SubhanAllah”. Jest to zwrot, który Allah zatwierdził dla Siebie, który zainspirował Swoich aniołów do wypowiadania i który poprowadził najwspanialsze ze Swoich stworzeń do ogłaszania. „SubhanAllah” jest powszechnie tłumaczone jako „Chwała Allahowi”, jednak to tłumaczenie nie oddaje w pełni znaczenia tasbih.

Tasbih składa się z dwóch słów: Subhana i Allah. Z językowego punktu widzenia słowo „subhana” pochodzi od słowa „sabh”, które oznacza odległość, oddalenie. W związku z tym Ibn 'Abbas wyjaśnił to wyrażenie jako czystość i rozgrzeszenie Allaha ponad każdym złem lub nieodpowiednią sprawą. Innymi słowy, Allah jest wiecznie daleko i wiecznie wysoko ponad każdym złym uczynkiem, brakiem, niedociągnięciem i niewłaściwością.

Fraza ta pojawia się w Koranie, aby zanegować niewłaściwe i niezgodne z prawdą opisy przypisywane Allahowi:

{{ $page->verse('23:91') }}

Jeden tasbih wystarczy, aby zanegować każdą wadę lub kłamstwo przypisywane Allahowi w każdym miejscu i każdym czasie przez jakiekolwiek, lub wszystkie stworzenia. Tak więc Allah obala liczne kłamstwa przypisywane Jemu jednym tasbih:

{{ $page->verse('37:149-159') }}

Celem negacji jest potwierdzenie jej przeciwieństwa. Na przykład, gdy Koran neguje ignorancję Allaha, to muzułmanin neguje ignorancję i z kolei potwierdza przeciwieństwo Allaha, które jest wszechogarniającą wiedzą. Tasbih jest zasadniczo negacją (braków, wykroczeń i fałszywych stwierdzeń), a zatem zgodnie z tą zasadą, pociąga za sobą potwierdzenie przeciwieństwa, którym jest piękne istnienie, doskonałe postępowanie i najwyższa prawda.

Sa'd bin Abu Waqqas (niech Allah będzie z niego zadowolony) przekazał:

{{ $page->hadith('muslim:2698') }}
