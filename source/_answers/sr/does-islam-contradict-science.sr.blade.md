---
extends: _layouts.answer
section: content
title: Da li je islam u suprotnosti sa naukom?
date: 2024-08-07
description: Kur'an sadrži naučne činjenice koje su otkrivene tek nedavno kroz napredak
  tehnologije i naučnog znanja.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 584497
---

Kur'an, knjiga islama, je poslednja objava od Boga čovečanstvu i poslednja u nizu objava datih prorocima.

Iako Kur'an (otkriven pre više od 1400 godina) nije prvenstveno knjiga nauke, on sadrži naučne činjenice koje su otkrivene tek nedavno kroz napredak tehnologije i naučnog znanja. Islam podstiče razmišljanje i naučno istraživanje, jer razumevanje prirode stvaranja omogućava ljudima da dalje cene svog Stvoritelja i stepen Njegove moći i mudrosti.

Kur'an je objavljen u vreme kada je nauka bila primitivna; Nije bilo teleskopa, mikroskopa ili bilo čega sličnog današnjoj tehnologiji. Ljudi su verovali da sunce kruži oko Zemlje, a nebo su držali stubovi na uglovima ravne zemlje. U tom kontekstu otkriven je Kur'an, koji sadrži mnoge naučne činjenice o temama počevši od astronomije pa sve do biologije, preko geologije i zoologije.

Neke od mnogih naučnih činjenica koje se nalaze u Kur'anu uključuju:

### Činjenica #1 - Poreklo života

{{ $page->verse('21:30') }}

Voda je istaknuta kao poreklo sveg života. Sva živa bića su napravljena od ćelija i sada znamo da su ćelije uglavnom napravljene od vode. Ovo je otkriveno tek nakon pronalaska mikroskopa. U pustinjama Arabije, bilo bi nezamislivo pomisliti da bi neko pretpostavio da sav život potiče iz vode.

### Činjenica #2 - Ljudski embrionalni razvoj

Bog govori o fazama čovekovog embrionalnog razvoja:

{{ $page->verse('23:12-14') }}

Arapska reč "alaka" ima tri značenja: pijavica, suspendovana stvar i krvni ugrušak. "Mudghah" znači sažvakana supstanca. Naučnici embriologije su primetili da je upotreba ovih termina u opisivanju formiranja embriona tačna i u skladu sa našim trenutnim naučnim razumevanjem procesa razvoja.

Malo se znalo o postavljanju i klasifikaciji ljudskih embriona sve do dvadesetog veka, što znači da se opisi ljudskog embriona u Kur'anu ne mogu zasnivati na naučnim saznanjima iz sedmog veka.

### Činjenica #3 - Širenje univerzuma

U vreme kada je astronomska nauka još uvek bila primitivna, objavljen je sledeći ajet u Kur'anu:

{{ $page->verse('51:47') }}

Jedno od predviđenih značenja gornjeg ajeta je da Bog širi univerzum (tj. nebesa). Druga značenja su da Bog obezbeđuje i ima moć nad univerzumom - što je takođe tačno.

Činjenica da se univerzum širi (npr. Planete se udaljavaju jedna od druge) otkrivena je u prošlom veku. Fizičar Stiven Hoking u svojoj knjizi "Kratka istorija vremena" piše: "Otkriće da se univerzum širi bila je jedna od velikih intelektualnih revolucija dvadesetog veka."

Kur'an aludira na širenje univerzuma čak i pre pronalaska teleskopa!

### Činjenica #4 - Gvožđe poslano dole

Gvožđe nije prirodno za Zemlju, jer je na ovu planetu došlo iz svemira. Naučnici su otkrili da su pre nekoliko milijardi godina Zemlju pogodili meteoriti koji su nosili gvožđe iz udaljenih zvezda koje su eksplodirale.

{{ $page->verse('..57:25..') }}

Bog koristi reči "poslano dole". Činjenica da je gvožđe poslato na Zemlju iz svemira je nešto što nije moglo biti poznato primitivnoj nauci sedmog veka.

### Činjenica #5 - Zaštita neba

Nebo igra ključnu ulogu u zaštiti zemlje i njenih stanovnika od smrtonosnih sunčevih zraka, kao i ledene hladnoće svemira.

Bog traži od nas da razmotrimo nebo u sledećem ajetu:

{{ $page->verse('21:32') }}

Kur'an ukazuje na zaštitu neba kao znak Boga, zaštitna svojstva koja su otkrivena naučnim istraživanjima sprovedenim u dvadesetom veku.

### Činjenica #6 - Planine

Bog nam skreće pažnju na važnu karakteristiku planina:

{{ $page->verse('78:6-7') }}

Kur'an tačno opisuje duboke korene planina koristeći reč "stupovi". Mount Everest, na primer, ima približnu visinu od 9 km iznad zemlje, dok je njegov koren dublji od 125 km!

Činjenica da planine imaju duboke korene nalik na "stub" nije bila poznata sve do razvoja teorije tektonskih ploča početkom dvadesetog veka. Bog takođe kaže u Kur'anu (16:15), da planine imaju ulogu u stabilizaciji zemlje "... tako da se ne bi tresla", što su naučnici tek počeli da shvataju.

### Činjenica #7 - Sunčeva orbita

Godine 1512. astronom Nikolaj Kopernik izneo je svoju teoriju da je Sunce nepomično u centru Sunčevog sistema i da se planete okreću oko njega. Ovo verovanje je bilo široko rasprostranjeno među astronomima sve do dvadesetog veka. Sada je dobro utvrđena činjenica da Sunce nije stacionarno, već se kreće u orbiti oko centra naše galaksije Mlečni put.

{{ $page->verse('21:33') }}

### Činjenica #8 - Unutrašnji talasi u okeanu

Obično se mislilo da se talasi javljaju samo na površini okeana. Međutim, okeanografi su otkrili da postoje unutrašnji talasi koji se odvijaju ispod površine koji su nevidljivi ljudskom oku i mogu se detektovati samo specijalizovanom opremom.

Kur'an pominje:

{{ $page->verse('24:40..') }}

Ovaj opis je neverovatan jer pre 1400 godina nije postojala specijalna oprema za otkrivanje unutrašnjih talasa duboko u okeanima.

### Činjenica #9 - Laganje i pokret

Postojao je okrutni opresivni plemenski vođa koji je živeo u vreme proroka Muhameda {{ $page->pbuh() }}. Bog je objavio ajet da ga upozori:

{{ $page->verse('96:15-16') }}

Bog ne naziva ovu osobu lažovom, već naziva njeno čelo (prednji deo mozga) "lažljivim" i "grešnim", i upozorava ga da prestane. Brojne studije su otkrile da je prednji deo našeg mozga (frontalni režanj) odgovoran i za laganje i za dobrovoljno kretanje, a samim tim i za greh. Ove funkcije su otkrivene sa medicinskom opremom za snimanje koja je razvijena u dvadesetom veku.

### Činjenica #10 - Dva mora koja se ne mešaju

Što se tiče mora, naš Stvoritelj kaže:

{{ $page->verse('55:19-20') }}

Fizička sila koja se zove površinski napon sprečava mešanje voda susednih mora, zbog razlike u gustini ovih voda. Kao da je između njih tanak zid. Ovo su tek nedavno otkrili okeanografi.

### Zar Muhamed nije mogao da napiše Kur'an?

Poslanik Muhamed {{ $page->pbuh() }} je u istoriji bio poznat kao nepismen; nije mogao da čita ni da piše i nije bio obrazovan ni u jednoj oblasti koja bi mogla da objasni naučne tačnosti u Kur'anu.

Neki mogu tvrditi da ga je kopirao od učenih ljudi ili naučnika svog vremena. Ako je kopiran, očekivali bismo da vidimo sve netačne naučne pretpostavke tog vremena koje se takođe kopiraju. Umesto toga, nalazimo da Kur'an ne sadrži nikakve greške - bilo da su naučne ili druge.

Neki ljudi takođe mogu tvrditi da je Kur'an promenjen kako su otkrivene nove naučne činjenice. To ne može biti slučaj jer je istorijski dokumentovana činjenica da je Kur'an sačuvan na svom izvornom jeziku - što je samo po sebi čudo.

### Samo slučajnost?

{{ $page->verse('41:53') }}

Iako se ovaj odgovor fokusirao samo na naučna čuda, postoji mnogo više vrsta čuda koje se pominju u Kur'anu: istorijska čuda; proročanstva koja su se obistinila; lingvističke i književne stilove koji se ne mogu podudarati; Da ne pominjemo motivirajuci efekat koji ima na ljude. Sva ova čuda ne mogu biti zbog slučajnosti. Oni jasno ukazuju na to da je Kur'an od Boga, Stvoritelja svih ovih zakona nauke. On je jedan te isti Bog koji je poslao sve proroke sa istom porukom - da obožavaju samo jednog Boga i da slede učenja Njegovog Poslanika.

Kur'an je knjiga uputstava koja pokazuje da Bog nije stvorio ljude da jednostavno lutaju besciljno. Umesto toga, uči nas da imamo smislenu i višu svrhu u životu - da priznamo Božiju potpunu savršenost, veličinu i jedinstvenost, i da mu se pokoravamo.

Na svakoj osobi je da koristi svoj Bogom dat intelekt i sposobnosti rasuđivanja da razmišlja i prepozna Božije znakove - Kur'an je najvažniji znak. Pročitajte i otkrijte lepotu i istinu Kurana, kako biste postigli uspeh!
