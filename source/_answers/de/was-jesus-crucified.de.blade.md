---
extends: _layouts.answer
section: content
title: Wurde Jesus gekreuzigt?
date: 2024-10-01
description: Laut dem Quran wurde Jesus nicht getötet oder gekreuzigt. Allah erhob ihn lebendig in den Himmel, und ein anderer Mann wurde an seiner Stelle gekreuzigt.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 587746
---

Als die Juden und der römische Kaiser planten, Jesus {{ $page->pbuh() }} zu töten, ließ Allah einen der Anwesenden Jesus in jeder Hinsicht ähneln. Also töteten sie den Mann, der Jesus ähnlich sah. Sie töteten Jesus nicht. Jesus {{ $page->pbuh() }} wurde lebendig in den Himmel erhoben. Der Beweis dafür sind die Worte Allahs bezüglich der Lügen der Juden und deren Widerlegung:

{{ $page->verse('4:157-158') }}

So erklärte Allah die Worte der Juden für falsch, als sie behaupteten, sie hätten ihn getötet und gekreuzigt, und Er verkündete, dass Er ihn zu Sich erhoben habe. Dies war eine Barmherzigkeit von Allah für Jesus (Isa) {{ $page->pbuh() }}, und eine Ehre für ihn, damit er eines Seiner Zeichen sei, eine Ehre, die Allah jenen Seiner Gesandten verleiht, die Er will.

Die Implikation der Worte „Aber Allah hat ihn (Jesus) mit Leib und Seele zu Sich erhoben“ ist, dass Allah, gepriesen sei Er, Jesus mit Körper und Seele zu Sich erhoben hat, um die Behauptung der Juden zu widerlegen, sie hätten ihn gekreuzigt und getötet, da Töten und Kreuzigung mit dem physischen Körper zu tun haben. Wenn Er nur seine Seele emporgehoben hätte, würde das die Behauptung, ihn getötet und gekreuzigt zu haben, nicht widerlegen, da die Erhebung nur der Seele ihre Aussage nicht entkräftet hätte. Außerdem bezieht sich der Name Jesus, {{ $page->pbuh() }}, tatsächlich auf die Einheit von Körper und Seele; er kann sich nicht nur auf eines von beidem beziehen, es sei denn, es gibt einen Hinweis darauf. Darüber hinaus zeigt die Erhebung von Seele und Körper zugleich die vollkommene Macht und Weisheit Allahs sowie Seine Ehre und Unterstützung für diejenigen Seiner Gesandten, die Er will, wie Er, erhaben sei Er, am Ende des Verses sagt: „Und Allah ist Allmächtig, Allweise.“
