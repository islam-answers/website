---
extends: _layouts.answer
section: content
title: Qual è la condizione della donna nell'Islam?
date: 2024-05-25
description: Le donne musulmane hanno il diritto di possedere proprietà, guadagnare
  denaro e spenderlo come desiderano.
sources:
- href: https://www.islam-guide.com/it/ch3-13.htm
  title: islam-guide.com
---

L'Islam vede la donna, sola o sposata, come un individuo con pieni diritti, con il diritto di possedere e disporre delle sue proprietà e guadagnare senza tutela (se non quella di suo padre, marito o qualsiasi altra persona). Ha il diritto di vendere e comprare, di fare doni e la carità e può spendere il suo denaro come crede. La dote è fornita dallo sposo alla sposa per suo uso personale e essa mantiene il nome della sua famiglia piuttosto che prendere quello del marito.

L'Islam incoraggia il marito nel trattare bene sua moglie, come dice il profeta Mohammed {{ $page->pbuh() }}:

{{ $page->hadith('ibnmajah:1977') }}

Nell'Islam la figura materna è molto rispettata. L'Islam raccomanda di trattarla nel miglior modo.

{{ $page->hadith('bukhari:5971') }}
