---
extends: _layouts.answer
section: content
title: Kto napisał Koran?
date: 2024-11-10
description: Koran to objawione słowo Boga, które zostało przekazane Prorokowi Mahometowi
  (pokój niech będzie z nim) przez Anioła Gabriela.
sources:
- href: https://www.islam-guide.com/ch1-1.htm
  title: islam-guide.com
translator_id: 584496
---

Bóg wspierał swojego ostatniego Proroka Mahometa {{ $page->pbuh() }} wieloma cudami i licznymi dowodami, które potwierdzały, że jest on prawdziwym Prorokiem wysłanym przez Boga. Ponadto Bóg wspierał swoją ostatnią objawioną księgę, Święty Koran, wieloma cudami, które dowodzą, że ten Koran jest dosłownym słowem Boga, objawionym przez Niego, i że nie został napisany przez żadnego człowieka.

Koran to objawione słowo Boga, które zostało przekazane Prorokowi Mahometowi {{ $page->pbuh() }} przez Anioła Gabriela. Prorok Mahomet {{ $page->pbuh() }} zapamiętał je, a następnie podyktował swoim Towarzyszom. Oni z kolei zapamiętali, zapisali i przeglądali je z Prorokiem Mahometem {{ $page->pbuh() }}. Dodatkowo Prorok Mahomet {{ $page->pbuh() }} przeglądał Koran z Aniołem Gabrielem raz w roku i dwa razy w ostatnim roku swojego życia. Od momentu objawienia Koranu aż do dziś, zawsze istniała ogromna liczba muzułmanów, którzy zapamiętali cały Koran, litera po literze. Niektórzy z nich byli w stanie zapamiętać cały Koran już w wieku dziesięciu lat. Żadna litera Koranu nie została zmieniona przez wieki.

Koran, który został objawiony czternaście wieków temu, wspomina fakty odkryte lub udowodnione dopiero niedawno przez naukowców. To dowodzi bez wątpienia, że Koran musi być wiernym słowem Boga, objawionym przez Niego Prorokowi Muhammadowi {{ $page->pbuh() }}, i że Koranu nie napisał Prorok Muhammad {{ $page->pbuh() }} ani żaden inny człowiek. To również dowodzi, że Prorok Muhammad {{ $page->pbuh() }} jest prawdziwym prorokiem posłanym przez Boga. To ponad wszelką miarę, aby ktokolwiek czternaście wieków temu znał te fakty odkryte lub udowodnione dopiero niedawno za pomocą zaawansowanego sprzętu i wyrafinowanych metod naukowych.
