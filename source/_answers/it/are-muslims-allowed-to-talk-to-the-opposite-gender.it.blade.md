---
extends: _layouts.answer
section: content
title: I musulmani possono parlare con persone del sesso opposto?
old_titles:
- Ai musulmani è consentito parlare con persone del sesso opposto?
date: 2024-12-01
description: Nell'Islam, l'interazione con l'altro sesso è consentita in caso di necessità,
  ma deve essere limitata, breve e deve evitare ogni tentazione.
sources:
- href: https://islamqa.info/en/answers/27304/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1497/
  title: islamqa.info
- href: https://islamqa.info/en/answers/34841/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21183/
  title: islamqa.info
translator_id: 573107
---

Nell'Islam, l'interazione con il sesso opposto è consentita in caso di necessità, ma deve essere limitata, breve e deve evitare ogni tentazione.

### Il principio della prevenzione (evitare il pericolo)

Nell'Islam, tutto ciò che potrebbe portare una persona a cadere nell'illecito (haram) è anch'esso illecito, anche se in linea di principio sarebbe consentito. Questo è ciò che gli studiosi chiamano il principio di prevenzione del danno. Allah dice nel Corano:

{{ $page->verse('24:21..') }}

La conversazione tra uomini e donne, sia di persona, verbale o per iscritto, è di per sé lecita, ma può essere un modo per cadere nella tentazione (fitnah) preparata da Satana (Shaytaan).

### I pericoli dell'interazione con il sesso opposto

Non vi è dubbio che la fitnah (tentazione) delle donne sia grande. Il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2741') }}

Pertanto, il musulmano deve fare molta attenzione a questo tipo di fitnah, tenendosi lontano da qualsiasi cosa che possa farlo cadere in questa tentazione. Tra le cause maggiori di questa fitnah vi sono il guardare le donne e mescolarsi con loro.

Allah dice nel Corano:

{{ $page->verse('24:30-31') }}

In questo caso Allah ordina al Suo Profeta {{ $page->pbuh() }} di dire agli uomini e alle donne credenti di abbassare lo sguardo e di custodire la loro castità, spiegando che ciò è più puro per loro. Custodire la propria castità ed evitare azioni immorali si ottengono solo evitando le cause che conducono a tali azioni. Senza dubbio, lasciare che il proprio sguardo vaghi, lavorare in ambienti misti o frequentare altri luoghi in cui uomini e donne si mescolano liberamente, sono tra le azioni che conducono più facilmente all'immoralità.

Molte volte queste conversazioni hanno portato a conseguenze negative, facendo persino innamorare gli individui, e, in alcuni casi, portando ad azioni ancora più gravi di queste. Satana (Shaytaan) fa sì che ognuno di loro immagini qualità attraenti nell'altro, il che provoca lo sviluppo di un attaccamento che è dannoso per il loro benessere spirituale e per la vita terrena.

### Come ci si comporta quando si parla con il sesso opposto

Il mahram di una persona (tradotto in italiano come "parente non sposabile") è qualcuno che non è permesso sposare a causa della stretta parentela di sangue (per una donna si tratta dei suoi ascendenti, discendenti, fratelli, zii e nipoti), o a causa dell'allattamento al seno, o perché sono parenti acquisiti tramite matrimonio (per una donna si tratta degli ascendenti del marito, dei suoi patrigni, dei suoi discendenti e dei mariti delle sue figlie).

Parlare con una persona del sesso opposto con la quale non si ha alcun vincolo di parentela (cioè non-mahram) deve avvenire solo nel caso di qualche necessità specifica, come ad esempio fare una domanda, comprare o vendere, chiedere informazioni sul capofamiglia e così via. Tali conversazioni devono essere brevi e pertinenti, senza alcun dubbio su ciò che si dice o su come lo si dice. Non si deve permettere che la conversazione si allarghi troppo al di fuori dell'argomento in discussione. Non si devono porre domande personali che non hanno attinenza con l'argomento in discussione, come ad esempio l'età di una persona, la sua altezza, il luogo in cui vive, ecc.

L'Islam preclude tutte le vie che potrebbero condurre alla fitnah (tentazione), quindi proibisce l'utilizzo di un tono dolce e tenero nelle conversazioni, e non consente a un uomo di rimanere da solo con una donna non-mahram. Non devono usare tra loro un tono di voce dolce, o espressioni tenere e gentili. piuttosto devono parlare con il tono di voce normale che utilizzerebbero con chiunque altro. Allah, che Egli sia esaltato, dice, rivolgendosi alle Madri dei Credenti:

{{ $page->verse('..33:32') }}

Non è inoltre consentito ad un uomo di restare da solo con una donna che non sia la sua mahram, perché il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:1341') }}

La conversazione o la corrispondenza devono essere interrotte immediatamente se il cuore inizia a provare sentimenti di desiderio. Inoltre, bisogna evitare di scherzare, ridere o flirtare. Un altro comportamento da tenere nell'interazione con l'altro sesso consiste nell'evitare di fissare, cercando di abbassare lo sguardo il più possibile. Jarir bin 'Abdullah ha riferito:

{{ $page->hadith('muslim:2159') }}

Vale anche la pena di ricordare che non è consentito a un uomo che crede in Allah e nel Suo Messaggero {{ $page->pbuh() }} avere contatti fisici con un individuo del sesso opposto che non sia un mahram per lui.
