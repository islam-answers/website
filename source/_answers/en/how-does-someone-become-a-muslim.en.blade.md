---
extends: _layouts.answer
section: content
title: How does someone become a Muslim?
date: 2024-03-12
description: Simply by saying with conviction, “La ilaha illa Allah, Muhammadur rasoolu
  Allah,” one converts to Islam and becomes a Muslim.
sources:
- href: https://www.islam-guide.com/ch3-6.htm
  title: islam-guide.com
---

Simply by saying with conviction, “La ilaha illa Allah, Muhammadur rasoolu Allah,” one converts to Islam and becomes a Muslim. This saying means “There is no true god (deity) but God (Allah), and Muhammad is the Messenger (Prophet) of God.” The first part, “There is no true god but God,” means that none has the right to be worshipped but God alone, and that God has neither partner nor son. To be a Muslim, one should also:

- Believe that the Holy Quran is the literal word of God, revealed by Him.
- Believe that the Day of Judgment (the Day of Resurrection) is true and will come, as God promised in the Quran.
- Accept Islam as his or her religion.
- Not worship anything nor anyone except God.

The Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:2747') }}
