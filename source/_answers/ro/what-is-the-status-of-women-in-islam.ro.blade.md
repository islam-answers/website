---
extends: _layouts.answer
section: content
title: Care este statutul femeilor în Islam?
date: 2024-02-11
description: Femeile musulmane au multe drepturi acordate de Dumnezeu. De asemenea,
  Islamul încurajează bărbații să-și trateze soțiile bine și onorează foarte mult
  mamele.
sources:
- href: https://www.islam-guide.com/ch3-13.htm
  title: islam-guide.com
translator_id: 554943
---

Islamul vede femeia, fie că ea este singură sau căsătorită, ca pe un individ de sine stătător, cu dreptul de a deține și de a dispune de proprietatea și câștigurile ei fără nicio tutelă asupra ei (fie că este tatăl ei, soțul sau oricine altcineva). Ea are dreptul de a cumpăra și de a vinde, de a oferi cadouri și caritate și își poate cheltui banii după bunul plac. O zestre de căsătorie este dată de mire miresei pentru uzul ei personal, iar ea își păstrează propriul nume de familie în loc să-i ia numele soțului ei.

Islamul îl încurajează pe soț să-și trateze bine soția, așa cum a spus Profetul Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('ibnmajah:1977') }}

Mamele în Islam sunt foarte onorate. Islamul recomandă să le tratezi în cel mai bun mod.

{{ $page->hadith('bukhari:5971') }}
