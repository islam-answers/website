---
extends: _layouts.answer
section: content
title: Qu'est-ce que la Sunnah ?
date: 2025-01-06
description: La Sunnah, d’inspiration divine, complète le Coran en clarifiant et en
  élargissant ses règles pour la pratique islamique.
sources:
- href: https://islamqa.info/en/answers/77243/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145520/
  title: islamqa.info
translator_id: 586055
---

La Sunnah - qui désigne les paroles et les actes attribués au prophète Muhammad {{ $page->pbuh() }} ainsi que les actions qu'il a approuvées - est l'une des deux parties de la révélation divine (Wahy) qui a été révélée au messager d'Allah {{ $page->pbuh() }}. L'autre partie de la révélation est le Saint Coran. Allah dit :

{{ $page->verse('53:3-4') }}

Il a été rapporté que le Messager d'Allah {{ $page->pbuh() }} a dit :

{{ $page->hadith('ibnmajah:12') }}

Hassan ibn Atiyah a dit :

> Jibreel (l'ange Gabriel) avait l'habitude de faire descendre la sunnah sur le Prophète {{ $page->pbuh() }} comme il avait l'habitude de faire descendre sur lui le Coran.

L'importance de la Sounnah réside tout d'abord dans le fait qu'elle explique le Coran et le commente, puis qu'elle ajoute certaines règles à celles du Coran. Allah dit :

{{ $page->verse('..16:44') }}

Ibn 'Abd al-Barr a dit :

> Le commentaire du Prophète {{ $page->pbuh() }} sur le Coran est de deux types : 
> 
> 1. Expliquer des choses qui sont mentionnées en termes généraux dans le Saint Coran, telles que les cinq prières quotidiennes, leurs horaires, la prosternation, l'inclinaison et toutes les autres règles. 
> 1. Ajouter des règles aux règles du Coran, telles que l'interdiction d'être marié à une femme et à sa tante paternelle ou maternelle en même temps.

Le prophète Muhammad {{ $page->pbuh() }} a dit :

{{ $page->hadith('bukhari:3461') }}
