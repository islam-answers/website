---
extends: _layouts.answer
section: content
title: O que o Islam diz sobre o tratamento dos pais?
date: 2024-11-11
description: Mostrar bondade aos pais é um dos actos mais bem recompensados perante
  Deus. A ordem de sermos bons para os nossos pais é um ensinamento claro do Alcorão.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 594172
---

Mostrar bondade aos pais é um dos actos mais bem recompensados perante a Allah. A ordem de sermos bons para os nossos pais é um ensinamento claro do Alcorão. Allah diz:

{{ $page->verse('4:36..') }}

A menção da servidão aos pais segue-se imediatamente à servidão a Deus. Este facto repete-se em todo o Alcorão.

{{ $page->verse('17:23') }}

Neste versículo, Deus ordena-nos que O adoremos só a Ele e a mais ninguém e, imediatamente a seguir, chama a atenção para o comportamento correto com os pais, especialmente quando estes envelhecem e dependem de outros para se abrigarem e satisfazerem as suas necessidades. Os muçulmanos são ensinados a não serem rudes com os pais ou a repreendê-los, mas sim a falar-lhes com palavras amáveis e a mostrar amor e ternura.

No Alcorão, Allah pede ainda que se Lhe suplique pelos pais:

{{ $page->verse('17:24') }}

Allah sublinha que obedecer aos pais é obedecer-Lhe, exceto se Lhe associarem parceiros.

{{ $page->verse('31:14-15..') }}

Este versículo sublinha que todos os pais merecem um bom tratamento. As mães devem ser objeto de uma atenção especial, devido às dificuldades adicionais que suportam.

Cuidar dos pais é considerado uma das melhores acções:

{{ $page->hadith('muslim:85e') }}

Em conclusão, no Islam, honrar os pais significa obedecer-lhes, respeitá-los, rezar por eles, baixar a voz na sua presença, sorrir-lhes, não lhes mostrar desagrado, esforçar-se por servi-los, satisfazer os seus desejos, consultá-los e ouvir o que dizem.
