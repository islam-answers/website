---
extends: _layouts.answer
section: content
title: Chi è il profeta Mohammed?
date: 2024-04-04
description: Il Profeta Mohammed (pace su di lui) fu un esempio perfetto di essere
  umano onesto, giusto, misericordioso, compassionevole, fedele e coraggioso.
sources:
- href: https://www.islam-guide.com/it/ch3-8.htm
  title: islam-guide.com
---

Mohammed {{ $page->pbuh() }} nacque alla Mecca nel 570. Fin dalla morte del padre, prima della sua nascita e dopo la morte della madre, avvenuta subito dopo, fu allevato dallo zio che apparteneva alla rispettata tribù di Quraysh. Crebbe analfabeta e così rimase fino alla sua morte. Il suo popolo, prima della sua missione come profeta, non conosceva la scienza e la maggior parte di loro era analfabeta. Quando crebbe, divenne conosciuto per la sua sincerità, onestà, fedeltà e generosità. Era così fedele da essere chiamato il Fedele. Mohammed {{ $page->pbuh() }} era molto religioso e detestò a lungo la decadenza e l'idolatria della sua società.

All'età di quarant'anni, Mohammed {{ $page->pbuh() }} ricevette la sua prima rivelazione da Dio attraverso l'Arcangelo Gabriele. Le rivelazioni continuarono per ventitré anni e sono conosciute, nel complesso, con il nome di Corano.

Appena iniziò a recitare il Corano e a predicare la verità che Dio gli aveva rivelato, lui e il suo piccolo gruppo di seguaci furono perseguitati dai non credenti. Le persecuzioni diventarono così feroci che nell'anno 622 Dio diede loro l'ordine di emigrare. Questa migrazione dalla Mecca alla città di Medina, circa 450 km a Nord, segna l'inizio del calendario musulmano.

Dopo diversi anni, Mohammed {{ $page->pbuh() }} e i suoi seguaci ritornarono alla Mecca, dove perdonarono i loro nemici. Prima della sua morte, all'età di sessantatré anni, la maggior parte della penisola arabica divenne musulmana e circa dopo cento anni dalla sua morte, l'Islam conquistò la parte Ovest della Spagna e la Cina. Tra le ragioni di una così rapida e pacifica diffusione dell'Islam vi è la verità e la chiarezza della sua dottrina. L'Islam richiede la fede in un solo Dio, che è l'unico degno di essere adorato.

Il profeta Mohammed {{ $page->pbuh() }} fu un esempio perfetto di essere umano onesto, giusto, misericordioso, compassionevole, fedele e coraggioso. Sebbene fosse un uomo, non possedeva caratteristiche cattive e si sforzò unicamente per predicare nell'interesse di Dio e nella sua ricompensa nell'aldilà. Inoltre, in tutte le sue azioni e relazioni fu attento e timido verso Dio.
