<?php

use Illuminate\Support\Str;
use TightenCo\Jigsaw\Jigsaw;
use App\CollectionItem\Answer;

return [
    "baseUrl" => "http://localhost:3000",
    "production" => false,
    "siteName" => "Islam Answers",
    "alternateSiteName" => "Islam-Answers.com",
    "siteDescription" => "Guidance through the harmony of life and Islam",

    "l10n" => [
        "strings" => json_decode(file_get_contents("l10n/strings.json"), true),
        "refs" => json_decode(file_get_contents("l10n/refs.json"), true),
        "team" => json_decode(file_get_contents("l10n/team.json"), true),
    ],

    "relations" => collect(require_once "config/relations.php"),

    "collections" => [
        "answers" => [
            "sort" => "-date",
            "path" => fn($answer) => sprintf(
                "answers/%s/%s",
                $answer->getLanguage(),
                $answer->getSlug()
            ),
            "map" => fn($answer) => Answer::fromItem($answer),
            "related" => fn($answer) => $answer->collection
                ->filter(fn($a) => $a->getLanguage() === $answer->getLanguage())
                ->filter(
                    fn($a) => $a->getSharedFilename() !==
                        $answer->getSharedFilename()
                )
                ->filter(
                    fn($a) => in_array(
                        $a->getSharedFilename(),
                        isset($answer->relations[$answer->getSharedFilename()])
                            ? $answer->relations[
                                $answer->getSharedFilename()
                            ]->toArray()
                            : []
                    )
                ),
            "verse" => function ($answer, $reference) {
                $language = $answer->getLanguage();
                $slug = $answer->getSharedFilename();
                $verses = json_decode(
                    file_get_contents("l10n/verses/$slug.$language.json"),
                    true
                );

                $text = $verses[$reference];

                // Monitor for footnote references
                if (preg_match("/\d/", $text)) {
                    throw new Exception("Verse text contains a number: $text");
                }

                return app("view")->make("_components.quran", [
                    "text" => $text,
                    "reference" => trim($reference, "."),
                    "answer" => $answer,
                ]);
            },
            "hadith" => function ($answer, $reference) {
                $language = $answer->getLanguage();
                $slug = $answer->getSharedFilename();
                $ahadith = json_decode(
                    file_get_contents("l10n/ahadith/$slug.$language.json"),
                    true
                );
                $referenceParts = explode(":", $reference);
                $bookName = $referenceParts[0];
                $hadithNumber = $referenceParts[1];
                $referenceLabel = sprintf(
                    "%s, %s",
                    $answer->l10n["refs"]["hadith_books"][$bookName],
                    $hadithNumber
                );

                $text = $ahadith[$reference];

                $text = preg_replace_callback(
                    ["/\((pbuh)\)/", "/\[%key:(pbuh)%\]/"],
                    fn($matches) => $answer->pbuh(),
                    $text
                );

                return app("view")->make("_components.hadith", [
                    "text" => $text,
                    "reference" => trim($reference, "."),
                    "reference_label" => $referenceLabel,
                ]);
            },
            "pbuh" => fn($answer) => app("view")->make("_components.pbuh", [
                "answer" => $answer,
            ]),
            "translator" => fn($answer) => $answer->l10n["team"][
                $answer->translator_id
            ] ?? null,
            "image" => fn(
                $answer
            ) => "{$answer->baseUrl}/assets/img/answers/{$answer->getSharedFilename()}.jpg",
            "imageSet" => fn($answer) => "image-set(" .
                "url(\"/assets/img/answers/{$answer->getSharedFilename()}.avif\") type(\"image/avif\")," .
                "url(\"/assets/img/answers/{$answer->getSharedFilename()}.jpg\") type(\"image/jpeg\"))",
        ],
        "languages" => [
            "path" => "answers/{filename}",
            "answers" => fn($language, $answers) => $answers->filter(
                fn($answer) => $answer->getLanguage() ===
                    $language->getFilename()
            ),
            "language" => fn($language) => $language->getFilename(),
            "filter" => fn($language) => !empty(
                glob(
                    __DIR__ .
                        "/source/_answers/" .
                        $language->getFilename() .
                        "/*.md"
                )
            ),
        ],
    ],

    // helpers
    "getString" => fn($page, $key) => $page->l10n["strings"][$key][
        $page->getLanguage()
    ],
    "groupByLanguage" => fn($page) => $page->collection
        ->groupBy(fn($item) => $item->getLanguage())
        ->map(
            fn($group) => $group->mapWithKeys(
                fn($item) => [
                    $item->getSharedFilename() => $item,
                ]
            )
        ),
    "groupByQuestion" => fn($page) => $page->collection
        ->groupBy(fn($item) => $item->getSharedFilename())
        ->map(
            fn($group) => $group->mapWithKeys(
                fn($item) => [$item->getLanguage() => $item]
            )
        ),
    "getAllLanguages" => fn($page) => $page
        ->groupByQuestion()
        ->first(fn($item, $key) => $key == $page->getSharedFilename()),
    "getSharedFilename" => fn($answer) => Str::before(
        $answer->getFilename(),
        "."
    ),
    "getSlug" => fn($answer) => strlen(Str::slug($answer->title)) > 7
        ? Str::slug($answer->title)
        : $answer->getSharedFilename(),
    "getLanguage" => function ($page) {
        if ($page->getRelativePath()) {
            return Str::before($page->getRelativePath(), "/");
        }

        if ($page->language) {
            return $page->language();
        }

        return "en";
    },
    "getDirection" => function ($page) {
        $rtlLanguages = ["he"];

        return in_array($page->getLanguage(), $rtlLanguages) ? "rtl" : "ltr";
    },
    "getLanguageName" => fn($page) => $page->l10n["strings"]["languages"][
        $page->getLanguage()
    ],
    "getDate" => fn($page) => Datetime::createFromFormat("U", $page->date),
    "getRealUrl" => fn($page) => rtrim($page->getPath(), "/") . "/",
    "getCanonicalUrl" => fn($page) => rtrim($page->getUrl(), "/") . "/",
    "getExcerpt" => fn($page) => $page->excerpt ?: $page->description ?: "",
    "isActive" => fn($page, $path) => Str::endsWith(
        trimPath($page->getPath()),
        trimPath($path)
    ),
];
