---
extends: _layouts.answer
section: content
title: Pourquoi le porc est-il interdit par l'islam ?
date: 2024-10-18
description: Les musulmans suivent les commandements d'Allah et évitent ce qu'Il leur
  interdit, que la raison soit compréhensible ou non.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 573111
---

Le principe fondamental du musulman est d'obéir aux ordres d'Allah et de s'abstenir de ce qu'Il interdit, que la raison soit claire ou non.

Un musulman n'est pas autorisé à rejeter une règle de la charia ou à hésiter à la suivre simplement parce que la raison qui la sous-tend n'est pas évidente. Au contraire, il doit accepter les règles concernant le halal (licite) et le haram (illicite) lorsqu'elles sont établies dans les textes, qu'il en comprenne la raison ou non. Allah dit :

{{ $page->verse('33:36') }}

### Pourquoi le porc est-il haram (illicite) ?

Le porc est haram (illicite) dans l'islam, selon le texte du Coran, où Allah dit :

{{ $page->verse('2:173..') }}

Il est en aucun cas permis à un musulman de consommer du porc, sauf en cas de nécessité, lorsque la vie d'une personne est en jeu, comme lors d'une famine où une personne craint de mourir et ne peut trouver aucune autre source de nourriture.

Les textes de la charia ne mentionnent aucune raison spécifique pour l'interdiction du porc, à l'exception du verset dans lequel Allah dit :

{{ $page->verse('..6:145..') }}

Le mot « rijs » (traduit ici par « impur ») est utilisé pour désigner tout ce qui est considéré comme répugnant dans l'islam et contraire à la saine nature humaine (fitrah). Cette raison suffit en soi.

Il existe également une raison générale expliquée concernant l'interdiction des aliments et des boissons haram (illicites), qui indique la raison derrière l'interdiction de la viande de porc. Cette raison générale est mentionnée dans le verset où Allah dit :

{{ $page->verse('..7:157..') }}

### Raisons scientifiques et médicales de l'interdiction de la consommation de viande de porc

La recherche scientifique et médicale a également démontré que le porc, parmi tous les autres animaux, est un porteur de germes nocifs pour le corps humain. Il serait trop long de détailler toutes ces maladies, mais nous pouvons en énumérer brièvement quelques-unes : maladies parasitaires, infections bactériennes, virus, etc.

Ces effets néfastes, ainsi que d'autres, montrent qu'Allah a interdit la viande de porc pour une seule raison : préserver la vie et la santé, qui font partie des cinq besoins fondamentaux protégés par la charia.
