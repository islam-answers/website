---
extends: _layouts.answer
section: content
title: Por que os muçulmanos jejuam?
date: 2024-10-25
description: A principal razão pela qual os muçulmanos jejuam durante o mês do Ramadan
  é para alcançarem Taqwa (consciência de Deus).
sources:
- href: https://seekersguidance.org/articles/https-www-seekersguidance-org-courses-why-muslims-fast-the-higher-aims-of-fasting-explained/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/fast/why-do-we-fast/
  title: seekersguidance.org
- href: https://seekersguidance.org/articles/quran-articles/spiritual-purpose-fasting-closeness-allah/
  title: seekersguidance.org
translator_id: 594172
---

O jejum é um dos maiores actos de adoração que os muçulmanos praticam todos os anos durante o mês do Ramadan. É um ato de adoração verdadeiramente sincero que o próprio Allah Altíssimo recompensará -

{{ $page->hadith('bukhari:5927') }}

Na Surat Al-Baqarah, Allah Altíssimo diz,

{{ $page->verse('2:185') }}

A principal razão pela qual os muçulmanos jejuam durante o mês do Ramadan está patente no versículo:

{{ $page->verse('2:183') }}

Além disso, as virtudes do jejum são numerosas, como o facto de:

1. É uma expiação dos pecados e dos erros cometidos.
1. É um meio de quebrar os desejos não permitidos.
1. Facilita os actos de devoção.

O jejum coloca muitos desafios às pessoas, desde a fome e a sede à perturbação dos padrões de sono e muito mais. Cada um deles faz parte das dificuldades que nos foram impostas para que possamos aprender, desenvolver e crescer através delas.
Estas dificuldades não passam despercebidas a Allah, e é-nos dito para estarmos ativamente conscientes delas, para que possamos esperar d'Ele uma generosa recompensa por elas. Isto é compreendido nas palavras do Profeta Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:37') }}
