---
extends: _layouts.answer
section: content
title: L’abattage halal est-il cruel ?
date: 2025-01-26
description: L'abattage halal est plus humain et hygiénique que les méthodes occidentales,
  qui causent des douleurs intenses et retiennent le sang dans la viande.
sources:
- href: https://islamqa.org/hanafi/qibla-hanafi/35739/
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/136275/
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 586055
---

La pratique islamique consistant à abattre les animaux par une coupure tranchante à l'avant du cou a souvent fait l'objet d'attaques de la part de certains défenseurs des droits des animaux, qui y voient une forme de cruauté envers les animaux, l'argument étant qu'il s'agit d'une méthode douloureuse et inhumaine de mise à mort des animaux. En Occident, la loi impose d'étourdir les animaux d'une balle dans la tête avant l'abattage, censée rendre l'animal inconscient et l'empêcher de se réveiller avant d'être tué, afin de ne pas ralentir le mouvement de la chaîne de traitement. Il est également utilisé pour empêcher l'animal de ressentir la douleur avant de mourir.

### Études allemandes sur la douleur

Ceux qui se sont acclimatés à ces méthodes seront donc peut-être surpris d'apprendre les résultats d'une étude menée par le professeur Wilhelm Schulze et son collègue, le docteur Hazim, à l'école de médecine vétérinaire de l'université de Hanovre, en Allemagne. L'étude, intitulée "Tentatives d'objectivation de la douleur et de la conscience dans les méthodes conventionnelles d'abattage des moutons et des veaux (C.B.P.) (étourdissement par pistolet à tige captive) et rituelles (halal, couteau)", conclut que :

> L'abattage islamique est la méthode d'abattage la plus humaine et le C.B.P., pratiqué en Occident, cause de graves souffrances à l'animal.

Dans le cadre de l'étude, plusieurs électrodes ont été implantées chirurgicalement en divers points du crâne de tous les animaux, touchant la surface du cerveau. Les animaux ont été laissés en convalescence pendant plusieurs semaines. Certains animaux ont ensuite été abattus par une incision rapide et profonde du cou à l'aide d'un couteau tranchant, coupant les veines jugulaires et les artères carotides ainsi que la trachée et l'œsophage (méthode islamique). D'autres animaux ont été étourdis à l'aide d'un C.B.P. Pendant l'expérience, un électroencéphalographe (EEG) et un électrocardiogramme (ECG) ont enregistré l'état du cerveau et du cœur de tous les animaux au cours de l'abattage et de l'étourdissement.

Les résultats ont été les suivants :

##### Méthode islamique

1. Les trois premières secondes à partir du moment de l'abattage islamique telles qu'enregistrées sur l'EEG n'ont montré aucun changement par rapport au graphique avant l'abattage, indiquant ainsi que l'animal n'a ressenti aucune douleur pendant ou immédiatement après l'incision. 
1. Pendant les 3 secondes suivantes, l'EEG a enregistré un état de sommeil profond - inconscience. Cela est dû à la grande quantité de sang jaillissant du corps. 
1. Après les 6 secondes mentionnées ci-dessus, l'EEG a enregistré un niveau zéro, ne montrant aucune sensation de douleur du tout. 
1. Alors que le message cérébral (EEG) est tombé au niveau zéro, le cœur battait toujours fort et le corps convulsait vigoureusement (un réflexe de la moelle épinière) chassant une quantité maximale de sang du corps, ce qui a permis d'obtenir une viande hygiénique pour le consommateur.

##### Méthode occidentale par étourdissement C.B.P

1. Les animaux étaient apparemment inconscients peu de temps après l'étourdissement. 
1. L'EEG a montré une douleur intense immédiatement après l'étourdissement. 
1. Le cœur des animaux étourdis par CBP a cessé de battre plus tôt que celui des animaux abattus selon la méthode islamique, ce qui a entraîné une rétention de sang plus importante dans la viande. Cela n'est pas hygiénique pour le consommateur.

### Réglementations islamiques concernant l'abattage

Comme on peut le constater à partir de cette étude, l’abattage islamique des animaux est une bénédiction pour l’animal et pour l’homme. Pour que l’abattage soit licite, plusieurs mesures doivent être prises par celui qui effectue cet acte. Cela vise à assurer le plus grand bien à l’animal et au consommateur. À cet égard, le Prophète Mohammed {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:1955') }}

L'objet utilisé pour égorger l'animal doit être tranchant et utilisé rapidement. La coupure rapide des vaisseaux du cou interrompt le flux sanguin vers les nerfs du cerveau responsables de la douleur. Ainsi, l'animal ne ressent pas de douleur. Les mouvements et le flétrissement qui se produisent chez l'animal après la coupure ne sont pas dus à la douleur, mais à la contraction et à la relaxation des muscles déficients en sang. Le Prophète {{ $page->pbuh() }} a également enseigné aux musulmans de ne pas aiguiser la lame du couteau devant l'animal ni d'égorger un animal devant d'autres de sa même espèce.

La découpe doit impliquer la trachée, l'œsophage et les deux veines jugulaires sans couper la moelle épinière. Cette méthode permet un écoulement rapide du sang qui évacue la majeure partie du corps de l'animal. Si la moelle épinière est coupée, les fibres nerveuses du cœur peuvent être endommagées, ce qui peut entraîner un arrêt cardiaque et une stagnation du sang dans les vaisseaux sanguins. Le sang doit être complètement drainé avant de retirer la tête. Cela purifie la viande en éliminant la majeure partie du sang qui sert de support aux micro-organismes ; la viande reste également fraîche plus longtemps que les autres méthodes d'abattage.

Par conséquent, les accusations de cruauté envers les animaux devraient à juste titre être concentrées sur ceux qui n'utilisent pas la méthode islamique d'abattage mais préfèrent utiliser des méthodes qui causent de la douleur et de l'agonie à l'animal et qui pourraient également très bien nuire à ceux qui consomment la viande.
