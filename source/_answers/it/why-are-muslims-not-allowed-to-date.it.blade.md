---
extends: _layouts.answer
section: content
title: Perché i musulmani non si frequentano prima del matrimonio?
old_titles:
- Perché ai musulmani non è permesso frequentare qualcuno prima di sposarsi?
date: 2024-08-13
description: Il Corano proibisce di avere fidanzate o fidanzati. L'Islam preserva
  le relazioni tra gli individui nel modo più adatto all'indole umana.
sources:
- href: https://islamqa.org/shafii/seekersguidance-shafii/241530
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107140
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107560
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/shafii/qibla-shafii/33045
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/askimam/17968
  title: islamqa.org (AskImam.org)
- href: https://www.islamweb.net/en/fatwa/89542
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/82217
  title: islamweb.net
- href: https://islamqa.info/en/answers/20949
  title: islamqa.info
- href: https://islamqa.info/en/answers/33702
  title: islamqa.info
- href: https://islamqa.info/en/answers/1200
  title: islamqa.info
- href: https://islamqa.info/en/answers/21933
  title: islamqa.info
- href: https://islamqa.info/en/answers/93450
  title: islamqa.info
- href: https://islamqa.info/en/answers/1114
  title: islamqa.info
- href: https://islamqa.info/en/answers/3807
  title: islamqa.info
translator_id: 573107
---

L'Islam preserva le relazioni tra gli individui in un modo che si adatta all'indole umana. Il contatto tra un uomo e una donna è proibito se non sotto la tutela del matrimonio legale. Se vi è necessità di parlare con qualcuno del sesso opposto, ciò dovrebbe avvenire nei limiti della cortesia e delle buone maniere.

## Prevenire le tentazioni

Nell'Islam è raccomandato vivamente eliminare ogni forma di fornicazione, ancor prima che possa avere inizio. Non è permesso a una donna o a un uomo instaurare un'amicizia o una relazione d'amore con qualcuno del sesso opposto attraverso chat room, internet o qualsiasi altro mezzo, poiché ciò porta la donna o l'uomo alla tentazione. Questa è la via che il diavolo utilizza per trascinare la persona nel peccato, nella fornicazione o nell'adulterio. Allah dice:

{{ $page->verse('24:21..') }}

Una donna non può usare un tono di voce dolce con chi non le è permesso, come dice Allah:

{{ $page->verse('..33:32') }}

La Legge Islamica (Shari'ah) proibisce che uomini e donne si possano incontrare, mescolare o frequentare liberamente in luoghi affollati, mostrando ed esponendo le donne davanti agli uomini. Questi comportamenti sono proibiti perché costituiscono una delle cause principali della fitnah (tentazione o atto che implica conseguenze malvagie), del risveglio dei desideri e che portano a commettere indecenze e atti illeciti. Il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('ahmad:1369') }}

Il Corano proibisce di avere fidanzate o fidanzati. Allah dice:

{{ $page->verse('..4:25..') }}

## Valutazioni sociali

Entrare in una relazione fidanzato/fidanzata può danneggiare gravemente la reputazione di una donna, che invece non influenzerà necessariamente il partner maschile. Il sesso prima del matrimonio è contrario alla religione e mette a repentaglio l'onore della propria discendenza. È difficile vedere attraverso la nebbia dell'amore, della lussuria o dell'infatuazione, ed è per questo motivo che Allah ordina a tutti noi di stare lontani dal sesso pre-matrimoniale. Molti danni possono accadere dopo un solo atto illecito di sesso pre-matrimoniale, ad esempio gravidanze indesiderate, malattie sessualmente trasmissibili, sofferenze e sensi di colpa.

Oltre all'esplicito divieto di Dio, ci sono ovvi motivi per cui le relazioni pre-matrimoniali sono illecite. Tra queste:

1. L'incontestabilità della paternità se la donna rimane incinta. Nonostante relazioni anche di lunga durata, chi può dire che la donna non dorma con altri uomini per stabilire la compatibilità di un futuro coniuge?
2. Un bambino nato fuori dal matrimonio non viene attribuito al padre. La preservazione della discendenza è uno degli obiettivi principali della Shariah.
3. Tali relazioni danno agli uomini il vantaggio di "fare ciò che vogliono" con le donne e sono liberi da qualsiasi responsabilità emotiva e finanziaria nei confronti della donna e degli eventuali figli nati dalla relazione.
4. Esiste una virtù innata nel preservare la propria castità prima del matrimonio che è stata riconosciuta per molti secoli e stabilita da ogni religione.
5. Anche se potremmo non notare alcune differenze esteriori nelle nostre attività mondane, le pratiche scorrette hanno un effetto sull'anima che non possiamo percepire. Più ci si abbandona al peccato, più il cuore si indurisce.
6. Avere più partner aumenta il rischio di contrarre malattie sessualmente trasmissibili e di contagiare altri.
7. Non vi è alcuna garanzia che la convivenza prima del matrimonio sia un'indicazione sicura che la relazione possa continuare anche dopo il matrimonio. Molte coppie non musulmane vivono insieme per anni, per poi lasciarsi dopo il matrimonio.
8. Ci si dovrebbe chiedere, inoltre, se vorremmo che i nostri figli e figlie facessero la stessa cosa prima del matrimonio.

In conclusione, non ha alcun senso che le relazioni a lungo termine e il sesso prima del matrimonio siano consentiti. Le considerazioni individuali sono trascurabili rispetto ai moltissimi problemi seri che potrebbero affliggere gli individui, soprattutto le donne, e sono un modo sicuro per portare alla decadenza della società e della propria anima.
