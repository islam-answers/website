---
extends: _layouts.answer
section: content
title: L’Islam contraddice la scienza?
date: 2024-07-11
description: Nel Corano sono riportate nozioni scientifiche scoperte solo di recente
  grazie al progresso della tecnologia e della scienza.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 573107
---

Il Corano, il libro dell'Islam, è l'ultimo libro della rivelazione di Dio all'umanità e l'ultima delle rivelazioni date ai Profeti.

Sebbene il Corano (rivelato oltre 1400 anni fa) non sia principalmente un libro di scienza, contiene nozioni scientifiche scoperte solo di recente grazie al progresso della tecnologia e della scienza. L’Islam incoraggia la riflessione e la ricerca scientifica perché comprendere la natura della creazione consente alle persone di apprezzare maggiormente il Creatore e l'estensione del Suo potere e della Sua saggezza.

Il Corano fu rivelato in un’epoca in cui la scienza era ancora rudimentale; non esistevano telescopi, microscopi o qualcosa di simile alle tecnologie odierne. Si credeva che il sole orbitasse intorno alla terra e che il cielo fosse sostenuto da pilastri situati agli angoli di una terra piatta. In questo contesto fu rivelato il Corano che contiene molti dati scientifici su argomenti che spaziano dall’astronomia alla biologia, dalla geologia alla zoologia.

Tra le tante informazioni scientifiche riportate nel Corano, si trovano:

### Nozione n. 1 - Origine della vita

{{ $page->verse('21:30') }}

L'acqua viene indicata come l'origine della vita. Tutti gli esseri viventi sono costituiti da cellule e oggi sappiamo che le cellule sono per lo più costituite da acqua. Ma ciò è stato scoperto solo dopo l'invenzione del microscopio. Nei deserti dell'Arabia, sarebbe inconcepibile pensare che qualcuno avesse potuto intuire che la vita provenisse dall'acqua.

### Nozione n. 2 - Sviluppo embrionale umano

Dio parla delle varie fasi dello sviluppo embrionale dell'uomo:

{{ $page->verse('23:12-14') }}

La parola araba "alaqah" ha tre significati: sanguisuga, cosa sospesa e coagulo di sangue. "Mudghah" significa sostanza masticata. Gli scienziati che si occupano di embriologia hanno osservato che l'uso di questi termini per descrivere la formazione dell'embrione è accurato e conforme alla nostra attuale comprensione scientifica del processo di sviluppo.

Fino al XX secolo si sapeva ben poco sulle diverse fasi e sulla classificazione degli embrioni umani, il ché significa che le descrizioni dell'embrione umano riportate nel Corano non possono essere basate sulle conoscenze scientifiche del VII secolo.

### Nozione n. 3 - Espansione dell'Universo

In un’epoca in cui la scienza dell’Astronomia era ancora primitiva, fu rivelato il seguente versetto del Corano:

{{ $page->verse('51:47') }}

Uno dei significati del versetto riportato sopra è che Dio sta espandendo l'universo (cioè i cieli). Altri significati sono che Dio provvede e ha potere sull'universo, il che è altrettanto vero.

Il fatto che l'universo si stia espandendo (ad esempio, i pianeti si allontanano l'uno dall'altro) è stato scoperto nel secolo scorso. Il fisico Stephen Hawking, nel suo libro "Una breve storia del tempo", scrive: "La scoperta che l'universo si sta espandendo è stata una delle grandi rivoluzioni intellettuali del ventesimo secolo".

Il Corano fa riferimento all'espansione dell'universo ancor prima che il telescopio fosse stato inventato!

### Nozione n. 4 - Ferro fatto scendere sulla terra

Il ferro non è un metallo naturale sulla terra, poiché è arrivato su questo pianeta dallo spazio. Gli scienziati hanno infatti scoperto che miliardi di anni fa la terra fu colpita da meteoriti che hanno trasportato il ferro sul nostro pianeta da stelle lontane, dopo che erano esplose.

{{ $page->verse('..57:25..') }}

Dio usa le parole 'facemmo scendere'. Il fatto che il ferro sia stato inviato sulla terra dallo spazio è qualcosa che non poteva essere conosciuto dalla scienza rudimentale del VII secolo.

### Nozione n. 5 - Protezione del cielo

Il cielo svolge un ruolo fondamentale nel proteggere la terra e i suoi abitanti dai raggi letali del sole e dal freddo gelido dello spazio.

Dio ci chiede di meditare e osservare il cielo nel seguente versetto:

{{ $page->verse('21:32') }}

Il Corano descrive la protezione del cielo come un segno di Dio, proprietà protettive che sono state scoperte dalla ricerca scientifica soltanto nel XX secolo.

### Nozione n. 6 - Montagne

Dio richiama la nostra attenzione su un'importante caratteristica delle montagne:

{{ $page->verse('78:6-7') }}

Il Corano descrive correttamente le radici profonde delle montagne utilizzando la parola "pioli". Il monte Everest, ad esempio, ha un'altezza approssimativa di 9 km dal suolo, mentre la sua radice è più profonda di 125 km!

Il fatto che le montagne abbiano radici profonde simili a "pioli" è stato appurato soltanto dopo lo sviluppo della teoria della tettonica a placche, all'inizio del XX secolo. Nel Corano (16:15) Dio dice inoltre che le montagne hanno un ruolo nello stabilizzare la terra "...in modo che non oscilli", fenomeno che gli scienziati hanno cominciato a comprendere solo di recente.

### Nozione n. 7 - Orbita del Sole

Nel 1512, l'astronomo Niccolò Copernico formulò la teoria secondo cui il Sole è immobile al centro del sistema solare e i pianeti ruotano attorno ad esso. Questa convinzione era diffusa tra gli astronomi fino al XX secolo. È invece ormai accertato che il Sole non è immobile, ma si muove lungo un'orbita intorno al centro della nostra galassia, la Via Lattea.

{{ $page->verse('21:33') }}

### Nozione n. 8 - Onde sommerse negli oceani

Si pensava comunemente che le onde si formassero solo sulla superficie dell'oceano. Tuttavia, gli oceanografi hanno scoperto che esistono onde interne sommerse che si verificano sotto la superficie, invisibili all'occhio umano, e che possono essere rilevate solo con attrezzature specifiche.

Il Corano dice:

{{ $page->verse('24:40..') }}

Questa descrizione è sorprendente perché 1400 anni fa non esistevano attrezzature specifiche per scoprire le onde interne e sommerse nelle profondità degli oceani.

### Nozione n. 9 - Menzogne e azioni

Vi era un leader tribale crudele e oppressivo che visse al tempo del profeta Mohammed {{ $page->pbuh() }}. Dio rivelò un versetto per metterlo in guardia:

{{ $page->verse('96:15-16') }}

Dio non chiama questa persona bugiarda, ma chiama la sua fronte, il ciuffo (la parte anteriore del cervello) "mendace" e "peccaminoso" e lo avverte che deve smettere. Numerosi studi hanno scoperto che la parte anteriore del nostro cervello (lobo frontale) è responsabile sia della menzogna che delle azioni volontarie, e quindi del peccato. Queste funzioni sono state scoperte con apparecchiature mediche di radiologia per immagini sviluppate nel XX secolo.

### Nozione n. 10 - I due mari che non si mischiano

Per quanto riguarda i mari, il nostro Creatore dice:

{{ $page->verse('55:19-20') }}

Una forza fisica chiamata tensione superficiale impedisce alle acque di mari confinanti di mischiarsi, a causa della differenza di densità delle loro acque. È come se tra loro ci fosse un muro sottile. Questo fenomeno è stato scoperto solo di recente dagli oceanografi.

### Mohammed avrebbe potuto essere l'autore del Corano?

Il profeta Mohammed {{ $page->pbuh() }} è conosciuto storicamente per essere un'analfabeta. Infatti, egli non sapeva né leggere né scrivere, e non aveva conoscenza di nessuna materia che potesse spiegare la precisione scientifica del Corano.

Alcuni potrebbero affermare che Mohammed abbia copiato il Corano da persone erudite o da scienziati dell'epoca. Se fosse stato copiato, molto probabilmente sarebbero state copiate anche tutte le ipotesi scientifiche errate dell’epoca. Troviamo, invece, che il Corano non contiene nessun errore, sia esso scientifico o di altro tipo.

Alcune persone potrebbero inoltre sostenere che il Corano sia stato modificato in seguito a nuove scoperte scientifiche. Ma anche questo non può essere vero perché è un fatto storicamente documentato che il Corano è stato conservato nella sua lingua originale, il che è già di per sé un miracolo.

### Solo una coincidenza?

{{ $page->verse('41:53') }}

Sebbene questa risposta si sia concentrata solo sui miracoli scientifici, ci sono molti altri tipi di miracoli menzionati nel Corano: miracoli storici, profezie che si sono avverate, stili linguistici e letterari che non hanno eguali, per non parlare della commozione che provoca nelle persone quando lo leggono o lo ascoltano. Tutti questi miracoli non possono essere dovuti a coincidenze. Indicano chiaramente che il Corano proviene da Dio, il Creatore di tutte queste leggi scientifiche. Egli è lo stesso e unico Dio che ha inviato tutti i Profeti con lo stesso messaggio: adorare l'unico Dio e seguire gli insegnamenti del Suo Messaggero.

Il Corano è un libro-guida che dimostra che Dio non ha creato gli esseri umani semplicemente perché vaghino senza una méta. Piuttosto, ci insegna che abbiamo uno scopo importante e più elevato nella vita: riconoscere la completa perfezione, grandezza e unicità di Dio e obbedirGli.

Spetta a ciascuno di noi usare l'intelletto e le capacità di ragionamento che Dio gli ha donato per contemplare e riconoscere i Suoi segni e messaggi, e il Corano è il messaggio più importante. Leggete e scoprite la bellezza e la verità del Corano, affinché possiate raggiungere il successo!
