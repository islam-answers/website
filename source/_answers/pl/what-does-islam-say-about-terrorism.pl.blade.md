---
extends: _layouts.answer
section: content
title: Co Islam mówi o terroryzmie?
date: 2024-08-11
description: Islam, religia miłosierdzia, nie zezwala na terroryzm.
sources:
- href: https://www.islam-guide.com/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 584496
---

Islam, religia miłosierdzia, nie pozwala na terroryzm. W Koranie Bóg powiedział:

{{ $page->verse('60:8') }}

Prorok Muhammad {{ $page->pbuh() }}, zabraniał żołnierzom zabijania kobiet i dzieci, i radził im:

{{ $page->hadith('tirmidhi:1408') }}

Powiedział również:

{{ $page->hadith('bukhari:3166') }}

Również Prorok Muhammad {{ $page->pbuh() }}, zabronił karania ogniem.

Kiedyś wymienił morderstwo jako drugie z głównych grzechów, a nawet ostrzegał, że w Dniu Sądu,

{{ $page->hadith('bukhari:6533') }}

Muzułmanie są nawet zachęcani do bycia życzliwymi dla zwierząt i zabrania się im krzywdzenia ich. Kiedyś Prorok Muhammad {{ $page->pbuh() }}, powiedział:

{{ $page->hadith('bukhari:3318') }}

Powiedział również, że pewien człowiek dał bardzo spragnionemu psu pić, więc Bóg wybaczył mu grzechy za to działanie. Proroka zapytano: „Posłańcu Boga, czy jesteśmy wynagradzani za dobroć wobec zwierząt?” Powiedział:

{{ $page->hadith('bukhari:2466') }}

Ponadto, podczas odbierania życia zwierzęciu w celu zdobycia pożywienia, muzułmanie są zobowiązani do robienia tego w sposób, który powoduje jak najmniej strachu i cierpienia. Prorok Muhammad {{ $page->pbuh() }}, powiedział:

{{ $page->hadith('tirmidhi:1409') }}

W świetle tych i innych tekstów islamskich podżeganie do terroru w sercach bezbronnych cywilów, hurtowe niszczenie budynków i mienia, bombardowanie i okaleczanie niewinnych mężczyzn, kobiet i dzieci są czynami zakazanymi i godnymi potępienia według islamu i muzułmanów.

Wyznawcy islamu, religii pokoju, miłosierdzia i przebaczenia, w zdecydowanej większości nie mają nic wspólnego z brutalnymi zdarzeniami, które niektórzy przypisują muzułmanom. Jeśli muzułmanin dokonałby aktu terroryzmu, naruszyłby tym samym prawa islamu.

Jednakże ważne jest rozróżnienie między terroryzmem a legalnym oporem przeciwko okupacji, ponieważ są to dwie bardzo różne rzeczy.
