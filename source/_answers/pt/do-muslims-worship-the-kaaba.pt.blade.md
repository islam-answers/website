---
extends: _layouts.answer
section: content
title: Os muçulmanos adoram a Kaaba?
date: 2025-01-13
description: Os muçulmanos adoram apenas Allah, obedecendo Seus mandamentos, como
  orar em direção à Kaaba, símbolo de unidade e monoteísmo, construída por Abraão
  e Ismael.
sources:
- href: https://islamqa.info/en/answers/82349/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/85437/
  title: islamweb.net
- href: https://www.islamweb.net/en/article/141834/
  title: islamweb.net
translator_id: 594172
---

A Kaaba é a Qiblah (direção) para a qual todos os muçulmanos viram o rosto quando rezam. É um dever virar o rosto para a Kaaba enquanto rezamos. Allah diz no Alcorão:

{{ $page->verse('2:144..') }}

Todos os anos, mais de 2,5 milhões de peregrinos muçulmanos de todo o mundo cumprem os ritos da Hajj (peregrinação) em Meca, onde se situa a Kaaba. Linguisticamente, Kaaba significa simplesmente "cubo" em árabe, mas é muito mais do que um edifício em forma de cubo coberto de preto. É o símbolo da unidade islâmica, no coração do Islam, e baseia-se no princípio do monoteísmo.

Fazendo referência à Kaaba, Allah diz-nos no Alcorão que:

{{ $page->verse('3:96') }}

Nessa altura, os alicerces da Kaaba ainda não tinham sido erguidos por Ibrahim (Abraão), que mais tarde a construiria com o seu filho Ismail (Ismael), conforme ordenado por Allah:

{{ $page->verse('22:26') }}

Aqui, vemos que o objetivo desta casa era a adoração exclusiva de Allah, sem parceiros. Abraão passou toda a sua vida a rejeitar o politeísmo, indo contra as crenças e costumes de toda a sua comunidade e dos seus líderes, e mesmo contra os do seu pai, que costumava fabricar ídolos com as suas próprias mãos.

Como muçulmanos, o nosso princípio fundamental é que adoramos apenas Allah. Acções como rezar em direção à Kaaba e fazer Tawaf (dar a volta à Kaaba) são cumprir as ordens de Allah, não adorar o objeto em si. Existem grandes diferenças entre estar de frente para a Kaaba e adorar ídolos.
