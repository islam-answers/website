---
extends: _layouts.answer
section: content
title: A fost Iisus răstignit?
date: 2024-09-21
description: Conform Coranului, Iisus nu a fost ucis sau răstignit. Iisus a fost ridicat
  viu la cer de Allah, iar un alt om a fost răstignit în locul său.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 554943
---

Când evreii și împăratul roman au complotat pentru a-l ucide pe Iisus {{ $page->pbuh() }}, Allah a făcut ca unul dintre oamenii prezenți să semene în toate cu Iisus. Astfel, ei l-au ucis pe omul care semăna cu Iisus. Ei nu l-au ucis pe Iisus. Iisus {{ $page->pbuh() }} a fost ridicat viu la Ceruri. Dovada în acest sens sunt cuvintele lui Allah cu privire la invenția evreilor și la respingerea acesteia:

{{ $page->verse('4:157-158') }}

Astfel, Allah a declarat false cuvintele evreilor atunci când au afirmat că l-au ucis și l-au răstignit și a afirmat că l-a luat sus la El. Aceasta a fost mila lui Allah față de Iisus (Isa) {{ $page->pbuh() }}, și a fost o onoare pentru el, astfel încât să fie unul dintre semnele Sale, o onoare pe care Allah o acordă oricui dorește dintre mesagerii Săi.

Implicația cuvintelor "(...) ci Allah l-a înălțat (pe Iisus) (cu trupul și sufletul său) la El" este că Allah, glorificat fie El, l-a luat pe Iisus cu trupul și sufletul, pentru a respinge afirmația evreilor că l-au răstignit și l-au ucis, deoarece uciderea și răstignirea au legătură cu corpul fizic. Mai mult, dacă El ar fi luat doar sufletul, acest lucru nu ar exclude afirmația că l-au ucis și răstignit, deoarece luarea doar a sufletului nu ar fi o respingere a afirmației lor. De asemenea, numele lui Iisus {{ $page->pbuh() }}, în realitate, se referă la el atât în suflet, cât și în trup împreună; nu se poate referi doar la unul dintre ele, cu excepția cazului în care există o indicație în acest sens. În plus, luarea în considerare atât a sufletului, cât și a trupului împreună este un indiciu al puterii și înțelepciunii desăvârșite a lui Allah, precum și al onoarei și sprijinului pe care îl acordă oricui dorește El dintre Trimișii Săi, în conformitate cu ceea ce spune El, Allah să fie înălțat, la sfârșitul versetului: "(...) Allah este Puternic şi Înțelept.”
