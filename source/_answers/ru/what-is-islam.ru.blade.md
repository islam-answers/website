---
extends: _layouts.answer
section: content
title: Что такое ислам?
date: 2024-10-03
description: Ислам означает покорность, смирение, повиновение приказам и игнорирование
  запретов без возражений, поклонение одному Аллаху и веру в Него.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 594830
---

Если вы обратитесь к словарям арабского языка, то обнаружите, что значение слова "ислам" - это покорность, смирение, беспрекословное выполнение приказов и внимание запретов, искреннее поклонение одному лишь Аллаху, вера в то, что Он нам говорит, и вера в Него.

Слово «ислам» стало названием религии, которую принес пророк Мухаммад {{ $page->pbuh() }}.

### Почему эта религия называется исламом

Все религии на земле называются разными именами, либо по имени конкретного человека, либо по имени конкретного народа. Так, христианство получило свое название от имени Христа; буддизм - от имени своего основателя Будды; точно так же иудаизм получил свое название от племени, известного как Иегуда (Иуда), поэтому он стал называться иудаизмом. И так далее.

За исключением ислама, поскольку он не приписывается какому-либо конкретному человеку или народу, а его название относится к значению слова "ислам". Это название указывает на то, что создание и основание этой религии не было делом рук одного конкретного человека и что она не предназначена только для одного конкретного народа, исключая все остальные. Напротив, ее цель - придать атрибут, подразумеваемый под словом "ислам", всем народам земли. Поэтому каждый, кто приобретает этот атрибут, будь то в прошлом или в настоящем, является мусульманином, и каждый, кто приобретет этот атрибут в будущем, также будет мусульманином.

Ислам - это религия всех пророков. Он появился в начале пророчества, со времен нашего отца Адама, и все послания призывали к исламу (покорности Аллаху) в плане веры и основных предписаний. Верующие, последовавшие за предыдущими пророками, были мусульманами в общем смысле, и они войдут в Рай в силу своего ислама.
