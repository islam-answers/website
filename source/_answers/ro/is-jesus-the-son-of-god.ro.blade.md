---
extends: _layouts.answer
section: content
title: Este Iisus fiul lui Dumnezeu?
date: 2024-12-23
description: Iisus este considerat un mare profet și mesager al lui Dumnezeu, dar
  nu Dumnezeu sau fiul Lui; mai degrabă, el este un slujitor și un mesager printre
  oameni.
sources:
- href: https://islamqa.info/en/answers/26301/
  title: islamqa.info
- href: https://islamqa.info/en/answers/82361/
  title: islamqa.info
translator_id: 554943
---

Musulmanii îl consideră pe Iisus un profet și un mesager al lui Dumnezeu, născut din Fecioara Maria printr-o naștere miraculoasă. Cu toate acestea, credința centrală care distinge înțelegerea islamică de doctrina creștină este respingerea lui Iisus ca fiind divin sau fiul literal al lui Dumnezeu.

### Unicitatea lui Dumnezeu

În centrul credinței islamice se află Tawhid (unicitatea lui Dumnezeu), care subliniază faptul că nu există altă divinitate în afară de Dumnezeu, iar El nici nu naște, nici nu este născut. Prin urmare, atribuirea divinității lui Iisus sau a titlului de fiu al lui Dumnezeu contrazice principiul fundamental al monoteismului în islam.

Unul dintre cele mai importante principii ale credinței în Allah este acela de a declara că Allah este deasupra tuturor atributelor care implică neajunsuri. Unul dintre atributele care implică neajunsuri pe care musulmanul trebuie să le respingă este ideea că Allah are un fiu, deoarece acest lucru implică nevoia și faptul că există o ființă care este similară cu El, iar acestea sunt aspecte peste care El este cu mult deasupra. Allah spune în Coran:

{{ $page->verse('112:1-4') }}

Coranul abordează în mod explicit concepția greșită cu privire la divinitatea lui Iisus, subliniind statutul său uman și servitutea sa față de Dumnezeu. Allah spune:

{{ $page->verse('5:75') }}

Faptul că amândoi mâncau bucate (alimente) înseamnă că aveau nevoie de hrană. Acest lucru implică faptul că Iisus și mama sa trebuiau să se ușureze după ce mâncarea fusese digerată. Allah Atotputernicul este mult mai presus de a depinde de mâncare sau de a trebui să meargă la toaletă. Allah spune, de asemenea:

{{ $page->verse('4:171') }}

### Examinarea validității scripturilor creștine

Musulmanii cred că Evangheliile care se află astăzi în mâinile oamenilor, în care cred creștinii, au fost modificate și schimbate, și încă mai sunt modificate din când în când, astfel încât nu a mai rămas nimic din forma în care Evanghelia a fost revelată inițial de Allah (Dumnezeu).

Evanghelia care vorbește cel mai mult despre credința în trinitate și presupusa divinitate a lui Iisus {{ $page->pbuh() }}, și care a devenit un punct de referință pentru creștini în argumentele lor în sprijinul acestei falsități, este Evanghelia după Ioan. Această Evanghelie este supusă îndoielilor cu privire la paternitatea sa chiar printre unii cercetători creștini înșiși - ceea ce nu este cazul cu celelalte Evanghelii în care ei cred.

Encyclopaedia Britannica menționează:

> În ceea ce privește Evanghelia după Ioan, aceasta este fără îndoială fabricată. Autorul ei a vrut să pună față în față doi dintre ucenici, și anume Sfântul Ioan și Sfântul Matei.
> Acest scriitor care apare în text susținea că el este ucenicul care a fost iubit de Mesia, iar Biserica a luat acest lucru ca atare și a afirmat că scriitorul era ucenicul Ioan, și i-a pus numele pe carte, chiar dacă autorul nu era Ioan cu siguranță.

### Ce înseamnă "fiul lui Dumnezeu" în Biblie?

Biblia în care se spune că Iisus ar fi fiul lui Dumnezeu este aceeași Biblie în care linia strămoșilor lui Iisus se încheie cu Adam {{ $page->pbuh() }}, și tot el este descris ca fiu al lui Dumnezeu:

> Iisus avea aproape treizeci de ani când a început să înveţe pe norod; şi era, cum se credea, fiul lui Iosif, fiul lui Eli, (...) fiul lui Enos, fiul lui Set, fiul lui Adam, fiul lui Dumnezeu. (Luca 3:23-38)

Aceasta este aceeași Biblie care îl descrie pe Israel (Iacob) în aceiași termeni:

> Tu vei zice lui Faraon: ‘Aşa vorbeşte Domnul: «Israel este fiul Meu, întâiul Meu născut. (Exod 4:22)

Același lucru se spune despre Solomon {{ $page->pbuh() }}:

> El mi-a zis: ‘Fiul tău Solomon Îmi va zidi casa şi curţile, căci l-am ales ca fiu al Meu, şi-i voi fi Tată. (1 Cronici 28:6)

Au fost Adam, Israel (Iacob) și Solomon toți ceilalți fii ai lui Dumnezeu, înainte de Iisus {{ $page->pbuh() }}? Înălțat să fie El cu mult peste ceea ce spun ei! Într-adevăr, chiar în Evanghelia după Ioan există o explicație a ceea ce înseamnă a fi fiu; aceasta îi include pe toți slujitorii neprihăniți ai lui Dumnezeu, astfel încât nu există nimic unic la Iisus sau la oricare alt profet în această privință:

> Dar tuturor celor ce L-au primit, adică celor ce cred în Numele Lui, le-a dat dreptul să se facă copii ai lui Dumnezeu. (Ioan 1:12)

Ceva similar apare în Evanghelia după Matei:

> Ferice de cei cu inima curată, căci ei vor vedea pe Dumnezeu! Ferice de cei împăciuitori, căci ei vor fi chemaţi fii ai lui Dumnezeu! (Matei 5:8-9)

Această utilizare a cuvântului "fiu" în limbajul Bibliei este o metaforă pentru slujitorul neprihănit al lui Dumnezeu, fără ca aceasta să implice ceva special sau unic cu privire la modul în care a fost creat, sau să îl descrie în mod literal ca pe urmașul lui Dumnezeu. De aceea Ioan spune:

> Vedeți ce dragoste ne-a arătat Tatăl, să ne numim copii ai lui Dumnezeu! (1 Ioan 3:1)

Rămâne problema descrierii lui Iisus {{ $page->pbuh() }} ca fiu al lui Dumnezeu. Nici acest lucru nu este unic în limbajul Evangheliilor:

> „Nu mă ţine”, i-a zis Iisus, „căci încă nu M-am suit la Tatăl Meu. Ci du-te la fraţii Mei şi spune-le că Mă sui la Tatăl Meu şi Tatăl vostru, la Dumnezeul Meu şi Dumnezeul vostru.” (Ioan 20:17)

În acest verset, Iisus spune că Dumnezeu este un tată și pentru ei și că Dumnezeu este Dumnezeul tuturor. Coranul subliniază unicitatea absolută și perfecțiunea lui Dumnezeu, respingând orice asociere a vreunui partener sau unui presupus urmaș cu El. Termenul "fiul lui Dumnezeu" din limbajul biblic este metaforic, semnificând neprihănirea, nu divinitatea. Prin urmare, musulmanii susțin statutul nobil al lui Iisus, mesager al lui Dumnezeu, afirmând în același timp unicitatea lui Dumnezeu ca fundament al credinței lor.
