---
extends: _layouts.answer
section: content
title: Why does Islam not allow pork?
date: 2024-07-19
description: Muslims obey whatever Allah enjoins upon them, and refrain from whatever He forbids them, whether the reason behind that is clear or not.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
---

The basic principle for the Muslim is that he obeys whatever Allah enjoins upon him, and refrains from whatever He forbids him, whether the reason behind that is clear or not.

It is not permissible for a Muslim to reject any ruling of Shari’ah or to hesitate to follow it if the reason behind it is not clear. Rather he must accept the rulings on halal and haram when they are proven in the text, whether he understands the reason behind that or not. Allah says:

{{ $page->verse('33:36') }}

### Why is pork haram?

Pork is haram (forbidden) in Islam according to the text of the Quran, where Allah says:

{{ $page->verse('2:173..') }}

It is not permissible for a Muslim to consume it under any circumstances except in cases of necessity where a person’s life depends on eating it, such as in the case of starvation where a person fears that he is going to die, and he cannot find any other kind of food.

There is no mention in the Shari’ah texts of a specific reason for the prohibition on pork, apart from the verse in which Allah says:

{{ $page->verse('..6:145..') }}

The word rijs (translated here as ‘impure’) is used to refer to anything that is regarded as abhorrent in Islam and according to the sound human nature (fitrah). This reason alone is sufficient.

And there is a general reason which is given with regard to the prohibition on haram food and drink and the like, which points to the reason behind the prohibition on pork. This general reason is to be found in the verse in which Allah says:

{{ $page->verse('..7:157..') }}

### Scientific and medical reasons for prohibiting pork

Scientific and medical research has also proven that the pig, among all other animals, is regarded as a carrier of germs that are harmful to the human body. Explaining all these harmful diseases in detail would take too long, but in brief we may list them as: parasitic diseases, bacterial diseases, viruses and so on.

These and other harmful effects indicate that Allah has only forbidden pork for a reason, which is to preserve life and health, which are among the five basic necessities which are protected by Shari’ah.
