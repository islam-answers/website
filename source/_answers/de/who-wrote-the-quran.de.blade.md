---
extends: _layouts.answer
section: content
title: Wer hat den Koran geschrieben?
date: 2024-03-23
description: Der Koran ist das wörtliche Wort Gottes, das er seinem Propheten Muhammad
  (Friede sei mit ihm) durch den Engel Gabriel offenbart hat.
sources:
- href: https://www.islam-guide.com/de/ch1-1.htm
  title: islam-guide.com
alternativeTitles: []
---

Gott unterstützte seinen letzten Propheten Muhammad {{ $page->pbuh() }} mit vielen Wundern und Beweisen, die zeigen, dass er ein wahrer von Gott gesandter Prophet ist. Ebenso stärkte Gott sein letztes offenbartes Buch, den Heiligen Koran, mit zahlreichen Wundern, die beweisen, dass dieser Koran das unverfälschte Wort Gottes ist, dass er von Ihm offenbart wurde und dass er nicht von irgendeinem Menschen verfasst sein kann.

Der Koran ist das unverfälschte Wort Gottes, das er seinem Propheten Muhammad {{ $page->pbuh() }} durch den Engel Gabriel offenbart hat. Der Prophet Muhammad {{ $page->pbuh() }}, lernte es auswendig und diktierte es später seinen Gefährten. Auch sie lernten es nach und nach auswendig, schrieben es auf und überprüften es nochmals mit dem Propheten Muhammad {{ $page->pbuh() }}. Auch sie lernten es nach und nach auswendig, schrieben es auf und überprüften es nochmals mit dem Propheten Muhammad {{ $page->pbuh() }}. Außerdem überprüfte der Prophet Muhammad {{ $page->pbuh() }} den Koran einmal jeden Ramadhan mit dem Engel Gabriel, und im letzten Jahr seines Lebens sogar zweimal. Von der Zeit an, als der Koran offenbart wurde, bis in unsere Zeit gab es immer eine außerordentlich große Anzahl von Muslimen, die den gesamten Koran Wort für Wort auswendig gelernt haben. Einige von ihnen waren sogar in der Lage, den ganzen Koran mit zehn Jahren schon auswendig zu können. Kein einziger Buchstabe im Koran wurde im Laufe der Jahrhunderte verändert.

Der Koran wurde vor vierzehn Jahrhunderten offenbart und in ihm erwähnte Tatsachen wurden erst vor kurzem von Wissenschaftlern entdeckt und nachgewiesen. Dies beweist ohne Zweifel, dass der Koran das unveränderte Wort Gottes sein muss, dem Propheten Muhammad {{ $page->pbuh() }}, offenbart; und dass der Koran nicht von Muhammad {{ $page->pbuh() }} oder von irgendeinem anderen Menschen verfasst werden konnte. Und dies beweist auch, dass Muhammad {{ $page->pbuh() }} wirklich ein von Gott gesandter Prophet ist, denn es widerspricht jeglicher Vernunft, dass irgendjemand vor vierzehnhundert Jahren von diesen Tatsachen, die erst kürzlich mittels modernster Ausrüstung und sophistischen wissenschaftlichen Methoden entdeckt oder bewiesen wurden, Kenntnis gehabt haben könnte.
