---
extends: _layouts.answer
section: content
title: What is Shariah Law?
date: 2024-07-14
description: Shariah refers to the entire religion of Islam, which Allah chose for His slaves, to lead them forth from the depths of darkness to the light.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
---

The word Shari’ah refers to the entire religion of Islam, which Allah chose for His slaves, to lead them forth from the depths of darkness to the light. It is what He has prescribed for them and explained to them of commands and prohibitions, halal and haram.

The one who follows the Shari’ah of Allah, regarding as permissible what He has permitted and regarding as prohibited what He has prohibited, will attain triumph.

The one who goes against the Shari’ah of Allah is exposing himself to Allah’s wrath, anger and punishment.

Allah, may He be exalted, says:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (may Allah have mercy on him) said:

> The word Shari’ah (pl. sharai‘) refers to what Allah has prescribed (shara‘a) for people regarding matters of religion, and what He has commanded them to adhere to of prayer, fasting, Hajj and so on. It is the shir‘ah (the place in the river where one may drink).

Ibn Hazm (may Allah have mercy on him) said:

> Shari’ah is what Allah, may He be exalted, prescribed (shara‘a) on the lips of His Prophet {{ $page->pbuh() }} with regard to religion, and on the lips of the Prophets (peace be upon them) who came before him. The ruling of the abrogating text is to be regarded as the final ruling.

### Linguistic origin of the term Shari’ah

The linguistic origin of the term Shari’ah refers to the place in which a rider is able to come and drink water, and the place in the river where one may drink. Allah, may He be exalted, says:

{{ $page->verse('42:13') }}
