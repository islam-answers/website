---
extends: _layouts.answer
section: content
title: Có phải mọi tội lỗi được tha thứ sau khi cải sang Đạo Hồi?
date: 2025-02-02
description: Khi một người ngoại đạo trở thành người Hồi giáo, Allah sẽ tha thứ mọi
  điều họ đã làm khi còn là người không theo đạo Hồi và họ sẽ được tẩy sạch tội lỗi.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
translator_id: 587785
---

Nhờ Ân điển và Lòng thương xót của Người, Allah đã biến việc theo Hồi giáo thành lý do để xóa bỏ những tội lỗi đã phạm phải trước đó. Khi một người không tin đạo trở thành người Hồi giáo, Allah tha thứ cho tất cả những gì anh ta đã làm khi anh ta không phải là người Hồi giáo, và anh ta được tẩy sạch tội lỗi.

'Amr ibn al-'As (cầu xin Allah hài lòng với ông) đã nói:

{{ $page->hadith('muslim:121') }}

Imam Nawawi đã nói: “Hồi giáo phá hủy những gì đã có trước nó” có nghĩa là nó xóa bỏ và tiêu diệt những gì đã có trước nó.

Shaykh Ibn `Uthaymin (cầu Allah thương xót ông) đã được hỏi một câu hỏi tương tự, về một người kiếm tiền bằng cách buôn bán ma túy trước khi anh ta trở thành người Hồi giáo. Ông trả lời: Chúng tôi nói với người anh em này, người mà được Allah ban phước vào Hồi giáo sau khi anh ta kiếm được tài sản bất hợp pháp: hãy vui mừng, vì tài sản này là hợp pháp đối với anh ta và không có tội lỗi nào trong đó, dù anh ta giữ nó hay cho nó làm từ thiện, hoặc sử dụng nó để kết hôn, vì Allah nói trong Kinh của Người:

{{ $page->verse('8:38') }}

Điều này có nghĩa là tất cả những gì đã qua, nói chung, đều được tha thứ. Nhưng bất kỳ số tiền nào bị lấy đi bằng vũ lực từ chủ sở hữu của nó phải được trả lại cho anh ta. Nhưng số tiền kiếm được thông qua các thỏa thuận giữa mọi người, ngay cả khi nó là bất hợp pháp, như số tiền kiếm được thông qua Riba (lãi suất), hoặc bằng cách bán ma túy, v.v. thì được phép cho anh ta khi anh ta trở thành người Hồi giáo.

Nhiều người không theo đạo Hồi vào thời của Tiên tri Muhammad {{ $page->pbuh() }} đã trở thành người Hồi giáo sau khi họ giết những người Hồi giáo khác, nhưng họ không bị trừng phạt vì những gì họ đã làm.
