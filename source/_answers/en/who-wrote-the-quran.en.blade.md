---
extends: _layouts.answer
section: content
title: Who wrote the Quran?
date: 2024-03-01
description: The Quran is the literal word of God, which He revealed to His Prophet
  Muhammad (peace be upon him) through the Angel Gabriel.
sources:
- href: https://www.islam-guide.com/ch1-1.htm
  title: islam-guide.com
---

God supported His last Prophet Muhammad {{ $page->pbuh() }} with many miracles and much evidence which proved that he is a true Prophet sent by God. Also, God supported His last revealed book, the Holy Quran, with many miracles that prove that this Quran is the literal word of God, revealed by Him, and that it was not authored by any human being.

The Quran is the literal word of God, which He revealed to His Prophet Muhammad {{ $page->pbuh() }} through the Angel Gabriel. It was memorized by Prophet Muhammad {{ $page->pbuh() }}, who then dictated it to his Companions. They, in turn, memorized it, wrote it down, and reviewed it with the Prophet Muhammad {{ $page->pbuh() }}. Moreover, the Prophet Muhammad {{ $page->pbuh() }} reviewed the Quran with the Angel Gabriel once each year and twice in the last year of his life. From the time the Quran was revealed, until this day, there has always been a huge number of Muslims who have memorized all of the Quran, letter by letter. Some of them have even been able to memorize all of the Quran by the age of ten. Not one letter of the Quran has been changed over the centuries.

The Quran, which was revealed fourteen centuries ago, mentioned facts only recently discovered or proven by scientists. This proves without doubt that the Quran must be the literal word of God, revealed by Him to the Prophet Muhammad {{ $page->pbuh() }}, and that the Quran was not authored by Prophet Muhammad {{ $page->pbuh() }} or by any other human being. This also proves that Prophet Muhammad {{ $page->pbuh() }} is truly a prophet sent by God. It is beyond reason that anyone fourteen hundred years ago would have known these facts discovered or proven only recently with advanced equipment and sophisticated scientific methods.
