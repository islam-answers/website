<?php

namespace App\Commands;

use App\Helpers\FileHelper;
use Illuminate\Support\Str;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

#[AsCommand(
    name: 'answer:create',
    description: 'Create a new answer',
)]
class CreateAnswerCommand extends Command
{
    private function createAnswerFile(string $title, string $description, string $slug)
    {
        $date = date('Y-m-d');

        // Create a file under answers/en/{slug}.en.blade.md
        // Prepare the content using yaml_emit
        $metadata = [
            'extends' => '_layouts.answer',
            'section' => 'content',
            'title' => $title,
            'date' => $date,
            'description' => $description,
            'sources' => [
                [
                    'href' => 'https://example.com',
                    'title' => 'Example.com',
                ],
            ],
        ];

        $yaml = yaml_emit($metadata, YAML_UTF8_ENCODING, YAML_LN_BREAK);
        $yaml = preg_replace("/\"$date\"/", $date, $yaml);

        $yaml = preg_replace('/\.\.\.$/', '---', $yaml);

        $content = $yaml . PHP_EOL . 'To be updated';

        // Create the file
        $filename = __DIR__ . "/../source/_answers/en/$slug.en.blade.md";
        FileHelper::write($filename, $content);
    }

    private function generateJsonFile(string $file)
    {
        $filepath = __DIR__ . "/../l10n/$file";
        $content = [
            'reference1' => 'text1',
            'reference2' => 'text2',
        ];

        // Save the JSON file
        file_put_contents($filepath, json_encode($content, JSON_PRETTY_PRINT));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        // Ask for the title of the question
        $question = new Question('What is the title of the question? ');
        $title = $helper->ask($input, $output, $question);

        // Ask for the description of the question
        $question = new Question('What is the description of the question? ');
        $description = $helper->ask($input, $output, $question);

        // Generate a slug from the title
        $slug = Str::slug($title);

        // Create the answer file
        $this->createAnswerFile($title, $description, $slug);

        $jsonFiles = ["verses/$slug.en.json", "ahadith/$slug.en.json"];

        foreach ($jsonFiles as $file) {
            $this->generateJsonFile($file);
        }

        $output->writeln([
            "Don't forget to add a picture at $slug.jpg",
            "Also, add a relation in relations.php",
            "\n\t'$slug' => [\n\t\t//\n\t],"
        ]);

        return Command::SUCCESS;
    }
}