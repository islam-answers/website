---
extends: _layouts.answer
section: content
title: Gesù ha mai affermato di essere Dio?
date: 2024-11-01
description: I musulmani credono che Gesù sia stato un profeta che invitava le persone
  ad adorare solo Allah, e non ha mai affermato di essere Dio.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
translator_id: 573107
---

I musulmani credono che Gesù {{ $page->pbuh() }} sia stato uno dei servi di Allah e uno dei Suoi nobili Messaggeri. Allah inviò Gesù ai Figli di Israele per invitarli a credere solo in Allah e ad adorare solo Lui.

Allah sostenne Gesù {{ $page->pbuh() }} con miracoli, dimostrando che il Suo inviato stava dicendo la verità. Gesù nacque dalla Vergine Maria, senza padre. Egli permise agli ebrei alcune delle cose che erano state loro proibite, ma non morì e i suoi nemici, gli ebrei, non lo uccisero, anzi Allah lo salvò da loro elevandolo vivo in cielo. Gesù parlò ai suoi seguaci della prossima venuta del Profeta Muhammad {{ $page->pbuh() }}. Gesù tornerà sulla terra alla fine dei tempi. Nel Giorno della Resurrezione, smentirà le affermazioni secondo cui egli sarebbe stato un Dio.

Nel Giorno della Resurrezione, Gesù si troverà davanti al Signore dei Mondi Che gli chiederà, davanti a testimoni, cosa disse ai Figli di Israele, come afferma Allah:

{{ $page->verse('5:116-118') }}

Un'analisi sincera indica che Gesù {{ $page->pbuh() }} non ha mai detto esplicitamente e inequivocabilmente di essere Dio. Sono stati gli autori del Nuovo Testamento, nessuno dei quali ha mai incontrato Gesù, ad affermare che era Dio, trasmettendolo alle generazioni successive. Non esiste alcuna prova che i veri apostoli di Gesù abbiano mai affermato ciò, o che siano stati loro gli autori della Bibbia. Piuttosto, Gesù è sempre stato descritto mentre adora, e chiama gli altri ad adorare, solo Dio.

L'affermazione secondo cui Gesù {{ $page->pbuh() }} sia Dio non può essere attribuita a Gesù poiché contraddice nettamente il Primo Comandamento, che ordina di non prendere altri dèi all'infuori di Dio, o di considerare qualsiasi cosa come un idolo, né in cielo né in terra. Inoltre, seguendo la logica, sarebbe assurdo che Dio diventi la Sua creazione.

Quindi, soltanto l'Islam presenta un'argomentazione plausibile e coerente con le prime rivelazioni: Gesù {{ $page->pbuh() }} era un profeta, non Dio.
