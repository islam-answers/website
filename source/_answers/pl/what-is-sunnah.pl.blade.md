---
extends: _layouts.answer
section: content
title: Czym jest Sunna?
date: 2025-01-06
description: Sunna, natchniona przez Boga, uzupełnia Koran, wyjaśniając i rozszerzając
  jego postanowienia w odniesieniu do praktyki islamskiej.
sources:
- href: https://islamqa.info/en/answers/77243/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145520/
  title: islamqa.info
translator_id: 584496
---

Sunnah – co oznacza słowa i czyny przypisywane Prorokowi Muhammadowi {{ $page->pbuh() }} a także działania, które on zatwierdził – jest jedną z dwóch części boskiego objawienia (Wahy), które zostały objawione Posłańcowi Allaha {{ $page->pbuh() }}. Drugą częścią objawienia jest Święty Koran. Allah mówi:

{{ $page->verse('53:3-4') }}

Przekazano, że Wysłannik Allaha {{ $page->pbuh() }} powiedział:

{{ $page->hadith('ibnmajah:12') }}

Hassaan ibn 'Atiyah powiedział:

> Jibreel (anioł Gabriel) przynosił sunnę Prorokowi {{ $page->pbuh() }}, tak jak przynosił mu Koran.

Znaczenie Sunny polega przede wszystkim na tym, że wyjaśnia ona Koran i jest komentarzem do niego, a następnie dodaje pewne orzeczenia do tych w Koranie. Allah mówi:

{{ $page->verse('..16:44') }}

Ibn 'Abd al-Barr powiedział:

> Komentarze Proroka {{ $page->pbuh() }} do Koranu są dwojakiego rodzaju: 
> 
> 1. Wyjaśnienie rzeczy, które są wymienione w sposób ogólny w Świętym Koranie, takich jak pięć codziennych modlitw, ich pory, pokłony, kłanianie się i wszystkie inne zasady. 
> 1. Dodanie zasad do zasad Koranu, takich jak zakaz bycia żonatym z kobietą i jednocześnie z jej ciotką ze strony ojca lub matki.

Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('bukhari:3461') }}
