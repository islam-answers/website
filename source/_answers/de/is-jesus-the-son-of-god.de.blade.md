---
extends: _layouts.answer
section: content
title: Ist Jesus der Sohn Gottes?
date: 2024-12-28
description: Jesus gilt als großer Prophet und Gesandter Gottes, jedoch nicht als
  Gott oder Sohn Gottes; vielmehr ist er ein Diener und Gesandter unter den Menschen.
sources:
- href: https://islamqa.info/en/answers/26301/
  title: islamqa.info
- href: https://islamqa.info/en/answers/82361/
  title: islamqa.info
translator_id: 587746
---

Muslime betrachten Jesus als einen verehrten Propheten und Gesandten Gottes, der durch ein Wunder der Jungfrau Maria geboren wurde. Der Kernglaube, der das islamische Verständnis von der christlichen Lehre unterscheidet, ist jedoch die Ablehnung, dass Jesus göttlich oder der buchstäbliche Sohn Gottes ist.

### Die Einheit Gottes

Im Mittelpunkt des islamischen Glaubens steht Tawhid (die Einheit Gottes). Er betont, dass es außer Gott keine Gottheit gibt und dass er weder zeugt noch gezeugt wird. Folglich widerspricht es dem Grundprinzip des Monotheismus im Islam, Jesus Göttlichkeit zuzuschreiben oder ihm den Titel Gottessohn zu verleihen.

Einer der wichtigsten Grundsätze des Glaubens an Allah ist es, zu erklären, dass Allah über allen Attributen steht, die Unzulänglichkeiten implizieren. Eines der Attribute, die Unzulänglichkeiten implizieren, die der Muslim zurückweisen muss, ist die Vorstellung, dass Allah einen Sohn hat, denn das impliziert, dass er ein Bedürfnis hat und dass es ein Wesen gibt, das ihm ähnlich ist, und das sind Dinge, über die Allah weit erhaben ist. Allah sagt im Quran:

{{ $page->verse('112:1-4') }}

Der Quran geht explizit auf die falsche Vorstellung von der Göttlichkeit Jesu ein und betont seinen menschlichen Status und seine Dienstbarkeit gegenüber Gott. Allah sagt:

{{ $page->verse('5:75') }}

Die Tatsache, dass sie beide aßen, bedeutet, dass sie Nahrung brauchten, um sich zu ernähren. Gelehrten zufolge bedeutet dies, dass Jesus und seine Mutter sich erleichtern mussten, nachdem das Essen verdaut war. Der allmächtige Allah steht weit über der Abhängigkeit von Nahrung oder dem Gang zur Toilette. Allah sagt auch:

{{ $page->verse('4:171') }}

### Prüfung der Gültigkeit christlicher Schriften

Die Muslime glauben, dass die Evangelien, die heute in den Händen der Menschen sind und an die die Christen glauben, manipuliert und verändert wurden und immer noch von Zeit zu Zeit manipuliert werden, so dass nichts mehr von der Form übrig ist, in der das Evangelium ursprünglich von Allah offenbart wurde.

Das Evangelium, das am meisten über den Glauben an die Dreifaltigkeit und die Göttlichkeit Jesu {{ $page->pbuh() }} spricht und das für die Christen zu einem Bezugspunkt bei ihren Argumenten zur Unterstützung dieser Unwahrheit geworden ist, ist das Johannesevangelium. Dieses Evangelium wird sogar von einigen christlichen Gelehrten selbst in Bezug auf seine Urheberschaft angezweifelt - was bei den anderen Evangelien, an die sie glauben, nicht der Fall ist.

Die Encyclopaedia Britannica erwähnt:

> Das Johannesevangelium ist zweifellos eine Fälschung. Sein Autor wollte zwei Jünger gegeneinander aufhetzen, nämlich Johannes und Matthäus. 
 > Dieser im Text erscheinende Autor behauptete, er sei der Jünger, den der Messias geliebt habe, und die Kirche nahm dies für bare Münze und bestätigte, dass der Autor der Jünger Johannes sei, und setzte seinen Namen unter das Buch, obwohl der Autor mit Sicherheit nicht Johannes war.

### Was bedeutet „Sohn Gottes“ in der Bibel?

Die Bibel, in der es heißt, dass Jesus der Sohn Gottes ist, ist dieselbe Bibel, in der die Abstammung des Messias mit Adam endet {{ $page->pbuh() }}, und auch er wird als Sohn Gottes bezeichnet:

> Jesus selbst war etwa dreißig Jahre alt, als er sein Amt antrat. Er war, so glaubte man, der Sohn von Josef, dem Sohn Elis … des Sohnes Seths, des Sohnes Adams, des Sohnes Gottes. (Lukas 3:23-38)

Dies ist dieselbe Bibel, die Israel (Jakob) mit denselben Worten beschreibt:

> Dann sag zum Pharao: "So spricht der Herr: Israel ist mein erstgeborener Sohn." (Exodus 4:22)

Dasselbe wird von Salomon {{ $page->pbuh() }} gesagt:

> Er sagte zu mir: „Salomo, dein Sohn, ist derjenige, der mein Haus und meine Höfe bauen wird, denn ich habe ihn erwählt, mein Sohn zu sein, und ich werde sein Vater sein.“ (1. Chronik 28:6)

Waren Adam, Israel und Salomon vor Jesus {{ $page->pbuh() }} alle anderen Söhne Gottes? Allah sei weit über das erhaben, was sie sagen! Tatsächlich gibt es im Johannesevangelium selbst eine Erklärung, was mit „Sohn sein“ gemeint ist; es schließt alle rechtschaffenen Diener Gottes ein, also ist Jesus oder irgendein anderer Prophet in dieser Hinsicht nichts Einzigartiges:

> Allen aber, die ihn aufnahmen, gab er Macht, Kinder Gottes zu werden, denen, die an seinen Namen glauben. (Johannes 1:12)

Etwas Ähnliches findet sich im Matthäus-Evangelium:

> Selig sind die Friedfertigen; denn sie werden Söhne Gottes genannt werden. (Matthäus 5:8-9)

Diese Verwendung des Wortes „Sohn“ in der Sprache der Bibel ist eine Metapher für den rechtschaffenen Diener Gottes, ohne dass dies etwas Besonderes oder Einzigartiges hinsichtlich seiner Erschaffung bedeutet oder ihn buchstäblich als Nachkommen Gottes beschreibt. Daher sagt Johannes:

> Wie groß ist die Liebe, die der Vater uns geschenkt hat, dass wir Kinder Gottes heißen sollen! (1. Johannes 3:1)

Es bleibt die Frage, ob Jesus {{ $page->pbuh() }} als Sohn Gottes beschrieben wurde, und was sie über den Herrn der Welten erfunden haben, indem sie sagten, er sei der Vater von Jesus, dem Messias {{ $page->pbuh() }}. Auch das ist in der Sprache der Evangelien nicht einzigartig:

> Jesus sagte: Halte mich nicht fest, denn ich bin noch nicht zum Vater zurückgekehrt. Geh stattdessen zu meinen Brüdern und sage ihnen: „Ich kehre zu meinem Vater und eurem Vater, zu meinem Gott und eurem Gott zurück.“ (Johannes 20:17)

In diesem Vers sagt Jesus, dass Gott auch für sie ein Vater ist und dass Gott der Gott von allen ist. Der Quran betont Gottes absolute Einzigartigkeit und Vollkommenheit und lehnt jegliche Verbindung von Partnern oder Nachkommen mit ihm ab. Der Begriff „Sohn Gottes“ ist in der Sprache der Bibel metaphorisch und bedeutet Rechtschaffenheit, nicht Göttlichkeit. Daher halten Muslime an Jesu edlem Status fest und bekräftigen gleichzeitig die Einzigartigkeit Gottes als Kern ihres Glaubens.
