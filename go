<?php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Commands\PullCommand;
use App\Commands\PushCommand;
use App\Commands\AutoAssignTasksCommand;
use App\Commands\FixYamlMetadata;
use App\Commands\CreateAnswerCommand;
use App\Commands\MigrateTranslationsCommand;

$application = new Application();

$application->add(new PullCommand());
$application->add(new PushCommand());
$application->add(new AutoAssignTasksCommand());
$application->add(new FixYamlMetadata());
$application->add(new CreateAnswerCommand());
$application->add(new MigrateTranslationsCommand());

$application->run();
