<?php

return [
    /** Quran **/

    "who-wrote-the-quran" => [
        "what-is-the-quran-about",
        "who-is-the-prophet-muhammad",
        "what-is-islam",
        "was-the-quran-copied-from-the-bible",
        "what-is-sunnah",
    ],

    "what-is-the-quran-about" => [
        "who-wrote-the-quran",
        "who-is-the-prophet-muhammad",
        "what-is-islam",
        "was-the-quran-copied-from-the-bible",
        "does-islam-contradict-science",
    ],

    "was-the-quran-copied-from-the-bible" => [
        "who-wrote-the-quran",
        "what-is-the-quran-about",
        "what-does-islam-say-about-evolution",
        "does-islam-contradict-science",
        "what-do-muslims-believe-about-jesus",
    ],

    /** Allah **/

    "is-allah-different-from-god" => [
        "is-islam-only-for-arabs",
        "what-do-muslims-believe-about-jesus",
        "is-jesus-the-son-of-god",
        "what-is-islam",
        "who-wrote-the-quran",
    ],

    /** Terminology **/

    "what-does-inshallah-mean" => [
        "what-does-alhamdulillah-mean",
        "what-does-subhanallah-mean",
        "what-does-bismillah-mean",
        "what-does-assalamu-alaykum-mean",
        "what-is-islam",
    ],

    "what-does-assalamu-alaykum-mean" => [
        "what-does-subhanallah-mean",
        "what-does-inshallah-mean",
        "what-does-bismillah-mean",
        "what-does-alhamdulillah-mean",
        "what-is-islam",
    ],

    "what-does-bismillah-mean" => [
        "what-does-subhanallah-mean",
        "what-does-inshallah-mean",
        "what-does-assalamu-alaykum-mean",
        "what-does-alhamdulillah-mean",
        "what-is-islam",
    ],

    "what-does-subhanallah-mean" => [
        "what-does-bismillah-mean",
        "what-does-inshallah-mean",
        "what-does-alhamdulillah-mean",
        "what-does-assalamu-alaykum-mean",
        "what-is-islam",
    ],

    "what-does-alhamdulillah-mean" => [
        "what-does-subhanallah-mean",
        "what-does-bismillah-mean",
        "what-does-inshallah-mean",
        "what-does-assalamu-alaykum-mean",
        "what-is-islam",
    ],

    "what-is-shariah-law" => [
        "what-is-halal-food",
        "why-does-islam-not-allow-pork",
        "what-is-sunnah",
        "what-is-islam",
        "what-is-jihad",
    ],

    "what-is-sunnah" => [
        "what-is-shariah-law",
        "who-is-the-prophet-muhammad",
        "who-wrote-the-quran",
        "what-is-islam",
        "what-is-halal-food",
    ],

    "what-is-halal-food" => [
        "why-do-muslims-fast",
        "why-cant-muslims-drink-alcohol",
        "what-is-shariah-law",
        "why-does-islam-not-allow-pork",
        "is-halal-slaughter-cruel",
    ],

    /** Jesus (Peace be upon him) **/

    "what-do-muslims-believe-about-jesus" => [
        "is-islam-only-for-arabs",
        "is-allah-different-from-god",
        "was-jesus-crucified",
        "did-jesus-claim-to-be-god",
        "why-dont-muslims-celebrate-christmas",
    ],

    "was-jesus-crucified" => [
        "what-do-muslims-believe-about-jesus",
        "did-jesus-claim-to-be-god",
        "do-muslims-believe-in-virgin-mary",
        "are-the-jews-the-chosen-people",
        "is-jesus-the-son-of-god",
    ],

    "did-jesus-claim-to-be-god" => [
        "was-jesus-crucified",
        "what-do-muslims-believe-about-jesus",
        "do-muslims-believe-in-virgin-mary",
        "is-jesus-the-son-of-god",
        "why-dont-muslims-celebrate-christmas",
    ],

    "do-muslims-believe-in-virgin-mary" => [
        "did-jesus-claim-to-be-god",
        "was-jesus-crucified",
        "what-do-muslims-believe-about-jesus",
        "why-dont-muslims-celebrate-christmas",
        "is-jesus-the-son-of-god",
    ],

    "is-jesus-the-son-of-god" => [
        "was-jesus-crucified",
        "what-do-muslims-believe-about-jesus",
        "are-the-jews-the-chosen-people",
        "did-jesus-claim-to-be-god",
        "do-muslims-believe-in-virgin-mary",
    ],

    /** Muhammad (Peace be upon him) **/

    "who-is-the-prophet-muhammad" => [
        "who-wrote-the-quran",
        "what-is-sunnah",
        "why-did-allah-send-prophet-muhammad",
        "what-is-islam",
        "what-is-the-quran-about",
    ],

    "why-did-prophet-muhammad-marry-a-young-girl" => [
        "who-is-the-prophet-muhammad",
        "what-is-the-status-of-women-in-islam",
        "who-wrote-the-quran",
        "what-is-sunnah",
        "what-is-shariah-law",
    ],

    "why-did-allah-send-prophet-muhammad" => [
        "who-is-the-prophet-muhammad",
        "what-is-islam",
        "what-is-the-quran-about",
        "what-is-sunnah",
        "what-is-shariah-law",
    ],

    /** Israelites **/

    "are-the-jews-the-chosen-people" => [
        "who-is-the-prophet-muhammad",
        "was-jesus-crucified",
        "does-islam-promote-racism",
        "what-do-muslims-believe-about-jesus",
        "is-islam-only-for-arabs",
    ],

    /** Science **/

    "what-does-islam-say-about-evolution" => [
        "does-islam-contradict-science",
        "what-is-shariah-law",
        "what-is-the-quran-about",
        "what-is-islam",
        "who-wrote-the-quran",
    ],

    "does-islam-contradict-science" => [
        "what-does-islam-say-about-evolution",
        "what-is-islam",
        "what-is-the-quran-about",
        "who-wrote-the-quran",
        "was-the-quran-copied-from-the-bible",
    ],

    /** Jihad **/

    "what-does-islam-say-about-terrorism" => [
        "what-is-jihad",
        "what-is-islam",
        "why-did-prophet-muhammad-marry-a-young-girl",
        "what-is-shariah-law",
        "does-islam-promote-racism",
    ],

    "what-is-jihad" => [
        "what-is-the-quran-about",
        "what-does-islam-say-about-terrorism",
        "what-is-islam",
        "what-is-shariah-law",
        "who-wrote-the-quran",
    ],

    /** Relationships **/

    "does-islam-permit-forced-marriages" => [
        "what-is-the-status-of-women-in-islam",
        "what-is-shariah-law",
        "what-is-the-quran-about",
        "are-men-and-women-equal-in-islam",
        "why-are-muslims-not-allowed-to-date",
    ],

    "why-are-muslims-not-allowed-to-date" => [
        "what-is-shariah-law",
        "why-does-islam-not-allow-pork",
        "what-is-the-status-of-women-in-islam",
        "what-does-mahram-mean-in-islam",
        "are-muslims-allowed-to-talk-to-the-opposite-gender",
    ],

    "what-does-islam-say-about-treatment-to-parents" => [
        "what-is-the-quran-about",
        "what-is-islam",
        "what-does-mahram-mean-in-islam",
        "what-is-shariah-law",
        "what-is-sunnah",
    ],

    "what-does-mahram-mean-in-islam" => [
        "are-muslims-allowed-to-talk-to-the-opposite-gender",
        "what-is-shariah-law",
        "what-is-the-status-of-women-in-islam",
        "why-are-muslims-not-allowed-to-date",
        "what-is-islam",
    ],

    "are-muslims-allowed-to-talk-to-the-opposite-gender" => [
        "why-are-muslims-not-allowed-to-date",
        "what-does-mahram-mean-in-islam",
        "what-is-islam",
        "what-is-shariah-law",
        "what-is-the-status-of-women-in-islam",
    ],

    /** Women **/

    "what-is-the-status-of-women-in-islam" => [
        "what-is-wrong-with-modern-feminism",
        "does-islam-permit-forced-marriages",
        "why-do-muslim-women-wear-hijab",
        "why-are-muslims-not-allowed-to-date",
        "are-men-and-women-equal-in-islam",
        "what-does-mahram-mean-in-islam",
    ],

    "why-do-muslim-women-wear-hijab" => [
        "what-is-the-status-of-women-in-islam",
        "what-is-wrong-with-modern-feminism",
        "do-muslims-believe-in-virgin-mary",
        "why-do-muslim-men-have-beards",
        "are-men-and-women-equal-in-islam",
        "what-is-shariah-law",
    ],

    "are-men-and-women-equal-in-islam" => [
        "what-is-wrong-with-modern-feminism",
        "why-do-muslim-women-wear-hijab",
        "why-are-muslims-not-allowed-to-date",
        "what-is-the-status-of-women-in-islam",
        "does-islam-permit-forced-marriages",
        "what-is-shariah-law",
    ],

    /** Basics **/

    "what-is-islam" => [
        "who-wrote-the-quran",
        "what-is-the-quran-about",
        "who-is-the-prophet-muhammad",
        "why-is-islam-growing-so-rapidly",
        "what-are-the-main-practices-of-islam",
    ],

    "what-are-the-main-practices-of-islam" => [
        "how-does-someone-become-a-muslim",
        "do-muslims-worship-the-kaaba",
        "what-is-halal-food",
        "why-do-muslims-fast",
        "what-is-islam",
    ],

    "why-do-muslims-fast" => [
        "what-holidays-do-muslims-celebrate",
        "what-are-the-main-practices-of-islam",
        "what-is-halal-food",
        "what-is-islam",
        "what-is-sunnah",
    ],

    "what-holidays-do-muslims-celebrate" => [
        "what-is-halal-food",
        "why-do-muslims-fast",
        "why-dont-muslims-celebrate-christmas",
        "what-are-the-main-practices-of-islam",
        "what-is-islam",
    ],

    "is-halal-slaughter-cruel" => [
        "what-is-halal-food",
        "what-is-the-quran-about",
        "what-are-the-main-practices-of-islam",
        "what-is-islam",
        "what-is-shariah-law",
    ],

    "do-muslims-worship-the-kaaba" => [
        "what-are-the-main-practices-of-islam",
        "what-is-islam",
        "what-is-the-quran-about",
        "what-is-sunnah",
        "who-is-the-prophet-muhammad",
    ],

    /** Festivals **/

    "why-dont-muslims-celebrate-christmas" => [
        "what-holidays-do-muslims-celebrate",
        "why-do-muslims-fast",
        "what-do-muslims-believe-about-jesus",
        "what-is-islam",
        "is-jesus-the-son-of-god",
    ],

    "why-was-february-14-chosen-for-valentines-day" => [
        "why-dont-muslims-celebrate-christmas",
        "what-holidays-do-muslims-celebrate",
        "what-is-islam",
        "what-do-muslims-believe-about-jesus",
        "what-is-the-quran-about",
    ],

    /** Conversion **/

    "how-does-someone-become-a-muslim" => [
        "do-you-need-witnesses-to-take-shahadah",
        "is-islam-only-for-arabs",
        "are-all-sins-forgiven-after-converting-to-islam",
        "who-can-become-a-muslim",
        "what-is-islam",
    ],

    "is-islam-only-for-arabs" => [
        "how-does-someone-become-a-muslim",
        "does-islam-promote-racism",
        "do-you-have-to-learn-arabic-to-be-muslim",
        "what-is-islam",
        "who-can-become-a-muslim",
    ],

    "do-you-have-to-learn-arabic-to-be-muslim" => [
        "is-islam-only-for-arabs",
        "how-does-someone-become-a-muslim",
        "are-all-sins-forgiven-after-converting-to-islam",
        "who-can-become-a-muslim",
        "what-is-islam",
    ],

    "who-can-become-a-muslim" => [
        "how-does-someone-become-a-muslim",
        "is-islam-only-for-arabs",
        "are-all-sins-forgiven-after-converting-to-islam",
        "do-you-have-to-learn-arabic-to-be-muslim",
        "what-is-islam",
    ],

    "does-islam-force-people-to-become-muslim" => [
        "how-does-someone-become-a-muslim",
        "is-it-difficult-to-become-a-muslim",
        "are-all-sins-forgiven-after-converting-to-islam",
        "who-can-become-a-muslim",
        "what-is-islam",
    ],

    "do-you-need-witnesses-to-take-shahadah" => [
        "how-does-someone-become-a-muslim",
        "is-islam-only-for-arabs",
        "are-all-sins-forgiven-after-converting-to-islam",
        "who-can-become-a-muslim",
        "what-is-islam",
    ],

    "is-it-ok-to-delay-conversion-to-islam" => [
        "how-does-someone-become-a-muslim",
        "do-you-need-witnesses-to-take-shahadah",
        "are-all-sins-forgiven-after-converting-to-islam",
        "is-it-difficult-to-become-a-muslim",
        "who-can-become-a-muslim",
    ],

    "is-it-difficult-to-become-a-muslim" => [
        "do-you-need-witnesses-to-take-shahadah",
        "are-all-sins-forgiven-after-converting-to-islam",
        "is-circumcision-necessary-for-conversion-to-islam",
        "how-does-someone-become-a-muslim",
        "who-can-become-a-muslim",
    ],

    "are-all-sins-forgiven-after-converting-to-islam" => [
        "is-it-difficult-to-become-a-muslim",
        "who-can-become-a-muslim",
        "is-circumcision-necessary-for-conversion-to-islam",
        "how-does-someone-become-a-muslim",
        "what-is-islam",
    ],

    "is-circumcision-necessary-for-conversion-to-islam" => [
        "are-all-sins-forgiven-after-converting-to-islam",
        "is-it-difficult-to-become-a-muslim",
        "is-islam-only-for-arabs",
        "how-does-someone-become-a-muslim",
        "who-can-become-a-muslim",
    ],

    /** Purpose & Afterlife **/

    "how-do-muslims-view-the-purpose-of-life-and-the-hereafter" => [
        "what-is-the-muslims-outlook-on-life",
        "what-is-karma",
        "what-does-islam-say-about-the-day-of-judgment",
        "what-is-islam",
        "why-did-god-create-us-and-why-are-we-here-on-this-earth",
    ],

    "what-is-the-muslims-outlook-on-life" => [
        "what-does-islam-say-about-the-day-of-judgment",
        "how-do-muslims-view-the-purpose-of-life-and-the-hereafter",
        "what-is-karma",
        "what-is-islam",
        "why-did-god-create-us-and-why-are-we-here-on-this-earth",
    ],

    "what-does-islam-say-about-the-day-of-judgment" => [
        "what-is-the-muslims-outlook-on-life",
        "how-does-someone-become-a-muslim",
        "what-is-karma",
        "what-is-islam",
        "how-do-muslims-view-the-purpose-of-life-and-the-hereafter",
    ],

    "why-did-god-create-us-and-why-are-we-here-on-this-earth" => [
        "how-do-muslims-view-the-purpose-of-life-and-the-hereafter",
        "what-is-the-muslims-outlook-on-life",
        "what-does-islam-say-about-the-day-of-judgment",
        "what-is-karma",
        "what-is-islam",
    ],

    "what-is-karma" => [
        "what-does-islam-say-about-the-day-of-judgment",
        "what-is-the-muslims-outlook-on-life",
        "how-do-muslims-view-the-purpose-of-life-and-the-hereafter",
        "what-is-islam",
        "why-does-god-allow-suffering-and-evil",
    ],

    "why-does-god-allow-suffering-and-evil" => [
        "what-is-the-muslims-outlook-on-life",
        "how-do-muslims-view-the-purpose-of-life-and-the-hereafter",
        "what-does-islam-say-about-the-day-of-judgment",
        "what-is-karma",
        "what-is-islam",
    ],

    /** Prohibitions **/

    "why-does-islam-not-allow-pork" => [
        "what-is-shariah-law",
        "what-is-halal-food",
        "why-are-muslims-not-allowed-to-date",
        "why-cant-muslims-drink-alcohol",
        "what-is-islam",
    ],

    "why-cant-muslims-drink-alcohol" => [
        "what-is-halal-food",
        "why-does-islam-not-allow-pork",
        "what-is-shariah-law",
        "what-is-islam",
        "what-is-sunnah",
    ],

    "is-islam-against-dogs" => [
        "what-is-islam",
        "what-is-sunnah",
        "what-is-shariah-law",
        "what-is-halal-food",
        "what-is-the-quran-about",
    ],

    /** Trends **/

    "why-is-islam-growing-so-rapidly" => [
        "is-islam-only-for-arabs",
        "what-is-islam",
        "who-wrote-the-quran",
        "how-does-someone-become-a-muslim",
        "who-can-become-a-muslim",
    ],

    /** Sects **/

    "is-the-ahmadiyya-qadianiyyah-sect-muslim" => [
        "what-is-jihad",
        "what-is-islam",
        "what-do-muslims-believe-about-jesus",
        "what-is-shariah-law",
        "who-wrote-the-quran",
    ],

    /** Values & morals **/

    "does-islam-promote-racism" => [
        "is-islam-only-for-arabs",
        "are-the-jews-the-chosen-people",
        "what-is-islam",
        "what-is-shariah-law",
        "what-is-the-quran-about",
    ],

    /** Lifestyle & Sunnah **/

    "why-do-muslim-men-have-beards" => [
        "what-is-sunnah",
        "why-do-muslim-women-wear-hijab",
        "who-is-the-prophet-muhammad",
        "what-is-islam",
        "what-is-shariah-law",
    ],
];
