---
extends: _layouts.answer
section: content
title: አንድ ሰው ሙስሊም የሚሆነው እንዴት ነው?
date: 2024-07-16
description: በፅኑ እምነት "ላ ኢላሃ ኢለሏህ፤ ሙሐመዱ ረሱሉሏህ" በማለት ብቻ አንድ ሰው በቀላሉ እስልምናን ተቀብሎ ሙስሊም
  መሆን ይችላል።
sources:
- href: https://www.islam-guide.com/ch3-6.htm
  title: islam-guide.com
translator_id: 573104
---

በፅኑ እምነት "ላ ኢላሃ ኢለሏህ፤ ሙሐመዱ ረሱሉሏህ" በማለት ብቻ አንድ ሰው በቀላሉ እስልምናን በመቀበል ሙስሊም መሆን ይችላል። ይህ አባባል "ከአሏህ በስተቀር እውነተኛ አምላክ የለም፤ ሙሐመድም የአሏህ መልዕክተኛ (ነብይ) ናቸው" ማለት ነው። "ከአሏህ በስተቀር እውነተኛ አምላክ የለም" የሚለው የመጀመሪያው ክፍል፥ ከአሏህ ውጪ ሌላ ማንኛውም አካል የመመለክ ስልጣን የለውም፤ እንዲሁም አሏህ አጋርም ሆነ ልጅ የለውም ማለት ነው። በተጨማሪም፥ አንድ ሰው ሙስሊም ለመሆን፥

- ቅዱስ ቁርዓን አሏህ ያወረደው የራሱ ቀጥተኛ ቃል መሆኑን ማመን፤
- አሏህ በቁርዓን ውስጥ ቃል እንደገባው፥ የመጨረሻው የፍርድ ቀን (ሙታን ከቀብር የሚቀሰቀሱበት ቀን) እውነት እንደሆነና እንደሚመጣ ማመን፤
- እስልምናን ሐይማኖቱ/ቷ አድርጎ/ጋ መቀበል፤
- ከአሏህ በቀር ምንንም እና ማንንም አለማምለክ፤ ይኖርበታል።

ነቢዩ ሙሐመድ {{ $page->pbuh() }} እንዲህ ብለዋል፡-

{{ $page->hadith('muslim:2747') }}
