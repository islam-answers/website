---
extends: _layouts.answer
section: content
title: Does Islam permit forced marriages?
date: 2024-03-23
description: Both males and females have the right to choose their spouse, and a marriage
  is considered null if a woman's genuine approval is not granted.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
---

Arranged marriages are cultural practices which are predominant in certain countries throughout the world. Although not restricted to Muslims, forced marriages have become incorrectly associated with Islam.

In Islam, both males and females have the right to choose or reject their potential spouse, and a marriage is considered null and void if a woman's genuine approval is not granted prior to the marriage.
