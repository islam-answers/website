<?php

return [
    "what-do-muslims-believe-about-jesus" => [
        "groups" => ["europe", "americas", "africa"],
    ],

    "was-jesus-crucified" => [
        "groups" => ["europe", "americas", "africa"],
    ],

    "why-dont-muslims-celebrate-christmas" => [
        "groups" => ["europe", "americas", "africa"],
    ],

    "did-jesus-claim-to-be-god" => [
        "groups" => ["europe", "americas", "africa"],
    ],

    "do-muslims-believe-in-virgin-mary" => [
        "groups" => ["europe", "americas", "africa"],
    ],

    "is-jesus-the-son-of-god" => [
        "groups" => ["europe", "americas", "africa"],
    ],

    "is-the-ahmadiyya-qadianiyyah-sect-muslim" => [
        "groups" => ["europe", "americas", "africa", "hinduism"],
    ],
];
