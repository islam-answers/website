---
extends: _layouts.answer
section: content
title: Czy islam promuje rasizm?
date: 2024-10-10
description: Islam nie zwraca uwagi na różnice w kolorze skóry, rasie czy pochodzeniu.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 583847
---

Wszyscy ludzie są potomkami jednego mężczyzny i jednej kobiety, wierzących i kaafirów (niewierzących), czarnych i białych, Arabów i nie-Arabów, bogatych i biednych, szlachetnych i pokornych.

Islam nie zwraca uwagi na różnice w kolorze skóry, rasie czy pochodzeniu. Wszyscy ludzie pochodzą od Adama, a Adam został stworzony z prochu. W islamie rozróżnienie między ludźmi opiera się na wierze (Iman) i pobożności (Taqwa), poprzez czynienie tego, co Allah nakazał i unikanie tego, co Allah zakazał. Allah mówi:

{{ $page->verse('49:13') }}

Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:2564b') }}

Islam uważa wszystkich ludzi za równych pod względem praw i obowiązków. Ludzie są równi wobec prawa (Szariatu), jak mówi Allah:

{{ $page->verse('16:97') }}

Wiara, prawdomówność i pobożność prowadzą do Raju, który jest prawem tego, kto posiada te atrybuty, nawet jeśli jest jednym z najsłabszych lub najniższych ludzi. Allah mówi:

{{ $page->verse('..65:11') }}

Kufr (niewiara), arogancja i ucisk prowadzą do Piekła, nawet jeśli ten, kto robi te rzeczy, jest jednym z najbogatszych lub najszlachetniejszych ludzi. Allah mówi:

{{ $page->verse('64:10') }}

Doradcy Proroka Muhammada {{ $page->pbuh() }} obejmowali muzułmanów ze wszystkich plemion, ras i kolorów. Ich serca były wypełnione Tawhid (monoteizmem) i łączyła ich wiara i pobożność — tacy jak Abu Bakr z Kurajszytów, 'Ali ibn Abi Taalib z Bani Haashim, Bilal Etiopczyk, Suhayb Rzymianin, Salman Pers, bogaci ludzie jak 'Uthman i biedni ludzie jak 'Ammar, ludzie zamożni i ludzie biedni jak Ahl al-Suffah, i inni.

Oni wierzyli w Allaha i walczyli dla Niego, aż Allah i Jego Posłaniec byli z nich zadowoleni. Oni byli prawdziwymi wierzącymi.

{{ $page->verse('98:8') }}
