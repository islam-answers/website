---
extends: _layouts.answer
section: content
title: Cine este Profetul Muhammad?
date: 2024-02-27
description: Profetul Muhammad (pacea fie asupra lui) a fost un exemplu perfect de
  ființă umană cinstită, dreaptă, milostivă, plină de compasiune, sincerǎ și curajoasă.
sources:
- href: https://www.islam-guide.com/ch3-8.htm
  title: islam-guide.com
translator_id: 554943
---

Muhammad {{ $page->pbuh() }} s-a născut în Mecca în anul 570. Deoarece tatăl său a murit înainte de nașterea lui și mama sa a murit la scurt timp după aceea, el a fost crescut de unchiul său, care provenea din respectatul trib al Quraysh. A fost crescut analfabet, incapabil să citească sau să scrie și a rămas așa până la moarte. Poporul lui, înainte de misiunea sa de profet, nu cunoștea știința și majoritatea erau analfabeți. Pe măsură ce a crescut, a devenit cunoscut ca fiind sincer, demn de încredere, generos și onest. Era atât de demn de încredere încât l-au numit „Cel demn de încredere”. Muhammad {{ $page->pbuh() }} era foarte religios și detestase de mult decadența și idolatria societății sale.

La vârsta de patruzeci de ani, Muhammad {{ $page->pbuh() }} a primit prima sa revelație de la Dumnezeu prin Îngerul Gabriel. Revelațiile au continuat timp de douăzeci și trei de ani și sunt cunoscute în mod colectiv sub numele de Coran.

De îndată ce a început să recite Coranul și să predice adevărul pe care i-l revelase Dumnezeu, el și grupul său mic de adepți au suferit persecuție din partea necredincioșilor. Persecuția a devenit atât de aprigă încât în anul 622 Dumnezeu le-a dat porunca să emigreze. Această emigrare de la Mecca către orașul Medina, la aproximativ 260 de mile spre nord, marchează începutul calendarului musulman.

După câțiva ani, Profetul Muhammad {{ $page->pbuh() }} și adepții săi au putut să se întoarcă la Mecca, unde și-au iertat dușmanii. Înainte de moartea lui Profetul Muhammad {{ $page->pbuh() }}, la vârsta de șaizeci și trei de ani, cea mai mare parte a Peninsulei Arabe devenise musulmană și, în decurs de un secol de la moartea sa, Islamul se răspândise în Spania, în vest și în est, până în China. Printre motivele răspândirii rapide și pașnice a Islamului a fost adevărul și claritatea doctrinei sale. Islamul cere credință într-un singur Dumnezeu, Care este singurul demn de închinare.

Profetul Muhammad {{ $page->pbuh() }} a fost un exemplu perfect de ființă umană cinstită, dreaptă, milostivă, plină de compasiune, sincerǎ și curajoasă. Deși era om, el era departe de toate caracteristicile rele și s-a străduit numai de dragul lui Dumnezeu și a răsplatei Sale în Viața de Apoi. Mai mult decât atât, în toate acțiunile sale, el a fost mereu conștient și temǎtor de Dumnezeu.
