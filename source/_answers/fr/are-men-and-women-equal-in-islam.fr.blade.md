---
extends: _layouts.answer
section: content
title: Les hommes et les femmes sont-ils égaux en Islam ?
date: 2024-11-26
description: En islam, les hommes et les femmes sont spirituellement égaux mais ont des rôles complémentaires, assurant équilibre, justice et respect mutuel.
sources:
- href: https://islamqa.info/en/answers/12840/
  title: islamqa.info
- href: https://islamqa.org/hanafi/fatwacentre/179051/
  title: islamqa.org (FatwaCentre.org)
- href: https://www.islamweb.net/en/article/109364/
  title: islamweb.net
translator_id: 586055
---

L'islam est venu honorer les femmes et élever leur statut, leur donner une position qui leur convient, prendre soin d'elles et protéger leur dignité. L'islam ordonne donc aux tuteurs et aux maris des femmes de dépenser de l'argent pour elles, de bien les traiter, de prendre soin d'elles et d'être gentils avec elles. Allah dit dans le Coran :

{{ $page->verse('..4:19..') }}

On rapporte que le Prophète {{ $page->pbuh() }} a dit :

{{ $page->hadith('tirmidhi:3895') }}

L'Islam donne aux femmes tous leurs droits et leur permet d'organiser leurs affaires de manière appropriée. Cela inclut toutes sortes de transactions, l'achat, la vente, la désignation d'autres personnes pour agir en leur nom, le prêt, le dépôt de fonds, etc. Allah dit :

{{ $page->verse('..2:228') }}

L'Islam a imposé aux femmes les actes d'adoration et les devoirs qui leur conviennent, les mêmes devoirs que les hommes, à savoir la purification, la Zakat (charité prescrite), le jeûne, la prière, le Hajj (pèlerinage) et d'autres actes d'adoration.

Mais l'Islam accorde à la femme la moitié de la part de l'homme en matière d'héritage, car elle n'est pas obligée de dépenser pour elle-même, pour sa maison ou pour ses enfants. C'est plutôt l'homme qui est obligé de dépenser pour eux.

En ce qui concerne le fait que le témoignage de deux femmes équivaut au témoignage d'un homme dans certains cas, c'est parce que les femmes ont tendance à être plus distraites que les hommes en raison de leurs cycles naturels de menstruation, de grossesse, d'accouchement, d'éducation des enfants, etc. Toutes ces choses les préoccupent et les rendent oublieuses. Toutes ces choses les préoccupent et les rendent oublieuses. C'est pourquoi les preuves shar'i indiquent qu'une autre femme devrait renforcer le témoignage d'une femme, afin qu'il soit plus précis. Mais il y a des questions qui ne concernent que les femmes et pour lesquelles le témoignage d'une seule femme est suffisant, comme déterminer combien de fois un enfant a été allaité, les défauts qui peuvent affecter le mariage, et ainsi de suite.

Les femmes sont égales aux hommes en termes de récompense, de persévérance dans la foi et d'accomplissement des bonnes œuvres, de jouissance d'une bonne vie ici-bas et d'une grande récompense dans l'au-delà. Allah dit :

{{ $page->verse('16:97') }}

Les femmes ont des droits et des devoirs, tout comme les hommes ont des droits et des devoirs. Il y a des choses qui conviennent à l’homme, et Allah en a fait sa responsabilité. Tout comme il y a des choses qui conviennent à la femme, Il en a fait sa responsabilité.
