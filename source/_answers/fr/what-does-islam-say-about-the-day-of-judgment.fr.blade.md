---
extends: _layouts.answer
section: content
title: Que dit l'islam sur le Jour du Jugement ?
date: 2024-06-08
description: Un jour viendra où tout l'univers sera détruit et les morts seront ressuscités
  afin d'être jugés par Dieu.
sources:
- href: https://www.islam-guide.com/fr/ch3-5.htm
  title: islam-guide.com
---

Comme les chrétiens, les musulmans croient que la vie d'ici-bas n'est qu'une épreuve préparatoire en vue de l'existence de l'au-delà. Pour chaque individu, cette vie est un test pour la vie qui l'attend après sa mort. Un jour viendra où tout l'univers sera détruit et les morts seront ressuscités afin d'être jugés par Dieu. Ce jour-là sera le début d'une vie qui ne finira jamais. Ce jour-là est le Jour du Jugement, au cours duquel tous les gens seront récompensés par Dieu en fonction de leurs croyances et leurs actions. Ceux qui meurent tout en croyant que "Il n'y a pas d'autre dieu que Dieu et Mohammed est Son messager" et en étant musulmans seront récompensés ce jour-là et seront admis au Paradis où ils demeureront éternellement, tel que Dieu l'a dit:

{{ $page->verse('2:82') }}

Mais ceux qui meurent tout en ne croyant pas que "Il n'y a pas d'autre dieu que Dieu et Mohammed est Son messager", ou en n'étant pas musulmans, perdront pour toujours la chance d'être admis au Paradis et seront envoyés en Enfer, tel que Dieu l'a dit:

{{ $page->verse('3:85') }}

Et Il a aussi dit:

{{ $page->verse('3:91') }}

Certaines personnes disent: "Je crois que l'islam est une bonne religion, mais si je me convertissais à l'islam, ma famille, mes amis, et les autres gens me persécuteraient et se moqueraient de moi. Donc si je ne me convertis pas à l'islam, est-ce que j'entrerai quand même au Paradis et est-ce que je serai sauvé du feu de l'Enfer?"

La réponse à cela est ce que Dieu a dit dans le verset mentionné plus haut: "Et quiconque désire une religion autre que l'islam ne sera point agréé, et il sera, dans l'au-delà, parmi les perdants."

Après avoir envoyé le prophète Mohammed {{ $page->pbuh() }} pour appeler les gens à l'islam, Dieu n'accepte pas que les gens adhèrent à une religion autre que l'islam. Dieu est notre Créateur et Celui qui pourvoit à nos besoins. Il a créé, pour nous, tout ce qu'il y a sur la terre. Toutes les grâces et les bonnes choses dont nous profitons viennent de Lui. Donc après tout cela, lorsque quelqu'un refuse de croire en Dieu, au prophète Mohammed {{ $page->pbuh() }} ou à l'islam, il est juste qu'il ou qu'elle soit puni(e) dans l'au-delà. En fait, le but principal de notre création est d'adorer Dieu seul et de Lui obéir, tel que Dieu l'a dit dans le Coran (51:56).

Cette vie que nous vivons en ce moment est très courte. Les mécréants, au Jour du Jugement, croiront que la vie qu'ils ont vécue sur terre n'aura duré que l'espace d'un jour, ou d'une partie d'un jour, comme Dieu l'a dit:

{{ $page->verse('23:112-113..') }}

Et Il a dit:

{{ $page->verse('23:115-116..') }}

La vie dans l'au-delà est tout à fait réelle. Ce n'est pas qu'une vie spirituelle, c'est aussi une vie physique. Nous vivrons dans l'au-delà avec nos âmes et nos corps.

En comparant le monde d'ici-bas avec l'au-delà, le prophète Mohammed {{ $page->pbuh() }} a dit:

{{ $page->hadith('muslim:2858') }}

La signification de ce hadith est que la valeur de ce monde comparée à celle de l'au-delà est comme quelques gouttes d'eau comparées à la mer tout entière.
