---
extends: _layouts.answer
section: content
title: O que significa Bismillah?
date: 2024-10-09
description: Bismillah significa "em nome de Allah". Os muçulmanos dizem-no antes
  de iniciarem uma ação com o nome de Allah, pedindo a Sua ajuda e as Suas bênçãos.
sources:
- href: https://islamqa.info/en/answers/21722/
  title: islamqa.info
- href: https://islamqa.info/en/answers/163573/
  title: islamqa.info
translator_id: 594172
---

Bismillah significa "Em nome de Allah". A forma completa é "Bismillah al-Rahman al-Rahim", que se traduz por "Em nome de Allah, o Misericordioso, o Misericordiador".

Quando alguém diz "Bismillah" ao começar a fazer qualquer coisa, o que isso significa é: inicio essa ação acompanhada pelo nome de Allah ou buscando ajuda através do nome de Allah, buscando bênção por meio disso. Allah é Deus, O amado e adorado, a quem os corações se voltam em amor, veneração e obediência (adoração). Ele é ar-Rahman (o Misericordioso), cujo atributo é uma grande misericórdia; e ar-Rahim (o Misericordiador), que faz com que essa misericórdia alcance Sua criação.

Ibn Jarir (que Allah tenha misericórdia dele) disse:

> Allah, exaltado e santificado seja o Seu nome, ensinou ao Profeta Muhammad {{ $page->pbuh() }} maneiras adequadas, ensinando-o a mencionar Seus nomes e atributos mais bonitos antes de quaisquer ações. Ele ordenou que se mencionasse esses atributos antes de começar a fazer qualquer coisa, e fez, através deste ensinamento, um exemplo para todas as pessoas seguirem antes de começarem qualquer coisa, ou iniciarem suas redações nos começos de suas cartas e livros. O significado aparente dessas palavras indica exatamente o que elas significam e não precisa ser explicado.

Os muçulmanos dizem "Bismillah" antes de comer devido ao relato narrado por Aisha (que Allah esteja satisfeito com ela), que o Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('ibnmajah:3264') }}

Há uma coisa implícita na frase "Bismillah" quando é dita antes de começar a fazer algo, que pode ser "eu inicio minha ação em nome de Allah", como dizer: "Em nome de Allah eu leio", "Em nome de Allah eu escrevo”, “Em nome de Allah eu subo/cavalgo”, e assim por diante. Ou, "Minha partida é em nome de Allah", "Minha equitação é em nome de Allah", "Minha leitura é em nome de Allah" e assim por diante. Pode ser que a bênção venha ao se dizer o nome de Allah primeiro, e isso também transmite o significado de começar em nome de Allah apenas, e não em nome de mais ninguém.
