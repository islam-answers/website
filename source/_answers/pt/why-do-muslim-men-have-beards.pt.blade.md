---
extends: _layouts.answer
section: content
title: Por que os homens muçulmanos têm barba?
date: 2025-01-23
description: Os homens muçulmanos deixam a barba para seguir o exemplo do Profeta
  Muhammad, preservar sua natureza inata e se distinguir dos não muçulmanos.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 594172
---

O último e melhor dos Mensageiros, Muhammad {{ $page->pbuh() }}, deixava crescer a barba, tal como os califas que se lhe seguiram, os seus companheiros, os líderes e a gente comum dos muçulmanos. Esta é a maneira dos Profetas e Mensageiros e dos seus seguidores, e faz parte da fitrah (predisposição original) com que Allah criou as pessoas. Aisha (que Allah esteja satisfeito com ela) narrou que o Mensageiro de Allah {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:261a') }}

O que se exige do crente, se Allah e Seu Mensageiro ordenaram algo, é que diga: Ouvimos e obedecemos, como Deus disse:

{{ $page->verse('24:51') }}

Os homens muçulmanos deixam crescer a barba em obediência a Allah e ao Seu Mensageiro. O Profeta {{ $page->pbuh() }} ordenou aos muçulmanos que deixassem crescer livremente a barba e que aparassem o bigode. Foi narrado por Ibn 'Umar que o Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn 'Abd al-Barr disse: "É proibido rapar a barba, e ninguém o faz, exceto os homens efeminados", ou seja, os que imitam as mulheres. Jabir relata o seguinte sobre o Profeta Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2344b') }}

O Sheikh al-Islam Ibn Taymiyah (que Allah tenha misericórdia dele) disse:

> O Alcorão, a Sunnah e o ijma' (consenso académico) indicam que devemos diferir dos incrédulos em todos os aspectos e não imitá-los, porque imitá-los exteriormente fará com que os imitemos nas suas más acções e hábitos, e mesmo nas crenças. (...)
