---
extends: _layouts.answer
section: content
title: Por que os muçulmanos não podem namorar?
date: 2024-12-23
description: O Alcorão proíbe ter namoradas ou namorados. O Islam preserva as relações
  entre os indivíduos de uma forma que se adapta à natureza humana.
sources:
- href: https://islamqa.org/shafii/seekersguidance-shafii/241530
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107140
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107560
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/shafii/qibla-shafii/33045
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/askimam/17968
  title: islamqa.org (AskImam.org)
- href: https://www.islamweb.net/en/fatwa/89542
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/82217
  title: islamweb.net
- href: https://islamqa.info/en/answers/20949
  title: islamqa.info
- href: https://islamqa.info/en/answers/33702
  title: islamqa.info
- href: https://islamqa.info/en/answers/1200
  title: islamqa.info
- href: https://islamqa.info/en/answers/21933
  title: islamqa.info
- href: https://islamqa.info/en/answers/93450
  title: islamqa.info
- href: https://islamqa.info/en/answers/1114
  title: islamqa.info
- href: https://islamqa.info/en/answers/3807
  title: islamqa.info
translator_id: 594172
---

O Islam preserva as relações entre os indivíduos de uma forma que se adapta à natureza humana. O contacto entre um homem e uma mulher é proibido, exceto no âmbito de um casamento legal. Se houver necessidade de falar com o sexo oposto, deve ser dentro dos limites da cortesia e das boas maneiras.

## Prevenir a tentação

O Islam está ansioso por extinguir qualquer forma de fornicação no seu início. Não é permitido a uma mulher ou a um homem estabelecer uma amizade ou uma relação amorosa com alguém do sexo oposto através de salas de conversação, da Internet ou de qualquer outro meio, uma vez que isso conduz a mulher ou o homem à tentação. Este é o caminho do demónio, que arrasta a pessoa para o pecado, a fornicação ou o adultério. Allah diz:

{{ $page->verse('24:21..') }}

A mulher está proibida de falar suavemente com quem não lhe é permitido, como diz Deus:

{{ $page->verse('..33:32') }}

O encontro, a mistura e a interligação de homens e mulheres num mesmo local, a aglomeração dos mesmos e a revelação e exposição das mulheres aos homens são proibidas pela Lei do Islam (Shari'ah). Estes actos são proibidos porque se encontram entre as causas da fitnah (tentação ou provação que implica más consequências), do despertar de desejos e do cometimento de indecência e de actos ilícitos. O Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('ahmad:1369') }}

O Alcorão proíbe ter namoradas ou namorados. Allah diz:

{{ $page->verse('..4:25..') }}

## Considerações sociais

O início de uma relação do tipo namorado/namorada pode prejudicar a reputação de uma mulher de formas graves que não afectam necessariamente o parceiro masculino. O sexo antes do casamento é contrário à religião e põe em causa a honra da linhagem. É difícil ver através da névoa do amor, da luxúria ou da paixão, e é por isso que Allah nos ordena a todos que nos mantenhamos afastados do sexo antes do casamento. Muitos danos podem ocorrer depois de apenas um ato ilegal de sexo antes do casamento, por exemplo, gravidezes indesejadas, doenças sexualmente transmissíveis, desgosto, culpa.

Além da proibição explícita de Deus, há sabedorias óbvias de que as relações pré-matrimoniais são ilegais. Entre elas, destacam-se:

1. A inequivocidade da paternidade se a mulher ficar grávida. Apesar das relações de longa duração, quem pode dizer que a mulher não dorme com outros homens para estabelecer a compatibilidade de um futuro cônjuge?
1. Uma criança nascida fora do matrimónio não é atribuída ao pai. A preservação da linhagem é um dos principais objectivos da lei islâmica (Sharia).
1. Estas relações dão aos homens a vantagem de "fazerem o que lhes apetece" com as mulheres e estão isentos de qualquer responsabilidade emocional e financeira para com a mulher e as crianças nascidas da relação.
1. Existe uma virtude inata na preservação da castidade antes do casamento, reconhecida há muitos séculos e estabelecida por todas as religiões.
1. Embora possamos não observar qualquer diferença exterior nos nossos assuntos mundanos, as práticas impróprias têm um efeito na alma que não conseguimos perceber. Quanto mais uma pessoa se entrega ao pecado, mais o seu coração se endurece.
1. Ter múltiplos parceiros aumenta o risco de contrair doenças sexualmente transmissíveis e de infetar outras pessoas.
1. Não existe qualquer garantia de que a vida em comum antes do casamento seja um indicador seguro da relação após o casamento. Muitos casais não muçulmanos vivem juntos durante anos, mas acabam por se separar depois do casamento.
1. Também devemos perguntar-nos se gostaríamos que os nossos próprios filhos e filhas fizessem o mesmo antes do casamento.

Em conclusão, não faz qualquer sentido que sejam permitidas relações de longa duração e sexo antes do casamento. As considerações individuais são insignificantes quando comparadas com a série de problemas graves que afectam os indivíduos, principalmente as mulheres, e é uma forma segura de conduzir à decadência da sociedade e da própria alma.
