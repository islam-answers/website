---
extends: _layouts.answer
section: content
title: Que pensent les musulmans de Jésus ?
date: 2024-06-08
description: Les musulmans respectent et révèrent Jésus. Ils le considèrent comme
  l'un des plus importants messagers que Dieu a envoyé à l'humanité.
sources:
- href: https://www.islam-guide.com/fr/ch3-10.htm
  title: islam-guide.com
---

Les musulmans respectent et révèrent Jésus. Ils le considèrent comme l'un des plus importants messagers que Dieu a envoyé à l'humanité. Le Coran confirme sa naissance miraculeuse (d'une vierge), et un chapitre du Coran est intitulé "Maryam" (Marie). Le Coran décrit la naissance de Jésus comme suit:

{{ $page->verse('3:45-47') }}

Jésus est né de façon miraculeuse sur l'ordre de Dieu, tout comme Adam, à qui Dieu avait insufflé la vie sans qu'il n'ait eu de père. Dieu a dit:

{{ $page->verse('3:59') }}

Durant sa mission prophétique, Jésus a accompli plusieurs miracles. Dieu nous révèle que Jésus a dit:

{{ $page->verse('3:49..') }}

Les musulmans croient que Jésus n'a pas été crucifié. Ses ennemis avaient l'intention de le crucifier, mais Dieu l'a sauvé et l'a élevé vers Lui. Et l'apparence physique de Jésus fut donnée à un autre homme. Alors les ennemis de Jésus s'emparèrent de cet homme et le crucifièrent, croyant ainsi crucifier Jésus. Dieu a dit:

{{ $page->verse('..4:157..') }}

Ni Mohammed {{ $page->pbuh() }} ni Jésus ne vinrent pour apporter des changements à la doctrine de base selon laquelle on ne doit croire qu'en un seul Dieu, doctrine qui fut prêchée par d'autres prophètes avant eux; il vinrent plutôt pour la confirmer et la renouveler.
