<div class="flex flex-wrap -mx-4">
    @php($sortedLanguages = $languages->sortByDesc(fn ($language) => $language->answers($answers)->count() ))
    @foreach ($sortedLanguages as $language)
    <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8" dir="{{ $language->getDirection() }}">
        <a href="{{ $language->getRealUrl() }}" title="{{ $language->description }}" class="block bg-gray-200 hover:bg-blue-200 rounded-lg p-6 text-center">
            <span class="text-2xl mb-2 text-gray-800">{{ $language->title }}</span>
            @if (!$compact)
            <p class="text-lg text-gray-700 h-16">{{ $language->description }}</p>
            <p class="text-sm text-blue-700">{{ $language->answers($answers)->count() }} {{ $language->getString('answers') }}</p>
            @endif
        </a>
    </div>
    @endforeach
    <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
        <a href="/join/" title="Contribute a new language" class="block bg-transparent hover:bg-blue-200 rounded-lg p-6 text-center">
            <span class="text-2xl mb-2 text-gray-800">Other?</span>
            @if (!$compact)
            <p class="text-lg text-gray-700 h-16">Translate answers to your language!</p>
            <p class="text-sm text-blue-700">Join the team</p>
            @endif
        </a>
    </div>
</div>
