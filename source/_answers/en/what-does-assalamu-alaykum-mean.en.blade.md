---
extends: _layouts.answer
section: content
title: What does Assalamu Alaykum mean?
date: 2024-07-29
description: Assalamu alaykum means “peace be upon you” and is a greeting used among Muslims. Giving salam means harmlessness, safety and protection from evil and faults.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
---

When Islam came, Allah prescribed that the manner of greeting among Muslims should be Al-salamu ‘alaykum, and that this greeting should only be used among Muslims.

The meaning of “salam” (literally meaning “peace”) is harmlessness, safety and protection from evil and from faults. The name al-Salam is a Name of Allah, may He be exalted, so the meaning of the greeting of salam which is required among Muslims is, May the blessing of His Name descend upon you. The usage of the preposition ‘ala in ‘alaykum (upon you) indicates that the greeting is inclusive.

The most complete form of greeting is to say “As-Salamu `alaykum wa rahmatu-Allahi wa barakatuhu”, which means “Peace be upon you and the mercy of Allah and His blessings)”.

The Prophet {{ $page->pbuh() }} made spreading salam a part of faith.

{{ $page->hadith('bukhari:12') }}

Therefore, the Prophet {{ $page->pbuh() }} explained that giving salam spreads love and brotherhood. The Messenger of Allah {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:54') }}

The Prophet {{ $page->pbuh() }} commanded us to return salams, and made it a right and a duty. He said:

{{ $page->hadith('muslim:2162a') }}

It is clear that it is obligatory to return salams, because by doing so a Muslim is giving you safety and you have to give him safety in return. It is as if he is saying to you, I am giving you safety and security, so you have to give him the same, so that he does not get suspicious or think that the one to whom he has given salam is betraying him or ignoring him.
