---
extends: _layouts.answer
section: content
title: Co Islam mówi na temat ewolucji?
date: 2024-08-23
description: Bóg stworzył pierwszą istotę ludzką, Adama, w jego ostatecznej formie.
  Jeśli chodzi o żywe stworzenia inne niż ludzie, źródła islamskie milczą na ten temat.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 583847
---

Bóg stworzył pierwszą istotę ludzką, Adama, w jego ostatecznej formie – w przeciwieństwie do stopniowej ewolucji z zaawansowanych małp człekokształtnych. Nie może to być bezpośrednio potwierdzone ani zaprzeczone przez naukę, ponieważ było to wyjątkowe i jedyne wydarzenie historyczne – cud.

Jeśli chodzi o żywe stworzenia inne niż ludzie, źródła islamskie milczą na ten temat i wymagają od nas jedynie, abyśmy wierzyli, że Bóg stworzył je tak jak tego chciał, używając ewolucji lub w inny sposób.
