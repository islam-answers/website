---
extends: _layouts.answer
section: content
title: Werden nach der Konversion zum Islam alle Sünden vergeben?
date: 2025-01-26
description: Wenn ein Ungläubiger Muslim wird, vergibt Allah ihm alles, was er als
  Nichtmuslim getan hat, und er wird von seinen Sünden gereinigt.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
translator_id: 587746
---

Durch Seine Gnade und Barmherzigkeit hat Allah die Annahme des Islams zu einem Grund gemacht, die Sünden zu tilgen, die davor begangen wurden. Wenn ein Ungläubiger Muslim wird, vergibt Allah ihm alles, was er getan hat, als er noch ein Nicht-Muslim war, und er wird von seinen Sünden befreit.

'Amr ibn al-'As (möge Allah mit ihm zufrieden sein) sagte:

{{ $page->hadith('muslim:121') }}

Imam Nawawi erklärte: „Der Islam tilgt alles, was vorher war", bedeutet, dass es vollständig ausgelöscht wird.

Shaykh Ibn `Uthaymin (möge Allah ihm gnädig sein) wurde eine ähnliche Frage über jemanden gestellt, der Geld mit Drogenhandel verdiente, bevor er Muslim wurde. Er antwortete: Wir sagen zu diesem Bruder, den Allah mit dem Islam gesegnet hat, nachdem er unrechtmäßigen Reichtum erworben hatte: "Sei guten Mutes, denn dieser Reichtum ist für ihn erlaubt, und es liegt keine Sünde auf ihm, ob er ihn behält oder ihn für wohltätige Zwecke spendet oder ihn benutzt, um zu heiraten, denn Allah sagt in Seinem Heiligen Buch:

{{ $page->verse('8:38') }}

Das bedeutet, dass alles, was vergangen ist, im Allgemeinen vergeben wird. Aber alles Geld, das seinem Besitzer mit Gewalt genommen wurde, muss ihm zurückgegeben werden. Aber Geld, das durch Vereinbarungen zwischen Menschen verdient wurde, selbst wenn es ungesetzlich ist, wie das, das durch Riba (Zinsen) oder durch den Verkauf von Drogen usw. verdient wurde, ist für ihn zulässig, wenn er Muslim wird.

Viele der Nicht-Muslime zur Zeit des Propheten Muhammad {{ $page->pbuh() }} wurden Muslime, nachdem sie Muslime getötet hatten, aber sie wurden nicht für ihre Taten bestraft.
