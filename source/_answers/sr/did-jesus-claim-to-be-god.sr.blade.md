---
extends: _layouts.answer
section: content
title: Je li Isus tvrdio da je Bog?
date: 2024-11-25
description: Muslimani veruju da je Isus bio prorok koji je pozivao ljude da obožavaju
  samo Allaha, nikada ne tvrdeći da je Bog.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
translator_id: 584497
---

Muslimani veruju da je Isus {{ $page->pbuh() }}, bio jedan od Allahovih robova i jedan od Njegovih plemenitih poslanika. Allah je poslao Isusa sinovima Israilovim da ih pozove da veruju samo u Allaha i da samo Njega obožavaju.

Allah je podržao Isusa {{ $page->pbuh() }} čudima koja su dokazala da je govorio istinu; Isusa je rodila Djevica Marija bez oca. Dozvolio je Židovima neke od stvari koje su im bile zabranjene, nije umro i njegovi neprijatelji Židovi ga nisu ubili, nego ga je Allah spasio od njih i živog ga digao na nebo. Isus je rekao svojim sljedbenicima o dolasku našeg Poslanika Muhammeda {{ $page->pbuh() }}. Isus će se vratiti dolje na kraju vremena. On će se na Sudnjem danu odreći tvrdnji da je Bog.

Na Sudnjem danu Isus će stati pred Gospodara svetova, koji će ga pred svedocima upitati šta je rekao sinovima Israilovim, kao što Allah kaže:

{{ $page->verse('5:116-118') }}

Iskreno ispitivanje pokazuje da Isus {{ $page->pbuh() }} nikada nije eksplicitno i nedvosmisleno rekao da je Bog. Autori Novog zavjeta, od kojih nitko nije susreo Isusa, tvrdili su da je on Bog i to propagirali. Nema dokaza da su stvarni Isusovi apostoli to tvrdili ili da su oni bili autori Biblije. Umjesto toga, prikazano je da Isus neprestano štuje i poziva druge da štuju samo Boga.

Tvrdnja da je Isus {{ $page->pbuh() }} Bog ne može se pripisati Isusu, ako je tako oštro u suprotnosti s Prvom zapovesti da se ne uzimaju drugi bogovi osim Boga, niti da se od ičega na nebu i na zemlji pravi idol. Također, logično je apsurdno da Bog postane njegova kreacija.

Prema tome, jedino je islamska tvrdnja uverljiva i u skladu s ranom objavom: Isus {{ $page->pbuh() }} bio je prorok, a ne Bog.
