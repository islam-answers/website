---
extends: _layouts.answer
section: content
title: 如为成为穆斯林？
date: 2024-07-08
description: 只要真诚诵祷：“La ilaha illa Allah, Muhammadur rasoolu Allah”，即可转信伊斯兰教并成为穆斯林。
sources:
- href: https://www.islam-guide.com/ch3-6.htm
  title: islam-guide.com
translator_id: 554943
---

只要真诚诵祷：“La ilaha illa Allah, Muhammadur rasoolu Allah”，即可转信伊斯兰教并成为穆斯林。这句话的意思是说“除了真主（安拉）之外，再无其它的主， 而穆罕默德是 安拉的使者（先知）。” 第一部分是说：“安拉是唯一的真主，除了他以外再没有其它的主”表示除了安拉之外，再也没有其它的主值得我们崇拜，而安拉也没有其它的同伴或儿子。 若要成为穆斯林，另外应具备下列条件：

- 信仰主圣古兰经是真主的真言，而由他所揭示。
- 如同真主在古兰经的陈述，信仰审判日（复活日）为真且将到来。
- 接受伊斯兰教为其宗教。
- 除了真主，不崇拜任何事物或任何人。

先知穆罕默德 {{ $page->pbuh() }} 说道：

{{ $page->hadith('muslim:2747') }}
