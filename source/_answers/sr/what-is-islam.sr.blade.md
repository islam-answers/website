---
extends: _layouts.answer
section: content
title: Što je islam?
date: 2024-10-25
description: Islam znači pokornost, poniznost, pokoravanje naredbama i poštovanje
  zabrana bez prigovora, obožavanje samo Allaha i vera u Njega.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 584497
---

Ako potražite u rečnicima arapskog jezika, naći ćete da je značenje riječi islam: pokornost, poniznost, pokoravanje naredbama i poštovanje zabrana bez prigovora, iskreno obožavanje samo Allaha, verovanje u ono što nam On kaže i imanje vere u Njega.

Reč islam postala je naziv vere koju je doneo poslanik Muhammed {{ $page->pbuh() }}.

### Zašto se ova vera zove islam

Sve religije na zemlji nazivaju se različitim imenima, bilo imenom određenog čoveka ili određenog naroda. Kršćanstvo je tako dobilo ime po Kristu; Budizam je dobio ime po svom utemeljitelju, Budi; Slično tome, judaizam je uzelo ime po plemenu poznatom kao Yehudah (Judah), pa je postalo poznato kao judaizam. I tako dalje.

Osim islama; jer se ne pripisuje nijednom određenom čoveku ili nekoj određenoj naciji, već se njegovo ime odnosi na značenje riječi islam. Ono što ovaj naziv ukazuje je da uspostava i utemeljenje ove vere nije delo jednog određenog čoveka i da nije samo za jednu određenu naciju isključujući sve druge. Umjesto toga, njegov cilj je dati atribut koji implicira reč islam svim narodima na zemlji. Dakle, svako ko stekne ovo svojstvo, bio on iz prošlosti ili sadašnjosti, je musliman, a svako ko stekne to svojstvo u budućnosti također će biti musliman.

Islam je vera svih poslanika. Pojavila se na početku poslanstva, od vremena našeg oca Adema a.s., i sve poruke su pozivale na islam (pokornost Allahu) u smislu verovanja i osnovnih pravila. Svi vernici koji su sledili ranije poslanike bili su muslimani u općem smislu, i oni će ući u Džennet zahvaljujući svom islamu.
