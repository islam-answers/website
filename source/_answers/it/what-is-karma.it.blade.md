---
extends: _layouts.answer
section: content
title: Che cos'è il karma?
date: 2025-01-26
description: Il karma - credere che le azioni abbiano conseguenze - è un falso concetto
  delle religioni indiane. L'Islam insegna che solo Allah ci ricompensa o punisce.
sources:
- href: https://www.islamweb.net/en/fatwa/343198/
  title: islamweb.net
- href: https://islamqa.info/ar/answers/183131/
  title: islamqa.info
translator_id: 573107
---

"Karma" è un termine comune nelle religioni indiane (Induismo, Giainismo, Sikhismo e Buddismo). La parola "karma" si riferisce alle azioni di un essere vivente e alle conseguenze morali che ne derivano. Ogni atto di bene o di male, che sia una parola, un'azione o un semplice pensiero, deve avere delle conseguenze (ricompensa o punizione), purché sia il risultato di una precedente consapevolezza e percezione.

Secondo queste religioni, il Karma funziona in base ad una legge morale naturale e non è sotto l'autorità di giudizi divini. Secondo esse, il Karma determina, ad esempio, l'aspetto esteriore, la bellezza, l'intelligenza, l'età, la ricchezza e lo status sociale. Sostengono inoltre che la legge del Karma governa tutto ciò che viene creato ed è una legge immutabile. Affermano che questa legge governa e controlla ogni momento e quindi ogni nostra azione, buona o cattiva, ha delle conseguenze.

Come affermato nella "The Facilitated Encyclopedia Of Contemporary Religions Doctrines And Parties" [Enciclopedia Facilitata delle Dottrine e dei Gruppi Religiosi Contemporanei]:

> Il karma - secondo gli indù - è la legge della compensazione, il che significa che il sistema dell'universo è divino e basato sulla pura giustizia, una giustizia che inevitabilmente si verificherà nella vita presente o nella prossima, e la compensazione di una vita avverrà in un'altra vita (...)

Si legge inoltre:

> L'uomo continua a nascere e morire finché il karma rimane attaccato alla sua anima, e la sua anima non si purifica finché non si libera dal karma (è qui dove vanno a finire i suoi desideri), e in quel momento egli rimarrà vivo e immortale nella beatitudine della salvezza, raggiungendo lo stadio del "Nirvana". Questa fase si può ottenere sia in questo mondo, attraverso l'addestramento e l'esercizio, sia attraverso la morte.

### Il karma è un concetto giusto o sbagliato?

Non vi è alcun dubbio sul fatto che queste religioni indiane siano culti pagani, fondati su errate convinzioni e interpretazioni immaginarie e insostenibili. Credere nel "karma" è uno dei falsi concetti in cui queste persone credono e a cui si attengono.

Riassumendo i motivi per cui questa religione corrotta è falsa, possiamo affermare che:

1. È un credo inventato che non si basa su una rivelazione divina infallibile, ma la sua fonte è invece una religione pagana falsificata. 
1. È un sistema che opera secondo una legge morale naturale autonoma, indipendentemente dalla legge divina o dalle concezioni religiose.
1. Si tratta di una legge onnipresente che controlla ogni creatura, giudica le azioni, gestisce i destini e premia per le azioni compiute. Questa è chiaramente una blasfemia poiché Dio è Colui Che domina, Colui Che gestisce tutti gli affari e Colui Che ritiene le persone responsabili per le loro azioni. 
1. Questo concetto fa parte del loro insieme di false credenze secondo cui vogliono raggiungere lo stadio della salvezza eterna che, come affermano, è il loro obiettivo più alto. Ma dato che il karma è la conseguenza delle azioni che le persone compiono, non c'è salvezza finché esiste il karma.

E noi, Dio sia lodato, non dipendiamo da queste false credenze o sette immaginarie, grazie alla Religione e alla Grazia di Dio. Per noi è sufficiente dire che:

{{ $page->verse('99:7-8') }}

Per noi è sufficiente sapere che Allah è il Custode di tutto, e che Allah ha racchiuso ogni cosa nella Sua Sapienza. Allah dice nel Corano:

{{ $page->verse('53:31') }}

Chiunque sappia tutto questo, ci crede ed ha la certezza che Allah resusciterà coloro che sono nelle tombe per chieder conto del peso di un atomo di azioni, e Che ha creato testimoni e scribi che registreranno tali azioni, non avrà bisogno delle falsità inventate e di questa fede corrotta e fuorviante per smettere di peccare e astenersi dalle cattive parole, azioni e abitudini.
