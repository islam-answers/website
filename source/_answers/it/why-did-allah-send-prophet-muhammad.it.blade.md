---
extends: _layouts.answer
section: content
title: Perché Allah ha inviato il Profeta Muhammad?
date: 2025-02-19
description: Allah ha inviato il profeta Muhammad per ristabilire il vero monoteismo,
  chiarire le divergenze religiose e guidare le persone alla rettitudine.
sources:
- href: https://islamqa.info/en/answers/11575/
  title: islamqa.info
- href: https://islamqa.info/en/answers/13957/
  title: islamqa.info
- href: https://islamqa.info/en/answers/10468/
  title: islamqa.info
- href: https://islamqa.info/en/answers/2114/
  title: islamqa.info
translator_id: 573107
---

I profeti sono i messaggeri di Allah che Egli ha inviato ai Suoi servi; trasmettono i Suoi comandi e danno loro la buona novella delle delizie che Allah ha promesso se obbediranno ai Suoi comandi, avvertendoli della punizione eterna se andranno contro i Suoi divieti. Raccontano le storie dei popoli passati e la punizione e la vendetta che li hanno colpiti in questo mondo perché hanno disobbedito ai comandi del loro Signore.

Questi divieti e ordini divini non si possono conoscere soltanto attraverso il pensiero critico, perciò Allah ha prescritto leggi e imposto comandamenti e divieti per onorare l'umanità e proteggerne gli interessi, perché le persone possono seguire i loro desideri arrivando ad oltrepassare i limiti, abusando delle persone e privandole dei loro diritti.

### Perché Allah ha inviato profeti e messaggeri?

Con la Sua sapienza, Allah ha inviato più volte tra gli uomini dei messaggeri per ricordare loro i comandamenti di Allah e per metterli in guardia dal commettere peccati, per divulgare e diffondere la fede, per raccontare le storie di coloro che li avevano preceduti. Quando si ascoltano storie meravigliose, infatti, le menti diventano più attente e consapevoli, riuscendo a comprendere correttamente ed aumentare la conoscenza. Più le persone ascoltano, più idee avranno; più idee hanno, più penseranno; più pensano, più sapranno; e più sanno, più faranno. In breve, non c'è alternativa all'invio di messaggeri per spiegare la verità.

I popoli della terra hanno bisogno dei messaggeri più di quanto necessitano del sole, della luna, del vento e della pioggia, ed anche per l'uomo non si tratta dello stesso bisogno che egli ha della propria vita, o il bisogno che l'occhio ha della luce, o il bisogno che il corpo ha di cibo e bevande. In verità, i messaggeri sono più necessari di tutte queste esigenze, più grandi di qualsiasi altro bisogno si possa pensare. I messaggeri [che la pace e le benedizioni di Allah siano su di loro] sono intermediari tra Allah e la Sua creazione, trasmettendo i Suoi comandi e le Sue proibizioni. Sono ambasciatori di Lui presso i Suoi servi. L'ultimo e il più grande di loro, il più nobile davanti al suo Signore, fu Muhammad {{ $page->pbuh() }}. Allah lo ha inviato come misericordia per i mondi, guida per coloro che vogliono avvicinarsi ad Allah, prova che non ha lasciato scuse per nessuno tra gli uomini.

I Profeti e i Messaggeri sono stati molti, e nessuno ne conosce il numero tranne Allah. Tra loro ci sono quelli di cui Allah ci ha parlato, e alcuni di cui non ci ha parlato affatto. Allah dice:

{{ $page->verse('4:164..') }}

### Perché Allah ha inviato il Profeta Muhammad?

Il Messaggio trasmesso dal Profeta {{ $page->pbuh() }} non era esclusivo ed unico, ma era simile al messaggio portato da altri Messaggeri prima di lui, sia nell'essenza che nei contenuti. Allah aveva inviato Profeti e Messaggeri, come Musa (Mosè), 'Isa (Gesù) e altri, ai Figli di Israele, e molti tra essi avevano creduto in loro e testimoniato la verità dei loro Libri, che contenevano messaggi simili a quelli portati dal Corano. Ciò dimostrava e testimoniava la verità del Messaggio con cui era stato inviato, soprattutto perché apparteneva allo stesso tipo di Messaggio del quale ne avevano attestato la verità.

Quando Allah inviò Muhammad {{ $page->pbuh() }} con lo stesso Messaggio dei Profeti che lo avevano preceduto, il Corano venne per confermare i loro Libri e la loro Profezia, e per invitare la gente a credere in essi. Pertanto, quando la Gente del Libro non credette in lui e nel suo Libro, ciò dimostrò che non credeva nemmeno nei propri Libri e Messaggeri. Il fatto che il Corano conteneva gli stessi principi dei loro libri, confermandoli, dimostrava che era meno probabile che fosse stato inventato o che provenisse da una fonte diversa da Allah, perché tutti i Libri discendono da Allah, che Egli sia Lodato.

Tra i Figli di Israele sorsero divergenze e dispute. Alterarono e introdussero cambiamenti nelle loro credenze e leggi. Così la verità si estinse e la falsità prevalse, l'oppressione e il male si diffusero e, di conseguenza, le persone ebbero la necessità di una religione che ristabilisse la verità, distruggesse il male e guidasse i popoli sulla retta via, perciò Allah inviò Muhammad {{ $page->pbuh() }}, come dice Allah:

{{ $page->verse('16:64') }}

Allah ha inviato tutti i Profeti e i Messaggeri per chiamare all'adorazione di Allah l'Unico, senza associati, e per guidare i popoli dall'oscurità alla luce. Il primo di questi Messaggeri fu Nuh (Noé) e l'ultimo di loro fu Muhammad {{ $page->pbuh() }}, come dice Allah:

{{ $page->verse('16:36..') }}
