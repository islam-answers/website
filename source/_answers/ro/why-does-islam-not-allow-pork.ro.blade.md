---
extends: _layouts.answer
section: content
title: De ce Islamul nu permite carnea de porc?
date: 2024-07-19
description: Musulmanii ascultă poruncile lui Allah și se abțin de la interdicțiile prescrise de El, indiferent dacă înțeleg sau nu motivul din spatele acestora.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 554943
---

Principiul de bază al musulmanului este că el ascultă de tot ceea ce Allah îi poruncește și se abține de la tot ceea ce El îi interzice, indiferent dacă motivul din spatele acestui lucru este clar sau nu.

Nu este permis unui musulman să respingă nicio lege islamicǎ sau să ezite să o urmeze dacă motivul din spatele acesteia nu este clar. Mai degrabă, el trebuie să accepte hotărârile privind ceea ce este permis (halal) și ceea ce este interzis (haram), atunci când acestea sunt dovedite în textele religioase, indiferent dacă el înțelege sau nu motivul din spatele acestora. Allah spune:

{{ $page->verse('33:36') }}

### De ce este carnea de porc haram?

Carnea de porc este haram (interzisă) în Islam conform textului Coranului, unde Allah spune:

{{ $page->verse('2:173..') }}

Nu este permis unui musulman să o consume în nicio circumstanță, cu excepția cazurilor de necesitate în care viața unei persoane depinde de consumul ei, cum ar fi în caz de foamete, când o persoană se teme că va muri și nu poate găsi niciun alt fel de hrană.

Nu există nicio mențiune în textele religioase despre un motiv specific pentru interzicerea cărnii de porc, în afară de versetul în care Allah spune:

{{ $page->verse('..6:145..') }}

Cuvântul din arabă "rijs" (tradus aici prin "impur") este folosit pentru a se referi la orice este considerat detestabil în Islam și conform naturii umane sănătoase (fitrah). Numai acest motiv este suficient.

Și există un motiv general care este dat cu privire la interzicerea alimentelor și băuturilor haram și altele asemenea, care indică motivul din spatele interdicției privind carnea de porc. Acest motiv general se găsește în versetul în care Allah spune:

{{ $page->verse('..7:157..') }}

### Motive științifice și medicale pentru interzicerea cărnii de porc

Cercetările științifice și medicale au dovedit, de asemenea, că porcul, dintre toate animalele, este considerat un purtător de germeni care sunt dăunători organismului uman. Explicarea în detaliu a tuturor acestor boli dăunătoare ar dura prea mult, dar pe scurt le putem enumera astfel: boli parazitare, boli bacteriene, viruși și așa mai departe.

Acestea și alte efecte dăunătoare indică faptul că Allah a interzis carnea de porc cu un motiv, și anume păstrarea viații și sănătății.
