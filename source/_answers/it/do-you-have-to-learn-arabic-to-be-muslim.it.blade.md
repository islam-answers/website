---
extends: _layouts.answer
section: content
title: E' necessario imparare l'arabo per essere musulmani?
date: 2025-01-12
description: Se una persona non conosce l'arabo, ciò non pregiudica il suo Islam e
  non la priva dell'onore di appartenere all'Islam.
sources:
- href: https://islamqa.info/en/answers/20815/
  title: islamqa.info
translator_id: 573107
---

Il fatto che una persona non conosca l'arabo non pregiudica il suo Islam e non la priva dell'onore di appartenere alla religione islamica. I cuori di molte persone si aprono all'Islam ogni giorno, anche se non conoscono nemmeno una lettera di arabo.

Ci sono migliaia di musulmani in India, Pakistan, Filippine e altrove che hanno persino imparato a memoria il Corano, ma nessuno di loro è in grado di sostenere una lunga conversazione in arabo. Questo perché il Creatore ha reso facile il Corano, e ne ha reso facile la memorizzazione, come Allah dice:

{{ $page->verse('54:17') }}

### Importanza della preghiera nell'Islam

La preghiera è il pilastro più importante dell'Islam dopo la Shahadah (dichiarazione di fede). E' obbligatorio eseguire cinque preghiere durante il giorno e la notte. In questo modo si stabilisce una relazione diretta tra il credente e il suo Signore, per mezzo della quale la persona trova pace, felicità e appagamento. Mentre si trova di fronte al suo Signore, il credente Gli parla, Lo invoca e conversa con Lui, si prostra davanti a Lui, si lamenta con Lui delle proprie preoccupazioni e dolori, e si rivolge a Lui nei momenti di calamità.

Qualunque cosa vi sia stata detta riguardo alla preghiera, nulla può veramente descrivere quanto sia straordinaria e importante, e nessuno può apprezzarla se non colui che ne assapora la gioia, trascorrendo le notti in preghiera e riempiendo le proprie giornate con essa. È la delizia di coloro che credono nell'Unicità di Dio e la gioia dei credenti.

### Come si fa la preghiera nell'Islam?

Per quanto riguarda il modo in cui si fa la preghiera, essa prevede di iniziare in piedi dicendo "Allahu akbar (Allah è il Più Grande)", per poi recitare il Corano, inchinarsi e prostrarsi. Basta recarsi in un centro islamico del proprio paese per vedere come pregano i musulmani e, allo stesso tempo, per imparare.

### Cosa fare se non si riesce a leggere la Surat "Al-Fatihah" durante la preghiera?

Durante la preghiera, il musulmano deve recitare la Surat Al-Fatihah ("L'Aprente") in arabo, e pertanto deve impararla. Se non è in grado di farlo ma ne conosce un versetto, deve ripeterlo sette volte, che corrisponde al numero di versetti della Surat Al-Fatihah. Se non è in grado di farlo, allora deve dire:

> Subhan Allah, wal-hamdu Lillah, wa la ilaha illallah, wa Allahu akbar, wa la ilaha illallah, wa la hawla wa la quwwata illa Billah.

Quanto sopra significa: "Gloria ad Allah, lode ad Allah, non c'è nessuno degno di adorazione all'infuori di Allah, Allah è il Più Grande, non c'è nessuno degno di adorazione all'infuori di Allah, e non c'è potere e forza se non presso Allah".

La questione è semplice, sia lodato Allah. Quante persone hanno imparato a parlare molto bene una lingua diversa dalla propria, anche due o tre lingue, quindi come possono non essere in grado di imparare trenta o quaranta parole di cui hanno bisogno nelle loro preghiere? Se la lingua fosse davvero un ostacolo, non vi sarebbero milioni di musulmani non arabi che eseguono gli atti di adorazione con facilità, sia lodato Allah.

Affrettatevi dunque ad entrare nell'Islam, poiché nessuno sa quando arriverà il momento stabilito (cioè la morte). Che Allah vi salvi e vi protegga dalla Sua Ira e dal Suo Castigo.

### Come diventare musulmano

Per entrare in questa religione perfetta è sufficiente dire:

> Ash-hadu alla ilaha illallah wa ash-hadu anna Muhammadan 'abduhu wa Rasuluhu.

Ciò significa: "Testimonio che non c'è nessuno degno di adorazione tranne Allah e testimonio che Muhammad è il Suo Servo e Messaggero).

Dopodiché, troverai persone tra i tuoi fratelli musulmani che ti aiuteranno ad imparare la preghiera e ti informeranno su tutte le altre questioni riguardanti l'Islam.
