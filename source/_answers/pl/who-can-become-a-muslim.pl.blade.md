---
extends: _layouts.answer
section: content
title: Kto może zostać muzułmaninem?
date: 2024-10-06
description: Każdy człowiek może zostać muzułmaninem, bez względu na jego wcześniejszą
  religię, wiek, narodowość czy pochodzenie etniczne.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 584496
---

Muzułmaninem może zostać każdy człowiek, bez względu na wyznawaną wcześniej religię, wiek, narodowość czy pochodzenie etniczne. Aby zostać muzułmaninem, wystarczy wypowiedzieć następujące słowa: „Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah”. To świadectwo wiary oznacza: „Zaświadczam, że nie ma boga godnego czci poza Allahem i że Mohammad jest Jego Posłańcem". Musisz to powiedzieć i w to uwierzyć”.

Kiedy wypowiesz to zdanie, automatycznie staniesz się muzułmaninem. Nie musisz pytać o czas lub godzinę, noc lub dzień, aby zwrócić się do swojego Pana i rozpocząć nowe życie. Każdy czas jest na to odpowiedni.

Nie potrzebujesz niczyjego pozwolenia ani kapłana, który by cię ochrzcił, ani pośrednika, który by za ciebie pośredniczył, ani nikogo, kto by cię do Niego prowadził, bo On, niech będzie uwielbiony, sam cię prowadzi. Więc zwróć się do Niego, bo On jest blisko; On jest bliżej ciebie, niż myślisz lub możesz sobie wyobrazić:

{{ $page->verse('2:186') }}

Allah, niech będzie wywyższony, mówi:

{{ $page->verse('6:125..') }}
