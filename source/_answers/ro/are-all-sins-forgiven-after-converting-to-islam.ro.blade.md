---
extends: _layouts.answer
section: content
title: Sunt iertate toate păcatele după convertirea la islam?
date: 2025-01-01
description: Atunci când un necredincios devine musulman, Allah iartă tot ceea ce
  acesta a făcut înainte, devenind astfel complet curățat de păcat.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
translator_id: 554943
---

Prin Harul și Milostivirea Sa, Allah a făcut din îmbrățișarea islamului o cauză pentru ștergerea păcatelor care au fost făcute înainte de aceasta. Atunci când un necredincios devine musulman, Allah îi iartă tot ceea ce a făcut atunci când era nemusulman, iar el devine curățat de păcate.

'Amr ibn al-'As (Allah să fie mulțumit de el) a spus:

{{ $page->hadith('muslim:121') }}

Imam Nawawi a spus: "Islamul distruge ceea ce a fost înainte de el" înseamnă că el șterge și elimină (trecutul).

Shaykh Ibn `Uthaymin (Allah să aibă milă de el) a fost întrebat un lucru similar, despre cineva care obișnuia să câștige bani din traficul de droguri înainte de a deveni musulman. El a răspuns: Îi spunem acestui frate pe care Allah l-a binecuvântat cu Islam după ce câștigase bogății în mod ilegal: să fie bucuros, pentru că această avere îi este permisă și nu este niciun păcat pentru el în ea, fie că o păstrează, fie că o dă în scopuri caritabile, fie că o folosește pentru a se căsători, deoarece Allah spune în Cartea Sa Sfântă:

{{ $page->verse('8:38') }}

Asta înseamnă că tot ceea ce a trecut, în termeni generali, este iertat. Dar orice sumă de bani care a fost luată cu forța de la proprietar trebuie să îi fie returnată. Dar banii care au fost câștigați prin acorduri între oameni, chiar dacă sunt ilegali, cum ar fi cei care au fost câștigați prin riba (dobândă), sau prin vânzarea de droguri etc., îi sunt permiși atunci când devine musulman.

Mulți dintre non-musulmanii de pe vremea profetului Muhammad {{ $page->pbuh() }} au devenit musulmani după ce i-au ucis pe musulmani, dar nu au fost pedepsiți pentru ceea ce au făcut (în trecut).
