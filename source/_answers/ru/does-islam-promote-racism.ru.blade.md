---
extends: _layouts.answer
section: content
title: Пропагандирует ли ислам расизм?
date: 2024-10-22
description: Ислам не обращает внимания на различия в цвете кожи, расе или происхождении.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 592987
---

Все люди - потомки одного мужчины и одной женщины, верующие и кафиры (неверующие), черные и белые, арабы и неарабы, богатые и бедные, знатные и простые.

Ислам не обращает внимания на различия в цвете кожи, расе или происхождении. Все люди происходят от Адама, а Адам был создан из праха. В исламе различия между людьми основаны на вере (иман) и благочестии (таква), на выполнении того, что предписал Аллах, и на избегании того, что Аллах запретил. Аллах говорит:

{{ $page->verse('49:13') }}

Пророк Мухаммед {{ $page->pbuh() }} сказал:

{{ $page->hadith('muslim:2564b') }}

В исламе всех люди рассматриваются как равные в отношении прав и обязанностей. Они равны перед законом (Шариатом), как говорит Аллах:

{{ $page->verse('16:97') }}

Вера, правдивость и благочестие ведут в Рай, на который имеет право тот, кто приобрел эти качества, даже если он один из самых слабых или ничтожных людей. Аллах говорит:

{{ $page->verse('..65:11') }}

Куфр (неверие), высокомерие и угнетение — все это ведет в Ад, даже если тот, кто совершает эти поступки, является одним из самых богатых или благородных людей. Аллах говорит:

{{ $page->verse('64:10') }}

Среди сподвижников пророка Мухаммеда {{ $page->pbuh() }} были мусульмане всех племен, рас и цветов кожи. Их сердца были наполнены таухидом (единобожием), их объединяли вера и благочестие: Абу Бакр из Курайша, 'Али ибн Аби Таалиб из Бани Хашим, Билал из Эфиопии, Сухайб из Рима, Салман из Персии, богачи, как 'Усман, и бедняки, как 'Аммар, люди с достатком и бедняки, как Ахл ас-Саффах, и другие.

Они уверовали в Аллаха и старались ради Него, пока Аллах и Его посланник не стали довольны ими. Они были истинными верующими.

{{ $page->verse('98:8') }}
