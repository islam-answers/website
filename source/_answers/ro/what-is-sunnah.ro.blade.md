---
extends: _layouts.answer
section: content
title: Ce este Sunnah?
date: 2024-12-11
description: Sunnah, inspirată de Allah (Dumnezeu), completează Coranul prin clarificarea
  și extinderea regulilor sale.
sources:
- href: https://islamqa.info/en/answers/77243/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145520/
  title: islamqa.info
translator_id: 554943
---

Sunnah – care înseamnă cuvintele și faptele care sunt atribuite Profetului Muhammad {{ $page->pbuh() }} precum și acțiunile pe care el le-a aprobat – este una dintre cele două părți ale revelației divine (Wahy) care au fost revelate Trimisului lui Allah {{ $page->pbuh() }}. Cealaltă parte a revelației este Sfântul Coran. Allah spune:

{{ $page->verse('53:3-4') }}

S-a relatat că Trimisul lui Allah {{ $page->pbuh() }} a spus:

{{ $page->hadith('ibnmajah:12') }}

Hassaan ibn 'Atiyah a spus:

> Jibreel (îngerul Gabriel) obișnuia să aducă sunnah Profetului {{ $page->pbuh() }}, așa cum obișnuia să îi aducă Coranul.

Importanța Sunnei constă în primul rând în faptul că explică Coranul și este un comentariu asupra lui, precum și faptul că adaugă unele reguli la cele din Coran. Allah spune:

{{ $page->verse('..16:44') }}

Ibn 'Abd al-Barr a spus:

> Comentariul Profetului {{ $page->pbuh() }} asupra Coranului este de două tipuri:
>
> 1. Explicarea lucrurilor care sunt menționate în termeni generali în Sfântul Coran, cum ar fi cele cinci rugăciuni zilnice, orele lor, prosternarea, plecăciunea și toate celelalte reguli.
> 1. Adăugarea de reguli la regulile Coranului, cum ar fi interdicția de a fi căsătorit cu o femeie și cu mătușa paternă sau maternă a acesteia în același timp.

Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('bukhari:3461') }}
