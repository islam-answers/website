---
extends: _layouts.answer
section: content
title: É preciso aprender árabe para ser muçulmano?
date: 2025-02-26
description: Se alguém não sabe árabe, isso não afeta seu Islam nem o impede de ter
  a honra de pertencer ao Islam.
sources:
- href: https://islamqa.info/en/answers/20815/
  title: islamqa.info
translator_id: 594172
---

O fato de uma pessoa não saber árabe não afeta o seu Islam nem a priva da honra de pertencer à fé. Todos os dias, muitos corações são abertos para o Islam, mesmo sem conhecerem uma única letra em árabe.

Há milhares de muçulmanos na Índia, no Paquistão, nas Filipinas e em outros lugares que até memorizaram o Alcorão de cor, mas nenhum deles consegue manter uma conversa longa em árabe. Isso porque Allah tornou o Alcorão fácil e facilitou a sua memorização para as pessoas, como Allah diz:

{{ $page->verse('54:17') }}

### Importância da oração no Islam

A oração é o maior dos pilares do Islam depois da Shahadah (declaração de fé). O que é obrigatório são cinco orações ao longo do dia e da noite. Isso estabelece uma relação entre a pessoa e seu Senhor, na qual ela encontra paz, felicidade e contentamento, ao se colocar diante de seu Senhor, falar com Ele, invocá-Lo e dialogar com Ele, prostrar-se diante d’Ele, queixar-se a Ele de suas preocupações e tristezas, e voltar-se para Ele nos momentos de calamidade.

Por mais que se fale sobre a oração, nada pode realmente descrever o quão grandiosa e importante ela é, e ninguém pode compreendê-la verdadeiramente, exceto aquele que prova sua alegria, passa suas noites em oração e preenche seus dias com ela. É o deleite daqueles que creem na Unicidade de Deus e a alegria dos crentes.

### Como é feita a oração no Islam?

Quanto à forma como a oração é realizada, ela envolve ficar de pé, dizer “Allahu Akbar (Allah é o Maior)”, recitar o Alcorão, inclinar-se e prostrar-se. Tudo o que você precisa fazer é ir a um Centro Islâmico em seu país para ver como os muçulmanos rezam e aprender sobre isso.

### O que fazer se não conseguir ler o Fatiha durante a oração

Na oração, o muçulmano deve recitar a Surah Al-Fatiha ("A Abertura") em árabe, por isso precisa aprendê-la. Se não for capaz, mas souber um verso dela, deve repeti-lo sete vezes, que é o número de versos da Surah Al-Fatiha. Se não puder fazer isso, então deve dizer:

> Subhan Allah, wal-hamdu Lillah, wa la ilaha illallah, wa Allahu akbar, wa la ilaha illallah, wa la hawla wa la quwwata illa Billah.

O que foi mencionado acima significa: (Glória a Allah, louvor a Allah, não há ninguém digno de adoração além de Allah, Allah é o Maior, não há ninguém digno de adoração além de Allah, e não há poder nem força exceto com Allah.)

A questão é fácil, louvado seja Allah. Quantas pessoas aprenderam a falar muito bem uma língua diferente da sua, ou até duas ou três línguas? Então, como não seriam capazes de aprender trinta ou quarenta palavras que precisam para suas orações? Se a língua realmente fosse um obstáculo, não haveria milhões de muçulmanos que não são árabes e que realizam os atos de adoração com facilidade, louvado seja Allah.

Então, apresse-se a entrar no Islam, pois ninguém sabe quando chegará seu tempo determinado (ou seja, a morte). Que Allah o salve e o proteja de Sua ira e punição.

### Como se tornar muçulmano

Tudo o que tens de fazer para entrar nesta grande religião é dizer:

> Ash-hadu alla ilaha illallah wa ash-hadu anna Muhammadan 'abduhu wa Rasuluhu.

O que foi mencionado acima significa: (Testemunho que não há ninguém digno de adoração além de Allah e testemunho que Muhammad é Seu servo e Mensageiro.)

Então, encontrarás entre os teus irmãos muçulmanos pessoas que te ajudarão a aprender a oração e outros assuntos do Islam.
