---
extends: _layouts.answer
section: content
title: Cine a scris Coranul?
date: 2024-03-01
description: Coranul este cuvântul literal al lui Dumnezeu, pe care El l-a revelat
  profetului său Muhammad (pacea fie asupra lui) prin Îngerul Gabriel.
sources:
- href: https://www.islam-guide.com/ch1-1.htm
  title: islam-guide.com
translator_id: 554943
---

Dumnezeu l-a sprijinit pe ultimul Său profet, Muhammad {{ $page->pbuh() }}, cu multe minuni și multe dovezi care au arătat că el este un adevărat profet trimis de Dumnezeu. De asemenea, Dumnezeu a susținut ultima Sa carte revelată, Sfântul Coran, cu multe minuni care dovedesc că acest Coran este cuvântul literal al lui Dumnezeu, revelat de El și că nu a fost scris de nicio ființă umană.

Coranul este cuvântul literal al lui Dumnezeu, pe care El l-a revelat profetului său Muhammad {{ $page->pbuh() }} prin Îngerul Gabriel. Sfântul Coran a fost memorat de Profetul Muhammad {{ $page->pbuh() }}, care apoi l-a dictat însoțitorilor săi. Ei, la rândul lor, l-au memorat, l-au notat și l-au revizuit împreună cu Profetul Muhammad {{ $page->pbuh() }}. Mai mult decât atât, Profetul Muhammad {{ $page->pbuh() }} a revizuit Coranul împreună cu Îngerul Gabriel o dată pe an și de două ori în ultimul an al vieții sale. Din momentul în care a fost revelat Coranul, până în ziua de azi, a existat întotdeauna un număr mare de musulmani care au memorat tot Coranul, literă cu literă. Unii dintre ei au reușit chiar să memoreze întregul Coran până la vârsta de zece ani. Nicio literă a Coranului nu a fost schimbată de-a lungul secolelor.

Coranul, care a fost dezvăluit cu 14 secole în urmă, a menționat fapte descoperite sau dovedite doar recent de oamenii de știință. Acest lucru dovedește fără îndoială că Coranul este cuvântul literal al lui Dumnezeu, revelat profetului Muhammad {{ $page->pbuh() }} și că Coranul nu a fost scris de Profetul Muhammad {{ $page->pbuh() }} sau de niciun alt autor (altă ființă umană). Aceasta dovedește, de asemenea, că Profetul Muhammad {{ $page->pbuh() }} este cu adevărat un profet trimis de Dumnezeu. Este dincolo de rațiune că oricine cu 1400 de ani în urmă ar fi cunoscut aceste fapte descoperite sau dovedite doar recent cu echipamente avansate și metode științifice sofisticate.
