---
extends: _layouts.answer
section: content
title: Was sind die wichtigsten Praktiken des Islam?
date: 2024-10-28
description: Die fünf Säulen des Islam umfassen das Glaubensbekenntnis, die Gebete, die Wohltätigkeit, das Fasten und die Pilgerfahrt.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 587746
---

Die wichtigsten Praktiken des Islam werden als die fünf Säulen bezeichnet.

### Die erste Säule: Das Glaubenszeugnis

Zu bezeugen, dass es keinen Gott gibt, der anbetungswürdig ist außer Allah, und dass Muhammad Sein letzter Gesandter ist.

### Die zweite Säule: Gebete

Fünfmal täglich zu verrichten: je einmal bei Morgendämmerung, am Mittag, am Nachmittag, nach Sonnenuntergang und nachts.

### Die dritte Säule: Vorgeschriebene Wohltätigkeit „Zakat“

Diese verpflichtende jährliche Abgabe für Bedürftige wird als kleiner Anteil des Gesamtvermögens berechnet. Sie beträgt 2,5 % des finanziellen Vermögens und kann weitere Besitztümer umfassen. Geleitet wird sie von denjenigen, die über genug Wohlstand verfügen, um andere zu unterstützen.

### Die vierte Säule: Fasten im Monat Ramadan

Während dieses Monats müssen Muslime von der Morgendämmerung bis zum Sonnenuntergang auf jegliches Essen, Trinken und sexuelle Beziehungen mit ihren Ehepartnern verzichten. Es fördert Selbstbeherrschung, Gottesbewusstsein und Mitgefühl für die Armen.

### Die fünfte Säule: Die Pilgerfahrt

Jeder Muslim, der dazu fähig ist, muss im Laufe seines Lebens eine Pilgerreise nach Mekka unternehmen. Diese Reise umfasst Gebete, Bittgebete, Wohltätigkeit und Reisen und ist eine tief bewegende und spirituelle Erfahrung.
