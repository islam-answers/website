---
extends: _layouts.answer
section: content
title: šta islam kaže o tretmanu roditelja?
date: 2024-09-06
description: Pokazivanje ljubaznosti prema roditeljima jedno je od najviše nagrađenih
  dela u očima Boga. Zapovest da se bude dobar prema roditeljima je jasno učenje Kur'ana.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 584497
---

Pokazivanje ljubaznosti prema roditeljima jedno je od najviše nagrađenih dela u očima Allaha. Zapovest da se bude dobar prema roditeljima je jasno učenje Kur'ana. Allah kaže:

{{ $page->verse('4:36..') }}

Pominjanje služenja roditelja sledi odmah na posle služenja Bogu. Oo se ponavlja u celom Kur'anu.

{{ $page->verse('17:23') }}

U ovom ajetu, Bog naređuje klanjanje samo Njemu i nikome više, i odmah zatim pažnju usmerava na ponašanje prema roditeljima, posebno kada ostare i postanu ovisni o drugima za sklonište i svoje potrebe. Muslimani se uče da ne budu grubi prema roditeljima i da ih ne grde, nego da im se lepo obračaju pokazuju ljubav i nežnost.

U Kur'anu, Allah dalje traži od osobe da Mu se moli za svoje roditelje:

{{ $page->verse('17:24') }}

Allah naglašava da je poslušnost roditeljima poslušnost Njemu, osim pripisivanja Mu partnera.

{{ $page->verse('31:14-15..') }}

Ovaj ajet naglašava da svi roditelju zaslužuju dobar tretman. Majkama treba pokazati posebnu pažnju, zbog svih muka koje su pretrpele.

Briga o roditeljima se smatra jednim od najboljih dela:

{{ $page->hadith('muslim:85e') }}

Kao zaključak, u islamu, poštovanje roditelja znači pokoravati im se, moliti se za njih, ne povisiti ton u njihovoj blizini, smešiti im se, ne pokazivati nezadovoljstvo prema njima, nastojati da im služiš, ispunjavaš njihove želje, konsultuješ i saslušaš šta imaju da kažu.
