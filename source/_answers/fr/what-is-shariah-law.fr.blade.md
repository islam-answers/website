---
extends: _layouts.answer
section: content
title: Qu’est ce que la Chariah ?
date: 2024-07-14
description: La Chariah englobe toute la religion choisie par Allah pour Ses esclaves-serviteurs
  afin de les sortir des ténèbres vers la lumière.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 554943
---

Premièrement, la Chariah englobe toute la religion choisie par Allah pour Ses esclaves-serviteurs afin de les sortir des ténèbres vers la lumière.C’est la législation établie et expliquée pour eux. Elle renferme des ordres, des interdits, du licite et de l’illicite.

Quiconque suit la loi d’Allah, en applique le licite et évite l’illicite, aura réussi.

Celui qui viole la loi d’Allah s’expose à Sa colère et partant à Son chatîment.

Allah le Très-haut dit:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (puisse Allah lui accorder Sa miséricorde) a dit:

Chariah est le singulier de charaaie. Elle signifie ce qu’Allah établit pour les fidèles en fait de religion, et leur donne l’ordre de s’y attacher comme la prière, le jeûne et consort. On l’appelle aussi chir’a.

Ibn Hazem (puisse Allah lui accorder Sa miséricorde) a dit:

Chariah est ce qu’Allah le Très-haut a établi en matière religieuse et fait exprimer par Son Prophète {{ $page->pbuh() }} et par les prophètes prédédents.On en utilise la partie abrogeant dans les jugements.

### Origine linguistique du terme Shari'ah

Linguistiquement, elle renvoie à l’endroit où le voyageur peut trouver de l’eau ou la rivière où l’on peut boire. Sous ce rapport, le Très-haut :

{{ $page->verse('42:13') }}
