---
extends: _layouts.answer
section: content
title: 穆斯林相信耶稣的哪些事迹？
date: 2024-07-12
description: 穆斯林尊敬并尊崇耶稣。 他们将他视为真主派遣给人类的最伟大信使之一。
sources:
- href: https://www.islam-guide.com/ch3-10.htm
  title: islam-guide.com
translator_id: 554943
---

穆斯林尊敬并尊崇耶稣（他所带来的和平）。 他们将他视为真主派遣给人类的最伟大信使之一。 古兰经确认他是童贞女之子，并在古兰经中标题为 Maryam [玛利亚] 的章节中有所描述。 古兰经说明耶稣的出生如下：

{{ $page->verse('3:45-47') }}

耶稣和亚当一样，都是在真主的旨意下奇迹般的出生，而没有父亲。真主说道：

{{ $page->verse('3:59') }}

在担任先知期间，耶稣曾展现许多奇迹。 真主告诉我们耶稣曾说：

{{ $page->verse('3:49..') }}

穆斯林相信耶稣并没有被钉在十字架上。 而是耶稣的敌人打算把他钉在十字架上，但真主拯救他并将他养育成人。 而且可能是他人代耶稣承受这种劫难。 耶稣的敌人捉走此人并将他钉在十字架上，并认为他就是耶稣。真主说道：

{{ $page->verse('..4:157..') }}

穆罕默德 {{ $page->pbuh() }} 及耶稣都未变更较早的先知所传播的一个真主基本信仰，并加以确认且未曾变更。
