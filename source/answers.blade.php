---
title: Answers
description: Index of all questions and answers
---

@extends('_layouts.main')

@section('body')
<nav class="breadcrumbs text-xs mb-4" itemscope itemtype="https://schema.org/BreadcrumbList">
    <ol>
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <a href="/" title="{{ $page->siteName }}" class="text-blue-600" itemprop="item">
                <span itemprop="name">{{ $page->siteName }}</span>
            </a>
            <meta itemprop="position" content="1" />
        </li>
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <span class="text-gray-600" itemprop="name">{{ $page->title }}</span>
            <meta itemprop="position" content="2" />
        </li>
    </ol>
</nav>

    <h1>Answers</h1>

    @include('_components.languages-list', ['compact' => true])

    <p class="text-gray-600 text-xs mt-0">
        <b>{{ $answers->count() }}</b> answers in <b>{{ $languages->count() }} languages.
    </p>

@stop
