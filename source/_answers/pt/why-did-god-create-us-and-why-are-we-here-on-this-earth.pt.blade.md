---
extends: _layouts.answer
section: content
title: Por que Deus nos criou e por que estamos aqui na Terra?
date: 2025-01-06
description: Uma das maiores razões pelas quais Deus nos criou é o mandamento de afirmar
  a Sua Unicidade e de O adorarmos só a Ele, sem parceiro ou associado.
sources:
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/108100/
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.info/en/answers/45529/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/127330/
  title: islamqa.org (AskImam.org)
translator_id: 594172
---

Um dos maiores atributos de Allah é a sabedoria, e um dos Seus maiores nomes é al-Hakim (o Prudentíssimo). Ele não criou nada em vão; exaltado seja Deus, muito acima de tal coisa. Pelo contrário, Ele cria as coisas por grandes e sábias razões, e para fins sublimes. Allah afirmou-o no Alcorão:

{{ $page->verse('23:115-116') }}

Allah também diz no Alcorão:

{{ $page->verse('44:38-39') }}

Tal como está provado que existe sabedoria por detrás da criação do homem do ponto de vista da shari'ah (lei islâmica), também está provado do ponto de vista da razão. O homem sábio não pode deixar de aceitar que as coisas foram criadas por uma razão, e o homem sábio considera-se acima de fazer coisas na sua própria vida sem razão, então o que dizer de Allah, o mais sábio dos sábios?

Allah não criou o homem para comer, beber e multiplicar-se, caso em que seria como os animais. Deus honrou o homem e favoreceu-o muito mais do que muitos daqueles que criou, mas muitas pessoas insistem na descrença, pelo que ignoram ou negam a verdadeira sabedoria por detrás da sua criação, e tudo o que lhes interessa é desfrutar dos prazeres deste mundo. A vida de tais pessoas é como a dos animais e, de facto, estão ainda mais desviados. Allah diz:

{{ $page->verse('..47:12') }}

Uma das maiores razões pelas quais Allah criou a humanidade - que é uma das maiores provas - é a ordem para afirmar a Sua Unicidade e adorá-Lo sozinho, sem parceiro ou associado. Allah declarou esta razão para a criação da humanidade, como Ele diz:

{{ $page->verse('51:56') }}

Ibn Kathir (que Allah tenha misericórdia dele) disse:

> Isto significa: "Criei-os para lhes ordenar que Me adorem, e não porque tenha necessidade deles".

Como muçulmanos, o nosso objetivo na vida é profundo, mas simples. Esforçamo-nos por colocar todos os aspectos do nosso ser exterior e interior em total conformidade com o que Allah ordenou. Allah não nos criou porque precisa de nós. Ele está para além de todas as necessidades, e somos nós que precisamos de Allah.

Allah disse-nos que a criação dos céus e da terra, da vida e da morte, foi feita com o objetivo de pôr à prova o homem. Quem Lhe obedecer, recompensá-lo-á, e quem Lhe desobedecer, castigá-lo-á. Allah diz:

{{ $page->verse('67:2') }}

Desta prova resulta uma manifestação dos nomes e atributos de Allah, tais como os nomes de Allah al-Rahman (o Misericordioso), al-Ghafur (o Perdoador), al-Hakim (o Sábio), al-Tawwab (o Remissório), al-Rahim (o Misericordiador), e outros nomes de Allah.
