---
extends: _layouts.answer
section: content
title: Ce este Jihadul?
date: 2024-03-23
description: Esența Jihadului este să lupți și să te sacrifici pentru religie într-un
  mod care să fie plăcut lui Dumnezeu.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 554943
---

Esența Jihadului este să lupți și să te sacrifici pentru religie într-un mod care să fie plăcut lui Dumnezeu. Din punct de vedere lingvistic, "jihad" înseamnă „a lupta” și se poate referi la o luptă interioară - de exemplu, la străduința omului de a face fapte bune, a face acte de caritate sau de cumpătare și abținere de la păcate.

Cea mai cunoscută formă a jihadului este cel militar, care este permis pentru a păstra bunăstarea societății, pentru a preveni răspândirea opresiunii și pentru a promova justiția.
