---
extends: _layouts.answer
section: content
title: Da li su vam potrebni svedoci kada izgovarate šehadet?
date: 2024-07-21
description: Nije neophodno izgovoriti šehadet (svedočanstvo vere) pred svedocima.
  Islam je stvar koja je između osobe i njegovog Gospodara.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 584497
---

Da bi osoba postala musliman, nije neophodno da obznani svoju odluku pred bilo kim. Islam je stvar koja je između osobe i Gospodara njegovog, neka je blagosloven i uzvišen.

Ako osoba traži od ljudi da svedoče o njegovom prihavtanju islama kako bi to moglo biti dokumentovano među njegovim ličnim dokumentima, nema ništa loše u tome, ali to treba da se uradi bez da se to učini uslovom da njegovo prihvatanje islama bude validno.
