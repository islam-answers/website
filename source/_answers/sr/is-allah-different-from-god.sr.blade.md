---
extends: _layouts.answer
section: content
title: Da li je Allah drugačiji od Boga?
date: 2024-07-29
description: Muslimani obožavaju istog Boga kojeg su obožavali proroci Noa,
  Abraham, Mojsije i Isus. Reč "Allah" je jednostavno arapska
  reč za Svemogućeg Boga.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 584497
---

Muslimani obožavaju istog Boga kojeg su obožavali proroci Noa, Abraham, Mojsije i Isus. Reč "Allah" je jednostavno arapska reč za Svemogućeg Boga – arapska reč bogatog značenja, koja označava jednog i jedinog Boga. Allah je takođe ista reč koju hrišćani i Jevreji, koji govore arapski, koriste kada govore o Bogu.

Međutim, iako muslimani, Jevreji i hrišćani veruju u istog Boga (Stvoritelja), njihovi koncepti u vezi sa Njim se značajno razlikuju. Na primer, muslimani odbacuju ideju da Bog ima bilo kakve partnere ili da je deo "trojstva", i pripisuju savršenstvo samo Bogu, Svemogućem.
