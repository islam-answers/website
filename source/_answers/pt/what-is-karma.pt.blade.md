---
extends: _layouts.answer
section: content
title: O que é o Carma?
date: 2025-02-26
description: O carma, crença em consequências das ações, é um conceito falso das religiões
  indianas. No Islam, só Allah governa a recompensa e o castigo.
sources:
- href: https://www.islamweb.net/en/fatwa/343198/
  title: islamweb.net
- href: https://islamqa.info/ar/answers/183131/
  title: islamqa.info
translator_id: 594172
---

"Carma" é um termo comum nas religiões indianas (hinduísmo, jainismo, sikhismo e budismo). A palavra "Carma" refere-se às ações de um ser vivo e às consequências morais que delas resultam. Qualquer ato de bem ou mal, seja uma palavra, uma ação ou apenas um pensamento, deve ter consequências (recompensa ou punição), desde que seja resultado de consciência e percepção prévias.

Para estas religiões, o sistema de Carma funciona de acordo com uma lei moral natural, e não está sob a autoridade de julgamentos divinos. Segundo elas, o Carma determina coisas como a aparência externa, a beleza, a inteligência, a idade, a riqueza e o estatuto social. Afirmam também que a lei do Carma governa tudo o que é criado, e é uma lei imutável. Afirmam que esta lei governa e monitoriza todos os momentos e, portanto, cada uma das nossas boas e más acções tem as suas consequências.

Foi afirmado na "Enciclopédia Facilitada das Doutrinas e Partidos das Religiões Contemporâneas":

> O Carma - segundo os hindus - é a lei da retribuição, o que significa que o sistema do universo é divino e baseado na justiça pura, justiça essa que ocorrerá inevitavelmente ou na vida atual ou na próxima vida, e a retribuição de uma vida será noutra vida (...)

Também afirmou:

> E o homem continua a nascer e a morrer enquanto o carma estiver ligado à sua alma, e a sua alma não se purifica enquanto não se livrar do carma, onde os seus desejos terminam, e então ele permanece vivo e imortal na bem-aventurança da salvação, que é o estágio do "Nirvana" ou salvação que pode ser obtida neste mundo através do treino e exercício ou através da morte.

### O Carma é um conceito verdadeiro?

Não há dúvida de que estas religiões indianas são religiões pagãs, formadas de acordo com falsas crenças e percepções impossíveis e imaginárias. A crença no "carma" está entre as falsas crenças que estas pessoas acreditam e aderem.

Podemos resumir as razões para dizer que esta crença corrupta é falsa da seguinte forma:

1. É uma crença fabricada, não baseada numa revelação divina infalível, mas sim numa religião pagã inventada.
1. É um sistema que funciona de acordo com uma lei moral natural que é autossuficiente, independente da lei divina e das crenças religiosas celestiais.
1. Afirmam que é uma lei dominante que controla todas as criaturas, monitoriza as acções, gere os destinos e recompensa as acções. Isto é uma clara blasfémia, pois Deus é o dominante e aquele que conduz todos os assuntos e aquele que responsabiliza as pessoas pelas suas acções.
1. Esta crença faz parte do seu sistema de falsas crenças que pretendem atingir o estádio da salvação eterna, como afirmam, que é o objetivo mais elevado para eles, uma vez que o carma é a consequência das acções que as pessoas praticam, não havendo salvação enquanto o carma existir.

E nós, louvado seja Deus, somos independentes dessas falsas crenças e seitas inventadas por meio da religião de Deus e da graça de Deus. Basta dizer:

{{ $page->verse('99:7-8') }}

Basta-nos saber que Allah é o Guardião de todas as coisas e que Allah abrange todas as coisas com o Seu conhecimento. Allah diz no Alcorão:

{{ $page->verse('53:31') }}

Quem conhece isso, crê nele e tem a certeza de que Allah ressuscitará aqueles que estão nos túmulos para responsabilizá-los pelo peso de um átomo de ações, e que Ele estabeleceu testemunhas e escribas para registrar essas ações, não precisará dessa falsidade inventada e dessa crença corrupta e desviada para parar de pecar e se abster de palavras, ações e moralidades ruins.
