---
extends: _layouts.answer
section: content
title: Des témoins sont-ils nécessaires pour dire la Shahadah ?
date: 2024-07-14
description: La validité de la conversion à l’Islam ne dépend pas de sa proclamation
  devant des témoins.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 554943
---

La proclamation de la conversion à l’Islam devant quelqu’un n’en constitue pas une condition de validité, la conversion étant une affaire entre le converti et son Maître Béni et Très Haut.

Cependant il n’y a aucun mal à ce que le nouveau converti sollicite des témoins pour bien documenter sa conversion - ce qui n’en est pas une condition de validité.
