@extends('_layouts.main')

@push('head')

@section('title', $page->title)
@section('image', $page->image())

@if(($languages = $page->getAllLanguages())->count() > 1)
@foreach($languages as $item)
    <link rel="alternate" hreflang="{{ $item->getLanguage() }}" href="{{ $item->getCanonicalUrl() }}" />
@endforeach
@endif

<meta property="article:published_time" content="{{ $page->getDate()->format(DateTime::ATOM) }}" />

@endpush

@section('html'){!! 'itemscope itemtype="https://schema.org/QAPage"' !!}@endsection

@section('body')
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "BlogPosting",
    "headline": "{{ $page->title }}",
    "image": "{{ $page->image() }}",
    "datePublished": "{{ $page->getDate()->format(DateTime::ATOM) }}",
    "dateModified": "{{ $page->getDate()->format(DateTime::ATOM) }}",
    "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "{{ $page->getCanonicalUrl() }}"
    },
    "publisher": {
        "@type": "Organization",
        "name": "{{ $page->siteName }}",
        "url": "{{ $page->baseUrl }}/"
    }
}
</script>

<article itemprop="mainEntity" itemscope itemtype="https://schema.org/Question">
    <meta itemprop="answerCount" content="1" />
    <meta itemprop="datePublished" content="{{ $page->getDate()->format(DateTime::ATOM) }}" />
    <meta itemprop="text" content="{{ $page->title }}" />
    <meta itemprop="image" content="{{ $page->image() }}" />

    <div class="leading-tight mb-6 p-8 bg-center bg-cover bg-no-repeat rounded-lg shadow-lg text-white flex flex-col" style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), {{ $page->imageSet() }}; min-height: 200px;">
        <div class="flex justify-between items-start">
            <nav class="text-xs mb-2 breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
                <ol>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <meta itemprop="item" content="{{ $page->baseUrl }}/" />
                        <a href="{{ $page->baseUrl }}/" title="{{ $page->siteName }}" class="text-white hover:text-blue-200"><span itemprop="name">{{ $page->siteName }}</span></a>
                        <meta itemprop="position" content="1" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <meta itemprop="item" content="{{ $page->baseUrl }}/answers/" />
                        <a href="{{ $page->baseUrl }}/answers/" title="{{ $page->getString('answers') }}" class="text-white hover:text-blue-200"><span itemprop="name">{{ $page->getString('answers') }}</span></a>
                        <meta itemprop="position" content="2" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <meta itemprop="item" content="{{ $page->baseUrl }}/answers/{{ $page->getLanguage() }}/" />
                        <a href="{{ $page->baseUrl }}/answers/{{ $page->getLanguage() }}/" title="{{ $page->getLanguageName() }}" class="text-white hover:text-blue-200" itemprop="name"><span itemprop="name">{{ $page->getLanguageName() }}</span></a>
                        <meta itemprop="position" content="3" />
                    </li>
                    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <meta itemprop="item" content="{{ $page->getCanonicalUrl() }}" />
                        <span class="text-white" itemprop="name">{{ $page->title }}</span>
                        <meta itemprop="position" content="4" />
                    </li>
                </ol>
            </nav>

            <div class="flex space-x-2">
                <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($page->getCanonicalUrl()) }}"
                   aria-label="Share on Facebook"
                   target="_blank"
                   class="text-white hover:text-blue-200">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
                </a>
                <a href="https://x.com/intent/post?url={{ urlencode($page->getCanonicalUrl()) }}&text={{ urlencode($page->title . ' - ' . $page->description) }}"
                   aria-label="Share on X (formerly Twitter)"
                   target="_blank"
                   class="text-white hover:text-blue-200">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 24 24"><path d="M14.283 10.153L23.218 0H21.101L13.343 8.821L7.147 0H0L9.37 13.338L0 24H2.117L10.31 14.671L16.853 24H24L14.283 10.153H14.283ZM11.383 13.467L10.434 12.127L2.88 1.559H6.132L12.228 10.092L13.178 11.423L21.102 22.505H17.85L11.383 13.467V13.467Z"/></svg>
                </a>
                <a href="https://www.linkedin.com/sharing/share-offsite/?url={{ urlencode($page->getCanonicalUrl()) }}"
                    aria-label="Share on LinkedIn"
                    target="_blank"
                    class="text-white hover:text-blue-200">
                    <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 24 24"><path d="M20.447 20.452h-3.554v-5.569c0-1.328-.027-3.037-1.852-3.037-1.853 0-2.136 1.445-2.136 2.939v5.667H9.351V9h3.414v1.561h.046c.477-.9 1.637-1.85 3.37-1.85 3.601 0 4.267 2.37 4.267 5.455v6.286zM5.337 7.433c-1.144 0-2.063-.926-2.063-2.065 0-1.138.92-2.063 2.063-2.063 1.14 0 2.064.925 2.064 2.063 0 1.139-.925 2.065-2.064 2.065zm1.782 13.019H3.555V9h3.564v11.452zM22.225 0H1.771C.792 0 0 .774 0 1.729v20.542C0 23.227.792 24 1.771 24h20.451C23.2 24 24 23.227 24 22.271V1.729C24 .774 23.2 0 22.222 0h.003z"/></svg>
                </a>
                <button onclick="navigator.share && navigator.share({title: '{{ $page->title }}', text: '{{ $page->description }}', url: '{{ $page->getCanonicalUrl() }}'})" class="text-white hover:text-blue-200" id="navigatorShare" aria-label="Share with other applications">
                    <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z"></path></svg>
                </button>
                <script>
                    if (!('share' in navigator)) {
                        document.getElementById('navigatorShare').style.display = 'none';
                    }
                </script>
                <noscript>
                    <style>
                        #navigatorShare {
                            display: none;
                        }
                    </style>
                </noscript>
            </div>
        </div>

        <h1 itemprop="name" class="text-4xl font-bold flex items-center justify-center flex-grow text-white min-h-[5.5rem]">{{ $page->title }}</h1>
        <div class="text-sm font-bold mt-4">
            @if (count($languages) > 1)
                @foreach($languages as $item)
                    @if ($item->getLanguage() === $page->getLanguage())
                    <span class="text-white">{{ $item->getLanguageName() }}</span>
                    @else
                    <a href="{{ $item->getRealUrl() }}" title="{{ $item->title }}" class="text-white hover:text-blue-200">{{ $item->getLanguageName() }}</a>
                    @endif
                    @if (! $loop->last)
                    <span class="text-white">&middot;</span>
                    @endif
                @endforeach
            @else
                <span class="text-white font-bold">&nbsp;</span>
            @endif
        </div>
    </div>

    <div class="border-b border-blue-200 mb-10 pb-4" v-pre>
        <div itemprop="acceptedAnswer" itemscope itemtype="https://schema.org/Answer">
            <meta itemprop="datePublished" content="{{ $page->getDate()->format(DateTime::ATOM) }}" />
            <div itemprop="text">
                @yield('content')
            </div>
        </div>

        <p class="text-xs text-gray-600">
            @if ($page->sources)
                {{ $page->getString('source') }}:
                @foreach ($page->sources as $source)
                <cite>
                    <a href="{{ $source['href'] }}" title="{{ $source['title'] }}" class="text-blue-600" target="_blank" rel="noopener noreferrer">{{ $source['title'] }}</a>
                </cite>
                @if (! $loop->last)
                &middot;
                @endif
                @endforeach
                <br />
            @endif

            @if (($translator = $page->translator()) && $translator['languages']->contains($page->getLanguage()))
                {{ $page->getString('translated_by') }}:
                <a href="{{ $page->translator()['link'] }}" title="{{ $page->translator()['name'] }}" class="text-blue-600" target="_blank">{{ $page->translator()['name'] }}</a>

                <br />
            @endif
        </p>
    </div>
</article>

@if ($page->related()->count() > 0)
<span class="text-2xl font-bold text-gray-800">
    {{ $page->getString('similar_questions') }}
</span>
<div class="mt-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
    @foreach ($page->related()->take(3) as $answer)
    <a href="{{ $answer->getRealUrl() }}" title="{{ $answer->title }}" class="group">
        <div class="relative rounded-lg overflow-hidden shadow-md transition-transform duration-300 hover:scale-105">
            <div class="absolute inset-0 bg-cover bg-center" style="background-image: {{ $answer->imageSet() }};"></div>
            <div class="absolute inset-0 bg-black bg-opacity-50 group-hover:bg-opacity-40 transition-opacity"></div>
            <div class="relative p-6 h-40 flex items-center justify-center">
                <h3 class="text-white text-lg font-semibold text-center">{{ $answer->title }}</h3>
            </div>
        </div>
    </a>
    @endforeach
</div>
@endif

@endsection
