---
extends: _layouts.answer
section: content
title: Je li Isus bio razapet?
date: 2024-09-25
description: Prema Kuranu, Isus nije ubijen niti razapet. Isusa je Allah uzdigao živog
  na nebesa, a drugi čovek je razapet umesto njega.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 584497
---

Kada su Jevreji i rimski car skovali zaveru da ubiju Isusa {{ $page->pbuh() }}, Allah je jednog od prisutnih učinio sličnim u svemu Isusu. Dakle, ubili su čoveka koji je izgledao kao Isus. Oni nisu ubili Isusa. Isus {{ $page->pbuh() }}, živ je podignut na nebesa. Dokaz za to su Allahove reči o izmišljotini jevreja i pobijanju istih:

{{ $page->verse('4:157-158') }}

Tako je Allah proglasio lažnim riječi Jevreja kada su tvrdili da su ga ubili i razapeli, i rekao da ga je On uzeo k sebi. To je bila Allahova milost prema Isusu (Isau), {{ $page->pbuh() }}, i bila mu je čast, da bi on bio jedan od Njegovih znakova, čast koju Allah daje onome kome On hoće od Njegovih poslanika.

Implikacija reči “Ali Allah ga je uzdigao (sa njegovim telom i dušom) Sebi” je da je Allah, dž.š., uzeo Isusa telom i dušom, kako bi pobio tvrdnju Židova da su ga razapeli i da su ga ubili, jer ubijanje i razapinjanje imaju veze s fizičkim telom. Štoviše, da je On samo uzeo Isusovu dušu, to ne bi isključilo tvrdnju da ga je ubio i razapeo, jer samo uzimanje duše ne bi bilo pobijanje njihove tvrdnje. Također ime Isusa {{ $page->pbuh() }}, u stvarnosti, odnosi se na njega i dušom i telom zajedno; ne može se odnositi samo na jedno od njih, osim ako postoji naznaka u tom smislu. Nadalje, uzimanje i njegove duše i njegovog tela zajedno je pokazatelj savršene Allahove moći i mudrosti, i Njegove časti i podrške kome god hoće od Njegovih poslanika, u skladu s onim što On, neka je uzvišen, kaže na kraju. ajeta: “A Allah je svemoćan i mudar.”
