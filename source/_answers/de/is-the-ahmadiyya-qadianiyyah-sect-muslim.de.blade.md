---
extends: _layouts.answer
section: content
title: Ist die Ahmadiyya-Sekte (Qadianiyyah) muslimisch?
date: 2025-02-09
description: Ahmadiyya ist eine fehlgeleitete Gruppe, die überhaupt nicht zum Islam
  gehört. Ihre Glaubensinhalte stehen im völligen Widerspruch zum Islam.
sources:
- href: https://islamqa.info/en/answers/170092/
  title: islamqa.info
- href: https://islamqa.info/en/answers/4060/
  title: islamqa.info
- href: https://islamqa.info/en/answers/45525/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/103015/
  title: islamqa.org (AskImam.org)
translator_id: 587746
---

Ahmadiyya (Qadianiyyah) ist eine fehlgeleitete Gruppe, die überhaupt nicht Teil des Islams ist. Ihre Glaubensinhalte stehen im völligen Widerspruch zum Islam, daher sollten Muslime sich vor ihren Aktivitäten in Acht nehmen, da die 'Ulama' (Gelehrten) des Islam erklärt haben, dass sie Kaafirs (Ungläubige) sind.

Qadianiyyah ist eine Bewegung, die 1900 als Verschwörung der britischen Kolonialisten auf dem indischen Subkontinent begann. Ihr Ziel war es, Muslime von ihrer Religion und insbesondere von der Verpflichtung zum Jihad abzubringen, damit sie sich nicht im Namen des Islams dem Kolonialismus widersetzen. Das Sprachrohr dieser Bewegung ist die Zeitschrift Majallat Al-Adyaan (Zeitschrift der Religionen), die auf Englisch veröffentlicht wurde.

Mirza Ghulam Ahmad al-Qadiani war das Hauptinstrument zur Gründung der Qadianiyyah. Er wurde 1839 im Dorf Qadian in Punjab, Indien, geboren. Seine Familie war dafür bekannt, ihre Religion und ihr Land verraten zu haben, sodass Ghulam Ahmad in völliger Loyalität und Gehorsam gegenüber den Kolonialherren aufwuchs. Daher wurde er als sogenannter Prophet auserwählt, um Muslime um sich zu scharen und sie von der Führung des Jihad gegen die englischen Kolonialisten abzuhalten. Die britische Regierung erwies ihnen viele Gefälligkeiten, weshalb sie ihr treu ergeben waren. Unter seinen Anhängern war Ghulam Ahmad für seine Unbeständigkeit, zahlreiche gesundheitliche Probleme und seine Abhängigkeit von Drogen bekannt.

Ghulam Ahmad begann seine Tätigkeit als islamischer Daa'iya (Aufrufer zum Islam), um Anhänger um sich zu scharen. Dann behauptete er, ein von Allah inspirierter Mujaddid (Erneuerer) zu sein. Dann ging er einen Schritt weiter und behauptete, der erwartete Mahdi und der verheißene Messias zu sein. Dann behauptete er, ein Prophet zu sein und dass sein Prophetentum höher sei als das von Muhammad {{ $page->pbuh() }}.

Die Qadianis glauben, dass Allah fastet, betet, schläft, aufwacht, schreibt, Fehler macht und Geschlechtsverkehr hat – gepriesen sei Allah weit über alles, was sie sagen. Sie glauben auch, dass ihr Gott Engländer ist, weil er auf Englisch zu ihnen spricht. Sie glauben, dass ihr Buch offenbart wurde. Sein Name ist al-Kitaab al-Mubeen und es unterscheidet sich vom Heiligen Quran. Sie forderten die Abschaffung des Jihads und blinden Gehorsam gegenüber der britischen Regierung, weil, wie sie behaupteten, die Briten „die Autorität“ hätten, wie es im Quran heißt. Sie erlauben auch Alkohol, Opium, Drogen und Rauschmittel.

Die heutigen Gelehrten sind sich einig, dass die Qadianis außerhalb des Islam stehen, da ihr Glaube Dinge beinhaltet, die Unglauben darstellen und den grundlegenden Lehren des Islam widersprechen. Diese Sekte widerspricht dem endgültigen Konsens der Muslime, dass es nach unserem Propheten Muhammad {{ $page->pbuh() }} keinen Propheten mehr gibt. Darauf weisen zahlreiche Texte des Qurans und der Sahih-Sunnah hin.

Die meisten Qadianis leben heute in Indien und Pakistan, einige wenige im besetzten Palästina "Israel" und in der arabischen Welt. Sie versuchen, mit Hilfe der Kolonialisten überall, wo sie leben, sensible Positionen zu erlangen. Auch die britische Regierung unterstützt diese Bewegung und macht es ihren Anhängern leicht, Positionen in Regierungen der Welt, in der Unternehmensverwaltung und in Konsulaten zu erlangen. Einige von ihnen sind auch hochrangige Offiziere in den Geheimdiensten. Um die Menschen zu ihrem Glauben zu bewegen, verwenden die Qadianis alle möglichen Methoden, insbesondere pädagogische Mittel, da sie hochgebildet sind und viele Wissenschaftler, Ingenieure und Ärzte in ihren Reihen haben.

Es ist einem Muslim nicht gestattet, Salaah (Gebet) mit Jama'ah (Gemeinde) hinter einem Imam (Gebetsführer) zu verrichten, der der Ahmedi-Sekte angehört, da diese als Nichtmuslime gelten. Muslime sollten sich auch des Besuchs ihrer Gebetsstätten und Versammlungen enthalten, da sie Ungläubige sind. Es ist einem Muslim auch nicht gestattet, einen von ihnen zu heiraten oder ihm seine Tochter zur Frau zu geben, da sie Ungläubige und Abtrünnige sind.
