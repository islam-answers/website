---
extends: _layouts.answer
section: content
title: Glauben Muslime an die Jungfrau Maria?
date: 2024-12-16
description: Mariam wird im Quran als eine Person des Glaubens erwähnt. Die Muslime
  respektieren sie und glauben an ihre Reinheit und Unschuld.
sources:
- href: https://islamqa.info/en/answers/241999/
  title: islamqa.info
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/32915/
  title: islamqa.org (SeekersGuidance.org)
translator_id: 587746
---

Mariam (Maria), die Tochter von 'Imraan, möge Allah mit ihr zufrieden sein, ist eine der hingebungsvollen Anbeterinnen und rechtschaffenen Frauen. Sie wird im Quran erwähnt, wo sie als Besitztümer der gläubigen Menschen beschrieben wird. Sie war eine Gläubige, deren Glaube, Bejahung der Einheit Allahs und Gehorsam Ihm gegenüber vollkommen waren.

Der Quran beschreibt sie als eine wahre Dienerin Allahs, die demütig und gehorsam gegenüber Allah, dem Herrn der Welten, war. Allah, erhaben sei Er, sagt:

{{ $page->verse('3:42-43') }}

### Mariams Unschuld

Muslime respektieren Mariam und glauben an das, was im Heiligen Quran über sie erwähnt wird, nämlich ihre Reinheit und Unschuld gegenüber den Vorwürfen, die ihr die Feinde Allahs machten:

{{ $page->verse('4:156') }}

Im Koran ist ein ganzes Kapitel nach Mariam benannt, in dem die Geschichte der Jungfrauengeburt sehr schön dargestellt wird. Nach islamischer Auffassung war Mariam eine Jungfrau; sie war weder vor noch zur Zeit der Geburt Jesu verheiratet {{ $page->pbuh() }}. Mariam war ziemlich schockiert, als der Engel ihr mitteilte, dass sie einen Sohn gebären würde, und sagte:

{{ $page->verse('19:20') }}

Die islamischen Quellen berichten weder von einer Verlobung mit Mariam, noch von Joseph, noch von einer späteren Ehe, noch von irgendwelchen Geschwistern für Jesus {{ $page->pbuh() }}.

Unter den Muslimen ist sie die vollkommene Muslimin, eine der tugendhaftesten Frauen des Paradieses, wie der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:2431') }}

In einer anderen Überlieferung berichtete der Prophet {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3432') }}

### Die wundersame Geburt Jesu - ein Zeichen für die ganze Menschheit

Muslime glauben, dass Allah sie und ihren Sohn zu einem Zeichen für die gesamte Menschheit und zu einem Zeugnis für die Einheit, Herrschaft und Macht Allahs gemacht hat. Allah, erhaben sei Er, sagt:

{{ $page->verse('23:50..') }}

Er sagt auch:

{{ $page->verse('3:45') }}

Allah warnt auch diejenigen, die Jesus und seine Mutter Mariam als Gottheiten betrachten und sie mit Allah gleichsetzen:

{{ $page->verse('5:75..') }}

Allah, der Allmächtige, hat uns mitgeteilt, dass der Prophet Allahs, 'Isa ibn Mariam (Jesus, Sohn der Maria) {{ $page->pbuh() }} am Tag der Auferstehung den Polytheismus derjenigen verleugnen wird, die ihn mit Allah assoziieren und über ihn und seine Mutter übertreiben. Lasst uns über diesen erhabenen Austausch nachdenken, der zwischen dem Herrn der Herrlichkeit und Majestät und seinem Diener und Gesandten 'Isa ibn Mariam {{ $page->pbuh() }} stattfinden wird. Allah, der Allmächtige, sagt:

{{ $page->verse('5:116-117') }}
