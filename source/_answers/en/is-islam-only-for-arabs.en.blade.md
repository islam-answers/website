---
extends: _layouts.answer
section: content
title: Is Islam only for Arabs?
date: 2024-03-13
description: Islam is not only for Arabs. The majority of Muslims are non-Arabs, and
  Islam is a universal religion for all people.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=MSC01&articleID=MSC010003&articlePages=1
  title: spubs.com
---

The fastest way to prove that this is completely false is to state the fact that only about 15% to 20% of the Muslims in the world are Arabs. There are more Indian Muslims than Arab Muslims, and more Indonesian Muslims than Indian Muslims! Believing that Islam is only a religion for Arabs is a myth that was spread by the enemies of Islam early in its history. This mistaken assumption is possibly based on the fact that most of the first generation of Muslims were Arabs, the Qur'an is in Arabic and the Prophet Muhammad {{ $page->pbuh() }} was an Arab. However, both the teachings of Islam and the history of its spread show that the early Muslims made every effort to spread their message of Truth to all nations, races and peoples.

Furthermore, it should be clarified that not all Arabs are Muslims and not all Muslims are Arabs. An Arab can be a Muslim, Christian, Jew, atheist - or of any other religion or ideology. Also, many countries that some people consider to be "Arab" are not "Arab" at all -- such as Turkey and Iran (Persia). The people who live in these countries speak languages other than Arabic as their native tongues and are of a different ethnic heritage than the Arabs.

It is important to realize that from the very beginning of the mission of Prophet Muhammad {{ $page->pbuh() }}, his followers came from a wide spectrum of individuals -- there was Bilal, the African slave; Suhaib, the Byzantine Roman; Ibn Sailam, the Jewish Rabbi; and Salman, the Persian. Since religious truth is eternal and unchanging, and mankind is one universal brotherhood, Islam teaches that Almighty God's revelations to mankind have always been consistent, clear and universal.

The Truth of Islam is meant for all people regardless of race, nationality or linguistic background. Taking a look at the Muslim World, from Nigeria to Bosnia and from Malaysia to Afghanistan is enough to prove that Islam is a Universal message for all of mankind --- not to mention the fact that significant numbers of Europeans and Americans of all races and ethnic backgrounds are coming into Islam.
