---
extends: _layouts.answer
section: content
title: Da li islam tera ljude da postanu mislimani?
date: 2024-10-02
description: Niko ne može biti primoran da prihvati islam. Da bi prihvatio islam,
  čovek mora iskreno i dobrovoljno da veruje i da se pokorava Bogu
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 584497
---

Bog kaže u Kuranu:

{{ $page->verse('2:256..') }}

Iako je dužnost muslimana da prenose i podele prelepu poruku islama, niko ne može biti primoran da prihvati islam. Da bi prihvatio islam, čovek mora iskreno i dobrovoljno da veruje i da se pokorava Bogu, tako da po definiciji, niko ne može (i ne treba) na silu da prihvati islam.

Uzmite u obzir sledeće:

- Indonezija ima najveću muslimansku populaciju, iako se tamo nije vodila bitka za širenje islama.
- Postoji oko 14 miliona arapskih koptskih hrišćana koji generacijama žive u srcu Arabije.
- Islam je danas jedna od najbrže rastućih religija i zapadnom svetu.
- Iako su borba protiv ugnjetavanja i promovisanje pravde valjani razlozi za dzihad, prisiljavanje ljudi da prihvate islam nije jedan od njih.
- Muslimani su vladali Španijom oko 800 godina ali nikada nisu prisiljavali ljude da se preobrate
