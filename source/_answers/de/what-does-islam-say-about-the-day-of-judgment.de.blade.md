---
extends: _layouts.answer
section: content
title: Was sagt der Islam über den Tag des Gerichts?
date: 2024-03-24
description: Es wird ein Tag kommen, an dem das ganze Universum zerstört wird, und
  die Toten wieder auferweckt werden zum Gericht Gottes.
sources:
- href: https://www.islam-guide.com/de/ch3-5.htm
  title: islam-guide.com
---

Wie die Christen glauben Muslime, dass dieses jetzige Leben nur eine Prüfung als Vorbereitung für die nächste Existenzsphäre darstellt. Dieses Leben ist eine Prüfung für jedes Individuum für das Leben nach dem Tod. Es wird ein Tag kommen, an dem das ganze Universum zerstört wird, und die Toten wieder auferweckt werden zum Gericht Gottes. Dieser Tag wird der Anfang eines Lebens sein, das niemals enden wird. Es ist der Tag des Gerichts. An diesem Tag werden alle Menschen von Gott gemäß ihres Glaubens und ihrer Taten belohnt. Jene, die sterben und daran glauben: „Es ist kein wahrer Gott außer Gott, und Muhammad ist der Gesandte (Prophet) Gottes“ und dadurch Muslime sind, werden an diesem Tag belohnt und für immer in das Paradies gelassen, wie Gott sagt:

{{ $page->verse('2:82') }}

Aber jene, die nicht daran „Es ist kein wahrer Gott außer Gott, und Muhammad ist der Gesandte (Prophet) Gottes“ glauben oder keine Muslime sind, werden das Paradies für immer verlieren und ins Höllenfeuer geschickt werden, wie Gott sagt:

{{ $page->verse('3:85') }}

Und wie er sagte:

{{ $page->verse('3:91') }}

Man könnte fragen: „Ich glaube, der Islam ist eine gute Religion, aber wenn ich zum Islam konvertieren würde, würden meine Familie, meine Freunde und andere Leute mich verfolgen und sich über mich lustig machen. Wenn ich also nicht zum Islam konvertiere, werde ich dann in das Paradies kommen und dem Höllenfeuer entgehen?’

Die Antwort gibt Gott in folgendem Quran vers, “Und wer eine andere Religion als den Islam begehrt: nimmer soll sie von ihm angenommen werden, und im Jenseits wird er unter den Verlierern sein.”

Nachdem Er den Propheten Muhammad {{ $page->pbuh() }} geschickt hat, um die Menschen zum Islam zu rufen, akzeptiert Gott kein Festhalten an einer anderen Religion als dem Islam. Gott ist unser Schöpfer und Erhalter. Er erschuf für uns, was sich auf der Erde befindet. Allen Segen und alle guten Dinge bekommen wir von Ihm. Wer sich nach all dem noch weigert, an Gott, seinen Propheten Muhammad {{ $page->pbuh() }}, oder seine Religion, den Islam, zu glauben, so ist es seine Strafe, wenn er oder sie im Jenseits gepeinigt wird. Eigentlich sind wir erschaffen worden, um Gott allein zu dienen und ihm gehorsam zu sein, wie es Gott im Heiligen Quran (51:56) sagt.

Das Leben, das wir hier verbringen, ist ein sehr kurzes Leben. Die Ungläubigen werden am Tag des Gerichts denken, das Leben, das sie auf der Erde verbracht haben, hätte nur einen Tag gedauert oder einen Teil von einem Tag, wie Gott sagt:

{{ $page->verse('23:112-113..') }}

Und wie er sagt:

{{ $page->verse('23:115-116..') }}

Das Leben im Jenseits ist ein reelles Leben. Es ist nicht nur spirituell, sondern auch physikal. Wir werden dort mit unseren Körpern und Seelen leben.

Im Vergleich zwischen dieser Welt und dem Jenseits sagte der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:2858') }}

Das bedeutet, der Wert dieser Welt verglichen mit dem Jenseits ist wie ein paar Tropfen Wasser verglichen mit dem Meer.
