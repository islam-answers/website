---
extends: _layouts.answer
section: content
title: What is Islam?
date: 2024-07-16
description: Islam means submission, humbling oneself, and obeying commands and heeding prohibitions without objection, worshipping Allah alone, and having faith in Him.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
---

If you refer to Arabic language dictionaries, you will find that the meaning of the word Islam is: submission, humbling oneself, and obeying commands and heeding prohibitions without objection, sincerely worshipping Allah alone, believing what He tells us and having faith in Him.

The word Islam has become the name of the religion which was brought by Prophet Muhammad {{ $page->pbuh() }}.

### Why this religion is called Islam

All the religions on earth are called by various names, either the name of a specific man or a specific nation. So Christianity takes its name from Christ; Buddhism takes its name from its founder, the Buddha; Similarly, Judaism took its name from a tribe known as Yehudah (Judah), so it became known as Judaism. And so on.

Except for Islam; for it is not attributed to any specific man or to any specific nation, rather its name refers to the meaning of the word Islam. What this name indicates is that the establishment and founding of this religion was not the work of one particular man and that it is not only for one particular nation to the exclusion of all others. Rather its aim is to give the attribute implied by the word Islam to all the peoples of the earth. So everyone who acquires this attribute, whether he is from the past or the present, is a Muslim, and everyone who acquires this attribute in the future will also be a Muslim.

Islam is the religion of all the Prophets. It appeared at the beginning of Prophethood, from the time of our father Adam, and all the messages called to Islam (submission to Allah) in terms of belief and basic rulings. The believers who followed the earlier prophets were all Muslims in a general sense, and they will enter Paradise by virtue of their Islam.
