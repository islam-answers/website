---
extends: _layouts.answer
section: content
title: Gesù fu crocifisso?
date: 2024-09-22
description: Secondo il Corano, Gesù non fu ucciso o crocifisso. Gesù fu resuscitato
  vivo nei Cieli da Allah, e un altro uomo fu crocifisso al suo posto.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 573107
---

Quando gli ebrei e l'imperatore romano complottarono per uccidere Gesù {{ $page->pbuh() }}, Allah fece in modo che una delle persone presenti fosse simile in tutto e per tutto a Gesù. Così, uccisero l'uomo che assomigliava a Gesù. Non uccisero Gesù. Gesù {{ $page->pbuh() }} fu resuscitato ed elevato vivo al cielo. La prova di ciò sono le parole di Allah riguardanti la falsificazione degli Ebrei e la loro confutazione:

{{ $page->verse('4:157-158') }}

Così Allah dichiarò false le parole degli Ebrei quando affermarono di aver ucciso e crocifisso Gesù {{ $page->pbuh() }}, e affermò di averlo portato presso di sé. Questa fu la misericordia di Allah nei confronti di Gesù (Isa) {{ $page->pbuh() }}, e fu un onore per lui, affinché fosse uno dei Suoi segni, un onore che Allah concede a chiunque Egli voglia dei Suoi messaggeri.

Il significato delle parole "Ma Allah lo ha elevato fino a sé" è che Allah, che Egli sia glorificato, ha innalzato Gesù con il corpo e l'anima, così da confutare l'affermazione degli Ebrei secondo i quali lo avevano crocifisso e ucciso, perché uccidere e crocifiggere hanno a che fare con il corpo fisico. Inoltre, se Egli avesse elevato solo la sua anima, ciò non escluderebbe l'affermazione di averlo ucciso e crocifisso, perché elevare solo l'anima non sarebbe una confutazione della loro affermazione. Anche il nome di Gesù {{ $page->pbuh() }}, in realtà, si riferisce a lui sia nell'anima che nel corpo; non può riferirsi ad uno solo di essi, a meno che non ci sia un'indicazione chiara in tal senso. Inoltre, innalzare insieme la sua anima e il suo corpo indica la perfezione della potenza e della saggezza di Allah, e del Suo rispetto e sostegno a chiunque Egli voglia tra i Suoi Messaggeri, secondo quanto Egli, che Egli sia esaltato, dice alla fine del versetto: "Allah è Eccelso, Saggio".
