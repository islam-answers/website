---
extends: _layouts.answer
section: content
title: Ce cred musulmanii despre Iisus?
date: 2024-02-27
description: Musulmanii îl respectă și reveră pe Iisus. El este considerat unul dintre
  cei mai mari mesageri ai lui Dumnezeu trimis către omenire.
sources:
- href: https://www.islam-guide.com/ch3-10.htm
  title: islam-guide.com
translator_id: 554943
---

Musulmanii îl respectă și îl venerează pe Iisus {{ $page->pbuh() }}. El este considerat unul dintre cei mai mari mesageri ai lui Dumnezeu pentru omenire. Coranul confirmă nașterea sa din fecioară, iar un întreg capitol al Coranului este intitulat „Maryam” (Maria). Coranul descrie nașterea lui Iisus după cum urmează:

{{ $page->verse('3:45-47') }}

Iisus s-a născut în mod miraculos prin porunca lui Dumnezeu, aceeași poruncă care l-a adus pe Adam în existențǎ fǎrǎ tată sau mamă. Dumnezeu a spus:

{{ $page->verse('3:59') }}

În timpul misiunii sale profetice, Iisus a făcut multe minuni. Dumnezeu ne spune că Iisus a spus:

{{ $page->verse('3:49..') }}

Musulmanii cred că Iisus nu a fost crucificat. Planul vrăjmașilor lui Iisus era să-l răstignească, dar Dumnezeu l-a salvat și l-a ridicat la El. Și alt om a fost fǎcut sǎ arate asemǎnǎtor lui Iisus. Dușmanii lui Iisus l-au luat pe acest om și l-au răstignit, crezând că el este Iisus. Dumnezeu a spus:

{{ $page->verse('..4:157..') }}

Nici Profetul Muhammad {{ $page->pbuh() }} nici Iisus nu au venit să schimbe doctrina de bază a credinței într-un singur Dumnezeu, adusă de profeții anteriori, ci mai degrabă să o confirme și să o reînnoiască.
