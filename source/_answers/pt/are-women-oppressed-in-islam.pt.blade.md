---
extends: _layouts.answer
section: content
title: As mulheres são oprimidas no Islam?
date: 2025-01-13
description: O Islam promove a igualdade para as mulheres e condena a opressão. Muitos
  equívocos vêm de práticas culturais, não dos ensinamentos islâmicos.
sources:
- href: https://www.islamweb.net/en/article/109366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 594172
---

Entre os tópicos mais importantes de interesse para os não muçulmanos está o estatuto das mulheres muçulmanas e o tema dos seus direitos, ou melhor, a perceção da falta deles. O retrato que os meios de comunicação social fazem das mulheres muçulmanas, geralmente descrevendo a sua "opressão e mistério", parece contribuir para esta perceção negativa.

A principal razão para isso é o facto de as pessoas frequentemente não distinguirem entre cultura e religião - duas coisas completamente diferentes. De facto, o Islam condena qualquer tipo de opressão, quer seja contra uma mulher ou contra a humanidade em geral.

O Alcorão é o livro sagrado pelo qual os muçulmanos vivem. Este livro foi revelado há 1400 anos a um homem chamado Muhammad {{ $page->pbuh() }}, que mais tarde se tornaria o Profeta, que Allah exalte a sua menção. Passaram catorze séculos e este livro não foi alterado desde então, nem uma letra foi alterada.

No Alcorão, Allah, o Todo-Poderoso, diz:

{{ $page->verse('33:59') }}

Este versículo mostra que o Islam torna necessário o uso do Hijab. Hijab é a palavra usada para cobrir, não apenas os lenços de cabeça (como algumas pessoas podem pensar), mas também usar roupas largas que não sejam demasiado brilhantes.

Por vezes, as pessoas vêem mulheres muçulmanas cobertas e pensam que isso é opressão. Isso está errado. Uma mulher muçulmana não é oprimida, pelo contrário, é libertada. Isto porque ela já não é valorizada por algo material, como a sua boa aparência ou a forma do seu corpo. Ela obriga os outros a julgá-la pela sua inteligência, bondade, honestidade e personalidade. Assim, as pessoas julgam-na por aquilo que ela realmente é.

Quando as mulheres muçulmanas cobrem o cabelo e usam roupas largas, estão a obedecer às ordens do seu Senhor para serem modestas, e não a costumes culturais ou sociais. De facto, as freiras cristãs cobrem o cabelo por modéstia, mas ninguém as considera "oprimidas". Ao seguirem a ordem de Allah, as mulheres muçulmanas estão a fazer exatamente a mesma coisa.

As vidas das pessoas que responderam ao Alcorão mudaram drasticamente. Teve um impacto tremendo em muitas pessoas, especialmente nas mulheres, uma vez que foi a primeira vez que as almas do homem e da mulher foram declaradas iguais - com as mesmas obrigações e as mesmas recompensas.

O Islam é uma religião que tem as mulheres em grande consideração. Há muito tempo, quando nasciam meninos, estes traziam grande alegria à família. O nascimento de uma menina era recebida com muito menos alegria e entusiasmo. Por vezes, as meninas eram tão odiadas que eram enterradas vivas. O Islam sempre se opôs a esta discriminação irracional das meninas e ao infanticídio feminino.

## Os direitos das mulheres no Islam têm sido negligenciados?

No que diz respeito às alterações destes direitos ao longo dos tempos, os princípios básicos não mudaram, mas no que diz respeito à aplicação destes princípios, não há dúvida de que, durante a idade de ouro do Islam, os muçulmanos aplicaram mais a shari'ah (lei islâmica) do seu Senhor, e as regras desta shari'ah incluem honrar a mãe e tratar a esposa, a filha, a irmã e as mulheres em geral de uma forma amável. Quanto mais fraco era o compromisso religioso, mais estes direitos eram negligenciados, mas até ao Dia da Ressurreição continuará a haver um grupo que adere à sua religião e aplica a shari'ah (leis) do seu Senhor. Este é o povo que mais honra as mulheres e lhes concede os seus direitos.

Apesar da fraqueza do empenhamento religioso de muitos muçulmanos nos dias de hoje, as mulheres continuam a gozar de um estatuto elevado, quer como filhas, esposas ou irmãs, embora reconheçamos que há falhas, erros e negligência dos direitos das mulheres entre algumas pessoas, mas cada um responderá por si próprio.

O Islam é uma religião que trata as mulheres de forma justa. A mulher muçulmana recebeu, há 1400 anos, um papel, deveres e direitos de que a maioria das mulheres não goza ainda hoje no Ocidente. Estes direitos provêm de Deus e destinam-se a manter um equilíbrio na sociedade; o que pode parecer "injusto" ou "em falta" num lugar é compensado ou explicado noutro lugar.
