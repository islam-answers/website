---
extends: _layouts.answer
section: content
title: Što muslimani veruju o Isusu?
date: 2024-10-02
description: Muslimani poštuju i veruju u Isusa. Smatraju ga jednim od najvećih Božjih
  poslanika čovečanstvu.
sources:
- href: https://www.islam-guide.com/ch3-10.htm
  title: islam-guide.com
translator_id: 584497
---

Muslimani poštuju i veruju u Isusa (mir s njim). Smatraju ga jednim od najvećih Božjih poslanika čovečanstvu. Kur'an potvrđuje njegovo devičansko rođenje, a jedno poglavlje Kur'ana nosi naslov 'Maryam' (Marija). Kur'an opisuje rođenje Isusa na sljedeći način:

{{ $page->verse('3:45-47') }}

Isus je rođen na čudesan način po Božjoj zapovedi, istoj zapovedi koja je dovela Adama bez oca i majke. Bog je rekao:

{{ $page->verse('3:59') }}

Tokom svoje proročke misije Isus je činio mnoga čuda. Bog nam govori da je Isus rekao:

{{ $page->verse('3:49..') }}

Muslimani veruju da Isus nije razapet. Bio je plan Isusovih neprijatelja da ga razapnu, ali Bog ga je spasio i uzdigao k sebi. I obličje Isusovo stavljeno je na drugog čovjeka. Isus je uzeo neprijatelja ovog čovjeka i razapeo ga misleći da je on Isus. Bog je rekao:

{{ $page->verse('..4:157..') }}

Ni Poslanik Muhammed {{ $page->pbuh() }} ni Isus nisu došli promeniti osnovnu doktrinu verovanja u jednog Boga, koju su doneli raniji poslanici, već je potvrditi i obnoviti.
