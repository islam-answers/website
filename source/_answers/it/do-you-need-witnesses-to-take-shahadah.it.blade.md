---
extends: _layouts.answer
section: content
title: Sono necessari dei testimoni per attestare la Shahadah?
date: 2024-07-14
description: Non è indispensabile attestare la shahadah davanti a dei testimoni. L'Islam, la sottomissione, è un evento che riguarda
  soltanto la persona e il suo Signore.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 573107
---

Affinché una persona diventi musulmana, non è indispensabile che dichiari il proprio Islam dinanzi a qualcuno. L'Islam è una questione che riguarda la persona e il suo Signore, che Egli sia Benedetto e Lodato.

Non vi è niente di sbagliato se si chiede alle persone di testimoniare il proprio Islam dinanzi a qualcuno affinché possa essere un evento registrato tra i suoi atti personali. Tuttavia, tale richiesta non deve essere considerata una delle condizioni necessarie perché la persona possa essere considerata musulmana dopo aver accettato l'Islam
