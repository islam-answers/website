---
extends: _layouts.answer
section: content
title: I musulmani venerano la Kaaba?
date: 2024-12-15
description: I musulmani adorano solo Allah, obbedendo ai Suoi comandamenti, come ad esempio pregare verso la Kaaba, simbolo di unità e monoteismo.
sources:
- href: https://islamqa.info/en/answers/82349/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/85437/
  title: islamweb.net
- href: https://www.islamweb.net/en/article/141834/
  title: islamweb.net
translator_id: 573107
---

La Kaaba è la Qiblah (direzione) verso cui tutti i musulmani si rivolgono mentre eseguono le preghiere. È un dovere volgere il viso verso la Kaaba mentre si prega. Allah dice nel Corano:

{{ $page->verse('2:144..') }}

Ogni anno, più di 2,5 milioni di pellegrini musulmani provenienti da tutto il mondo compiono i riti del Hajj (pellegrinaggio) alla Mecca, dove si trova la Kaaba. Dal punto di vista linguistico, Kaaba significa semplicemente "cubo" in arabo, ma è molto più di un edificio a forma di cubo drappeggiato di nero. È il simbolo dell'unità islamica, il cuore dell'Islam, basato sul principio del monoteismo.

Riferendosi alla Kaaba, Allah ci dice nel Corano che:

{{ $page->verse('3:96') }}

A quel tempo, le fondamenta della Kaaba non erano ancora state gettate da Ibrahim (Abramo), che in seguito avrebbe costruito la Kaaba con suo figlio Ismail (Ismaele), come comandato da Allah:

{{ $page->verse('22:26') }}

In questo versetto vediamo che lo scopo di questa casa era l'adorazione esclusiva di Allah l'Unico, senza alcun partner. Ibrahim aveva trascorso tutta la sua vita rifiutando il politeismo, andando contro le credenze e le usanze di tutta la sua comunità e dei suoi leader, e persino contro quelle di suo padre, che era solito fabbricare idoli con le sue stesse mani.

In quanto musulmani, il nostro principio fondamentale è che adoriamo solo Allah. Azioni come pregare verso la Kaaba e fare il Tawaf (girarci intorno) sono adempimenti dei comandi di Allah, non rappresentano l'adorazione dell'oggetto in sé. Esistono enormi differenze tra il rivolgersi verso la Kaaba e l'adorazione degli idoli.
