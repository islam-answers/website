---
extends: _layouts.answer
section: content
title: O Alcorão foi copiado da Bíblia?
date: 2024-10-28
description: O Alcorão não foi copiado da Bíblia. O Profeta Muhammad era analfabeto,
  não existia nenhuma Bíblia árabe na altura e as revelações vieram diretamente de
  Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 594172
---

Allah criou tudo à nossa volta com um objetivo. O homem também foi criado com um objetivo, que é o de adorar Allah. Allah diz no Alcorão:

{{ $page->verse('51:56') }}

Para guiar a humanidade, Allah enviou profetas de tempos a tempos. Estes profetas receberam revelações de Allah e cada religião possui a sua própria coleção de escrituras divinas, que constituem a base das suas crenças. Estas escrituras são a transcrição material da revelação divina, diretamente, como no caso do profeta Moisés (Musa), que recebeu os mandamentos de Allah, ou indiretamente, como no caso de Jesus (Isa) e Muhammad {{ $page->pbuh() }}, que transmitiram ao povo a revelação que lhes foi comunicada pelo anjo Gabriel (Jibreel).

O Alcorão obriga todos os muçulmanos a acreditar nas escrituras que o precedem. No entanto, como muçulmanos, acreditamos na Tora (Tawrah), no Livro dos Salmos (Zaboor) e no Evangelho (Injeel) na sua forma original, ou seja, da forma como foram revelados, como sendo a palavra de Allah.

## Poderá o profeta Muhammad ter copiado o Alcorão da Bíblia?

A discussão sobre a forma como o Profeta Muhammad {{ $page->pbuh() }} poderia ter aprendido as histórias do Povo do Livro, transmitindo-as depois no Alcorão - o que significa que não se tratava de uma revelação - é uma discussão de várias alegações segundo as quais ele {{ $page->pbuh() }} poderia ter aprendido as histórias diretamente dos seus livros sagrados, ou aprendido o que as escrituras continham de histórias, ordens e proibições, verbalmente do Povo do Livro, se não lhe fosse possível estudar ele próprio os seus livros.

Estas reivindicações podem ser resumidas em três reivindicações básicas:

1. A alegação de que o Profeta Muhammad {{ $page->pbuh() }} não era iletrado ou analfabeto
1. A alegação de que as escrituras cristãs estavam disponíveis para o Profeta {{ $page->pbuh() }} e ele poderia citá-las ou copiá-las
1. A alegação de que Meca era um importante centro educacional para estudos das escrituras

Em primeiro lugar, o texto do Alcorão e da Sunnah indica claramente, sem deixar margem para dúvidas, que o Profeta Muhammad {{ $page->pbuh() }} era, de facto, iletrado e que nunca narrou nenhuma das histórias do Povo do Livro antes de a revelação lhe chegar, e nunca escreveu nada com a sua própria mão. Ele não sabia nada sobre as histórias antes do início da sua missão, e nunca falou delas antes disso.

Allah, exaltado seja, diz:

{{ $page->verse('29:48') }}

Os livros da biografia e da história do Profeta não mencionam que o Profeta tenha escrito a revelação ou que ele próprio tenha escrito as suas cartas aos reis. Pelo contrário, é mencionado o contrário: o Profeta Muhammad {{ $page->pbuh() }} tinha escribas que escreviam a revelação e outras coisas. Num texto brilhante de Ibn Khaldun, há uma descrição do nível de educação na Península Arábica pouco antes do início da missão do Profeta. Diz ele:

> Entre os árabes, a capacidade de escrever era mais rara do que os ovos de camelo. A maioria das pessoas era analfabeta, especialmente os habitantes do deserto, porque esta é uma habilidade que se encontra normalmente entre os habitantes das cidades.

Por isso, os árabes não se referiam à pessoa analfabeta como sendo analfabeta, mas descreviam a pessoa que sabia ler e escrever como sendo conhecedora, porque saber ler e escrever era a exceção e não a norma entre as pessoas.

Em segundo lugar, não há qualquer prova, indicação ou sugestão de que existisse uma tradução árabe da Bíblia nessa altura. Al-Bukhari narrou de Abu Hurayrah (que Allah esteja satisfeito com ele) que ele disse:

{{ $page->hadith('bukhari:7362') }}

Este hadith indica que os judeus detinham o monopólio total do texto e da sua explicação. Se os seus textos fossem conhecidos em árabe, não teria havido necessidade de os judeus lerem o texto ou de o explicarem [aos muçulmanos].

Esta ideia é apoiada pelo versículo em que Allah, exaltado seja, diz:

{{ $page->verse('3:78') }}

Talvez o livro mais importante escrito sobre este tema seja o livro de Bruce Metzger, Professor de Língua e Literatura do Novo Testamento, The Bible in Translation, no qual ele diz:

> É muito provável que a mais antiga tradução árabe da Bíblia date do século VIII.

O orientalista Thomas Patrick Hughes escreveu:

> Não há provas de que Muhammad tenha lido as Escrituras Cristãs... É de notar que não há provas claras que sugiram a existência de qualquer tradução árabe do Antigo e do Novo Testamento antes do tempo de Muhammad.

Em terceiro lugar, a ideia de que o Profeta obteve a revelação das pessoas é uma ideia antiga. Os politeístas acusaram-no disso e o Alcorão refutou-os, como Allah, exaltado seja, diz:

{{ $page->verse('25:5-6') }}

Além disso, a afirmação de que o Profeta Muhammad {{ $page->pbuh() }} adquiriu conhecimentos do Povo do Livro quando esteve em Meca é uma afirmação que é refutada pelo estatuto académico e cultural de Meca e pela verdadeira natureza da ligação que esta sociedade árabe tinha com os conhecimentos do Povo do Livro.

Por conseguinte, depois de provar que o Profeta Muhammad {{ $page->pbuh() }} não aprendeu o conhecimento do Povo do Livro diretamente ou através de um intermediário, não nos resta outra opção senão afirmar que foi uma revelação de Allah ao Seu Profeta escolhido.
