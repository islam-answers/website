---
extends: _layouts.answer
section: content
title: ¿Por qué se casó el profeta Muhammad con una joven?
date: 2025-02-08
description: El matrimonio del Profeta Muhammad con Aisha reflejaba las normas del
  siglo VII, y la crítica moderna aplica las normas actuales a las practicas históricas.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 573109
---

Aisha (que Allah esté complacido con ella) tenía 6 años cuando se casó con el Profeta Muhammad {{ $page->pbuh() }} y tenía 9 cuando fue consumado el matrimonio.

Lamentablemente, en la actualidad vemos cómo los esfuerzos de otras religiones se centran principalmente en tergiversar y manipular hechos históricos y etimológicos. Uno de estos temas ha sido la acusación del joven matrimonio de Aisha con el Profeta Muhammad {{ $page->pbuh() }}. Los misioneros intentan acusar al Profeta de ser un pederasta, aunque en términos políticamente correctos, debido al hecho de que Aisha fue prometida a la edad de 6 años y el matrimonio se consumó 3 años más tarde -a los 9 años- cuando ella estaba en plena pubertad. El lapso de tiempo transcurrido entre los esponsales y la consumación del matrimonio de Aisha demuestra claramente que sus padres esperaban a que alcanzara la pubertad antes de consumar su matrimonio. Esta respuesta pretende refutar la injusta acusación contra el Profeta Muhammad {{ $page->pbuh() }}.

### Pubertad y matrimonio joven en la cultura semítica

La acusación de pederastia contradice el hecho básico de que una niña se convierte en mujer cuando comienza su ciclo menstrual. Cualquier persona mínimamente familiarizada con la fisiología puede afirmar que la menstruación es una señal de que la niña se está preparando para ser madre.

Las mujeres alcanzan la pubertad a edades diferentes, entre los 8 y los 12 años, dependiendo de la genética, la raza y el entorno. Hay poca diferencia en la talla de niños y niñas hasta los diez años, el estirón de la pubertad empieza antes en las niñas pero dura más en los niños. Los primeros signos de la pubertad aparecen en torno a los 9 o 10 años en las niñas, pero más cerca de los 12 en los niños. Además, las mujeres de entornos más cálidos alcanzan la pubertad a una edad mucho más temprana que las de entornos fríos. La temperatura media del país o la provincia se considera el factor más importante en este caso, no sólo en lo que respecta a la menstruación, sino a todo el desarrollo sexual en la pubertad.

El matrimonio en los primeros años de la pubertad era aceptable en la Arabia del siglo VII, como lo era la norma social en todas las culturas semíticas, desde los israelitas hasta los árabes y todas las naciones intermedias. Según el Talmud, que los judíos consideran su "Torá oral", el Sanedrín 76b afirma claramente que es preferible que una mujer se case cuando tenga su primera menstruación, y en Ketuvot 6a hay normas relativas a las relaciones sexuales con chicas que aún no han menstruado. Jim West, ministro baptista, observa la siguiente tradición de los israelitas:

- Para mantener la pureza de la línea familiar, la esposa debía proceder del círculo familiar más amplio (normalmente al inicio de la pubertad o en torno a los 13 años).
- La pubertad siempre ha sido un símbolo de la edad adulta a lo largo de la historia.
- La pubertad se define como la edad o periodo en el que una persona es capaz de reproducirse sexualmente por primera vez; en otras épocas de la historia, un rito o celebración de este hito formaba parte de la cultura.

Los reputados sexólogos R.E.L. Masters y Allan Edwards, en su estudio sobre la expresión sexual afroasiática, afirman lo siguiente:

> Hoy en día, en muchas partes del norte de África y Oriente Medio, las niñas se casan y se acuestan entre los cinco y los nueve años; y ninguna mujer que se precie permanece soltera más allá de la pubertad.

### Razones para casarse con Aisha

En cuanto a la historia de su matrimonio, el Profeta {{ $page->pbuh() }} se había afligido por la muerte de su primera esposa, Khadeejah, que le había apoyado y permanecido a su lado, y llamó al año en que murió El Año del Dolor. Entonces se casó con Sawdah, que era una mujer mayor y no era muy hermosa; más bien se casó con ella para consolarla tras la muerte de su marido. Cuatro años después, el Profeta {{ $page->pbuh() }} se casó con Aisha. Los motivos del matrimonio fueron los siguientes:

1. Soñó con casarse con ella. Está probado por la narración de Aisha que el Profeta {{ $page->pbuh() }} le dijo:

{{ $page->hadith('bukhari:3895') }}

2. Las características de inteligencia e inteligencia que el Profeta {{ $page->pbuh() }} había notado en Aisha incluso cuando era pequeña, por lo que quiso casarse con ella para que fuera más capaz que los demás de transmitir informes de lo que él hacía y decía. De hecho, como ya se ha dicho, ella era un punto de referencia para los compañeros del Profeta {{ $page->pbuh() }} en lo que respecta a sus asuntos y dictámenes.

3. El amor del Profeta {{ $page->pbuh() }} por su padre Abu Bakr, y la persecución que Abu Bakr había sufrido por causa de la llamada al Islam, que soportó con paciencia. Fue la persona más fuerte y sincera en la fe después de los Profetas.

### ¿Hubo alguna objeción a su matrimonio?

La respuesta es no. No hay absolutamente ningún registro musulmán, secular o de cualquier otra fuente histórica que muestre implícitamente otra cosa que no sea la alegría absoluta de todas las partes implicadas en este matrimonio. Nabia Abbott describe el matrimonio de Aisha con el Profeta {{ $page->pbuh() }} de la siguiente manera:

> En ninguna versión se comenta la disparidad de edades entre Muhammad y Aishah ni la tierna edad de la novia que, como mucho, no podía tener más de diez años y que aún estaba muy enamorada de su juego.

Incluso el conocido orientalista crítico, W. Montgomery Watt, dijo lo siguiente sobre el carácter moral del Profeta {{ $page->pbuh() }}:

> Desde el punto de vista de la época de Muhammad, pues, no se pueden mantener las acusaciones de traición y sensualidad. Sus contemporáneos no lo consideraron moralmente defectuoso en ningún aspecto. Al contrario, algunos de los actos criticados por el occidental moderno demuestran que los estándares de Muhammad eran más elevados que los de su época.

Aparte de que nadie se disgustó con él ni con sus actos, fue un ejemplo supremo de carácter moral en su sociedad y en su época. Por lo tanto, juzgar su moralidad basándose en las normas de nuestra sociedad y cultura actuales no sólo es absurdo, sino también injusto.

### El matrimonio en la pubertad hoy

Los contemporáneos del Profeta (tanto enemigos como amigos) aceptaron claramente el matrimonio del Profeta {{ $page->pbuh() }} con Aisha sin ningún problema. La prueba de ello es la ausencia de críticas contra este matrimonio hasta los tiempos modernos.

Lo que ha provocado esto es un cambio en la cultura de nuestros tiempos en lo que respecta a la edad a la que la gente suele casarse. Pero incluso hoy, en pleno siglo XXI, la edad de consentimiento sexual sigue siendo bastante baja en muchos lugares. En Alemania, Italia y Austria, las personas pueden mantener relaciones sexuales legalmente a los 14 años, y en Filipinas y Angola pueden hacerlo a los 12 años. California fue el primer estado en cambiar la edad de consentimiento a los 14 años, lo que hizo en 1889. Después de California, otros estados se sumaron y elevaron también la edad de consentimiento.

### El Islam y la edad de la pubertad

El Islam enseña claramente que la edad adulta comienza cuando una persona ha alcanzado la pubertad. Alá dice en el Corán:

{{ $page->verse('24:59..') }}

Las mujeres alcanzan la pubertad con el comienzo de la menstruación, como dice Alá en el Corán:

{{ $page->verse('65:4..') }}

Así, forma parte del Islam reconocer la llegada de la pubertad como el inicio de la edad adulta. Es el momento en que la persona ya ha madurado y está preparada para las responsabilidades de un adulto. Entonces, ¿en qué se basa alguien para criticar el matrimonio de Aisha, ya que su matrimonio se consumó después de que alcanzara la pubertad?

Si la acusación de "pederastia" se dirigiera contra el Profeta {{ $page->pbuh() }}, también habría que incluir a todos los pueblos semitas que aceptaron el matrimonio en la pubertad como norma.

Además, sabiendo que el Profeta {{ $page->pbuh() }} no se casó con ninguna virgen aparte de Aisha y que todas sus otras esposas habían estado casadas previamente (y muchas de ellas eran ancianas), esto refutará la noción difundida por muchas fuentes hostiles, de que el motivo básico detrás de los matrimonios del Profeta era el deseo físico y el disfrute de las mujeres, porque si esa fuera su intención habría elegido sólo a las que eran jóvenes.

### Conclusiones

Así lo hemos visto:

- Era norma de la sociedad semita en la Arabia del siglo VII permitir los matrimonios entre púberes.
- No hay noticias de oposición al matrimonio del Profeta con Aisha, ni por parte de sus amigos ni de sus enemigos.
- Incluso hoy en día, hay culturas que siguen permitiendo el matrimonio entre púberes.

A pesar de enfrentarse a estos hechos bien conocidos, algunas personas siguen acusando al Profeta {{ $page->pbuh() }} de inmoralidad. Sin embargo, fue él quien hizo justicia a las mujeres de Arabia y las elevó a un nivel nunca visto en su sociedad, algo que las civilizaciones antiguas nunca habían hecho con sus mujeres.

Por ejemplo, cuando se convirtió en Profeta, los paganos de Arabia habían heredado un desprecio por las mujeres que se había transmitido entre sus vecinos judíos y cristianos. Tan vergonzoso era para ellos ser bendecidos con una niña, que llegaban incluso a enterrarla viva para evitar la deshonra asociada a las niñas. Alá dice en el Corán:

{{ $page->verse('16:58-59') }}

Muhammad {{ $page->pbuh() }} no sólo desaconsejaba y condenaba severamente este acto, sino que también solía enseñar a la gente a respetar y apreciar a sus hijas y madres como compañeras y fuentes de salvación para los hombres de su familia. Dijo:

{{ $page->hadith('ibnmajah:3669') }}

También se narró en un hadiz de Anas Ibn Malik que:

{{ $page->hadith('muslim:2631') }}

En otras palabras, si uno ama al Mensajero de Allah {{ $page->pbuh() }} y desea estar con él el Día de la Resurrección en el Cielo, entonces debe ser bueno con sus hijas. Esto no es ciertamente el acto de un "pederasta", como algunos quieren hacernos creer.
