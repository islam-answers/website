---
extends: _layouts.answer
section: content
title: Worüber spricht der Quran?
date: 2024-03-24
description: Die Hauptquelle für den Glauben und den Gottesdienst eines jeden Muslims.
sources:
- href: https://www.islam-guide.com/de/ch3-7.htm
  title: islam-guide.com
---

Der Quran, das letzte offenbarte Wort Gottes, ist die Hauptquelle für den Glauben und den Gottesdienst eines jeden Muslims.Er befasst sich mit allen Themen, die die Menschen betreffen: Weisheit, Lehre, Gottesdienst, Geschäfte, Gesetze usw., aber das Hauptthema ist die Verbindung zwischen Gott und seinen Geschöpfen. Zur gleichen Zeit stellt er Richtlinien und detaillierte Lehren für eine gerechte Gesellschaft, gutes menschliches Benehmen und ein gerechtes Wirtschaftssystem.

Beachte: der Quran wurde Muhammad {{ $page->pbuh() }} lediglich in arabischer Sprache offenbart, jegliche Übersetzung, sei es auf deutsch oder irgendeine andere Sprache, ist weder der Quran noch eine Version des Quran, sondern der Versuch einer Übersetzung der Bedeutung des Quran. Der Quran existiert nur auf arabisch, wie er offenbart wurde.
