---
extends: _layouts.answer
section: content
title: Co islam mówi o traktowaniu rodziców?
date: 2024-10-10
description: Okazywanie życzliwości rodzicom jest jednym z najwyżej nagradzanych czynów
  w oczach Boga. Nakaz bycia dobrym dla rodziców jest jasnym nauczaniem Koranu.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 583847
---

Okazywanie życzliwości rodzicom jest jednym z najwyżej nagradzanych czynów w oczach Allaha. Nakaz bycia dobrym dla swoich rodziców jest jasnym nauczaniem Koranu. Allah mówi:

{{ $page->verse('4:36..') }}

Wzmianka o posłuszeństwie wobec rodziców następuje bezpośrednio po posłuszeństwie wobec Boga. Powtarza się to w całym Koranie.

{{ $page->verse('17:23') }}

W tym wersecie Bóg nakazuje nam czcić tylko Jego i nikogo innego, a następnie natychmiast zwraca uwagę na dobre zachowanie wobec rodziców, zwłaszcza gdy się starzeją i są zależni od innych w kwestii schronienia i swoich potrzeb. Muzułmanów uczy się, aby nie byli szorstcy wobec rodziców ani ich nie karcili, ale raczej mówili do nich miłymi słowami i okazywali miłość i czułość.

W Koranie Allah dalej prosi człowieka, aby modlił się do Niego za swoich rodziców:

{{ $page->verse('17:24') }}

Allah podkreśla, że posłuszeństwo wobec rodziców jest posłuszeństwem wobec Niego, z wyjątkiem przypisywania Mu współtowarzyszy.

{{ $page->verse('31:14-15..') }}

Werset ten podkreśla, że wszyscy rodzice zasługują na dobre traktowanie. Szczególną troską należy otoczyć matki ze względu na dodatkowe trudności, jakie znoszą.

Opieka nad rodzicami jest uważana za jeden z najlepszych uczynków:

{{ $page->hadith('muslim:85e') }}

Podsumowując, w islamie czcić rodziców oznacza być im posłusznym, okazywać im szacunek, modlić się za nich, mówić cicho w ich obecności, uśmiechać się do nich, nie okazywać im niezadowolenia, starać się im służyć, spełniać ich życzenia, konsultować się z nimi i słuchać tego, co mają do powiedzenia.
