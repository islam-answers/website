---
extends: _layouts.answer
section: content
title: Que signifie Mahram dans l'islam ?
date: 2024-12-12
description: Un Mahram est une personne avec laquelle le mariage est définitivement
  interdit, comme des proches parents, des parents adoptifs ou des beaux-parents.
sources:
- href: https://islamqa.org/hanafi/daruliftaa/8495/
  title: islamqa.org (Daruliftaa.com)
- href: https://islamqa.info/en/answers/5538/
  title: islamqa.info
- href: https://islamqa.info/en/answers/130002/
  title: islamqa.info
translator_id: 586055
---

En règle générale, un mahram est une personne avec laquelle le mariage est définitivement interdit. C’est la raison pour laquelle le mot « mahram » est traduit en français par « parent non mariable ». Il est permis à une femme d’enlever son hijab devant ses mahram. Il est également permis de serrer la main d’un mahram ou de l’embrasser sur la tête, le nez ou la joue.

Cette (interdiction permanente du mariage) est établie de trois manières : par la parenté (liens de sang), par la parenté d'accueil (allaitement) et par la parenté par le mariage.

Ainsi, l’illicéité permanente du mariage est établie par les trois types de relations mentionnés ci-dessus, et un Mahram est celui avec qui le mariage est définitivement illicite. En d’autres termes, une personne devient un Mahram en raison de ces trois types de relations.

## Lien de parenté/lignée

Il est définitivement interdit à un homme d’épouser les personnes suivantes (il sera donc considéré comme un Mahram pour elles) :

- Mère, grand-mère maternelle et ainsi de suite ;
- Grand-mère paternelle et ainsi de suite ;
- Filles, petites-filles et ainsi de suite ;
- Tous les types de sœurs (qu'elles soient de même mère et père, demi-sœurs, ou sœur de lait),
- Tantes maternelles et paternelles,
- Nièces (filles du frère ou de la sœur),

Allah dit dans le Coran :

{{ $page->verse('4:23') }}

De même, les Mahrams d'une femme selon les liens de parenté sont :

- Père, grand-père paternel, et ainsi de suite ;
- Grand-père maternel, et ainsi de suite ;
- Fils, petits-fils, et ainsi de suite ;
- Tous les types de frères (qu'ils soient de même mère et père, demi-frères ou frère de lait),
- Oncles maternels et paternels,
- Neveux (fils d'un frère ou d'une sœur),

Allah dit dans le Coran :

{{ $page->verse('..24:31..') }}

## Relation de fosterage (famille d'accueil)

Quiconque est Mahram par lien de parenté sera également considéré comme Mahram par fosterage (lien d'adoption). Ainsi, le père adoptif (le mari de la mère adoptive), le frère adoptif, l'oncle adoptif, le neveu adoptif, etc. seront tous considérés comme le Mahram d'une femme. Et un fils adoptif sera le Mahram de sa mère adoptive, sa sœur adoptive, sa nièce adoptive, etc.

Toutefois, il convient de rappeler que cette règle ne s'applique que lorsque l'enfant adoptif a été allaité pendant la période prévue à cet effet, c'est-à-dire deux ans. Il faut être prudent lorsqu'il s'agit de déterminer qui est un mahram par l'intermédiaire d'une famille d'accueil, car cela peut parfois être complexe et compliqué. Il faut se référer à un savant avant d'émettre un jugement.

## Relation de mariage

La troisième relation avec laquelle le mariage devient définitivement illicite et, par conséquent, la relation de mahram est établie, est celle du mariage.

Il existe quatre types de personnes avec lesquelles le mariage devient définitivement illégal en raison de la relation conjugale :

- La mère de l'épouse (belle-mère), la grand-mère et ainsi de suite... : Le mariage avec elle devient illicite par le simple fait de contracter un mariage avec la fille, que le mariage ait été consommé ou non.
- La fille de l'épouse (issue d'un précédent mariage), la petite-fille et les suivantes : Le mariage avec elle devient illicite (définitivement) si le mariage avec sa mère a été consommé.
- La femme de son fils, de son petit-fils, etc : Que le fils ait consommé le mariage ou non.
- La belle-mère, la grand-mère par alliance et ainsi de suite : Il s'agit des femmes qui ont été mariées au père ou au grand-père paternel ou maternel de l'intéressé.

En résumé, un Mahram est celui avec qui le mariage est définitivement illicite, et cette illégalité/interdiction permanente du mariage est établie de trois manières : la relation de lignage, la relation par l’adoption et la relation par le mariage.
