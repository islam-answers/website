<?php

namespace App\Commands;

use App\Helpers\GitHelper;
use App\Helpers\JigsawHelper;
use App\Helpers\LokaliseHelper;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[
    AsCommand(
        name: "tasks:auto-assign",
        description: "Auto-assign tasks to contributors"
    )
]
class AutoAssignTasksCommand extends Command
{
    private LokaliseHelper $lokalise;
    private GitHelper $git;
    private JigsawHelper $jigsaw;
    private array $languageGroups;
    private array $conditions;

    public function __construct()
    {
        $this->lokalise = new LokaliseHelper();
        $this->git = new GitHelper();
        $this->jigsaw = new JigsawHelper();
        $this->languageGroups = require __DIR__ .
            "/../config/language-groups.php";
        $this->conditions = require __DIR__ .
            "/../config/auto-assign-conditions.php";

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption("dry-run", "d", InputOption::VALUE_NONE, "Dry run")
            ->addOption(
                "target-open-tasks",
                "o",
                InputOption::VALUE_REQUIRED,
                "Add tasks to contributors so that they have X number of tasks open",
                2
            )
            ->addOption(
                "days-threshold",
                "t",
                InputOption::VALUE_REQUIRED,
                "Assign only if last assigned X number of days ago",
                7
            );
    }

    protected function getParagraphsCount(): Collection
    {
        return $this->jigsaw
            ->getPublishedAnswers()
            ->filter(
                fn(Collection $answers, string $language) => $language === "en"
            )
            ->flatMap(function (Collection $answers) {
                return $answers->map(function (string $slug) {
                    $content = $this->jigsaw->getContent($slug, "en");
                    $markdown = $this->jigsaw->getMarkdown($content);
                    $paragraphs = $this->jigsaw->getParagraphsInAnswer(
                        $markdown
                    );

                    return [
                        "slug" => $slug,
                        "paragraphs" => $paragraphs->count(),
                    ];
                });
            });
    }

    protected function getAnswersToTranslate(Collection $tasks): Collection
    {
        // Find english answers that are not published in other languages
        $languages = $this->jigsaw->getPublishedLanguages();
        $answers = $this->jigsaw->getPublishedAnswers();
        $englishAnswers = $answers["en"];
        // Make sure answers has keys for all languages (even if empty)
        $answers = $answers->union(
            $languages->mapWithKeys(fn($language) => [$language => collect()])
        );
        $missingAnswers = $answers
            // diff slugs against english slugs
            ->map(
                fn(Collection $answers, string $language) => collect(
                    array_diff($englishAnswers->toArray(), $answers->toArray())
                )->values()
            );

        return $missingAnswers
            // Reject the answers that are already assigned to a task
            ->map(function (Collection $answers, string $language) use (
                $tasks
            ) {
                $assignedAnswers = $tasks
                    ->filter(fn(array $task) => $task["language"] === $language)
                    ->map(fn(array $task) => $task["slug"]);

                return $answers
                    ->reject(
                        fn(string $slug) => $assignedAnswers->contains($slug)
                    )
                    ->values();
            })
            // Apply conditions
            ->map(function (Collection $answers, string $language) {
                return $answers
                    ->filter(function (string $slug) use ($language) {
                        if (!isset($this->conditions[$slug])) {
                            return true;
                        }

                        $condition = $this->conditions[$slug];
                        $allowedLanguages = collect(
                            $condition["languages"] ?? []
                        );
                        $allowedGroups = collect(
                            $condition["groups"] ?? []
                        )->flatMap(
                            fn(string $group) => $this->languageGroups[
                                $group
                            ] ?? []
                        );

                        return $allowedLanguages->contains($language) ||
                            $allowedGroups->contains($language);
                    })
                    ->values();
            });
    }

    protected function getTasks(): Collection
    {
        return $this->lokalise
            ->getTasks(["limit" => 1000])
            ->filter(fn(array $task) => $task["task_type"] === "translation")
            ->filter(fn(array $task) => !empty($task["languages"][0]["users"]))
            ->map(
                fn(array $task) => [
                    "id" => $task["task_id"],
                    "title" => $task["title"],
                    "slug" => Str::slug($task["title"]),
                    "status" => $task["status"],
                    "keys_count" => $task["keys_count"],
                    "words_count" => $task["words_count"],
                    "created_at_timestamp" => $task["created_at_timestamp"],
                    "language" => $task["languages"][0]["language_iso"],
                    "assignee_user_id" =>
                        $task["languages"][0]["users"][0]["user_id"],
                    "completed_at_timestamp" =>
                        $task["languages"][0]["completed_at_timestamp"],
                ]
            )
            ->values();
    }

    protected function getContributors(): Collection
    {
        return $this->lokalise
            ->getContributors()
            ->filter(fn(array $contributor) => !$contributor["is_admin"])
            ->map(
                fn(array $contributor) => [
                    "id" => $contributor["user_id"],
                    "name" => $contributor["fullname"],
                    "email" => $contributor["email"],
                    "languages" => collect($contributor["languages"])
                        ->filter(
                            fn(array $language) => $language["is_writable"]
                        )
                        ->map(fn(array $language) => $language["lang_iso"])
                        ->values(),
                ]
            )
            ->values();
    }

    protected function addTasksInfoToContributors(
        Collection $contributors,
        Collection $tasks
    ): Collection {
        return $contributors
            // Add tasks
            ->map(function (array $contributor) use ($tasks) {
                $contributor["tasks"] = $tasks
                    ->filter(
                        fn(array $task) => $task["assignee_user_id"] ===
                            $contributor["id"]
                    )
                    ->values();

                return $contributor;
            })
            // Add tasks statistics
            ->map(function (array $contributor) {
                $contributor["tasks_count"] = $contributor["tasks"]->count();
                $contributor["open_tasks_count"] = $contributor["tasks"]
                    ->filter(fn(array $task) => $task["status"] !== "completed")
                    ->count();
                $contributor["completed_tasks_count"] = $contributor["tasks"]
                    ->filter(fn(array $task) => $task["status"] === "completed")
                    ->count();
                $contributor["last_assigned_timestamp"] = $contributor[
                    "tasks"
                ]->isNotEmpty()
                    ? $contributor["tasks"]->max(
                        fn(array $task) => $task["created_at_timestamp"]
                    )
                    : null;

                return $contributor;
            })
            ->values();
    }

    protected function pickTodosForContributor(
        array $contributor,
        Collection $todo, // collection of lang => [slug1, slug2, ...]
        Collection $paragraphs, // collection of [slug, paragraphCount]
        int $openTasksThreshold
    ): Collection {
        $numberOfTasks = $openTasksThreshold - $contributor["open_tasks_count"];
        $completedTasks = $contributor["completed_tasks_count"];
        $languages = $contributor["languages"];

        return $todo
            // Filter to only the languages the contributor can contribute to
            ->filter(
                fn(Collection $slugs, string $language) => $languages->contains(
                    $language
                )
            )
            // Flatten while keeping the language
            ->flatMap(
                fn(Collection $slugs, string $language) => $slugs->map(
                    fn(string $slug) => [
                        "slug" => $slug,
                        "language" => $language,
                    ]
                )
            )
            // Add paragraph counts
            ->map(
                fn(array $todo) => [
                    "slug" => $todo["slug"],
                    "language" => $todo["language"],
                    "paragraphs" => $paragraphs->firstWhere(
                        "slug",
                        $todo["slug"]
                    )["paragraphs"],
                ]
            )
            // Sort by paragraph count
            ->sortBy("paragraphs")
            ->when(
                $completedTasks >= 6,
                fn(Collection $todo) => $todo->shuffle()
            )
            ->take($numberOfTasks)
            ->values();
    }

    /** not needed anymore, but i'm keeping it for now **/
    protected function getOpenBranches(): Collection
    {
        return collect($this->git->fetch()->getBranches())
            ->map(
                fn(string $branch) => [
                    "slug" => Str::beforeLast($branch, "-"),
                    "language" => Str::afterLast($branch, "-"),
                ]
            )
            ->reject(
                fn(array $branch) => !preg_match(
                    '/^[a-z]{2}$/',
                    $branch["language"]
                )
            )
            ->values();
    }

    protected function generatePushCommand(
        int $contributorId,
        int $reviewerId,
        string $slug,
        string $language
    ): string {
        return sprintf(
            "php go lokalise:push --slug %s --lang %s --contributor %d --reviewer %d --non-interactive",
            $slug,
            $language,
            $contributorId,
            $reviewerId
        );
    }

    protected function getReviewersMap(): Collection
    {
        $team = $this->lokalise->getTeams()[0];
        $userGroups = $this->lokalise
            ->getUserGroups($team["team_id"])
            ->filter(
                fn(array $userGroup) => str_starts_with(
                    $userGroup["name"],
                    "Team"
                )
            )
            ->map(
                fn(array $userGroup) => [
                    "name" => $userGroup["name"],
                    "members" => $userGroup["members"],
                ]
            )
            ->values();

        return $this->lokalise
            ->getTeamUsers($team["team_id"])
            ->filter(fn(array $user) => $user["role"] == "owner")
            ->map(
                fn(array $user) => [
                    "reviewer_id" => $user["user_id"],
                    "contributor_ids" => $userGroups
                        ->filter(
                            fn(array $userGroup) => in_array(
                                $user["user_id"],
                                $userGroup["members"]
                            )
                        )
                        ->map(fn(array $userGroup) => $userGroup["members"])
                        ->flatten()
                        ->reject(fn(int $id) => $id == $user["user_id"])
                        ->values(),
                ]
            )
            ->values();
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $dryRun = $input->getOption("dry-run");
        $targetOpenTasks = $input->getOption("target-open-tasks");
        $daysThreshold = $input->getOption("days-threshold");

        $tasks = $this->getTasks(); // collection of [id, title, slug, status, keys_count, words_count, created_at_timestamp, language, assignee_user_id, completed_at_timestamp]
        $contributors = $this->getContributors(); // collection of [id, name, email, languages]
        $reviewersMap = $this->getReviewersMap(); // collection of [reviewer_id, contributor_ids]

        $paragraphs = $this->getParagraphsCount(); // collection of [slug, paragraphCount]
        $todo = $this->getAnswersToTranslate($tasks); // collection of lang => [slug1, slug2, ...]

        $contributors = $this->addTasksInfoToContributors(
            $contributors,
            $tasks,
            $targetOpenTasks,
            $daysThreshold
        ) // adds [tasks, tasks_count, open_tasks_count, completed_tasks_count, last_assigned_timestamp]
            // Find the contributors who should be assigned new tasks
            ->filter(
                fn(array $contributor) => $contributor["open_tasks_count"] <
                    $targetOpenTasks
            )
            ->filter(
                fn(array $contributor) => $contributor[
                    "last_assigned_timestamp"
                ] === null ||
                    Carbon::createFromTimestamp(
                        $contributor["last_assigned_timestamp"]
                    )->diffInDays(Carbon::now()) >= $daysThreshold
            )
            ->values();

        $toAssign = $contributors->mapWithKeys(
            fn(array $contributor) => [
                $contributor["id"] => $this->pickTodosForContributor(
                    $contributor,
                    $todo,
                    $paragraphs,
                    $targetOpenTasks
                ),
            ]
        );

        $commands = $toAssign
            ->filter(
                fn(
                    Collection $todos,
                    int $contributorId
                ) => $reviewersMap->first(
                    fn(array $reviewer) => $reviewer[
                        "contributor_ids"
                    ]->contains($contributorId)
                ) !== null
            )
            ->map(
                fn(Collection $todos, int $contributorId) => $todos->map(
                    fn(array $todo) => $this->generatePushCommand(
                        $contributorId,
                        $reviewersMap->first(
                            fn(array $reviewer) => $reviewer[
                                "contributor_ids"
                            ]->contains($contributorId)
                        )["reviewer_id"],
                        $todo["slug"],
                        $todo["language"]
                    )
                )
            )
            ->flatten();

        $commands->each(function (string $command) use ($output, $dryRun) {
            $output->writeln("Executing: $command");

            if (!$dryRun) {
                $output->writeln(shell_exec($command));
                $output->writeln("");
            }
        });

        return Command::SUCCESS;
    }
}
