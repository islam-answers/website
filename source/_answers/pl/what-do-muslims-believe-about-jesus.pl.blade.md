---
extends: _layouts.answer
section: content
title: W co wierzą muzułmanie na temat Jezusa?
date: 2024-09-20
description: Muzułmanie szanują i czczą Jezusa. Uważają go za jednego z największych
  posłańców Boga dla ludzkości.
sources:
- href: https://www.islam-guide.com/ch3-10.htm
  title: islam-guide.com
translator_id: 584496
---

Muzułmanie szanują i czczą Jezusa (niech pokój będzie z nim). Uważają go za jednego z największych posłańców Boga dla ludzkości. Koran potwierdza jego dziewicze narodziny, a rozdział Koranu nosi tytuł „Maryam” (Maryja). Koran opisuje narodziny Jezusa w następujący sposób:

{{ $page->verse('3:45-47') }}

Jezus narodził się w cudowny sposób na polecenie Boga, to samo polecenie, które powołało Adama do życia bez ojca i matki. Bóg powiedział:

{{ $page->verse('3:59') }}

Podczas swojej proroczej misji Jezus dokonał wielu cudów. Bóg mówi nam, że Jezus powiedział:

{{ $page->verse('3:49..') }}

Muzułmanie wierzą, że Jezus nie został ukrzyżowany. Wrogowie Jezusa planowali go ukrzyżować, ale Bóg go ocalił i wskrzesił. Podobizna Jezusa została umieszczona nad innym człowiekiem. Wrogowie Jezusa wzięli tego człowieka i ukrzyżowali go, myśląc, że to Jezus. Bóg rzekł:

{{ $page->verse('..4:157..') }}

Ani prorok Muhammad {{ $page->pbuh() }}, ani Jezus nie przyszli, aby zmienić podstawową doktrynę wiary w jednego Boga, przyniesioną przez wcześniejszych proroków, ale raczej, aby ją potwierdzić i odnowić.
