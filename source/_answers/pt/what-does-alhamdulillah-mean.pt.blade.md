---
extends: _layouts.answer
section: content
title: O que significa Alhamdulillah?
date: 2025-02-05
description: Alhamdulillah significa "Todo agradecimento é exclusivamente devido a
  Allah, somente, não a ídolos adorados em seu lugar, nem a qualquer parte de Sua
  criação."
sources:
- href: https://www.islamweb.net/en/fatwa/82878
  title: islamweb.net
- href: https://islamqa.info/en/answers/125984
  title: islamqa.info
translator_id: 594172
---

De acordo com Muhammad ibn Jarir at-Tabari, o significado de "Alhamdulillah" é: "Todos os agradecimentos são devidos exclusivamente a Allah, não a nenhum dos objectos que são adorados em vez d'Ele, nem a nenhuma das Suas criações. Estes agradecimentos são devidos a Allah pelos seus inúmeros favores e recompensas, cujo número só Ele conhece. As mercês de Allah incluem os instrumentos que ajudam as criaturas a adorá-Lo, os corpos físicos com os quais são capazes de implementar as Suas ordens, o sustento que Ele lhes proporciona nesta vida e a vida confortável que lhes concedeu, sem que nada nem ninguém O obrigue a isso. Deus também avisou as Suas criaturas e alertou-as para os meios e métodos com que podem ganhar a morada eterna na residência da felicidade eterna. Todos os agradecimentos e louvores são devidos a Allah por estes favores do princípio ao fim."

Quem mais merece o agradecimento e o louvor das pessoas é Allah, glorificado e exaltado seja, devido aos grandes favores e bênçãos que concedeu aos Seus servos, tanto em termos espirituais como mundanos. Allah ordenou-nos que Lhe déssemos graças por essas bênçãos e que não as negássemos. Ele diz:

{{ $page->verse('2:152') }}

E há muitas outras bênçãos. Apenas mencionámos aqui algumas dessas bênçãos; enumerá-las todas é impossível, como diz Allah:

{{ $page->verse('14:34') }}

Então, Deus abençoou-nos e perdoou-nos as nossas faltas de agradecimento por essas bênçãos. Ele diz:

{{ $page->verse('16:18') }}

A gratidão pelas bênçãos é uma causa do aumento das mesmas, como diz Allah:

{{ $page->verse('14:7') }}

Anas ibn Malik disse: O Mensageiro de Allah {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2734a') }}
