---
extends: _layouts.answer
section: content
title: A circuncisão é necessária para a conversão ao Islam?
date: 2025-02-05
description: A circuncisão não é necessária para converter-se ao Islam, pois a validade
  do Islam de uma pessoa não depende de ela ser circuncidada.
sources:
- href: https://islamqa.info/en/answers/4/
  title: islamqa.info
translator_id: 594172
---

A questão da circuncisão é uma das questões que, em muitos casos, constitui uma barreira para alguns daqueles que querem entrar na religião do Islam. O assunto é mais fácil do que muitas pessoas pensam.

### A circuncisão masculina no Islam

Quanto à circuncisão, é um dos símbolos do Islam. Faz parte da natureza humana sã (fitrah), e faz parte do caminho de Ibrahim {{ $page->pbuh() }}. Deus, exaltado seja, diz:

{{ $page->verse('16:123..') }}

O Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2370') }}

### A circuncisão masculina é obrigatória no Islam?

A circuncisão é obrigatória para um homem muçulmano, se ele for capaz de a fazer. Se não o puder fazer, por exemplo, se temer morrer se for circuncidado, ou se um médico de confiança lhe disser que a circuncisão provocará uma hemorragia que pode causar a sua morte, então, nesse caso, está isento da circuncisão e não está a pecar se não a fizer.

### A circuncisão é necessária para a conversão ao Islam?

Não é apropriado, em circunstância alguma, deixar que a questão da circuncisão seja uma barreira que impeça uma pessoa de entrar na fé; pelo contrário, a solidez do Islam de uma pessoa não depende de ela ser circuncidada. A entrada de uma pessoa na religião do Islam é válida mesmo que não seja circuncidada.
