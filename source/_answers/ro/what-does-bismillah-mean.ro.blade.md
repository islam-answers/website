---
extends: _layouts.answer
section: content
title: Ce înseamnă Bismillah?
date: 2024-08-28
description: Bismillah înseamnă "în numele lui Allah". Musulmanii spun Bismillah înainte
  de a începe o acțiune cu numele lui Allah, căutând ajutorul și binecuvântările Sale.
sources:
- href: https://islamqa.info/en/answers/21722/
  title: islamqa.info
- href: https://islamqa.info/en/answers/163573/
  title: islamqa.info
translator_id: 554943
---

Bismillah înseamnă "În numele lui Allah". Forma completă este "Bismillah al-Rahman al-Rahim", care se traduce prin "În numele lui Allah, Cel Milostiv, Îndurător".

Atunci când cineva spune "Bismillah" înainte de a începe să facă ceva, acest lucru înseamnă: "Încep această acțiune însoțită de numele lui Allah sau căutând ajutor prin numele lui Allah, căutând astfel binecuvântare. Allah este Dumnezeu, Cel iubit și adorat, către Care inimile se îndreaptă cu dragoste, venerație și supunere (adorare). El este al-Rahman (Cel Milostiv), al Cărui atribut este vasta milă; și al-Rahim (Cel Îndurător), Care face ca această milă să ajungă la creația Sa.

Ibn Jarir (Allah să aibă milă de el) a spus:

> Allah, fie ca Numele Său să fie înălțat și sfințit, l-a învățat pe Profetul Său Mohammed {{ $page->pbuh() }} bunele maniere, învățându-l să Îi menționeze cele mai frumoase nume înainte de toate acțiunile sale. El i-a poruncit să menționeze aceste atribute înainte de a începe să facă ceva și a creat din aceste învățături o cale de urmat pentru toți oamenii înainte de a începe ceva, cuvinte care să fie scrise la începutul scrisorilor și cărților lor. Sensul aparent al acestor cuvinte indică exact ce se înțelege prin ele și nu este nevoie să fie explicat.

Musulmanii spun "Bismillah" înainte de a mânca, pe baza raportului relatat de `A'ishah (Allah să fie mulțumit de ea), că Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('ibnmajah:3264') }}

Există ceva omis în fraza "Bismillah" atunci când este rostită înainte de a începe să faci ceva. "Îmi încep acțiunea în numele lui Allah" poate înseamna, de exemplu: "În numele lui Allah citesc", "În numele lui Allah scriu", "În numele lui Allah călăresc" și așa mai departe. Sau: "Pornirea mea este în numele lui Allah", "Călăritul meu este în numele lui Allah", "Cititul meu este în numele lui Allah" și așa mai departe. Este posibil ca binecuvântarea să vină prin rostirea mai întâi a numelui lui Allah, iar acest lucru transmite, de asemenea, sensul de a începe doar în numele lui Allah și nu în numele altcuiva.
