---
extends: _layouts.answer
section: content
title: Quelle est la vision des musulmans sur le but de la vie ?
date: 2024-06-19
description: L'islam enseigne que le but de la vie est d'adorer Dieu et que les êtres
  humains seront jugés par Dieu dans l'au-delà.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=NMM01&articleID=NMM010003&articlePages=4
  title: spubs.com
translator_id: 573111
---

Dans le Saint Coran, Dieu enseigne aux êtres humains qu'ils ont été créés pour l'adorer et que la base de toute véritable adoration est la conscience de Dieu. Dans la mesure où les enseignements de l'islam englobent tous les aspects de la vie et de l'éthique, la conscience de Dieu est encouragée dans toutes les affaires de l'homme.

L'islam indique explicitement que tous les actes humains sont des actes d'adoration s'ils sont accomplis pour Dieu seul et conformément à sa loi divine. Ainsi, l'adoration en islam ne se limite pas aux rituels religieux. Les enseignements de l'islam agissent comme une miséricorde et une guérison pour l'âme humaine, et des qualités telles que l'humilité, la sincérité, la patience et la charité sont fortement encouragées. En outre, l'islam condamne l'orgueil et l'autosatisfaction, car Dieu tout-puissant est le seul juge de la droiture humaine.

La vision islamique de la nature humaine est également réaliste et équilibrée. Les êtres humains ne sont pas considérés comme pécheurs par nature, mais comme étant tout autant capables de faire le bien que le mal. L'islam enseigne par ailleurs que la foi et l'action vont de pair. Dieu a donné aux hommes le libre arbitre, et la foi d'une personne se mesure à ses actes. Cependant, les êtres humains ont également été créés faibles et tombent régulièrement dans le péché.

Telle est la nature de l'être humain tel qu'il a été créé par Dieu dans Sa Sagesse, et elle n'est pas intrinsèquement "corrompue" ni nécessitant de corrections. En effet, la voie du repentir est toujours ouverte à tous les êtres humains, et Dieu Tout-Puissant aime le pécheur repentant plus que celui qui ne pèche pas du tout.

Le véritable équilibre d'une vie islamique repose sur une crainte saine de Dieu et une croyance sincère en Sa miséricorde infinie. Une vie sans crainte de Dieu conduit au péché et à la désobéissance, tandis que croire que nos péchés sont trop grands pour être pardonnés par Dieu ne mène qu'au désespoir. En ce sens, l'islam enseigne que seuls les égarés désespèrent de la miséricorde de leur Seigneur.

{{ $page->verse('39:53') }}

En outre, le Saint Coran, révélé au Prophète Muhammad, paix soit sur lui, contient de nombreux enseignements sur la vie dans l'au-delà et le Jour du Jugement. Par conséquent, les musulmans croient que tous les êtres humains seront ultimement jugés par Dieu pour leurs croyances et leurs actions durant leur vie terrestre.

En jugeant les êtres humains, Dieu Tout-Puissant sera aussi Juste que Miséricordieux, et chacun ne sera jugé que pour ce dont ils étaient capables. Il va sans dire que l'islam enseigne que la vie est une épreuve et que tous les êtres humains devront rendre des comptes à Dieu. Une croyance sincère dans la vie de l'au-delà est essentielle pour mener une vie équilibrée et morale. Autrement, la vie est considérée comme une fin en soi, ce qui incite les êtres humains à devenir plus égoïstes, matérialistes et immoraux.
