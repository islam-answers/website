---
extends: _layouts.answer
section: content
title: מהו האוכל החלאל?
date: 2024-09-12
description: מזונות חלאלים הם אלה שמותר על ידי אללה למוסלמים לצרוך, כאשר החריגים העיקריים
  הם חזיר ואלכוהול.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 593514
---

מזונות חלאלים או חוקיים הם אלה שאללה התיר למוסלמים לאכול. באופן כללי, רוב המזונות והמשקאות נחשבים לחלאל, למעט בשר חזיר ואלכוהול. צריך לשחוט בשר ועופות בצורה אנושית ונכונה, לרבות הזכרת שם אללה לפני השחיטה וצמצום סבל של בעלי חיים.
