---
extends: _layouts.answer
section: content
title: What does Islam say about treatment to parents?
date: 2024-04-01
description: Showing kindness to parents is one of the highest rewarded acts in the
  sight of God. The command to be good to one's parents is a clear teaching of the
  Qur'an.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
---

Showing kindness to parents is one of the highest rewarded acts in the sight of Allah. The command to be good to one's parents is a clear teaching of the Qur'an. Allah says:

{{ $page->verse('4:36..') }}

The mention of servitude to parents follows immediately after servitude to God. This is repeated throughout the Qur'an.

{{ $page->verse('17:23') }}

In this verse, God commands us to worship Him alone and no one else, and immediately then turns attention to behaving well with parents, especially when they become old and depend on others for shelter and their needs. Muslims are taught not to be rough with parents or scold them, but rather speak to them with kind words and show love and tenderness.

In the Qur’an, Allah further asks a person to supplicate to Him for his/her parents:

{{ $page->verse('17:24') }}

Allah emphasises that obeying parents is obeying Him, except in associating partners with Him.

{{ $page->verse('31:14-15..') }}

This verse emphasises that all parents deserve good treatment. Extra care should be shown to mothers, due to the additional hardship they endure.

Taking care of parents is considered to be one of the best deeds:

{{ $page->hadith('muslim:85e') }}

In conclusion, in Islam, honouring one’s parents means obeying them, respecting them, praying for them, lowering one's voice in their presence, smiling at them, not showing displeasure towards them, striving to serve them, fulfilling their wishes, consulting them and listening to what they say.
