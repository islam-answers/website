---
extends: _layouts.answer
section: content
title: Pourquoi les musulmans jeûnent-ils ?
date: 2024-08-03
description: La raison principale pour laquelle les musulmans jeûnent pendant le mois
  de Ramadan est pour atteindre la "Taqwa" (conscience de Dieu).
sources:
- href: https://seekersguidance.org/articles/https-www-seekersguidance-org-courses-why-muslims-fast-the-higher-aims-of-fasting-explained/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/fast/why-do-we-fast/
  title: seekersguidance.org
- href: https://seekersguidance.org/articles/quran-articles/spiritual-purpose-fasting-closeness-allah/
  title: seekersguidance.org
translator_id: 586055
---

Le jeûne est l'un des plus grands actes d'adoration que les musulmans accomplissent chaque année pendant le mois de Ramadan. Il s'agit d'un acte d'adoration purement sincère qu'Allah le Très-Haut récompensera lui-même.

{{ $page->hadith('bukhari:5927') }}

Dans la sourate al-Baqarah, Allah le Très-Haut dit :

{{ $page->verse('2:185') }}

La raison principale pour laquelle les musulmans jeûnent pendant le mois de Ramadan est clairement indiquée dans ce verset :

{{ $page->verse('2:183') }}

De plus, les vertus du jeûne sont nombreuses, par exemple :

1. Il s'agit d'une expiation des péchés et des fautes,
1. C'est un moyen de rompre avec les désirs inadmissibles,
1. Il facilite les actes de dévotion.

Le jeûne pose de nombreux défis, tels que de la faim, la soif ou encore les perturbations du sommeil, etc. Chacun de ces défis fait partie des luttes qui nous ont été imposées afin que nous puissions apprendre, nous développer et grandir.
Ces difficultés ne passent pas inaperçues aux yeux d'Allah, et il nous est demandé d'en être activement conscients afin d'obtenir de Lui une généreuse récompense. C'est ce que l'on comprend des paroles du prophète Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:37') }}
