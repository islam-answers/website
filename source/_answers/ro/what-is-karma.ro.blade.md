---
extends: _layouts.answer
section: content
title: Ce este karma?
date: 2025-01-26
description: Karma - o credință în consecințele acțiunilor - este un concept fals
  din religiile indiene. Islamul ne învață că numai Allah guvernează răsplata și pedeapsa.
sources:
- href: https://www.islamweb.net/en/fatwa/343198/
  title: islamweb.net
- href: https://islamqa.info/ar/answers/183131/
  title: islamqa.info
translator_id: 554943
---

"Karma" este un termen comun în religiile indiene (hinduism, jainism, sikhism și budism). Cuvântul "karma" se referă la acțiunile unei ființe vii și la consecințele morale care rezultă din acestea. Orice act de bine sau de rău, fie că este un cuvânt, o acțiune sau doar un gând, trebuie să aibă consecințe (recompensă sau pedeapsă), atât timp cât este rezultatul unei conștiințe și percepții anterioare.

Pentru aceste religii, sistemul karmei funcționează conform unei legi morale naturale și nu se află sub autoritatea judecăților divine. Potrivit acestora, Karma determină lucruri precum aspectul exterior, frumusețea, inteligența, vârsta, bogăția și statutul social. De asemenea, ei susțin că legea karmei guvernează tot ceea ce este creat și că este o lege imuabilă. Ei susțin că această lege guvernează și monitorizează fiecare moment și, prin urmare, fiecare dintre acțiunile noastre bune și rele are consecințele sale.

"Enciclopedia facilitată a doctrinelor și cultelor religioase contemporane" menționează:

> Karma - conform hindușilor - este legea retribuției, ceea ce înseamnă că sistemul universului este divin și bazat pe dreptate pură, această dreptate care va avea loc în mod inevitabil fie în viața prezentă, fie în viața următoare, iar retribuția unei vieți va fi în altă viață (...)

De asemenea, se menționează:

> Și omul continuă să se nască și să moară atâta timp cât karma este atașată sufletului său, iar sufletul său nu este purificat până când nu scapă de karma, unde dorințele sale încetează, iar atunci el rămâne viu și nemuritor în beatitudinea mântuirii, care este stadiul de "Nirvana" sau mântuirea care poate fi obținută în această lume prin antrenament și exercițiu sau prin moarte.

### Este karma un concept adevărat?

Nu există nicio îndoială că aceste religii indiene sunt religii păgâne, formate în conformitate cu credințe false și percepții imposibile, imaginare. Credința în "karma" este una dintre aceste credințe false la care acești oameni aderă.

Putem rezuma motivele pentru care această credință coruptă este falsă în felul următor:

1. Este o credință inventată, care nu se bazează pe o revelație divină infailibilă, ci își are sursa într-o religie păgână inventată.
1. Este un sistem care funcționează conform unei legi morale naturale care este autosuficientă, independentă de legea divină și de credințele religioase cerești.
1. Ei susțin că este o lege dominantă care controlează fiecare creatură, monitorizează acțiunile, gestionează destinele și recompensează pentru acțiuni. Aceasta este o blasfemie clară; pentru că Dumnezeu este cel dominant și cel care îi face pe oameni responsabili pentru acțiunile lor.
1. Această credință face parte din sistemul lor de credințe false prin care doresc să ajungă la stadiul de mântuire veșnică, așa cum pretind ei, care este cel mai înalt obiectiv pentru ei, deoarece karma este consecința acțiunilor pe care oamenii le fac și nu există mântuire atâta timp cât karma există.

Iar noi, slavă lui Dumnezeu (Allah), suntem independenți de aceste credințe false și secte inventate prin religia și harul lui Dumnezeu. Este suficient pentru noi să spunem:

{{ $page->verse('99:7-8') }}

Este suficient pentru noi să știm că Allah este Păzitorul tuturor lucrurilor și că Allah a cuprins toate lucrurile în cunoașterea Sa. Allah spune în Coran:

{{ $page->verse('53:31') }}

Oricine știe acest lucru și crede în el și este sigur că Allah îi va învia pe cei din morminte pentru a-i trage la răspundere pentru faptele lor, iar El a stabilit martori și scribi care vor înregistra acele fapte, nu va avea nevoie de această credință coruptă și greșită pentru a înceta să păcătuiască și a se abține de la cuvinte, fapte și moravuri rele.
