---
extends: _layouts.answer
section: content
title: Warum dürfen Muslime keinen Alkohol trinken?
date: 2024-10-16
description: Alkoholkonsum ist eine schwere Sünde, die den Geist trüb und Geld verschwendet. Allah hat alles verboten, was Körper und Geist schadet.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
translator_id: 587746
---

Der Islam kam, um Gutes zu fördern und zu vermehren sowie Schädliches abzuwenden und zu verringern. Alles, was nützlich oder überwiegend nützlich ist, ist erlaubt (halal), während alles, was schädlich oder überwiegend schädlich ist, verboten (haram) ist. Alkohol gehört zweifellos zur zweiten Kategorie. Allah sagt:

{{ $page->verse('2:219') }}

Die schädlichen und negativen Auswirkungen von Alkohol sind allen Menschen gut bekannt. Zu den schädlichen Folgen von Alkohol zählt auch das, was Allah erwähnt hat:

{{ $page->verse('5:90-91') }}

Alkohol führt zu vielen schädlichen Dingen und verdient es, „der Schlüssel zu allem Übel“ genannt zu werden – wie es der Prophet Muhammad {{ $page->pbuh() }} beschrieb, der sagte:

{{ $page->hadith('ibnmajah:3371') }}

Es schafft Feindschaft und Hass zwischen den Menschen, hindert sie daran, Allah zu gedenken und zu beten, und verführt sie zu Zina (unrechtmäßigen sexuellen Beziehungen). Alkohol kann sogar dazu führen, dass Menschen Inzest mit ihren Töchtern, Schwestern oder anderen weiblichen Verwandten begehen. Er nimmt den Menschen den Stolz und die beschützende Eifersucht, erzeugt Scham, Bedauern und Schande. Alkohol führt zur Offenbarung von Geheimnissen und dem Aufdecken von Fehlern. Zudem ermutigt er die Menschen dazu, Sünden und böse Taten zu begehen.

Darüber hinaus berichtete Ibn 'Umar, dass der Prophet {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:2003a') }}

Der Prophet {{ $page->pbuh() }} sagte auch:

{{ $page->hadith('ibnmajah:3381') }}
