---
extends: _layouts.answer
section: content
title: Kakav je muslimanov pogled na život?
date: 2024-10-18
description: Muslimanski pogled na život oblikovan je uvjerenjima poput ravnoteže
  nade i straha, plemenita svrha, zahvalnost, strpljenje, povjerenje u Boga i odgovornost.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 584497
---

Muslimanski pogled na život oblikovan je sljedećim vjerovanjima:

- Ravnoteža nade u Božje milosrđe i straha od Njegove kazne
- Imati plemenitu svrhu
- Ako se dobro dogodi, budi zahvalan. Ako se loše dogodi, imajte strpljenja
- Ovaj život je test i Bog vidi sve što radim
- Uzdaj se u Boga - ništa se ne događa osim s Njegovim dopuštenjem
- Sve što imam je od Boga
- Istinska nada i briga za nemuslimane koje treba uputiti
- Usredočiti se na ono što je u mojoj kontroli i dati sve od sebe
- Vratit ću se Bogu i odgovarat ću
