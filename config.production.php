<?php

return [
    "baseUrl" => "https://islam-answers.com",
    "production" => true,
    "build" => [
        "destination" => "public",
    ],
];
