---
extends: _layouts.answer
section: content
title: O Que o Islã Diz sobre o Dia do Juízo?
date: 2024-06-02
description: Chegará um dia quando todo o universo será destruído e os mortos serão
  ressuscitados, para o julgamento perante Deus.
sources:
- href: https://www.islam-guide.com/pt/ch3-5.htm
  title: islam-guide.com
---

Como os cristãos, os muçulmanos acreditam que a vida presente é apenas um teste para o próximo campo de existência. Esta vida é um teste para cada indivíduo para a vida após a morte. Chegará um dia quando todo o universo será destruído e os mortos serão ressuscitados, para o julgamento perante Deus. Esse dia será o começo de uma vida que nunca terminará. Esse dia é o Dia do Juízo. Nesse dia todas as pessoas serão recompensadas por Deus de acordo com suas crenças e atos. Aqueles que morreram acreditando que “Não existe deus mas Deus, e Muhammad é o Mensageiro (Profeta) de Deus” e são muçulmanos, serão recompensados naquele dia e serão admitidos no Paraíso para sempre, como Deus disse:

{{ $page->verse('2:82') }}

Mas aqueles que morreram não acreditando que “Não existe deus mas Deus, e Muhammad é o Mensageiro (Profeta) de Deus” ou não são muçulmanos, perderão o Paraíso para sempre e serão enviados para o Inferno, como Deus disse:

{{ $page->verse('3:85') }}

E como Ele disse:

{{ $page->verse('3:91') }}

Alguém pode perguntar, “Eu acho que o Islã é uma boa religião, mas se eu me converter ao Islã, minha família, amigos e outras pessoas me perseguirão e caçoarão de mim. Então, se eu não me converter ao Islã, eu entrarei no Paraíso e serei salvo do Inferno?”

A resposta é a que Deus disse no versículo anterior, “E quem quer que busque outra religião além do Islã, não será aceita e ele se contará entre os perdedores na Vida Futura.”

Depois de ter enviado o Profeta Muhammad {{ $page->pbuh() }} para chamar as pessoas para o Islã, Deus não aceitou aderência a nenhuma religião além do Islã. Deus é nosso Criador e Sustentador. Ele criou para nós tudo o que está na terra. Todas as bênçãos e coisas boas que temos vieram Dele. Então, depois de tudo isso, quando alguém rejeita a crença em Deus, Seu Profeta Muhammad {{ $page->pbuh() }}, ou Sua religião do Islã, é justo que ele ou ela seja punido na Outra Vida. De fato, o propósito maior de nossa criação é adorar a Deus somente e obedecê-Lo, como Deus disse no Alcorão Sagrado (51:56).

Essa vida que vivemos hoje é muito curta. Os descrentes no Dia do Juízo pensarão que a vida que viveram na terra foi de apenas um dia ou parte de um dia, como Deus disse:

{{ $page->verse('23:112-113..') }}

E como Ele disse:

{{ $page->verse('23:115-116..') }}

A vida na Vida Futura é uma vida muito real. Não é apenas espiritual, mas física também. Nós viveremos lá com nossas almas e corpos.

Comparando esse mundo com a Outra Vida, o Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2858') }}

O significado é que o valor desse mundo comparado com o da Outra Vida é como algumas gotas de água comparadas com o oceano.
