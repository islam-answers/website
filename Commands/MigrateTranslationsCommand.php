<?php

namespace App\Commands;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'migrate:translations',
    description: 'Migrate translations from single JSON to multiple JSON files',
)]
class MigrateTranslationsCommand extends Command
{
    private OutputInterface $output;

    private function migrateTranslations(string $file)
    {
        $json = json_decode(file_get_contents(__DIR__ . "/../l10n/$file.json"), true);
        $newJson = [];

        // Purpose is to change from key>reference>lang to key>lang file that contains all references and translations

        foreach ($json as $key => $value) {
            foreach ($value as $reference => $translations) {
                foreach ($translations as $lang => $translation) {
                    if (!isset($newJson[$key])) {
                        $newJson[$key] = [];
                    }
                    if (!isset($newJson[$key][$lang])) {
                        $newJson[$key][$lang] = [];
                    }
                    $newJson[$key][$lang][$reference] = $translation;
                }
            }
        }

        foreach ($newJson as $key => $value) {
            foreach ($value as $lang => $translations) {
                $this->output->writeln("Writing $file/$key.$lang.json with " . count($translations) . " translations");
                file_put_contents(__DIR__ . "/../l10n/$file/$key.$lang.json", json_encode($translations, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
            }
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;

        $this->migrateTranslations('ahadith');
        $this->migrateTranslations('verses');

        return Command::SUCCESS;
    }
}
