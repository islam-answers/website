---
extends: _layouts.answer
section: content
title: 《古兰经》是谁写的？
date: 2024-07-08
description: 古兰经是真主的真言，是由真主靠加百列 (Gabriel) 天使，对其先知穆罕默德 所揭示的。
sources:
- href: https://www.islam-guide.com/ch1-1.htm
  title: islam-guide.com
translator_id: 554943
---

真主支持其最后的先知穆罕默德 {{ $page->pbuh() }} 有相当多的奇迹及证据证明他是真主所派来的真正先知。 而且真主也支持其最后一本启示录，即神圣古兰经，其中的许多证据都证明这本古兰经是真主的真言，由其揭示，而非人类所著述。

古兰经是真主的真言，是由真主靠加百列 (Gabriel) 天使，对其先知穆罕默德 {{ $page->pbuh() }} 所揭示的。 它由穆罕默德记诵 {{ $page->pbuh() }}，再对他的追随者口述。 他们依次记诵并写下它，再与先知穆罕默德 {{ $page->pbuh() }}一起校阅。 此外，先知穆罕默德 {{ $page->pbuh() }} 与加百列天使每年校阅古兰经一次，并在其生命的最后一年校阅两次。 从古兰经揭示至今，已经有数不清的穆斯林逐字记诵整本古兰经。 其中的部分穆斯林甚至在十岁时即可记住整本古兰经。 经过若干世纪，古兰经未曾更改一字。

古兰经于十四世纪前降示，所提及的真理直到最近才被科学家发现或证实。 证据显示古兰经无疑是真主的真言，由其对先知穆罕默德 {{ $page->pbuh() }} 降示，而且古兰经绝非由穆罕默德 {{ $page->pbuh() }} 或其他任何人所著述。 这也证明穆罕默德 {{ $page->pbuh() }} 确实是真主所派来的先知。 对于十四世纪前的任何人而言，都无法了解这些利用先进的设备及完备的科学方法，于近日才被发现或证实的真理。
