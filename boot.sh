echo -n "Node " && node --version
echo -n "Yarn " && yarn --version
php --version
composer --version

yarn install --pure-lockfile --offline --silent
composer install --no-interaction --prefer-dist

yarn watch
