---
extends: _layouts.answer
section: content
title: Perché i musulmani non festeggiano il Natale?
date: 2024-12-22
description: I musulmani rispettano Gesù in quanto grande profeta, ma ne rifiutano
  la divinità. Il Natale non è pertanto ammissibile nell'Islam.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 573107
---

I musulmani rispettano e venerano Gesù {{ $page->pbuh() }}, il figlio di Maryam (Maria) e attendono la sua seconda venuta. Lo considerano uno dei più grandi messaggeri di Dio per l'umanità. La fede di un musulmano non è considerata valida se non crede in tutti i Messaggeri di Allah, incluso Gesù {{ $page->pbuh() }}.

Ma i musulmani non credono che Gesù fosse Dio o il Figlio di Dio. Né credono che sia stato crocifisso. Allah mandò Gesù ai Figli di Israele per invitarli a credere solo in Allah e ad adorare Lui soltanto. Allah fornì sostegno a Gesù con alcuni miracoli per dimostrare che stava dicendo la verità.

Allah dice nel Corano:

{{ $page->verse('19:88-92') }}

### È permesso festeggiare il Natale?

Date queste differenze teologiche, ai musulmani non è consentito celebrare le festività di altre religioni in nessuno dei loro rituali o usanze. Pertanto, il giorno che è in concomitanza con qualcuna delle loro feste, per i musulmani si tratta di un giorno normale. Non devono quindi celebrarlo con nessuna delle attività praticate dai non musulmani in questi giorni.

La celebrazione cristiana del Natale è una tradizione che riguarda credenze e pratiche non radicate negli insegnamenti islamici. Per questo motivo, i musulmani non possono partecipare a queste celebrazioni, poiché non corrispondono alla concezione islamica di Gesù {{ $page->pbuh() }} o ai suoi insegnamenti.

Oltre ad essere un'innovazione, rientra tra le imitazioni dei miscredenti in questioni che riguardano soltanto loro e la loro religione. Il profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('abudawud:4031') }}

Ibn `Uthaymin (che Allah abbia misericordia di lui) disse:

> Congratularsi con i miscredenti in occasione del Natale, o di qualsiasi altra loro festa religiosa, è proibito secondo il consenso degli studiosi perché implica il riconoscimento della miscredenza che seguono e l'approvazione di quel che fanno. Anche se un musulmano non accetta questa miscredenza per se stesso, è proibito riconoscerne i rituali o congratularsi con qualcuno per essi. Ai musulmani non è consentito imitarli in alcun modo nelle loro feste, che si tratti di cibo, vestiti, bagni, accensione di fuochi o astensione dal lavoro o dal culto abituale, e così via. E non è permesso fare festa, scambiare regali, vendere cose che li aiutino a celebrare le loro feste (...) o adornarsi o allestire addobbi e decorazioni.

L'Enciclopedia Britannica afferma quanto segue:

> La parola Natale deriva dall'inglese antico Cristes maesse, "Messa di Cristo". Non esiste una tradizione certa sulla data di nascita di Cristo. I cronografi cristiani del III secolo ritenevano che la creazione del mondo fosse avvenuta all'equinozio di primavera, allora calcolato come il 25 marzo. Di conseguenza, la nuova creazione nell'incarnazione (cioè il concepimento) e la morte di Cristo sarebbero avvenute nello stesso giorno, con la sua nascita nove mesi dopo, al solstizio d'inverno, il 25 dicembre.

> Il motivo per cui il Natale venga celebrato il 25 dicembre rimane tuttora incerto, ma molto probabilmente deriva dal fatto che i primi cristiani desideravano che la data coincidesse con la festa pagana romana che segnava la "nascita dell'indomito sole" (natalis solis invicti). Questa festa celebrava il solstizio d'inverno, quando le giornate ricominciano ad allungarsi e il sole inizia a salire più in alto nel cielo. Le usanze tradizionali legate al Natale quindi hanno avuto origine da diverse fonti, a seguito della coincidenza della celebrazione della nascita di Cristo con le ricorrenze agricole e solari pagane, a metà inverno. Nel mondo romano, i Saturnali (17 dicembre) erano un momento di festa e scambio di doni. Il 25 dicembre era anche considerato la data di nascita del dio misterioso iraniano Mithra, il Sole della Giustizia. Nel capodanno romano (1° gennaio), le case venivano decorate con verde e luci e si portavano doni ai bambini e ai poveri.

Pertanto, come si può vedere, non esiste alcuna base solida per il Natale: sia Gesù {{ $page->pbuh() }} che i suoi veri seguaci non hanno mai celebrato il Natale o chiesto a qualcuno di festeggiarlo, né vi sono testimonianze di persone, che si definiscono cristiane, che abbiano mai celebrato il Natale per molte centinaia di anni dopo la nascita di Gesù. Quindi, erano ben guidati i compagni di Gesù, che non celebravano il Natale, o lo sono di più le persone di oggi?

Perciò, se volete rispettare Gesù {{ $page->pbuh() }} come fanno i musulmani, non celebrate una tradizione inventata, scelta soltanto perché coincida con feste pagane e per imitare le usanze ad esse correlate. Pensate veramente che Dio, o anche Gesù stesso, approverebbe o condannerebbe una cosa del genere?
