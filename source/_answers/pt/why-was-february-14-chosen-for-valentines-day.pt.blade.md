---
extends: _layouts.answer
section: content
title: Por que escolheram 14 de fevereiro para o Dia dos Namorados?
date: 2025-02-11
description: O Dia dos Namorados, em 14 de fevereiro, substituiu o festival pagão
  de Lupercália e mais tarde foi associado a São Valentim.
sources:
- href: https://muslimskeptic.com/2022/02/14/the-dark-origins-of-valentines-day-muslims-might-be-unaware-of/
  title: muslimskeptic.com
- href: https://muslimskeptic.com/2022/02/15/its-not-just-christmas-how-christianity-adopts-paganism-for-valentines-day/
  title: muslimskeptic.com
- href: https://islamqa.info/en/answers/73007/
  title: islamqa.info
- href: https://islamqa.info/en/articles/65/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145893/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21694/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1130/
  title: islamqa.info
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 594172
---

### Qual é a origem do Dia dos Namorados?

Para entender as origens do Dia dos Namorados, primeiro precisamos analisar sua história, que, segundo historiadores, é uma fusão de duas ocorrências históricas distintas: uma envolvendo um sacerdote cristão (São Valentim) e a outra relacionada ao festival pagão de Lupercália.

Lupercália era uma celebração sangrenta, violenta e carregada de conotações sexuais, repleta de sacrifícios de animais, emparelhamentos aleatórios e uniões com a esperança de afastar espíritos malignos e a infertilidade. Esse festival repugnante era celebrado anualmente em 15 de fevereiro, apenas um dia após o atual Dia dos Namorados. Originalmente era conhecido como Februa, que significa "Purificações" ou "Expurgos", sendo a base para o nome do mês de fevereiro.

Tudo começava com um grupo de sacerdotes romanos chamados Luperci reunindo-se em um local específico, como a caverna de Lupercal. Esses Luperci sacrificavam então um bode e um cão, obviamente em nome de suas falsas divindades. O sacrifício do bode simbolizava a fertilidade. Os Luperci corriam descontroladamente pela cidade, chicoteando todas as mulheres que encontrassem. Isso, novamente, era considerado um símbolo de fertilidade. Acreditava-se que essas mulheres aceitavam essa prática na esperança de se tornarem férteis. No entanto, há relatos históricos de que essa parte do ritual nem sempre era consensual. O ritual "romântico" terminava com jovens homens e mulheres formando pares. Em outras palavras, era uma noite de fornicação.

A fusão desse festival com a história de São Valentim, que muitas pessoas atualmente acreditam ser a origem principal dessa ocasião, foi provavelmente uma estratégia da Igreja Católica para tornar o cristianismo mais atraente aos pagãos. São Valentim é um nome atribuído a dois dos antigos "mártires" da Igreja Cristã. Diz-se que eram dois, ou que era apenas um. Quando os romanos abraçaram o cristianismo, continuaram celebrando seu festival chamado Festa do Amor, mas mudaram o conceito pagão de "amor espiritual" para outro conceito conhecido como "mártires do amor", representado por São Valentim, que teria defendido o amor e a paz, pelos quais, segundo suas alegações, foi martirizado. Esse festival também passou a ser chamado de Festa dos Amantes, e São Valentim foi considerado o santo padroeiro dos amantes.

Uma das falsas crenças associadas a esse festival era que os nomes de meninas que haviam atingido a idade para casar seriam escritos em pequenos rolos de papel e colocados em um recipiente sobre uma mesa. Então, os jovens que desejavam se casar eram chamados, e cada um pegava um pedaço de papel. Ele se colocaria ao serviço da garota cujo nome havia tirado durante um ano, para que se conhecessem melhor. Depois disso, poderiam se casar ou repetir o mesmo processo no dia do festival no ano seguinte. O clero cristão reagiu contra essa tradição, que consideravam ter uma influência corruptora sobre a moral dos jovens.

Também se disse, sobre as origens desse feriado, que quando os romanos se tornaram cristãos, após a disseminação do cristianismo, o imperador romano Cláudio II decretou, no terceiro século EC, que os soldados não deveriam se casar, pois o casamento os distraía das guerras que costumavam travar. Esse decreto foi contestado por São Valentim, que começou a realizar casamentos secretos para os soldados. Quando o imperador descobriu, mandou prendê-lo e o condenou à execução. Na prisão, São Valentim se apaixonou pela filha do carcereiro, mas isso permaneceu em segredo, pois, segundo as leis cristãs, sacerdotes e monges eram proibidos de casar ou se apaixonar. Mesmo assim, ele ainda é altamente estimado pelos cristãos devido à sua firmeza em se manter fiel ao cristianismo, mesmo quando o imperador lhe ofereceu perdão, caso ele abandonasse o cristianismo e adorasse os deuses romanos; nesse caso, ele seria um dos seus mais próximos confidentes e se tornaria seu genro. Porém, Valentim recusou a oferta e preferiu o cristianismo, sendo executado em 14 de fevereiro de 270 EC, na véspera do festival de Lupercalis, em 15 de fevereiro. Assim, esse dia recebeu o nome em homenagem a esse santo.

### Porque os muçulmanos não celebram o Dia dos Namorados?

Alguém poderá perguntar: porque é que os muçulmanos não celebram esta festa? Em primeiro lugar, as festas fazem parte das cerimónias religiosas de que Allah diz no Alcorão:

{{ $page->verse('..5:48..') }}

Allah também diz:

{{ $page->verse('22:67..') }}

Uma vez que o Dia dos Namorados remonta à época romana e não à época islâmica, isto significa que é algo que pertence exclusivamente ao cristianismo e não ao Islam, e que os muçulmanos não têm qualquer participação nele. O Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:892') }}

Isso significa que cada nação deve ser distinguida por seus festivais. Se os cristãos têm um festival e os judeus têm um festival que lhes pertence exclusivamente, então nenhum muçulmano deve participar deles, assim como ele não compartilha da sua religião nem da sua direção de oração.

Em segundo lugar, celebrar o Dia dos Namorados significa assemelhar-se ou imitar os romanos pagãos. Não é permitido para os muçulmanos imitar não-muçulmanos em assuntos que não fazem parte do Islam. O Profeta, {{ $page->pbuh() }} disse:

{{ $page->hadith('abudawud:4031') }}

Em terceiro lugar, é um erro confundir o nome dado ao dia com as verdadeiras intenções por trás dele. O amor referido neste dia é o amor romântico, envolvendo amantes, namorados e namoradas. Sabe-se que é um dia marcado por promiscuidade e relações sexuais, sem limites ou restrições. Não se trata do amor puro entre um homem e sua esposa, ou uma mulher e seu marido, ou, pelo menos, não fazem distinção entre o amor legítimo dentro do relacionamento conjugal e o amor proibido entre amantes.

No Islam, o marido ama a sua mulher durante todo o ano e exprime-lhe esse amor com presentes, em verso e em prosa, em cartas e de outras formas, ao longo dos anos - e não apenas num dia do ano. O Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:1468') }}

Não há nenhuma religião que encoraje os seus seguidores a amarem-se e a cuidarem uns dos outros mais do que o Islam. Isto aplica-se em todos os momentos e em todas as circunstâncias. O Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:54') }}

### Quem beneficia com o Dia dos Namorados?

O Dia dos Namorados evoluiu para um feriado altamente comercializado, com inúmeras tradições e costumes. As pessoas trocam cartões com poemas românticos, organizam festas comemorativas e oferecem presentes aos seus entes queridos. O impacto comercial é significativo — somente na Grã-Bretanha, as vendas de flores alcançaram 22 milhões de libras. O consumo de chocolate aumenta drasticamente, enquanto os preços das rosas podem multiplicar-se por dez, passando de 1 a 10 dólares por haste. Lojas de presentes e cartões competem intensamente com produtos especializados para o Dia dos Namorados, e algumas famílias até decoram suas casas com rosas vermelhas para a ocasião. A escala econômica do Dia dos Namorados é massiva — em 2020, os americanos gastaram 27,4 bilhões de dólares em presentes de Dia dos Namorados, segundo a National Retail Federation (NRT), a maior associação de comércio varejista do mundo. No final das contas, são os fabricantes e varejistas que mais se beneficiam dessa celebração comercializada do amor.
