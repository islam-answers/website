---
extends: _layouts.answer
section: content
title: Quali sono le principali pratiche da osservare nell'Islam?
date: 2024-06-26
description: Le principali pratiche nell'Islam (i Cinque Pilastri) sono la Testimonianza
  di Fede, la Preghiera, l'Elemosina Prescritta, il Digiuno e il Pellegrinaggio.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 573107
---

Le principali pratiche da osservare nell'Islam sono indicate come i Cinque Pilastri.

### Prima pratica: La Testimonianza di Fede

Dichiarare che non c'è nessun Dio degno di essere adorato all'infuori di Allah, e che Mohammed è il Suo ultimo Messaggero.

### Seconda pratica: La Preghiera

Da eseguire cinque volte al giorno: all'alba, a mezzogiorno, a metà pomeriggio, dopo il tramonto e alla sera.

### Terza pratica: L'Elemosina Prescritta o "Zakat"

Si tratta di un'elemosina annuale obbligatoria da versare alle persone bisognose. Corrisponde ad una piccola parte del totale di quanto risparmiato per un intero anno, esattamente il 2,5% del patrimonio in denaro, ma può includere anche altri beni. Viene pagata da coloro che hanno ricchezze in eccesso rispetto ai propri bisogni.

### Quarta pratica: Il Digiuno nel mese di Ramadan

Durante questo mese, i musulmani devono astenersi da qualsiasi cibo, bevanda e rapporto sessuale col proprio coniuge, dall'alba al tramonto. Questo mese di digiuno rafforza l'autocontrollo, la consapevolezza di Dio e l'empatia verso i poveri.

### Quinta pratica: Il Pellegrinaggio.

Ogni musulmano capace di intendere e di volere è tenuto a compiere il Pellegrinaggio alla Mecca nel corso della propria vita. Il Pellegrinaggio prevede le preghiere, le suppliche, l'elemosina e il viaggio. Si tratta di un'esperienza spirituale estremamente toccante.
