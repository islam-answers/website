---
extends: _layouts.answer
section: content
title: Làm thế nào để trở thành một người Islam?
date: 2024-06-25
description: 'Rất đơn giản chỉ cần bạn nói hay đọc câu sau đây kèm theo sự nhận thức
  rõ ràng: “Là i-là-hà il-lol-loh, Mu-ham-ma-dur-ro-su-lul-loh.'
sources:
- href: https://www.islam-guide.com/vi/ch3-6.htm
  title: islam-guide.com
---

Rất đơn giản chỉ cần bạn nói hay đọc câu sau đây kèm theo sự nhận thức rõ ràng: “Là i-là-hà il-lol-loh, Mu-ham-ma-dur-ro-su-lul-loh (phiên âm theo tiếng Việt)”. Tới đây bạn đã chuyển sang Islam và trở thành một người Islam. Câu nói trên có nghĩa là: “Không có thần linh nào khác ngoại trừ Thượng Đế (Allah), và Muhammad là Người đưa tin (Thiên Sứ) của Thượng Đế.” Phần đầu của câu này, “Trên đời này không có thánh thần nào ngoài Thượng Đế,” có nghĩa là ngoài Thượng Đế ra, không ai có quyền được thờ phụng, và rằng Thượng Đế không có vợ và cũng chẳng có con. Để trở thành một người Islam thực sự, người ta còn phải tin:

- Tin rằng Thánh Kinh Qur'an là lời của Thượng Đế và do Ngài mặc khải.
- Tin rằng Ngày Phán quyết (Ngày Hồi sinh) là có thực và ngày đó sẽ tới, như Thượng Đế đã hứa trong Kinh Qur'an.
- Chấp nhận Islam như tôn giáo của mình.
- Không thờ phụng ai hay vật gì ngoài Thượng Đế.

Thiên Sứ Muhammad {{ $page->pbuh() }} nói:

{{ $page->hadith('muslim:2747') }}
