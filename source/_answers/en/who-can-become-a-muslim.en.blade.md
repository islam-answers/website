---
extends: _layouts.answer
section: content
title: Who can become a Muslim?
date: 2024-09-03
description: Any human being can become a Muslim, no matter their previous religion, age, nationality or ethnic background.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
---

Any human being can become a Muslim, no matter their previous religion, age, nationality or ethnic background. To become a Muslim, you simply say the following words: “Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah”. This testimony of faith means: “I testify that there is no god worthy of being worshipped other than Allah and that Mohammad is His Messenger”. You must say it and believe in it.

Once you say this sentence, you automatically become Muslim. You do not need to ask about a time or an hour, by night or by day, to turn to your Lord and begin your new life. Anytime is the time for that.

You do not need anyone’s permission, or a priest to baptize you, or an intermediary to mediate for you, or anyone to guide you to Him, for He, may He be glorified, guides you Himself. So turn to Him, for He is near; He is closer to you than you think or can imagine:

{{ $page->verse('2:186') }}

Allah, may He be exalted, says:

{{ $page->verse('6:125..') }}
