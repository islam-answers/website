---
extends: _layouts.answer
section: content
title: Di cosa parla il Corano?
date: 2024-04-03
description: La fonte primaria della fede e della pratica di ogni musulmano.
sources:
- href: https://www.islam-guide.com/it/ch3-7.htm
  title: islam-guide.com
---

Il Corano, l'ultima parola rivelata di Dio, è la fonte primaria della fede e della pratica di ogni musulmano. Si occupa di tutto ciò che concerne l'essere umano: saggezza, dottrina, adorazione, transazioni, legge ecc., ma il tema di base è la relazione tra Dio e le Sue creature. Allo stesso tempo, fornisce le linee guida e gli insegnamenti dettagliati per una società giusta, per un comportamento umano corretto e un sistema economico equo.

Si noti che il Corano fu rivelato a Mohammed {{ $page->pbuh() }} solo in arabo. Quindi, ogni traduzione coranica, sia in inglese che in altre lingue, non è il Corano, non è una sua versione, ma piuttosto solo una traduzione del suo significato. Il Corano esiste solo in arabo nel quale fu rivelato.
