---
extends: _layouts.answer
section: content
title: මුස්ලිම්වරු උපවාස කරන්නේ ඇයි?
date: 2024-10-29
description: රාමසාන් මාසය තුළ මුස්ලිම්වරුන් උපවාසයේ නිරත වීමට මූලික හේතුව වන්නේ තක්වා
  (දෙවියන් පිළිබඳ දැනුවත්භාවය) ලබා ගැනීමයි.
sources:
- href: https://seekersguidance.org/articles/https-www-seekersguidance-org-courses-why-muslims-fast-the-higher-aims-of-fasting-explained/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/fast/why-do-we-fast/
  title: seekersguidance.org
- href: https://seekersguidance.org/articles/quran-articles/spiritual-purpose-fasting-closeness-allah/
  title: seekersguidance.org
translator_id: 583845
---

නිරාහාරව සිටීම සෑම වසරකම රාමසාන් මාසය තුළ මුස්ලිම්වරුන් කරන ශ්‍රේෂ්ඨතම නමස්කාර ක්‍රියාවකි. මහෝත්තම අල්ලාහ් විසින්ම විපාක දෙන සැබෑ අවංක නමස්කාර ක්‍රියාවකි -

{{ $page->hadith('bukhari:5927') }}

සූරා අල්-බකාරාහි දී අල්ලාහ් මහෝත්තමයාණන් වහන්සේ මෙසේ පවසයි.

{{ $page->verse('2:185') }}

රාමසාන් මාසය තුළ මුස්ලිම්වරුන් උපවාසයේ යෙදීමට මූලික හේතුව මෙම වැකියෙන් පැහැදිලි වේ.

{{ $page->verse('2:183') }}

මීට අමතරව, උපවාසයේ ගුණ බොහෝය, එනම්:

1. එය කෙනෙකුගේ පව් සහ වැරදි සඳහා සමාව දීමකි. 
1. එය අනුමත කළ නොහැකි ආශාවන් බිඳ දැමීමේ මාධ්‍යයකි. 
1. එය භක්ති ක්‍රියාවන් පහසු කරයි.

නිරාහාරව සිටීම මිනිසුන්ට බොහෝ අභියෝග මතු කරයි, කුසගින්න සහ පිපාසයේ සිට බාධා වූ නින්ද රටා සහ තවත් බොහෝ දේ. ඒ සෑම එකක්ම අප මත අනිවාර්ය කර ඇති අරගලවල කොටසක් වන අතර එමඟින් අපට ඒවා ඉගෙන ගැනීමට, සංවර්ධනය කිරීමට සහ වර්ධනය වීමට හැකිය. මෙම දුෂ්කරතා අල්ලාහ්ගේ අවධානයට ලක් නොවන අතර, ඒවා පිළිබඳව ක්‍රියාශීලීව දැනුවත්ව සිටින ලෙස අපට පවසා ඇත, එවිට අපට ඔහුගෙන් ත්‍යාගශීලී විපාකයක් අපේක්ෂා කළ හැකිය. මුහම්මද් නබි තුමාගේ වදන් වලින් මෙය අවබෝධ වේ {{ $page->pbuh() }} :

{{ $page->hadith('bukhari:37') }}
