---
extends: _layouts.answer
section: content
title: Ko je prorok Muhamed?
date: 2024-07-14
description: Prorok Muhamed (Mir Božiji sa njim) bio je savršen primer iskrenog, pravednog,
  milostivog, saosećajnog, istinitog i hrabrog ljudskog bića.
sources:
- href: https://www.islam-guide.com/ch3-8.htm
  title: islam-guide.com
translator_id: 584497
---

Muhamed {{ $page->pbuh() }} rođen je u Meki 570 godine. S obzirom da mu je otac umro pre njegovog rođenja, a majka kada mu je bilo samo šest godina, odgajao ga je ujak koji je bio iz uglednog plemena Kurajš. Muhamed je bio nepismen, nije znao da čita ili piše, i ostao je tako do svoje smrti. Njegov narod, pre njegove misije proroka, bio je neupućen u nauku i većina njih je bila nepismena. Kako je odrastao, postao je poznat po tome da je istinoljubiv, pošten, vredan poverenja, velikodušan i iskren. Bio je toliko vredan poverenja da su ga zvali "Povjerljivi". Muhamed {{ $page->pbuh() }} je bio veoma religiozan, i dugo je prezirao dekadenciju i idolatriju svog društva.

U četrdesetoj godini Muhamed {{ $page->pbuh() }} je primio prvu objavu od Boga preko Anđela Džibrila. Objave su se nastavila dvadeset i tri godine, i kolektivno su poznate kao Kuran.

Čim je počeo da čita Kuran i propoveda istinu koju mu je Bog otkrio, on i njegova mala grupa sledbenika trpeli su progon od nevernika. Progon je postao toliko žestok da im je 622 godine Bog dao zapovest da emigriraju. Ova emigracija od Meke do grada Medine, nekih 418 kilometara severno, označava početak muslimanskog kalendara.

Posle nekoliko godina, prorok Muhamed {{ $page->pbuh() }} i njegovi sledbenici uspeli su da se vrate u Meku, gde su oprostili svojim neprijateljima. Pre nego što je prorok Muhamed {{ $page->pbuh() }} umro, u 63 godini, veliki dio Arapskog poluostrva su postali muslimani. U roku od jednog veka nakon Njegove smrti, islam se rasprostranio sve od Španije na zapadu, pa do Kine na istoku. Među razlozima brzog i mirnog širenja islama bila je i istina i jasnoća njegove doktrine. Islam poziva na veru u samo jednog Boga, koji je jedini dostojan bogosluženja.

Prorok Muhamed {{ $page->pbuh() }} bio je savršen primer iskrenog, samilostivog, saosećajnog, istinitog i hrabrog ljudskog bića. Iako je bio čovek, bio je daleko od svih zlih osobina i težio je isključivo ka dobrobiti Boga i Njegove nagrade na drugom svijetu. Štaviše, u svim svojim postupcima i poslovima, uvek je bio pažljiv i bogobojazan.
