---
extends: _layouts.answer
section: content
title: Thiên Sứ Muhammad là ai?
date: 2024-04-04
description: Thiên Sứ Muhammad (hòa bình đến với Ngài) là một ví dụ hoàn hảo về một
  con người thật thà, công bằng, nhân từ, luôn có lòng trắc ẩn, trung thực và dũng
  cảm.
sources:
- href: https://www.islam-guide.com/vi/ch3-8.htm
  title: islam-guide.com
---

Thiên Sứ Muhammad {{ $page->pbuh() }} sinh ra tại Makkah vào năm 570. Do cha của Ông mất trước khi Ông chào đời và mẹ Ông cũng qua đời ít lâu sau đó, Thiên Sứ Muhammad được người chú xuất thân từ bộ tộc cao quý Quraysh dưỡng dục. Ông lớn lên mà không biết chữ, không biết đọc và không biết viết, và cho đến khi Ông qua đời, Thiên Sứ Muhammad {{ $page->pbuh() }} vẫn mù chữ. Trước khi Ông thực hiện sứ mệnh làm Thiên Sứ, người của bộ tộc của Ông chẳng hiểu biết gì về khoa học, thậm chí phần đông trong số họ còn mù chữ. Khi Ông khôn lớn, Ông được người ta biết đến như một con người trung thực, đáng tin cậy, lương thiện, hào phóng, và chân thành. Ông đáng tin cậy đến mức, người ta gọi Ông là Ngài Đáng Tin. Thiên Sứ Muhammad {{ $page->pbuh() }} rất sùng đạo, và Ông từng có thời gian dài ghét cay ghét đắng sự suy đồi và thói sùng bái thần tượng trong xã hội thời bấy giờ.

Ở tuổi tứ tuần, Thiên Sứ Muhammad {{ $page->pbuh() }} nhận được mặc khải đầu tiên của Thượng Đế thông qua Thiên Thần Gabriel. Và sự mặc khải này kéo dài trong vòng 23 năm thì mới hoàn tất (Thánh Kinh Qur'an).

Ngay khi Ông bắt đầu đọc Kinh Qur'an và thuyết giáo về sự thật mà Thượng Đế đã mặc khải cho Ông, Ông và một nhóm nhỏ những người bạn của Ông đã phải chịu những sự ngược đãi của những kẻ thờ đa thần. Sự ngược đãi đã lên đến đỉnh điểm vào năm 622 và Thượng Đế đã ra lệnh cho họ phải di chuyển sang nơi khác. Lần di chuyển này, từ Makkah đến thành phố Madinah, khoảng 260 dặm lên phía bắc, đánh dấu cho sự khởi đầu của lịch Islam.

Sau đó vài năm, Thiên Sứ Muhammad {{ $page->pbuh() }} và những người theo Ông có thể trở lại Makkah, nơi mà họ đã tha thứ cho kẻ thù của mình. Trước khi Thiên Sứ Muhammad qua đời, ở tuổi 63, phần lớn bán đảo Ả-rập đã trở thành lãnh địa của người Islam, và chỉ trong vòng một thế kỷ sau khi Ông mất, Islam đã phát triển sang tận Tây Ban Nha ở phía tây và đến tận Trung Quốc ở phía đông. Một trong những lý do khiến Islam lan truyền nhanh và dễ dàng chính là sự thật và sự rõ ràng trong học thuyết của nó. Islam kêu gọi người ta chỉ tin duy nhất vào Thượng Đế, Người duy nhất đáng được thờ phụng.

Thiên Sứ Muhammad {{ $page->pbuh() }} là một ví dụ hoàn hảo về một con người thật thà, công bằng, nhân từ, luôn có lòng trắc ẩn, trung thực và dũng cảm. Mặc dù Ông chỉ là con người, nhưng Ông đã rũ bỏ được tất cả dục vọng và chỉ phấn đấu vì lợi ích của Thượng Đế và sự báo đáp của Ngài trong Tương lai. Thêm vào đó, trong mọi hành động và cách đối xử của mình, Ông luôn nghĩ đến và vâng lệnh Thượng Đế.
