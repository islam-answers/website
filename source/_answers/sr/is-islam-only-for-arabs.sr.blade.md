---
extends: _layouts.answer
section: content
title: Da li je islam samo za Arape?
date: 2024-10-11
description: Islam nije samo za Arape. Većina muslimana nisu Arapi, a islam je univerzalna
  vera za sve ljude.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=MSC01&articleID=MSC010003&articlePages=1
  title: spubs.com
translator_id: 584497
---

Najbrži način da se dokaže da je ovo potpuno netačno je da se navede činjenica da su samo oko 15% do 20% muslimana u svetu Arapi. Ima više indijskih muslimana nego arapskih muslimana, i više indonezijskih muslimana nego indijskih muslimana! Verovanje da je islam samo vera za Arape je mit koji su širili neprijatelji islama rano u njegovoj povesti. Ova pogrešna pretpostavka se verovatno temelji na činjenici da su većina prve generacije muslimana bili Arapi, da je Kur'an na arapskom i da je Poslanik Muhammed, {{ $page->pbuh() }}, bio Arap. Međutim, i učenja islama i povest njegovog širenja pokazuju da su rani muslimani uložili sve napore da svoju poruku Istine prošire svim nacijama, rasama i narodima.

Nadalje, treba pojasniti da nisu svi Arapi muslimani i da nisu svi muslimani Arapi. Arapin može biti musliman, kršćanin, Židov, ateist - ili bilo koje druge vere ili ideologije. Također, mnoge zemlje koje neki ljudi smatraju "arapskim" uopće nisu "arapske" -- poput Turske i Irana (Perzije). Ljudi koji žive u ovim zemljama govore jezike koji nisu arapski kao materinji jezici i različitog su etničkog nasleđa od Arapa.

Važno je shvatiti da su od samog početka misije poslanika Muhammeda {{ $page->pbuh() }}, njegovi sjedbenici dolazili iz širokog spektra pojedinaca -- tu je bio Bilal, afrički rob; Suhaib, vizantijski Rimljanin; Ibn Sailam, židovski rabin; i Salman, Perzijanac. Budući da je verska istina večna i nepromenjiva, a čovečanstvo jedno univerzalno bratstvo, islam uči da su objave Svemogućeg Boga čovečanstvu uvek bile dosljedne, jasne i univerzalne.

Istina islama je namenjena svim ljudima bez obzira na rasu, nacionalnost ili jezično poreklo. Pogled na muslimanski svet, od Nigerije do Bosne i od Malezije do Afganistana, dovoljan je dokaz da je islam univerzalna poruka za celo čovečanstvo --- da ne spominjemo činjenicu da značajan broj Europljana i Amerikanaca svih rasa i etničkog porijekla dolaze u islam.
