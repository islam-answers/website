---
extends: _layouts.answer
section: content
title: L'Islam tollera il razzismo?
date: 2024-08-21
description: Nell'Islam non sono importanti le differenze di colore della pelle, razza
  o estrazione sociale.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 573107
---

Tutti gli uomini sono discendenti di un uomo e di una donna, credenti e kaafir (non credenti), neri e bianchi, arabi e non arabi, ricchi e poveri, nobili e umili.

Nell'Islam non sono importanti le differenze di colore della pelle, razza o estrazione sociale. Tutti gli uomini discendono da Adamo, e Adamo fu creato dalla polvere. Nell'Islam, le differenze tra le persone si basano sulla fede (Iman) e sulla pietà (Taqwa), facendo ciò che Allah ha raccomandato ed evitando ciò che Allah ha proibito. Allah dice:

{{ $page->verse('49:13') }}

Il Profeta Mohammed {{ $page->pbuh() }} ha detto:

{{ $page->hadith('muslim:2564b') }}

L'Islam considera tutte le persone uguali per quanto riguarda i diritti e i doveri. Le persone sono uguali davanti alla legge (Shari'ah), come dice Allah:

{{ $page->verse('16:97') }}

La fede, la sincerità e la pietà conducono al Paradiso che è la ricompensa per chi mette in pratica questi valori, anche se si tratta di una persona estremamente debole o umile. Allah dice:

{{ $page->verse('..65:11') }}

Il kufr (miscredenza), l'arroganza e l'oppressione conducono all'Inferno anche se colui che commette questi peccati è una delle persone più ricche o blasonate. Allah dice:

{{ $page->verse('64:10') }}

Tra i Compagni del Profeta Mohammed {{ $page->pbuh() }} vi erano uomini musulmani di ogni colore, appartenenti a tutte le tribù e razze. I loro cuori erano colmi di Tawhid (monoteismo) ed erano uniti dalla loro fede e devozione. Tra questi, Abu Bakr della tribù dei Quraysh, 'Ali ibn Abi Taalib dei Bani Haashim, Bilal l'Etiope, Suhayb il Romano, Salman il Persiano. Vi erano uomini ricchi come 'Uthman e poveri come 'Ammar, persone benestanti e indigenti come Ahl al-Suffah e altri.

Credevano in Allah e per amor Suo si sforzavano di essere dei bravi musulmani, affinché Allah e il Suo Messaggero fossero soddisfatti di loro. Erano dei veri credenti.

{{ $page->verse('98:8') }}
