---
extends: _layouts.answer
section: content
title: ¿Quién puede convertirse en musulmán?
date: 2024-10-04
description: Cualquier ser humano puede convertirse en musulmán, independientemente
  de su religión pasada, edad, nacionalidad y origen étnico.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 597714
---

Cualquier ser humano puede convertirse en musulmán, independientemente de su religión pasada, edad, nacionalidad y origen étnico. Para convertirse en musulmán, basta con decir las siguientes palabras: "Ash hadu alla ilaha illa Allah, wa Ash hadu anna Mohammadan rasulAllah". Este testimonio de fe significa: "Atestiguo que no hay otro dios digno de ser adorado que Allah y que Muhammad es Su Mensajero". Debes decirlo y creer en ello.

Una vez que dices esta frase, automáticamente te conviertes en musulmán. No es necesario que preguntes por un momento o una hora, de noche o de día, para volverte a tu Señor y comenzar tu nueva vida. Cualquier momento es el momento para eso.

No necesitas el permiso de nadie, ni un sacerdote que te bautice, ni un intermediario que medie por ti, ni nadie que te guíe hacia Él, pues Él mismo, glorificado sea, te guía. Así que acude a Él, porque está cerca; está más cerca de ti de lo que piensas o puedes imaginar: 

{{ $page->verse('2:186') }}

Allah, glorificado y exaltado sea, dice:

{{ $page->verse('6:125..') }}
