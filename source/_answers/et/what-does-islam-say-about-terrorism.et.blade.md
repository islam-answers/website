---
extends: _layouts.answer
section: content
title: Mida ütleb islam terrorismi kohta?
date: 2024-06-08
description: Islam, religioon halastusest, ei luba terrorismi.
sources:
- href: https://www.islam-guide.com/ee/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

Islam, religioon halastusest, ei luba terrorismi. Jumal on Koraanis öelnud:

{{ $page->verse('60:8') }}

Prohvet Muhammed {{ $page->pbuh() }} keelas sõduritel tappa naisi ja lapsi, ja ta andis neile nõu:

{{ $page->hadith('tirmidhi:1408') }}

Ja ta ütles ka :

{{ $page->hadith('bukhari:3166') }}

Samuti keelas prohvet Muhammed {{ $page->pbuh() }} tulega karistamise .

Kord loetles ta mõrva olemaks teisel kohal peamistest pattudest, ja ta koguni hoiatas, et:,

{{ $page->hadith('bukhari:6533') }}

Moslemeid julgustatakse olema head loomade vastu ja neile mitte haiget tegema. Kord ütles prohvet Muhammed {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3318') }}

Ta ütles ka, et üks mees andis väga janusele koerale juua, nii et Jumal andestas tema patud selle teo eest. Prohvetilt küsiti: „Sõnumitooja, kas meile tasutakse loomade vastu üles näidatud headuse eest?” Ta vastas:

{{ $page->hadith('bukhari:2466') }}

Lisaks sellele, võttes loomalt elu toiduks, on moslemid kohustatud tegema seda viisil, mis põhjustab vähimal võimalikul määral hirmu ja kannatusi. Prohvet Muhammed {{ $page->pbuh() }} ütles:

{{ $page->hadith('tirmidhi:1409') }}

Selle ja teiste islamialaste tekstide valguses on islami ja moslemite seisukohalt kaitsetute tsiviilisikute südametes kabuhirmu õhutamine, ehitiste ja vara laialulatuslik hävitamine, süütute inimeste, naiste ja laste pommitamine ja sandistamine, kõik keelatud ja jälestusväärsed teod.

Moslemid järgivad rahu, halastuse ja andestuse religiooni ja suuremal enamusel ei ole mingit pistmist nende vägivaldsete sündmustega, mida mõned on moslemitega seostanud. Kui moslemist indiviid sooritab terrorismiakti, on see inimene süüdi islamiseaduste rikkumises.

Siiski on oluline teha vahet terrorismil ja legitiimsel vastupanul okupatsioonile, kuna need kaks on väga erinevad.
