---
extends: _layouts.answer
section: content
title: Bạn có cần chứng nhân để thực hiện Shahadah không?
date: 2024-08-09
description: Không cần thiết phải thực hiện shahadah (tuyên bố đức tin) trước những
  người làm chứng. Hồi giáo là vấn đề giữa một người và Thượng đế của người ấy.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 587785
---

Để một người trở thành người Hồi giáo, không nhất thiết phải tuyên bố đạo Hồi của mình trước bất cứ ai. Đạo Hồi là vấn đề giữa một người và Thượng đế của người ấy, nguyện cho Ngài được chúc phúc và tôn cao.

Nếu nguời ấy yêu cầu người làm chứng cho đạo Hồi của mình để có thể ghi vào trong các tài liệu cá nhân của mình, thì điều đó không có gì là sai trái, nhưng điều đó không nên được thực hiện như một điều kiện để đạo Hồi của người ấy được chấp nhận.
