---
extends: _layouts.answer
section: content
title: What does Islam say about the Day of Judgment?
date: 2024-03-11
description: A day will come when the whole universe will be destroyed and the dead
  will be resurrected for judgment by God.
sources:
- href: https://www.islam-guide.com/ch3-5.htm
  title: islam-guide.com
---

Like Christians, Muslims believe that the present life is only a trial preparation for the next realm of existence. This life is a test for each individual for the life after death. A day will come when the whole universe will be destroyed and the dead will be resurrected for judgment by God. This day will be the beginning of a life that will never end. This day is the Day of Judgment. On that day, all people will be rewarded by God according to their beliefs and deeds. Those who die while believing that “There is no true god but God, and Muhammad is the Messenger (Prophet) of God” and are Muslim will be rewarded on that day and will be admitted to Paradise forever, as God has said:

{{ $page->verse('2:82') }}

But those who die while not believing that “There is no true god but God, and Muhammad is the Messenger (Prophet) of God” or are not Muslim will lose Paradise forever and will be sent to Hellfire, as God has said:

{{ $page->verse('3:85') }}

And as He has said:

{{ $page->verse('3:91') }}

One may ask, ‘I think Islam is a good religion, but if I were to convert to Islam, my family, friends, and other people would persecute me and make fun of me. So if I do not convert to Islam, will I enter Paradise and be saved from Hellfire?’

The answer is what God has said in the preceding verse, “And whoever seeks a religion other than Islam, it will not be accepted from him and he will be one of the losers in the Hereafter.”

After having sent the Prophet Muhammad {{ $page->pbuh() }} to call people to Islam, God does not accept adherence to any religion other than Islam. God is our Creator and Sustainer. He created for us whatever is in the earth. All the blessings and good things we have are from Him. So after all this, when someone rejects belief in God, His Prophet Muhammad {{ $page->pbuh() }}, or His religion of Islam, it is just that he or she be punished in the Hereafter. Actually, the main purpose of our creation is to worship God alone and to obey Him, as God has said in the Holy Quran (51:56).

This life we live today is a very short life. The unbelievers on the Day of Judgment will think that the life they lived on earth was only a day or part of a day, as God has said:

{{ $page->verse('23:112-113..') }}

And He has said:

{{ $page->verse('23:115-116..') }}

The life in the Hereafter is a very real life. It is not only spiritual, but physical as well. We will live there with our souls and bodies.

In comparing this world with the Hereafter, the Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:2858') }}

The meaning is that, the value of this world compared to that of the Hereafter is like a few drops of water compared to the sea.
