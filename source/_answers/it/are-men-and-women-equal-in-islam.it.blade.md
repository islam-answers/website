---
extends: _layouts.answer
section: content
title: Uomini e donne sono uguali nell'Islam?
date: 2024-11-25
description: Nell'Islam, uomini e donne sono uguali religiosamente e spiritualmente, ma hanno ruoli complementari che garantiscono equilibrio e rispetto reciproco.
sources:
- href: https://islamqa.info/en/answers/12840/
  title: islamqa.info
- href: https://islamqa.org/hanafi/fatwacentre/179051/
  title: islamqa.org (FatwaCentre.org)
- href: https://www.islamweb.net/en/article/109364/
  title: islamweb.net
translator_id: 573107
---

L'Islam è arrivato per onorare le donne e per elevare il loro status, per riconoscere loro una posizione equa, per prendersi cura di loro e per proteggere la loro dignità. Per questi motivi l'Islam ordina ai tutori e ai mariti delle donne di spendere per loro, di trattarle bene, di prendersi cura e di essere gentili con loro. Allah dice nel Corano:

{{ $page->verse('..4:19..') }}

Si narra che il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('tirmidhi:3895') }}

L'Islam riconosce alle donne tutti i loro diritti e consente loro di gestire i propri affari in modo appropriato. Ciò include tutti i tipi di negoziazioni: acquisti, vendite, nomine di altre persone che agiscano per loro conto, prestiti, depositi terzi, ecc. Allah dice:

{{ $page->verse('..2:228') }}

L'Islam ha imposto alle donne gli atti di culto e i doveri a cui devono attenersi, gli stessi doveri degli uomini, ossia la purificazione, la Zakat (l'elemosina prescritta), il digiuno, la preghiera, l'Hajj (pellegrinaggio) e gli altri atti di adorazione.

Però, quando si tratta di eredità, l'Islam riconosce alla donna la metà della quota di un uomo perché lei non è obbligata a spendere per sé stessa, per la sua casa o per i suoi figli. Colui che invece è obbligato a spendere per loro è l'uomo.

Per quanto riguarda la testimonianza di due donne, che in alcuni casi equivale alla testimonianza di un uomo, ciò è dovuto al fatto che le donne tendono ad essere più distratte rispetto agli uomini a causa dei loro cicli naturali di mestruazioni, gravidanza, parto, educazione dei figli, ecc. Tutte queste cose le preoccupano e le rendono più disattente. Per questo, le prove shar'i indicano che è meglio rafforzare la testimonianza di una donna con quella di un'altra donna, in modo che sia più accurata. Ma ci sono questioni che riguardano solo le donne in cui la testimonianza di una sola donna è sufficiente, come ad esempio nel caso si tratti di determinare la frequenza con cui un bambino è stato allattato, i difetti che possono influenzare il matrimonio e così via.

Le donne sono uguali agli uomini per quel che riguarda la ricompensa, se rimangono salde nella fede e compiono opere giuste, godendo di una buona vita in questo mondo e di una enorme ricompensa nell'Aldilà. Allah dice:

{{ $page->verse('16:97') }}

Le donne hanno diritti e doveri, esattamente come gli uomini. Ci sono questioni che si addicono agli uomini e che Allah ha reso di competenza degli uomini, così come esistono questioni che si addicono alle donne e che Egli ha reso di competenza delle donne.
