<?php

namespace App\Commands;

use App\Helpers\FileHelper;
use App\Helpers\GitHelper;
use App\Helpers\JigsawHelper;
use App\Helpers\LokaliseHelper;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

#[AsCommand(
    name: 'lokalise:pull',
    description: 'Pull translations from Lokalise',
)]
class PullCommand extends Command
{
    private LokaliseHelper $lokalise;
    private GitHelper $git;
    private JigsawHelper $jigsaw;

    public function __construct()
    {
        $this->lokalise = new LokaliseHelper();
        $this->git = new GitHelper();
        $this->jigsaw = new JigsawHelper();

        parent::__construct();
    }

    function restoreTags(string $markdown, string $slug): string
    {
        $markdown = preg_replace('/\[%key:pbuh%]/', '{{ $page->pbuh() }}', $markdown);
        $markdown = preg_replace('/\[%key:' . $slug . '\.30\.verse:(.*?)%]/', '{{ $page->verse(\'$1\') }}', $markdown);
        $markdown = preg_replace('/\[%key:' . $slug . '\.40\.hadith:(.*?)%]/', '{{ $page->hadith(\'$1\') }}', $markdown);

        return $markdown;
    }

    function pickTask(Collection $tasks, QuestionHelper $helper, OutputInterface $output, InputInterface $input): array
    {
        $idPadding = "task-";

        $question = new ChoiceQuestion(
            'Please select a task',
            $tasks->mapWithKeys(fn ($task) => [$idPadding . $task['task_id'] => sprintf("%s (%s)", $task['title'], collect($task['languages'])->map(fn($task) => sprintf("%s: %s", $task['language_iso'], Carbon::createFromTimestamp($task['completed_at_timestamp'])->diffForHumans()))->join(', '))])->all(),
        );

        $question->setErrorMessage('Task %s is invalid.');
        $task = $helper->ask($input, $output, $question);

        return $tasks->firstWhere('task_id', substr($task, strlen($idPadding)));
    }

    function findSlug(string $keyname): string
    {
        return explode('.', $keyname)[0];
    }

    function generateMarkdown(Collection $translations): string
    {
        return $translations
            ->sort()
            ->map(function ($translation) {
                $value = $translation['translation'];
                $tags = $translation['tags'];

                if (!in_array('Content', $tags)) {
                    return null;
                }

                $markdown = $value;

                return $markdown;
        })
            ->filter()
            ->implode("\n\n");
    }

    function getSourcesForAnswer(string $slug): array
    {
        $answerPath = __DIR__ . "/../source/_answers/en/$slug.en.blade.md";

        $answerContent = FileHelper::read($answerPath) or throw new \Exception('Failed to English answer file to get sources');

        preg_match('/---(.*?)---/s', $answerContent, $matches);
        $metadata = $matches[1];
        $metadata = yaml_parse($metadata);

        return $metadata['sources'] ?? [];
    }

    function generateYaml(Collection $translations, int $translatorId, string $slug): string
    {
        $title = $translations->firstWhere(fn ($translation) => str_ends_with($translation['key_name'], "title"))['translation'];
        $description = $translations->firstWhere(fn ($translation) => str_ends_with($translation['key_name'], "summary"))['translation'];
        $sources = $this->getSourcesForAnswer($slug);
        $date = date('Y-m-d');

        $metadata = [
            'extends' => '_layouts.answer',
            'section' => 'content',
            'title' => $title,
            'date' => $date,
            'description' => $description,
            'sources' => $sources,
            'translator_id' => $translatorId,
        ];

        $yaml = yaml_emit($metadata, YAML_UTF8_ENCODING, YAML_LN_BREAK);
        $yaml = preg_replace("/\"$date\"/", $date, $yaml);

        return preg_replace('/\.\.\.$/', '---', $yaml);
    }

    function findSomething(Collection $translations, string $arrayName, string $tag, int $id): Collection
    {
        return $translations
            ->filter(fn ($translation) => in_array($arrayName, $translation['tags']))
            ->map(function ($translation) use ($id, $tag) {
                $keyName = $translation['key_name'];
                $value = $translation['translation'];

                preg_match("/^(.*?)\.$id\.$tag:(.*?)$/", $keyName, $matches);
                $reference = $matches[2];

                return [
                    'reference' => $reference,
                    'text' => $value,
                ];
            });
    }

    function findVerses(Collection $translations): Collection
    {
        return $this->findSomething($translations, 'Quran', 'verse', 30);
    }

    function findAhadith(Collection $translations): Collection
    {
        return $this->findSomething($translations, 'Hadith', 'hadith', 40);
    }

    function inject(string $directory, Collection $newItems, string $slug, string $languageCode): ?string
    {
        if ($newItems->isEmpty()) {
            return null;
        }

        $jsonPath = __DIR__ . "/../l10n/$directory/$slug.$languageCode.json";
        $preparedItems = $newItems->pluck('text', 'reference')->all();

        $jsonData = json_encode($preparedItems, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        if (FileHelper::write($jsonPath, $jsonData)) {
            return $jsonPath;
        } else {
            throw new \Exception('Failed to write to file');
        }
    }

    function injectVerses(Collection $newVerses, string $slug, string $languageCode): ?string
    {
        return $this->inject('verses', $newVerses, $slug, $languageCode);
    }

    function injectAhadith(Collection $newAhadith, string $slug, string $languageCode): ?string
    {
        return $this->inject('ahadith', $newAhadith, $slug, $languageCode);
    }

    function saveAnswer(string $markdown, string $yaml, string $slug, string $languageCode): string
    {
        $path = __DIR__ . "/../source/_answers/$languageCode/$slug.$languageCode.blade.md";

        if (FileHelper::write($path, $yaml . "\n" . $markdown)) {
            return $path;
        } else {
            throw new \Exception('Failed to write to file');
        }
    }

    function confirmChanges(OutputInterface $output, string $yaml, string $markdown, Collection $verses, Collection $ahadith, array $task, QuestionHelper $helper, InputInterface $input): bool
    {
        $output->writeln("Changes for task: {$task['title']}");

        $output->writeln("YAML:");
        $output->writeln($yaml);

        $output->writeln("Markdown:");
        $output->writeln($markdown);

        $output->writeln("Verses:");
        $output->writeln(json_encode($verses, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        $output->writeln("Ahadith:");
        $output->writeln(json_encode($ahadith, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

        $question = new ConfirmationQuestion('Do you want to proceed with these changes? (y/n)', false);

        return $helper->ask($input, $output, $question);
    }

    private function titleToSlug(string $title): string
    {
        return Str::slug($title, '-');
    }

    protected function configure(): void
    {
        $this
            ->addOption('lang', 'l', InputOption::VALUE_OPTIONAL, 'Filter by language')
            ->addOption('slug', 's', InputOption::VALUE_OPTIONAL, 'Slug of the answer to pull')
            ->addOption('non-interactive', null, InputOption::VALUE_NONE, 'Run in non-interactive mode');
    }

    protected function getTasks(string|null $filterLanguage): Collection
    {
        return $this->lokalise->getTasks(['filter_statuses' => 'completed', 'limit' => 1000])
            ->filter(fn ($task) =>
                $task['task_type'] === 'review' &&
                ($filterLanguage ? $task['languages'][0]['language_iso'] === $filterLanguage : true)
            )
            ->sort(fn ($a, $b) =>
                $a['languages'][0]['completed_at'] <=> $b['languages'][0]['completed_at']
            )
            ->reverse();
    }

    protected function handleTask(
        array $task,
        array $language,
        InputInterface $input,
        OutputInterface $output,
        QuestionHelper $helper,
        bool $nonInteractive,
    )
    {
        $slug = $this->titleToSlug($task['title']);
        $languageCode = $language['lang_iso'];
        $languageId = $language['lang_id'];

        $branchName = sprintf("%s-%s",
            $slug,
            $languageCode,
        );

        if (count($task['languages']) > 1) {
            $output->writeln("Tasks with more than one language are not supported yet.");
            return false;
        }

        $translationTask = $this->lokalise->getTask($task['parent_task_id']);
        $translatorId = $translationTask['languages'][0]['users'][0]['user_id'];

        $keyIds = collect($task['languages'][0]['keys']);
        $keyObjects = $this->lokalise->getKeys(query: [
            'filter_key_ids' => $keyIds->implode(','),
            'filter_platforms' => 'web',
            'disable_references' => true,
            'include_translations' => true,
            'filter_translation_lang_ids' => $languageId,
        ]);

        $translations = $keyObjects->pluck('translations')->flatten(1);

        // Add key names to translations
        $translations = $translations->map(function ($translation) use ($keyObjects) {
            $key = $keyObjects->firstWhere('key_id', $translation['key_id']);
            $translation['key_name'] = $key['key_name']['web'];
            $translation['tags'] = $key['tags'];

            return $translation;
        });

        $markdown = $this->generateMarkdown($translations);
        $markdown = $this->restoreTags($markdown, $slug);
        $markdown .= "\n";

        $yaml = $this->generateYaml($translations, $translatorId, $slug);

        $verses = $this->findVerses($translations);
        $ahadith = $this->findAhadith($translations);

        $confirmation = $nonInteractive || $this->confirmChanges($output, $yaml, $markdown, $verses, $ahadith, $task, $helper, $input);

        if (!$confirmation) {
            return true;
        }

        $answerPath = $this->saveAnswer($markdown, $yaml, $slug, $languageCode);

        $versesPath = $this->injectVerses($verses, $slug, $languageCode);
        $ahadithPath = $this->injectAhadith($ahadith, $slug, $languageCode);

        $confirmation = $nonInteractive || $helper->ask($input, $output, new ConfirmationQuestion('Do you want to commit and push these changes? (y/n)', false));

        if (!$confirmation) {
            return true;
        }

        $updatedFiles = array_filter([
            $answerPath,
            $versesPath,
            $ahadithPath,
        ]);

        $mergeRequestTitle = "Add $slug in $languageCode";
        $commitMessage = "Add $slug in $languageCode\n\nThis was committed using the `php go lokalise:pull` command.";

        $this->git
            ->switch($branchName, create: true)
            ->reset()
            ->add($updatedFiles)
            ->commit($commitMessage)
            ->push($mergeRequestTitle, $branchName)
            ->switch('main')
            ->pull();

        return true;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filterLanguage = $input->getOption('lang');
        $optionalSlug = $input->getOption('slug');
        $nonInteractive = $input->getOption('non-interactive');

        $languages = $this->lokalise->getLanguages();

        $this->git->fetch();
        $branches = $this->git->getBranches();
        $publishedAnswers = $this->jigsaw->getPublishedAnswers();

        $tasks = $this->getTasks($filterLanguage)
            ->filter(function ($task) use ($publishedAnswers, $branches) {
                $slug = $this->titleToSlug($task['title']);
                $languageCode = $task['languages'][0]['language_iso'];

                $branchName = sprintf("%s-%s",
                    $slug,
                    $languageCode,
                );

                // Skip if branch already exists
                $branchExists = collect($branches)->contains(fn ($branch) => str_starts_with($branch, $branchName));

                if ($branchExists) {
                    return false;
                }

                // Include if language is not published yet
                if (!$publishedAnswers->has($languageCode)) {
                    return true;
                }

                // Skip if answer already published
                if ($publishedAnswers->get($languageCode)->contains($slug)) {
                    return false;
                }

                return true;
            });

        if ($tasks->isEmpty()) {
            $output->writeln("Nothing to pull.");
            return Command::SUCCESS;
        }

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        if ($optionalSlug) {
            $task = $tasks->firstWhere(fn ($task) => $this->titleToSlug($task['title']) === $optionalSlug);

            if (!$task) {
                $output->writeln("Task with slug $optionalSlug not found or already handled.");
                return Command::FAILURE;
            }

            $languageCode = $task['languages'][0]['language_iso'];
            $language = $languages->firstWhere('lang_iso', $languageCode);

            return $this->handleTask($task, $language, $input, $output, $helper, $nonInteractive) ? Command::SUCCESS : Command::FAILURE;
        }

        if ($nonInteractive) {
            foreach ($tasks as $task) {
                $languageCode = $task['languages'][0]['language_iso'];
                $language = $languages->firstWhere('lang_iso', $languageCode);
                $output->writeln("Handling task: {$task['title']} in {$languageCode}.");
                $success = $this->handleTask($task, $language, $input, $output, $helper, $nonInteractive);
                $output->writeln($success ? "Task handled successfully." : "Task handling failed.");
            }

            return Command::SUCCESS;
        } else {
            $task = $this->pickTask($tasks, $helper, $output, $input);

            return $this->handleTask($task, $input, $output, $helper, $nonInteractive) ? Command::SUCCESS : Command::FAILURE;
        }
    }
}
