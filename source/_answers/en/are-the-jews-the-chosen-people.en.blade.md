---
extends: _layouts.answer
section: content
title: Are the Jews the chosen people?
date: 2024-12-15
description: The Jews were chosen for their Prophets but lost favor due to rejection,
  with the best nation becoming Prophet Muhammad’s followers.
sources:
- href: https://www.islamweb.net/en/fatwa/91584/
  title: islamweb.net
- href: https://islamqa.org/hanafi/fatwaa-dot-com/76468/
  title: islamqa.org (Fatwaa.com)
- href: https://islamqa.info/en/answers/9905/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/15096/
  title: islamweb.net
---

The Quran testifies that Allah had chosen the Children of Israel (Israel being the Prophet Ya‘qoob / Jacob {{ $page->pbuh() }}) at their time, and that He preferred them over all other people of their time. There are many verses in the Quran in this regard.

However, they were not chosen for their race or colour, but it was due to the great number of Prophets, may Allah exalt their mention, that were sent to them. Allah will always favour those who believe in Him and do good deeds, regardless of their colour, race or nationality. Allah says in the Quran:

{{ $page->verse('16:97') }}

Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:1842') }}

Allah says in the Quran:

{{ $page->verse('5:20') }}

Allah further says:

{{ $page->verse('44:32') }}

Yet, they were ungrateful for this bounty and denied what the Prophets brought to them. Thus, they deserved Allah's wrath and curse. They have been put under humiliation [by Allah] wherever they were overtaken, except for a covenant from Allah and a treaty from the people, and Allah made of them apes and pigs.

During the life of Prophet Muhammad {{ $page->pbuh() }}, the Jews always claimed that they were the chosen beloveds of Allah. Allah revealed the following verse in response to them:

{{ $page->verse('5:18..') }}

To conclude, the Jews were preferred over other people at the time of their Prophets as they sometimes obeyed them, but most of the time they belied them and even killed them. Allah says in the Quran:

{{ $page->verse('..2:87') }}

However, when Allah sent the Prophet Muhammad {{ $page->pbuh() }} as the last Messenger, Allah considered the best people those who followed him. Allah says in the Quran:

{{ $page->verse('3:110') }}

Allah further says:

{{ $page->verse('2:143..') }}

Since this was the situation, the Jews at the time of the Prophet Muhammad {{ $page->pbuh() }} were not better with him than they were with the previous Prophets, may Allah exalt their mention, rather they belied him, and harmed him and even plotted to kill him.
