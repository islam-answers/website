---
extends: _layouts.answer
section: content
title: Comment devient-on musulman(e) ?
date: 2024-06-08
description: 'C''est simplement en disant: “La ilaha illa Allah, Mohammadour rasoolou
  Allah” avec conviction qu''une personne se convertit à l''islam et devient musulmane.'
sources:
- href: https://www.islam-guide.com/fr/ch3-6.htm
  title: islam-guide.com
---

C'est simplement en disant: “La ilaha illa Allah, Mohammadour rasoolou Allah” avec conviction qu'une personne se convertit à l'islam et devient musulmane. Ces paroles signifient: "Il n'y a pas d'autre Dieu qu'Allah et Mohammed est Son messager". La première partie, "Il n'y a pas d'autre Dieu qu'Allah", signifie que personne ne mérite d'être adoré à part Dieu, et que Dieu n'a ni partenaire ni fils. Pour être musulman, on doit aussi:

- Croire que le Coran est la parole de Dieu, révélée par Lui.
- Croire que le Jour du Jugement (ou Jour de la Résurrection) est bien réel et qu'il viendra, ainsi que Dieu l'a promis dans le Coran.
- Accepter l'islam et en faire sa religion.
- Ne pas adorer quoi que ce soit ou qui que ce soit à part Dieu.

Le prophète Mohammed {{ $page->pbuh() }} a dit:

{{ $page->hadith('muslim:2747') }}
