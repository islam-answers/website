---
extends: _layouts.answer
section: content
title: Što je šerijatski zakon?
date: 2024-09-25
description: Šerijat se odnosi na cjelokupnu vjeru islam, koju je Allah odabrao za
  svoje robove, da ih izvede iz dubine tmine ka svjetlu.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 584497
---

Riječ šerijat se odnosi na cjelokupnu vjeru islam, koju je Allah odabrao za svoje robove, da ih izvede iz dubine tmine ka svjetlu. To je ono što im je On propisao i objasnio im naredbe i zabrane, halal i haram.

Onaj ko slijedi Allahov šerijat, smatrajući dozvoljenim ono što je On dozvolio i smatrajući zabranjenim ono što je On zabranio, postići će pobedu.

Onaj ko se protivi Allahovom šerijatu izlaže se Allahovom gnevu, srdžbi i kazni.

Allah, dž.š., kaže:

{{ $page->verse('45:18') }}

El-Halil ibn Ahmed, Allah mu se smilovao, rekao je:

> Riječ Šerijat (mn. šerai‘) odnosi se na ono što je Allah propisao (šera‘a) ljudima u vezi s pitanjima vjere, i onoga čega im je naredio da se pridržavaju od molitve, posta, hadža i tako dalje. To je šir'ah (mesto u reci gde se može piti).

Ibn Hazm, Allah mu se smilovao, rekao je:

> Šerijat je ono što je Allah, dž.š., propisao (šera'a) na usta Svoga Poslanika {{ $page->pbuh() }} u pogledu vere, i na usta poslanika  (Mir Božiji sa njima) koji su došli pre njega. Odluku o ukidajućem tekstu treba smatrati konačnom presudom.

### Jezičko poreklo pojma šerijat

Jezičko poreklo pojma šerijat odnosi se na mjesto na koje jahač može doći i piti vodu, te mesto u reci gde se može napiti. Allah, dž.š., kaže:

{{ $page->verse('42:13') }}
