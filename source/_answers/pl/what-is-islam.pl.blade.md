---
extends: _layouts.answer
section: content
title: Czym jest Islam?
date: 2024-07-23
description: Islam oznacza uległość, pokorę, posłuszeństwo nakazom i zakazom bez sprzeciwu,
  oddawanie czci samemu Allahowi i wiarę w Niego.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 583847
---

Jeśli odwołasz się do słowników języka arabskiego, przekonasz się, że znaczenie słowa islam to: poddanie się, uniżenie, posłuszeństwo nakazom i przestrzeganie zakazów bez sprzeciwu, szczere oddawanie czci samemu Allahowi, wiara w to, co On nam mówi i wiara w Niego.

Słowo islam stało się nazwą religii, którą przyniósł Prorok Muhammad {{ $page->pbuh() }}.

### Dlaczego ta religia nazywa się Islamem

Wszystkie religie na ziemi są nazywane różnymi imionami, albo imieniem konkretnego człowieka, albo konkretnego narodu. Tak więc Chrześcijaństwo wzięło swoją nazwę od Chrystusa; Buddyzm wziął swoją nazwę od swojego założyciela, Buddy; podobnie Judaizm wziął swoją nazwę od plemienia znanego jako Jehuda (Juda), więc stał się znany jako Judaizm. I tak dalej.

Z wyjątkiem Islamu, ponieważ nie jest on przypisywany żadnemu konkretnemu człowiekowi ani żadnemu konkretnemu narodowi, a raczej jego nazwa odnosi się do znaczenia słowa islam. Nazwa ta wskazuje, że ustanowienie i założenie tej religii nie było dziełem jednego konkretnego człowieka i że nie jest ona przeznaczona tylko dla jednego konkretnego narodu z wyłączeniem wszystkich innych. Jej celem jest raczej nadanie atrybutu zawartego w słowie islam wszystkim narodom ziemi. Tak więc każdy, kto nabywa ten atrybut, niezależnie od tego, czy pochodzi z przeszłości, czy z teraźniejszości, jest muzułmaninem, a każdy, kto nabędzie ten atrybut w przyszłości, również będzie muzułmaninem.

Islam jest religią wszystkich Proroków. Pojawił się na początku Proroctwa, od czasów naszego ojca Adama, a wszystkie przesłania wzywały do Islamu (poddania się Allahowi) pod względem wiary i podstawowych zasad. Wierzący, którzy podążali za wcześniejszymi prorokami, byli muzułmanami w sensie ogólnym i wejdą do Raju na mocy swojego Islamu.
