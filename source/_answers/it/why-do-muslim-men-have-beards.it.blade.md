---
extends: _layouts.answer
section: content
title: Perché gli uomini musulmani hanno la barba?
date: 2025-01-06
description: I musulmani fanno crescere la barba per seguire l'esempio di Muhammad
  (pace su di lui), mantenere la loro indole naturale e per distinguersi dai non credenti.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 573107
---

L'ultimo e il migliore dei Messaggeri, Muhammad {{ $page->pbuh() }}, si lasciò crescere la barba, come fecero i califfi che vennero dopo di lui, i suoi compagni, i capi e la gente comune tra i musulmani. Questa è la via dei Profeti e dei Messaggeri e dei loro seguaci, ed è parte della fitrah (predisposizione originaria) con cui Allah ha creato gli esseri umani. Aisha (che Allah sia soddisfatto di lei) narrò che il Messaggero di Allah {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:261a') }}

Quando Allah e il Suo Messaggero ordinano qualcosa, i credenti devono rispondere "Ascoltiamo e obbediamo", come Allah dice:

{{ $page->verse('24:51') }}

Gli uomini musulmani si fanno crescere la barba in obbedienza ad Allah e al Suo Messaggero. Il Profeta {{ $page->pbuh() }} ordinò ai musulmani di lasciar crescere liberamente la barba e di tagliarsi i baffi. È stato narrato da Ibn 'Umar che il Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn 'Abd al-Barr disse: "È proibito radersi la barba, e nessuno lo fa tranne gli uomini che sono effeminati", cioè coloro che imitano le donne. Jabir riferisce quanto segue a proposito del Profeta Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2344b') }}

Lo Shaykh al-Islam Ibn Taymiyah (che Allah abbia misericordia di lui) disse:

> Il Corano, la Sunnah e l'ijma' (consenso degli studiosi) indicano tutti che dobbiamo differenziarci dai miscredenti in tutti gli aspetti e non imitarli, perché imitarli esteriormente ci porterà ad imitarli anche nelle loro cattive azioni e abitudini, e perfino nelle loro credenze. (...)
