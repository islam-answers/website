---
title: Anonymous feedback
description: Send anonymous feedback to the Islam Answers team.
extends: _layouts.main
section: body
---

# Anonymous feedback

<iframe
    data-src="https://tally.so/embed/w2kVEe?alignLeft=1&hideTitle=1&transparentBackground=1"
    loading="lazy" width="100%" height="800"
    frameborder="0" marginheight="0" marginwidth="0"
    title="Feedback"
></iframe>

<noscript>
    <iframe
        src="https://tally.so/embed/w2kVEe?alignLeft=1&hideTitle=1&transparentBackground=1"
        loading="lazy" width="100%" height="800"
        frameborder="0" marginheight="0" marginwidth="0"
        title="Feedback"
    ></iframe>
</noscript>

<script async>
    window.addEventListener('load', (event) => {
    document.querySelectorAll('iframe').forEach(iframe => {
        iframe.src = iframe.dataset.src;
    });
    });
</script>
