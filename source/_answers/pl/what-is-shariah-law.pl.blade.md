---
extends: _layouts.answer
section: content
title: Czym jest prawo szariatu?
date: 2024-08-30
description: Szariat odnosi się do całej religii islamu, którą Allah wybrał dla swoich
  sług, aby wyprowadzić ich z głębin ciemności ku światłu.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 584496
---

Słowo Szariat odnosi się do całej religii Islamu, którą Allah wybrał dla swoich niewolników, aby wyprowadzić ich z głębin ciemności do światła. To jest to, co On im przepisał i wyjaśnił w kwestii nakazów i zakazów, halal i haram.

Ten, kto przestrzega szariatu Allaha, uważając za dozwolone to, na co On pozwala, a za zakazane to, co On zabrania, osiągnie zwycięstwo.

Ten, kto sprzeciwia się szariatowi Allaha, naraża się na gniew, oburzenie i karę Allaha.

Bóg, niech będzie wywyższony, mówi:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (niech Allah się nad nim zmiłuje) powiedział:

> Słowo Shari'ah (l.mn. sharai') odnosi się do tego, co Allah przepisał (shara'a) ludziom w kwestiach religii i do tego, co nakazał im przestrzegać w kwestii modlitwy, postu, hadżdżu itd. Jest to shir'ah (miejsce w rzece, gdzie można pić).

Ibn Hazm (niech Allah się nad nim zmiłuje) powiedział:

> Szariat jest tym, co Allah, niech będzie wywyższony, przepisał (shara'a) na ustach Swojego Proroka {{ $page->pbuh() }} w odniesieniu do religii, i na ustach Proroków (niech pokój będzie z nimi), którzy byli przed nim. Orzeczenie znoszącego tekstu należy uważać za ostateczne.

### Pochodzenie językowe terminu Szariat

Językowe pochodzenie terminu Shari'ah odnosi się do miejsca, w którym jeździec może przyjść i napić się wody, oraz miejsca w rzece, gdzie można pić. Allah, niech będzie wywyższony, mówi:

{{ $page->verse('42:13') }}
