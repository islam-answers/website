---
extends: _layouts.answer
section: content
title: Czy kobiety w islamie są uciśnione?
date: 2024-11-10
description: Islam promuje równouprawnienie kobiet, potępiając ucisk. Błędne interpretacje
  często wynikają z praktyk kulturowych, a nie z nauk islamu.
sources:
- href: https://www.islamweb.net/en/article/109366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 584496
---

Jednym z najważniejszych tematów zainteresowania niemuzułmanów jest status kobiet muzułmańskich i temat ich praw, a raczej postrzegany ich brak. Medialny portret kobiet muzułmańskich, zwykle przedstawiający ich „ucisk i tajemnicę”, wydaje się przyczyniać do tej negatywnej percepcji.

Głównym powodem jest to, że ludzie często nie potrafią odróżnić kultury od religii – dwóch rzeczy, które są zupełnie różne. W rzeczywistości islam potępia ucisk wszelkiego rodzaju, czy to wobec kobiety, czy wobec ludzkości w ogóle.

Koran to święta księga, według której żyją muzułmanie. Została objawiona 1400 lat temu człowiekowi o imieniu Muhammad {{ $page->pbuh() }}, który później stał się Prorokiem, niech Allah wywyższy jego imię. Minęło czternaście wieków, a ta księga nie została zmieniona, ani jedna litera nie została przekształcona.

W Koranie Allah, Wszechmogący, mówi:

{{ $page->verse('33:59') }}

Ten werset pokazuje, że islam wymaga noszenia hidżabu. Hidżab to słowo używane do zakrywania, nie tylko chust na głowę (jak niektórzy mogą sądzić), ale także noszenia luźnych ubrań, które nie są zbyt jaskrawe.

Czasami ludzie widzą zakryte muzułmanki i uważają to za ucisk. Jest to błędne myślenie. Muzułmanka nie jest uciskana, w rzeczywistości jest wyzwolona. Dzieje się tak, ponieważ nie jest już ceniona za coś materialnego, takiego jak jej dobry wygląd lub kształt ciała. Zmusza innych do oceniania jej za inteligencję, życzliwość, uczciwość i osobowość. Dlatego ludzie oceniają ją za to, kim naprawdę jest.

Kiedy muzułmanki zakrywają włosy i noszą luźne ubrania, wypełniają nakazy swojego Pana, aby być skromnymi, a nie kulturowe lub społeczne obyczaje. W rzeczywistości chrześcijańskie zakonnice zakrywają włosy ze skromności, ale nikt nie uważa ich za „uciśnione”. Postępując zgodnie z nakazem Allaha, muzułmanki robią dokładnie to samo.

Życie ludzi, którzy zareagowali na Koran, zmieniło się drastycznie. Koran wywarł ogromny wpływ na wielu ludzi, zwłaszcza na kobiety, ponieważ po raz pierwszy dusze mężczyzn i kobiet zostały uznane za równe — z tymi samymi obowiązkami i tymi samymi nagrodami.

Islam jest religią, która darzy kobiety wielkim szacunkiem. Dawno temu, gdy rodzili się chłopcy, przynosili rodzinie wielką radość. Narodziny dziewczynki witano z dużo mniejszą radością i entuzjazmem. Czasami dziewczynki były tak znienawidzone, że grzebano je żywcem. Islam zawsze sprzeciwiał się tej irracjonalnej dyskryminacji dziewcząt i dzieciobójstwu dziewcząt.

## Czy prawa kobiet w islamie zostały zaniedbane?

Jeśli chodzi o zmiany tych praw na przestrzeni wieków, podstawowe zasady nie uległy zmianie, ale jeśli chodzi o stosowanie tych zasad, nie ma wątpliwości, że w złotym wieku islamu muzułmanie bardziej stosowali szariat (prawo islamskie) swojego Pana, a orzeczenia tego szariatu obejmują honorowanie matki i traktowanie żony, córki, siostry i kobiet w ogóle w uprzejmy sposób. Im słabsze było zaangażowanie religijne, tym bardziej zaniedbywano te prawa, ale aż do Dnia Zmartwychwstania będzie nadal istniała grupa, która przestrzega swojej religii i stosuje szariat (prawa) swojego Pana. To są ludzie, którzy najbardziej szanują kobiety i przyznają im prawa.

Pomimo słabego zaangażowania religijnego wśród wielu muzułmanów w dzisiejszych czasach, kobiety nadal cieszą się wysokim statusem, czy to jako córki, żony czy siostry, podczas gdy przyznajemy, że istnieją niedociągnięcia, wykroczenia i zaniedbania praw kobiet wśród niektórych ludzi, ale każdy będzie odpowiadał za siebie.

Islam jest religią, która traktuje kobiety sprawiedliwie. Muzułmanka otrzymała rolę, obowiązki i prawa 1400 lat temu, których większość kobiet nie ma nawet dzisiaj na Zachodzie. Prawa te pochodzą od Boga i mają na celu utrzymanie równowagi w społeczeństwie; to, co może wydawać się „niesprawiedliwe” lub „brakujące” w jednym miejscu, jest rekompensowane lub wyjaśniane w innym miejscu.
