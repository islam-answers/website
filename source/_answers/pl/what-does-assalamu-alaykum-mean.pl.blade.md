---
extends: _layouts.answer
section: content
title: Co oznacza Assalamu Alaykum?
date: 2024-09-20
description: Assalamu alaykum oznacza „pokój z tobą” i jest muzułmańskim pozdrowieniem. Salam oznacza nieszkodliwość, bezpieczeństwo i ochronę przed złem.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
translator_id: 584496
---

Kiedy nadszedł islam, Allah nakazał, aby sposobem pozdrowienia wśród muzułmanów było Al-salamu 'alaykum i aby to pozdrowienie było używane tylko wśród muzułmanów.

Znaczenie słowa „salam” (dosłownie oznaczające „pokój”) to nieszkodliwość, bezpieczeństwo i ochrona przed złem i wadami. Imię al-Salam jest imieniem Allaha, niech będzie wywyższony, więc znaczenie pozdrowień salam, które są wymagane wśród muzułmanów, to: Niech błogosławieństwo Jego imienia zstąpi na ciebie. Użycie przyimka 'ala w 'alaykum (na ciebie) wskazuje, że pozdrowienie jest inkluzywne.

Najbardziej kompletną formą powitania jest powiedzenie: „As-Salamu `alaykum wa rahmatu-Allahi wa barakatuhu”, co oznacza „Pokój z tobą, miłosierdzie Allaha i Jego błogosławieństwa”.

Prorok {{ $page->pbuh() }} Uczynienie szerzenia Salam częścią wiary.

{{ $page->hadith('bukhari:12') }}

Dlatego Prorok {{ $page->pbuh() }} wyjaśnił, że dawanie Salam szerzy miłość i braterstwo. Wysłannik Allaha {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:54') }}

Prorok {{ $page->pbuh() }} nakazał nam odwzajemniać Salam i uczynił z tego prawo i obowiązek. Powiedział:

{{ $page->hadith('muslim:2162a') }}

Oczywiste jest, że zwracanie Salam jest obowiązkowe, ponieważ w ten sposób muzułmanin daje ci bezpieczeństwo, a ty musisz dać mu bezpieczeństwo w zamian. To tak, jakby mówił do ciebie: "Daję ci bezpieczeństwo i ochronę, więc musisz mu dać to samo, aby nie nabrał podejrzeń ani nie pomyślał, że ten, któremu dał salam, zdradza go lub ignoruje".
