---
extends: _layouts.answer
section: content
title: Co Islam mówi o Dniu Sądu Ostatecznego?
date: 2024-08-18
description: Nadejdzie dzień, w którym cały wszechświat zostanie zniszczony, a umarli
  zostaną wskrzeszeni na sąd Boży.
sources:
- href: https://www.islam-guide.com/ch3-5.htm
  title: islam-guide.com
translator_id: 584496
---

Podobnie jak chrześcijanie, muzułmanie wierzą, że obecne życie jest jedynie próbnym przygotowaniem do następnego królestwa egzystencji. To życie jest testem dla każdej jednostki na życie po śmierci. Nadejdzie dzień, w którym cały wszechświat zostanie zniszczony, a zmarli zostaną wskrzeszeni na sąd Boży. Ten dzień będzie początkiem życia, które nigdy się nie skończy. Ten dzień to Dzień Sądu Ostatecznego. W tym dniu wszyscy ludzie zostaną nagrodzeni przez Boga zgodnie ze swoimi przekonaniami i uczynkami. Ci, którzy umrą, wierząc, że „Nie ma prawdziwego Boga oprócz Boga, a Muhammad jest Posłańcem (Prorokiem) Boga” i są muzułmanami, zostaną nagrodzeni w tym dniu i zostaną przyjęci do Raju na zawsze, jak powiedział Bóg:

{{ $page->verse('2:82') }}

Ale ci, którzy umierają, nie wierząc, że „Nie ma prawdziwego boga oprócz Boga, a Muhammad jest Posłańcem (Prorokiem) Boga” lub nie są muzułmanami, utracą Raj na zawsze i zostaną wysłani do ognia piekielnego, jak powiedział Bóg:

{{ $page->verse('3:85') }}

I jak On powiedział:

{{ $page->verse('3:91') }}

Ktoś może zapytać: „Myślę, że islam jest dobrą religią, ale gdybym przeszedł na islam, moja rodzina, przyjaciele i inni ludzie prześladowaliby mnie i wyśmiewali się ze mnie. Jeśli więc nie przejdę na islam, to czy wejdę do raju i zostanę ocalony przed ogniem piekielnym?”.

Odpowiedzią jest to, co Bóg powiedział w poprzednim wersecie: „A od tego, kto poszukuje innej religii niż islam, nie będzie ona przyjęta; i on w życiu ostatecznym będzie w liczbie tych, którzy ponieśli stratę.”

Po wysłaniu Proroka Muhammada {{ $page->pbuh() }}, aby wezwał ludzi do islamu, Bóg nie akceptuje przynależności do żadnej religii innej niż islam. Bóg jest naszym Stwórcą i Tym, Który Cię podtrzymuje. On stworzył dla nas wszystko, co jest na ziemi. Wszystkie błogosławieństwa i dobre rzeczy, które mamy, pochodzą od Niego. Tak więc po tym wszystkim, jeśli ktoś odrzuca wiarę w Boga, Jego Proroka Muhammada {{ $page->pbuh() }} lub Jego religię — Islam, to tylko po to, aby został ukarany w życiu ostatecznym. W rzeczywistości głównym celem naszego stworzenia jest oddawanie czci samemu Bogu i posłuszeństwo Mu, jak Bóg powiedział w Świętym Koranie (51:56).

To życie, które przeżywamy dzisiaj, jest bardzo krótkie. Niewierzący w Dniu Sądu będą myśleć, że życie, które przeżyli na ziemi, było tylko dniem lub częścią dnia, jak powiedział Bóg:

{{ $page->verse('23:112-113..') }}

On powiedział:

{{ $page->verse('23:115-116..') }}

Życie w zaświatach jest bardzo realne. Jest ono nie tylko duchowe, ale także fizyczne. Będziemy tam żyć z naszymi duszami i ciałami.

Porównując ten świat z życiem pozagrobowym, Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:2858') }}

Znaczenie tego jest takie, że wartość tego świata w porównaniu z wartością życia ostatecznego jest jak kilka kropel wody w porównaniu z morzem.
