---
extends: _layouts.answer
section: content
title: 谁可以成为穆斯林？
date: 2025-02-05
description: 任何人都可以成为穆斯林，无论其之前的宗教、年龄、国籍或民族背景如何。
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 615298
---

任何人都可以成为穆斯林，无论其之前的宗教信仰、年龄、国籍或种族背景如何。要成为穆斯林，只需说以下的话：“Ash hadu alla ilaha illa Allah，wa ash hadu anna Mohammadan rasulAllah”。这个信仰证词的意思是：“我作证，除真主外，没有值得崇拜的神，穆罕默德是祂的使者”。你必须说出来并相信它。

一旦你说出这句话，你就自动成为穆斯林。你不需要询问何时或何时，无论是白天还是黑夜，来转向你的真主并开始你的新生活。任何时候都是合适的时间。

你不需要任何人的许可，不需要牧师为你洗礼或中间人为你调解或任何人引导你你接近祂，因为祂，愿祂得荣耀，会亲自引导你。所以转向祂，因祂就在你身边；祂比你想象或理解的更加接近你：

{{ $page->verse('2:186') }}

安拉说：

{{ $page->verse('6:125..') }}
