---
extends: _layouts.answer
section: content
title: Quem pode tornar-se muçulmano?
date: 2025-01-30
description: Qualquer ser humano pode tornar-se muçulmano, independentemente da sua
  religião anterior, idade, nacionalidade ou origem étnica.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 594172
---

Qualquer ser humano pode tornar-se muçulmano, independentemente da sua religião anterior, idade, nacionalidade ou origem étnica. Para se tornar muçulmano, basta dizer as seguintes palavras: "Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah". Este testemunho de fé significa: "Testemunho que não existe outro deus digno de ser adorado para além de Allah e que Mohammad é o Seu Mensageiro". É preciso dizê-lo e acreditar nele.

Quando dizes esta frase, tornas-te automaticamente muçulmano. Não precisas de perguntar por um tempo ou por uma hora, de noite ou de dia, para te voltares para o teu Senhor e começares a tua nova vida. Qualquer altura é a altura certa para isso.

Não precisas da permissão de ninguém, nem de um padre para te batizar, nem de um intermediário para mediar por ti, nem de ninguém para te guiar até Ele, pois Ele, glorificado seja, guia-te Ele mesmo. Por isso, voltai-vos para Ele, porque Ele está perto; está mais perto de vós do que pensais ou podeis imaginar:

{{ $page->verse('2:186') }}

Deus, exaltado seja, diz:

{{ $page->verse('6:125..') }}
