---
extends: _layouts.answer
section: content
title: Do Muslims believe in Virgin Mary?
date: 2024-12-05
description: Maryam is mentioned in the Quran as a person of faith. Muslims respect her and believe in her purity and innocence.
sources:
- href: https://islamqa.info/en/answers/241999/
  title: islamqa.info
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/32915/
  title: islamqa.org (SeekersGuidance.org)
---

Maryam (Mary), the daughter of ‘Imraan, may Allah be pleased with her, is one of the devoted worshippers and righteous women. She is mentioned in the Quran, where she is described as possessing the characteristics of the people of faith. She was a believer whose faith, affirmation of Allah’s Oneness and obedience to Him were perfect.

The Quran describes her as a true slave of Allah, who was humble and obedient to Allah, the Lord of the Worlds. Allah, may He be exalted, says:

{{ $page->verse('3:42-43') }}

### Maryam’s Innocence

Muslims respect Maryam and believe in what is mentioned in the Holy Quran about her, namely her purity and innocence of what the enemies of Allah accused her of:

{{ $page->verse('4:156') }}

In the Quran, there is an entire chapter named after Maryam, which beautifully conveys the story of the virgin birth. In the Islamic view, Maryam was a virgin; she had never been married before or during the time of the birth of Jesus {{ $page->pbuh() }}. Maryam was quite shocked when the angel informed her that she would bear a son, saying:

{{ $page->verse('19:20') }}

The Islamic sources do not report a betrothal for Maryam, nor do they mention Joseph, nor a later marriage, nor any siblings for Jesus {{ $page->pbuh() }}.

Among the Muslims, she is the perfect Muslim woman, one of the most virtuous of the women of Paradise, as the Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:2431') }}

In another narration, the Prophet {{ $page->pbuh() }} said:

{{ $page->hadith('bukhari:3432') }}

### The miraculous birth of Jesus - a sign to all mankind

Muslims believe that Allah made her and her son a sign to all mankind, and a testimony to the Oneness, Lordship and Might of Allah. Allah, may He be exalted, says:

{{ $page->verse('23:50..') }}

He also says:

{{ $page->verse('3:45') }}

Allah also warns those who take Jesus and his mother, Maryam, as deities, associating them with Allah:

{{ $page->verse('5:75..') }}

Allah, may He be glorified and exalted, has told us that on the Day of Resurrection the Prophet of Allah ‘Isa ibn Maryam (Jesus son of Mary) {{ $page->pbuh() }} will disavow the polytheism of those who associate him with Allah and exaggerate about him and his mother. Let us reflect upon that sublime exchange that will take place between the Lord of Glory and Majesty and His slave and Messenger ‘Isa ibn Maryam {{ $page->pbuh() }}. Allah, may He be exalted, says:

{{ $page->verse('5:116-117') }}
