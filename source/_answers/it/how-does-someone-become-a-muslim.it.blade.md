---
extends: _layouts.answer
section: content
title: Come si diventa musulmano?
date: 2024-06-01
description: Semplicemente dicendo con convinzione, “La ilaha illa Allah, Muhammadur
  rasoolu Allah,” e si diventa musulmano.
sources:
- href: https://www.islam-guide.com/it/ch3-6.htm
  title: islam-guide.com
---

Semplicemente dicendo con convinzione, “La ilaha illa Allah, Muhammadur rasoolu Allah,” e si diventa musulmano. Queste parole significano “Non esiste vero dio se non Dio (Allah), e Mohammed è il messaggero (Profeta) di Dio.” La prima parte, “Non esiste vero dio se non Dio,” significa che nessuno ha il diritto di essere adorato se non solo Dio ed Egli non ha compagni né figli. Per essere un musulmano una persona dovrebbe anche:

- Credere che il Sacro Corano è la parola letterale di Dio, da esso rivelata.
- Credere che il giorno del giudizio (il giorno della Resurrezione) esiste e arriverà, come Dio promise nel Corano.
- Accettare l'Islam e la sua religione.
- Non adorare nessuno all'infuori di Dio.

Il profeta Mohammed {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2747') }}
