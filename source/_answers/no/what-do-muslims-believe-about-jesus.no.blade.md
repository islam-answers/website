---
extends: _layouts.answer
section: content
title: Hva tror muslimene om Jesus?
date: 2024-06-25
description: Muslimene føler respekt og ærbødighet for Jesus. De anser ham for å være
  en av Guds største budbringere til menneskeheten.
sources:
- href: https://www.islam-guide.com/no/ch3-10.htm
  title: islam-guide.com
---

Muslimene føler respekt og ærbødighet for Jesus {{ $page->pbuh() }}. De anser ham for å være en av Guds største budbringere til menneskeheten. Koranen bekrefter hans jomfrufødsel, og et av kapitlene i Koranen har tittelen ”Maryam” (Maria). Koranen beskriver Jesu fødsel slik:

{{ $page->verse('3:45-47') }}

Jesu fødsel var et mirakel etter Guds befaling, den samme befaling som skapte Adam uten verken far eller mor. Gud har sagt:

{{ $page->verse('3:59') }}

I løpet av sin misjon som profet utførte Jesus mange mirakler. Gud forteller oss at Jesus sa:

{{ $page->verse('3:49..') }}

Muslimer tror at Jesus ikke ble korsfestet. Jesu fiender planla å korsfeste ham, men Gud reddet ham og løftet ham opp til Seg. Likheten med Jesus ble lagt på en annen mann. Fiender av Jesus fanget denne mannen og korsfestet ham, og de trodde at han var Jesus. Gud har sagt:

{{ $page->verse('..4:157..') }}

Verken Mohammad {{ $page->pbuh() }} eller Jesus kom for å forandre grunnlæren om troen på én Gud. De kom derimot for å bekrefte og fornye budskapet som var brakt av tidligere profeter.
