---
extends: _layouts.answer
section: content
title: Kes on prohvet Muhammed?
date: 2024-06-08
description: Prohvet Muhammed oli ideaalne näide ausast, õiglasest, heldest, kaastundlikust,
  tõde armastavast ja vaprast inimesest.
sources:
- href: https://www.islam-guide.com/ee/ch3-8.htm
  title: islam-guide.com
---

Muhammed {{ $page->pbuh() }} sündis Mekas aastal 570. Kuna tema isa suri enne tema sündi ja ema veidi pärast seda, kasvatas Muhammedi {{ $page->pbuh() }} üles tema onu, kes kuulus austatud Kuraiši hõimu. Ta oli kirjaoskamatu – ta ei olnud võimeline ei lugema ega kirjutama - ja ta jäi selleks kuni surmani. Enne tema prohvetlikku missiooni ei teadnud tema rahvas teadusest palju ja enamus neist olid kirjaoskamatud. Täiskasvanuks sirgudes sai ta tuntuks kui tõde armastav, aus, usaldusväärne, helde ja siiras mees. Ta oli nii usaldusväärne, et see sai tema hüüdnimeks Usaldusväärne. Muhammed {{ $page->pbuh() }} oli väga usklik ja ta oli kaua oma ühiskonna dekadentsi ja ebajumalakummardamist jälestanud.

Neljakümne aastaselt sai Muhammed {{ $page->pbuh() }} Ingel Gaabrieli kaudu oma esimese ilmutuse Jumalalt. Ilmutused jätkusid kahekümne kolme aasta jooksul ja ühiselt tuntakse neid Koraanina.

Niipea, kui ta hakkas Koraanist rääkima ja Jumala poolt talle ilmutatud tõde kuulutama, kannatas ta, nagu ka ta väike grupp järgijaid, uskmatute tagakiusamise all. Tagakiusamine muutus nii ägedaks, et aastal 622 andis Jumal neile käsu emigreeruda. See emigratsioon Mekast Mediina linna, umbes 420 km põhja poole, tähistab moslemite kalendri algust.

Mõne aasta pärast oli Muhammedil {{ $page->pbuh() }} ja tema järgijatel võimalik Mekasse tagasi pöörduda, kus nad oma vaenlastele andestasid. Enne, kui Muhammed {{ $page->pbuh() }} kuuekümne kolme aastaselt suri, oli suurem osa Araabia poolsaarest moslemite käes ja sajandi jooksul pärast tema surma oli islam levinud Hispaaniani läänes ja Hiinani idas. Muuhulgas olid islami kiire ja rahumeelse leviku põhjusteks selle õpetuse tõemeelsus ja selgus. Islam kutsub usku vaid ühte Jumalasse, kes on ainsana väärt teenimist.

Prohvet Muhammed {{ $page->pbuh() }} oli ideaalne näide ausast, õiglasest, heldest, kaastundlikust, tõde armastavast ja vaprast inimesest. Kuigi ta oli kõigest inimene, oli ta vaba kõikidest õelatest iseloomujoontest ja püüdles vaid Jumala ja Teispoolsuses Tema poolt antava tasu poole. Veelgi enam, kõikides oma tegudes ja tehingutes oli ta alati Jumala suhtes tähelepanelik ja jumalakartlik.
