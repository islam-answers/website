---
extends: _layouts.answer
section: content
title: Ce spune Islamul despre teoria evoluției?
date: 2024-03-23
description: Dumnezeu a creat prima ființă umană, Adam, în forma sa finală. În ceea
  ce privește creaturile vii, altele decât oamenii, sursele islamice sunt tăcute.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 554943
---

Dumnezeu a creat prima ființă umană, Adam, în forma sa finală - ceea ce opune teoria care spune că ființa umană ar fi evoluat treptat din maimuțe. Acest lucru nu poate fi confirmat sau negat direct de știință, deoarece acesta a fost un eveniment istoric unic – un miracol.

În ceea ce privește creaturile vii, altele decât oamenii, sursele islamice ne cer doar să credem că Dumnezeu le-a creat în orice fel a dorit El, fie că prin intermediul evoluție sau nu.
