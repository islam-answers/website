---
extends: _layouts.answer
section: content
title: Wer ist der Prophet Muhammad?
date: 2024-03-27
description: Der Prophet Muhammad (Friede sei mit ihm) war das vollkommene Beispiel
  eines zuverlässigen, gerechten, mitleidvollen, ehrlichen und mutigen Menschen.
sources:
- href: https://www.islam-guide.com/de/ch3-8.htm
  title: islam-guide.com
---

Muhammad {{ $page->pbuh() }} wurde im Jahr 570 in Makkah geboren. Weil sein Vater noch vor seiner Geburt starb und seine Mutter kurze Zeit später, wuchs er bei seinem Onkel auf, der dem einflussreichen Stamm der Quraisch angehörte. Er wuchs ungebildet auf, des Lesens und Schreibens unkundig, und blieb es bis zu seinem Tod. Vor seiner Berufung zum Propheten kannte sein Volk keine Wissenschaften und die meisten von ihnen waren Analphabeten. Als er heranwuchs, wurde er für seine Ehrlichkeit, Zuverlässigkeit, Vertrauenswürdigkeit, Großzügigkeit und Ernsthaftigkeit bekannt. Er war so vertrauenswürdig, dass sie ihn „den Vertrauenswürdigen“ nannten. Muhammad {{ $page->pbuh() }} war sehr religiös und der Verfall und der Götzendienst seiner Gesellschaft war ihm schon lange verhasst.

Mit vierzig Jahren erhielt Muhammad {{ $page->pbuh() }} seine erste Offenbarung von Gott durch den Engel Gabriel. Die Offenbarungen hielten über einen Zeitraum von dreiundzwanzig Jahren an und sie alle zusammen bilden den Quran.

Als er begann, den Quran öffentlich zu rezitieren und die Wahrheit, die Gott ihm offenbart hat, zu predigen, litten er und seine kleine Gruppe von Anhängern unter der Verfolgung durch die Ungläubigen. Diese Verfolgung wurde derart grausam, dass Gott ihnen im Jahr 622 den Befehl erteilte auszuwandern. Diese Emigration von Makkah in die Stadt Medinah, etwa 260 Meilen nördlich, kennzeichnet den Beginn des muslimischen Kalenders.

Nach einigen Jahren waren Muhammad {{ $page->pbuh() }} und seine Gefährten in der Lage, nach Makkah zurückzukehren, wo sie ihren Feinden vergaben. Bevor Muhammad {{ $page->pbuh() }} im Alter von dreiundsechzig Jahren starb, hatte fast die gesamte arabische Halbinsel den Islam angenommen und innerhalb eines Jahrhunderts nach seinem Tod war der Islam auch im Westen bis nach Spanien und im Osten bis China vorgedrungen. Einige Gründe für die schnelle friedliche Ausbreitung des Islam sind die Wahrheit und Klarheit seiner Lehren. Islam ruft dazu auf an den einen Gott zu glauben, der der Einzig Anbetungswürdige ist.

Der Prophet Muhammad {{ $page->pbuh() }} war das vollkommene Beispiel eines zuverlässigen, gerechten, barmherzigen, mitleidvollen, ehrlichen und mutigen Menschen. Obwohl er ein Mann war, war er weit entfernt von allen schlechten Eigenschaften und strebte einzig und allein für den Willen Gottes und seine Belohnung im Jenseits. Darüber hinaus war er immer bei allen Taten und Verhandlungen bewusst und gottesfürchtig.
