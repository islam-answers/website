---
extends: _layouts.answer
section: content
title: Cosa significa Subhanallah?
date: 2024-10-07
description: Solitamente tradotto come “Gloria ad Allah”, nel Corano appare per escludere
  qualsiasi carenza e difetto attribuiti ad Allah e per affermare la Sua perfezione.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 573107
---

Il termine arabo "tasbih" di solito si riferisce alla frase "SubhanAllah". È una frase che Allah ha adottato per Sé stesso, ha ispirato i Suoi angeli a pronunciare, e ha guidato i più eccellenti della Sua creazione a proclamare. "SubhanAllah" viene comunemente tradotto come "Gloria ad Allah", tuttavia questa traduzione non riesce a trasmettere appieno il significato del tasbih.

Il tasbih è composto da due parole: Subhana e Allah. Linguisticamente, il termine "subhana" deriva dalla parola "sabh", che significa distanza, lontananza. Di conseguenza, Ibn 'Abbas ha spiegato la frase come la purezza e l'assolutezza di Allah, al di sopra di ogni male o di ogni manchevolezza. In altre parole, Allah è eternamente al di sopra di ogni male, mancanza, carenza e inadeguatezza.

Questa frase compare nel Corano per escludere qualsiasi descrizione inappropriata e incompatibile attribuita ad Allah:

{{ $page->verse('23:91') }}

Un singolo tasbih è sufficiente a negare ogni difetto o menzogna attribuita ad Allah, in ogni luogo e in ogni tempo, da una sola creatura o da tutta la creazione. In tal modo, Allah confuta numerose menzogne a Lui attribuite con un singolo tasbih:

{{ $page->verse('37:149-159') }}

Lo scopo della negazione è quello di affermare il suo contrario. Ad esempio, quando il Corano nega l'ignoranza da parte di Allah, il musulmano nega a sua volta l'ignoranza, affermando l'opposto per Allah Che rappresenta la conoscenza onnicomprensiva, universale. Il tasbih è fondamentalmente una negazione (delle mancanze, degli errori e delle affermazioni false) e quindi, secondo questo principio, comporta l'affermazione del contrario, cioè della bellezza, del comportamento perfetto e della verità assoluta.

Sa'd bin Abu Waqqas (che Allah sia soddisfatto di lui) ha riferito:

{{ $page->hadith('muslim:2698') }}
