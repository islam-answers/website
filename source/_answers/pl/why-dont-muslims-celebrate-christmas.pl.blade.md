---
extends: _layouts.answer
section: content
title: Dlaczego muzułmanie nie obchodzą Bożego Narodzenia?
date: 2025-01-06
description: Muzułmanie czczą Jezusa jako wielkiego proroka, ale odrzucają jego boskość.
  Boże Narodzenie nie jest dozwolone w islamie.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 584496
---

Muzułmanie szanują i czczą Jezusa {{ $page->pbuh() }}, syna Maryam (Maryi) i oczekują jego drugiego przyjścia. Uważają go za jednego z największych posłańców Boga dla ludzkości. Wiara muzułmanina nie jest ważna, jeśli nie wierzy on we wszystkich Posłańców Allaha, w tym w Jezusa {{ $page->pbuh() }}.

Ale muzułmanie nie wierzą, że Jezus był Bogiem lub Synem Bożym. Muzułmanie nie wierzą również, że został ukrzyżowany. Allah posłał Jezusa do dzieci Izraela, aby wezwać je do wiary tylko w Allaha i czczenia tylko Jego. Allah wspierał Jezusa cudami, które dowodziły, że mówił prawdę.

Allah mówi w Koranie:

{{ $page->verse('19:88-92') }}

### Czy wolno obchodzić Boże Narodzenie?

Biorąc pod uwagę te różnice teologiczne, muzułmanom nie wolno wyodrębniać świąt innych religii dla żadnego z tych rytuałów lub zwyczajów. Raczej dzień ich świąt jest po prostu zwykłym dniem dla muzułmanów i nie powinni wyodrębniać go dla żadnej aktywności, która jest częścią tego, co niemuzułmanie robią w te dni.

Chrześcijańskie świętowanie Bożego Narodzenia to tradycja, która obejmuje wierzenia i praktyki nieosadzone w naukach islamu. W związku z tym muzułmanom nie wolno brać udziału w tych obchodach, ponieważ nie są one zgodne z islamskim rozumieniem Jezusa {{ $page->pbuh() }} ani jego nauk.

Oprócz tego, że jest innowacją, mieści się w kategorii naśladowania niewierzących w kwestiach, które są dla nich i ich religii wyjątkowe. Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('abudawud:4031') }}

Ibn `Uthaymin (niech Allah się nad nim zmiłuje) powiedział:

> Gratulacje dla niewiernych z okazji Bożego Narodzenia lub innych świąt religijnych są zakazane zgodnie z konsensusem uczonych, ponieważ oznacza to akceptację tego, co oni praktykują w niewierze i akceptację tego dla nich. Nawet jeśli nie akceptuje tej niewiary dla siebie, muzułmaninowi zabrania się akceptowania rytuałów niewiary lub gratulowania komuś innemu za nie. Muzułmanom nie wolno ich naśladować w żaden sposób, który jest charakterystyczny dla ich świąt, czy to w jedzeniu, ubraniach, kąpieli, rozpalaniu ognisk, czy powstrzymywaniu się od zwykłej pracy lub oddawania czci itd. I nie wolno im urządzać uczt, wymieniać prezentów ani sprzedawać rzeczy, które pomagają im obchodzić ich święta [...] ani ozdabiać się lub wieszać dekoracje.

W Encyklopedii Britannica czytamy:

> Słowo Boże Narodzenie pochodzi od staroangielskiego Cristes maesse, „Msza Chrystusa”. Nie ma pewnej tradycji co do daty narodzin Chrystusa. Chrześcijańscy chronografowie z III wieku wierzyli, że stworzenie świata miało miejsce w równonocy wiosennej, liczonej wówczas jako 25 marca; stąd nowe stworzenie w inkarnacji (tj. poczęciu) i śmierci Chrystusa musiało zatem nastąpić tego samego dnia, a jego narodziny nastąpiły dziewięć miesięcy później, w przesileniu zimowym, 25 grudnia.

> Powód, dla którego Boże Narodzenie zaczęto obchodzić 25 grudnia, pozostaje niepewny, ale najprawdopodobniej powodem jest to, że pierwsi chrześcijanie chcieli, aby data ta pokrywała się z pogańskim rzymskim świętem upamiętniającym „urodziny niezwyciężonego słońca” (natalis solis invicti); święto to obchodziło przesilenie zimowe, kiedy dni znów zaczynają się wydłużać, a słońce zaczyna wznosić się wyżej na niebie. Tradycyjne zwyczaje związane z Bożym Narodzeniem rozwinęły się z kilku źródeł w wyniku zbiegu okoliczności obchodów narodzin Chrystusa z pogańskimi obrzędami rolniczymi i słonecznymi w środku zimy. W świecie rzymskim Saturnalia (17 grudnia) były czasem wesołości i wymiany prezentów. 25 grudnia był również uważany za datę narodzin irańskiego boga tajemnic Mitry, Słońca Sprawiedliwości. W rzymski Nowy Rok (1 stycznia) domy były dekorowane zielenią i światłami, a prezenty były dawane dzieciom i biednym.

Więc jak każda racjonalna osoba może zobaczyć, nie ma solidnych podstaw dla Bożego Narodzenia, ani Jezus {{ $page->pbuh() }}, ani jego prawdziwi naśladowcy nie obchodzili Bożego Narodzenia, ani nie prosili nikogo o świętowanie Bożego Narodzenia, ani nie było żadnego zapisu o tym, aby ktokolwiek nazywał siebie chrześcijaninem obchodzącym Boże Narodzenie aż do kilkuset lat po Jezusie. Więc czy towarzysze Jezusa byli bardziej sprawiedliwie prowadzeni, nie obchodząc Bożego Narodzenia, czy też ludzie dzisiaj?

Więc jeśli chcesz szanować Jezusa {{ $page->pbuh() }} tak jak muzułmanie, nie świętuj jakiegoś wymyślonego wydarzenia, które zostało wybrane, aby zbiegało się z pogańskimi świętami i kopiować pogańskie zwyczaje. Czy naprawdę myślisz, że Bóg, a nawet sam Jezus, pochwaliliby lub potępili taką rzecz?
