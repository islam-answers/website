---
extends: _layouts.answer
section: content
title: Hva sier islam om dommedag?
date: 2024-06-25
description: En dag vil komme da hele universet skal bli ødelagt, og de døde vil gjenoppstå
  for å motta Guds dom.
sources:
- href: https://www.islam-guide.com/no/ch3-5.htm
  title: islam-guide.com
---

Som de kristne, tror muslimer at det nåværende liv bare er en prøvelse og en forberedelse til eksistensen i den kommende verden. Dette livet er en test for hvert enkelt individ, en test for livet etter døden. En dag vil komme da hele universet skal bli ødelagt, og de døde vil gjenoppstå for å motta Guds dom. Denne dagen vil være begynnelsen på et liv som aldri tar slutt. Denne dagen er dommedag, og da vil alle mennesker bli belønnet av Gud ut fra sin tro og sine handlinger. De som dør med troen om at ”Det finnes ingen annen sann guddom foruten Gud, og Mohammad er Guds budbringer (profet)”, og som er muslimer, vil bli belønnet på denne dag, og de får adgang til det evige paradis. Gud har sagt:

{{ $page->verse('2:82') }}

Men de som dør mens de fornekter at ”Det finnes ingen annen sann guddom foruten Gud, og Mohammad er Guds budbringer (profet)”, eller som ikke er muslimer, de vil miste paradiset for evig, og de vil bli sendt til helvetesilden. Gud har sagt:

{{ $page->verse('3:85') }}

Og Han har sagt:

{{ $page->verse('3:91') }}

Man kan spørre seg: ”Jeg synes islam er en bra religion, men hvis jeg skulle konvertere til islam, så ville både min familie, mine venner og andre folk forfølge meg og gjøre narr av meg. Så om jeg velger å ikke konvertere til islam, ville jeg da likevel få komme til paradiset og bli spart for helvetesilden?”

Svaret er hva Gud har sagt i det følgende verset: ”Hvis noen søker annet enn islam som religion, så vil dette ikke godtas fra ham, og i det kommende liv vil han være blant taperne.”

Etter å ha sendt profeten Mohammad {{ $page->pbuh() }} for å kalle folket til islam, så vil ikke Gud akseptere at man holder fast ved andre religioner enn islam. Gud er vår Skaper og Opprettholder. Han skapte for oss alt som er på jorden. Alt vi er velsignet med og har fått, det er fra Ham. Dersom noen etter alt dette, fornekter troen på Gud, Hans profet Mohammad {{ $page->pbuh() }} eller Hans religion som er islam, da er det rettferdig at han eller hun blir straffet i det hinsidige. Hovedhensikten med vår skapelse er å tilbe bare Gud alene og adlyde Ham, som Gud har sagt i den hellige Koranen (Koranen, 51:56).

Det livet som vi lever i dag er et meget kort liv. På dommedag vil de vantro synes at det livet de levde på jorden bare varte en dag eller en del av en dag. Gud har sagt:

{{ $page->verse('23:112-113..') }}

Og Han har sagt:

{{ $page->verse('23:115-116..') }}

Livet i det hinsidige er et virkelig liv. Det er ikke bare åndelig men også fysisk. Vi vil leve der med kropp og sjel.

I sammenligningen av dette livet med det hinsidige, har Mohammad {{ $page->pbuh() }} sagt:

{{ $page->hadith('muslim:2858') }}

Det vil si at verdien av denne verden sammenlignet med det hinsidige, er som noen dråper vann i forhold til et stort hav.
