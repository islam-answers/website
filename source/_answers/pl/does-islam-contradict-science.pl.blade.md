---
extends: _layouts.answer
section: content
title: Czy Islam przeczy nauce?
date: 2024-08-11
description: Koran zawiera fakty naukowe, które zostały odkryte dopiero niedawno dzięki
  postępowi technologicznemu i rozwojowi wiedzy naukowej.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 584496
---

Koran, księga Islamu, jest ostatnią księgą objawienia od Boga do ludzkości i ostatnią w linii objawień danej Prorokom.

Mimo że Koran (objawiony ponad 1400 lat temu) nie jest głównie księgą naukową, zawiera on fakty naukowe, które zostały odkryte dopiero niedawno dzięki postępowi technologicznemu i wiedzy naukowej. Islam zachęca do refleksji i badań naukowych, ponieważ zrozumienie natury stworzenia pozwala ludziom lepiej docenić swojego Stwórcę i zakres Jego mocy i mądrości.

Koran został objawiony w czasach, gdy nauka była prymitywna; nie było teleskopów, mikroskopów ani niczego, co by się zbliżało do dzisiejszej technologii. Ludzie wierzyli, że słońce krąży wokół ziemi, a niebo jest podtrzymywane przez filary na rogach płaskiej ziemi. Na tym tle został objawiony Koran, zawierający wiele faktów naukowych na tematy od astronomii do biologii, geologii do zoologii.

Niektóre z wielu naukowych faktów znalezionych w Koranie to:

### Fakt #1 - Początek życia

{{ $page->verse('21:30') }}

Woda jest wskazywana jako źródło wszelkiego życia. Wszystkie żywe organizmy składają się z komórek, a teraz wiemy, że komórki w większości składają się z wody. Odkryto to dopiero po wynalezieniu mikroskopu. Na pustyniach Arabii nie do pomyślenia byłoby, że ktoś mógłby przypuszczać, że całe życie pochodzi z wody.

### Fakt #2 - Rozwój embrionalny człowieka

Bóg mówi o etapach rozwoju embrionalnego człowieka:

{{ $page->verse('23:12-14') }}

Arabskie słowo „alaqah” ma trzy znaczenia: pijawka, zawieszona rzecz i skrzep krwi. „Mudghah” oznacza przeżuty materiał. Naukowcy zajmujący się embriologią zauważyli, że użycie tych terminów w opisie formowania się zarodka jest dokładne i zgodne z naszym obecnym naukowym rozumieniem procesu rozwoju.

Niewiele wiadomo było o etapach i klasyfikacji ludzkich embrionów aż do dwudziestego wieku, co oznacza, że opisy ludzkiego embrionu w Koranie nie mogą opierać się na wiedzy naukowej z siódmego wieku.

### Fakt #3 - Rozszerzanie się Wszechświata

W czasie, gdy nauka astronomii była jeszcze prymitywna, następujący werset w Koranie został objawiony:

{{ $page->verse('51:47') }}

Jednym z zamierzonych znaczeń powyższego wersetu jest to, że Bóg rozszerza wszechświat (tj. niebiosa). Inne znaczenia to, że Bóg dostarcza i ma władzę nad wszechświatem — co jest również prawdą.

Fakt, że wszechświat się rozszerza (np. planety oddzielają się od siebie) został odkryty w ostatnim wieku. Fizyk Stephen Hawking w swojej książce 'Krótka historia czasu' pisze: „Odkrycie, że wszechświat się rozszerza, było jednym z wielkich intelektualnych rewolucji XX wieku”.

Koran nawiązuje do ekspansji wszechświata jeszcze przed wynalezieniem teleskopu!

### Fakt #4 - Zesłane żelazo

Żelazo nie jest naturalne dla Ziemi, ponieważ przybyło na tę planetę z kosmosu. Naukowcy odkryli, że miliardy lat temu Ziemia została uderzona przez meteoryty, które niosły żelazo z odległych gwiazd, które eksplodowały.

{{ $page->verse('..57:25..') }}

Bóg używa słów "zesłany". Fakt, że żelazo zostało zesłane na ziemię z kosmosu, to coś, czego nie mogła poznać prymitywna nauka VII wieku.

### Fakt #5 - Ochrona nieba

Niebo odgrywa kluczową rolę w ochronie ziemi i jej mieszkańców przed śmiertelnymi promieniami słońca, a także mroźnym chłodem kosmosu.

Bóg prosi nas, abyśmy rozważyli niebo w następującym wersie:

{{ $page->verse('21:32') }}

Koran wskazuje na ochronę nieba jako znak Boga, właściwości ochronne, które zostały odkryte dzięki badaniom naukowym przeprowadzonym w XX wieku.

### Fakt #6 - Góry

Bóg zwraca naszą uwagę na istotną cechę gór:

{{ $page->verse('78:6-7') }}

Koran dokładnie opisuje głębokie korzenie gór, używając słowa „kołki”. Na przykład Mount Everest ma przybliżoną wysokość 9 km nad ziemią, podczas gdy jego korzeń jest głębszy niż 125 km!

Fakt, że góry mają głębokie 'kołkowate' korzenie, nie był znany aż do rozwoju teorii tektoniki płyt na początku XX wieku. Bóg mówi również w Koranie (16:15), że góry odgrywają rolę w stabilizacji ziemi „...aby się nie trzęsła”, co dopiero zaczęło być rozumiane przez naukowców.

### Fakt #7 - Orbita Słońca

W 1512 roku astronom Mikołaj Kopernik wysunął teorię, że Słońce jest nieruchome w centrum układu słonecznego, a planety krążą wokół niego. Ta teoria była powszechna wśród astronomów aż do XX wieku. Teraz jest już dobrze ugruntowanym faktem, że Słońce nie jest stacjonarne, ale porusza się po orbicie wokół centrum naszej galaktyki Drogi Mlecznej.

{{ $page->verse('21:33') }}

### Fakt #8 - Fale wewnętrzne w oceanie

Powszechnie uważano, że fale występują tylko na powierzchni oceanu. Jednak oceanografowie odkryli, że pod powierzchnią występują fale wewnętrzne, które są niewidoczne dla ludzkiego oka i można je wykryć tylko za pomocą specjalistycznego sprzętu.

Koran wspomina:

{{ $page->verse('24:40..') }}

Opis ten jest niesamowity, ponieważ 1400 lat temu nie istniał żaden specjalistyczny sprzęt umożliwiający odkrywanie fal wewnętrznych głęboko w oceanach.

### Fakt #9 - Kłamstwo i ruch

W czasach proroka Muhammada {{ $page->pbuh() }}, żył pewien okrutny i uciążliwy przywódca plemienny. Bóg objawił werset, aby go ostrzec:

{{ $page->verse('96:15-16') }}

Bóg nie nazywa tej osoby kłamcą, ale nazywa jego czoło (przednią część mózgu) 'kłamliwym' i 'grzesznym', i ostrzega go, aby przestał. Wiele badań wykazało, że przednia część naszego mózgu (płat czołowy) jest odpowiedzialna zarówno za kłamstwo, jak i ruchy dobrowolne, a więc i grzech. Te funkcje zostały odkryte dzięki sprzętowi do obrazowania medycznego, który został opracowany w XX wieku.

### Fakt #10 - Dwa morza, które się nie mieszają

Odnośnie do mórz, nasz Stwórca mówi:

{{ $page->verse('55:19-20') }}

Fizyczna siła zwana napięciem powierzchniowym zapobiega mieszaniu się wód sąsiednich mórz, ze względu na różnicę w gęstości tych wód. To tak, jakby między nimi była cienka ściana. Oceanografowie odkryli to dopiero niedawno.

### Czy Muhammad nie mógł być autorem Koranu?

Prorok Muhammad {{ $page->pbuh() }} był znany w historii jako analfabeta; nie umiał czytać ani pisać i nie był wykształcony w żadnej dziedzinie, która mogłaby tłumaczyć naukową precyzję Koranu.

Niektórzy mogą twierdzić, że skopiował on (Muhammad — Niech spłynie na Niego pokój i błogosławieństwo) to od uczonych ludzi lub naukowców swojego czasu. Gdyby to było skopiowane, spodziewalibyśmy się zobaczyć również skopiowane wszystkie błędne naukowe założenia tamtego czasu. Zamiast tego, znajdujemy, że Koran nie zawiera żadnych błędów — czy to naukowych, czy innych.

Niektórzy ludzie mogą również twierdzić, że Koran został zmieniony, gdy odkryto nowe fakty naukowe. Nie może to być prawdą, ponieważ jest to historycznie udokumentowany fakt, że Koran jest zachowany w swoim oryginalnym języku — co samo w sobie jest cudem.

### Tylko zbieg okoliczności?

{{ $page->verse('41:53') }}

Podczas gdy ta odpowiedź skupia się tylko na cudach naukowych, w Koranie wspomniano o wielu innych rodzajach cudów: cudach historycznych; proroctwach, które się spełniły; stylach językowych i literackich, którym nie można dorównać; nie mówiąc już o poruszającym wpływie, jaki ma na ludzi. Wszystkie te cuda nie mogą być wynikiem zbiegu okoliczności. Wyraźnie wskazują, że Koran pochodzi od Boga, Twórcy wszystkich tych praw nauki. On jest tym samym Bogiem, który wysłał wszystkich Proroków z tym samym przesłaniem — aby czcić tylko jednego Boga i przestrzegać nauk Jego Posłańca.

Koran to księga wskazówek, która pokazuje, że Bóg nie stworzył ludzi, aby po prostu błąkać się bez celu. Raczej uczy nas, że mamy sensowny i wyższy cel w życiu — uznać całkowitą doskonałość, wielkość i unikalność Boga oraz być Mu posłusznym.

Od każdego człowieka zależy, czy użyje swojego danego mu przez Boga intelektu i zdolności rozumowania, aby kontemplować i rozpoznawać znaki Boga – Koran jest najważniejszym znakiem. Przeczytaj i odkryj piękno i prawdę Koranu, abyś mógł osiągnąć sukces!
