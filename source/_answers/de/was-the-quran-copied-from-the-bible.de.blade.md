---
extends: _layouts.answer
section: content
title: Wurde der Quran von der Bibel abgeschrieben?
date: 2024-11-17
description: Der Quran wurde nicht von der Bibel abgeschrieben. Der Prophet Muhammad war Analphabet, es gab keine arabische Bibel, und die Offenbarungen kamen von Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 587746
---

Allah hat alles um uns herum mit einem Zweck erschaffen. Auch der Mensch wurde mit einem Ziel erschaffen, nämlich Allah anzubeten. Allah sagt im Quran:

{{ $page->verse('51:56') }}

Zur Führung der Menschheit sandte Allah im Laufe der Zeit Propheten. Diese Propheten erhielten göttliche Offenbarungen, und jede Religion besitzt ihre eigenen heiligen Schriften, die die Grundlage ihres Glaubens darstellen. Diese Schriften sind die niedergeschriebene Form der göttlichen Offenbarung: direkt, wie bei Prophet Musa (Moses), der die Gebote von Allah empfing, oder indirekt, wie bei Jesus (Isa) und Muhammad, {{ $page->pbuh() }}, die die Offenbarungen des Engels Gabriel (Jibril) an die Menschen weitergaben.

Der Quran weist alle Muslime an, an die vor ihm offenbarten Schriften zu glauben. Als Muslime glauben wir jedoch an die Tora (Tawrah), die Psalmen (Zaboor) und das Evangelium (Injeel) in ihrer ursprünglichen Form, das heißt, in der Weise, wie sie offenbart wurden, als das Wort Allahs.

## Könnte der Prophet Mohammed den Quran aus der Bibel übernommen haben?

Die Diskussion darüber, wie Prophet Muhammad, {{ $page->pbuh() }}, die Geschichten der „Leute der Schrift“ gelernt und dann im Quran weitergegeben haben könnte – was darauf hinweist, dass es keine Offenbarung war – beschäftigt sich mit verschiedenen möglichen Erklärungen. Eine Möglichkeit ist, dass er, {{ $page->pbuh() }}, diese Geschichten direkt aus den heiligen Schriften der „Leute der Schrift“ übernommen hat. Eine andere Möglichkeit ist, dass er die Geschichten, Gebote und Verbote mündlich von den „Leuten der Schrift“ gehört hat, falls es ihm nicht möglich war, ihre Bücher selbst zu lesen.

Diese Behauptungen lassen sich in drei grundlegenden Behauptungen zusammenfassen:

1. Die Behauptung, dass der Prophet Muhammad {{ $page->pbuh() }} weder ungebildet noch ungebildet war.
1. Die Behauptung, dass dem Propheten {{ $page->pbuh() }} die christlichen Schriften zur Verfügung standen und er daraus zitieren oder sie kopieren konnte
1. Die Behauptung, dass Mekka ein wichtiges Bildungszentrum für das Studium der Schriften war.

Erstens geht aus dem Text des Qurans und der Sunna an vielen Stellen eindeutig hervor, dass der Prophet Muhammad {{ $page->pbuh() }} in der Tat ungebildet war und dass er nie etwas von den Geschichten des Volkes des Buches erzählt hat, bevor die Offenbarung zu ihm kam, und er hat nie etwas mit eigener Hand geschrieben. Er wußte nichts von den Geschichten, bevor seine Mission begann, und er sprach auch nie vorher von ihnen.

Allah, möge Er erhaben sein, sagt:

{{ $page->verse('29:48') }}

In den Biografien und historischen Aufzeichnungen des Propheten wird nicht erwähnt, dass der Prophet, {{ $page->pbuh() }}, selbst die Offenbarung niederschrieb oder die Briefe an die Könige eigenhändig verfasste. Im Gegenteil, es wird berichtet, dass der Prophet Muhammad, {{ $page->pbuh() }}, Schreiber hatte, die die Offenbarungen und andere Dinge für ihn aufschrieben. In einem herausragenden Text von Ibn Khaldun wird das Bildungsniveau auf der Arabischen Halbinsel kurz vor Beginn der Prophetensendung beschrieben. Er sagt:

> Unter den Arabern war die Fähigkeit zu schreiben seltener als die Eier der Kameliden. Die meisten Menschen waren Analphabeten, vor allem die Wüstenbewohner, denn diese Fähigkeit ist normalerweise bei Stadtbewohnern zu finden.

Daher bezeichneten die Araber den Analphabeten nicht als Analphabeten; vielmehr würden sie jemanden, der lesen und schreiben konnte, als sachkundig beschreiben, denn lesen und schreiben zu können war die Ausnahme, nicht die Norm unter den Menschen.

Zweitens gibt es keinerlei Beweise, Hinweise oder Andeutungen, dass es damals eine arabische Übersetzung der Bibel gab. Al-Bukhari berichtete von Abu Hurayrah (möge Allah mit ihm zufrieden sein), dass dieser sagte:

{{ $page->hadith('bukhari:7362') }}

Dieser Hadith deutet darauf hin, dass die Juden ein vollständiges Monopol über den Text und seine Erklärung hatten. Wenn ihre Texte bereits auf Arabisch bekannt gewesen wären, hätte es keinen Bedarf gegeben, dass die Juden den Text vorlesen oder ihn den Muslimen erklären.

Diese Idee wird durch den Vers unterstützt, in dem Allah, möge Er erhaben sein, sagt:

{{ $page->verse('3:78') }}

Das vielleicht wichtigste Buch zu diesem Thema ist das Buch „The Bible in Translation“ von Bruce Metzger, Professor für neutestamentliche Sprache und Literatur. Darin sagt er:

> Es ist sehr wahrscheinlich, dass die älteste arabische Übersetzung der Bibel aus dem achten Jahrhundert stammt.

Der Orientalist Thomas Patrick Hughes schrieb:

> Es gibt keinen Beweis dafür, dass Muhammad die christlichen Schriften gelesen hat... Es muss angemerkt werden, dass es keine eindeutigen Beweise dafür gibt, dass eine arabische Übersetzung des Alten und Neuen Testaments vor der Zeit Mohammeds existierte.

Drittens ist die Vorstellung, dass der Prophet die Offenbarung von Menschen erhalten habe, eine alte Behauptung. Die Polytheisten warfen ihm das vor, und der Koran widerlegt diese Anschuldigung, wie Allah, erhaben sei Er, sagt:

{{ $page->verse('25:5-6') }}

Darüber hinaus ist die Behauptung, dass der Prophet Muhammad, {{ $page->pbuh() }}, Wissen von den „Leuten der Schrift“ erlangt habe, während er in Mekka war, eine Behauptung, die durch den akademischen und kulturellen Status Mekkas sowie die wahre Natur der Verbindung dieser arabischen Gesellschaft mit dem Wissen der „Leute der Schrift“ widerlegt wird.

Nachdem wir also bewiesen haben, dass der Prophet Muhammad {{ $page->pbuh() }} das Wissen der „Leute der Schrift“ weder direkt noch durch einen Vermittler von ihnen erhalten hat, bleibt uns nichts anderes übrig, als zu bestätigen, dass es sich um eine Offenbarung Allahs an seinen auserwählten Propheten handelt.
