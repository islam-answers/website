---
extends: _layouts.answer
section: content
title: Millest räägib Koraan?
date: 2024-06-08
description: Iga moslemi usu ja praktika peamine allikas.
sources:
- href: https://www.islam-guide.com/ee/ch3-7.htm
  title: islam-guide.com
---

Koraan, viimane ilmutatud Jumala sõna, on iga moslemi usu ja praktika peamine allikas. See tegeleb kõikide inimolendeid puudutavate teemadega: tarkus, õpetus, Jumala teenimine, äritehingud, seadused jne., kuid selle põhiteemaks on suhe Jumala ja Tema poolt loodu vahel. Samal ajal tagab Koraan juhtnöörid ja detailsed õpetused õiglase ühiskonna, inimese kõlbliku käitumise ja õigluspärase majandusliku süsteemi kohta.

Tuleb märkida, et Koraan ilmutati Muhammedile {{ $page->pbuh() }} just araabia keeles. Nii ei ole Koraani tõlge, kas siis inglise või mõnes muus keeles, ei Koraan ega ka Koraani versioon, vaid kõigest Koraani tähenduse tõlge. Koraan eksisteerib vaid araabia keeles, milles see ilmutati.
