---
extends: _layouts.answer
section: content
title: 古兰经的内容？
date: 2024-04-03
description: 每位穆斯林信仰及实践的主要来源。
sources:
- href: https://www.islam-guide.com/cs/ch3-7.htm
  title: islam-guide.com
---

古兰经是真主最后降示的真言，为每位穆斯林的信仰及试练的主要来源。 它处理有关人类的所有课题：智慧、信仰、崇拜、互动、法律等，但其基本课题则是真主与其子民之间的关系。 同时，它提供指引及详细教诲来求取公平的社会、正确的人类行为，以及平衡的经济系统。

请注意，古兰经仅以阿拉伯文降示给穆罕默德 {{ $page->pbuh() }}。 因此，以英文或其它任何语言所翻译的任何古兰经，都只是翻译古兰经的意义，而非古兰经或古兰经的版本。 只有存在降示的阿拉伯文古兰经。
