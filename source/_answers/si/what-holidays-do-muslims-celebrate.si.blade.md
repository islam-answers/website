---
extends: _layouts.answer
section: content
title: මුස්ලිම්වරුන් සමරන නිවාඩු දින මොනවාද?
date: 2024-07-31
description: මුස්ලිම්වරුන් සමරන්නේ ඊද් (උත්සව) දෙකක් පමණි, ඊද් අල්-ෆිතර් (රාමසාන්
  මාසය අවසානයේ) සහ ඊද් අල්-අදා, හජ් (වාර්ෂික වන්දනාව) අවසානයේ පමණි.
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 583844
---

මුස්ලිම්වරුන් සමරන්නේ ඊද් (උත්සව) දෙකක් පමණි: ඊද් අල්-ෆිතර් (රාමසාන් මාසය අවසානයේ) සහ ඊද් අල්-අදා, එය හජ් (වාර්ෂික වන්දනා කාලය) අවසන් කිරීම සනිටුහන් කරයි දුල්-හිජ්ජා මාසයේ 10 වන දින.

මෙම උත්සව දෙක තුළදී, මුස්ලිම්වරු එකිනෙකාට සුබ පතති, ඔවුන්ගේ ප්‍රජාව තුළ ප්‍රීතිය පතුරුවා හරිති, සහ ඔවුන්ගේ විශාල පවුල් සමඟ සමරති. නමුත් වඩා වැදගත් දෙය නම්, ඔවුන් අල්ලාහ්ගේ ආශීර්වාද සිහිපත් කර, ඔහුගේ නාමය සමරමින්, පල්ලියේ ඊද් යාඥාව පිරිනැමීමයි. මෙම අවස්ථා දෙක හැරුණු විට, මුස්ලිම්වරුන් වසරේ වෙනත් කිසිදු දිනයක් සමරන්නේ නැත.

ඇත්ත වශයෙන්ම, විවාහ උත්සව (වලිමා) හෝ දරුවෙකුගේ උපත (අඛීකා) වැනි සුදුසු සැමරුමක් සඳහා ඉස්ලාමය නියම කරන වෙනත් ප්‍රීතිමත් අවස්ථා තිබේ. කෙසේ වෙතත්, මෙම දින වසරේ විශේෂිත දින ලෙස සඳහන් කර නොමැත; ඒ වෙනුවට, ඒවා සමරනු ලබන්නේ මුස්ලිම්වරයෙකුගේ ජීවිතයේ සිදු වන පරිදි ය.

ඊද් අල්-ෆිතර් සහ ඊද් අල්-අදා යන දෙදිනම උදෑසන, මුස්ලිම්වරු මුස්ලිම් පල්ලියේ සාමූහිකව ඊද් යාච්ඤාවලට සහභාගී වන අතර, පසුව මුස්ලිම්වරුන්ට ඔවුන්ගේ යුතුකම් සහ වගකීම් මතක් කර දෙන ධර්ම දේශනයක් (කුත්බා ) පවත්වනු ලැබේ. යාච්ඤාවෙන් පසු මුස්ලිම්වරු "ඊද් මුබාරක්" (ආශීර්වාද ලත් ඊද්) යන වාක්‍ය ඛණ්ඩයෙන් එකිනෙකාට ආචාර කරන අතර තෑගි සහ රසකැවිලි හුවමාරු කර ගනිති.

## ඊද් අල්-ෆිතර්

ඊද් අල්-ෆිතර් යනු රාමසාන් උත්සවයේ අවසානය සනිටුහන් කරයි, එය චන්ද්‍ර පදනම් වූ ඉස්ලාමීය දින දර්ශනයේ නවවන මාසය තුළ සිදු වේ.

ඊද් අල්-ෆිතර් වැදගත් වන්නේ එය වඩාත් පූජනීය මාසවලින් එකක් වන රාමදාන් මාසය අනුගමනය කරන බැවිනි. රාමසාන් යනු මුස්ලිම්වරුන්ට අල්ලාහ් සමඟ ඇති බැඳීම ශක්තිමත් කිරීමටත්, කුරානය පාරායනය කිරීමටත්, යහපත් ක්‍රියාවන් වැඩි කිරීමටත් කාලයයි. රාමසාන් අවසානයේ, අල්ලාහ් මුස්ලිම්වරුන්ට ඊද් අල්-ෆිතර් දිනය ලබා දෙන්නේ උපවාසය සාර්ථකව නිම කිරීම සහ රාමසාන් මාසය තුළ නමස්කාර ක්‍රියාවන් වැඩි කිරීම සඳහා ත්‍යාගයක් ලෙස ය. ඊද් අල්-ෆිතර් දිනයේදී, මුස්ලිම්වරු අල්ලාහ්ට තවත් රාමසාන් උත්සවයක් දැකීමට, උන් වහන්සේ වෙත ළං වීමට, වඩා හොඳ මිනිසුන් වීමට සහ නිරාගින්නෙන් ගැලවීමට තවත් අවස්ථාවක් ලබා දීම ගැන දෙවියන්ට ස්තූති කරති.

{{ $page->hadith('ibnmajah:3925') }}

## ඊද් අල්-අදා

ඊද් අල්-අදා යනු චන්ද්‍ර පදනම් වූ ඉස්ලාමීය දින දර්ශනයේ දොළොස්වන සහ අවසාන මාසය වන දුල්-හිජ්ජා මාසයේ සිදු වන වාර්ෂික හජ් කාලය (මක්කම වන්දනාව) සම්පූර්ණ කිරීම සනිටුහන් කරයි.

ඊද් අල්-අදා සැමරුම යනු අනාගතවක්තෘ ඉබ්‍රාහිම් (ආබ්‍රහම්) අල්ලාහ් කෙරෙහි දක්වන භක්තිය සහ ඔහුගේ පුත් ඉස්මයිල් (ඉස්මායෙල්) බිලි දීමට ඔහු සූදානම් වීම සිහිපත් කිරීමයි. පූජා කරන අවස්ථාවේදීම අල්ලාහ් ඉස්මයිල් වෙනුවට බැටළුවෙකු තැබුවේය, එය ඔහුගේ පුත්‍රයා වෙනුවට මරා දැමීමට නියමිතව තිබුණි. අල්ලාහ්ගේ මෙම නියෝගය ප්‍රශ්නයකින් තොරව තම ස්වාමියාගේ අණට කීකරු වීමට අනාගතවක්තෘ ඊබ්‍රාහිම්ගේ කැමැත්ත සහ කැපවීම පරීක්ෂා කිරීමකි. එබැවින් ඊද් අල්-අදා යන්නෙහි තේරුම පූජාවේ උත්සවයයි.

ඊද් අල්-අදා දිනයේදී මුස්ලිම්වරුන්ට ඉටු කළ හැකි හොඳම ක්‍රියාවක් වන්නේ ඊද් සලාතයෙන් පසුව සිදු කරනු ලබන කුර්බානි/උහ්දියා (පූජා) ක්‍රියාවයි. ඉස්ලාම් භක්තිකයන් අල්ලාහ් වෙනුවෙන් ආබ්‍රහම්ගේ පරිත්‍යාගය සිහිපත් කිරීම සඳහා සතෙකු පූජා කරති. පූජා කරන සත්වයා බැටළුවෙකු, බැටළු පැටවෙකු, එළුවෙකු, ගවයෙකු හෝ ඔටුවෙකු විය යුතුය. හලාල්, ඉස්ලාමීය ආකාරයෙන් ඝාතනය කිරීම සඳහා සත්වයා හොඳ සෞඛ්‍ය සම්පන්න සහ නිශ්චිත වයසකට වඩා වැඩි විය යුතුය. පූජා කරන ලද සත්වයාගේ මස්, පූජාව කරන පුද්ගලයා, ඔවුන්ගේ මිතුරන් සහ පවුලේ අය සහ දුප්පතුන් සහ අසරණයන් අතර බෙදා හරිනු ලැබේ.

ඊද් අල්-අදා දින සතෙකු බිලි දීමෙන් පිළිබිඹු වන්නේ අනාගතවක්තෘ ආබ්‍රහම් තම පුත්‍රයා බිල්ලට දීමට ඇති සූදානම සහ එය අල් කුර්ආනයේ දක්නට ලැබෙන නමස්කාර ක්‍රියාවක් සහ මුහම්මද් නබිතුමාගේ {{ $page->pbuh() }} තහවුරු කළ සම්ප්‍රදායකි.

{{ $page->hadith('bukhari:5558') }}

අල් කුර්ආනයේ අල්ලාහ් මෙසේ පවසයි.

{{ $page->verse('2:196..') }}
