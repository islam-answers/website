---
extends: _layouts.answer
section: content
title: Cosa significa Assalamu Alaykum?
date: 2024-08-04
description: Assalamu alaykum significa “la pace sia con te” ed è un modo di salutare
  utilizzato tra musulmani.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
translator_id: 573107
---

Quando arrivò l'Islam, Allah prescrisse che il modo di salutarsi tra musulmani fosse Al-salamu 'alaykum, e che questo saluto dovesse essere usato solo tra i musulmani.

Il significato di “salam” (che letteralmente significa “pace”) è benessere, salvezza e protezione dal male e dalle mancanze. La parola al-Salam è uno dei nomi di Allah, che Egli sia lodato, quindi il significato del saluto "salam" previsto tra musulmani è: "Possa la benedizione del Suo Nome scendere su di voi". L'uso della preposizione 'ala in 'alaykum (su di te) indica che il saluto è comprensivo.

La forma di saluto più completa è "As-Salamu `alaykum wa rahmatu-Allahi wa barakatuhu", che significa "La pace sia su di voi e la misericordia di Allah e le Sue benedizioni".

Il Profeta {{ $page->pbuh() }} ha reso la diffusione del salam una parte della fede.

{{ $page->hadith('bukhari:12') }}

Pertanto, il Profeta {{ $page->pbuh() }} ha spiegato che porgere il salam trasmette amore e fratellanza. Il Messaggero di Allah {{ $page->pbuh() }} ha detto:

{{ $page->hadith('muslim:54') }}

Il Profeta {{ $page->pbuh() }} ci ha ordinato di ricambiare il salam rendendolo un diritto e un dovere. Egli ha detto:

{{ $page->hadith('muslim:2162a') }}

Risulta pertanto evidente che è obbligatorio restituire il saluto, perché così facendo un musulmano ti augura benessere e tu devi augurargli in cambio benessere. È come se ti stesse dicendo: "ti sto augurando sicurezza e protezione", quindi tu devi augurargli lo stesso in modo che non si insospettisca o pensi che colui a cui ha augurato salam lo stia ingannando o ignorando.
