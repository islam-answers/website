---
extends: _layouts.answer
section: content
title: Chi può diventare musulmano?
date: 2024-09-06
description: Ogni persona può diventare musulmana, indipendentemente dalla sua precedente
  religione, dall'età, dalla nazionalità o dall'origine etnica.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 573107
---

Ogni persona può diventare musulmana, indipendentemente dalla sua precedente religione, dall'età, dalla nazionalità o dall'origine etnica. Per diventare musulmani, è sufficiente pronunciare le parole seguenti: "Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah". Questa testimonianza di fede significa: "Testimonio che non esiste altro dio degno di essere adorato all'infuori di Allah e che Mohammed è il Suo Messaggero". E' obbligatorio dire questa testimonianza di fede e crederci.

Una volta pronunciata questa frase, si diventa automaticamente musulmani. Non è necessario dedicare un momento o un'ora precisi, di notte o di giorno, per rivolgersi al proprio Signore e iniziare così la nuova vita. Ogni momento è quello giusto per farlo.

Non avete bisogno del permesso di nessuno, né di un sacerdote che vi battezzi, né di una persona che faccia da intermediario per voi, né di qualcuno che vi guidi a Lui, perché è Lui stesso, che sia glorificato, a guidarvi. Quindi rivolgetevi a Lui, perché Lui è vicino; Lui è più vicino a voi di quanto pensiate o possiate immaginare:

{{ $page->verse('2:186') }}

Allah, l'Altissimo, dice:

{{ $page->verse('6:125..') }}
