---
extends: _layouts.answer
section: content
title: Quel est le statut de la femme en islam ?
date: 2024-06-04
description: Les femmes musulmanes ont le droit de posséder des biens, de gagner de
  l’argent et de le dépenser. L'Islam encourage les hommes à bien traiter le femmes.
sources:
- href: https://www.islam-guide.com/fr/ch3-13.htm
  title: islam-guide.com
---

L'islam perçoit la femme, qu'elle soit célibataire ou mariée, comme un être indépendant qui a le droit de disposer de ses biens et de ses revenus sans avoir à en rendre compte à qui que soit (que ce soit son père, son mari, ou quelqu'un d'autre). Elle a le droit d'acheter et de vendre, de donner des cadeaux et de donner en charité, et elle dépense de son argent comme bon lui semble. Lors du mariage, l'épouse reçoit, de la part de son mari, une dot dont elle dispose à sa guise, et elle garde son nom de famille plutôt que de prendre celui de son mari.

L'islam encourage le mari à bien traiter sa femme, car le prophète Mohammed {{ $page->pbuh() }} a dit:

{{ $page->hadith('ibnmajah:1977') }}

Les mères, en islam, sont traitées avec beaucoup d'égard. L'islam recommande de les traiter de la meilleure façon qui soit.

{{ $page->hadith('bukhari:5971') }}
