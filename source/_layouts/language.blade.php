@extends('_layouts.main')

@push('head')

@foreach($languages as $item)
    <link rel="alternate" hreflang="{{ $item->getLanguage() }}" href="{{ $item->getCanonicalUrl() }}" />
@endforeach

@endpush

@section('html'){!! 'itemscope itemtype="https://schema.org/FAQPage"' !!}@endsection

@section('body')
<nav class="text-xs mb-4 breadcrumbs" itemscope itemtype="https://schema.org/BreadcrumbList">
    <ol>
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <meta itemprop="item" content="{{ $page->baseUrl }}/" />
            <a href="{{ $page->baseUrl }}/" title="{{ $page->siteName }}" class="text-blue-600"><span itemprop="name">{{ $page->siteName }}</span></a>
            <meta itemprop="position" content="1" />
        </li>
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <meta itemprop="item" content="{{ $page->baseUrl }}/answers/" />
            <a href="{{ $page->baseUrl }}/answers/" title="{{ $page->getString('answers') }}" class="text-blue-600"><span itemprop="name">{{ $page->getString('answers') }}</span></a>
            <meta itemprop="position" content="2" />
        </li>
        <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
            <meta itemprop="item" content="{{ $page->getCanonicalUrl() }}" />
            <span class="text-gray-600" itemprop="name">{{ $page->title }}</span>
            <meta itemprop="position" content="3" />
        </li>
    </ol>
</nav>

<h1>{{ $page->title }}</h1>

<div class="text-2xl border-b border-blue-200 mb-6 pb-10">
    @yield('content')
</div>

<noscript>
    <style>
        #searchInput {
            display: none;
        }
    </style>
</noscript>
<input type="search" id="searchInput" placeholder="{{ $page->getString('search_placeholder') }}" class="w-full border border-gray-300 rounded p-2 mb-6">

<div id="answersContainer">
    @foreach ($page->answers($answers) as $answer)
    <div class="answer-preview">
        @include('_components.answer-preview-inline')
    </div>
    <div class="answer-divider">
        @if (! $loop->last)
        <hr class="w-full border-b mt-2 mb-6">
        @endif
    </div>
    @endforeach
</div>

<div id="noResultsMessage" class="hidden text-gray-600 text-center mt-6">
    {{ $page->getString('search_no_results') }}
</div>

<script>
const searchInput = document.getElementById('searchInput');
const answersContainer = document.getElementById('answersContainer');
const answerPreviews = answersContainer.getElementsByClassName('answer-preview');
const answerDividers = answersContainer.getElementsByClassName('answer-divider');
const noResultsMessage = document.getElementById('noResultsMessage');

const removeDiacritics = (str) => {
    return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
};

const updateSearchResults = (query) => {
    let hasResults = false;
    Array.from(answerPreviews).forEach((preview, index) => {
        const title = removeDiacritics(preview.querySelector('h2 a').textContent.toLowerCase());
        const excerpt = removeDiacritics(preview.querySelector('p').textContent.toLowerCase());
        const normalizedQuery = removeDiacritics(query);
        const matches = fuzzyMatch(title, normalizedQuery) || fuzzyMatch(excerpt, normalizedQuery);

        preview.style.display = matches ? '' : 'none';
        answerDividers[index].style.display = matches ? '' : 'none';
        if (matches) {
            highlightText(preview, query);
            hasResults = true;
        }
    });
    noResultsMessage.style.display = hasResults ? 'none' : 'block';
};

const fuzzyMatch = (text, query) => {
    const words = query.split(' ');
    return words.every(word => text.includes(word));
};

const highlightText = (preview, query) => {
    const titleElement = preview.querySelector('h2 a');
    const excerptElement = preview.querySelector('p');

    titleElement._originalContent = titleElement._originalContent || titleElement.textContent;
    excerptElement._originalContent = excerptElement._originalContent || excerptElement.textContent;

    titleElement.textContent = titleElement._originalContent;
    excerptElement.textContent = excerptElement._originalContent;

    titleElement.innerHTML = highlightMatch(titleElement.textContent, query);
    excerptElement.innerHTML = highlightMatch(excerptElement.textContent, query);
};

const highlightMatch = (text, query) => {
    if (!query.trim()) return text;

    const words = query.toLowerCase().split(' ').filter(word => word.length > 0);
    if (words.length === 0) return text;

    const normalized = removeDiacritics(text.toLowerCase());
    let result = text;
    let matches = [];

    words.forEach(word => {
        let searchText = normalized;
        let pos = 0;
        while ((pos = searchText.indexOf(removeDiacritics(word), pos)) !== -1) {
            const nextChar = searchText[pos + word.length] || ' ';
            const prevChar = pos > 0 ? searchText[pos - 1] : ' ';
            if (/\s/.test(prevChar) && /\s/.test(nextChar)) {
                const wordEnd = pos + word.length;
                let actualMatch = text.slice(pos, wordEnd);
                matches.push([pos, wordEnd, actualMatch]);
            }
            pos += word.length;
        }
    });

    matches.sort((a, b) => a[0] - b[0]);
    let mergedMatches = [];
    let current = null;

    matches.forEach(match => {
        if (!current) {
            current = match;
        } else if (match[0] <= current[1]) {
            current[1] = Math.max(current[1], match[1]);
        } else {
            mergedMatches.push(current);
            current = match;
        }
    });

    if (current) {
        mergedMatches.push(current);
    }

    for (let i = mergedMatches.length - 1; i >= 0; i--) {
        const [start, end] = mergedMatches[i];
        const actualMatch = text.slice(start, end);
        result = result.slice(0, start) +
                `<span class="bg-blue-200">${actualMatch}</span>` +
                result.slice(end);
    }

    return result;
};

const updateURL = (query) => {
    const url = new URL(window.location);
    url.searchParams.set('q', query);
    window.history.replaceState({query}, '', url);
};

searchInput.addEventListener('input', (event) => {
    const query = event.target.value.toLowerCase();
    updateSearchResults(query);
    updateURL(query);
});

window.addEventListener('load', () => {
    const query = new URLSearchParams(window.location.search).get('q');
    if (query) {
        searchInput.value = query;
        updateSearchResults(query.toLowerCase());
    }
});
</script>
@stop
