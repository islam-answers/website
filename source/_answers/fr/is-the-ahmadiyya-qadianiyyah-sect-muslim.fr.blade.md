---
extends: _layouts.answer
section: content
title: Les Ahmadis (Qadianis) sont-ils considérés comme musulmans ?
date: 2025-02-26
description: L'Ahmadiyya est un groupe égaré qui ne fait aucunement partie de l'islam.
  Ses croyances sont en totale contradiction avec les enseignements islamiques.
sources:
- href: https://islamqa.info/en/answers/170092/
  title: islamqa.info
- href: https://islamqa.info/en/answers/4060/
  title: islamqa.info
- href: https://islamqa.info/en/answers/45525/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/103015/
  title: islamqa.org (AskImam.org)
translator_id: 573111
---

L'Ahmadiyya (Qadianiyyah) est un groupe égaré qui ne fait aucunement partie de l'islam. Ses croyances sont en totale contradiction avec l'islam, et les musulmans doivent donc se méfier de leurs activités. En effet, les ‘ulama (érudits) de l’islam les ont déclarés kāfirs (non-croyants).

La Qadianiyyah est un mouvement apparu en 1900 de notre ère, résultant d’un complot des colonialistes britanniques dans le sous-continent indien. Son objectif était de détourner les musulmans de leur religion, et en particulier de l’obligation du djihad, afin qu’ils ne s’opposent pas au colonialisme au nom de l’islam. Le porte-parole de ce mouvement est le magazine Majallat Al-Adyaan (Magazine des religions), publié en anglais.

Mirza Ghulam Ahmad al-Qadiani a joué un rôle central dans la fondation de la Qadianiyyah. Il est né en 1839 dans le village de Qadian, au Pendjab, en Inde, au sein d’une famille connue pour sa trahison envers sa religion et son pays. Élevé dans la loyauté et l’obéissance aux colonialistes britanniques, il a été choisi pour se faire passer pour un prophète, afin de rassembler les musulmans autour de lui et de les détourner de la lutte pour le djihad contre l’occupation anglaise. En échange, le gouvernement britannique lui accordait de nombreux privilèges, renforçant ainsi la loyauté de son mouvement envers les autorités coloniales. Ghulam Ahmad était également connu de ses disciples pour son instabilité, ses nombreux problèmes de santé et sa dépendance aux drogues.

Ghulam Ahmad a d’abord commencé ses activités en tant que dâ‘iyah (prédicateur de l’islam) afin de rassembler des adeptes autour de lui. Il a ensuite prétendu être un mujaddid (rénovateur) inspiré par Allah, avant d’affirmer qu’il était le Mahdi attendu et le Messie promis. Il est allé encore plus loin en se proclamant prophète, prétendant que sa prophétie était supérieure à celle de Muhammad ({{ $page->pbuh() }}).

Les Qadianis croient qu’Allah jeûne, prie, dort, se réveille, écrit, commet des erreurs et aurait des relations sexuelles – exalté soit Allah, bien au-dessus de tout ce qu’ils prétendent. Ils affirment également que leur Dieu est anglais parce qu’il leur parle en anglais. Selon eux, un livre leur a été révélé, appelé Al-Kitâb al-Mubîn, qui est différent du Saint Coran. Ils ont prôné l’abolition du djihad et l’obéissance aveugle au gouvernement britannique, considérant que les Britanniques sont « ceux qui détiennent l’autorité », en se basant sur une interprétation erronée du Coran. De plus, ils autorisent la consommation d’alcool, d’opium, de drogues et d’autres substances intoxicantes.

Les érudits contemporains s’accordent unanimement à considérer les Qadianis comme étant en dehors de l’islam, en raison de croyances qui relèvent de la mécréance et contredisent les enseignements fondamentaux de la religion. Cette secte s’est opposée au consensus définitif des musulmans affirmant qu’il n’y a aucun prophète après notre Prophète Muhammad ({{ $page->pbuh() }}), comme l’attestent de nombreux textes du Coran et de la Sunna authentique.

La majorité des Qadianis vivent aujourd’hui en Inde et au Pakistan, tandis que certains se trouvent en Palestine occupée (« Israël ») et dans le monde arabe. Avec le soutien des colonialistes, ils cherchent à obtenir des postes stratégiques dans les sociétés où ils résident. Le gouvernement britannique appuie également ce mouvement, facilitant l’accès de ses adeptes à des fonctions au sein des gouvernements, des administrations d’entreprise et des consulats. Certains occupent même des postes de haut rang dans les services de renseignement. Pour propager leurs croyances, les Qadianis utilisent divers moyens, notamment l’éducation. Fortement instruits, ils comptent parmi eux de nombreux scientifiques, ingénieurs et médecins, ce qui leur permet d’influencer les sphères intellectuelles et professionnelles.

Il est interdit à un musulman d’accomplir la Salaah (prière) en Jama‘ah (assemblée) derrière un imam appartenant à la secte Ahmadi, car ils sont considérés comme des non-musulmans. De même, les musulmans doivent s’abstenir de fréquenter leurs lieux de culte et leurs rassemblements. Il n’est pas non plus permis à un musulman d’épouser l’un d’entre eux ou de donner sa fille en mariage à un adepte de cette secte, parce qu'ils sont considérés comme des non-croyants et des apostats.
