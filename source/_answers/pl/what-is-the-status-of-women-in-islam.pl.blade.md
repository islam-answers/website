---
extends: _layouts.answer
section: content
title: Jaki jest status kobiet w islamie?
date: 2024-10-13
description: Muzułmanki mają prawo do posiadania własności i wydawania pieniędzy według
  własnego uznania. Islam zachęca mężczyzn do dobrego traktowania kobiet.
sources:
- href: https://www.islam-guide.com/ch3-13.htm
  title: islam-guide.com
translator_id: 584496
---

Islam postrzega kobietę, niezależnie od tego, czy jest samotna, czy zamężna, jako samodzielną jednostkę, z prawem do posiadania i dysponowania swoją własnością i dochodami bez żadnej opieki nad nią (niezależnie od tego, czy jest to jej ojciec, mąż, czy ktokolwiek inny). Ma prawo kupować i sprzedawać, dawać prezenty i udzielać się charytatywnie, a także może wydawać swoje pieniądze według własnego uznania. Posag małżeński jest przekazywany przez pana młodego pannie młodej na jej własny użytek, a ona zachowuje własne nazwisko rodowe, a nie przyjmuje męża.

Islam zachęca męża do dobrego traktowania żony, jak powiedział Prorok Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('ibnmajah:1977') }}

Matki w islamie są bardzo szanowane. Islam zaleca traktowanie ich w najlepszy sposób.

{{ $page->hadith('bukhari:5971') }}
