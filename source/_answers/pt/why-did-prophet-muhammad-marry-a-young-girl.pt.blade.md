---
extends: _layouts.answer
section: content
title: Por que o profeta Muhammad se casou com uma menina?
date: 2025-01-04
description: O casamento do Profeta Muhammad com Aisha refletia normas do século VII,
  sem objeções na época; críticas atuais aplicam padrões modernos ao passado.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 594172
---

Aisha (que Allah esteja satisfeito com ela) tinha 6 anos quando se casou com o Profeta Muhammad {{ $page->pbuh() }} e tinha 9 anos quando o casamento foi consumado.

Infelizmente, vemos atualmente os esforços de outras religiões centrados principalmente na distorção e manipulação de factos históricos e etimológicos. Um desses temas tem sido a alegação do jovem casamento de Aisha com o Profeta Muhammad {{ $page->pbuh() }}. Os missionários tentam acusar o Profeta de ser um pedófilo, embora em termos politicamente corretos, devido ao facto de Aisha ter sido desposada aos 6 anos de idade e o casamento ter sido consumado 3 anos mais tarde - aos 9 anos - quando ela estava em plena puberdade. O lapso de tempo entre o noivado e a consumação de Aisha mostra claramente que os seus pais estavam à espera que ela atingisse a puberdade antes de o casamento ser consumado. Esta resposta procura refutar a alegação injusta contra o Profeta Muhammad {{ $page->pbuh() }}.

### Puberdade e casamento jovem na cultura semita

A alegação de abuso sexual de crianças contradiz o facto básico de que uma menina se torna uma mulher quando inicia o seu ciclo menstrual. O significado da menstruação, que qualquer pessoa com o mínimo de familiaridade com a fisiologia lhe dirá, é que é um sinal de que a menina está a ser preparada para ser mãe.

As mulheres atingem a puberdade em idades diferentes, entre os 8 e os 12 anos, consoante a genética, a raça e o ambiente. Até aos dez anos, há poucas diferenças no tamanho dos meninos e das meninas. O surto de crescimento na puberdade começa mais cedo nas meninas e dura mais tempo nos meninos. Os primeiros sinais de puberdade ocorrem por volta dos 9 ou 10 anos nas meninas, mas perto dos 12 anos nos meninos. Além disso, as mulheres de ambientes mais quentes atingem a puberdade muito mais cedo do que as de ambientes frios. A temperatura média do país ou da província é considerada o fator mais importante, não só no que diz respeito à menstruação, mas também no que se refere a todo o desenvolvimento sexual na puberdade.

O casamento nos primeiros anos da puberdade era aceitável na Arábia do século VII, tal como era a norma social em todas as culturas semíticas, desde os israelitas aos árabes e a todas as nações intermédias. De acordo com o Talmude, que os judeus consideram como a sua "Torá oral", o Sanhedrin 76b afirma claramente que é preferível que uma mulher se case quando tem a sua primeira menstruação, e em Ketuvot 6a há regras relativas a relações sexuais com meninas que ainda não menstruaram. Jim West, um ministro batista, observa a seguinte tradição dos israelitas:

- A esposa devia ser escolhida no seio do círculo familiar mais alargado (geralmente no início da puberdade ou por volta dos 13 anos de idade), a fim de manter a pureza da linhagem familiar.
- A puberdade sempre foi um símbolo da idade adulta ao longo da história.
- A puberdade é definida como a idade ou período em que uma pessoa é capaz de se reproduzir sexualmente pela primeira vez. Noutras épocas da história, um rito ou celebração deste acontecimento marcante fazia parte da cultura.

Os renomados sexólogos, R.E.L. Masters e Allan Edwards, em seu estudo sobre a expressão sexual afro-asiática, afirmam o seguinte:

> Hoje em dia, em muitas partes do Norte de África e do Médio Oriente, as meninas casam-se e vão para a cama entre os cinco e os nove anos de idade; e nenhuma mulher que se preze permanece solteira para além da idade da puberdade.

### Razões para casar com a Aisha

No que diz respeito à história do seu casamento, o Profeta {{ $page->pbuh() }} tinha sofrido com a morte da sua primeira mulher, Khadeejah, que o tinha apoiado e permanecido ao seu lado, e chamou ao ano em que ela morreu o Ano da Dor. Depois casou com Sawdah, que era uma mulher mais velha e não era muito bonita; casou-a para a consolar depois da morte do marido. Quatro anos mais tarde, o Profeta {{ $page->pbuh() }} casou com Aisha. As razões do casamento foram as seguintes:

1. Ele sonhou em casar-se com ela. Está provado pela narração de Aisha que o Profeta {{ $page->pbuh() }} lhe disse:

{{ $page->hadith('bukhari:3895') }}

2. As caraterísticas de inteligência e esperteza que o Profeta {{ $page->pbuh() }} tinha notado em Aisha desde criança, pelo que quis casar-se com ela para que fosse mais capaz do que os outros de transmitir relatos do que ele fazia e dizia. De facto, como já foi referido, ela foi um ponto de referência para os companheiros do Profeta {{ $page->pbuh() }} no que diz respeito aos seus assuntos e decisões.

3. O amor do Profeta {{ $page->pbuh() }} pelo seu pai Abu Bakr e a perseguição que Abu Bakr sofreu por causa do apelo ao Islam, que suportou com paciência. Foi a pessoa mais forte e sincera na fé, depois dos Profetas.

### Houve alguma objeção ao casamento deles?

A resposta a esta pergunta é não. Não há absolutamente nenhum registo de fontes históricas muçulmanas, seculares ou quaisquer outras que, mesmo implicitamente, mostrem outra coisa que não a alegria absoluta de todas as partes envolvidas neste casamento. Nabia Abbott descreve o casamento de Aisha com o Profeta {{ $page->pbuh() }} da seguinte forma:

> Em nenhuma das versões se comenta a disparidade de idades entre Mohammed e Aisha ou a tenra idade da noiva que, no máximo, não podia ter mais de dez anos e que ainda estava muito enamorada da sua jogo.

Até o conhecido orientalista crítico, W. Montgomery Watt, disse o seguinte sobre o carácter moral do Profeta {{ $page->pbuh() }}:

> Do ponto de vista do tempo de Mohammad, então, as alegações de traição e sensualidade não podem ser mantidas. Os seus contemporâneos não o consideraram moralmente defeituoso de forma alguma. Pelo contrário, alguns dos actos criticados pelo ocidental moderno mostram que os padrões de Mohammad eram mais elevados do que os do seu tempo.

Além do facto de ninguém lhe ter desagradado, nem às suas acções, ele foi um exemplo de carácter moral fundamental na sua sociedade e no seu tempo. Por conseguinte, julgar a sua moralidade com base nos padrões da nossa sociedade e cultura actuais não só é absurdo, como também injusto.

### O casamento na puberdade hoje

Os contemporâneos do Profeta (tanto inimigos como amigos) aceitaram claramente o casamento do Profeta {{ $page->pbuh() }} com Aisha sem qualquer problema. A prova deste facto é a ausência de críticas a este casamento até aos tempos modernos.

O que causou este facto foi uma mudança na cultura dos nossos dias, no que diz respeito à idade em que as pessoas normalmente se casam. Mas mesmo hoje, no século XXI, a idade de consentimento sexual ainda é bastante baixa em muitos sítios. Na Alemanha, Itália e Áustria, as pessoas podem ter relações sexuais legalmente aos 14 anos e nas Filipinas e em Angola podem ter relações sexuais legalmente aos 12 anos. A Califórnia foi o primeiro estado a alterar a idade de consentimento para 14 anos, o que aconteceu em 1889. Depois da Califórnia, outros estados juntaram-se a ela e aumentaram também a idade de consentimento.

### O Islam e a idade da puberdade

O Islam ensina claramente que a idade adulta começa quando uma pessoa atinge a puberdade. Allah diz no Alcorão:

{{ $page->verse('24:59..') }}

O início da puberdade nas mulheres dá-se com o início da menstruação, como Allah diz no Alcorão:

{{ $page->verse('65:4..') }}

Assim, faz parte do Islam reconhecer a chegada da puberdade como o início da idade adulta. É a altura em que a pessoa já amadureceu e está pronta para as responsabilidades de um adulto. Então, com que base é que alguém pode criticar o casamento de Aisha, uma vez que o seu casamento foi consumado depois de ela ter atingido a puberdade?

Se a alegação de "abuso de menores" fosse avançada contra o Profeta {{ $page->pbuh() }}, teríamos também de incluir todos os povos semitas que aceitavam o casamento na puberdade como norma.

Além disso, sabendo que o Profeta {{ $page->pbuh() }} não casou com nenhuma outra virgem além de Aisha e que todas as suas outras esposas tinham sido casadas anteriormente (e muitas delas eram velhas), isto refutará a noção, difundida por muitas fontes hostis, de que o motivo básico por detrás dos casamentos do Profeta era o desejo físico e o prazer das mulheres, porque se fosse essa a sua intenção, teria escolhido apenas as jovens.

### Conclusões

Vimos assim que:

- Era norma da sociedade semítica na Arábia do século VII permitir casamentos daqueles que haviam atingido a idade da puberdade.
- Não há relatos de oposição ao casamento do Profeta com Aisha, nem por parte dos seus amigos nem dos seus inimigos.
- Ainda hoje, há culturas que permitem o casamento de mulheres jovens na puberdade.

Apesar destes factos bem conhecidos, algumas pessoas continuam a acusar o Profeta {{ $page->pbuh() }} de imoralidade. No entanto, foi ele que trouxe justiça às mulheres da Arábia e as elevou a um nível nunca antes visto na sua sociedade, algo que as civilizações antigas nunca tinham feito às suas mulheres.

Por exemplo, quando se tornou Profeta, os pagãos da Arábia tinham herdado um desprezo pelas mulheres que tinha sido transmitido pelos seus vizinhos judeus e cristãos. Consideravam tão vergonhoso ser abençoado com uma criança do sexo feminino que chegavam ao ponto de a enterrar viva para evitar a desgraça associada às crianças do sexo feminino. Allah diz no Alcorão:

{{ $page->verse('16:58-59') }}

Muhammad {{ $page->pbuh() }} não só desencorajava e condenava severamente este ato, como também costumava ensinar as pessoas a respeitar e a acarinhar as suas filhas e mães como parceiras e fontes de salvação para os homens da sua família. Ele disse:

{{ $page->hadith('ibnmajah:3669') }}

Foi também narrado num hadith de Anas Ibn Malik que:

{{ $page->hadith('muslim:2631') }}

Por outras palavras, se alguém ama o Mensageiro de Allah {{ $page->pbuh() }} e deseja estar com ele no Dia da Ressurreição no Céu, então deve ser bom para as suas filhas. Este não é certamente o ato de um "molestador de crianças", como alguns gostariam de nos fazer crer.
