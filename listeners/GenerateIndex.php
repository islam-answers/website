<?php

namespace App\Listeners;

use TightenCo\Jigsaw\Jigsaw;

class GenerateIndex
{
    public function handle(Jigsaw $jigsaw)
    {
        $data = collect($jigsaw->getCollection('answers')->map(function ($answer) use ($jigsaw) {
            return [
                'title' => $answer->title,
                'date' => $answer->getDate()->format('Y-m-d'),
                'language' => $answer->getLanguage(),
                'canonical' => $answer->getCanonicalUrl(),
            ];
        })->values());

        file_put_contents($jigsaw->getDestinationPath() . '/index.json', json_encode($data));
    }
}
