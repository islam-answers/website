---
extends: _layouts.answer
section: content
title: Wer kann Muslim werden?
date: 2024-11-03
description: Jeder Mensch kann Muslim werden, unabhängig von seiner bisherigen Religion,
  seinem Alter, seiner Nationalität oder seiner ethnischen Herkunft.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 587746
---

Jeder Mensch kann Muslim werden, unabhängig von seiner bisherigen Religion, seinem Alter, seiner Nationalität oder ethnischen Herkunft. Um Muslim zu werden, spricht man einfach die folgenden Worte: „Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah“. Dieses Glaubensbekenntnis bedeutet: „Ich bezeuge, dass es außer Allah keinen Gott gibt, der der Anbetung würdig ist, und dass Mohammed sein Gesandter ist.“ Man muss diese Worte aus Überzeugung sprechen und tief in seinem Herzen daran glauben.

Sobald du diesen Satz sprichst, wirst du automatisch Muslim. Du musst nicht auf eine bestimmte Zeit oder Stunde, auf Tag oder Nacht warten, um dich deinem Herrn zuzuwenden und dein neues Leben zu beginnen. Jede Zeit ist die richtige Zeit dafür.

Du brauchst weder die Erlaubnis von irgendjemandem, noch einen Priester, der dich tauft, noch einen Vermittler, der für dich vermittelt, noch jemanden, der dich zu Ihm führt, denn Er, gepriesen sei Er, führt dich zu Ihm. Wende dich einfach Ihm zu, denn Er ist dir nah – näher, als du es dir vorstellen kannst.

{{ $page->verse('2:186') }}

Allah, gepriesen sei Er, sagt:

{{ $page->verse('6:125..') }}
