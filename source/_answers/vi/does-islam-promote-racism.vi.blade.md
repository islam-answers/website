---
extends: _layouts.answer
section: content
title: Hồi giáo có cổ xúy cho chủ nghĩa phân biệt chủng tộc không?
date: 2024-09-11
description: Hồi giáo không chú ý đến sự khác biệt về màu da, chủng tộc hoặc dòng
  dõi.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 587785
---

Tất cả mọi người đều là con cháu của một người đàn ông và một người phụ nữ, người có đức tin và người kaafir (người không có đức tin), người da đen và da trắng, người Ả Rập và người không phải Ả Rập, người giàu và người nghèo, người cao quý và người thấp hèn.

Hồi giáo không chú ý đến sự khác biệt về màu da, chủng tộc hay dòng dõi. Tất cả mọi người đều xuất phát từ Adam, và Adam được tạo ra từ bụi. Trong Hồi giáo, sự phân biệt giữa mọi người dựa trên đức tin (Iman) và lòng mộ đạo (Taqwa), bằng cách làm những gì Allah đã ra lệnh và tránh những gì Allah đã cấm. Allah Phán:

{{ $page->verse('49:13') }}

Nhà tiên tri Muhammad {{ $page->pbuh() }} đã nói:

{{ $page->hadith('muslim:2564b') }}

Hồi giáo coi mọi người đều bình đẳng về quyền lợi và nghĩa vụ. Mọi người đều bình đẳng trước pháp luật (Shari'ah), như Allah đã phán:

{{ $page->verse('16:97') }}

Đức tin, sự trung thực và lòng đạo đức đều dẫn đến Thiên đường, đó là quyền của người có được những phẩm chất này, ngay cả khi người ấy là một trong những người yếu đuối hoặc thấp kém nhất. Allah phán:

{{ $page->verse('..65:11') }}

Kufr (người vô tín), sự kiêu ngạo và sự áp bức đều dẫn đến Địa ngục, ngay cả khi người làm những điều này là một trong những người giàu có hoặc cao quý nhất. Allah phán:

{{ $page->verse('64:10') }}

Các cố vấn của Tiên tri Muhammad {{ $page->pbuh() }} bao gồm những người đàn ông Hồi giáo thuộc mọi bộ tộc, chủng tộc và màu da. Họ toàn tâm với Tawhid (độc thần giáo) và họ đến được với nhau nhờ đức tin và lòng mộ đạo của họ – chẳng hạn như Abu Bakr từ Quraysh, 'Ali ibn Abi Taalib từ Bani Haashim, Bilal người Ethiopia, Suhayb người La Mã, Salman người Ba Tư, những người đàn ông giàu có như 'Uthman và những người đàn ông nghèo như 'Ammar, những người giàu có và những người nghèo như Ahl al-Suffah, và những người khác.

Họ tin vào Allah và chiến đấu vì Ngài cho đến khi Allah và Sứ giả của Ngài hài lòng về họ. Họ là những tín đồ thực sự.

{{ $page->verse('98:8') }}
