---
extends: _layouts.answer
section: content
title: De ce nu li se permite musulmanilor să aibă întâlniri?
date: 2024-08-11
description: Coranul interzice să ai iubite sau iubiți. Islamul păstrează relațiile
  dintre indivizi într-un mod care se potrivește naturii umane.
sources:
- href: https://islamqa.org/shafii/seekersguidance-shafii/241530
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107140
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107560
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/shafii/qibla-shafii/33045
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/askimam/17968
  title: islamqa.org (AskImam.org)
- href: https://www.islamweb.net/en/fatwa/89542
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/82217
  title: islamweb.net
- href: https://islamqa.info/en/answers/20949
  title: islamqa.info
- href: https://islamqa.info/en/answers/33702
  title: islamqa.info
- href: https://islamqa.info/en/answers/1200
  title: islamqa.info
- href: https://islamqa.info/en/answers/21933
  title: islamqa.info
- href: https://islamqa.info/en/answers/93450
  title: islamqa.info
- href: https://islamqa.info/en/answers/1114
  title: islamqa.info
- href: https://islamqa.info/en/answers/3807
  title: islamqa.info
translator_id: 554943
---

Islamul păstrează relațiile dintre indivizi într-un mod care se potrivește naturii umane. Legătura dintre un bărbat și o femeie este interzisă, cu excepția căsătoriei legale. Dacă există vreo necesitate de a vorbi cu sexul opus, aceasta ar trebui să fie în limitele politeții și bunelor maniere.

## Prevenirea tentației

Islamul dorește să oprească orice formă de desfrânare de la bun început. Nu este permis ca o femeie sau un bărbat să stabilească o relație de prietenie sau de dragoste cu o persoană de sex opus prin intermediul camerelor de chat, al internetului sau prin orice alte mijloace, deoarece acest lucru conduce femeia sau bărbatul la ispită. Aceasta este calea diavolului prin care el trage persoana să cadă în păcat, desfrâu sau adulter. Allah spune:

{{ $page->verse('24:21..') }}

Femeii îi este interzis să vorbească folosind un ton blând, atrăgător cu cineva care nu este permis pentru ea, după cum spune Allah:

{{ $page->verse('..33:32') }}

Întâlnirea, amestecarea și adunarea bărbaților și femeilor într-un singur loc, înghesuirea lor, precum și dezvăluirea și expunerea femeilor în fața bărbaților sunt interzise de legea Islamului (Shari'ah). Aceste acte sunt interzise deoarece se numără printre cauzele tentației care implică consecințe nefaste, stârnirea dorințelor și comiterea indecențelor și faptelor rele. Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('ahmad:1369') }}

Coranul interzice să ai iubite sau iubiți. Allah spune:

{{ $page->verse('..4:25..') }}

## Considerații sociale

Intrarea într-o relație de tip iubit/iubită poate afecta reputația unei femei în moduri grave, care nu vor afecta neapărat bărbatul. Sexul înainte de căsătorie este contrar religiei și pune în pericol onoarea oamenilor. Este dificil să vezi prin ceața iubirii sau a infatuării, motiv pentru care Allah ne poruncește să ne ferim de sexul înainte de căsătorie; acest fapt poate produce atât de multe daune, de exemplu sarcini nedorite, boli cu transmitere sexuală, inimă frântă sau vinovăție.

În afară de interdicția explicită de la Dumnezeu, există înțelepciuni că relațiile pre-maritale sunt nepermise. Printre acestea se numără:

1. Lipsa de ambiguitate a paternității în cazul în care femeia rămâne însărcinată, fiind eliminată posibilitatea ca aceasta să aibă mai mulți parteneri.
1. Un copil născut în afara căsătoriei nu este atribuit tatălui. Păstrarea descendenței este unul dintre principalele obiective ale legii Islamice.
1. Astfel de relații le oferă bărbaților avantajul de a "face ce vor" cu femeile și nu au nicio responsabilitate emoțională și financiară față de femeie și de orice copii născuți în urma relației; pe când în căsătorie bărbatul are o responsabilitate foarte mare.
1. Există o virtute înnăscută în păstrarea purității (castității) înainte de căsătorie, recunoscută de multe secole și stabilită de fiecare religie.
1. Practicile necorespunzătoare au un efect asupra sufletului pe care nu îl putem percepe. Cu cât ne complacem mai mult în păcat, cu atât inima noastră devine mai împietrită.
1. A avea mai mulți parteneri crește riscul de a contracta boli cu transmitere sexuală și de a-i infecta pe alții.
1. Nu există nicio garanție că a trăi împreună înainte de căsătorie este un indiciu sau o garanție ale relației de după căsătorie. Multe cupluri non-musulmane trăiesc împreună ani de zile, doar pentru a se despărți după ce se căsătoresc.
1. De asemenea, trebuie să ne întrebăm dacă ne-ar plăcea ca fiii și fiicele noastre să facă același lucru înainte de căsătorie?

În concluzie, nu are niciun sens ca relațiile și sexul înainte de căsătorie să fie permise. Considerațiile individuale sunt nesemnificative în comparație cu multitudinea de probleme grave care afectează oamenii, în special femeile, și este o cale sigură de a duce la decadența societății și a propriului suflet.
