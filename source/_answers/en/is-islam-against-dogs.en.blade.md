---
extends: _layouts.answer
section: content
title: Is Islam against dogs?
date: 2024-08-11
description: Islam teaches us to treat animals in a dignified, humane and merciful manner. Dogs cannot be kept as pets, but they can be kept for specific purposes.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
---

Animals are from the creation of Allah and we don’t look upon animals poorly; rather, we are duty-bound, as Muslims, to treat animals in a dignified, humane and merciful manner. Allah's Messenger {{ $page->pbuh() }} said,

{{ $page->hadith('bukhari:3321') }}

## Killing dogs

Abdallah ibn Umar said:

{{ $page->hadith('muslim:1570b') }}

It is important to clarify what kind of dogs are referred to in this narration, as well as the general context. The dogs referred to here are packs of wild dogs that wander around towns and cities and are a nuisance to the community and a health risk. They carry and spread illnesses, just like rats do, and cause a lot of inconvenience to people. The killing of dangerous animals or disease spreading animals is common and accepted throughout the world.

Based on all the overall evidence, the jurists have mentioned that the command to kill dogs is only permissible if it is predatory and harmful. However, a harmless dog, regardless of colour, may not be killed.

## Purity

The saliva of dogs is impure according to the majority of Muslim scholars, as is their fur according to some. One of the most prominent teachings of Islam is purity on all levels, so this was a consideration, as was the fact that angels do not frequent a place where there are dogs. That is because of the words of the Prophet {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3322') }}

## Keeping dogs as pets

Islam forbids Muslims to keep dogs as pets. The Messenger of Allah {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:1574g') }}
