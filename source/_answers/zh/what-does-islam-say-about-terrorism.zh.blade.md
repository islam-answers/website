---
extends: _layouts.answer
section: content
title: 伊斯兰教如何评论恐怖主义？
date: 2024-07-12
description: 伊斯兰教是仁慈的宗教，不允许恐怖主义。
sources:
- href: https://www.islam-guide.com/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 554943
---

伊斯兰教是仁慈的宗教，不允许恐怖主义。真主在古兰经中说道：

{{ $page->verse('60:8') }}

先知穆罕默德 {{ $page->pbuh() }} 一向禁止士兵杀害妇女和儿童， 而且总是劝告他们：

{{ $page->hadith('tirmidhi:1408') }}

他也说：

{{ $page->hadith('bukhari:3166') }}

此外，先知穆罕默德 {{ $page->pbuh() }} 也禁止火刑。

他一度将谋杀列为第二严重的主要罪行， 他甚至警告在审判日时，

{{ $page->hadith('bukhari:6533') }}

穆斯林甚至被鼓励善待动物并且禁止伤害它们。 先知穆罕默德 {{ $page->pbuh() }} 曾说：

{{ $page->hadith('bukhari:3318') }}

他也说某个男人给一只很渴的猫饮水，因此真主原谅他的罪行。 先知 {{ $page->pbuh() }} 被问道：“真主使者，我们会因为善待动物而得赏吗？” 他说：

{{ $page->hadith('bukhari:2466') }}

此外，在夺取动物生命以将其作为食物时，穆斯林被教导尽可能在不造成恐惧及伤害的情况下做这件事。 先知穆罕默德 {{ $page->pbuh() }} 说道：

{{ $page->hadith('tirmidhi:1409') }}

在彰显这些以及其它回经文字时，即可知对无防卫能力的民众进行恐怖行动、大规模摧毁建筑物及资产，以及利用炸弹攻击或伤害无辜的男性、妇女和儿童，都是伊斯兰教及穆斯林所禁止并厌恶的。

穆斯林遵循伊斯兰教的和平、仁慈及宽恕，且绝大数穆斯林都和打着伊斯兰教名义所行的部分暴行无关。 如果个别穆斯林涉及恐怖主义行动，该人将因违反伊斯兰教法律而有罪。

然而，必须区分恐怖主义和对占领的合法抵抗，因为两者有很大的不同。
