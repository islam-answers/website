---
extends: _layouts.answer
section: content
title: Jesus foi crucificado?
date: 2024-10-01
description: De acordo com o Alcorão, Jesus não foi morto ou crucificado. Jesus foi
  elevado vivo aos céus por Allah, e outro homem foi crucificado em seu lugar.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 594172
---

Quando os judeus e o imperador romano conspiraram para matar Jesus {{ $page->pbuh() }}, Allah fez com que uma das pessoas presentes se assemelhasse em tudo a Jesus. Então, mataram o homem que se parecia com Jesus. Não mataram Jesus. Jesus {{ $page->pbuh() }} ressuscitou vivo para os Céus. A prova disso são as palavras de Deus sobre a invenção dos judeus e a sua refutação:

{{ $page->verse('4:157-158') }}

Assim, Deus desmentiu as palavras dos judeus, quando afirmaram que o haviam matado e crucificado, e declarou que o havia levado para junto de si. Isso foi uma misericórdia de Deus para com Jesus (Isa) {{ $page->pbuh() }}, e foi uma honra para ele, para que fosse um dos Seus sinais, uma honra que Deus concede a quem quer dos Seus mensageiros.

A implicação das palavras "Mas, Allah ascendeu-o até Ele." é que Deus, glorificado seja, levou Jesus em corpo e alma, de modo a refutar a alegação dos judeus de que eles o crucificaram e o mataram, porque matar e crucificar têm a ver com o corpo físico. Além disso, se Ele tivesse levado apenas a alma, isso não excluiria a pretensão de o terem matado e crucificado, porque levar apenas a alma não seria uma refutação da pretensão deles. Também o nome de Jesus {{ $page->pbuh() }}, na realidade, refere-se a ele na alma e no corpo juntos; não pode referir-se a um deles apenas, a menos que haja uma indicação para esse efeito. Para além disso, o facto de se ter tomado a alma e o corpo em conjunto é indicativo do poder e da sabedoria perfeitos de Deus, e da Sua honra e apoio a quem quer que Ele queira entre os Seus Mensageiros, de acordo com o que Ele, exaltado seja, diz no final do versículo: "E Allah é Todo-Poderoso, Sábio."
