---
extends: _layouts.answer
section: content
title: Was ist der Dschihad (Jihad)?
date: 2024-08-11
description: Der Kern des Jihad liegt darin, sich für den Glauben einzusetzen und
  Opfer zu bringen, auf eine Art und Weise, die Gott zufriedenstellt.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 587746
---

Der Kern des Jihad liegt darin, sich für den Glauben einzusetzen und Opfer zu bringen, auf eine Art und Weise, die Gott zufriedenstellt. Sprachlich bedeutet "Jihad" "sich anstrengen" und kann das Streben nach guten Taten, das Geben von Almosen oder das Kämpfen im Namen Gottes umfassen.

Die am weitesten bekannte Form des Jihad ist der militärische Jihad, der erlaubt ist, um das Wohl der Gesellschaft zu wahren, Unterdrückung zu verhindern und Gerechtigkeit zu fördern.
