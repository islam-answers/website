---
extends: _layouts.answer
section: content
title: W jaki sposób można zostać muzułmaninem?
date: 2024-11-17
description: 'Wystarczy powiedzieć z przekonaniem: "La ilaha illa Allah, Muhammadur
  rasoolu Allah", by przejść na islam i stać się muzułmaninem.'
sources:
- href: https://www.islam-guide.com/ch3-6.htm
  title: islam-guide.com
translator_id: 583847
---

Wystarczy powiedzieć z przekonaniem: "La ilaha illa Allah, Muhammadur rasoolu Allah", aby przejść na islam i stać się muzułmaninem. To powiedzenie oznacza "Nie ma prawdziwego boga (bóstwa) oprócz Boga (Allaha), a Muhammad jest Posłańcem (Prorokiem) Boga" Pierwsza część, "Nie ma prawdziwego boga poza Bogiem", oznacza, że nikt nie ma prawa być czczonym poza samym Bogiem i że Bóg nie ma ani partnera, ani syna. Aby być muzułmaninem, należy również:

- Wierzyć, że Święty Koran jest dosłownym słowem Boga, objawionym przez Niego.
- Wierzyć, że Dzień Sądu (Dzień Zmartwychwstania) jest prawdą i nadejdzie, tak jak Bóg obiecał w Koranie.
- Przyjąć Islam jako swoją religię.
- Nie czcić niczego i nikogo poza Bogiem.

Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:2747') }}
