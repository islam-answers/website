---
extends: _layouts.answer
section: content
title: Jakie są główne praktyki islamu?
date: 2024-09-11
description: Główne praktyki islamu nazywane są pięcioma filarami. Obejmują one świadectwo
  wiary, modlitwy, nakazaną dobroczynność, post i pielgrzymkę.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 584496
---

Główne praktyki islamu są określane jako pięć filarów.

### Pierwsza praktyka: Świadectwo wiary

Ogłosić, że nie ma Boga godnego czci poza Allahem, a Muhammad jest Jego ostatecznym Posłańcem.

### Druga praktyka: Modlitwy

Należy wykonywać pięć razy dziennie: raz o świcie, w południe, po południu, po zachodzie słońca i w nocy.

### Trzecia praktyka: przepisana dobroczynność „Zakat”

Jest to coroczna obowiązkowa opłata charytatywna na rzecz tych, którzy mają mniej szczęścia i jest obliczana jako niewielka część całkowitych rocznych oszczędności, która obejmuje 2,5% majątku pieniężnego i może obejmować inne aktywa. Płacą go ci, którzy mają nadwyżkę majątku.

### Czwarta praktyka: Post w miesiącu Ramadan

Przez cały ten miesiąc muzułmanie muszą powstrzymać się od wszelkiego jedzenia, picia i stosunków seksualnych ze swoimi małżonkami od świtu do zachodu słońca. Promuje to powściągliwość, świadomość Boga i empatię wobec biednych.

### Piąta praktyka: Pielgrzymka.

Każdy zdolny muzułmanin jest zobowiązany do odbycia pielgrzymki do Mekki w ciągu swojego życia. Wiąże się ona z modlitwą, błaganiem, dobroczynnością i podróżowaniem, i jest bardzo pokornym i duchowym doświadczeniem.
