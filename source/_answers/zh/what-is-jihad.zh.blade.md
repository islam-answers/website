---
extends: _layouts.answer
section: content
title: 什么是圣战？
date: 2024-11-16
description: 圣战的本质是以令真主喜悦的方式为自己的宗教而斗争和牺牲。
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 603054
---

圣战的本质是以取悦真主的方式为自己的宗教而奋斗和牺牲。从语言学上讲，它的意思是“奋斗”，可以指努力做好事、施舍或为了真主而战。

最常见的形式是军事圣战，其目的是维护社会福祉、防止压迫蔓延，并促进正义。
