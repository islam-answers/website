@php ($editors = [
    'Muhannad Ajjan',
    'Umm Yusuf',
    'Mohamed Yousry',
    'Sayem Hossen',
])

<ul>
    @foreach ($editors as $editor)
    <li>
        <span class="text-blue-600 font-semibold">{{ $editor }}</span>
    </li>
    @endforeach
</ul>