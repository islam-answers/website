---
extends: _layouts.answer
section: content
title: Was sagt der Islam über den Umgang mit den Eltern?
date: 2024-10-13
description: Freundlichkeit gegenüber den Eltern ist eine der höchst belohnten Taten im Islam. Das Gebot, gut zu seinen Eltern zu sein, ist eine klare Lehre des Qur'ans.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 587746
---

Güte gegenüber den Eltern zu zeigen, gehört zu den am höchsten belohnten Taten in den Augen Allahs. Die Aufforderung, gut zu seinen Eltern zu sein, ist eine klare Lehre des Qur'ans. Allah sagt:

{{ $page->verse('4:36..') }}

Die Erwähnung der Hingabe an die Eltern folgt direkt nach der Hingabe an Gott. Diese Botschaft wird im gesamten Quran immer wieder hervorgehoben.

{{ $page->verse('17:23') }}

In diesem Vers fordert Gott uns auf, Ihn allein und niemanden sonst anzubeten. Unmittelbar danach lenkt Er unsere Aufmerksamkeit darauf, wie wir uns besonders gegenüber unseren Eltern verhalten sollen, wenn sie alt werden und auf andere für Unterkunft und Unterstützung angewiesen sind. Muslime werden angeleitet, nicht grob oder schimpfend mit ihren Eltern umzugehen, sondern vielmehr mit liebevollen Worten zu sprechen und Zuneigung und Fürsorge zu zeigen.

Im Qur'an fordert Allah zudem dazu auf, die eigenen Eltern in seinen Gebeten zu erwähnen:

{{ $page->verse('17:24') }}

Allah hebt hervor, dass der Gehorsam gegenüber den Eltern dem Gehorsam Ihm gegenüber gleichkommt, außer wenn es darum geht, Ihm Partner zur Seite zu stellen.

{{ $page->verse('31:14-15..') }}

Dieser Vers hebt hervor, dass alle Eltern eine respektvolle Behandlung verdienen. Müttern sollte jedoch besondere Fürsorge zuteilwerden, da sie oft zusätzliche Herausforderungen und Mühen zu bewältigen haben.

Sich um die Eltern zu kümmern, gilt als eine der besten Taten:

{{ $page->hadith('muslim:85e') }}

Zusammenfassend bedeutet es im Islam, die eigenen Eltern zu ehren, ihnen zu gehorchen, sie zu respektieren, für sie zu beten, die Stimme in ihrer Gegenwart zu senken, ihnen freundlich zuzulächeln, Unmut zu vermeiden, sich um sie zu kümmern, ihre Wünsche zu erfüllen, sie zu konsultieren und auf ihre Worte zu hören.
