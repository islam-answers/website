---
extends: _layouts.answer
section: content
title: Šta je halal hrana?
date: 2024-07-21
description: Halal hrana je ona koju je Bog dozvolio muslimanima da konzumiraju, a
  glavni izuzeci su svinjetina i alkohol.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 584497
---

Halal, ili dozvoljena, hrana je ona koju je Bog dozvolio muslimanima da konzumiraju. Generalno, većina hrane i pića se smatra halal, sa glavnim izuzecima su svinjetina i alkohol. Meso i živina moraju biti zaklani humano i ispravno, što uključuje pominjanje Božjeg imena pre klanja i minimiziranje patnje životinja.
