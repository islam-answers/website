---
extends: _layouts.answer
section: content
title: Quelles sont les pratiques principales de l'Islam ?
date: 2024-09-21
description: Les pratiques principales de l'islam, appelées les "Cinq piliers", incluent le témoignage de foi, les prières, la charité, le jeûne et le pèlerinage.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 586055
---

Les principales pratiques de l'islam sont appelées les cinq piliers.

### Premier pilier : Le témoignage de la foi

Attester qu'il n'y a pas d'autre Dieu digne d'être adoré qu'Allah, et que Muhammad est son dernier messager.

### Deuxième pilier : les prières

À accomplir cinq fois par jour : une fois à l'aube, à midi, en milieu d'après-midi, au crépuscule et la nuit.

### Troisième pilier : L'aumône obligatoire "Zakat"

Il s'agit d'une charité annuelle obligatoire versée aux moins fortunés et calculée comme une petite partie de l'épargne annuelle totale, qui comprend 2,5 % de la richesse monétaire et peut inclure d'autres actifs. Elle est payée par ceux qui ont des richesses excédentaires.

### Quatrième pilier : Le jeûne pendant le mois de Ramadan

Durant tout ce mois, les musulmans doivent s'abstenir de toute nourriture, de toute boisson et de toute relation sexuelle avec leur conjoint, de l'aube au coucher du soleil. Cela favorise la maîtrise de soi, la conscience de Dieu et l'empathie envers les pauvres.

### Cinquième pilier : Le pèlerinage.

Tout musulman qui en a la capacité, est tenu d'accomplir le pèlerinage à la Mecque, au moins une fois au cours de sa vie. Ce pèlerinage comprend des prières, des supplications, des aumônes et des voyages. Il s'agit d'une expérience très humble et spirituelle.
