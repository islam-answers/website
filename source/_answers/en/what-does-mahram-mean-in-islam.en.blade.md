---
extends: _layouts.answer
section: content
title: What does Mahram mean in Islam?
date: 2024-11-27
description: A Mahram is someone with whom marriage is permanently forbidden,
  such as close blood relatives, foster relations, or in-laws.
sources:
- href: https://islamqa.org/hanafi/daruliftaa/8495/
  title: islamqa.org (Daruliftaa.com)
- href: https://islamqa.info/en/answers/5538/
  title: islamqa.info
- href: https://islamqa.info/en/answers/130002/
  title: islamqa.info
---

As a general principle, a Mahram is one with whom marriage is permanently unlawful. This is the reason why “Mahram” is translated in English as unmarriageable kin. It is permissible for a woman to take off her hijab in front of her Mahrams. It is likewise permissible to shake hands with or kiss a Mahram on the head, the nose or the cheek.

This (permanent prohibition of marriage) is established in three ways: By kinship (ties of blood), foster relationship (breastfeeding) and relationship through marriage.

Thus, permanent unlawfulness of marriage is established with the above-mentioned three types of relationships, and a Mahram is he with whom marriage is unlawful permanently. In other words, one becomes a Mahram due to these three types of relationships.

## Relationship of family/lineage

It is permanently unlawful for a man to marry the following (hence he will be considered a Mahram for them):

- Mother, grandmother, and on up;
- Paternal grandmother, and on up;
- Daughters, granddaughters, and on down;
- All type of sisters (whether full or half),
- Maternal and paternal aunts,
- Nieces (brother’s or sister’s daughters),

Allah says in the Quran:

{{ $page->verse('4:23') }}

Similarly, a women’s Mahrams by family relationships are:

- Father, grandfather, and on up;
- Maternal grandfather, and on up;
- Sons, grandsons, and on down;
- All type of brothers (whether full or half),
- Maternal and paternal uncles,
- Nephews (brother’s or sister’s sons),

Allah says in the Quran:

{{ $page->verse('..24:31..') }}

## Relationship of fosterage

Whosoever is a Mahram through the relationship of lineage, will also be considered a Mahram by fosterage. As such, a foster-father (foster mother’s husband), foster-brother, foster-uncle, foster-nephew, etc will all be considered to be a woman’s Mahram, and one will be a Mahram to a foster-mother, foster sister, foster niece, etc.

However, one should remember that this is only when breastfeeding takes place in the period designated for it, which is two years. One should be careful in determining who is a Mahram through foster relations, for determining this, at times, can be complex and complicated. One must refer to a scholar before coming to a judgment.

## Relationship of marriage

The third relationship with which marriage becomes permanently unlawful and consequently the relationship of being a Mahram is established is that of marriage.

There are four types of people with whom marriage becomes unlawful permanently due to the relationship of marriage:

- One’s wife’s mother (mother in-law), grandmother and on up: Marriage with her becomes unlawful by merely contracting marriage with the daughter, regardless of whether the marriage was consummated or otherwise.
- One’s wife’s daughter (from a previous marriage), grand-daughter and on down: Marriage with her becomes unlawful (permanently) if the marriage with her mother was consummated.
- The wife of one’s son, grandson, and on down: This is regardless whether the son consummated the marriage or otherwise.
- One’s stepmother, step grandmother and on up: Meaning those women who have been in the marriage of one’s father or paternal or maternal grandfather.

To sum up, a Mahram is he with whom marriage is permanently unlawful, and this permanent unlawfulness/prohibition of marriage is established in three ways: The relationship of lineage, relationship through fosterage and the relationship through marriage.
