---
extends: _layouts.answer
section: content
title: Perché i musulmani non possono bere alcolici?
date: 2024-10-10
description: Bere alcolici è un grave peccato che offusca la mente e spreca denaro. Allah ha proibito tutto ciò che danneggia corpo e mente.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
translator_id: 573107
---

L'Islam è venuto per portare e aumentare le cose buone, e per allontanare e ridurre quelle dannose. Tutto ciò che è benefico, o per lo più benefico, è lecito (halal) e tutto ciò che è dannoso, o per lo più dannoso, è proibito (haram). L'alcol rientra senza dubbio nella seconda categoria. Allah dice:

{{ $page->verse('2:219') }}

Gli effetti nocivi e dannosi dell'alcol sono ben noti a tutti. Tra gli effetti nocivi dell'alcol vi è quel che è stato menzionato da Allah:

{{ $page->verse('5:90-91') }}

L’alcol provoca molti danni e merita di essere definito “la chiave di tutti i mali”, come descritto dal Profeta Muhammad {{ $page->pbuh() }} che ha detto:

{{ $page->hadith('ibnmajah:3371') }}

Crea inimicizia e odio tra le persone, impedisce di ricordare Allah e di pregare, spinge a commettere zina [relazioni sessuali illecite] e può persino spingere a commettere incesto con le figlie, sorelle o altre parenti femmine. Priva dell'orgoglio e della gelosia protettiva, genera vergogna, rammarico e disonore. Conduce alla rivelazione di segreti e all'esposizione di difetti. Incoraggia le persone a commettere peccati e azioni malvagie.

Inoltre, Ibn 'Umar ha narrato che il Profeta [pace e benedizioni di Allah su di lui] disse:

{{ $page->hadith('muslim:2003a') }}

Il Profeta {{ $page->pbuh() }} disse anche:

{{ $page->hadith('ibnmajah:3381') }}
