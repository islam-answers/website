---
extends: _layouts.answer
section: content
title: A fost Coranul copiat după Biblie?
date: 2024-10-25
description: Coranul nu a fost copiat după Biblie. Profetul Muhammad era analfabet,
  nu exista nicio Biblie arabă la acea vreme, iar revelațiile au venit direct de la
  Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 554943
---

Allah a creat totul în jurul nostru cu un scop. Omul a fost, de asemenea, creat cu un scop și acesta este să se închine lui Allah. Allah spune în Coran:

{{ $page->verse('51:56') }}

Pentru a îndruma omenirea, Allah a trimis profeți din când în când. Acești profeți au primit revelația de la Allah și fiecare religie are propria colecție de scripturi divine, care constituie fundamentul credințelor lor. Aceste scripturi reprezintă transcrierea revelației divine, în mod direct, ca în cazul profetului Moise (Musa), care a primit poruncile de la Allah, sau indirect, ca în cazul lui Isus (Isa) și Muhammad {{ $page->pbuh() }}, care au transmis oamenilor revelația care le-a fost comunicată de îngerul Gabriel (Jibreel).

Coranul le cere tuturor musulmanilor să creadă în scripturile care l-au precedat. În calitate de musulmani, credem că Tora (Tawrah), Cartea Psalmilor (Zaboor), Evanghelia (Injeel) în forma lor originală, adică în modul în care au fost revelate, sunt cuvântul lui Allah.

## Ar fi putut profetul Muhammad să copieze Coranul după Biblie?

Discuția despre modul în care Profetul Muhammad {{ $page->pbuh() }} ar fi putut să învețe poveștile Oamenilor Cărții, apoi să le transmită în Coran - ceea ce înseamnă că nu a fost o revelație - este o discuție a diferitelor afirmații conform cărora el {{ $page->pbuh() }} ar fi putut să învețe poveștile direct din cărțile lor sfinte sau ar fi putut să învețe verbal de la Oamenii Cărții ceea ce scrierile conțineau în materie de povești, porunci și interdicții, dacă nu i-a fost posibil să le studieze cărțile personal.

Aceste afirmații pot fi rezumate în trei afirmații de bază:

1. Afirmația că Profetul Muhammad {{ $page->pbuh() }} nu ar fi fost analfabet
1. Afirmația că scripturile creștine ar fi fost la dispoziția Profetului {{ $page->pbuh() }} și că acesta putea să citeze din ele sau să le copieze
1. Afirmația că Mecca ar fi fost un important centru educațional pentru studiul scripturilor

În primul rând, textul Coranului și Sunnah (narațiunile și tradițiile Profetului) indică în mod clar în multe locuri, fără să lase loc de îndoială, că Profetul Muhammad {{ $page->pbuh() }} a fost într-adevăr analfabet și că nu a povestit niciodată poveștile Oamenilor Cărții înainte ca revelația să ajungă la el și că nu a scris niciodată nimic cu mâna sa. El nu știa nimic despre aceste povești înainte de a-și începe misiunea și nu a vorbit niciodată despre ele înainte de aceasta.

Allah, fie ca El să fie înălțat, spune:

{{ $page->verse('29:48') }}

Cărțile despre biografia și istoria Profetului nu menționează că Profetul a scris revelația sau că a scris el însuși scrisorile către regi. Mai degrabă este menționat contrariul; Profetul Muhammad {{ $page->pbuh() }} avea scribi care scriau revelația și alte lucruri. Într-un text genial al lui Ibn Khaldun, există o descriere a nivelului de educație din Peninsula Arabică chiar înainte de începerea misiunii Profetului. El spune:

> Printre arabi, abilitatea de a scrie era mai rară decât ouăle de cămilă. Majoritatea oamenilor erau analfabeți, în special locuitorii deșertului, deoarece această abilitate se regăsește de obicei printre locuitorii orașelor.

Prin urmare, arabii nu se refereau la o persoană analfabetă ca fiind analfabetă; mai degrabă o descriau pe cea care știa să citească și să scrie ca fiind instruită, deoarece a ști să citești și să scrii era excepția, nu norma printre oameni.

În al doilea rând, nu există nicio dovadă, indicație sau sugestie că ar fi existat vreo traducere în limba arabă a Bibliei la acea vreme. Al-Bukhari a relatat de la Abu Hurayrah (Allah să fie mulțumit de el) că acesta a spus:

{{ $page->hadith('bukhari:7362') }}

Această narațiune indică faptul că evreii dețineau monopolul deplin asupra textului și a explicațiilor sale. Dacă textele lor ar fi fost cunoscute în arabă, nu ar fi fost nevoie ca evreii să citească textul sau să îl explice [musulmanilor].

Această idee este susținută de versetul în care Allah, fie ca El să fie înălțat, spune:

{{ $page->verse('3:78') }}

Poate cea mai importantă carte scrisă pe această temă este cea a lui Bruce Metzger, profesor de limba și literatura Noului Testament, "The Bible in Translation", în care acesta spune:

> Este cel mai probabil ca cea mai veche traducere arabă a Bibliei să dateze din secolul al VIII-lea.

Orientalistul Thomas Patrick Hughes a scris:

> Nu există nicio dovadă că Muhammad a citit Scripturile Creștine... Trebuie remarcat că nu există dovezi clare care să sugereze că vreo traducere arabă a Vechiului și Noului Testament a existat înainte de vremea lui Muhammad.

În al treilea rând, ideea că Profetul a dobândit revelația de la oameni este o idee veche. Politeiștii l-au acuzat de asta, iar Coranul i-a respins, așa cum Allah, fie ca El să fie înălțat, spune:

{{ $page->verse('25:5-6') }}

Mai mult, afirmația că Profetul Muhammad {{ $page->pbuh() }} a dobândit cunoștințe de la Oamenii Cărții atunci când se afla în Mecca este o afirmație care este infirmată de statutul academic și cultural al regiunii și de adevărata natură a legăturii pe care această societate arabă a avut-o cu cunoștințele Oamenilor Cărții.

Prin urmare, după ce am dovedit că Profetul Muhammad {{ $page->pbuh() }} nu a învățat cunoștințele Oamenilor Cărții de la aceștia direct sau printr-un intermediar, nu ne rămâne altă opțiune decât să afirmăm că a fost o revelație de la Allah către Profetul Său ales.
