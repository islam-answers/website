---
extends: _layouts.answer
section: content
title: 你需要证人来宣读清真言吗？
date: 2024-07-13
description: 在证人前面公开宣布信仰不是进入伊斯兰教的正确条件
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 554943
---

一个人要成为穆斯林，并不必须在任何人面前宣告他的伊斯兰教信仰。伊斯兰教是一个人与他的主之间的事情，愿他受到祝福和尊崇。

如果他只是为了获得个人的一些手续和文件，而让人作证他信仰了伊斯兰教，则是可以的；但是不能把它作为正确地进入伊斯兰教的一个条件。
