---
extends: _layouts.answer
section: content
title: Người Islam tin vào Jesus (Giê-xu) như thế nào?
date: 2024-06-30
description: Những người Islam luôn luôn ngưỡng mộ và tôn trọng đức Giê-xu. Họ coi
  Ông là một trong những người đưa tin vĩ đại nhất mà Thượng Đế đã phái đến cho mọi
  người.
sources:
- href: https://www.islam-guide.com/vi/ch3-10.htm
  title: islam-guide.com
---

Những người Islam luôn luôn ngưỡng mộ và tôn trọng đức Giê-xu. Họ coi Ông là một trong những người đưa tin vĩ đại nhất mà Thượng Đế đã phái đến cho mọi người. Kinh Qur'an khẳng định sự ra đời của Ông, và có một chương trong Kinh Qur'an có tên gọi ‘Maryam’ (Mary). Kinh Qur'an mô tả sự ra đời của Giê-xu như sau:

{{ $page->verse('3:45-47') }}

Giê-xu được sinh ra một cách phi thường theo lệnh của Thượng Đế, và cũng bằng mệnh lệnh tương tự, Adam đã trở thành con người mà không cần có bố hay mẹ. Thượng Đế nói:

{{ $page->verse('3:59') }}

Trong khi thực hiện sứ mệnh Thiên Sứ của mình, Giê-xu đã thực hiện một số phép thần. Thượng Đế cho chúng ta biết những điều Giê-xu đã nói:

{{ $page->verse('3:49..') }}

Người Islam tin rằng Giê-su không bị đóng đinh. Đó chỉ là dự định của những kẻ thù của Ông định đóng đinh Ông mà thôi. Thượng Đế đã cứu Ông và đưa Ông về bên Ngài. Và hình dáng của Giê-su được chuyển sang cho một người khác (một trong những kẻ thù đầu tiên bắt gặp Giê-su). Kẻ thù của Giê-su đã bắt người đàn ông này và đóng đinh ông lên thánh giá mà vẫn nghĩ rằng đó là Ông. Thượng Đế nói:

{{ $page->verse('..4:157..') }}

Không phải Thiên Sứ Muhammad {{ $page->pbuh() }} hay Giê-su đến là để thay đổi học thuyết căn bản về đức tin vào Thượng Đế duy nhất mà các Thiên Sứ trước họ đã làm. Ngược lại hai vị Thiên Sứ này đến chỉ là để củng cố và khôi phục lại điều đó.
