---
extends: _layouts.answer
section: content
title: イスラム教はテロリズムについて何と言っていますか？
date: 2024-07-18
description: 慈悲の教えであるイスラームはテロリズムを認めていない｡
sources:
- href: https://www.islam-guide.com/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 554943
---

慈悲の教えであるイスラームはテロリズムを認めていない｡クルアーンにはこうある｡

{{ $page->verse('60:8') }}

預言者ムハンマド{{ $page->pbuh() }}は兵士たちに女性と子供を殺すことを禁じて､彼らに次のように忠告していた｡

{{ $page->hadith('tirmidhi:1408') }}

また次のようにも言った｡

{{ $page->hadith('bukhari:3166') }}

また預言者ムハンマド{{ $page->pbuh() }}は火で罰することを禁じた｡

ある時彼は殺人を2番目の大罪に挙げ､次のように警告した｡

{{ $page->hadith('bukhari:6533') }}

ムスリムは動物に優しく接することを薦めており､それらを不正に傷つけることは禁じられている｡預言者ムハンマド{{ $page->pbuh() }}は申された｡

{{ $page->hadith('bukhari:3318') }}

彼はまた､非常に喉の渇いた犬に水を与えた男が､神に過去に犯した罪を赦されたことについても語った｡それについて人々は預言者に訊ねた｡｢預言者よ､畜獣にも(それらに対する善行による)報酬はあるのですか?｣預言者は言った｡

{{ $page->hadith('bukhari:2466') }}

更に､食べる目的で動物を殺す場合､ムスリムはその動物の恐怖や苦しみをできるだけ少なくするように命じられている｡預言者ムハンマド{{ $page->pbuh() }}は言った｡

{{ $page->hadith('tirmidhi:1409') }}

このような教えを含むイスラームのテキストの中では､無防備な市民の心に恐怖を与える行為や､建物や所有物の完全な破壊､無垢な人々や女性や子供を爆撃したり傷つけたりすることは全て禁じられており､イスラームとムスリムにとって憎むべき行為となっている ｡

ムスリムは平和､慈悲､寛容の教えに従っているのであり､彼らの大多数は現在ムスリムと結び付けられている諸々の野蛮な行為とは何の関わりもない｡もしムスリムがテロ行為を犯すならば､その人物はイスラームの法を犯した罪人ということになる｡

しかし、テロリズムと占領に対する正当な抵抗はまったく異なるものであるため、区別することが重要である。
