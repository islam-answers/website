---
extends: _layouts.answer
section: content
title: Da li su svi gresi oprošteni nakon prelaska na islam?
date: 2025-01-26
description: Kada nevernik postane musliman, Allah mu oprašta sve ono što je radio
  dok je bio nemusliman, i on postaje očišćen od greha.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
translator_id: 584497
---

Njegovom milošću, Allah je učinio prihvatanje islama razlogom za brisanje greha koji su počinjeni prije njega. Kada nevernik postane musliman, Allah mu oprašta sve ono što je radio dok je bio nemusliman, i on postaje očišćen od grijeha.

Amr ibn al-'As (da je Allah zadovoljan njim) je rekao:

{{ $page->hadith('muslim:121') }}

Imam Nawawi je rekao: “Islam uništava ono što je bilo prije njega” znači da ga briše.

Šejh Ibn `Uthaymin ra je postavljeno slično pitanje, o nekome ko je zaradio novac prodavajući drogu pre nego što je postao musliman. On je odgovorio: Ovom bratu koga je Allah blagoslovio islamom nakon što je stekao nezakonito imetak, kažemo: budi raspoložen, jer mu je ovo imetak dozvoljeno i na njemu nema grijeha, bilo da ga čuva ili daje u dobročinstvo, ili ga koristi za venčanje, jer Allah kaže u Svojoj Časnoj Knjizi:

{{ $page->verse('8:38') }}

To znači da je sve što je prošlo, generalno, oprošteno. Ali svaki novac koji je nasilno oduzet od vlasnika mora mu se vratiti. Ali novac koji je zarađen dogovorima među ljudima, čak i ako je nezakonit, kao što je zarađen preko Ribe (kamate), ili prodajom droge itd. njemu je dozvoljen kada postane musliman.

Mnogi od nemuslimana u vreme poslanika Muhammeda {{ $page->pbuh() }} postali su muslimani nakon što su ubili muslimane, ali nisu bili kažnjeni za ono što su učinili.
