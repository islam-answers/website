<div itemscope itemprop="mainEntity" itemtype="https://schema.org/Question" class="flex flex-col mb-4">
    <h2 itemprop="name" class="text-3xl mt-0">
        <a href="{{ $answer->getRealUrl() }}" title="{{ $answer->getString('read_answer') }} - {{ $answer->title }}" class="text-gray-900 font-extrabold">{{ $answer->title }}</a>
    </h2>

    <div itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <p itemprop="text" class="mb-4 mt-0">{!! $answer->getExcerpt() !!}</p>
    </div>

    <a href="{{ $answer->getRealUrl() }}" title="{{ $answer->getString('read_answer') }} - {{ $answer->title }}" class="uppercase font-semibold text-blue-700 tracking-wide mb-2">{{ $answer->getString('read_answer') }}</a>
</div>
