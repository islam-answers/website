<?php

namespace App\Listeners;

use TightenCo\Jigsaw\Jigsaw;

class ReportBadSubstitutions
{
    public function handle(Jigsaw $jigsaw)
    {
        if ($jigsaw->getEnvironment() === "production") {
            return;
        }

        $violations = collect();

        $answers = $jigsaw->getCollection("answers");

        $pbuh = $jigsaw->getSiteData()->page->l10n['strings']['pbuh']->values()->toArray();

        $answers->each(function ($answer) use (&$violations, $pbuh) {
            $title = $answer->title;
            $description = $answer->description;
            $content = $answer->getContent();

            // Look for plain pbuh in the content in all languages
            $pbuhRegex = implode('|', $pbuh);
            $matches = [];
            preg_match_all("/\b($pbuhRegex)\b/i", $content, $matches);

            if (!empty($matches[0])) {
                $violations->add([
                    "class" => "Plain PBUH",
                    "title" => $title,
                    "message" => implode(', ', $matches[0]),
                ]);
            }

            // Look for " , " or " . " (commonly found in answers from IslamQA.info)
            $matches = [];
            preg_match_all("/\s[,.]\s/i", $content, $matches);

            if (!empty($matches[0])) {
                $violations->add([
                    "class" => "Space before and after comma or period",
                    "title" => $title,
                ]);
            }

            // Look for double spaces
            if (strpos($content, "  ") !== false) {
                $pos = strpos($content, "  ");

                $needle = "<blockquote>";
                $isAfterBlockquote = substr($content, max(0, $pos - strlen($needle) - 1), strlen($needle)) === $needle;

                if (!$isAfterBlockquote) {
                    $violations->add([
                        "class" => "Double spaces",
                        "title" => $title,
                    ]);
                }
            }
        });

        if ($violations->isNotEmpty()) {
            printf("Error: %s bad substitutions found.\n", $violations->count());

            // Show class followed by all violations
            $violations->groupBy("class")->each(function ($violations, $class) {
                printf("  %s:\n", $class);
                $violations->each(function ($violation) {
                    printf("    %s\n", $violation["title"]);
                    if (isset($violation["message"])) {
                        printf("      %s\n", $violation["message"]);
                    }
                });
            });
        }
    }
}