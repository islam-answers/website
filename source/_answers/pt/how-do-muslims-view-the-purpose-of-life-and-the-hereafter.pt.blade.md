---
extends: _layouts.answer
section: content
title: Como os muçulmanos veem o objetivo da vida e a vida no além?
old_titles:
- Como os muçulmanos encaram o objetivo da vida e a vida no além?
date: 2024-12-16
description: O Islam ensina que o objetivo da vida é adorar Deus e que os seres humanos
  serão julgados por Deus no outro mundo.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=NMM01&articleID=NMM010003&articlePages=4
  title: spubs.com
translator_id: 594172
---

No Alcorão Sagrado, Deus ensina aos seres humanos que foram criados para O adorarem e que a base de toda a verdadeira adoração é a consciência de Deus. Uma vez que os ensinamentos do Islam abrangem todos os aspectos da vida e da ética, a consciência de Deus é encorajada em todos os assuntos humanos.

O Islam deixa claro que todos os actos humanos são actos de adoração se forem feitos apenas para Deus e de acordo com a Sua Lei Divina. Como tal, a adoração no Islam não se limita aos rituais religiosos. Os ensinamentos do Islam actuam como uma misericórdia e uma cura para a alma humana, e qualidades como a humildade, a sinceridade, a paciência e a caridade são fortemente encorajadas. Além disso, o Islam condena o orgulho e a presunção, uma vez que Deus Todo-Poderoso é o único juiz da justiça humana.

A visão islâmica da natureza do homem é também realista e equilibrada. Não se acredita que os seres humanos sejam inerentemente pecadores, mas são vistos como igualmente capazes tanto do bem como do mal. O Islam também ensina que a fé e a ação andam de mãos dadas. Deus deu às pessoas o livre arbítrio e a medida da sua fé são os seus actos e acções. No entanto, os seres humanos também foram criados fracos e caem regularmente em pecado.

Esta é a natureza do ser humano, tal como foi criado por Deus na Sua Sabedoria, e não é inerentemente "corrupta" ou necessitada de reparação. Isto porque a via do arrependimento está sempre aberta a todos os seres humanos e Deus Todo-Poderoso ama mais o pecador arrependido do que aquele que não peca de todo.

O verdadeiro equilíbrio de uma vida islâmica é estabelecido por um saudável temor de Deus, bem como por uma crença sincera na Sua infinita Misericórdia. Uma vida sem temor de Deus conduz ao pecado e à desobediência, enquanto que a crença de que pecámos tanto que Deus não nos perdoará conduz ao desespero. À luz disto, o Islam ensina que: só os desencaminhados desesperam da Misericórdia do seu Senhor.

{{ $page->verse('39:53') }}

Além disso, o Alcorão Sagrado, que foi revelado ao Profeta Muhammad {{ $page->pbuh() }}, contém uma grande quantidade de ensinamentos sobre a vida futura e o Dia do Juízo. Por isso, os muçulmanos acreditam que todos os seres humanos acabarão por ser julgados por Deus pelas suas crenças e acções durante a sua vida terrena.

Ao julgar os seres humanos, Deus Todo-Poderoso será Misericordioso e Justo, e as pessoas só serão julgadas por aquilo de que são capazes. Basta dizer que o Islam ensina que a vida é um teste e que todos os seres humanos serão responsáveis perante Deus. Uma crença sincera na vida futura é fundamental para levar uma vida equilibrada e moral. Caso contrário, a vida é vista como um fim em si mesma, o que leva os seres humanos a tornarem-se mais egoístas, materialistas e imorais.
