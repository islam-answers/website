---
title: Ask a question
description: Send your question to the Islam Answers team. We will do our best to answer it.
extends: _layouts.main
section: body
---

# Ask a question

<iframe
    data-src="https://tally.so/embed/m6v0dY?alignLeft=1&hideTitle=1&transparentBackground=1"
    loading="lazy" width="100%" height="800"
    frameborder="0" marginheight="0" marginwidth="0"
    title="Feedback"
></iframe>

<noscript>
    <iframe
        src="https://tally.so/embed/m6v0dY?alignLeft=1&hideTitle=1&transparentBackground=1"
        loading="lazy" width="100%" height="800"
        frameborder="0" marginheight="0" marginwidth="0"
        title="Feedback"
    ></iframe>
</noscript>

<script async>
  window.addEventListener('load', (event) => {
    document.querySelectorAll('iframe').forEach(iframe => {
      iframe.src = iframe.dataset.src;
    });
  });
</script>
