---
extends: _layouts.answer
section: content
title: Dlaczego muzułmańscy mężczyźni noszą brody?
date: 2025-01-15
description: Muzułmańscy mężczyźni zapuszczają brody, aby podążać za przykładem proroka
  Mahometa, podtrzymywać swoje wrodzone usposobienie i odróżniać się od niewierzących.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 584496
---

Ostatni i najlepszy z Posłańców, Muhammad {{ $page->pbuh() }} pozwolił, aby jego broda rosła, podobnie jak kalifowie, którzy przyszli po nim, jego towarzysze, przywódcy i zwykli ludzie muzułmanów. Taka jest droga Proroków i Posłańców oraz ich zwolenników, i jest to część fitrah (pierwotnej predyspozycji), z którą Allah stworzył ludzi. Aisza (niech Allah będzie z niej zadowolony) przekazała, że Posłaniec Allaha {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:261a') }}

Jeśli Allah i Jego Posłaniec coś nakazali, to od wierzącego wymaga się powiedzenia: Słyszymy i jesteśmy posłuszni, jak powiedział Allah:

{{ $page->verse('24:51') }}

Muzułmańscy mężczyźni zapuszczają brody w posłuszeństwie Allahowi i Jego Posłańcowi. Prorok {{ $page->pbuh() }} nakazał muzułmanom, aby pozwolili swoim brodom rosnąć swobodnie i przycinali wąsy. Zostało przekazane od Ibn 'Umara, że Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn 'Abd al-Barr powiedział: „Zabronione jest golenie brody i nikt tego nie robi, z wyjątkiem mężczyzn zniewieściałych”, tj. tych, którzy naśladują kobiety. Jabir podaje następujące informacje o Proroku Muhammadzie {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2344b') }}

Szejk al-Islam Ibn Tajmija (niech Allah zmiłuje się nad nim) powiedział:

> Koran, Sunna i idżma (konsensus uczonych) wskazują, że powinniśmy różnić się od niewierzących pod każdym względem i nie powinniśmy ich naśladować, ponieważ naśladowanie ich zewnętrznie sprawi, że będziemy naśladować ich w złych uczynkach, nawykach, a nawet w wierzeniach. (...)
