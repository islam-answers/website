---
extends: _layouts.answer
section: content
title: Por que o Islam não permite carne de porco?
date: 2024-12-15
description: Os muçulmanos obedecem a tudo o que Allah lhes ordena e abstêm-se de
  tudo o que Ele lhes proíbe, quer a razão por detrás disso seja clara ou não.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 594172
---

O princípio básico para o muçulmano é obedecer a tudo o que Allah lhe ordena e abster-se de tudo o que Ele lhe proíbe, quer a razão por detrás disso seja clara ou não.

Não é permitido a um muçulmano rejeitar qualquer regra da Shari'ah (lei islâmica) ou hesitar em segui-la se a razão por detrás dela não for clara. Em vez disso, ele deve aceitar as regras sobre halal e haram quando elas são comprovadas no texto, quer ele entenda a razão por trás disso ou não. Allah diz:

{{ $page->verse('33:36') }}

### Por que a carne de porco é haram (proibido)?

A carne de porco é haram (proibido) no Islam, de acordo com o texto do Alcorão, onde Allah diz:

{{ $page->verse('2:173..') }}

Não é permitido a um muçulmano consumi-lo em nenhuma circunstância, exceto em casos de necessidade em que a vida de uma pessoa depende de o comer, como no caso de fome em que uma pessoa teme que vai morrer e não consegue encontrar qualquer outro tipo de alimento.

Os textos da Shari'ah (lei islâmica) não mencionam uma razão específica para a proibição da carne de porco, para além do versículo em que Allah diz:

{{ $page->verse('..6:145..') }}

A palavra rijs (traduzida aqui como "impuro") é utilizada para designar tudo o que é considerado abominável no Islam e de acordo com a natureza humana sã (fitrah). Só esta razão é suficiente.

E há uma razão geral que é dada em relação à proibição de alimentos e bebidas haram (proibida) e similares, que aponta para a razão por trás da proibição da carne de porco. Esta razão geral encontra-se no versículo em que Allah diz:

{{ $page->verse('..7:157..') }}

### Razões científicas e médicas para proibir a carne de porco

A investigação científica e médica provou também que o porco, entre todos os outros animais, é considerado um portador de germes nocivos para o corpo humano. Explicar em pormenor todas estas doenças nocivas levaria muito tempo, mas, resumidamente, podemos enumerá-las como: doenças parasitárias, doenças bacterianas, vírus, etc.

Estes e outros efeitos nocivos indicam que Allah só proibiu a carne de porco por uma razão, que é a de preservar a vida e a saúde, que se encontram entre as cinco necessidades básicas protegidas pela Shari'ah (lei islâmica).
