---
extends: _layouts.answer
section: content
title: Pourquoi les musulmans ne célèbrent-ils pas Noël ?
date: 2024-12-23
description: Les musulmans honorent Jésus comme un grand prophète mais rejettent sa
  divinité. Noël, enraciné dans des croyances non islamiques, n'est pas autorisé en
  Islam.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 586055
---

Les musulmans respectent et honorent Jésus {{ $page->pbuh() }}, le fils de Maryam (Marie), et attendent sa seconde venue. Ils le considèrent comme l'un des plus grands messagers de Dieu à l'humanité. La foi d'un musulman n'est valide que s'il croit en tous les messagers d'Allah, y compris Jésus {{ $page->pbuh() }}.

Mais les musulmans ne croient pas que Jésus était Dieu ou le Fils de Dieu. Ils ne croient pas non plus qu'il ait été crucifié. Allah a envoyé Jésus aux enfants d'Israël pour les appeler à croire en Allah seul et à l'adorer seul. Allah a soutenu Jésus par des miracles qui ont prouvé qu'il disait la vérité.

Allah dit dans le Coran :

{{ $page->verse('19:88-92') }}

### Est-il permis de fêter Noël ?

Compte tenu de ces différences théologiques, les musulmans ne sont pas autorisés à réserver les fêtes des autres religions à l'un ou l'autre de ces rituels ou coutumes. Au contraire, le jour de leur fête est un jour ordinaire pour les musulmans, et ils ne doivent pas le distinguer pour une activité qui fait partie de ce que les non-musulmans font ces jours-là.

La célébration chrétienne de Noël est une tradition qui intègre des croyances et des pratiques qui ne sont pas enracinées dans les enseignements de l'islam. Il n'est donc pas permis aux musulmans de participer à ces célébrations, car elles ne correspondent pas à la conception islamique de Jésus {{ $page->pbuh() }} ou de ses enseignements.

En plus d'être une innovation, elle relève de l'imitation des mécréants dans des domaines qui leur sont propres et qui relèvent de leur religion. Le prophète Muhammad {{ $page->pbuh() }} a dit :

{{ $page->hadith('abudawud:4031') }}

Ibn `Uthaymin (qu'Allah lui fasse miséricorde) a dit :

> Féliciter les mécréants à l’occasion de Noël ou de l’une de leurs autres fêtes religieuses est interdit selon l’avis unanime des savants, car cela implique une approbation de ce qu’ils suivent comme mécréance et une approbation de celle-ci pour eux. Même s’il n’approuve pas cette mécréance pour lui-même, il est interdit au musulman d’approuver les rituels de mécréance ou de féliciter quelqu’un d’autre pour eux. Il n’est pas permis aux musulmans de les imiter de quelque manière que ce soit qui soit propre à leurs fêtes, que ce soit la nourriture, les vêtements, le bain, l’allumage du feu ou l’abstention des travaux ou des adorations habituels, etc. Et il n’est pas permis d’offrir un festin ou d’échanger des cadeaux ou de vendre des objets qui les aident à célébrer leurs fêtes (…) ou de se parer ou de mettre des décorations.

L’Encyclopedia Britannica déclare ce qui suit :

> Le mot Noël vient du l’anglais ancien Cristes maesse, « la messe du Christ ». Il n'existe pas de tradition certaine concernant la date de naissance du Christ. Les chronographes chrétiens du IIIe siècle croyaient que la création du monde avait eu lieu à l'équinoxe de printemps, alors compté comme le 25 mars ; par conséquent, la nouvelle création dans l'incarnation (c'est-à-dire la conception) et la mort du Christ ont dû se produire le même jour, sa naissance ayant eu lieu neuf mois plus tard au solstice d'hiver, le 25 décembre.

> La raison pour laquelle Noël a été célébré le 25 décembre reste incertaine, mais la raison la plus probable est que les premiers chrétiens souhaitaient que la date coïncide avec la fête romaine païenne marquant « l’anniversaire du soleil invaincu » (natalis solis invicti) ; Cette fête célébrait le solstice d’hiver, lorsque les jours recommencent à s’allonger et que le soleil commence à monter plus haut dans le ciel. C’est pourquoi les coutumes traditionnelles liées à Noël se sont développées à partir de plusieurs sources, à la suite de la coïncidence de la célébration de la naissance du Christ avec les observances païennes, agricoles et solaires du milieu de l’hiver. Dans le monde romain, les Saturnales (17 décembre) étaient une période de réjouissances et d’échange de cadeaux.  Le 25 décembre était également considéré comme la date de naissance du dieu mystère iranien Mithra, le soleil de justice. Le jour du Nouvel An romain (1er janvier), les maisons étaient décorées de verdure et de lumières, et des cadeaux étaient offerts aux enfants et aux pauvres.

Ainsi, comme toute personne rationnelle peut le constater, il n'existe aucune base solide pour Noël, et Jésus {{ $page->pbuh() }} ou ses véritables disciples n'ont pas célébré Noël ou n'ont demandé à personne de le faire, et il n'y a eu aucune trace de quiconque se disant chrétien célébrant Noël avant plusieurs centaines d'années après Jésus. Les compagnons de Jésus étaient-ils donc plus justement guidés en ne célébrant pas Noël ou les gens d'aujourd'hui ?

Donc, si vous voulez respecter Jésus {{ $page->pbuh() }} comme le font les musulmans, ne célébrez pas un événement inventé qui a été choisi pour coïncider avec des fêtes païennes et copier des coutumes païennes. Pensez-vous vraiment que Dieu, ou même Jésus lui-même, approuverait ou condamnerait une telle chose ?
