---
extends: _layouts.answer
section: content
title: Jésus est-il le fils de Dieu ?
date: 2025-01-06
description: Jésus est considéré comme un grand prophète et messager de Dieu, mais
  pas Dieu ou le fils de Dieu ; Il est plutôt un serviteur et un messager parmi les
  humains.
sources:
- href: https://islamqa.info/en/answers/26301/
  title: islamqa.info
- href: https://islamqa.info/en/answers/82361/
  title: islamqa.info
translator_id: 586055
---

Les musulmans considèrent Jésus comme un prophète vénéré et un messager de Dieu, né de la Vierge Marie par une naissance miraculeuse. Cependant, la croyance fondamentale qui distingue la conception islamique de la doctrine chrétienne est le rejet de Jésus comme divinité ou comme fils littéral de Dieu.

### L'unicité de Dieu

Le Tawhid (l'unicité de Dieu) est au cœur de la croyance islamique. Il souligne qu'il n'y a pas de divinité en dehors de Dieu, qu'Il n'engendre ni n'est engendré. Par conséquent, attribuer la divinité à Jésus ou lui attribuer le titre de fils de Dieu contredit le principe fondamental du monothéisme en islam.

L'un des principes les plus importants de la croyance en Allah est de déclarer qu'Allah est au-dessus de tous les attributs qui impliquent des défauts. L'un des attributs qui impliquent des défauts et que le musulman doit rejeter est la notion qu'Allah a un fils, car cela implique un besoin et qu'il y a un être qui lui est semblable, et ce sont des questions qu'Allah dépasse de loin. Allah dit dans le Coran :

{{ $page->verse('112:1-4') }}

Le Coran aborde explicitement la question de la conception erronée de la divinité de Jésus, en insistant sur son statut humain et sa servitude envers Dieu. Allah dit :

{{ $page->verse('5:75') }}

Le fait qu'ils aient tous deux mangé signifie qu'ils avaient besoin de nourriture pour se nourrir. Selon les savants, cela implique que Jésus et sa mère devaient se soulager après avoir digéré la nourriture. Allah tout-puissant est bien au-dessus de la dépendance à l'égard de la nourriture ou de la nécessité d'aller aux toilettes. Allah dit également :

{{ $page->verse('4:171') }}

### Examiner la validité des écritures chrétiennes

Les musulmans pensent que les évangiles qui sont aujourd'hui entre les mains des gens, et auxquels croient les chrétiens, ont été altérés et modifiés, et le sont encore de temps à autre, de sorte qu'il ne reste plus rien de la forme sous laquelle l'évangile a été révélé à l'origine par Allah.

L'Évangile qui parle le plus de la croyance en la trinité et en la divinité de Jésus {{ $page->pbuh() }}, et qui est devenu un point de référence pour les chrétiens dans leurs arguments à l'appui de cette fausseté, est l'Évangile de Jean. Cet évangile fait l'objet de doutes quant à sa paternité, même parmi certains érudits chrétiens, ce qui n'est pas le cas des autres évangiles auxquels ils croient.

L'Encyclopaedia Britannica mentionne :

> Quant à l'évangile de Jean, il est sans doute fabriqué. Son auteur a voulu monter deux disciples l'un contre l'autre, à savoir saint Jean et saint Matthieu. 
 > Cet écrivain qui apparaît dans le texte prétendait être le disciple aimé du Messie, et l'Église a pris cela au pied de la lettre et a affirmé que l'écrivain était le disciple Jean, et elle a mis son nom sur le livre, même si l'auteur n'était pas Jean à coup sûr.

### Que signifie « fils de Dieu » dans la Bible ?

La Bible dans laquelle il est dit que Jésus est le fils de Dieu est la même Bible dans laquelle la lignée du Messie se termine avec Adam {{ $page->pbuh() }}, et lui aussi est décrit comme un fils de Dieu :

> Or, Jésus avait environ trente ans lorsqu’il commença son ministère. Il était, croyait-on, fils de Joseph, fils d’Héli, fils de Seth, fils d’Adam, fils de Dieu. (Luc 3:23-38)

C'est la même Bible qui décrit Israël (Jacob) dans les mêmes termes :

> Et tu diras à Pharaon : Voici ce que dit Yahvé : Israël est mon fils premier-né" (Exode 4:22)

Il en va de même pour Solomon {{ $page->pbuh() }}:

> Il me dit : C'est ton fils Salomon qui bâtira ma maison et mes parvis ; car je l'ai choisi pour être mon fils, et je serai pour lui un père. (1 Chroniques 28:6)

Adam, Israël et Salomon étaient-ils tous des fils de Dieu avant Jésus {{ $page->pbuh() }}? Que Dieu soit exalté bien au-dessus de ce qu'ils disent ! En effet, dans l'Évangile de Jean lui-même, il y a une explication de ce que signifie être un fils ; cela inclut tous les serviteurs vertueux de Dieu, donc il n'y a rien d'unique chez Jésus ou chez tout autre prophète à cet égard :

> Mais à tous ceux qui l'ont reçu, à ceux qui croient en son nom, elle a donné le pouvoir de devenir enfants de Dieu. (Jean 1:12)

Quelque chose de similaire apparaît dans l’Évangile de Matthieu :

> Heureux ceux qui procurent la paix, car ils seront appelés fils de Dieu. (Matthieu 5:8-9)

L’emploi du mot « fils » dans le langage biblique est une métaphore pour le serviteur juste de Dieu, sans que cela implique quoi que ce soit de spécial ou d’unique dans la manière dont il a été créé, ou qu’il soit décrit littéralement comme la progéniture de Dieu. C’est pourquoi Jean dit :

> Quel est grand l’amour que le Père nous a témoigné, pour que nous soyons appelés enfants de Dieu! (1 Jean 3:1)

Il reste la question de la description de Jésus {{ $page->pbuh() }} comme fils de Dieu, et de ce qu'ils ont inventé au sujet du Seigneur des mondes, disant qu'il était le père de Jésus, le Messie {{ $page->pbuh() }}. Cela non plus n'est pas unique dans le langage des Évangiles :

> Jésus dit : Ne me retiens pas, car je ne suis pas encore retourné vers le Père. Va plutôt vers mes frères et dis-leur : Je retourne vers mon Père et votre Père, vers mon Dieu et votre Dieu. (Jean 20:17)

Dans ce verset, Jésus dit que Dieu est aussi leur père et qu’Il est le Dieu de tous. Le Coran met l’accent sur l’unicité et la perfection absolues de Dieu, rejetant toute association de partenaires ou de descendants avec Lui. Le terme « fils de Dieu » dans le langage biblique est métaphorique, signifiant la droiture et non la divinité. Par conséquent, les musulmans soutiennent le statut noble de Jésus tout en affirmant l’unicité de Dieu comme étant le cœur de leur foi.
