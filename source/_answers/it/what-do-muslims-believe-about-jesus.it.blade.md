---
extends: _layouts.answer
section: content
title: Cosa credono i musulmani a proposito di Gesù?
date: 2024-05-26
description: I musulmani rispettano e venerano Gesù. Lo considerano uno dei più grandi
  messaggeri di Dio all'umanità.
sources:
- href: https://www.islam-guide.com/it/ch3-10.htm
  title: islam-guide.com
---

I musulmani rispettano e riveriscono Gesù (la pace sia su di lui). Essi lo considerano uno dei più grandi messaggeri di Dio al genere umano. Il Corano conferma la sua nascita virginale e un capitolo del Corano è intitolato ‘Maryam’ (Maria). Il Corano descrive la nascita di Gesù come segue:

{{ $page->verse('3:45-47') }}

Gesù nacque miracolosamente per comando di Dio che creò Adamo senza un padre. Dio disse:

{{ $page->verse('3:59') }}

Durante la sua missione profetica, Gesù attuò molti miracoli. Dio ci disse che Gesù disse:

{{ $page->verse('3:49..') }}

I musulmani non credono che Gesù fu crocifisso. Questo era il piano dei suoi nemici, ma Dio lo salvò e lo portò a Sè. E le fattezze di Gesù furono date a un altro uomo. I nemici di Gesù presero quest'uomo e lo crocifissero, pensando che fosse Gesù. Dio disse:

{{ $page->verse('..4:157..') }}

Né Mohammed {{ $page->pbuh() }} né Gesù riuscirono a cambiare la dottrina di base della fede in un unico Dio, portata dai primi profeti, ma piuttosto la confermarono e la rinnovarono.
