---
extends: _layouts.answer
section: content
title: ¿Quién es el profeta Muhammad?
date: 2024-04-03
description: El Profeta Muhammad (la paz sea con él) fue el ejemplo perfecto de un
  ser humano honesto, justo misericordioso, compasivo, veraz, confiable y valiente.
sources:
- href: https://www.islam-guide.com/es/ch3-8.htm
  title: islam-guide.com
---

Muhammad {{ $page->pbuh() }} nació en la Meca, en el año 570 de la E.C. Siendo que su padre había muerto antes de su nacimiento y su madre poco después de su nacimiento, Muhammad {{ $page->pbuh() }}, fue criado por su tío quien pertenecía a una respetada tribu de Curaish. Fue criado analfabeto, no podía leer o escribir, y así permaneció hasta el día de su muerte. Al crecer, se hizo conocido entre su gente por ser veraz, confiable, generoso y sincero. Era tan veraz que lo llamaban de "el confiable". Muhammad {{ $page->pbuh() }} era muy religioso y siempre detestó la decadencia y la idolatría de su sociedad.

A la edad de 40, Muhammad {{ $page->pbuh() }} recibió su primera revelación a través del Arcángel Gabriel. Las revelaciones continuaron durante veintitrés años y son colectivamente conocidas como el Corán.

Tan pronto como empezó a recitar el Corán y a predicar la verdad que Dios le había revelado, él y su pequeño grupo de seguidores sufrieron la persecución de los incrédulos. Esa persecución se hizo tan feroz que en el año 622, Dios les dio la orden de emigrar. Esa emigración de La Meca a la ciudad de Medinah, a unos 400 Km. al norte, marca el comienzo del calendario musulmán.

Después de varios años, Muhammad {{ $page->pbuh() }} y sus seguidores volvieron a Meca (triunfantes), donde perdonaron a sus enemigos y perseguidores. Antes de que Muhammad {{ $page->pbuh() }} muriera a la edad de 63 años, la mayor parte de la península arábiga se había convertido al Islam y en el transcurso de un siglo después de su muerte, el Islam se había esparcido hacia España en el Occidente y tan lejos como China en el Este. Entre las razones de la rápida y pacífica expansión del Islam, estaba la verdad y claridad de sus doctrinas. El Islam llama a la fe en un solo Dios, que es Él único que merece la adoración.

El Profeta Muhammad {{ $page->pbuh() }} fue el ejemplo perfecto de un ser humano honesto, justo misericordioso, compasivo, veraz, confiable y valiente. Estaba muy lejos de cualquier característica maligna y luchó solamente por la causa de Dios y Su recompensa en la Otra Vida. Más aun, era en todas sus acciones y tratos muy consciente y temeroso de Dios.
