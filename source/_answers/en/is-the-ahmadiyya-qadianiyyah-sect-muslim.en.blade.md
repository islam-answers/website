---
extends: _layouts.answer
section: content
title: Is the Ahmadiyya (Qadianiyyah) sect Muslim?
date: 2025-01-09
description: Ahmadiyya is a misguided group, which is not part of Islam at all. Its
  beliefs are completely contradictory to Islam.
sources:
- href: https://islamqa.info/en/answers/170092/
  title: islamqa.info
- href: https://islamqa.info/en/answers/4060/
  title: islamqa.info
- href: https://islamqa.info/en/answers/45525/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/103015/
  title: islamqa.org (AskImam.org)
---

Ahmadiyya (Qadianiyyah) is a misguided group, which is not part of Islam at all. Its beliefs are completely contradictory to Islam, so Muslims should beware of their activities, since the ‘Ulama’ (scholars) of Islam have stated that they are Kaafirs (non-believers).

Qadianiyyah is a movement that started in 1900 CE as a plot by the British colonialists in the Indian subcontinent, with the aim of diverting Muslims away from their religion and from the obligation of Jihad in particular, so that they would not oppose colonialism in the name of Islam. The mouthpiece of this movement is the magazine Majallat Al-Adyaan (Magazine of Religions) which was published in English.

Mirza Ghulam Ahmad al-Qadiani was the main tool by means of which Qadianiyyah was founded. He was born in the village of Qadian, in the Punjab in India, in 1839 CE. He came from a family that was well known for having betrayed its religion and country, so Ghulam Ahmad grew up loyal and obedient to the colonialists in every sense. Thus he was chosen for the role of a so-called prophet, so that the Muslims would gather around him and he would distract them from waging Jihad against the English colonialists. The British government did lots of favours for them, so they were loyal to the British. Ghulam Ahmad was known among his followers to be unstable, with a lot of health problems and dependent on drugs.

Ghulam Ahmad began his activities as an Islamic daa’iyah (caller to Islam) so that he could gather followers around him, then he claimed to be a mujaddid (renewer) inspired by Allah. Then he took a further step and claimed to be the Awaited Mahdi and the Promised Messiah. Then he claimed to be a Prophet and that his prophethood was higher than that of Muhammad {{ $page->pbuh() }}.

The Qadianis believe that Allah fasts, prays, sleeps, wakes up, writes, makes mistakes and has intercourse – exalted be Allah far above all that they say. They also believe that their God is English because he speaks to them in English. They believe that their book was revealed. Its name is al-Kitaab al-Mubeen and it is different from the Holy Quran. They called for the abolition of Jihad and blind obedience to the British government because, as they claimed, the British were “those in authority” as stated in the Quran. They also allow alcohol, opium, drugs and intoxicants.

Contemporary scholars have unanimously agreed that Qadianis are outside the pale of Islam, because their beliefs include things that constitute disbelief and are contrary to the fundamental teachings of Islam. This sect has gone against the definitive consensus of the Muslims that there is no Prophet after our Prophet Muhammad {{ $page->pbuh() }}; this is indicated by a number of texts of the Quran and saheeh Sunnah.

Most of the Qadianis nowadays live in India and Pakistan, with a few in Occupied Palestine “Israel” and the Arab world. They are trying, with the help of the colonialists, to obtain sensitive positions in all the places where they live. The British government is also supporting this movement and making it easy for their followers to get positions in world governments, corporate administration and consulates. Some of them are also high-ranking officers in the secret services. In calling people to their beliefs, the Qadianis use all kinds of methods, especially educational means, because they are highly-educated and there are many scientists, engineers and doctors in their ranks.

It is impermissible for a Muslim to perform Salaah (prayer) with Jama’ah (congregation) behind an Imam (prayer leader) belonging to the Ahmedi sect since they are classified as non-Muslims. Muslims should also abstain from attending their places of worship and gatherings since they are non-believers. It is also not permissible for a Muslim to marry one of them or to give his daughter in marriage to them, because they are non-believers and apostates.
