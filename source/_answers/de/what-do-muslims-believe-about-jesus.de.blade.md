---
extends: _layouts.answer
section: content
title: Was glauben Muslime über Jesus?
date: 2024-04-11
description: Muslime respektieren und verehren Jesus (Friede sei mit ihm). Sie betrachten
  ihn als einen der größten Gesandten Gottes zu den Menschen.
sources:
- href: https://www.islam-guide.com/de/ch3-10.htm
  title: islam-guide.com
---

Muslime respektieren und verehren Jesus {{ $page->pbuh() }}. Sie betrachten ihn als einen der größten Gesandten Gottes zu den Menschen. Der Quran bekräftigt seine Geburt aus einer Jungfrau und ein Kapitel im Quran trägt die Überschrift „Maryam“ (Maria). Der Quran beschreibt Jesus Geburt folgendermaßen:

{{ $page->verse('3:45-47') }}

Jesus wurde auf wunderbare Weise geboren - mit dem Befehl Gottes, der auch Adam ohne einen Vater hervorgebracht hat. Gott sagt:

{{ $page->verse('3:59') }}

Während seiner prophetischen Berufung vollbrachte Jesus viele Wunder. Gott berichtet uns, dass Jesus sagte:

{{ $page->verse('3:49..') }}

Muslime glauben nicht, dass Jesus gekreuzigt wurde. Es war der Plan seiner Feinde, ihn zu kreuzigen, aber Gott errettete ihn und erhob ihn zu Sich. Ein ähnliches Aussehen wie Jesus wurde einem anderem Mann gegeben. Jesus’ Feinde ergriffen diesen Mann und kreuzigten ihn und dachten er wäre Jesus. Gott sagt:

{{ $page->verse('..4:157..') }}

Muhammad {{ $page->pbuh() }} und Jesus kamen nicht, um die Grundlehre, den Glauben an einen Gott, die die früheren Propheten brachten, zu ändern, sondern um sie zu kräftigen und zu erneuern.
