---
extends: _layouts.answer
section: content
title: Le donne sono oppresse nell'Islam?
date: 2024-11-17
description: L'Islam promuove l'uguaglianza delle donne e condanna l'oppressione. Le pratiche culturali, non l'Islam, spesso causano interpretazioni errate.
sources:
- href: https://www.islamweb.net/en/article/109366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 573107
---

Tra gli argomenti di maggiore interesse per i non musulmani vi è la condizione delle donne nell'Islam e il tema dei loro diritti, o meglio, la percezione diffusa che non ne abbiano. Il ritratto che i media fanno delle donne musulmane, sottolineandone erroneamente "l'oppressione e il riserbo", sembra contribuire a questa percezione negativa.

Il motivo principale è che le persone non fanno distinzione tra cultura e religione, due cose completamente diverse. Infatti, l'Islam condanna l'oppressione di qualsiasi tipo, sia nei confronti della donna che dell'umanità in generale.

Il Corano è il libro sacro fondamentale nella vita dei musulmani. Questo libro fu rivelato 1400 anni fa a un uomo di nome Muhammad {{ $page->pbuh() }} che sarebbe poi diventato Profeta, che Allah esalti il suo nome. Sono trascorsi quattordici secoli e questo libro da allora non è stato cambiato, non una lettera è stata alterata.

Nel Corano, Allah l'Altissimo dice:

{{ $page->verse('33:59') }}

Questo versetto dimostra che l'Islam rende necessario indossare l'Hijab. Hijab ha il significato di coprire, ma non indica solo il velo (come alcuni potrebbero pensare), ma anche indossare abiti larghi con colori non troppo vivaci.

A volte, le persone vedono donne musulmane coperte e pensano che si tratti di oppressione. Questo è sbagliato. Una donna musulmana non è oppressa, anzi, è liberata. Questo perché non viene più valutata per qualcosa di materiale, come il suo bell'aspetto o la forma del suo corpo. Una donna musulmana costringe gli altri a giudicarla per la sua intelligenza, gentilezza, onestà e personalità. Così facendo, le persone la giudicano per quello che è veramente.

Quando le donne musulmane si coprono i capelli e indossano abiti larghi, stanno obbedendo agli ordini del loro Signore di essere modeste e pudiche, non ai costumi culturali o sociali. Infatti, le suore cristiane si coprono i capelli per pudore, eppure nessuno le considera "oppresse". Seguendo il comando di Allah, le donne musulmane fanno esattamente la stessa cosa.

La vita delle persone che hanno messo in pratica il Corano è cambiata drasticamente. Ha avuto un impatto enorme su tante persone, soprattutto sulle donne, poiché questa è stata la prima volta che l'anima dell'uomo e quella della donna sono state dichiarate uguali, con gli stessi obblighi e le stesse ricompense.

L'Islam è una religione che tiene in grande considerazione le donne. Molto tempo fa, quando nascevano dei bambini maschi, portavano grande gioia alla famiglia. Al contrario, la nascita di una bambina era accolta con molta meno gioia ed entusiasmo. A volte, le bambine erano odiate a tal punto da essere sepolte vive. L'Islam si è sempre opposto a questa discriminazione irrazionale nei confronti delle bambine e dell'infanticidio femminile.

## Nel corso dei secoli, l'Islam ha trascurato i diritti delle donne?

Per quel che concerne la salvaguardia di questi diritti nel corso dei secoli, i principi di base sono rimasti sempre gli stessi, ma ciò che è cambiata è la loro applicazione. Indubbiamente, durante l'età d'oro dell'Islam, i musulmani applicavano maggiormente la shari'ah (legge islamica) del loro Signore, le cui sentenze includono onorare la propria madre e trattare con modi gentili la moglie, la figlia, la sorella e le donne in generale. Più la devozione religiosa diventava debole, più questi diritti venivano trascurati, ma fino al Giorno della Resurrezione continuerà ad esistere un gruppo di fedeli che aderisce alla propria religione e applica la shari'ah (leggi) del proprio Signore. Queste sono le persone che onorano di più le donne e concedono loro i propri diritti.

Oggigiorno, nonostante la debolezza della devozione religiosa di molti musulmani, le donne godono ancora di uno status elevato, che siano figlie, mogli o sorelle. Riconosciamo però che ci sono carenze, ingiustizie e negligenze nei confronti dei diritti delle donne tra alcune persone, ma ognuno risponderà per se stesso.

L'Islam è una religione che tratta le donne in modo equo. Alla donna musulmana sono stati attribuiti, 1400 anni fa, un ruolo, dei doveri e dei diritti di cui la maggior parte delle donne non gode nemmeno oggi in Occidente. Questi diritti provengono da Dio e sono concepiti per mantenere un equilibrio nella società; ciò che può sembrare "ingiusto" o "mancante" in un posto è compensato o spiegato in un altro.
