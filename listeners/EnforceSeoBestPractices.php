<?php

namespace App\Listeners;

use Exception;
use TightenCo\Jigsaw\Jigsaw;

class EnforceSeoBestPractices
{
    public function handle(Jigsaw $jigsaw)
    {
        if ($jigsaw->getEnvironment() === "production") {
            return;
        }

        $violations = collect();

        $answers = $jigsaw
            ->getCollection("answers");

        $answers->each(function ($answer) use (&$violations) {
            $title = $answer->title;
            $description = $answer->description;

            if (($titleOverflow = mb_strlen($title) - 60) > 0) {
                $violations->add([
                    "title" => $title,
                    "message" => "{$titleOverflow} characters should be removed from the title."
                ]);
            }

            if (($descriptionOverflow = mb_strlen($description) - 160) > 0) {
                $violations->add([
                    "title" => $title,
                    "message" => "{$descriptionOverflow} characters should be removed from the description."
                ]);
            }
        });

        if ($violations->isNotEmpty()) {
            printf("Error: %s SEO best practices violations found.\n", $violations->count());

            $violations->each(function ($violation) {
                printf(" - %s: %s\n", $violation["title"], $violation["message"]);
            });
        }
    }
}
