---
extends: _layouts.answer
section: content
title: Gli Ahmadiyya (Qadianiyyah) sono musulmani?
date: 2025-01-25
description: Gli Ahmadiyya sono un gruppo fuorviato, che non fa assolutamente parte
  dell'Islam. Le sue idee e il suo credo sono totalmente in contraddizione con l'Islam.
sources:
- href: https://islamqa.info/en/answers/170092/
  title: islamqa.info
- href: https://islamqa.info/en/answers/4060/
  title: islamqa.info
- href: https://islamqa.info/en/answers/45525/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/103015/
  title: islamqa.org (AskImam.org)
translator_id: 573107
---

Gli Ahmadiyya (Qadianiyyah) sono un gruppo fuorviato, che non fa parte dell'Islam. Il loro credo è in contraddizione con l'Islam, quindi i musulmani devono diffidare delle pratiche eseguite da questo gruppo, anche perché gli 'Ulama' (studiosi) dell'Islam hanno dichiarato che gli Ahmadiyya sono Kaafir (non credenti).

Il movimento Qadianiyyah ha avuto inizio nel 1900 d.C. a seguito di un piano subdolo organizzato dai colonialisti britannici nel subcontinente indiano. Questo progetto coloniale aveva l'obiettivo di distogliere i musulmani dalla loro religione e in particolare dall'obbligo della Jihad, in modo che non si opponessero al colonialismo in nome dell'Islam. Il portavoce di questo movimento è la rivista Majallat Al-Adyaan (Rivista delle religioni), pubblicata in inglese.

Mirza Ghulam Ahmad al-Qadiani fu il personaggio principale attraverso il quale venne fondato il gruppo Qadianiyyah. Mirza era nato nel villaggio di Qadian, nel Punjab, India, nel 1839 d.C. Proveniva da una famiglia conosciuta per aver tradito la propria religione e il proprio paese, e così anche Ghulam Ahmad crebbe fedele e obbediente ai colonialisti. Pertanto, fu scelto per il ruolo di presunto profeta, in modo tale che i musulmani si raccogliessero attorno a lui mentre lui li avrebbe distratti dal condurre la Jihad contro i colonialisti inglesi. Il governo britannico li ricambiò con molti favori e per questo rimasero sempre leali agli inglesi. Ghulam Ahmad era noto tra i suoi seguaci per avere un carattere instabile, con gravi problemi di salute e dipendente dalle droghe.

Ghulam Ahmad iniziò la sua attività come daa'iyah (colui che chiama all'Islam) in modo da raccogliere il maggior numero di seguaci attorno a sé, poi affermò di essere un mujaddid (innovatore) ispirato da Allah. Quindi fece un ulteriore passo in avanti affermando di essere il Mahdi atteso e il Messia promesso. Infine, dichiarò di essere un profeta e che la sua profezia era superiore rispetto a quella di Mohammad {{ $page->pbuh() }}.

I Qadiani credono che Allah digiuni, preghi, dorma, si svegli, scriva, commetta errori e abbia rapporti sessuali - che Allah sia Glorificato e Lodato ben al di sopra di tutto ciò che dicono. Credono anche che il loro Dio sia inglese perché parla loro in lingua inglese. Credono che il loro libro sia stato rivelato. Il suo nome è al-Kitaab al-Mubin ed è diverso dal Sacro Corano. Hanno sempre chiesto l'abolizione della Jihad e l'obbedienza cieca al governo britannico perché, come sostenevano, gli inglesi erano "coloro che detenevano l'autorità", come affermato nel Corano. Consentono anche il consumo di alcol, oppio, droghe e sostanze intossicanti.

Gli studiosi musulmani contemporanei concordano all'unanimità sul fatto che i Qadiani sono al di fuori dell'Islam perché il loro credo include elementi che costituiscono miscredenza e che sono contrari agli insegnamenti fondanti dell'Islam. Questa setta è andata decisamente contro il consenso unanime dei musulmani secondo cui non esisterà nessun altro Profeta dopo il nostro Profeta Muhammad {{ $page->pbuh() }}, come confermato da numerosi versi del Corano e narrazioni della Sunnah sahih.

Attualmente, la maggior parte dei Qadiani vive in India e Pakistan, alcuni nella Palestina occupata (Israele) e nel mondo arabo. Aiutati e supportati dalle potenze coloniali, i Qadiani cercano di ottenere posizioni chiave e di rilievo in tutti i posti in cui vivono. Anche il governo britannico sta supportando questo movimento e favorendolo affinché i suoi seguaci ottengano incarichi decisivi nei governi mondiali, nelle amministrazioni aziendali e nei consolati. Alcuni di loro sono anche ufficiali di alto rango nei servizi segreti. Nel chiamare le persone al loro credo, i Qadiani usano qualsiasi metodo, e in particolare gli strumenti educativi perché spesso si tratta di persone molto istruite e tra di loro vi sono molti scienziati, ingegneri e medici.

Non è permesso pregare la Salah (preghiera) con la Jama'ah (congregazione) guidata da un Imam appartenente alla setta Ahmadiyyah poiché è considerata come non musulmana. Inoltre, i musulmani non devono frequentare i loro luoghi di culto e le loro riunioni poiché si tratta di non credenti. Il musulmano non può sposare qualcuno appartenente a questa setta o dare loro la propria figlia in sposa perché si tratta di non credenti e apostati.
