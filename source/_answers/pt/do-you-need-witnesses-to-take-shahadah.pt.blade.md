---
extends: _layouts.answer
section: content
title: São necessárias testemunhas para dizer a Shahadah?
date: 2024-07-13
description: Não é essencial que se declare o Islam diante de testemunhas para que
  seja válido (o testemunho de fé).
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
translator_id: 554943
---

Para que uma pessoa se torne muçulmana, não é essencial que ela declare seu Islam diante de outra pessoa. O Islam é um assunto entre uma pessoa e seu Senhor, glorificado e exaltado seja.

Se é solicitado às pessoas que testemunhem seu Islam para que se possa documentar e constar em seus documentos pessoais, não há nada de errado nisso, mas isso deve ser feito sem que se torne uma condição de validação do Islam de alguém.
