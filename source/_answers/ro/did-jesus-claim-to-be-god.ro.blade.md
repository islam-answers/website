---
extends: _layouts.answer
section: content
title: A pretins Iisus că este Dumnezeu?
date: 2024-10-29
description: Musulmanii cred că Iisus a fost un profet care i-a îndemnat pe oameni
  să se închine doar lui Allah, fără să pretindă că ar fi Dumnezeu.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
translator_id: 554943
---

Musulmanii cred că Iisus {{ $page->pbuh() }} a fost unul dintre robii lui Allah și unul dintre nobilii Săi Mesageri. Allah l-a trimis pe Iisus la Israeliți (descendenții lui Iacob) pentru a-i chema să creadă numai în Allah și să se închine numai Lui.

Allah l-a sprijinit pe Iisus {{ $page->pbuh() }} cu minuni care au dovedit că el spunea adevărul; Iisus s-a născut din Fecioara Maria (Maryam) fără tată. El le-a permis evreilor unele dintre lucrurile care le fuseseră interzise. Iisus nu a murit și dușmanii săi, evreii, nu l-au ucis, ci Allah l-a salvat de ei și l-a ridicat la cer viu. Iisus le-a spus adepților săi despre venirea Profetului nostru Muhammad {{ $page->pbuh() }}. Iisus se va întoarce la sfârșitul timpurilor. În Ziua Judecății, el se va dezvinovăți de afirmațiile conform cărora ar fi fost un Dumnezeu.

În Ziua Judecății, Iisus va sta în fața Stăpânului Lumilor, care îl va întreba în fața martorilor despre ce le-a spus Israeliților, după cum spune Allah:

{{ $page->verse('5:116-118') }}

O examinare onestă arată că Iisus {{ $page->pbuh() }} nu a spus niciodată în mod explicit și fără echivoc că ar fi Dumnezeu. Autorii Noului Testament, dintre care niciunul nu l-a întâlnit pe Isus, au fost cei care au afirmat că el era Dumnezeu și au propagat acest lucru. Nu există nicio dovadă că apostolii reali ai lui Isus au afirmat acest lucru sau că ei au scris Biblia. Mai degrabă, Iisus se închina constant lui Dumnezeu și îi chema pe alții să se închine numai lui Dumnezeu.

Afirmația că Iisus {{ $page->pbuh() }} este Dumnezeu nu poate fi atribuită lui Iisus, ea contrazicând în mod atât de flagrant Prima Poruncă de a nu lua alți dumnezei în afară de Dumnezeu și de a nu face un idol din nimic din cer și de pe pământ. De asemenea, este absurd din punct de vedere logic ca Dumnezeu să devină creația Sa.

Astfel, doar afirmația Islamului este plauzibilă și în concordanță cu revelația timpurie: Isus {{ $page->pbuh() }} a fost un profet, nu Dumnezeu.
