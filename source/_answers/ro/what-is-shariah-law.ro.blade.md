---
extends: _layouts.answer
section: content
title: Ce este legea Shariah?
date: 2024-07-14
description: Shariah se referă la întreaga religie a islamului, pe care Allah a ales-o pentru robii Săi, pentru a-i conduce din adâncurile întunericului către lumină.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 554943
---

Cuvântul Shari'ah se referă la întreaga religie a islamului, pe care Allah a ales-o pentru robii Săi, pentru a-i conduce din adâncurile întunericului către lumină. Se referă, de asemenea, la porunci și interdicții, halal și haram.

Cel care urmează legea Shari'ah (a lui Allah), considerând permis ceea ce El a permis și considerând interzis ceea ce El a interzis, va avea triumf.

Cel care se opune legii Shari'ah (a lui Allah) este expus furiei, mâniei și pedepsei lui Allah.

Allah, fie ca El să fie înălțat, spune:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad a spus:

> Cuvântul Shari'ah (pl. sharai') se referă la ceea ce Allah le-a poruncit (shara'a) oamenilor în ceea ce privește chestiunile religioase, precum ar fi rugăciunea, postul, pelerinajul (Hajj) și așa mai departe. Este shir'ah (locul din râu unde se poate bea).

Ibn Hazm a spus:

> Shari'ah este ceea ce Allah, fie ca El să fie înălțat, a poruncit (shara'a) pe buzele Profetului Său {{ $page->pbuh() }} cu privire la religie și pe buzele profeților care au venit înaintea sa. Hotărârea textului de abrogare (anulare) trebuie considerată hotărâre definitivă.

### Originea lingvistică a termenului Shari'ah

Originea lingvistică a termenului Shari'ah se referă la locul în care un călător poate veni să bea apă și locul din râu unde se poate bea. Allah, fie ca El să fie înălțat, spune:

{{ $page->verse('42:13') }}
