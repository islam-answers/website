---
extends: _layouts.answer
section: content
title: Jésus a-t-il prétendu être Dieu ?
date: 2024-11-16
description: Les musulmans croient que Jésus était un prophète qui a appelé les gens
  à adorer Allah seul, sans jamais prétendre être Dieu.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
translator_id: 586055
---

Les musulmans croient que Jésus {{ $page->pbuh() }} était l'un des serviteurs d'Allah et l'un de Ses nobles messagers. Allah a envoyé Jésus aux enfants d'Israël pour les appeler à croire en Allah seul et à L'adorer seul.

Allah a soutenu Jésus {{ $page->pbuh() }} par des miracles qui ont prouvé qu'il disait la vérité. Jésus est né de la Vierge Marie sans père. Il a permis aux Juifs certaines des choses qui leur avaient été interdites, il n'est pas mort et ses ennemis, les Juifs, ne l'ont pas tué, mais Allah l'a sauvé d'eux et l'a élevé vivant au ciel. Jésus a annoncé à ses disciples la venue de notre Prophète Muhammad {{ $page->pbuh() }}. Jésus reviendra à la fin des temps. Il reniera lui-même, le Jour de la Résurrection, les prétentions selon lesquelles il était un Dieu.

Le Jour de la Résurrection, Jésus se tiendra devant le Seigneur des Mondes, qui lui demandera devant témoins ce qu'il a dit aux Enfants d'Israël, comme Allah le dit :

{{ $page->verse('5:116-118') }}

Un examen honnête montre que Jésus {{ $page->pbuh() }} n'a jamais dit explicitement et sans équivoque qu'il était Dieu. Ce sont les auteurs du Nouveau Testament, dont aucun n'a rencontré Jésus, qui ont affirmé qu'il était Dieu et qui ont propagé cette affirmation. Il n'y a aucune preuve que les apôtres réels de Jésus aient affirmé cela, ou qu'ils aient écrit la Bible. Au contraire, Jésus est montré comme adorant constamment Dieu seul et appelant les autres à l'adorer.

L'affirmation selon laquelle Jésus {{ $page->pbuh() }} est Dieu ne peut être attribuée à Jésus, si elle est en contradiction flagrante avec le premier commandement de ne pas prendre d'autres dieux que Dieu, ni de faire une idole de quoi que ce soit dans les cieux et sur la terre. De plus, il est logiquement absurde que Dieu devienne sa création.

Ainsi, seule la revendication de l'islam est plausible et cohérente avec la révélation ancienne : Jésus {{ $page->pbuh() }} était un prophète, pas Dieu.
