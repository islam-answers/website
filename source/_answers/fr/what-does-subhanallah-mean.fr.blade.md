---
extends: _layouts.answer
section: content
title: Que signifie Subhanallah ?
date: 2024-10-07
description: Communément traduit par "Gloire à Allah", il apparaît dans le Coran afin
  de nier les déficiences et les défauts attribués à Allah, et d'affirmer sa perfection.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 586055
---

Le terme arabe "tasbih" fait généralement référence à la phrase "SubhanAllah". Il s'agit d'une phrase qu'Allah a approuvée pour lui-même, qu'il a inspirée à ses anges et qu'il a guidée pour qu'elle soit prononcée par les plus excellents de ses créatures. "SubhanAllah" est généralement traduit par "Gloire à Allah", mais cette traduction ne rend pas tout à fait compte de la signification du tasbih.

Le tasbih est composé de deux mots : Subhana et Allah. Linguistiquement, le mot « subhana » vient du mot « sabh », qui signifie distance, éloignement. Ibn 'Abbas explique donc cette expression comme la pureté et l'absolution d'Allah au-dessus de tout mal ou de toute chose inconvenante. En d'autres termes, Allah est toujours loin et éternellement au-dessus de toute faute, de toute déficience, de tout manquement et de toute inconvenance.

Cette phrase apparaît dans le Coran afin d'infirmer les descriptions inappropriées et incompatibles attribuées à Allah :

{{ $page->verse('23:91') }}

Un seul tasbih suffit à annuler chaque défaut ou mensonge attribué à Allah en tout lieu et en tout temps par l'une ou l'autre des créations. Ainsi, Allah réfute de nombreux mensonges qui lui sont attribués avec un seul tasbih :

{{ $page->verse('37:149-159') }}

Le but de la négation est d'affirmer son contraire. Par exemple, lorsque le Coran nie l'ignorance d'Allah, le musulman nie l'ignorance et affirme à son tour le contraire d'Allah, c'est-à-dire la connaissance globale. Le tasbih est fondamentalement une négation (des déficiences, des actes répréhensibles et des fausses déclarations) et, par conséquent, selon ce principe, il entraîne l'affirmation de son contraire, à savoir une existence magnifique, une conduite parfaite et une vérité absolue.

Sa'd ibn Abou Waqqas (qu'Allah l'agrée) a rapporté :

{{ $page->hadith('muslim:2698') }}
