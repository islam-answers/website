---
extends: _layouts.answer
section: content
title: Warum wurde der 14. Februar als Valentinstag gewählt?
date: 2025-02-12
description: Der Valentinstag am 14. Februar löste das heidnische Fest Lupercalia
  ab und wurde später mit dem Heiligen Valentin in Verbindung gebracht.
sources:
- href: https://muslimskeptic.com/2022/02/14/the-dark-origins-of-valentines-day-muslims-might-be-unaware-of/
  title: muslimskeptic.com
- href: https://muslimskeptic.com/2022/02/15/its-not-just-christmas-how-christianity-adopts-paganism-for-valentines-day/
  title: muslimskeptic.com
- href: https://islamqa.info/en/answers/73007/
  title: islamqa.info
- href: https://islamqa.info/en/articles/65/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145893/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21694/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1130/
  title: islamqa.info
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 587746
---

### Was ist der Ursprung des Valentinstags?

Um die Ursprünge des Valentinstags zu verstehen, müssen wir uns zunächst seine Geschichte ansehen. Historiker sind sich einig, dass es sich bei ihm um eine Mischung aus zwei historischen Ereignissen handelt. An dem einen war ein christlicher Priester (der heilige Valentin) beteiligt, an dem anderen das heidnische Fest Lupercalia.

Lupercalia war ein blutiges, gewalttätiges und sexuell aufgeladenes Fest voller Tieropfer, zufälliger Verheiratung und Paarung in der Hoffnung, böse Geister und Unfruchtbarkeit abzuwehren. Dieses abscheuliche Fest wurde jährlich am 15. Februar gefeiert, nur einen Tag nach dem heutigen Valentinstag. Es war ursprünglich als Februa bekannt, was „Reinigungen“ oder „Säuberungen“ bedeutet, was die Grundlage für den Namen des Monats Februar ist.

Es begann damit, dass sich eine Gruppe römischer Priester, die Luperci genannt wurden, an einem bestimmten Ort versammelten, zum Beispiel in der Höhle von Lupercal. Diese Luperci opferten dann eine Ziege und einen Hund, offensichtlich im Namen ihrer eigenen falschen Gottheiten. Das Opfer der Ziege sollte Fruchtbarkeit symbolisieren. Die Luperci rannten dann wild durch die Stadt und peitschten alle Frauen aus, die ihnen über den Weg liefen. Dies war wiederum als Symbol der Fruchtbarkeit gedacht. Man glaubt, dass diese Frauen diese Auspeitschung begrüßten, in der Hoffnung, fruchtbar zu werden. Es gibt jedoch historische Berichte, dass dieser Teil des Rituals nicht immer so einvernehmlich war. Das „romantische“ Ritual endete damit, dass sich junge Männer und Frauen paarten. Mit anderen Worten, dies war eine Nacht der Unzucht.

Die Verschmelzung dieses Festes mit der Geschichte des Heiligen Valentin, die heute von vielen als der eigentliche Ursprung dieses Anlasses angesehen wird, war höchstwahrscheinlich ein Schritt der katholischen Kirche, um das Christentum für Heiden attraktiver zu machen. Der Heilige Valentin ist ein Name, der zwei der antiken „Märtyrer“ der christlichen Kirche gegeben wurde. Man sagte, es habe zwei von ihnen gegeben oder nur einen. Als die Römer das Christentum annahmen, feierten sie ihr Fest namens „Fest der Liebe“ weiterhin, änderten jedoch das heidnische Konzept der „spirituellen Liebe“ zu einem anderen Konzept namens „Märtyrer der Liebe“, repräsentiert durch den Heiligen Valentin, der sich für Liebe und Frieden eingesetzt hatte und für den er ihren Behauptungen zufolge den Märtyrertod starb. Es wurde auch das Fest der Liebenden genannt und der Heilige Valentin galt als Schutzpatron der Liebenden.

Einer ihrer Irrglauben im Zusammenhang mit diesem Fest war, dass die Namen von Mädchen, die das heiratsfähige Alter erreicht hatten, auf kleine Papierrollen geschrieben und in einer Schale auf einen Tisch gelegt wurden. Dann wurden die jungen Männer, die heiraten wollten, aufgerufen, und jeder von ihnen wählte ein Stück Papier aus. Er stellte sich für ein Jahr in den Dienst des Mädchens, dessen Namen er gezogen hatte, damit sie sich gegenseitig kennenlernen konnten. Dann heirateten sie, oder sie wiederholten den gleichen Vorgang am Tag des Festes im folgenden Jahr. Der christliche Klerus wandte sich gegen diese Tradition, die seiner Ansicht nach einen verderblichen Einfluss auf die Moral der jungen Männer und Frauen hatte.

Zu den Ursprüngen dieses Feiertags wurde auch gesagt, dass der römische Kaiser Claudius II. im dritten Jahrhundert n. Chr., als die Römer christlich wurden, nachdem das Christentum weit verbreitet war, verfügte, dass die Soldaten nicht heiraten sollten, weil die Heirat sie von den Kriegen ablenken würde, in denen sie kämpften. Der Heilige Valentin widersetzte sich diesem Erlass und begann, im Geheimen Ehen für die Soldaten zu schließen. Als der Kaiser dies herausfand, warf er ihn ins Gefängnis und verurteilte ihn zur Hinrichtung. Im Gefängnis verliebte er sich in die Tochter des Gefängniswärters, aber das war ein Geheimnis, denn nach den christlichen Gesetzen war es Priestern und Mönchen verboten, zu heiraten oder sich zu verlieben. Dennoch wird er von den Christen hoch geschätzt, weil er standhaft am Christentum festhielt, als der Kaiser ihm anbot, ihn zu begnadigen, wenn er dem Christentum abschwöre und die römischen Götter anbete; dann wäre er einer seiner engsten Vertrauten und er würde ihn zu seinem Schwiegersohn machen. Valentin lehnte dieses Angebot jedoch ab und zog das Christentum vor. Deshalb wurde er am 14. Februar 270 n. Chr. hingerichtet, am Vorabend des 15. Februar, dem Fest des Lupercalis. Daher wurde dieser Tag nach diesem Heiligen benannt.

### Warum feiern Muslime den Valentinstag nicht?

Man könnte fragen: Warum feiern Muslime dieses Fest nicht? Zunächst einmal sind Feste Teil der religiösen Zeremonien, von denen Allah im Quran sagt:

{{ $page->verse('..5:48..') }}

Allah sagt auch:

{{ $page->verse('22:67..') }}

Da der Valentinstag auf die Römerzeit zurückgeht und nicht auf die islamische Zeit, bedeutet dies, dass er ausschließlich dem Christentum und nicht dem Islam vorbehalten ist und die Muslime daran keinen Anteil haben. Der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:892') }}

Das bedeutet, dass sich jede Nation durch ihre Feste unterscheiden sollte. Wenn die Christen ein Fest haben und die Juden ein Fest, das ausschließlich ihnen gehört, dann sollte kein Muslim mitmachen, genauso wie er weder ihre Religion noch ihre Gebetsrichtung teilt.

Zweitens bedeutet das Feiern des Valentinstags, den heidnischen Römern zu ähneln oder sie zu imitieren. Es ist Muslimen nicht erlaubt, Nichtmuslime in Dingen zu imitieren, die nicht Teil des Islam sind. Der Prophet {{ $page->pbuh() }} sagte:

{{ $page->hadith('abudawud:4031') }}

Drittens ist es ein Fehler, den Namen des Tages mit den wahren Absichten zu verwechseln. Die Liebe, auf die an diesem Tag Bezug genommen wird, ist die romantische Liebe, die sich Mätressen und Liebhaber, Freunde und Freundinnen nimmt. Es ist bekannt, dass dieser Tag ein Tag der Promiskuität und des Sex ist, ohne Einschränkungen oder Begrenzungen. Es geht nicht um die reine Liebe zwischen einem Mann und seiner Frau oder einer Frau und ihrem Mann, oder zumindest wird nicht zwischen der legitimen Liebe in der Beziehung zwischen Mann und Frau und der verbotenen Liebe von Geliebten und Liebhabern unterschieden.

Im Islam liebt ein Ehemann seine Frau das ganze Jahr über und bringt seine Liebe ihr gegenüber durch Geschenke zum Ausdruck, in Versen und Prosa, in Briefen und auf andere Weise, das ganze Jahr über – nicht nur an einem Tag im Jahr. Der Prophet {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:1468') }}

Es gibt keine Religion, die ihre Anhänger mehr dazu ermutigt, einander zu lieben und füreinander zu sorgen, als der Islam. Dies gilt zu allen Zeiten und unter allen Umständen. Der Prophet {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:54') }}

### Wer profitiert vom Valentinstag?

Der Valentinstag hat sich zu einem stark kommerzialisierten Feiertag mit zahlreichen Traditionen und Bräuchen entwickelt. Die Menschen tauschen Karten mit Liebesgedichten aus, veranstalten Feste und machen ihren Liebsten Geschenke. Die kommerziellen Auswirkungen sind beträchtlich – allein in Großbritannien haben die Blumenverkäufe 22 Millionen Pfund erreicht. Der Schokoladenkonsum steigt dramatisch an, während sich die Rosenpreise verzehnfachen können, von 1 auf 10 Dollar pro Stiel. Geschenkartikelläden und Kartengeschäfte konkurrieren intensiv mit spezialisierten Valentinstagsartikeln, und manche Familien schmücken ihre Häuser zu diesem Anlass sogar mit roten Rosen. Die wirtschaftliche Tragweite des Valentinstags ist enorm – im Jahr 2020 gaben die Amerikaner laut der National Retail Federation (NRT), dem weltweit größten Einzelhandelsverband, 27,4 Milliarden Dollar für Valentinstagsgeschenke aus. Letztendlich sind es die Hersteller und Einzelhändler, die am meisten von diesem kommerzialisierten Fest der Liebe profitieren.
