---
extends: _layouts.answer
section: content
title: Sobre o que é o Alcorão?
date: 2024-06-02
description: O Alcorão, a última palavra revelada de Deus, é a fonte primária de toda
  a crença e prática do muçulmano.
sources:
- href: https://www.islam-guide.com/pt/ch3-7.htm
  title: islam-guide.com
---

O Alcorão, a última palavra revelada de Deus, é a fonte primária de toda a crença e prática do muçulmano. Ele lida com todos os assuntos que interessam aos seres humanos: sabedoria, doutrina, adoração, transações, lei, etc., mas seu tema básico é a relação entre Deus e Suas criaturas. Ao mesmo tempo, ele provê orientações e ensinamentos detalhados para uma sociedade justa, conduta humana adequada, e um sistema econômico eqüitativo.

Note que o Alcorão foi revelado a Muhammad {{ $page->pbuh() }} apenas em árabe. Portanto, qualquer tradução do Alcorão, seja em inglês ou outro idioma qualquer, não é o Alcorão nem uma versão do Alcorão, mas apenas uma tradução do significado do Alcorão. O Alcorão existe apenas no árabe, no qual foi revelado.
