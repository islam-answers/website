<!DOCTYPE html>
<html lang="{{ $page->getLanguage() }}" dir="ltr" @yield('html')>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{ $page->description ?? $page->siteDescription }}">

    <meta property="og:site_name" content="{{ $page->siteName }}">
    <meta property="og:title" content="@yield('title', ($page->title ? $page->title . ' - ' : '') . $page->siteName)" />
    <meta property="og:type" content="{{ $page->type ?? 'website' }}" />
    <meta property="og:url" content="{{ $page->getCanonicalUrl() }}" />
    <meta property="og:description" content="{{ $page->description ?? $page->siteDescription }}" />

    <title>@yield('title', ($page->title ? $page->title . ' - ' : '') . $page->siteName)</title>

    <link rel="home" href="{{ $page->baseUrl }}">
    <link rel="canonical" href="{{ $page->getCanonicalUrl() }}">
    <link href="/answers/feed.atom" type="application/atom+xml" rel="alternate" title="{{ $page->siteName }} Atom Feed">

    <link rel="icon" href="/favicon.ico?v=1712141015">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#1e40af">
    <meta name="msapplication-TileColor" content="#ffc40d">
    <meta name="theme-color" content="#ffffff">

    @php ($defaultImage = '/assets/img/default.jpg')
    <meta property="og:image" content="@yield('image', $defaultImage)">
    <meta itemprop="image" content="@yield('image', $defaultImage)">
    <meta name="image" content="@yield('image', $defaultImage)">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:image" content="@yield('image', $defaultImage)">

    @stack('head')

    <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
</head>

<body class="flex flex-col justify-between min-h-screen bg-gray-100 text-gray-800 leading-normal font-sans">
    <div id="app">
        @include('_layouts.header')

        <main role="main" class="flex-auto w-full container max-w-4xl mx-auto py-16 px-6" dir="{{ $page->getDirection() }}">
            @yield('body')
        </main>

        @include('_layouts.footer')
    </div>

    @stack('scripts')

    <!-- 100% privacy-first analytics -->
    <script data-collect-dnt="true" defer src="https://scripts.simpleanalyticscdn.com/latest.js"></script>
</body>

</html>
