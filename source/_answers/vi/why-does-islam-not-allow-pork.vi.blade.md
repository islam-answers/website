---
extends: _layouts.answer
section: content
title: Tại sao Hồi giáo không cho phép thịt lợn?
date: 2024-09-08
description: Người Hồi giáo tuân thủ mọi điều Allah truyền lệnh cho họ và tránh mọi
  điều Ngài cấm, bất kể lý do đằng sau đó có rõ ràng hay không.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 587785
---

Nguyên tắc cơ bản đối với người Hồi giáo là phải tuân theo bất cứ điều gì Allah truyền lệnh và tránh bất cứ điều gì Ngài cấm, bất kể lý do đằng sau đó có rõ ràng hay không.

Người Hồi giáo không được phép từ chối bất kỳ phán quyết nào của luật Shari'ah hoặc ngần ngại tuân theo nếu lý do đằng sau đó không rõ ràng. Tốt hơn, người ấy phải chấp nhận các phán quyết về halal và haram khi chúng đã được chứng minh bằng văn bản, bất kể người ấy có hiểu lý do đằng sau đó hay không. Allah phán:

{{ $page->verse('33:36') }}

### Tại sao thịt lợn lại bị coi là haram?

Theo kinh Quran, thịt heo là haram (bị cấm) trong đạo Hồi, trong đó Allah phán rằng:

{{ $page->verse('2:173..') }}

Người Hồi giáo không được phép sử dụng nó trong bất kỳ trường hợp nào, ngoại trừ trường hợp cần thiết khi mạng sống của một người phụ thuộc vào việc ăn nó, chẳng hạn như trong trường hợp một người sợ rằng mình sẽ chết đói và không thể tìm thấy bất kỳ loại thực phẩm nào khác.

Trong các văn bản Shari'ah không đề cập đến lý do cụ thể cho lệnh cấm thịt heo, ngoại trừ câu mà Allah phán:

{{ $page->verse('..6:145..') }}

Từ rijs (dịch ra là 'không tinh khiết') được dùng để chỉ bất cứ thứ gì bị coi là ghê tởm trong Hồi giáo và trái với sự lành mạnh vốn có của con người (fitrah). Chỉ riêng lý do này thôi cũng đủ.

Và có một lý do chung đằng sau lệnh cấm thịt lợn, được đưa ra liên quan đến lệnh cấm thức ăn, đồ uống haram và những thứ tương tự. Lý do chung này có thể được tìm thấy trong câu mà Allah phán:

{{ $page->verse('..7:157..') }}

### Cấm thịt heo vì lý do khoa học và y tế.

Nghiên cứu khoa học và y học cũng đã chứng minh rằng trong số tất cả các loài động vật, heo được coi là vật mang mầm bệnh có hại cho cơ thể con người. Giải thích chi tiết tất cả các bệnh có hại này sẽ mất quá nhiều thời gian, nhưng tóm lại chúng ta có thể liệt kê chúng như sau: bệnh ký sinh trùng, bệnh do vi khuẩn, vi-rút, v.v.

Những tác hại này cùng những tác hại khác cho thấy rằng Allah chỉ cấm thịt heo vì một lý do, đó là để bảo vệ sự sống và sức khỏe, vốn là một trong năm nhu cầu cơ bản được luật Shari'ah bảo vệ.
