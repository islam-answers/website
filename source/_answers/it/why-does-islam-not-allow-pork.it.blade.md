---
extends: _layouts.answer
section: content
title: Perché l’Islam non permette di consumare la carne di maiale?
date: 2024-07-21
description: I musulmani obbediscono ad Allah e si astengono da ciò che Egli proibisce, anche se le motivazioni non sono del tutto comprensibili all’uomo.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 573107
---

Il principio fondamentale per il musulmano è che egli obbedisce a tutto ciò che Allah gli ordina e si astiene da tutto ciò che Egli gli proibisce, anche se le motivazioni di ciò non sono comprensibili all'uomo.

Chi è musulmano non può rifiutare una norma prevista dalla Shari'ah o avere esitazioni nel seguirla anche se non ne comprende appieno il motivo. Piuttosto, deve accettare le sentenze che riguardano quel che è halal e haram quando sono dimostrate nel testo, sia che ne comprenda o meno le ragioni. Allah dice:

{{ $page->verse('33:36') }}

### Perché la carne di maiale è haram?

La carne di maiale è haram (proibita) nell'Islam secondo il Corano, nel quale Allah dice:

{{ $page->verse('2:173..') }}

Chi è musulmano non può consumare la carne di maiale in nessun caso. Si fa eccezione nei casi di estrema necessità, quando la vita di una persona dipende dal suo consumo, come nel caso si soffra la fame, rischiando di morire, e non si riesca a trovare nessun altro tipo di cibo.

Nei testi della Shari'ah non vi è alcuna menzione della ragione specifica per cui sia proibito il consumo della carne di maiale, a parte il versetto in cui Allah dice:

{{ $page->verse('..6:145..') }}

La parola rijs (qui tradotta con 'impuro') viene utilizzata per indicare tutto ciò che nell'Islam è considerato ripugnante, ma anche secondo la naturale indole umana (fitrah). Questa ragione, da sola, è più che sufficiente.

E vi è una motivazione generale che viene fornita a proposito della proibizione del consumo di cibi e bevande haram e simili, che spiega il motivo per cui è stata vietata la carne di maiale. Questa spiegazione si trova nel versetto in cui Allah dice:

{{ $page->verse('..7:157..') }}

### Motivi scientifici e medici per il divieto della carne di maiale

Ricerche scientifiche e mediche hanno inoltre dimostrato che il maiale, tra tutti gli altri animali, è considerato un vettore di germi dannosi per l'organismo umano. Spiegare nel dettaglio tutte queste malattie dannose richiederebbe troppo tempo, ma in breve possiamo elencarle: malattie parassitarie, malattie batteriche, virus e così via.

Questi ed altri effetti dannosi indicano che Allah ha proibito la carne di maiale solo per un motivo, ovvero preservare la vita e la salute che sono tra i cinque bisogni primari tutelati dalla Shari'ah.
