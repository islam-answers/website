---
extends: _layouts.answer
section: content
title: Ko može postati musliman?
date: 2024-10-11
description: Svako ljudsko biće može postati musliman, bez obzira na prethodnu veru,
  dob, nacionalnost ili etničku pripadnost.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 584497
---

Svako ljudsko biće može postati musliman, bez obzira na prethodnu veru, dob, nacionalnost ili etničku pripadnost. Da biste postali musliman, jednostavno izgovorite sljedeće riječi: “Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah”. Ovo svedočanstvo vere znači: “Svedočim da nema boga vrijednog obožavanja osim Allaha i da je Muhammed Njegov poslanik”. Morate to reći i verovati u to.

Kada izgovorite ovu rečenicu, automatski postajete musliman. Ne trebaš pitati za vrijeme ili sat, noću ili danju, da se obratiš svome Gospodaru i započneš svoj novi život. Bilo kada je vreme za to.

Ne treba vam ničije dopuštenje, ni svećenik da vas krsti, ni posrednik da posreduje za vas, ni iko da vas uputi do Njega, jer vas On, slavljen neka je On, sam vodi. Zato se Njemu obratite, jer On je blizu; Bliži vam je nego što mislite ili možete zamisliti:

{{ $page->verse('2:186') }}

Allah, dž.š., kaže:

{{ $page->verse('6:125..') }}
