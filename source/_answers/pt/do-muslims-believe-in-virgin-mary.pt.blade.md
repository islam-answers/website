---
extends: _layouts.answer
section: content
title: Os muçulmanos acreditam na Virgem Maria?
date: 2024-12-16
description: Maryam é mencionada no Alcorão como uma pessoa de fé. Os muçulmanos respeitam-na
  e acreditam na sua pureza e inocência.
sources:
- href: https://islamqa.info/en/answers/241999/
  title: islamqa.info
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/32915/
  title: islamqa.org (SeekersGuidance.org)
translator_id: 594172
---

Maryam (Maria), filha de 'Imraan, que Allah esteja satisfeito com ela, é uma das adoradoras devotas e mulheres virtuosas. É mencionada no Alcorão, onde é descrita como possuindo as caraterísticas das pessoas de fé. Ela era uma crente cuja fé, afirmação da Unicidade de Allah e obediência a Ele eram perfeitas.

O Alcorão descreve-a como uma verdadeira escrava de Allah, humilde e obediente a Allah, o Senhor do Universo. Deus, exaltado seja, diz:

{{ $page->verse('3:42-43') }}

### A inocência de Maryam

Os muçulmanos respeitam Maryam e acreditam no que é mencionado no Alcorão Sagrado sobre ela, nomeadamente a sua pureza e inocência em relação àquilo de que os inimigos de Allah a acusaram:

{{ $page->verse('4:156') }}

No Alcorão, há um capítulo inteiro com o nome de Maryam, que transmite maravilhosamente a história do nascimento virginal. Na visão islâmica, Maryam era virgem; nunca tinha sido casada antes ou durante o tempo do nascimento de Jesus {{ $page->pbuh() }}. Maryam ficou bastante chocada quando o anjo a informou de que daria à luz um filho, dizendo:

{{ $page->verse('19:20') }}

As fontes islâmicas não referem um noivado de Maryam, nem mencionam José, nem um casamento posterior, nem quaisquer irmãos de Jesus {{ $page->pbuh() }}.

Entre os muçulmanos, ela é a mulher muçulmana perfeita, uma das mais virtuosas das mulheres do Paraíso, como disse o Profeta Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2431') }}

Noutra narração, o Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:3432') }}

### O nascimento milagroso de Jesus - um sinal para toda a humanidade

Os muçulmanos acreditam que Allah fez dela e do seu filho um sinal para toda a humanidade e um testemunho da Unicidade, Senhorio e Poder de Allah. Deus, exaltado seja, diz:

{{ $page->verse('23:50..') }}

Ele também diz:

{{ $page->verse('3:45') }}

Allah também adverte aqueles que tomam Jesus e a sua mãe, Maria, como divindades, associando-os a Allah:

{{ $page->verse('5:75..') }}

Allah, glorificado e exaltado seja, disse-nos que, no Dia da Ressurreição, o Profeta de Allah 'Isa ibn Maryam (Jesus, filho de Maria) {{ $page->pbuh() }} repudiará o politeísmo daqueles que o associam a Allah e exageram acerca dele e da sua mãe. Reflictamos sobre o sublime intercâmbio que terá lugar entre o Senhor da Glória e da Majestade e o Seu servo e Mensageiro 'Isa ibn Maryam {{ $page->pbuh() }}. Deus, exaltado seja, diz:

{{ $page->verse('5:116-117') }}
