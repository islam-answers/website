---
extends: _layouts.answer
section: content
title: Do you need witnesses to take Shahadah?
date: 2024-07-12
description: It is not essential to take shahadah (say the testimony of faith) before witnesses. Islam is a matter that is between a person and his Lord.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
---

In order for a person to become Muslim, it is not essential for him to declare his Islam before anyone. Islam is a matter that is between a person and his Lord, may He be blessed and exalted.

If he asks people to bear witness to his Islam so that it may be documented among his personal documents, there is nothing wrong with that, but it should be done without making that a condition of his Islam being valid.
