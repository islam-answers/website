---
title: Ask a question
description: Send your question to the Islam Answers team. We will do our best to answer it.
extends: _layouts.main
permalink: ask/thanks.html
section: body
---

# Ask a question

Thank you for sending your question! We will do our best to answer it as soon as possible.
