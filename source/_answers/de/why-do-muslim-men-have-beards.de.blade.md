---
extends: _layouts.answer
section: content
title: Warum haben muslimische Männer Bärte?
date: 2025-02-09
description: Muslime tragen Bärte nach dem Vorbild des Propheten Muhammad und zur
  Wahrung ihrer natürlichen Veranlagung.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 587746
---

Der letzte und beste der Gesandten, Muhammad {{ $page->pbuh() }}, ließ seinen Bart wachsen, ebenso wie die Kalifen, die nach ihm kamen, und seine Gefährten sowie die Führer und das einfache Volk der Muslime. Dies ist der Weg der Propheten und Gesandten und ihrer Anhänger, und es ist Teil der Fitrah (ursprünglichen Veranlagung), mit der Allah die Menschen erschaffen hat. Aisha (möge Allah mit ihr zufrieden sein) überlieferte, dass der Gesandte Allahs {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:261a') }}

Wenn Allah und Sein Gesandter etwas angeordnet haben, ist vom Gläubigen zu sagen: Wir hören und gehorchen, wie Allah sagt:

{{ $page->verse('24:51') }}

Muslimische Männer lassen sich aus Gehorsam gegenüber Allah und Seinem Gesandten einen Bart wachsen. Der Prophet {{ $page->pbuh() }} befahl den Muslimen, ihre Bärte frei wachsen zu lassen und ihre Schnurrbärte zu stutzen. Ibn 'Umar berichtete, dass der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn 'Abd al-Barr sagte: „Es ist verboten, den Bart zu rasieren, und niemand tut dies, außer weibischen Männern“, d. h. solchen, die Frauen imitieren. Jabir berichtet folgendes über den Propheten Muhammad {{ $page->pbuh() }} :

{{ $page->hadith('muslim:2344b') }}

Shaykh al-Islam Ibn Taymiyah (möge Allah ihm gnädig sein) sagte:

> Der Quran, die Sunnah und die Ijma' (Gelehrtenkonsens) weisen alle darauf hin, dass wir uns in allen Aspekten von den Ungläubigen unterscheiden und sie nicht nachahmen dürfen, denn wenn wir sie nach außen hin nachahmen, werden wir sie in ihren schlechten Taten und Gewohnheiten und sogar im Glauben nachahmen. (...)
