---
extends: _layouts.answer
section: content
title: Ce sunt alimentele halal?
date: 2024-03-23
description: Alimentele "Halal" sunt cele permise de Dumnezeu pentru musulmani, principalele
  excepții fiind porcul și alcoolul.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 554943
---

Alimentele "Halal" (care în limba arabă înseamnă "legale") sunt alimentele permise de Dumnezeu pentru ca musulmanii să le consume. În general, majoritatea alimentelor și băuturilor sunt considerate Halal, principalele excepții fiind porcul și alcoolul. Carnea (de exemplu, de vită și de pasăre) trebuie să fie sacrificată în mod uman și corect, ceea ce include menționarea numelui lui Dumnezeu înainte de sacrificare și reducerea la minimum a suferinței animalelor.
