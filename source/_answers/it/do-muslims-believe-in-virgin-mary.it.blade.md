---
extends: _layouts.answer
section: content
title: I musulmani credono nella Vergine Maria (Maryam)?
date: 2025-01-26
description: Maria è menzionata nel Corano come persona di grande fede. I musulmani
  la rispettano e credono nella sua purezza e innocenza.
sources:
- href: https://islamqa.info/en/answers/241999/
  title: islamqa.info
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/32915/
  title: islamqa.org (SeekersGuidance.org)
translator_id: 573107
---

Maryam (Maria), figlia di 'Imran, che Allah si compiaccia di lei, è una donna di fede devota e retta. È menzionata nel Corano dove viene descritta come una persona che possiede le caratteristiche dei credenti. Era una credente la cui fede, l'affermazione dell'Unicità di Allah e l'obbedienza a Lui erano perfette.

Il Corano la descrive come una vera serva di Allah, umile e obbediente ad Allah, il Signore dei Mondi. Allah, che Egli sia glorificato, dice:

{{ $page->verse('3:42-43') }}

### L'innocenza di Maria

I musulmani rispettano Maria e credono in ciò che afferma il Sacro Corano, ossia che lei era pura e innocente nonostante ciò di cui i nemici di Allah l'avevano accusata:

{{ $page->verse('4:156') }}

Nel Corano c'è un intero capitolo (titolato "Maryam") dedicato a Maria che racconta perfettamente la storia della nascita verginale. Secondo la concezione islamica, Maria era vergine; non si era mai sposata né prima né durante il periodo della nascita di Gesù {{ $page->pbuh() }}. Maria rimase profondamente scioccata quando l'angelo la informò che avrebbe partorito un figlio, dicendo:

{{ $page->verse('19:20') }}

Le fonti islamiche non riportano alcun fidanzamento per Maria, né menzionano Giuseppe, né un successivo matrimonio, né fratelli per Gesù {{ $page->pbuh() }}.

Secondo i musulmani, lei è la donna credente perfetta, una delle più virtuose tra le donne del Paradiso, come disse il Profeta Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2431') }}

In un'altra narrazione, il Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:3432') }}

### La nascita miracolosa di Gesù - un messaggio per tutta l'umanità

I musulmani credono che Allah abbia reso lei e suo figlio un messaggio per tutta l'umanità e una testimonianza dell'Unicità, della Signoria e della Potenza di Allah. Allah, che Egli sia glorificato, dice:

{{ $page->verse('23:50..') }}

Egli dice inoltre:

{{ $page->verse('3:45') }}

Allah mette in guardia anche coloro che considerano Gesù e sua madre Maria delle divinità, associandoli ad Allah:

{{ $page->verse('5:75..') }}

Allah, che Egli sia glorificato ed esaltato, ci ha detto che nel Giorno della Resurrezione il Profeta di Allah 'Isa ibn Maryam (Gesù figlio di Maria) {{ $page->pbuh() }} sconfesserà il politeismo di coloro che lo associano ad Allah e che oltrepassano i limiti nei confronti suoi e di sua madre. Riflettiamo a proposito di questo dialogo eccelso che avverrà tra il Signore della Gloria e della Maestà e il Suo servo e Messaggero 'Isa ibn Maryam {{ $page->pbuh() }}. Allah, che Egli sia lodato, dice:

{{ $page->verse('5:116-117') }}
