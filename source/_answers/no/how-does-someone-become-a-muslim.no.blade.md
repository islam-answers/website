---
extends: _layouts.answer
section: content
title: Hvordan blir man muslim?
date: 2024-06-15
description: 'Man konverterer til islam og blir muslim ved å si de følgende ord med
  overbevisning: ”La ilaha illa Allah, Mohammador rasoolo Allah”.'
sources:
- href: https://www.islam-guide.com/no/ch3-6.htm
  title: islam-guide.com
---

Man konverterer til islam og blir muslim ved å si de følgende ord med overbevisning: ”La ilaha illa Allah, Mohammador rasoolo Allah”. Disse ordene betyr ”Det finnes ingen annen sann guddom foruten Gud (Allah), og Mohammad er Guds budbringer (profet).” Den første delen, ”Det finnes ingen annen sann guddom foruten Gud”, betyr at ingen har rett til å bli tilbedt unntatt Gud alene, og at Gud ikke har noen partner og heller ingen sønn. For å være muslim bør man også:

- Tro at den hellige Koranen er de ordrette ord fra Gud, åpenbart av Ham.
- Tro at dommedagen (dagen for gjenoppvekkelse) er sann, og at denne vil komme slik som Gud har lovet i Koranen.
- Akseptere islam som sin religion.
- Ikke tilbe noe annet eller noen annen foruten Gud.

Profeten Mohammad {{ $page->pbuh() }} sa:

{{ $page->hadith('muslim:2747') }}
