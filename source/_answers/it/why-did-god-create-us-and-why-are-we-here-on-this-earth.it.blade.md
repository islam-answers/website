---
extends: _layouts.answer
section: content
title: Perché Dio ci ha creati e perché siamo su questa terra?
date: 2024-11-01
description: Uno degli scopi principali per cui Dio ci ha creati è obbedire al Suo comando di affermare la Sua Unicità e adorare soltanto Lui, senza alcun partner.
sources:
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/108100/
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.info/en/answers/45529/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/127330/
  title: islamqa.org (AskImam.org)
translator_id: 573107
---

Uno degli attributi principali di Allah è la saggezza, e uno dei Suoi nomi più importanti è al-Hakim (il Più Saggio). Egli non ha creato nulla invano, sia lodato Allah al di sopra di ogni cosa. Egli crea tutto per motivi nobili e saggi, e per scopi sublimi. Allah afferma nel Corano:

{{ $page->verse('23:115-116') }}

Allah dice inoltre nel Corano:

{{ $page->verse('44:38-39') }}

Così come è dimostrato che esiste una saggezza dietro la creazione dell'uomo dal punto di vista della shari'ah (legge islamica), lo stesso vale anche dal punto di vista della ragione. L'uomo saggio accetta il fatto che ogni cosa sia stata creata per uno scopo, e l'uomo saggio nella sua vita non agisce senza un motivo, quindi che dire di Allah, il Più Saggio tra i saggi?

Allah non ha creato l'uomo per mangiare, bere e moltiplicarsi, nel qual caso si comporterebbe come gli animali. Allah ha onorato l'uomo e lo ha favorito molto al di sopra di altri esseri che Egli ha creato, ma molti si ostinano a non credere, ignorando o negando la vera saggezza che sta dietro la loro creazione, e tutto ciò che li interessa è godere dei piaceri di questo mondo. La vita di queste persone è come quella degli animali, e in effetti sono ancora più sviati. Allah dice:

{{ $page->verse('..47:12') }}

Uno degli scopi principali per cui Allah ha creato gli uomini - tra le sfide più grandi - è per obbedire al Suo comando di affermare la Sua Unicità e di adorare Lui Solo, senza associarGli nessun'altro. Allah ha indicato questo motivo, riguardo alla creazione degli uomini, quando afferma:

{{ $page->verse('51:56') }}

Ibn Kathir (che Allah abbia misericordia di lui) disse:

"Ciò significa: 'Li ho creati per comandar loro di adorarMi, non perché Io abbia bisogno di loro'".

Come musulmani, il nostro scopo nella vita è importantissimo, ma semplice. Ci sforziamo di praticare tutti gli aspetti del nostro essere, esteriore e interiore, in completa conformità con ciò che Allah ci ha comandato. Allah non ci ha creati perché ha bisogno di noi. Egli è al di sopra di ogni necessità, e siamo noi ad aver bisogno di Allah.

Allah ci ha detto che la creazione dei cieli e della terra, della vita e della morte, hanno anche lo scopo di mettere alla prova l'uomo. Chiunque Gli obbedisca, Egli lo ricompenserà, e chiunque Gli disobbedisca, Egli lo punirà. Allah dice:

{{ $page->verse('67:2') }}

Con questa prova si manifestano i nomi e gli attributi di Allah, come al-Rahman (il Compassionevole), al-Ghafur (il Perdonatore), al-Hakim (il Saggio), al-Tawwab (Colui Che Accetta il Pentimento), al-Rahim (il Misericordioso) e altri nomi di Allah.
