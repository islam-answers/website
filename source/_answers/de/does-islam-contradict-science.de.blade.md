---
extends: _layouts.answer
section: content
title: Widerspricht der Islam der Wissenschaft?
date: 2024-08-30
description: Der Quran enthält wissenschaftliche Erkenntnisse, die erst durch den
  Fortschritt moderner Technologie und Wissenschaft in unserer Zeit entdeckt wurden.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 587746
---

Der Quran, das heilige Buch des Islam, ist die endgültige Offenbarung Gottes an die Menschheit und das letzte in der Reihe der göttlichen Offenbarungen, die den Propheten zuteilwurden.

Obwohl der Quran (der vor über 1400 Jahren offenbart wurde) nicht in erster Linie ein wissenschaftliches Buch ist, enthält er doch wissenschaftliche Erkenntnisse, die erst durch den Fortschritt in Technologie und Forschung kürzlich entdeckt wurden. Der Islam fördert Reflexion und wissenschaftliche Forschung, da das Verständnis der Natur der Schöpfung den Menschen ermöglicht, ihren Schöpfer und die Weite Seiner Macht und Weisheit noch mehr zu schätzen.

Der Koran wurde in einer Zeit offenbart, als die Wissenschaft noch in ihren Anfängen steckte; es gab weder Teleskope noch Mikroskope oder vergleichbare Technologien wie heute. Die Menschen glaubten, dass die Sonne die Erde umkreise und der Himmel von Säulen an den Ecken einer flachen Erde gestützt werde. Vor diesem Hintergrund wurde der Koran offenbart und enthält zahlreiche wissenschaftliche Fakten zu Themen wie Astronomie, Biologie, Geologie und Zoologie.

Zu den vielen wissenschaftlichen Fakten im Quran gehören unter anderem:

### Fakt Nr. 1 – Ursprung des Lebens

{{ $page->verse('21:30') }}

Wasser wird im Koran als Ursprung allen Lebens bezeichnet. Alle lebenden Dinge bestehen aus Zellen, und wir wissen heute, dass Zellen überwiegend aus Wasser bestehen. Diese Erkenntnis wurde erst nach der Erfindung des Mikroskops gemacht. In den Wüsten Arabiens wäre es unvorstellbar gewesen, dass jemand ohne diese wissenschaftlichen Mittel hätte vermuten können, dass alles Leben aus Wasser hervorgeht.

### Fakt Nr. 2 - Embryonale Entwicklung des Menschen

Gott spricht über die Stadien der embryonalen Entwicklung des Menschen:

{{ $page->verse('23:12-14') }}

Das arabische Wort "alaqah" hat drei Bedeutungen: Blutegel, etwas Hängendes und ein Blutgerinnsel. "Mudghah" bezeichnet eine gekaute Substanz. Wissenschaftler der Embryologie haben festgestellt, dass die Verwendung dieser Begriffe zur Beschreibung der Embryonalentwicklung präzise ist und mit dem aktuellen wissenschaftlichen Verständnis des Entwicklungsprozesses übereinstimmt.

Bis ins 20. Jahrhundert war nur wenig über die Stadien und Klassifizierung menschlicher Embryonen bekannt, was bedeutet, dass die Beschreibungen des menschlichen Embryos im Quran nicht auf dem wissenschaftlichen Wissen des siebten Jahrhunderts basieren können.

### Fakt Nr. 3 – Ausdehnung des Universums

Zu einer Zeit, als die Wissenschaft der Astronomie noch in ihren Anfängen steckte, wurde folgender Vers im Quran offenbart:

{{ $page->verse('51:47') }}

Eine der beabsichtigten Bedeutungen des obigen Verses ist, dass Gott das Universum (d.h. die Himmel) ausdehnt. Weitere Bedeutungen sind, dass Gott für das Universum sorgt und Macht über es hat – was ebenfalls zutrifft.

Die Erkenntnis, dass sich das Universum ausdehnt und die Planeten sich zunehmend voneinander entfernen, wurde erst im letzten Jahrhundert gemacht. Der Physiker Stephen Hawking beschreibt in seinem Buch Eine kurze Geschichte der Zeit: „Die Entdeckung, dass sich das Universum ausdehnt, war eine der bedeutendsten geistigen Revolutionen des zwanzigsten Jahrhunderts.“

Der Quran deutet auf die Ausdehnung des Universums hin – und das lange bevor das Teleskop erfunden wurde!

### Fakt Nr. 4 – Herabgesandtes Eisen

Eisen ist nicht von Natur aus auf der Erde entstanden, sondern gelangte aus dem Weltraum auf unseren Planeten. Wissenschaftler haben herausgefunden, dass die Erde vor Milliarden von Jahren von Meteoriten getroffen wurde, die Eisen aus explodierten, weit entfernten Sternen zur Erde brachten.

{{ $page->verse('..57:25..') }}

Gott verwendet das Wort „herabgesandt“. Die Tatsache, dass Eisen aus dem Weltraum zur Erde gelangte, war etwas, das die primitive Wissenschaft des siebten Jahrhunderts nicht wissen konnte.

### Fakt Nr. 5 – Der Schutz des Himmels

Der Himmel spielt eine entscheidende Rolle dabei, die Erde und ihre Bewohner vor den gefährlichen Strahlen der Sonne sowie der eiskalten Leere des Weltraums zu schützen.

Im folgenden Vers fordert uns Gott auf, den Himmel zu betrachten:

{{ $page->verse('21:32') }}

Der Quran verweist auf den Schutz des Himmels als Zeichen Gottes, dessen schützende Eigenschaften durch wissenschaftliche Forschungen im zwanzigsten Jahrhundert entdeckt wurden.

### Fakt Nr. 6 - Berge

Gott lenkt unsere Aufmerksamkeit auf eine wichtige Eigenschaft der Berge:

{{ $page->verse('78:6-7') }}

Der Koran beschreibt die tiefen Wurzeln der Berge präzise, indem er das Wort „Hacken“ verwendet. Zum Beispiel beträgt die Höhe des Mount Everest etwa 9 km über dem Erdboden, während seine Wurzeln mehr als 125 km tief reichen!

Die Tatsache, dass Berge tiefe, „pflugartige“ Wurzeln besitzen, war erst nach der Entwicklung der Plattentektonik-Theorie zu Beginn des 20. Jahrhunderts bekannt. Auch sagt Gott im Quran (16:15), dass die Berge eine Rolle bei der Stabilisierung der Erde spielen „…damit sie nicht erschüttert werde“, was erst allmählich von Wissenschaftlern verstanden wird.

### Fakt Nr. 7 - Die Umlaufbahn der Sonne

Im Jahr 1512 stellte der Astronom Nikolaus Kopernikus seine Theorie auf, dass die Sonne unbeweglich im Zentrum des Sonnensystems steht und die Planeten sich um sie drehen. Diese Überzeugung war unter Astronomen weit verbreitet, bis zum zwanzigsten Jahrhundert. Heute ist es eine allgemein anerkannte Tatsache, dass die Sonne nicht stationär ist, sondern sich in einer Umlaufbahn um das Zentrum unserer Milchstraße bewegt.

{{ $page->verse('21:33') }}

### Fakt Nr. 8 - Interne Wellen im Ozean

Früher glaubte man, dass Wellen nur an der Oberfläche des Ozeans auftreten. Doch Ozeanographen haben entdeckt, dass es auch unterhalb der Oberfläche innere Wellen gibt, die für das menschliche Auge unsichtbar sind und nur mit spezialisierter Ausrüstung nachgewiesen werden können.

Der Quran erwähnt:

{{ $page->verse('24:40..') }}

Diese Beschreibung ist erstaunlich, weil es vor 1400 Jahren keine spezialisierten Geräte existierten, um die inneren Wellen tief im Ozean zu erfassen.

### Fakt #9 - Lügen und Bewegung

Zur Zeit des Propheten Muhammad {{ $page->pbuh() }} lebte ein grausamer und unterdrückerischer Stammesführer. Gott offenbarte ihm einen Vers, um ihn zu warnen:

{{ $page->verse('96:15-16') }}

Gott nennt diese Person nicht direkt einen Lügner, sondern bezeichnet ihre Stirn (den vorderen Teil des Gehirns) als „lügend“ und „sündhaft“ und warnt sie, damit aufzuhören. Zahlreiche Studien haben gezeigt, dass der vordere Teil unseres Gehirns (der Frontallappen) sowohl für das Lügen als auch für willentliche Bewegungen – und damit auch für Sünden – verantwortlich ist. Diese Funktionen wurden durch medizinische Bildgebung entdeckt, die erst im 20. Jahrhundert entwickelt wurde.

### Fakt Nr. 10 - Die zwei Meere, die sich nicht vermischen

Bezüglich der Meere sagt unser Schöpfer:

{{ $page->verse('55:19-20') }}

Eine physikalische Kraft, die als Oberflächenspannung bekannt ist, verhindert das Vermischen der Gewässer benachbarter Meere aufgrund ihrer unterschiedlichen Dichte. Es ist, als ob eine unsichtbare Barriere zwischen ihnen existiert. Diese Entdeckung wurde erst kürzlich von Ozeanographen gemacht.

### Konnte Muhammad den Quran nicht selbst verfasst haben?

Der Prophet Muhammad {{ $page->pbuh() }} war in der Geschichte als Analphabet bekannt; er konnte weder lesen noch schreiben und verfügte über keine Bildung, die die wissenschaftlichen Genauigkeiten des Qurans erklären würde.

Manche behaupten, er habe diese Erkenntnisse von Gelehrten oder Wissenschaftlern seiner Zeit abgeschrieben. Wäre dies der Fall, hätten wir erwarten können, dass auch die fehlerhaften wissenschaftlichen Annahmen der damaligen Zeit übernommen worden wären. Stattdessen finden wir im Quran keinerlei Fehler – weder wissenschaftlicher noch anderer Art.

Manche Leute behaupten auch, dass der Quran geändert wurde, als neue wissenschaftliche Erkenntnisse entdeckt wurden. Dies kann jedoch nicht zutreffen, da historisch belegt ist, dass der Koran in seiner Originalsprache bewahrt wurde – was an sich bereits ein Wunder ist.

### Nur ein Zufall?

{{ $page->verse('41:53') }}

Während sich diese Antwort nur auf die wissenschaftlichen Wunder konzentriert hat, gibt es viele weitere Arten von Wundern, die im Quran erwähnt werden: historische Wunder, Prophezeiungen, die sich erfüllt haben, sprachliche und literarische Stile, die unerreicht bleiben, ganz zu schweigen von der bewegenden Wirkung auf die Menschen. All diese Wunder können nicht dem Zufall zugeschrieben werden. Sie deuten eindeutig darauf hin, dass der Quran von Gott, dem Schöpfer aller wissenschaftlichen Gesetze, stammt. Er ist derselbe Gott, der alle Propheten mit derselben Botschaft gesandt hat – den einen Gott allein anzubeten und den Lehren seines Gesandten zu folgen

Der Quran ist ein Buch der Führung, das zeigt, dass Gott die Menschen nicht geschaffen hat, um ziellos umherzuirren. Vielmehr lehrt er uns, dass wir einen bedeutungsvollen und höheren Zweck im Leben haben – nämlich die vollkommene Perfektion, Größe und Einzigartigkeit Gottes anzuerkennen und Ihm zu gehorchen.

Es liegt an jedem Einzelnen, seinen von Gott gegebenen Verstand und seine Fähigkeiten zur Vernunft zu nutzen, um über die Zeichen Gottes nachzudenken und sie zu erkennen – wobei der Quran das wichtigste Zeichen ist. Lese und entdecke die Schönheit und Wahrheit des Qurans, damit du Erfolg finden kannst!
