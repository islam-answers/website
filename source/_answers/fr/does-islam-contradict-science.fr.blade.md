---
extends: _layouts.answer
section: content
title: L'islam contredit-il la science ?
date: 2024-09-01
description: Le Coran contient des faits scientifiques qui n'ont été découverts que
  récemment grâce aux progrès de la technologie et des connaissances scientifiques.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 573111
---

Le Coran, le livre de l'islam, est le dernier livre de la révélation adressée par Dieu à l'humanité et le dernier de la lignée des révélations transmises aux prophètes.

Bien que le Coran, révélé il y a plus de 1400 ans, ne soit pas à l'origine un ouvrage scientifique, il contient des faits scientifiques qui n'ont été découverts que récemment grâce aux avancées technologiques et scientifiques. L'islam encourage la réflexion et la recherche, car comprendre la nature de la création permet aux croyants d'apprécier davantage leur Créateur, ainsi que l'étendue de Sa puissance et de Sa sagesse.

Le Coran a été révélé à une époque où la science en était encore à ses balbutiements ; il n'existait ni télescopes, ni microscopes, ni aucune technologie comparable à celle d'aujourd'hui. À l'époque, on croyait que le soleil tournait autour de la Terre et que le ciel était soutenu par des piliers aux coins d'une terre plate. C'est dans ce contexte que le Coran a été révélé, renfermant de nombreux faits scientifiques touchant à divers domaines, tels que l'astronomie, la biologie, la géologie et la zoologie.

Voici quelques-uns des nombreux faits scientifiques que l'on trouve dans le Coran :

### Fait n° 1 - L'origine de la vie

{{ $page->verse('21:30') }}

L'eau est décrite comme la source de toute vie. Tous les êtres vivants sont composés de cellules, et nous savons aujourd'hui que celles-ci sont en grande partie constituées d'eau. Cette découverte n'a été possible qu'après l'invention du microscope. Dans les déserts de l'Arabie, il aurait été inimaginable que quelqu'un puisse deviner que l'eau est à l'origine de toute forme de vie.

### Fait n°2 - Le développement embryonnaire humain

Dieu évoque les étapes du développement embryonnaire de l'homme :

{{ $page->verse('23:12-14') }}

Le mot arabe « alaqah » a trois significations : une sangsue, une chose suspendue et un caillot de sang. « Mudghah » signifie une substance mâchée. Les spécialistes en embryologie ont constaté que ces termes décrivent de manière précise la formation de l'embryon et correspondent à notre compréhension scientifique actuelle du processus de développement.

Jusqu'au vingtième siècle, les connaissances sur l'état et la classification des embryons humains étaient très limitées, ce qui implique que les descriptions de l'embryon humain dans le Coran ne pouvaient pas être fondées sur les connaissances scientifiques disponibles au septième siècle.

### Fait n°3 - L'expansion de l'univers

À une époque où l'astronomie était encore à ses débuts, le verset suivant du Coran a été révélé :

{{ $page->verse('51:47') }}

L'un des sens de ce verset est que Dieu étend l'univers (c'est-à-dire les cieux). D'autres interprétations suggèrent que Dieu subvient aux besoins de l'univers et qu'il exerce un contrôle absolu sur celui-ci, ce qui est également vrai.

Le fait que l'univers soit en expansion (par exemple, les planètes s'éloignent les unes des autres) a été découvert au siècle dernier. Dans son livre Une brève histoire du temps, le physicien Stephen Hawking écrit : « La découverte de l'expansion de l'univers a été l'une des grandes révolutions intellectuelles du vingtième siècle. »

Le Coran fait allusion à l'expansion de l'univers bien avant l'invention du télescope !

### Fait n°4 - La descente du fer

Le fer n'est pas un élément naturel de la Terre, car il est arrivé sur notre planète depuis l'espace. Les scientifiques ont découvert que, il y a des milliards d'années, la Terre a été frappée par des météorites contenant du fer provenant d'étoiles lointaines qui avaient explosé.

{{ $page->verse('..57:25..') }}

Dieu utilise les termes « fait descendre ». Le fait que le fer ait été apporté sur Terre depuis l'espace est une connaissance que la science du septième siècle ne pouvait pas posséder.

### Fait n° 5 - La protection du ciel

Le ciel joue un rôle crucial en protégeant la Terre et ses habitants des rayons mortels du soleil ainsi que du froid glacial de l'espace.

Dieu nous invite à réfléchir sur le ciel dans le verset suivant :

{{ $page->verse('21:32') }}

Le Coran indique que la protection offerte par le ciel est un signe de Dieu, une caractéristique protectrice qui n'a été révélée que par des recherches scientifiques menées au vingtième siècle.

### Fait n° 6 - Les montagnes

Dieu attire notre attention sur une caractéristique importante des montagnes :

{{ $page->verse('78:6-7') }}

Le Coran décrit avec précision les racines profondes des montagnes en utilisant le terme « piquets ». Par exemple, le mont Everest a une hauteur d'environ 9 km au-dessus du sol, tandis que ses racines s'étendent à plus de 125 km de profondeur !

Le fait que les montagnes possèdent des racines profondes, semblables à des piquets, n'était pas connu avant le développement de la théorie de la tectonique des plaques au début du vingtième siècle. Dieu affirme également dans le Coran (16:15) que les montagnes ont pour rôle de stabiliser la Terre « ... afin qu'elle ne branle pas », un aspect que les scientifiques commencent à peine à comprendre.

### Fait n° 7 - L'orbite du soleil

En 1512, l'astronome Nicolas Copernic propose sa théorie selon laquelle le Soleil est immobile au centre du système solaire, avec les planètes tournant autour de lui. Cette croyance a été largement acceptée par les astronomes jusqu'au XXe siècle. Aujourd'hui, nous savons que le Soleil n'est pas immobile, mais qu'il se déplace sur une orbite autour du centre de notre galaxie, la Voie lactée.

{{ $page->verse('21:33') }}

### Fait n°8 - Vagues internes dans l'océan

Bien que l'on ait longtemps cru que les vagues se formaient uniquement à la surface de l'océan, les océanographes ont découvert qu'il existe des vagues internes sous la surface. Celles-ci sont invisibles à l'œil nu et ne peuvent être détectées qu'à l'aide d'équipements spécialisés.

Le Coran mentionne :

{{ $page->verse('24:40..') }}

Cette description est étonnante, car il y a 1400 ans, il n'existait aucun équipement spécialisé pour détecter les vagues internes au plus profond des océans.

### Fait n° 9 - Le mensonge et le mouvement

Un chef de tribu cruel et oppressif vivait à l'époque du Prophète Muhammad, paix et bénédiction sur lui. Dieu révéla un verset pour l'avertir :

{{ $page->verse('96:15-16') }}

Dieu ne traite pas cette personne de menteur, mais qualifie son front (la partie avant du cerveau) de « menteur » et de « pécheur », en l'avertissant de cesser. De nombreuses études ont montré que la partie antérieure de notre cerveau (le lobe frontal) est responsable à la fois du mensonge et des mouvements volontaires, donc également des péchés. Ces fonctions ont été découvertes grâce aux appareils d'imagerie médicale développés au XXe siècle.

### Fait n° 10 - Les deux mers qui ne se mélangent pas

À propos des mers, notre Créateur déclare :

{{ $page->verse('55:19-20') }}

Une force physique appelée tension superficielle empêche les eaux des mers voisines de se mélanger en raison de leur différence de densité. C'est comme si un mince mur les séparait. Ce phénomène n'a été découvert que très récemment par les océanographes.

### Muhammad aurait-il pu être l'auteur du Coran ?

Le prophète Muhammad, paix et bénédictions sur lui, est connu dans l'histoire comme étant analphabète : il ne savait ni lire ni écrire et n'avait reçu aucune éducation dans un domaine qui pourrait expliquer les précisions scientifiques contenues dans le Coran.

Certains prétendent qu'il aurait copié le Coran de savants ou de scientifiques de son époque. Si tel était le cas, on s’attendrait à y trouver également toutes les hypothèses scientifiques erronées de l'époque. Or, nous constatons que le Coran ne contient aucune erreur, qu'elle soit scientifique ou d'une autre nature.

Certaines personnes pourraient également prétendre que le Coran a été modifié après la découverte de nouveaux faits scientifiques. Ce n'est pas le cas, car il est historiquement prouvé que le Coran a été préservé dans sa langue d'origine, ce qui constitue un miracle en soi.

### Une simple coïncidence ?

{{ $page->verse('41:53') }}

Bien que cette réponse se soit concentrée sur les miracles scientifiques, le Coran mentionne également de nombreux autres types de miracles : des miracles historiques, des prophéties qui se sont réalisées, des styles linguistiques et littéraires inégalés, sans oublier l'impact profond qu'il a sur les individus. Tous ces miracles ne peuvent pas être le fruit du hasard. Ils indiquent clairement que le Coran vient de Dieu, le Créateur de toutes ces lois scientifiques. Il est le seul Dieu, qui a envoyé tous les prophètes avec le même message : adorer le Dieu unique et suivre les enseignements de son messager.

Le Coran est un livre de guidance qui montre que Dieu n'a pas créé les humains pour qu'ils errent sans but. Au contraire, il nous enseigne que nous avons un but profond et significatif dans la vie : reconnaître la perfection, la grandeur et l'unicité de Dieu et Lui obéir.

Il appartient à chacun d'utiliser l'intelligence et les capacités de raisonnement que Dieu lui a données pour contempler et reconnaître les signes divins, le Coran étant le plus important d'entre eux. Lisez-le et découvrez la beauté et la vérité du Coran pour atteindre le succès !
