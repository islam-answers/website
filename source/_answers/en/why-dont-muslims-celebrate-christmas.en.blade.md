---
extends: _layouts.answer
section: content
title: Why don't Muslims celebrate Christmas?
date: 2024-12-20
description: Muslims revere Jesus (peace be upon him) as a great prophet but reject
  his divinity. Christmas, rooted in non-Islamic beliefs, is not permissible in Islam.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
---

Muslims respect and revere Jesus {{ $page->pbuh() }}, the son of Maryam (Mary) and await his second coming. They consider him one of the greatest of God’s messengers to mankind. A Muslim’s faith is not valid unless he believes in all of the Messengers of Allah, including Jesus {{ $page->pbuh() }}.

But Muslims do not believe that Jesus was God or the Son of God. Nor do Muslims believe that he was crucified. Allah sent Jesus to the Children of Israel to call them to believe in Allah alone and worship Him alone. Allah supported Jesus with miracles that proved he was speaking the truth.

Allah says in the Quran:

{{ $page->verse('19:88-92') }}

### Is it permissible to celebrate Christmas?

Given these theological differences, Muslims are not allowed to single out the festivals of other religions for any of these rituals or customs. Rather, the day of their festivals is just an ordinary day for the Muslims, and they should not single it out for any activity that is part of what the non-Muslims do on these days.

The Christian celebration of Christmas is a tradition that incorporates beliefs and practices not rooted in Islamic teachings. As such, it is not permissible for Muslims to partake in these celebrations, as they do not align with the Islamic understanding of Jesus {{ $page->pbuh() }} or his teachings.

In addition to being an innovation, it comes under the heading of imitating the disbelievers in matters that are unique to them and their religion. Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('abudawud:4031') }}

Ibn `Uthaymin (may Allah have mercy on him) said:

> Congratulating the disbelievers on the occasion of Christmas or any of their other religious festivals is prohibited according to scholarly consensus, because it implies approval of what they follow of disbelief and approving of it for them. Even if he does not approve of this disbelief for himself, it is prohibited for the Muslim to approve of rituals of disbelief or to congratulate someone else for them. It is not permissible for the Muslims to imitate them in any way that is unique to their festivals, whether it be food, clothes, bathing, lighting fires or refraining from usual work or worship, and so on. And it is not permissible to give a feast or to exchange gifts or to sell things that help them to celebrate their festivals (...) or to adorn oneself or put up decorations.

Encyclopedia Britannica states the following:

> The word Christmas is derived from the Old English Cristes maesse, "Christ's Mass”. There is no certain tradition of the date of Christ's birth. Christian chronographers of the 3rd century believed that the creation of the world took place at the spring equinox, then reckoned as March 25; hence the new creation in the incarnation (i.e., the conception) and death of Christ must therefore have occurred on the same day, with his birth following nine months later at the winter solstice, December 25.

> The reason why Christmas came to be celebrated on December 25 remains uncertain, but most probably the reason is that early Christians wished the date to coincide with the pagan Roman festival marking the "birthday of the unconquered sun" ) (natalis solis invicti); this festival celebrated the winter solstice, when the days again begin to lengthen and the sun begins to climb higher in the sky. The traditional customs connected with Christmas have accordingly developed from several sources as a result of the coincidence of the celebration of the birth of Christ with the pagan agricultural and solar observances at midwinter. In the Roman world the Saturnalia (December 17) was a time of merrymaking and exchange of gifts.  December 25 was also regarded as the birth date of the Iranian mystery god Mithra, the Sun of Righteousness. On the Roman New Year (January 1), houses were decorated with greenery and lights, and gifts were given to children and the poor.

So as any rational person can see, there is no sound basis for Christmas, nor did Jesus {{ $page->pbuh() }} or his true followers celebrate Christmas or ask anyone to celebrate Christmas, nor was there any record of anyone calling themselves Christians celebrating Christmas until several hundred years after Jesus. So were the companions of Jesus more righteously guided in not celebrating Christmas or are the people of today?

So if you want to respect Jesus {{ $page->pbuh() }} as Muslims do, don't celebrate some fabricated event that was chosen to coincide with pagan festivals and copy pagan customs. Do you genuinely think God, or even Jesus himself, would approve or condemn such a thing?
