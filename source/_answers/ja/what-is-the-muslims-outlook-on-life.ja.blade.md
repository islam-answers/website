---
extends: _layouts.answer
section: content
title: イスラム教徒の人生観とは？
date: 2024-09-27
description: イスラム教徒の人生観は、希望と恐怖のバランス、高潔な目的、感謝、忍耐、アッラーへの信頼、説明責任「アカウンタビリティー」などの信念によって形作られます。
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 594447
---

イスラム教徒の人生観は、以下の信念に基づいて形成されています。

- アッラー「唯一の神様」の慈悲を希望と彼の罰を恐怖という気持ちのバランス
- 人生において高潔な目的を持つこと
- 人生の流れにいいことが起きたら感謝すること。悪いことが起きたら 我慢すること。
- この人生は試練であり、アッラーは私のすべての行為、苦労、悲しみなどをわかる。
- アッラーに絶対の信頼を持つこと。この世界で起こることが全部アッラーの許可がなければ起こるはずがないからです。
- 持っているものはすべてアッラーがくれたものです。
- ノンムスリムの人々に対して、 心からの関心で扱い、イスラムにガイドされるように正直に願うこと。
- コントロールできるものに集中し、できるだけ努力すること。
- いつか必ず私が死んで、アッラーの元に戻り、自分の行為に対して説明責任。
