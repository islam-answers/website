---
extends: _layouts.answer
section: content
title: Was Jesus crucified?
date: 2024-09-20
description: According to the Quran, Jesus was not killed or crucified. Jesus was raised alive to the Heavens by Allah, and another man was crucified in his place.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
---

When the Jews and the Roman emperor plotted to kill Jesus {{ $page->pbuh() }}, Allah made one of the people present similar in everything to Jesus. So, they killed the man who looked like Jesus. They did not kill Jesus. Jesus {{ $page->pbuh() }} was raised alive to the Heavens. The evidence for that is the words of Allah concerning the fabrication of the Jews and the refutation thereof:

{{ $page->verse('4:157-158') }}

Thus Allah declared false the words of the Jews when they claimed that they had killed him and crucified him, and He stated that He took him up to Him. That was mercy from Allah to Jesus (Isa) {{ $page->pbuh() }}, and was an honour to him, so that he would be one of His signs, an honour that Allah bestows upon whomever He wills of His messengers.

The implication of the words “But Allah raised him (Jesus) up (with his body and soul) unto Himself” is that Allah, may He be glorified, took Jesus up body and soul, so as to refute the claim of the Jews that they crucified him and killed him, because killing and crucifying have to do with the physical body. Moreover, if He had only taken up his soul, that would not rule out the claim of having killed and crucified him, because taking up the soul only would not be a refutation of their claim. Also the name of Jesus {{ $page->pbuh() }}, in reality, refers to him in both soul and body together; it cannot refer to one of them only, unless there is an indication to that effect. Furthermore, taking up both his soul and his body together is indicative of the perfect might and wisdom of Allah, and of His honour and support to whomever He wills among His Messengers, according to what He, may He be exalted, says at the end of the verse: “And Allah is Ever All-Powerful, All-Wise.”
