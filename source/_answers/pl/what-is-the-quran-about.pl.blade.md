---
extends: _layouts.answer
section: content
title: O czym jest Koran?
date: 2024-07-14
description: Podstawowe źródło wiary i praktyki każdego muzułmanina.
sources:
- href: https://www.islam-guide.com/ch3-7.htm
  title: islam-guide.com
translator_id: 583847
---

Koran, ostatnie objawione słowo Boga, jest głównym źródłem wiary i praktyki każdego muzułmanina. Zajmuje się wszystkimi tematami, które dotyczą istot ludzkich: mądrością, doktryną, wyznaniem wiary, interesami, prawem itp., ale jego podstawowym tematem jest relacja między Bogiem a Jego stworzeniami. Jednocześnie zawiera wytyczne i szczegółowe nauki dotyczące sprawiedliwego społeczeństwa, właściwego postępowania ludzi i sprawiedliwego systemu gospodarczego.

Należy pamiętać, że Koran został objawiony Prorokowi Muhammadowi {{ $page->pbuh() }} wyłącznie w języku arabskim. Tak więc każde tłumaczenie Koranu, czy to w języku angielskim, czy w jakimkolwiek innym języku, nie jest ani Koranem, ani wersją Koranu, ale jest jedynie tłumaczeniem znaczenia Koranu. Koran istnieje tylko w języku arabskim, w którym został objawiony.
