---
extends: _layouts.answer
section: content
title: Cosa dice l'Islam a proposito dell'evoluzione?
date: 2024-06-26
description: Dio creò il primo essere umano, Adamo, nella sua forma compiuta, piuttosto che evolversi lentamente dalle scimmie più evolute.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 573107
---

Dio creò il primo essere umano, Adamo, nella sua forma compiuta, piuttosto che evolversi lentamente dalle scimmie più evolute. Ciò non può essere confermato o smentito con certezza dalla scienza poiché si è trattato di un evento storico unico ed eccezionale, un miracolo.

Per quanto riguarda le creature viventi diverse dagli esseri umani, le fonti islamiche non trattano l'argomento e ci chiedono solo di credere che Dio le abbia create nel modo col quale ha voluto, utilizzando l'evoluzione o qualsiai altro modo.
