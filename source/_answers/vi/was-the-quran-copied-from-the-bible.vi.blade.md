---
extends: _layouts.answer
section: content
title: Kinh Quran có được sao chép từ Kinh Thánh không?
date: 2024-11-17
description: Kinh Quran không sao chép từ Kinh thánh. Nhà tiên tri Muhammad mù chữ, không có Kinh thánh tiếng Ả Rập khi đó và mặc khải đến trực tiếp từ Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 587785
---

Allah tạo ra mọi thứ xung quanh chúng ta với một mục đích. Con người cũng được tạo ra với một mục đích và đó là tôn thờ Allah. Allah phán trong Kinh Quran:

{{ $page->verse('51:56') }}

Để hướng dẫn nhân loại, Allah đã gửi các nhà tiên tri theo thời gian. Các nhà tiên tri này đã nhận được sự mặc khải từ Allah và mỗi tôn giáo đều sở hữu bộ kinh sách thiêng liêng riêng, tạo thành nền tảng cho niềm tin của họ. Những kinh sách này là bản sao vật chất của sự mặc khải thiêng liêng, trực tiếp như trong trường hợp của nhà tiên tri Moses (Musa), người đã nhận được các lệnh truyền từ Allah, hoặc gián tiếp như trong trường hợp của Jesus (Isa) và Muhammad {{ $page->pbuh() }}, người đã truyền đạt cho mọi người sự mặc khải được truyền đạt cho họ bởi thiên thần Gabriel (Jibreel).

Kinh Quran hướng dẫn tất cả người Hồi giáo tin vào các thánh thư trước đó. Tuy nhiên, với tư cách là người Hồi giáo, chúng tôi tin vào Torah (Tawrah), Sách Thi thiên (Zaboor), Phúc âm (Injeel) ở dạng ban đầu của chúng, nghĩa là cách chúng được mặc khải, là lời của Allah.

## Liệu nhà tiên tri Muhammad có thể sao chép Kinh Quran từ Kinh Thánh không?

Cuộc thảo luận về cách mà Nhà tiên tri Muhammad {{ $page->pbuh() }} có thể học được những câu chuyện từ những người thạo kinh sách, sau đó truyền lại chúng trong Kinh Qur'an – nghĩa là đó không phải là sự mặc khải – là một cuộc thảo luận về nhiều tuyên bố khác nhau trong đó Ngài {{ $page->pbuh() }} có thể học được những câu chuyện trực tiếp từ kinh thánh của họ, hoặc học được những gì kinh thánh chứa đựng về những câu chuyện, mệnh lệnh và lệnh cấm, từ lời của những người thạo kinh, nếu Ngài không thể tự mình nghiên cứu sách của họ.

Những tuyên bố này có thể được tóm tắt trong ba tuyên bố cơ bản:

1. Tuyên bố rằng Nhà tiên tri Muhammad {{ $page->pbuh() }} không phải là người không biết chữ hoặc mù chữ
1. Tuyên bố rằng các thánh thư của Cơ đốc giáo đã có sẵn cho Nhà tiên tri {{ $page->pbuh() }} và Ngài có thể trích dẫn hoặc sao chép chúng
1. Tuyên bố rằng Mecca là một trung tâm giáo dục lớn cho các nghiên cứu thánh thư

Trước hết, văn phong của Kinh Quran và Sunnah ở nhiều chỗ chỉ rõ, không có chỗ cho sự nghi ngờ, rằng Tiên tri Muhammad {{ $page->pbuh() }} thực sự không biết chữ và rằng Ngài chưa bao giờ thuật lại bất kỳ câu chuyện nào từ những người thạo kinh trước khi sự mặc khải đến với Ngài, và Ngài chưa bao giờ tự tay viết bất cứ điều gì. Ngài không biết gì về những câu chuyện trước khi sứ mệnh của Ngài bắt đầu, và ông chưa bao giờ nói về chúng trước đó.

Allah, cầu xin Người được tôn vinh, nói:

{{ $page->verse('29:48') }}

Các sách về tiểu sử và lịch sử của Nhà tiên tri không đề cập đến việc Nhà tiên tri viết ra sự mặc khải, hoặc về việc Ngài tự mình viết thư cho các vị vua. Ngược lại, điều được đề cập là; Nhà tiên tri Muhammad {{ $page->pbuh() }} có những người ghi chép đã viết ra sự mặc khải và những điều khác. Trong một văn bản tuyệt vời của Ibn Khaldun, có một mô tả về trình độ giáo dục ở Bán đảo Ả Rập ngay trước khi sứ mệnh của Nhà tiên tri bắt đầu. Ông nói:

> Trong số người Ả Rập, kỹ năng viết hiếm hơn cả trứng lạc đà cái. Hầu hết mọi người đều mù chữ, đặc biệt là những người sống ở sa mạc, vì đây là kỹ năng thường thấy ở những người sống ở thành phố.

Do đó, người Ả Rập không gọi người mù chữ là người mù chữ; thay vào đó, họ mô tả người biết đọc và viết là người có hiểu biết, bởi vì biết đọc và viết là ngoại lệ chứ không phải là chuẩn mực của mọi người.

Thứ hai, không có bằng chứng, dấu hiệu hoặc gợi ý nào cho thấy bất kỳ bản dịch tiếng Ả Rập nào của Kinh thánh tồn tại vào thời điểm đó. Al-Bukhari thuật lại từ Abu Hurayrah (cầu xin Allah hài lòng với ông) rằng ông đã nói:

{{ $page->hadith('bukhari:7362') }}

Hadith này chỉ ra rằng người Do Thái đã nắm toàn quyền về văn bản và lời giải thích của nó. Nếu văn bản của họ được biết đến bằng tiếng Ả Rập, thì người Do Thái không cần phải đọc văn bản hoặc giải thích nó [cho người Hồi giáo].

Ý tưởng này được hỗ trợ bởi câu mà Allah, cầu xin Người được tôn vinh, nói rằng:

{{ $page->verse('3:78') }}

Có lẽ cuốn sách quan trọng nhất được viết về chủ đề này là cuốn sách của Bruce Metzger, Giáo sư Ngôn ngữ và Văn học Tân Ước, Kinh thánh được dịch, trong đó ông nói:

> Nhiều khả năng bản dịch Kinh thánh tiếng Ả Rập lâu đời nhất có niên đại từ thế kỷ thứ tám.

Nhà nghiên cứu phương Đông Thomas Patrick Hughes đã viết:

> Không có bằng chứng nào chứng minh rằng Muhammad đã đọc Kinh thánh của Thiên Chúa giáo... Cần lưu ý rằng không có bằng chứng rõ ràng nào cho thấy có bất kỳ bản dịch tiếng Ả Rập nào của Cựu Ước và Tân Ước tồn tại trước thời Muhammad.

Thứ ba, ý tưởng rằng Nhà tiên tri đã nhận được sự mặc khải từ con người là một ý tưởng lỗi thời. Những người theo thuyết đa thần đã cáo buộc ông như thế, và Kinh Qur'an đã bác bỏ họ, như Allah, cầu xin Người được tôn vinh, đã nói:

{{ $page->verse('25:5-6') }}

Hơn nữa, tuyên bố rằng Nhà tiên tri Muhammad {{ $page->pbuh() }} đã tiếp thu kiến thức từ những người thạo Kinh sách khi Ngài ở Mecca là một tuyên bố bị bác bỏ, bởi tình trạng học thuật và văn hóa của Mecca khi ấy và bản chất thực sự của mối liên hệ của xã hội Ả Rập với kiến thức của những người thạo Kinh sách.

Do đó, sau khi chứng minh rằng Nhà tiên tri Muhammad {{ $page->pbuh() }} không học được kiến thức trực tiếp từ những người thạo kinh sách hoặc thông qua trung gian, chúng ta không còn lựa chọn nào khác ngoài việc khẳng định rằng đó là sự mặc khải từ Allah cho Nhà tiên tri được người lựa chọn.
