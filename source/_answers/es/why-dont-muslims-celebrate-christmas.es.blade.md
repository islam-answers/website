---
extends: _layouts.answer
section: content
title: ¿Por qué los musulmanes no celebran la Navidad?
date: 2025-02-08
description: 'Los musulmanes veneran a Jesús (la paz sea con él) como un gran profeta,
  pero rechazan su divinidad. La Navidad no está permitida en Islam '
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 573109
---

Los musulmanes respetan y veneran a Jesús {{ $page->pbuh() }}, hijo de Maryam (María) y esperan su segunda venida. Lo consideran uno de los más grandes mensajeros de Dios a la humanidad. La fe de un musulmán no es válida a menos que crea en todos los Mensajeros de Alá, incluido Jesús {{ $page->pbuh() }}.

Pero los musulmanes no creen que Jesús fuera Dios o el Hijo de Dios. Tampoco creen que fuera crucificado. Alá envió a Jesús a los hijos de Israel para llamarles a creer sólo en Alá y a adorarle sólo a Él. Alá apoyó a Jesús con milagros que probaban que decía la verdad.

Alá dice en el Corán:

{{ $page->verse('19:88-92') }}

### ¿Está permitido celebrar la Navidad?

Dadas estas diferencias teológicas, los musulmanes no pueden destacar las festividades de otras religiones por ninguno de estos rituales o costumbres. Más bien, el día de sus fiestas es un día normal para los musulmanes, y no deben destacarlo por ninguna actividad que forme parte de lo que hacen los no musulmanes en esos días.

La celebración cristiana de la Navidad es una tradición que incorpora creencias y prácticas no arraigadas en las enseñanzas islámicas. Por ello, no está permitido que los musulmanes participen en estas celebraciones, ya que no se ajustan a la interpretación islámica de Jesús {{ $page->pbuh() }} ni de sus enseñanzas.

Además de ser una innovación, cae bajo el epígrafe de imitar a los incrédulos en asuntos que son exclusivos de ellos y de su religión. El Profeta Muhammad {{ $page->pbuh() }} dijo:

{{ $page->hadith('abudawud:4031') }}

Ibn `Uzaymin (que Allah tenga piedad de él) dijo:

> Felicitar a los incrédulos con motivo de la Navidad o cualquiera de sus otras fiestas religiosas está prohibido según el consenso de los eruditos, porque implica aprobar lo que siguen de incredulidad y aprobarlo por ellos. Incluso si no aprueba esta incredulidad para sí mismo, está prohibido que el musulmán apruebe rituales de incredulidad o felicite a otra persona por ellos. No es permisible para los musulmanes imitarlos en ninguna forma que sea propia de sus festividades, ya sea comida, ropa, baño, encender fuego o abstenerse del trabajo habitual o de la adoración, etc. Y no está permitido ofrecer un banquete o intercambiar regalos o vender cosas que les ayuden a celebrar sus fiestas (...) ni adornarse o poner decoraciones.

La Enciclopedia Británica afirma lo siguiente:

> La palabra Navidad deriva del inglés antiguo Cristes maesse, "Misa de Cristo". No existe una tradición segura sobre la fecha del nacimiento de Cristo. Los cronógrafos cristianos del siglo III creían que la creación del mundo había tenido lugar en el equinoccio de primavera, entonces el 25 de marzo; por tanto, la nueva creación en la encarnación (es decir, la concepción) y la muerte de Cristo debieron ocurrir el mismo día, y su nacimiento nueve meses después, en el solsticio de invierno, el 25 de diciembre.

> La razón por la que la Navidad llegó a celebrarse el 25 de diciembre sigue siendo incierta, pero lo más probable es que los primeros cristianos quisieran que la fecha coincidiera con la fiesta pagana romana del "cumpleaños del sol invicto" (natalis solis invicti), que celebraba el solsticio de invierno, cuando los días empiezan a alargarse y el sol comienza a subir en el cielo. Las costumbres tradicionales relacionadas con la Navidad se han desarrollado, por tanto, a partir de varias fuentes como resultado de la coincidencia de la celebración del nacimiento de Cristo con las observancias agrícolas y solares paganas en pleno invierno. En el mundo romano, la Saturnalia (17 de diciembre) era una época de alegría e intercambio de regalos.  El 25 de diciembre también se consideraba la fecha de nacimiento del dios misterioso iraní Mitra, el Sol de Justicia. En el Año Nuevo romano (1 de enero), las casas se decoraban con verde y luces, y se hacían regalos a los niños y a los pobres.

Así que como cualquier persona racional puede ver, no hay ninguna base sólida para la Navidad, ni Jesús {{ $page->pbuh() }} ni sus verdaderos seguidores celebraron la Navidad ni pidieron a nadie que celebrara la Navidad, ni hubo ningún registro de nadie que se llamara a sí mismo cristiano celebrando la Navidad hasta varios cientos de años después de Jesús. Entonces, ¿fueron los compañeros de Jesús más rectamente guiados en no celebrar la Navidad o lo son las personas de hoy?

Así que si quieres respetar a Jesús {{ $page->pbuh() }} como hacen los musulmanes, no celebres un evento inventado que fue elegido para coincidir con festivales paganos y copiar costumbres paganas. ¿De verdad crees que Dios, o incluso el propio Jesús, aprobaría o condenaría algo así?
