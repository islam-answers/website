---
title: About
description: Learn more about Islam Answers, our mission, vision, and process.
extends: _layouts.main
section: body
---

# About Islam Answers

## Vision

Our vision is to be the leading online resource for providing clear, reliable answers about Islam, addressing contemporary challenges, fostering understanding, and dispelling misconceptions.

## Mission

Founded in Ramadan 1445, Islam Answers is dedicated to addressing the most common, controversial, and contemporary questions about Islam from people of other faiths. We offer clear answers from credible sources, supported by evidence from the Quran and Sunnah (the words, actions, and teachings of Prophet Muhammad, peace be upon him).

## Founders

**Muhannad Ajjan**, a Sunni Muslim from Syria, is a web developer with a strong commitment to Islam and Dawah. His technical expertise and dedication to promoting the message of Islam have been instrumental in the development and growth of Islam Answers. Muhannad utilized his technical skills to build the website from scratch, optimize it for search engines, and automate various maintenance tasks.

**Umm Yusuf**, who holds a degree in Psychology from UCL, reverted to Islam in 2018. She is a dedicated student of knowledge, having studied at Maryam Institute and Dar Al-Arqam (UK). Her areas of focus include Muslim marriage, family, children’s education, and opposing feminism, making her a valuable asset to the platform. In addition to her role as an editor, she also manages the website’s social media presence and the translation team.

## Aims

Our objective is to make these answers accessible in as many languages as possible to reach a broad audience interested in learning about Islam. The internet is rife with Islamophobic and misleading content, particularly in languages with limited quality information about Islam. We aim for people to discover the truth of Islam and, by the will of Allah, be guided to embrace it.

One of the key motivations behind creating this website is the ongoing war on Palestine and the resilience of the Palestinian people, which has inspired many to look into our beautiful religion. Their strength and perseverance have highlighted the importance of providing accurate and accessible information about Islam to counteract the widespread misinformation.

Our website is continually expanding in terms of the number of questions we answer and the number of languages we translate them into.

## Our Process

<img src="/assets/img/process.png" alt="Islam Answers Process" class="hidden md:block">

### Planning

We identify the most common questions people have about Islam, as well as trending questions on Google. These questions range from inquiries about Islam, Muslims, the Quran, and Prophet Muhammad, to broader questions about life, the afterlife, societal issues, and purpose, all viewed through the lens of Islam.

### Research

Answers are researched from reliable sources listed below. We ensure the answers are clear, concise, and backed by evidence from the Quran and Sunnah.

We trust the following sources for our answers:

@include('_components.sources')

### Translation

To reach a broader audience, we translate our answers into as many languages as possible. We use Lokalise as our translation management system to streamline this process. Our dedicated team of volunteer translators works diligently to ensure the accuracy and quality of these translations.

### Reach

After translation, we publish the answers on our website and share them across various social media platforms to maximize our reach.

## Contributors

Our website is maintained by a group of volunteers who are passionate about sharing the message of Islam with the world. We are thankful to the following contributors for their dedication and hard work:

### Translators

@include('_components.contributors')

### Editors

@include('_components.editors')

We are not affiliated with any organization, nor are we funded by any organization. We are a group of volunteers who believe that the world needs to know more about Islam, and we are doing our part to make that happen.

The source code for this website is available on [GitLab](https://gitlab.com/islam-answers/website). We welcome contributions from developers to help us improve the website.

## Contact

If you have any questions or would like to contribute to the website, please contact us at [contact (at) islam-answers.com](mailto:contact@islam-answers.com).

May Allah guide us all to the truth. Ameen.
