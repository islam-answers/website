---
extends: _layouts.answer
section: content
title: ¿Qué es lo que el Islam dice sobre el Terrorismo?
date: 2024-06-06
description: El Islam, una religión de misericordia, no permite el Terrorismo.
sources:
- href: https://www.islam-guide.com/es/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

El Islam, una religión de misericordia, no permite el Terrorismo. Dios dijo en el Corán:

{{ $page->verse('60:8') }}

El profeta Muhammad {{ $page->pbuh() }} prohibía a los soldados matar a mujeres y niños, y los aconsejaba diciéndoles:

{{ $page->hadith('tirmidhi:1408') }}

Y dijo también:

{{ $page->hadith('bukhari:3166') }}

El profeta Muhammad {{ $page->pbuh() }} prohibió también el castigo con fuego.

En una ocasión calificó al asesinato como el segundo de los pecados grandes (Capitales), y más aún advirtió a la gente que en el Día del Juicio:

{{ $page->hadith('bukhari:6533') }}

Los musulmanes hasta son incentivados para que sean misericordiosos con los animales, y les es prohibido lastimarlos. Cierta vez el profeta {{ $page->pbuh() }} dijo:

{{ $page->hadith('bukhari:3318') }}

También dijo que un hombre le dio de beber a un perro muy sediento, por lo cual Dios le perdonó sus pecados. Entonces el Profeta fue preguntado: ¿Acaso somos recompensados por tener bondad para con los animales?, dijo:

{{ $page->hadith('bukhari:2466') }}

En adición a esto, los musulmanes son ordenados por Dios, que cuando tomen la vida de un animal para alimentarse de él, lo hagan de la manera que le cause el mínimo terror y sufrimiento posible. El profeta Muhammad {{ $page->pbuh() }} dijo:

{{ $page->hadith('tirmidhi:1409') }}

A la luz de este, y otros textos islámicos, el incitar el terror en los corazones de indefensos civiles, la total destrucción de edificios y propiedades, la explosión de bombas y mutilación de hombres, mujeres y niños inocentes, son todos prohibidos y detestables actos de acuerdo con el Islam y los musulmanes.

Los musulmanes siguen una religión de Paz, misericordia y perdón, y la vasta mayoría de ellos no tienen nada que ver con los violentos eventos que algunos han asociado con los musulmanes. Si un musulmán comete un acto de terrorismo, esa persona será culpable de violar la ley del Islam.

Sin embargo, es importante distinguir entre terrorismo y resistencia legítima a la ocupación, ya que ambos son muy diferentes.
