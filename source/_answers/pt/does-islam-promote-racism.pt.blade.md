---
extends: _layouts.answer
section: content
title: O Islam promove o racismo?
date: 2025-01-06
description: O Islam não presta atenção às diferenças de cor, raça ou linhagem.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 594172
---

Todas as pessoas são descendentes de um homem e de uma mulher, crentes e kaafir (não crentes), negros e brancos, árabes e não árabes, ricos e pobres, nobres e humildes.

O Islam não presta atenção às diferenças de cor, raça ou linhagem. Todas as pessoas provêm de Adão, e Adão foi criado do pó. No Islam, a diferenciação entre as pessoas baseia-se na fé (Iman) e na piedade (Taqwa), fazendo o que Allah ordenou e evitando o que Allah proibiu. Allah diz:

{{ $page->verse('49:13') }}

O Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2564b') }}

O Islam considera que todas as pessoas são iguais em termos de direitos e deveres. As pessoas são iguais perante a lei (Shari'ah), como diz Allah:

{{ $page->verse('16:97') }}

A fé, a veracidade e a piedade conduzem ao Paraíso, que é o direito daquele que adquire estes atributos, mesmo que seja um dos mais fracos ou humildes. Allah diz:

{{ $page->verse('..65:11') }}

O Kufr (descrença), a arrogância e a opressão conduzem ao Inferno, mesmo que quem os pratica seja um dos mais ricos ou nobres. Allah diz:

{{ $page->verse('64:10') }}

Os conselheiros do Profeta Muhammad {{ $page->pbuh() }} incluíam homens muçulmanos de todas as tribos, raças e cores. Os seus corações estavam cheios de Tawhid (monoteísmo) e estavam unidos pela sua fé e piedade - tais como Abu Bakr de Quraysh, 'Ali ibn Abi Taalib de Bani Haashim, Bilal o Etíope, Suhayb o Romano, Salman o Persa, homens ricos como 'Uthman e homens pobres como 'Ammar, pessoas de posses e pessoas pobres como Ahl al-Suffah, e outros.

Acreditaram em Allah e lutaram por Ele, até que Allah e o Seu Mensageiro ficaram satisfeitos com eles. Eram os verdadeiros crentes.

{{ $page->verse('98:8') }}
