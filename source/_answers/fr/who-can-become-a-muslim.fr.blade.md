---
extends: _layouts.answer
section: content
title: Qui peut devenir musulman ?
date: 2024-09-29
description: Tout être humain peut devenir musulman, peu importe sa religion, son
  âge, sa nationalité ou son origine ethnique.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 586055
---

Tout être humain peut devenir musulman, quelle que soit sa religion, son âge, sa nationalité ou son origine ethnique. Pour devenir musulman, il suffit de prononcer les mots suivants : « Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah ». Cette attestation de foi signifie : « J’atteste qu’il n’y a pas d’autre dieu digne d’être adoré qu’Allah et que Muhammed est Son messager ». Vous devez le dire et y croire.

Une fois que vous aurez prononcé cette phrase, vous deviendrez automatiquement musulman. Vous n’aurez pas besoin de demander un moment ou une heure, de jour ou de nuit, pour vous tourner vers votre Seigneur et commencer votre nouvelle vie. N’importe quel moment est le bon moment pour cela.

Vous n'avez besoin de la permission de personne, ni d'un prêtre pour vous baptiser, ni d'un intermédiaire pour vous servir de médiateur, ni de personne pour vous guider vers Lui, car c'est Lui, qu'Il soit glorifié, qui vous guide Lui-même. Tournez-vous donc vers Lui, car Il est proche, Il est plus proche de vous que vous ne le pensez ou que vous ne pouvez l'imaginer :

{{ $page->verse('2:186') }}

Allah, Exalté soit-Il, dit :

{{ $page->verse('6:125..') }}
