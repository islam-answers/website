---
extends: _layouts.answer
section: content
title: Qu'est-ce que la nourriture halal ?
date: 2024-07-08
description: Les aliments halal sont ceux dont la consommation est autorisée par Dieu
  pour les musulmans, les principales exceptions étant le porc et l'alcool.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 573111
---

Les aliments halal, ou licites, sont ceux que Dieu autorise les musulmans à consommer. En règle générale, la plupart des aliments et des boissons sont considérés comme halal, les principales exceptions étant le porc et l'alcool. Les viandes et les volailles doivent être abattues de manière humaine et correcte, ce qui implique de mentionner le nom de Dieu avant l'abattage et de minimiser les souffrances des animaux.
