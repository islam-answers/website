---
extends: _layouts.answer
section: content
title: De ce s-a căsătorit profetul Muhammad cu o fată tânără?
date: 2024-12-18
description: Căsătoria lui Muhammad cu Aisha reflectă normele secolului al VII-lea; criticile apar din cauza  standardelor moderne aplicate practicilor de atunci.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 554943
---

Aisha (Allah să fie mulțumit de ea) avea 6 ani când s-a căsătorit cu Profetul Muhammad {{ $page->pbuh() }} și 9 ani când căsătoria a fost consumată.

Din păcate, în prezent vedem eforturile altor religii concentrate în principal pe răstălmăcirea și manipularea faptelor istorice și etimologice. Un astfel de subiect a fost acuzația privind căsătoria tinerei Aisha cu Profetul Muhammad {{ $page->pbuh() }}. Misionarii încearcă să îl acuze pe Profet de molestarea copiilor, din cauza faptului că Aisha a fost logodită la vârsta de 6 ani, iar căsătoria a fost consumată 3 ani mai târziu - la 9 ani - când era în plină pubertate. Intervalul de timp dintre logodnă și consumarea căsătoriei Aishei arată în mod clar că părinții ei așteptau ca ea să ajungă la pubertate înainte ca mariajul ei să fie consumat. Acest răspuns respinge acuzația nedreaptă împotriva Profetului Muhammad {{ $page->pbuh() }}.

### Pubertatea și căsătoria tânără în cultura semitică

Acuzația de molestare a copiilor contrazice faptul că o fată devine femeie atunci când își începe ciclul menstrual. Semnificația menstruației, pe care oricine are cea mai mică familiaritate cu fiziologia v-o va spune, este că este un semn că fata este pregătită să devină mamă.

Femeile ajung la pubertate la vârste diferite, de la 8 la 12 ani, în funcție de genetică, rasă și mediu (climǎ). Există puține diferențe în ceea ce privește dezvoltarea băieților și a fetelor până la vârsta de zece ani; sprintul de creștere la pubertate începe mai devreme la fete, dar durează mai mult la băieți. Primele semne ale pubertății apar în jurul vârstei de 9 sau 10 ani la fete, dar mai aproape de 12 ani la băieți. De asemenea, fetele din medii mai calde ajung la pubertate la o vârstă mult mai fragedă decât cele din medii reci. Temperatura medie a țării sau a provinciei este considerată cel mai important factor în acest caz, nu numai în ceea ce privește menstruația, ci și în ceea ce privește întreaga dezvoltare sexuală la pubertate.

Căsătoria în primii ani de pubertate era acceptabilă în Arabia secolului al VII-lea, așa cum era norma socială în toate culturile semite, de la israeliți la arabi și la toate națiunile intermediare. Conform Talmudului, pe care evreii îl consideră "Tora lor orală", Sanhedrin 76b afirmă în mod clar că este de preferat ca o femeie să se căsătorească atunci când are prima menstruație, iar în Ketuvot 6a există reguli privind relațiile sexuale cu fete care nu au avut încă menstruație. Jim West, un pastor baptist, observă următoarea tradiție a israeliților:

- Soția urma să fie luată din cadrul cercului familial mai larg (de obicei la începutul pubertății sau în jurul vârstei de 13 ani) pentru a menține puritatea liniei familiale.
- Pubertatea a fost întotdeauna un simbol al maturității de-a lungul istoriei.
- Pubertatea este definită ca vârsta sau perioada la care o persoană este capabilă de reproducere sexuală. În alte epoci ale istoriei, un ritual sau sărbătoare a acestui eveniment emblematic făcea parte din cultură.

Sexologii renumiți, R.E.L. Masters și Allan Edwards, în studiul lor asupra expresiei sexuale afro-asiatice, afirmă următoarele:

> Astăzi, în multe părți din Africa de Nord și Orientul Mijlociu, fetele se căsătoresc între cinci și nouă ani; și nicio femeie care se respectă nu rămâne nemăritată după vârsta pubertății.

### Motivele căsătoriei cu Aisha

În ceea ce privește povestea căsătoriei sale, Profetul {{ $page->pbuh() }} a fost îndurerat de moartea primei sale soții, Khadeejah, care l-a sprijinit și i-a stat alături, și a numit anul în care a murit aceasta Anul Durerii. Apoi s-a căsătorit cu Sawdah, care era o femeie mai în vârstă și nu era foarte frumoasă; mai degrabă s-a căsătorit cu ea pentru a o consola după moartea soțului ei. Patru ani mai târziu, Profetul {{ $page->pbuh() }} s-a căsătorit cu Aisha. Motivele căsătoriei au fost următoarele:

1. El a avut un vis despre căsătoria cu ea. Se dovedește din relatarea Aishei că Profetul {{ $page->pbuh() }} i-a spus:

{{ $page->hadith('bukhari:3895') }}

2. Caracteristicile de inteligență și deșteptăciune pe care Profetul {{ $page->pbuh() }} le-a observat la Aisha încă din copilărie, așa că a vrut să se căsătorească cu ea pentru ca ea să fie mai capabilă decât alții să transmită rapoarte (narațiuni) despre ceea ce a făcut și a spus el. De fapt, după cum s-a menționat mai sus, ea a fost un punct de referință pentru companionii Profetului {{ $page->pbuh() }} în ceea ce privește afacerile și hotărârile acestora.

3. Dragostea Profetului {{ $page->pbuh() }} pentru tatăl ei, Abu Bakr, și persecuția pe care Abu Bakr a suferit-o de dragul chemării la Islam, pe care a suportat-o cu răbdare. El a fost cel mai puternic și cel mai sincer în credință dintre oameni, după profeți.

### Au existat obiecții la căsătoria lor?

Răspunsul la această întrebare este nu. Nu există absolut nicio înregistrare din surse istorice musulmane, seculare sau de altă natură care să arate altceva decât bucuria tuturor părților implicate în această căsătorie. Nabia Abbott descrie căsătoria Aishei cu Profetul {{ $page->pbuh() }} după cum urmează:

> În nicio versiune nu se face vreun comentariu cu privire la diferența de vârstă dintre Mohammed și Aishah sau la vârsta fragedă a miresei care, cel mult, nu putea avea mai mult de zece ani și care era încă foarte îndrăgostită de jocul ei.

Chiar și cunoscutul orientalist critic, W. Montgomery Watt, a spus următoarele despre caracterul moral al Profetului {{ $page->pbuh() }}:

> Din punctul de vedere al epocii lui Muhammad, acuzațiile legate de trădare și senzualitate nu pot fi susținute. Contemporanii săi nu i-au găsit defect din punct de vedere moral în niciun fel. Dimpotrivă, unele dintre faptele criticate de occidentalii moderni arată că standardele lui Muhammad erau mai ridicate decât cele ale epocii sale.

Pe lângă faptul că nimeni nu era nemulțumit de el sau de acțiunile sale, el a fost un exemplu primordial de caracter moral în societatea și timpul său. Prin urmare, a-i judeca moralitatea pe baza standardelor societății și culturii noastre de astăzi nu este doar absurd, ci și nedrept.

### Căsătoria la pubertate astăzi

Contemporanii Profetului (atât dușmani, cât și prieteni) au acceptat fără probleme căsătoria Profetului {{ $page->pbuh() }} cu Aisha. Dovadă în acest sens este lipsa de critici la adresa acestei căsătorii până în epoca modernă.

Cauza este o schimbare în cultura zilelor noastre, în ceea ce privește vârsta la care oamenii se căsătoresc de obicei. Dar chiar și astăzi, în secolul al XXI-lea, vârsta de consimțământ sexual este încă destul de scăzută în multe locuri. În Germania, Italia și Austria, oamenii pot face sex legal la vârsta de 14 ani, iar în Filipine și Angola pot face sex legal la vârsta de 12 ani. California a fost primul stat care a modificat vârsta de consimțământ la 14 ani, ceea ce s-a întâmplat în 1889. După California, alte state s-au alăturat și au ridicat și ele vârsta de consimțământ.

### Islamul și vârsta pubertății

Islamul ne învață în mod clar că vârsta adultă începe atunci când o persoană a ajuns la pubertate. Allah spune în Coran:

{{ $page->verse('24:59..') }}

Atingerea pubertății de către femei este odată cu începerea menstruației, după cum spune Allah în Coran:

{{ $page->verse('65:4..') }}

Astfel, Islamul recunoaște venirea pubertății ca fiind începutul maturității. Este momentul în care persoana s-a maturizat și este pregătită pentru responsabilitățile unui adult. Așadar, pe ce bază ar trebui cineva să critice căsătoria cu Aisha, din moment ce căsătoria ei a fost consumată după ce ea a ajuns la pubertate?

Dacă acuzația de „molestare a copiilor” ar fi prezentată împotriva Profetului {{ $page->pbuh() }}, ar trebui să includem și toți oamenii semitici care au acceptat căsătoria la pubertate ca normă.

Mai mult decât atât, Profetul {{ $page->pbuh() }} nu s-a căsătorit cu nicio altă fecioară (virgină) în afară de Aisha și toate celelalte soții ale sale fuseseră căsătorite anterior (și multe dintre ele erau bătrâne). Acest lucru respinge noțiunea răspândită de multe surse ostile, cum că motivul de bază din spatele căsătoriilor Profetului ar fi fost dorința fizică și plăcerea, pentru că dacă aceasta ar fi fost intenția lui, ar fi ales doar femei tinere.

### Concluzii

Am văzut astfel că:

- Era norma societății semitice din Arabia secolului al VII-lea să permită căsătoriile pubescente.
- Nu au existat rapoarte de opunere față de căsătoria Profetului cu Aisha, nici din partea prietenilor sau a dușmanilor săi.
- Chiar și în prezent, există culturi care încă permit căsătoriile pubescente pentru tinerele lor.

În ciuda acestor fapte binecunoscute, unii oameni încă îl acuză pe Profet {{ $page->pbuh() }} de imoralitate. Însă el a fost cel care a adus dreptate femeilor din Arabia și le-a ridicat la un nivel pe care societatea lor nu îl văzuse până atunci și ceva ce civilizațiile antice făcuseră niciodată.

De exemplu, atunci când a devenit Profet, păgânii din Arabia moșteniseră un dispreț față de femei, așa cum se transmisese printre vecinii lor evrei și creștini. Atât de rușinos era considerat printre ei să fie binecuvântați cu un copil de sex feminin, încât mergeau până la a îngropa de viu acest copil pentru a evita rușinea asociată cu copiii de sex feminin. Allah spune în Coran:

{{ $page->verse('16:58-59') }}

Nu numai că Muhammad {{ $page->pbuh() }} a descurajat și a condamnat sever acest act, dar, de asemenea, îi învăța pe oameni să își respecte și să își prețuiască fiicele și mamele ca parteneri și surse de mântuire pentru bărbații din familia lor. El a spus:

{{ $page->hadith('ibnmajah:3669') }}

De asemenea, a fost relatat într-o narațiune a lui Anas Ibn Malik că:

{{ $page->hadith('muslim:2631') }}

Cu alte cuvinte, dacă cineva îl iubește pe Trimisul lui Allah {{ $page->pbuh() }} și dorește să fie alături de el în Ziua Învierii în Rai, atunci ar trebui să fie bun cu fiicele sale. Acesta nu este cu siguranță un act al unui "pedofil", așa cum ar dori unii să credem.
