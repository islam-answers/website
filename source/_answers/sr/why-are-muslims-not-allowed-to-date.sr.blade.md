---
extends: _layouts.answer
section: content
title: Zašto muslimanima nije dozvoljeno da budu u vezi?
date: 2024-08-19
description: Kur'an zabranjuje imati devojku ili momka. Islam čuva odnose između pojedinaca
  na način koji odgovara ljudskoj prirodi.
sources:
- href: https://islamqa.org/shafii/seekersguidance-shafii/241530
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107140
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107560
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/shafii/qibla-shafii/33045
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/askimam/17968
  title: islamqa.org (AskImam.org)
- href: https://www.islamweb.net/en/fatwa/89542
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/82217
  title: islamweb.net
- href: https://islamqa.info/en/answers/20949
  title: islamqa.info
- href: https://islamqa.info/en/answers/33702
  title: islamqa.info
- href: https://islamqa.info/en/answers/1200
  title: islamqa.info
- href: https://islamqa.info/en/answers/21933
  title: islamqa.info
- href: https://islamqa.info/en/answers/93450
  title: islamqa.info
- href: https://islamqa.info/en/answers/1114
  title: islamqa.info
- href: https://islamqa.info/en/answers/3807
  title: islamqa.info
translator_id: 584497
---

Islam čuva odnose između pojedinaca na način koji odgovara ljudskoj prirodi. Kontakt između muškarca i žene je zabranjen, osim pod okriljem legalnog braka. Ako postoji bilo kakva potreba za razgovorom sa suprotnim polom, to bi trebalo da bude u granicama uljudnosti i lepog ponašanja.

## Sprečavanje iskušenja

Islam teži da ugasi bilo koji oblik bluda na samom početku. Nije dozvoljeno da žena ili muškarac uspostave prijateljstvo ili ljubavni odnos sa nekim suprotnog pola putem soba za ćaskanje, interneta ili bilo kog drugog sredstva, jer to dovodi ženu ili muškarca do iskušenja. To je način đavola kojim on vuče osobu da padne u greh, blud ili preljubu. Allah kaže:

{{ $page->verse('24:21..') }}

Ženi je zabranjeno da razgovara umilno sa onim ko joj nije dozvoljen, kao što Allah kaže:

{{ $page->verse('..33:32') }}

Sastajanje, i mešanje muškaraca i žena na jednom mestu, njihovo okupljanje, i otkrivanje i izlaganje žena muškarcima su zabranjeni zakonom islama (šerijat). Ova dela su zabranjena jer su među uzrocima za fitnah (iskušenje ili pokušaj nečega što podrazumeva zle posledice), buđenje želja i činjenje nepristojnosti i nepravde. Poslanik {{ $page->pbuh() }} je rekao:

{{ $page->hadith('ahmad:1369') }}

Kur'an zabranjuje imati devojku ili momka. Allah kaže:

{{ $page->verse('..4:25..') }}

## Društvena razmatranja

Ulazak u momak/devojka tip odnosa može da naškodi ugled žene na ozbiljne načine koji neće nužno uticati na muškog partnera. Seks pre braka je u suprotnosti sa religijom i ugrožava nečiju lozu/čast. Teško je videti kroz izmaglicu ljubavi, požude ili zaljubljenosti, zbog čega nam Allah svima naređuje da se držimo podalje od predbračnog seksa. Toliko štete može nastati nakon samo jednog nezakonitog čina predbračnog seksa, na primer neželjene trudnoće, polno prenosive bolesti, slomljenog srce, krivice.

Osim eksplicitne zabrane od Boga, postoje očigledne mudrosti da predbračni odnosi budu nezakoniti. Među njima su:

1. Nedvosmislenost očinstva ako žena zatrudni. Uprkos čak i dugoročnim vezama, ko može reći da žena ne spava sa drugim muškarcima da bi utvrdila kompatibilnost budućeg supružnika?
1. Dete rođeno van braka se ne pripisuje ocu. Očuvanje loze je jedan od glavnih ciljeva šerijata.
1. Takvi odnosi daju muškarcima prednost da "rade kako žele" sa ženama, i oslobođeni su bilo kakve emocionalne i finansijske odgovornosti prema ženi i deci rođenoj u takvom odnosu.
1. Postoji urođena vrlina u očuvanju čednosti pre braka, koja je priznata već vekovima i uspostavljena od strane svake religije.
1. Iako možda ne primećujemo nikakvu spoljašnju razliku u našim svetovnim poslovima, neprikladne prakse utiču na dušu. Što se više prepuštamo grehu, to nam srca sve više otvrdnjavaju.
1. Imati više partnera povećava rizik od dobijanja seksualno prenosivih bolesti i zaraze drugih.
1. Ne postoji garancija da zajednički život braka je siguran pokazatelj odnosa posle braka. Mnogi nemuslimanski parovi žive zajedno godinama, samo da raskinu nakon što se venčaju.
1. Moramo se zapitati da li bi želeli da naši sinovi i kćeri rade istu stvar pre braka?

U zaključku, nema nikakvog smisla da su dugoročne veze i seks dozvoljene pre braka. Pojedinačna razmatranja su beznačajna u poređenju sa mnoštvom ozbiljnih pitanja koja bi uticala na pojedince, uglavnom žene, i siguran je način da se dovede do dekadencije društva i sopstvene duše.
