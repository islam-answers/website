---
extends: _layouts.answer
section: content
title: Warum heiratete der Prophet Mohammed ein junges Mädchen?
date: 2024-12-16
description: Die Ehe des Propheten Muhammad mit Aisha entsprach den Normen des 7.
  Jahrhunderts. Heutige Kritik ergibt sich aus dem Vergleich mit modernen Maßstäben.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 587746
---

Aisha (möge Allah mit ihr zufrieden sein) war 6 Jahre alt, als sie mit dem Propheten Muhammad {{ $page->pbuh() }} verheiratet wurde, und 9 Jahre alt, als die Ehe vollzogen wurde.

Leider sehen wir derzeit, dass die Bemühungen anderer Religionen sich hauptsächlich darauf konzentrieren, historische und etymologische Fakten zu verdrehen und zu manipulieren. Eines der Themen, das dabei aufgegriffen wird, ist die Behauptung über die Heirat der jungen Aisha mit dem Propheten Muhammad,{{ $page->pbuh() }}. Missionare versuchen, den Propheten – wenn auch in politisch korrekten Begriffen – als Kinderschänder zu beschuldigen, da Aisha im Alter von 6 Jahren verlobt wurde und die Ehe 3 Jahre später – im Alter von 9 Jahren – vollzogen wurde, als sie ihre volle Pubertät erreicht hatte. Der Zeitraum zwischen der Verlobung und der Vollziehung der Ehe zeigt eindeutig, dass ihre Eltern darauf warteten, dass sie die Pubertät erreichte, bevor die Ehe vollzogen wurde. Diese Antwort zielt darauf ab, die ungerechte Anschuldigung gegen den Propheten Muhammad, {{ $page->pbuh() }}, zu widerlegen.

### Pubertät und junge Heirat in der semitischen Kultur

Der Vorwurf des Kindesmissbrauchs widerspricht der grundlegenden Tatsache, dass ein Mädchen mit Beginn der Menstruation zur Frau wird. Jeder, der sich auch nur ein bisschen mit der Physiologie auskennt, wird Ihnen sagen, dass die Menstruation ein Zeichen dafür ist, dass das Mädchen darauf vorbereitet wird, Mutter zu werden.

Frauen erreichen die Pubertät in unterschiedlichem Alter, zwischen 8 und 12 Jahren, je nach Genetik, Rasse und Umwelt. Bis zum Alter von zehn Jahren gibt es kaum Unterschiede in der Größe von Jungen und Mädchen. Der Wachstumsschub in der Pubertät beginnt bei Mädchen früher, dauert aber bei Jungen länger. Die ersten Anzeichen der Pubertät treten bei Mädchen im Alter von etwa 9 oder 10 Jahren auf, bei Jungen jedoch eher im Alter von 12 Jahren. Außerdem erreichen Frauen in wärmeren Umgebungen die Pubertät in einem viel früheren Alter als Frauen in kalten Umgebungen. Die Durchschnittstemperatur des Landes oder der Provinz wird hier als der wichtigste Faktor angesehen, nicht nur im Hinblick auf die Menstruation, sondern im Hinblick auf die gesamte sexuelle Entwicklung in der Pubertät.

Eine Heirat in den frühen Jahren der Pubertät war im Arabien des 7. Jahrhunderts akzeptabel, wie es in allen semitischen Kulturen von den Israeliten bis zu den Arabern und allen Völkern dazwischen die gesellschaftliche Norm war. Im Talmud, den die Juden als ihre "mündliche Tora" betrachten, heißt es in Sanhedrin 76b eindeutig, dass es vorzuziehen ist, eine Frau zu heiraten, wenn sie ihre erste Menstruation hat, und in Ketuvot 6a gibt es Regeln für den Geschlechtsverkehr mit Mädchen, die noch nicht menstruiert haben. Jim West, ein baptistischer Geistlicher, hält sich an die folgende Tradition der Israeliten:

- Die Frau sollte aus dem größeren Familienkreis genommen werden (normalerweise zu Beginn der Pubertät oder im Alter von etwa 13 Jahren), um die Reinheit der Familienlinie zu wahren.
- Die Pubertät war im Laufe der Geschichte immer ein Symbol des Erwachsenseins.
- Als Pubertät wird das Alter oder der Zeitraum bezeichnet, in dem ein Mensch erstmals zur sexuellen Fortpflanzung fähig ist. In anderen Epochen der Geschichte war ein Ritual oder eine Feier dieses bahnbrechenden Ereignisses Teil der Kultur.

Die renommierten Sexualwissenschaftler R.E.L. Masters und Allan Edwards stellen in ihrer Studie zum afroasiatischen Sexualausdruck Folgendes fest:

> Heute werden Mädchen in vielen Teilen Nordafrikas und des Nahen Ostens im Alter zwischen fünf und neun Jahren verheiratet und ins Bett gegeben; und kein Mädchen mit Selbstachtung bleibt nach der Pubertät unverheiratet.

### Gründe für die Heirat mit Aisha

Was die Geschichte ihrer Heirat betrifft, so war der Prophet {{ $page->pbuh() }} über den Tod seiner ersten Frau Khadeejah betrübt, die ihn unterstützt und an seiner Seite gestanden hatte, und er nannte das Jahr, in dem sie starb, das Jahr der Trauer. Dann heiratete er Sawdah, die eine ältere Frau war und nicht sehr schön; er heiratete sie vielmehr, um sie zu trösten, nachdem ihr Mann gestorben war. Vier Jahre später heiratete der Prophet {{ $page->pbuh() }} Aisha. Die Gründe für die Heirat waren folgende:

1. Er hatte einen Traum, in dem er sie heiratete. Aus der Überlieferung von Aisha geht hervor, dass der Prophet {{ $page->pbuh() }} zu ihr sagte:

{{ $page->hadith('bukhari:3895') }}

2. Die Eigenschaften der Intelligenz und Klugheit, die dem Propheten {{ $page->pbuh() }} schon als kleines Kind an Aisha aufgefallen waren, so dass er sie heiraten wollte, damit sie besser als andere in der Lage sein würde, Berichte über das, was er tat und sagte, zu übermitteln. In der Tat war sie, wie bereits erwähnt, für die Gefährten des Propheten {{ $page->pbuh() }} ein Bezugspunkt in Bezug auf ihre Angelegenheiten und Rechtsprechung.

3. Die Liebe des Propheten {{ $page->pbuh() }} zu ihrem Vater Abu Bakr und die Verfolgung, die Abu Bakr für den Ruf zum Islam erlitten hatte, die er mit Geduld ertrug. Nach den Propheten war er der stärkste und aufrichtigste Mensch im Glauben.

### Gab es Einwände gegen ihre Heirat?

Die Antwort darauf ist nein. Es gibt absolut keine Aufzeichnungen aus muslimischen, säkularen oder anderen historischen Quellen, die auch nur implizit etwas anderes als die tiefe Freude aller Beteiligten über diese Hochzeit zeigen. Nabia Abbott beschreibt die Hochzeit von Aisha mit dem Propheten {{ $page->pbuh() }} wie folgt:

> In keiner Version wird irgendein Kommentar über den Altersunterschied zwischen Mohammed und Aischa oder über das zarte Alter der Braut gemacht, die höchstens nicht älter als zehn Jahre sein konnte und die noch sehr in ihr Spiel verliebt war.

Selbst der bekannte kritische Orientalist, W. Montgomery Watt, sagte über den moralischen Charakter des Propheten {{ $page->pbuh() }} folgendes:

> Aus der Sicht von Mohammeds Zeit können die Vorwürfe des Verrats und der Sinnlichkeit also nicht aufrechterhalten werden. Seine Zeitgenossen hielten ihn in keiner Weise für moralisch mangelhaft. Im Gegenteil, einige der vom modernen Westler kritisierten Taten zeigen, dass Mohammeds Maßstäbe höher waren als die seiner Zeit.

Abgesehen davon, dass niemand mit ihm oder seinen Taten unzufrieden war, war er in seiner Gesellschaft und Zeit ein herausragendes Beispiel für moralischen Charakter. Daher ist es nicht nur absurd, sondern auch unfair, seine Moral auf der Grundlage der Standards unserer heutigen Gesellschaft und Kultur zu beurteilen.

### Heirat in der Pubertät heute

Die Zeitgenossen des Propheten (sowohl Feinde als auch Freunde) akzeptierten die Ehe des Propheten {{ $page->pbuh() }} mit Aisha offensichtlich ohne Probleme. Der Beweis dafür ist, dass diese Ehe bis in die Neuzeit hinein nicht kritisiert wurde.

Der Grund dafür ist ein Wandel in der heutigen Kultur, was das Heiratsalter betrifft. Aber auch heute, im 21. Jahrhundert, ist das Schutzalter vielerorts noch recht niedrig. In Deutschland, Italien und Österreich ist Sex bereits ab 14 Jahren legal, auf den Philippinen und in Angola bereits ab 12 Jahren. Kalifornien war der erste Bundesstaat, der das Schutzalter 1889 auf 14 Jahre erhöhte. Nach Kalifornien zogen auch andere Bundesstaaten nach und erhöhten das Schutzalter.

### Der Islam und das Alter der Pubertät

Der Islam lehrt eindeutig, dass das Erwachsenenalter mit dem Erreichen der Pubertät beginnt. Allah sagt im Quran:

{{ $page->verse('24:59..') }}

Das Erreichen der Pubertät bei Frauen ist mit dem Beginn der Menstruation verbunden, wie Allah im Quran sagt:

{{ $page->verse('65:4..') }}

Daher ist es Teil des Islam, die Pubertät als Beginn des Erwachsenenlebens anzuerkennen. Es ist die Zeit, in der die Person bereits gereift und bereit für die Verantwortung eines Erwachsenen ist. Auf welcher Grundlage sollte also jemand die Ehe von Aisha kritisieren, da ihre Ehe vollzogen wurde, nachdem sie die Pubertät erreicht hatte?

Würde man den Vorwurf des „Kindesmissbrauchs“ gegen den Propheten {{ $page->pbuh() }} erheben, müssten wir auch alle Semiten in die Anklage einbeziehen, die eine Heirat mit der Pubertät als Norm akzeptierten.

Darüber hinaus widerlegt die Tatsache, dass der Prophet {{ $page->pbuh() }} außer Aischa keine Jungfrau geheiratet hatte und alle seine anderen Frauen bereits zuvor verheiratet waren (und viele von ihnen alt waren), die von vielen feindseligen Quellen verbreitete Vorstellung, dass das grundlegende Motiv hinter den Ehen des Propheten körperliches Verlangen und Freude an Frauen gewesen sei. Denn wenn dies seine Absicht gewesen wäre, hätte er nur junge Frauen gewählt.

### Schlussfolgerungen

Wir haben also gesehen, dass:

- Im Arabien des 7. Jahrhunderts war es in der semitischen Gesellschaft üblich, Ehen während der Pubertät zuzulassen.
- Es liegen keine Berichte über Einspruch gegen die Heirat des Propheten mit Aisha vor, weder von seinen Freunden noch von seinen Feinden.
- Auch heute noch gibt es Kulturen, die Ehen während der Pubertät für junge Frauen zulassen.

Trotz dieser wohlbekannten Tatsachen beschuldigen manche Menschen den Propheten {{ $page->pbuh() }} immer noch der Unmoral. Dabei war er es, der den Frauen Arabiens Gerechtigkeit brachte und sie auf ein Niveau hob, das sie in ihrer Gesellschaft zuvor nicht gekannt hatten, etwas, was antike Zivilisationen ihren Frauen nie angetan hatten.

Als er zum Beispiel zum ersten Mal Prophet wurde, hatten die Heiden in Arabien die Missachtung der Frauen geerbt, die bei ihren jüdischen und christlichen Nachbarn überliefert worden war. Sie betrachteten es als so schändlich, mit einem weiblichen Kind gesegnet zu werden, dass sie sogar so weit gingen, dieses Baby lebendig zu begraben, um die mit weiblichen Kindern verbundene Schande zu vermeiden. Allah sagt im Quran:

{{ $page->verse('16:58-59') }}

Muhammad {{ $page->pbuh() }} riet nicht nur streng von dieser Tat ab und verurteilte sie, sondern er lehrte die Menschen auch, ihre Töchter und Mütter als Partner und Quellen der Erlösung für die Männer ihrer Familie zu respektieren und zu schätzen. Er sagte:

{{ $page->hadith('ibnmajah:3669') }}

In einem Hadith von Anas Ibn Malik wurde außerdem Folgendes überliefert:

{{ $page->hadith('muslim:2631') }}

Mit anderen Worten: Wenn jemand den Gesandten Allahs {{ $page->pbuh() }} liebt und am Tag der Auferstehung mit ihm im Himmel sein möchte, dann sollte er gut zu seinen Töchtern sein. Das ist sicherlich nicht die Tat eines „Kinderschänders“, wie manche uns glauben machen wollen.
