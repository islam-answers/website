---
extends: _layouts.answer
section: content
title: Gli ebrei sono il popolo eletto?
date: 2025-01-19
description: Gli ebrei furono scelti grazie ai loro profeti, ma persero il favore
  a causa del rifiuto, e il popolo migliore divenne quello che seguì il profeta Mohammad.
sources:
- href: https://www.islamweb.net/en/fatwa/91584/
  title: islamweb.net
- href: https://islamqa.org/hanafi/fatwaa-dot-com/76468/
  title: islamqa.org (Fatwaa.com)
- href: https://islamqa.info/en/answers/9905/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/15096/
  title: islamweb.net
translator_id: 573107
---

Il Corano testimonia che Allah aveva scelto i Figli di Israele (Israele è il Profeta Ya'qub / Giacobbe {{ $page->pbuh() }}), preferendoli a tutti gli altri popoli di quel tempo. Ci sono molti versetti nel Corano al riguardo.

E non erano stati scelti per la loro razza o colore della pelle, ma fu grazie al gran numero di Profeti [su di loro la pace] che Allah l'Altissimo inviò in mezzo a loro. Allah favorirà sempre coloro che credono in Lui e che compiono buone azioni, indipendentemente dal colore, razza o nazionalità. Allah dice nel Corano:

{{ $page->verse('16:97') }}

Il profeta Mohammad {{ $page->pbuh() }} ha detto:

{{ $page->hadith('muslim:1842') }}

Allah dice nel Corano:

{{ $page->verse('5:20') }}

Allah dice inoltre:

{{ $page->verse('44:32') }}

Eppure, non furono riconoscenti per questa grazia ricevuta e negarono ciò che i Profeti avevano portato loro. Hanno quindi meritato l'ira e la maledizione di Allah. Sono stati umiliati [da Allah] ovunque siano stati sopraffatti, tranne che per un patto di Allah e un trattato del popolo, e Allah ha fatto di loro scimmie e maiali.

Durante la vita del Profeta Mohammad {{ $page->pbuh() }}, gli ebrei hanno sempre sostenuto di essere gli eletti, prescelti da Allah. Allah rivelò il seguente versetto in risposta a loro:

{{ $page->verse('5:18..') }}

In conclusione, gli ebrei erano preferiti ad altri popoli al tempo dei loro Profeti poiché a volte obbedivano loro, ma il più delle volte li tacciavano di menzogna e addirittura li uccidevano. Allah dice nel Corano:

{{ $page->verse('..2:87') }}

Tuttavia, quando Allah inviò il Profeta Mohammad {{ $page->pbuh() }} come ultimo Messaggero, Egli considerò come migliori le persone che lo seguirono. Allah dice nel Corano:

{{ $page->verse('3:110') }}

Allah dice inoltre:

{{ $page->verse('2:143..') }}

Data questa situazione, all'epoca del profeta Mohammad {{ $page->pbuh() }} gli ebrei con lui non si comportarono meglio di quanto avevano fatto con i profeti precedenti, che Allah li benedica, anzi lo tacciarono di menzogna, gli fecero del male e addirittura cospirarono per ucciderlo.
