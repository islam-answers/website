---
extends: _layouts.answer
section: content
title: Hvem skrev Koranen?
date: 2024-06-15
description: Koranen er de ordrette ord fra Gud, som Han åpenbarte til Sin profet
  Mohammad gjennom engelen Gabriel.
sources:
- href: https://www.islam-guide.com/no/frm-ch1.htm
  title: islam-guide.com
---

Gud har støttet Sin siste profet Mohammad {{ $page->pbuh() }} med mange mirakler og mange bevis som bekrefter at han er en sann profet sendt av Gud. Gud har også støttet Sitt siste testamente, den hellige Koranen, med mange mirakler som beviser at Koranen er de ordrette ord fra Gud, åpenbart av Ham og ikke forfattet av noe menneske.

Koranen er de ordrette ord fra Gud, som Han åpenbarte til Sin profet Mohammad {{ $page->pbuh() }} gjennom engelen Gabriel. Mohammad {{ $page->pbuh() }} lærte Koranen utenat, og dikterte den videre til sine følgesvenner. De memorerte også Koranen, skrev den ned og gikk gjennom den sammen med profeten Mohammad {{ $page->pbuh() }}. Dessuten gikk profeten Mohammad {{ $page->pbuh() }} gjennom Koranen sammen med engelen Gabriel en gang i året, og to ganger det siste året av hans liv. Fra Koranen ble åpenbart og fram til i dag, har det alltid vært et stort antall muslimer som har lært hele Koranen utenat, ord for ord. Noen av dem har til og med klart å lære hele Koranen utenat allerede i tiårsalderen. Ikke en eneste bokstav har blitt forandret i Koranen gjennom århundrene.

Koranen ble åpenbart for fjortenhundre år siden, og nevner fakta som kun i nyere tid har blitt oppdaget eller bevist av vitenskapsmenn. Dette beviser uten tvil at Koranen må være de ordrette ord fra Gud, åpenbart av Ham til profeten Mohammad {{ $page->pbuh() }}, og at Koranen ikke ble forfattet av Mohammad {{ $page->pbuh() }} eller av noe annet menneske. Dette beviser også at Mohammad {{ $page->pbuh() }} sannelig er en profet sendt av Gud. Det er helt urealistisk at noen for fjortenhundre år siden skulle ha kjent til disse fakta, som er oppdaget eller bevist i nyere tid med avansert utstyr og kompliserte vitenskapsmetoder.
