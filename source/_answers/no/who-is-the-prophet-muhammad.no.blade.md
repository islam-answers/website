---
extends: _layouts.answer
section: content
title: Hvem er profeten Mohammad?
date: 2024-06-15
description: Profeten Mohammad var et perfekt eksempel på hvor ærlig, barmhjertig,
  medfølende, sannferdig og heltemodig et menneske kan være.
sources:
- href: https://www.islam-guide.com/no/ch3-8.htm
  title: islam-guide.com
---

Mohammad {{ $page->pbuh() }} ble født i Mekka i år 570. Hans far døde før han ble født, og hans mor døde kort tid etter. Han ble derfor oppdratt av sin onkel, som var fra den respektable Kuraysh-stammen. Han vokste opp som analfabet, ute av stand til å lese eller skrive, og dette vedvarte helt til hans død. Før hans kall som profet, var hans folk uvitende om vitenskap, og de fleste var analfabeter. Under oppveksten ble han kjent for å være både rettskaffen, ærlig, pålitelig, sjenerøs og oppriktig. Han ble ansett som så pålitelig at han ble kalt ’den pålitelige’. Mohammad {{ $page->pbuh() }} var svært religiøs, og han hadde i lang tid følt avsky for det moralske forfallet og avgudsdyrkelsen i samfunnet rundt seg.

Da han var førti år gammel, mottok Mohammad {{ $page->pbuh() }} sin første åpenbaring fra Gud gjennom engelen Gabriel. Åpenbaringene fortsatte i tjuetre år, og samlet er de kjent som Koranen.

Så snart han begynte å resitere Koranen og forkynne sannheten som Gud hadde åpenbart for ham, ble han og hans lille gruppe av følgesvenner forfulgt av de vantro. Forfølgelsen ble så voldsom at Gud i år 622 ga dem ordre om å emigrere. Utvandringen fra Mekka til byen Medina, som ligger ca. 418 kilometer lengre nord, markerer begynnelsen på den muslimske kalenderen.

Etter mange år kunne Mohammad {{ $page->pbuh() }} og hans følgesvenner returnere til Mekka, hvor de tilga sine fiender. Før Mohammad {{ $page->pbuh() }} døde, i en alder av sekstitre år, var mesteparten av den arabiske halvøya blitt muslimsk, og innen et århundre etter hans død, hadde islam spredt seg til Spania i vest og så langt øst som til Kina. Noen av årsakene til den raske og fredelige utbredelsen av islam, var sannheten og klarheten i dens budskap. Islams kall er troen på kun en Gud, den Eneste som er verdig å tilbe.

Profeten Mohammad {{ $page->pbuh() }} var et perfekt eksempel på hvor ærlig, rettferdig, barmhjertig, medfølende, sannferdig og heltemodig et menneske kan være. Selv om han var et menneske, så var han høyt hevet over ondskapsfulle karaktertrekk, og han strevde kun for å tjene Gud og for å søke Hans belønning i det hinsidige. Alle hans handlinger og gjøremål var preget av hans pliktoppfyllenhet og ærefrykt overfor Gud.
