---
extends: _layouts.main
title: "Privacy Policy and Terms of Service"
section: body
---

# Privacy Policy

We use anonymous metrics to understand how you use our website and where our traffic comes from. We use this insight to improve our website and create a better experience for visitors. We do not track you, serve personalized advertising, or sell the data to anyone.

Our website is powered by Simple Analytics, a privacy-friendly web analytics provider. You can learn more about Simple Analytics on [its website](https://www.simpleanalytics.com/?referral=qatac).

Data submitted to forms on the website is stored and processed by Tally according to [their privacy policy](https://tally.so/help/privacy-policy).

Our website does not store any cookies on your device. We do not use any tracking scripts or third-party services that may track you across websites.

# Terms of Service

You may use or redistribute any content on this website for personal or educational purposes. You may not make changes to the content or use it for commercial purposes without our permission.

Please reach out at [contact at islam-answers.com](mailto:contact@islam-answers.com) for any questions or requests.
