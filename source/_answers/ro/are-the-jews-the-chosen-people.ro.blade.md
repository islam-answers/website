---
extends: _layouts.answer
section: content
title: Sunt evreii poporul ales?
date: 2024-12-16
description: Evreii au fost aleși pentru profeții lor, dar au pierdut favoarea din
  cauza respingerii, cea mai bună națiune devenind adepții profetului Muhammad.
sources:
- href: https://www.islamweb.net/en/fatwa/91584/
  title: islamweb.net
- href: https://islamqa.org/hanafi/fatwaa-dot-com/76468/
  title: islamqa.org (Fatwaa.com)
- href: https://islamqa.info/en/answers/9905/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/15096/
  title: islamweb.net
translator_id: 554943
---

Coranul mărturisește că Allah i-a ales pe Copiii lui Israel (Israel fiind Profetul Ya'qoob / Iacov {{ $page->pbuh() }}) la vremea lor și că i-a preferat față de toți ceilalți oameni din vremea lor. Există multe versete în Coran în această privință.

Cu toate acestea, ei nu au fost aleși pentru rasa sau culoarea lor, ci datorită numărului mare de profeți care le-au fost trimiși - fie ca Allah să le înalțe pomenirea. Allah îi va favoriza întotdeauna pe cei care cred în El și fac fapte bune, indiferent de culoarea, rasa sau naționalitatea lor. Allah spune în Coran:

{{ $page->verse('16:97') }}

Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:1842') }}

Allah spune în Coran:

{{ $page->verse('5:20') }}

Allah mai spune:

{{ $page->verse('44:32') }}

Cu toate acestea, ei au fost nerecunoscători pentru această generozitate și au negat ceea ce le-au adus Profeții. Astfel, ei au meritat mânia și blestemul lui Allah. Ei au fost supuși umilinței [de către Allah] oriunde au fost prinși, cu excepția unui legământ de la Allah și a unui tratat de la oameni, iar Allah a făcut din ei maimuțe și porci.

În timpul vieții Profetului Muhammad {{ $page->pbuh() }}, evreii au pretins întotdeauna că ei sunt cei aleși și iubiți de Allah. Allah a revelat următorul verset ca răspuns:

{{ $page->verse('5:18..') }}

În concluzie, evreii au fost preferați altor popoare pe vremea profeților lor; uneori îi ascultau, dar de cele mai multe ori îi dezmințeau și chiar îi ucideau. Allah spune în Coran:

{{ $page->verse('..2:87') }}

Cu toate acestea, atunci când Allah l-a trimis pe Profetul Muhammad {{ $page->pbuh() }} ca ultim Mesager, Allah i-a considerat cei mai buni oameni pe cei care l-au urmat pe acesta. Allah spune în Coran:

{{ $page->verse('3:110') }}

Allah mai spune:

{{ $page->verse('2:143..') }}

Întrucât aceasta era situația, evreii din vremea profetului Muhammad {{ $page->pbuh() }} nu au fost mai buni cu el decât au fost cu profeții anteriori, fie ca Allah să le înalțe pomenirea; ci l-au negat, i-au făcut rău și chiar au complotat să îl omoare.
