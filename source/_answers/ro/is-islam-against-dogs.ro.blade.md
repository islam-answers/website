---
extends: _layouts.answer
section: content
title: Este Islamul împotriva câinilor?
date: 2024-08-11
description: Islamul ne învață să tratăm animalele într-o manieră demnă, umană și
  milostivă. Câinii nu pot fi animale de companie, cu anumite excepții.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 554943
---

Animalele fac parte din creația lui Allah și nu le privim cu ochi răi; mai degrabă, suntem datori, ca musulmani, să tratăm animalele într-un mod demn, uman și milostiv. Trimisul lui Allah {{ $page->pbuh() }} a spus,

{{ $page->hadith('bukhari:3321') }}

## Uciderea câinilor

Abdallah ibn Umar a spus:

{{ $page->hadith('muslim:1570b') }}

Este important să clarificăm la ce fel de câini se face referire în această narațiune, precum și contextul general. Câinii la care se face referire aici sunt haitele de câini sălbatici care se plimbă prin orașe și sunt o pacoste pentru comunitate și un risc pentru sănătate. Ei transportă și răspândesc boli, la fel ca șobolanii, și provoacă multe neplăceri oamenilor. Uciderea animalelor periculoase sau a celor care răspândesc boli este un lucru comun și acceptat în întreaga lume.

Pe baza tuturor dovezilor generale, juriștii au menționat că porunca de a ucide un câine este permisă numai dacă acesta este prădător și dăunător; însă un câine inofensiv, indiferent de culoare, nu poate fi ucis.

## Puritate

Saliva câinilor este impură conform majorității juriștilor musulmani, la fel ca și blana lor, conform unora. Una dintre cele mai importante învățături ale Islamului este puritatea la toate nivelurile, așa că acest lucru a fost luat în considerare, la fel ca și faptul că îngerii nu frecventează un loc în care există câini. Acest lucru se bazează pe cuvintele Profetului {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3322') }}

## Câinii ca animale de companie

Islamul le interzice musulmanilor să aibă câini ca animale de companie. Trimisul lui Allah {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:1574g') }}
