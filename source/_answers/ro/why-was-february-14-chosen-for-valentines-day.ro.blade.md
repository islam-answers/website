---
extends: _layouts.answer
section: content
title: De ce Ziua Îndrăgostiților este pe 14 februarie?
date: 2025-02-12
description: Ziua Îndrăgostiților, pe 14 februarie, a înlocuit festivalul păgân Lupercalia
  și a fost ulterior asociată cu Sfântul Valentin.
sources:
- href: https://muslimskeptic.com/2022/02/14/the-dark-origins-of-valentines-day-muslims-might-be-unaware-of/
  title: muslimskeptic.com
- href: https://muslimskeptic.com/2022/02/15/its-not-just-christmas-how-christianity-adopts-paganism-for-valentines-day/
  title: muslimskeptic.com
- href: https://islamqa.info/en/answers/73007/
  title: islamqa.info
- href: https://islamqa.info/en/articles/65/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145893/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21694/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1130/
  title: islamqa.info
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 554943
---

### Care este originea Zilei Îndrăgostiților?

Pentru a înțelege originile Zilei Îndrăgostiților, trebuie mai întâi să ne uităm la istoria acesteia. Istoricii au căzut de acord că ea provine din două evenimente istorice separate, unul implicând un preot creștin (Sfântul Valentin), iar celălalt despre festivalul păgân Lupercalia.

Lupercalia era o sărbătoare sângeroasă, violentă și cu încărcătură sexuală, plină de sacrificii de animale, de potriviri la întâmplare între cupluri, în speranța de a alunga spiritele rele și infertilitatea. Acest festival josnic era sărbătorit anual pe 15 februarie, la doar o zi după Ziua Îndrăgostiților din zilele noastre. Inițial, era cunoscută sub numele de Februa, care înseamnă "Purificări", care stă la baza numelui lunii februarie.

Totul începea cu adunarea unui grup de preoți romani numiți Luperci într-un anumit loc, de exemplu în peștera Lupercal. Acești Luperci sacrificau apoi o capră și un câine, evident în numele propriilor zeități false. Sacrificarea caprei simboliza fertilitatea. Lupercii alergau apoi prin oraș, biciuind toate femeile pe care le întâlneau. Acest lucru era, din nou, un simbol al fertilității. Se credea că aceste femei voiau să fie biciuite în speranța de a deveni fertile. Cu toate acestea, există relatări istorice conform cărora această parte a ritualului nu era întotdeauna atât de consensuală. Ritualul "romantic" se încheia cu tineri bărbați și femei care se împerecheau. Cu alte cuvinte, aceasta era o noapte de fornicație.

Îmbinarea acestei sărbători cu povestea Sfântului Valentin, pe care mulți oameni din zilele noastre o consideră originea principală a acestei ocazii, a fost cel mai probabil o mișcare a Bisericii Catolice de a face creștinismul mai atractiv pentru păgâni. Sfântul Valentin este un nume care este dat la doi dintre vechii "martiri" ai Bisericii creștine. Se spunea că au fost doi, sau că a fost doar unul. Când romanii au acceptat creștinismul, au continuat să sărbătorească festivalul lor numit Sărbătoarea Iubirii, dar au înlocuit conceptul păgân de "iubire spirituală" cu un alt concept cunoscut sub numele de "martirii iubirii", reprezentat de Sfântul Valentin, care a pledat pentru iubire și pace, cauză pentru care a fost martirizat, potrivit afirmațiilor lor. Era numită și Sărbătoarea Îndrăgostiților, iar Sfântul Valentin era considerat protectorul îndrăgostiților.

Una dintre credințele lor false legate de acest festival era că numele fetelor care au ajuns la vârsta căsătoriei vor fi scrise pe mici role de hârtie și așezate într-un vas pe o masă. Apoi, tinerii care doreau să se căsătorească erau chemați și fiecare dintre ei alegea o bucată de hârtie. El se punea la dispoziția fetei al cărei nume îl alesese timp de un an, pentru ca ei să afle unul despre celălalt. Apoi se căsătoreau sau repetau din nou același proces în ziua festivalului din anul următor. Clerul creștin a reacționat împotriva acestei tradiții, pe care o considera ca având o influență corupătoare asupra moralității tinerilor și tinerelor.

De asemenea, s-a spus despre originile acestei sărbători că atunci când romanii au devenit creștini, după ce creștinismul s-a răspândit, împăratul roman Claudius al II-lea a decretat în secolul al III-lea că soldații nu ar trebui să se căsătorească, deoarece căsătoria i-ar distrage de la războaiele pe care le duceau. Acest decret a fost combătut de Sfântul Valentin, care a început să oficieze căsătorii pentru soldați în secret. Când împăratul a aflat despre acest lucru, l-a aruncat în închisoare și l-a condamnat la execuție. În închisoare, Sfântul Valentin s-a îndrăgostit de fiica temnicerului, dar acest lucru a rămas secret deoarece, potrivit legilor creștine, preoților și călugărilor le era interzis să se căsătorească sau să se îndrăgostească. Dar el este încă foarte apreciat de creștini datorită fermității sale de a adera la creștinism atunci când împăratul s-a oferit să îl ierte dacă renunță la creștinism și se închină zeilor romani; atunci ar fi devenit unul dintre cei mai apropiați confidenți ai săi și ginerele său. Dar Valentin a refuzat această ofertă și a preferat creștinismul, așa că a fost executat pe 14 februarie 270, în ajunul zilei de 15 februarie, sărbătoarea Lupercalia. Astfel, această zi a fost numită după acest sfânt.

### De ce nu sărbătoresc musulmanii Ziua Îndrăgostiților?

Cineva s-ar putea întreba: de ce nu sărbătoresc musulmanii acest festival? În primul rând, festivalurile fac parte din ceremoniile religioase despre care Allah spune în Coran:

{{ $page->verse('..5:48..') }}

Allah mai spune:

{{ $page->verse('22:67..') }}

Deoarece Ziua Îndrăgostiților datează din perioada romană, nu din perioada islamică, acest lucru înseamnă că este ceva ce aparține exclusiv creștinismului, nu islamului, iar musulmanii nu au nici o parte și nici un rol în aceasta. Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:892') }}

Aceasta înseamnă că fiecare națiune ar trebui să se distingă prin sărbătorile sale. Dacă creștinii au un festival și evreii au un festival care le aparține exclusiv, atunci niciun musulman nu ar trebui să li se alăture, la fel cum nu le împărtășește religia sau direcția de rugăciune.

În al doilea rând, sărbătorirea Zilei Îndrăgostiților înseamnă imitarea romanilor păgâni. Nu este permis musulmanilor să îi imite pe nemusulmani în lucruri care nu fac parte din islam. Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('abudawud:4031') }}

În al treilea rând, este o greșeală să confundăm ceea ce se numește această zi cu intențiile reale din spatele ei. Iubirea la care se face referire în această zi este iubirea romantică, dintre iubiți și iubite. Se știe că este o zi a promiscuității și a sexului, fără rețineri sau restricții. Nu este vorba despre iubirea pură dintre un bărbat și soția sa sau dintre o femeie și soțul ei, sau cel puțin nu se face distincție între iubirea legitimă din relația dintre soț și soție și iubirea interzisă dintre amanți.

În islam, soțul își iubește soția pe tot parcursul anului și își exprimă dragostea față de ea prin cadouri, în versuri și în proză, în scrisori și în alte moduri, de-a lungul anilor - nu doar într-o singură zi din an. Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:1468') }}

Nu există nicio religie care să își încurajeze adepții să se iubească și să aibă grijă unii de alții mai mult decât o face islamul. Acest lucru se aplică în orice moment și în orice situație. Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:54') }}

### Cine beneficiază de Ziua Îndrăgostiților?

Ziua Îndrăgostiților a devenit o sărbătoare foarte comercializată, cu numeroase tradiții și obiceiuri. Oamenii fac schimb de felicitări cu poezii de dragoste, organizează petreceri festive și oferă cadouri iubiților/iubitelor. Impactul comercial este semnificativ - numai în Marea Britanie, vânzările de flori au ajuns la 22 de milioane de lire sterline. Consumul de ciocolată crește spectaculos, în timp ce prețul trandafirilor poate crește de zece ori, de la 1 la 10 dolari pe tulpină. Magazinele de cadouri și de felicitări concurează intens cu marfă specializată pentru Ziua Îndrăgostiților, iar unele familii chiar își decorează casele cu trandafiri roșii pentru această ocazie. Amploarea economică a Zilei Îndrăgostiților este uriașă - în 2020, americanii au cheltuit 27,4 miliarde de dolari pe cadouri de Ziua Îndrăgostiților, potrivit National Retail Federation (NRT), cea mai mare asociație de comerț cu amănuntul din lume. În cele din urmă, producătorii și comercianții cu amănuntul sunt cei care beneficiază cel mai mult de pe urma acestei sărbători comercializate a iubirii.
