---
extends: _layouts.answer
section: content
title: 为什么伊斯兰教不允许吃猪肉？
date: 2024-07-19
description: 作为穆斯林，最基本的条件就是遵循真主的命令，远离真主所禁止的，无论真主制定这一法度的哲理是否已昭示。
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 554943
---

作为穆斯林，最基本的条件就是遵循真主的命令，远离真主所禁止的，无论真主制定这一法度的哲理是否已昭示。

作为一个穆斯林，即便他不明白伊斯兰教法的某项规定所具有的哲理，他也没有权利拒绝这个教法规定，或是在遵循上保持中立，而是应该在有依据的的前提下完全接受教法所判定的合法或非法，不管其是否知道这项判定的奥秘为何！清高的真主说：

{{ $page->verse('33:36') }}

### 为什么猪肉在伊斯兰教中是被禁止的？

以《古兰经》明文为据，猪肉在伊斯兰法律中是非法的。清高的真主说：

{{ $page->verse('2:173..') }}

穆斯林除非是在生命受到威胁的特殊情况下允许食用猪肉，而在其它的任何情况下都禁止吃猪肉。如在饥饿难耐，再不吃东西就有可能会被饿死，同时又找不到其它东西可吃的情况下，可以吃猪肉。这是符合教法原则的——特殊情况可以违禁。

除了清高真主所说的【它是污秽的】之外，没有任何教律说明禁止猪肉的特殊原因。

{{ $page->verse('..6:145..') }}

“污秽”包括所有教法和人类天性所认为丑陋的东西，仅就这一因素就足够了。

另外还有广义上的原因，一些饮料和食品等被禁止的原因也同时说明了禁止猪肉的原因。这个广义的原因即清高真主所说的

{{ $page->verse('..7:157..') }}

### 禁止食用猪肉的科学和医学原因

这里所指的“肮脏污秽之物”系指对人体健康，对人们的财产，对人性有消极因素的东西，所以任何事物只要它会导致某些不良结局，同时它还会影响人们的美满生活，那么它就被列为肮脏事物的范畴。诚然，一些医学报告研究证明猪身上携带着大量有害人体健康的病菌，对于此类病菌的危害性及其所导致的疾病的解释需要很长的篇幅，总而言之猪肉会导致寄生虫疾病、细菌性疾病、病毒性疾病等等。

这些所携带着的病菌足以说明智睿的真主之禁止猪肉的无穷奥秘，就是为了保护人身体健康免受伤害，保护自身的健康是伊斯兰教法所要求人们必须履行的五项重要责任之一。
