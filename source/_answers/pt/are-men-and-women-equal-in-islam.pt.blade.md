---
extends: _layouts.answer
section: content
title: Homens e mulheres são iguais no Islam?
date: 2024-12-31
description: No Islão, homens e mulheres são espiritualmente iguais, com papéis e
  responsabilidades complementares que asseguram equilíbrio, justiça e respeito mútuo.
sources:
- href: https://islamqa.info/en/answers/12840/
  title: islamqa.info
- href: https://islamqa.org/hanafi/fatwacentre/179051/
  title: islamqa.org (FatwaCentre.org)
- href: https://www.islamweb.net/en/article/109364/
  title: islamweb.net
translator_id: 594172
---

O Islam veio para honrar as mulheres e elevar o seu estatuto, para lhes dar uma posição que lhes é própria, para cuidar delas e proteger a sua dignidade. Por isso, o Islam ordena aos tutores e aos maridos das mulheres que gastem com elas, que as tratem bem, que cuidem delas e que sejam amáveis com elas. Allah diz no Alcorão:

{{ $page->verse('..4:19..') }}

É relatado que o Profeta {{ $page->pbuh() }} disse:

{{ $page->hadith('tirmidhi:3895') }}

O Islam confere às mulheres todos os seus direitos e permite-lhes organizar os seus assuntos de forma adequada. Isto inclui todos os tipos de negócios, compra, venda, nomeação de outros para atuar em seu nome, empréstimo, depósito de fundos, etc. Allah diz:

{{ $page->verse('..2:228') }}

O Islam impôs às mulheres os actos de culto e os deveres que lhes são próprios, os mesmos deveres que os homens, nomeadamente a purificação, o Zakat (caridade obrigatória), o jejum, a oração, o Hajj (peregrinação) e outros actos de culto.

Mas o Islam dá à mulher metade da parte do homem no que diz respeito à herança, porque ela não é obrigada a gastar consigo própria, com a sua casa ou com os seus filhos. Pelo contrário, quem é obrigado a gastar com eles é o homem.

O facto de o testemunho de duas mulheres equivaler, em certos casos, ao testemunho de um homem deve-se à tendência das mulheres para serem mais esquecidas do que os homens, devido aos seus ciclos naturais de menstruação, gravidez, parto, educação dos filhos, etc. Todas estas coisas preocupam-nas e tornam-nas esquecidas. Por isso, a evidência shar'i (lei islâmica) indica que outra mulher deve reforçar o testemunho de uma mulher, para que este seja mais exato. Mas há assuntos que só dizem respeito às mulheres e em que o testemunho de uma só mulher é suficiente, como determinar a frequência com que uma criança foi amamentada, os defeitos que podem afetar o casamento, etc.

As mulheres são iguais aos homens em termos de recompensa, permanecendo firmes na fé e praticando boas acções, gozando de uma boa vida neste mundo e de uma grande recompensa no outro. Allah diz:

{{ $page->verse('16:97') }}

As mulheres têm direitos e deveres, tal como os homens têm direitos e deveres. Há assuntos que são do agrado dos homens e, por isso, Deus os atribui aos homens, tal como há assuntos que são do agrado das mulheres e, por isso, Ele os atribui às mulheres.
