---
extends: _layouts.answer
section: content
title: Hva er kvinners status i islam?
date: 2024-06-19
description: Muslimske kvinner har rett til å eie eiendom, tjene penger og bruke dem
  som de vil. Islam oppfordrer menn til å behandle kvinner godt.
sources:
- href: https://www.islam-guide.com/no/ch3-13.htm
  title: islam-guide.com
---

Islam ser på kvinner, både enslige og gifte, som individer med egne rettigheter. Kvinner har rett til å eie og disponere sine eiendommer og inntekter uten noe formynderskap over seg (verken av hennes far, mann eller noen annen). Hun har rett til å kjøpe og selge, gi gaver og veldedighet, og hun kan bruke sine penger som hun vil. Brudgommen gir en brudegave til bruden, som hun personlig kan bruke, og hun beholder sitt eget familienavn heller enn å ta sin manns etternavn.

Islam oppfordrer ektemannen til å behandle sin kone bra, som profeten Mohammad {{ $page->pbuh() }} sa:

{{ $page->hadith('ibnmajah:1977') }}

Mødre er høyt æret i islam. Islam anbefaler å behandle dem på beste måte.

{{ $page->hadith('bukhari:5971') }}
