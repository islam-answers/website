---
extends: _layouts.answer
section: content
title: Czy Jezus twierdził, że jest Bogiem?
date: 2024-11-25
description: Muzułmanie wierzą, że Jezus był prorokiem, który wzywał ludzi do oddawania
  czci wyłącznie Allahowi, nigdy nie twierdząc, że jest Bogiem.
sources:
- href: https://seekersguidance.org/answers/general-counsel/did-jesus-claim-to-be-god/
  title: seekersguidance.org
- href: https://islamqa.info/en/answers/10277
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/88187
  title: islamweb.net
- href: https://islamqa.info/en/answers/43148
  title: islamqa.info
translator_id: 584496
---

Muzułmanie wierzą, że Jezus {{ $page->pbuh() }} był jednym z niewolników Allaha i jednym z Jego szlachetnych Posłańców. Allah posłał Jezusa do dzieci Izraela, aby wezwać je do wiary w samego Allaha i oddawania czci tylko Jemu.

Allah wspierał Jezusa {{ $page->pbuh() }} z cudami, które dowodziły, że mówi prawdę; Jezus urodził się z Dziewicy Maryam (Maryi) bez ojca. On pozwolił Żydom na niektóre rzeczy, które były dla nich zakazane, nie umarł, a jego wrogowie, Żydzi, nie zabili go, ale Allah uratował go od nich i wskrzesił żywego do nieba. Jezus powiedział swoim naśladowcom o nadejściu naszego Proroka Mahometa {{ $page->pbuh() }}. Jezus powróci na końcu czasów. W Dniu Zmartwychwstania wyrzeknie się twierdzeń, że był Bogiem.

W Dniu Zmartwychwstania Jezus stanie przed Władcą Światów, który zapyta Go w obecności świadków, co powiedział Dzieciom Izraela, jak powiedział Allah:

{{ $page->verse('5:116-118') }}

Uczciwa analiza pokazuje, że Jezus {{ $page->pbuh() }} nigdy nie powiedział wprost i jednoznacznie, że jest Bogiem. To autorzy Nowego Testamentu, z których żaden nie spotkał Jezusa, twierdzili, że jest Bogiem i propagowali to. Nie ma dowodów na to, że prawdziwi apostołowie Jezusa twierdzili to lub że byli autorami Biblii. Zamiast tego Jezus jest pokazany jako nieustannie czczący i wzywający innych do czczenia tylko Boga.

Twierdzenie, że Jezus {{ $page->pbuh() }} jest Bogiem, nie może być przypisywane Jezusowi, skoro jest tak jaskrawo sprzeczne z Pierwszym Przykazaniem, aby nie brać sobie bogów poza Bogiem ani nie czynić bożka z czegokolwiek na niebie i ziemi. Ponadto logicznie absurdalne jest, aby Bóg stał się swoim stworzeniem.

Zatem jedynie twierdzenie islamu jest wiarygodne i zgodne z wczesnym objawieniem: Jezus {{ $page->pbuh() }} był prorokiem, a nie Bogiem.
