---
extends: _layouts.answer
section: content
title: De quoi le Coran parle-t-il ?
date: 2024-04-03
description: La source principale de la foi et de la pratique de chaque musulman.
sources:
- href: https://www.islam-guide.com/fr/ch3-7.htm
  title: islam-guide.com
---

Le Coran, le dernier livre révélé par Dieu, est la principale source sur laquelle se base chaque musulman pour sa foi et la pratique de sa religion. Il traite de tous les sujets qui concernent les êtres humains: sagesse, doctrine, adoration, transactions, lois, etc., mais son thème de base est la relation entre Dieu et Ses créatures. Il contient également des lignes de conduite et des enseignements détaillés nécessaires à une société juste, à un comportement convenable et à un système économique équitable.

Soulignons que le Coran a été révélé à Mohammed {{ $page->pbuh() }} en arabe seulement. Donc n'importe quelle traduction du Coran, en anglais ou dans une autre langue, n'est pas le Coran lui-même, ni une autre version du Coran, mais seulement une traduction du sens de ses versets. Le Coran n'existe que dans la langue arabe dans laquelle il a été révélé.
