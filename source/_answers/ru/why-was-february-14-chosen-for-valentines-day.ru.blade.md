---
extends: _layouts.answer
section: content
title: Почему 14 февраля было выбрано для Дня святого Валентина?
date: 2025-02-18
description: День святого Валентина, отмечаемый 14 февраля, заменил языческий праздник
  Луперкалии и позднее был связан со Святым Валентином.
sources:
- href: https://muslimskeptic.com/2022/02/14/the-dark-origins-of-valentines-day-muslims-might-be-unaware-of/
  title: muslimskeptic.com
- href: https://muslimskeptic.com/2022/02/15/its-not-just-christmas-how-christianity-adopts-paganism-for-valentines-day/
  title: muslimskeptic.com
- href: https://islamqa.info/en/answers/73007/
  title: islamqa.info
- href: https://islamqa.info/en/articles/65/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145893/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21694/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1130/
  title: islamqa.info
- href: https://islamqa.info/en/answers/70042/
  title: islamqa.info
translator_id: 594830
---

### Каково происхождение Дня святого Валентина?

Чтобы понять происхождение Дня святого Валентина, нам нужно сначала взглянуть на его историю, которая, по мнению историков, представляет собой смесь двух отдельных исторических событий, одно из которых связано с христианским священником (святым Валентином), а другое - с языческим праздником Луперкалии.

Луперкалии были кровавым, жестоким и сексуально заряженным праздником, наполненным жертвоприношениями животных, случайными сватовствами и союзом в надежде отогнать злых духов и бесплодие. Этот мерзкий праздник отмечался ежегодно 15 февраля, всего через день после современного Дня святого Валентина. Изначально он назывался Februa, что означает "Очищение" или "Чистка", и именно это название послужило основой для названия месяца февраль.

Все начиналось с того, что группа римских священников, называемых луперками, собиралась в определенном месте, например, в пещере Луперкаль. Затем эти Луперки приносили в жертву козла и собаку, очевидно, во имя своих собственных ложных божеств. Жертвоприношение козла должно было символизировать плодородие. После этого луперчи в диком бешенстве носились по городу и бичевали всех встречных женщин. Это, опять же, символизировало плодородие. Считается, что женщины приветствовали эту порку в надежде стать плодовитыми. Однако есть исторические свидетельства того, что эта часть ритуала не всегда проходила по обоюдному согласию. "Романтический" ритуал заканчивался тем, что юноши и девушки объединялись в пары. Другими словами, это была ночь блуда.

Объединение этого праздника с историей о святом Валентине, которую многие сегодня считают первоисточником этого события, скорее всего, было сделано католической церковью для того, чтобы сделать христианство более привлекательным для язычников. Святой Валентин - это имя, которым называют двух древних "мучеников" христианской церкви. Говорили, что их было двое, или что был только один. Когда римляне приняли христианство, они продолжали отмечать свой праздник под названием Праздник любви, но они изменили его с языческой концепции "духовной любви" на другую концепцию, известную как "мученики любви", представленную святым Валентином, который выступал за любовь и мир, за что и был замучен, согласно их утверждениям. Этот праздник также назывался Праздником влюбленных, а святой Валентин считался покровителем влюбленных.

Одно из ложных поверий, связанных с этим праздником, заключалось в том, что имена девушек, достигших брачного возраста, писали на маленьких рулонах бумаги и клали в блюдо на столе. Затем вызывали молодых людей, желающих жениться, и каждый из них брал себе бумажку. Он отдавал себя в услужение той девушке, чье имя он вытянул, на один год, чтобы они могли узнать друг о друге. Затем они женились или повторяли тот же процесс в день праздника в следующем году. Христианское духовенство выступило против этой традиции, которая, по их мнению, оказывала развращающее влияние на нравственность юношей и девушек.

О происхождении этого праздника также говорят, что когда римляне приняли христианство, римский император Клавдий II в III веке издал указ о том, что солдаты не должны жениться, поскольку брак отвлекает их от войн, которые они вели. Против этого указа выступил Святой Валентин, который начал тайно совершать бракосочетания для солдат. Когда император узнал об этом, он бросил его в тюрьму и приговорил к казни. В тюрьме он (Святой Валентин) влюбился в дочь тюремщика, но это было тайной, потому что по христианским законам священникам и монахам запрещалось жениться и влюбляться. Но он до сих пор высоко ценится христианами за то, что твердо придерживался христианства, когда император предложил помиловать его, если он оставит христианство и будет поклоняться римским богам; тогда он станет одним из его ближайших доверенных лиц и сделает его своим зятем. Но Валентин отказался от этого предложения и предпочел христианство, поэтому его казнили 14 февраля 270 года, накануне 15 февраля, праздника Луперкалий. Так этот день был назван в честь этого святого.

### Почему мусульмане не празднуют День святого Валентина?

Кто-то может спросить: почему мусульмане не празднуют этот праздник? Во-первых, праздники - это часть религиозных церемоний, о которых Аллах говорит в Коране:

{{ $page->verse('..5:48..') }}

Аллах также говорит:

{{ $page->verse('22:67..') }}

Поскольку День святого Валентина восходит к римским, а не исламским временам, это означает, что он принадлежит исключительно христианству, а не исламу, и мусульмане не имеют в нем никакой доли и никакого участия. Пророк Мухаммад {{ $page->pbuh() }} сказал:

{{ $page->hadith('muslim:892') }}

Это означает, что каждый народ должен отличаться своими праздниками. Если у христиан есть праздник, а у иудеев - праздник, который принадлежит исключительно им, то ни один мусульманин не должен присоединяться к ним, так же как он не разделяет их религию или направление молитвы.

Во-вторых, празднование Дня святого Валентина означает уподобление или подражание язычникам-римлянам. Мусульманам не разрешается подражать немусульманам в том, что не является частью ислама. Пророк {{ $page->pbuh() }} сказал:

{{ $page->hadith('abudawud:4031') }}

В-третьих, ошибочно путать то, как называют этот день, с тем, что на самом деле за ним стоит. Любовь, о которой говорят в этот день, - это романтическая любовь, любовницы и любовники, парни и девушки. Известно, что это день распущенности и секса, без каких-либо ограничений и сдерживаний. Речь не идет о чистой любви между мужчиной и его женой или женщиной и ее мужем, или, по крайней мере, они не делают различия между законной любовью в отношениях между мужем и женой и запретной любовью к любовницам и любовникам.

В исламе муж любит свою жену в течение всего года, и он выражает эту любовь к ней подарками, стихами и прозой, письмами и другими способами в течение всего года - не только в один день в году. Пророк {{ $page->pbuh() }} сказал:

{{ $page->hadith('muslim:1468') }}

Ни одна религия не призывает своих последователей любить и заботиться друг о друге так, как это делает ислам. Это касается всех времен и обстоятельств. Пророк {{ $page->pbuh() }} сказал:

{{ $page->hadith('muslim:54') }}

### Кому выгоден День святого Валентина?

День святого Валентина превратился в высококоммерциализированный праздник с многочисленными традициями и обычаями. Люди обмениваются открытками со стихами о любви, устраивают праздничные вечеринки и дарят подарки своим любимым. Коммерческий эффект от этого праздника весьма значителен - только в Великобритании продажи цветов достигли 22 миллионов фунтов стерлингов. Резко возрастает потребление шоколада, а цены на розы могут вырасти в десять раз - от 1 до 10 долларов за стебель. Магазины подарков и открыток ведут острую конкуренцию, предлагая специализированные валентинки, а некоторые семьи даже украшают свои дома красными розами по этому случаю. Экономический масштаб Дня святого Валентина огромен - в 2020 году американцы потратили на подарки 27,4 миллиарда долларов, согласно данным Национальной федерации розничной торговли (NRT), крупнейшей в мире ассоциации розничной торговли. В конечном итоге именно производители и розничные торговцы получают наибольшую выгоду от этого коммерциализированного праздника любви.
