---
extends: _layouts.answer
section: content
title: O que é o Islam?
date: 2024-11-12
description: O Islam significa submissão, humildade, obediência às ordens e às proibições
  sem objecções, adoração exclusiva a Allah e fé n'Ele.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 594172
---

Se consultarmos os dicionários de língua árabe, veremos que o significado da palavra Islam é: submissão, humildade, obediência às ordens e às proibições sem objeção, adoração sincera a Allah, acreditando no que Ele nos diz e tendo fé n'Ele.

A palavra Islam tornou-se o nome da religião que foi introduzida pelo Profeta Muhammad {{ $page->pbuh() }}.

### Por que essa religião se chama Islam

Todas as religiões da terra são chamadas por vários nomes, seja o nome de um homem específico ou de uma nação específica. Assim, o cristianismo toma o nome de Cristo; o budismo toma o nome do seu fundador, Buda; da mesma forma, o judaísmo tomou o nome de uma tribo conhecida como Yehudah (Judá), por isso ficou conhecido como judaísmo. E assim por diante.

Exceto o Islam, porque não é atribuído a um homem específico ou a uma nação específica, mas o seu nome refere-se ao significado da palavra Islam. O que este nome indica é que o estabelecimento e a fundação desta religião não foi obra de um homem em particular e que não é apenas para uma nação em particular, excluindo todas as outras. Pelo contrário, o seu objetivo é dar o atributo implícito na palavra Islam a todos os povos da Terra. Assim, todo aquele que adquire este atributo, quer seja do passado ou do presente, é muçulmano, e todo aquele que adquire este atributo no futuro será também muçulmano.

O Islam é a religião de todos os Profetas. Surgiu no início da Profecia, desde o tempo do nosso pai Adão, e todas as mensagens apelavam ao Islam (submissão a Allah) em termos de crença e de regras básicas. Os crentes que seguiram os profetas anteriores eram todos muçulmanos num sentido geral e entrarão no Paraíso em virtude do seu Islam.
