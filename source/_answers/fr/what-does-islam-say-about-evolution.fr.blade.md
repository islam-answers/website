---
extends: _layouts.answer
section: content
title: Que dit l'Islam sur la théorie de l'évolution ?
date: 2024-07-23
description: Dieu a créé Adam dans sa forme définitive. Les sources islamiques ne donnent pas de réponse claire sur la création des autres êtres vivants.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 586055
---

Allah a créé le premier être humain, Adam, dans sa forme définitive, ce qui est en totale opposition avec l’idée que des singes auraient progressivement évolué en des humains parfaitement formés. La création d’Adam ne peut être directement confirmé ou infirmé par la science, car il s'agit d'un événement historique unique et singulier : un miracle.

En ce qui concerne les autres êtres vivants, les sources islamiques n’ont pas apportés de réponses, ils nous demandent seulement de croire que Dieu les a créées de la manière qu'il a voulu, en les faisant évoluer au fil des siècles.
