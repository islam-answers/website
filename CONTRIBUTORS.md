# Contributors

## Translators

This project would not have been possible without the help of our amazing translators.

- [Sabina Raus](https://www.linkedin.com/in/sabinaraus/) (Romanian, German)
- [Aisha T. Bravi](https://www.linkedin.com/in/aisha-bravi-6249466b/) (Italian)
- [Sarah Julien](https://www.linkedin.com/in/sarah-julien-5a5880187/) (French)
- [Abdu Ahmed](https://www.linkedin.com/in/abdu-ahmed-ali-74387326/) (Amharic)
- [Asma Haniffa](https://www.linkedin.com/in/fathima-asma-haniffa-96468841/) (Sinhala)
- [Agata W](https://www.linkedin.com/in/agata-w/) (Polish)
- [Klaudia Foltyn](https://www.linkedin.com/in/klaudia-foltyn-515a92252/) (Polish)
- [Dijana Beciri](https://www.linkedin.com/in/dijana-beciri-b080bb19/) (Serbian)
- [Maryam Barkat](https://www.linkedin.com/in/maryam-b-1465a92b2/) (Spanish)
- [Shazna Habib](https://www.linkedin.com/in/shazna-habib/) (Sinhala)
- [Sayem Hossen](https://www.linkedin.com/in/sayem-hossen-7975b7210/) (Hindi)
- [Houssaïna Diallo](https://www.linkedin.com/in/houssa%C3%AFnatou-diallo-503557171/) (French)
- [Amira Al Sarraj](https://www.linkedin.com/in/amira-al-sarraj-049482266/) (German)
- [Phong Doan](#) (Vietnamese)
- [Ali Wahba](https://www.linkedin.com/in/ali-wahba-687a28199/) (Hebrew)
- [Yasmin Ehab](https://www.linkedin.com/in/yasmin-ehab-571694266/) (Russian)
- [Nada Kamel Mostafa](https://www.linkedin.com/in/nada-kamel-093485288/) (Japanese)
- [Aisha Khattab](https://www.linkedin.com/in/aisha-khattab/) (Portuguese)
- [Gabriela Gassan Masri](https://www.linkedin.com/in/gabriela-masri-multilinguist-tutor-translator/) (Portuguese, Spanish)
- [Simona Di Giovanni](https://www.linkedin.com/in/simona-di-giovanni-799a3521b/) (Spanish)
- [Shahd Muhammed](#) (Russian)
- [Esmira Ismailova](https://linkedin.com/in/esmiraismailova) (Russian)
- [Farida Mohammed](https://linkedin.com/in/farida-mohammed-菲丽）-088181320) (Chinese)
- [Roqia Ahmed](https://www.linkedin.com/in/roqia-ahmed-4b282b264/) (Hebrew)
- [Ahmed Elghetany](https://www.linkedin.com/in/ahmed-elghetany-51285b158/) (Russian)
- [Salma Mohamed](https://www.linkedin.com/in/salma-mohamed-夏苗苗/) (Chinese)
