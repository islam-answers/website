---
extends: _layouts.answer
section: content
title: Who is the Prophet Muhammad?
date: 2024-02-11
description: The Prophet Muhammad (peace be upon him) was a perfect example of an
  honest, just, merciful, compassionate, truthful, and brave human being.
sources:
- href: https://www.islam-guide.com/ch3-8.htm
  title: islam-guide.com
---

Muhammad {{ $page->pbuh() }} was born in Makkah in the year 570. Since his father died before his birth and his mother died shortly thereafter, he was raised by his uncle who was from the respected tribe of Quraysh. He was raised illiterate, unable to read or write, and remained so till his death. His people, before his mission as a prophet, were ignorant of science and most of them were illiterate. As he grew up, he became known to be truthful, honest, trustworthy, generous, and sincere. He was so trustworthy that they called him the Trustworthy. Muhammad {{ $page->pbuh() }} was very religious, and he had long detested the decadence and idolatry of his society.

At the age of forty, Muhammad {{ $page->pbuh() }} received his first revelation from God through the Angel Gabriel. The revelations continued for twenty-three years, and they are collectively known as the Quran.

As soon as he began to recite the Quran and to preach the truth which God had revealed to him, he and his small group of followers suffered persecution from unbelievers. The persecution grew so fierce that in the year 622 God gave them the command to emigrate. This emigration from Makkah to the city of Madinah, some 260 miles to the north, marks the beginning of the Muslim calendar.

After several years, Prophet Muhammad {{ $page->pbuh() }} and his followers were able to return to Makkah, where they forgave their enemies. Before Prophet Muhammad {{ $page->pbuh() }} died, at the age of sixty-three, the greater part of the Arabian Peninsula had become Muslim, and within a century of his death, Islam had spread to Spain in the West and as far East as China. Among the reasons for the rapid and peaceful spread of Islam was the truth and clarity of its doctrine. Islam calls for faith in only one God, Who is the only one worthy of worship.

Prophet Muhammad {{ $page->pbuh() }} was a perfect example of an honest, just, merciful, compassionate, truthful, and brave human being. Though he was a man, he was far removed from all evil characteristics and strove solely for the sake of God and His reward in the Hereafter. Moreover, in all his actions and dealings, he was ever mindful and fearful of God.
