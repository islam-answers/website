---
extends: _layouts.answer
section: content
title: 什么是清真食品？
date: 2024-11-02
description: 清真食品是真主允许穆斯林食用的食品，但猪肉和酒类除外。
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 603054
---

清真食品或合法食品是真主允许穆斯林食用的食品。一般来说，大多数食品和饮料都被视为清真食品，但猪肉和酒精是最重要的例外。肉类和家禽必须以人道和正确的方式屠宰，包括在屠宰前提到真主的名字并尽量减少动物的痛苦。
