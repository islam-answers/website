---
extends: _layouts.answer
section: content
title: ¿Fue Jesús crucificado?
date: 2024-10-06
description: Según el Corán, Jesús no fue asesinado ni crucificado. Jesús fue elevado
  vivo a los cielos por Allah, y un otro hombre fue crucificado en su lugar.
sources:
- href: https://islamqa.org/hanafi/mathabah/133870/
  title: islamqa.org (Mathabah.org)
- href: https://www.islamweb.net/en/fatwa/85366/
  title: islamweb.net
- href: https://islamqa.info/en/answers/110592
  title: islamqa.info
translator_id: 597714
---

Cuando los judíos y el emperador romano conspiraron para matar a Jesús {{ $page->pbuh() }}, Allah hizo que una de las personas presentes se pareciera en todo a Jesús. Así que mataron al hombre que se parecía a Jesús. No mataron a Jesús. Jesús {{ $page->pbuh() }} fue resucitado vivo a los Cielos. La prueba de ello son las palabras de Allah sobre la invención de los judíos y su refutación:

{{ $page->verse('4:157-158') }}

Así, Allah declaró falsas las palabras de los judíos cuando afirmaban que lo habían matado y crucificado, y afirmó que lo había llevado hasta Él. Eso fue misericordia de Allah para Jesús (Isa) {{ $page->pbuh() }}, y fue un honor para él, para que fuera uno de Sus signos, un honor que Alá concede a quien Él quiere de Sus mensajeros.

La implicación de las palabras "Pero Allah lo resucitó (con su cuerpo y alma) para Sí" es que Allah, glorificado y exaltado sea, resucitó a Jesús en cuerpo y alma, para refutar la afirmación de los judíos de que lo crucificaron y mataron, porque matar y crucificar tienen que ver con el cuerpo físico. Pero, si Él sólo hubiera tomado su alma, eso no descartaría la afirmación de haberlo matado y crucificado, porque tomar sólo el alma no sería una refutación de su afirmación. También el nombre de Jesús {{ $page->pbuh() }}, en realidad, se refiere a él tanto en alma como en cuerpo juntos; no puede referirse a uno de ellos solamente, a menos que haya una indicación en ese sentido. Además, tomar su alma y su cuerpo juntos es indicativo del perfecto poder y sabiduría de Allah, y de Su honor y apoyo a quien Él quiera de entre Sus Mensajeros, según lo que Él, exaltado sea, dice al final del versículo: "Y Allah es Poderoso y Sabio."
