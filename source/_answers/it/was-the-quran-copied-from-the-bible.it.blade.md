---
extends: _layouts.answer
section: content
title: Il Corano è stato copiato dalla Bibbia?
date: 2024-10-25
description: Il Corano non è stato copiato dalla Bibbia. Il profeta Muhammad era analfabeta,
  non esisteva una Bibbia in arabo e le rivelazioni erano dirette da Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 573107
---

Allah ha creato tutto ciò che ci circonda con uno scopo. Anche l'uomo è stato creato con uno scopo, che è quello di adorare Allah. Allah dice nel Corano:

{{ $page->verse('51:56') }}

Di volta in volta, Allah ha inviato i profeti come guide ed esempi per l'umanità. Questi profeti hanno ricevuto la rivelazione da Allah e ogni religione possiede la propria raccolta di scritture divine, che costituiscono il fondamento della loro fede. Queste scritture sono la trascrizione materiale della rivelazione divina: diretta, come nel caso del profeta Mosè (Musa) che ha ricevuto i comandamenti da Allah, o indiretta, come nel caso di Gesù (Isa) e Muhammad {{ $page->pbuh() }} che hanno trasmesso al popolo la rivelazione comunicata loro dall'angelo Gabriele (Jibril).

Il Corano ordina a tutti i musulmani di credere nelle Scritture che lo hanno preceduto. Tuttavia, come musulmani crediamo nella Torah (Tawrah), nel Libro dei Salmi (Zaboor), nel Vangelo (Injil) nella loro forma originale, ovvero nel modo in cui sono stati rivelati, come parola di Allah.

## Il profeta Muhammad potrebbe aver copiato il Corano dalla Bibbia?

La diatriba sul fatto che il Profeta Muhammad {{ $page->pbuh() }} avrebbe potuto imparare le storie delle Genti del Libro, per poi trasmetterle attraverso il Corano - che non sarebbe quindi una rivelazione divina - è dovuta ad alcune osservazioni secondo le quali egli {{ $page->pbuh() }} avrebbe avuto la possibilità di conoscere le storie direttamente dai loro libri sacri, o apprendere le storie, i comandi e i divieti verbalmente, direttamente dai racconti delle Genti del Libro, nel caso non gli fosse stato possibile studiare i loro libri da solo.

Queste osservazioni possono essere riassunte in tre affermazioni fondamentali:

1. l'affermazione secondo cui il profeta Muhammad {{ $page->pbuh() }} non fosse illetterato o analfabeta;
2. l'affermazione che in quell'epoca le scritture cristiane fossero già a disposizione del profeta {{ $page->pbuh() }} e che quindi egli potesse citarle o copiarle;
3. l'affermazione che Mecca fosse un importante centro educativo per gli studi sulle sacre Scritture.

In primo luogo, i testi del Corano e della Sunnah in molti punti indicano chiaramente, senza lasciare alcun dubbio, che il Profeta Muhammad {{ $page->pbuh() }} era davvero illetterato e analfabeta (non aveva mai scritto nulla di suo pugno), e che non aveva mai narrato nessuna delle storie delle Genti del Libro prima che gli giungesse la rivelazione. Non sapeva nulla delle storie prima che la sua missione profetica avesse inizio, e non ne aveva mai parlato prima di allora.

Allah, l'Altissimo, dice:

{{ $page->verse('29:48') }}

I libri sulla biografia del Profeta e sulla storia dell'Islam non fanno menzione del fatto che egli abbia scritto la rivelazione, o che scrivesse lui stesso, di suo pugno, le lettere indirizzate ai re. Si parla piuttosto del contrario: il Profeta Muhammad {{ $page->pbuh() }} aveva degli scribi che scrivevano la rivelazione ed altre cose al posto suo. In un prezioso testo di Ibn Khaldun viene descritto il livello di istruzione diffuso generalmente nella Penisola Arabica poco prima che iniziasse la missione del Profeta. Dice:

> "Tra gli arabi, l'abilità di scrivere era più rara delle uova di cammella. La maggior parte della gente era analfabeta, specialmente gli abitanti del deserto, perché questa è un'abilità che di solito si trova tra gli abitanti delle città".

Per questo motivo gli arabi non definivano la persona analfabeta come tale; piuttosto descrivevano chi sapeva leggere e scrivere come una persona istruita, perché saper leggere e scrivere era l'eccezione, non la norma tra le persone.

In secondo luogo, non vi è alcuna prova, indicazione o suggerimento che a quel tempo esistesse già una traduzione araba della Bibbia. Al-Bukhari ha narrato da Abu Hurayrah (che Allah sia soddisfatto di lui) che egli disse:

{{ $page->hadith('bukhari:7362') }}

Questo hadith indica che gli ebrei avevano il monopolio totale del testo e della sua spiegazione. Se i loro testi fossero stati conosciuti in arabo, non ci sarebbe stato bisogno per gli ebrei di leggerli o di spiegarli [ai musulmani].

Questa ipotesi è supportata dal versetto in cui Allah, l'Altissimo, dice:

{{ $page->verse('3:78') }}

Forse il libro più importante scritto su questo argomento è quello di Bruce Metzger, professore di lingua e letteratura del Nuovo Testamento, La Bibbia in Traduzione, nel quale afferma:

> "È molto probabile che la più antica traduzione araba della Bibbia risalga all'VIII secolo".

L'orientalista Thomas Patrick Hughes ha scritto:

> "Non esiste alcuna prova che Muhammad avesse letto le Scritture cristiane... Va notato che non ci sono prove evidenti che suggeriscano che esistesse una traduzione araba dell'Antico e del Nuovo Testamento prima dell'epoca di Muhammad".

In terzo luogo, l'idea che il Profeta abbia appreso la rivelazione da altre persone è una vecchia idea. I politeisti lo accusarono di ciò, e il Corano li smentì, come dice Allah, l'Altissimo:

{{ $page->verse('25:5-6') }}

Inoltre, l'affermazione secondo cui il profeta Muhammad {{ $page->pbuh() }} abbia acquisito la conoscenza dalle Genti del Libro quando si trovava a Mecca è un'affermazione smentita dallo status accademico e culturale di Mecca e dalla reale natura del legame che questa società araba aveva con la conoscenza delle Genti del Libro.

Pertanto, dopo aver dimostrato che il Profeta Muhammad {{ $page->pbuh() }} non ha appreso la conoscenza delle Genti del Libro direttamente da loro o tramite qualche intermediario, non ci resta che affermare che si tratta di una rivelazione di Allah al Suo Profeta prescelto.
