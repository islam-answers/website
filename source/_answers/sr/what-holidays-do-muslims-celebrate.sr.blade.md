---
extends: _layouts.answer
section: content
title: Koje praznike slave muslimani?
date: 2024-08-12
description: Muslimani slave samo dva Bajrama (praznika), Ramazanski bajram (na kraju
  meseca Ramazana) i Kurban bajram, na kraju hadža (godišnjeg hodočašća).
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 584497
---

Muslimani slave samo dva Bajrama (praznika): Ramazanski bajram (na kraju meseca Ramazana) i Kurban bajram, koji označava završetak hadža (godišnjeg perioda hodočašća) na 10. dan meseca Zul-Hidždža.

Tokom ova dva praznika, muslimani čestitaju jedni drugima, šire radost u svojim zajednicama i slave sa svojim porodicama. Ali što je još važnije, oni se sjećaju Allahovog blagoslova na njih, slave Njegovo ime i klanjaju bajram namaz u džamiji. Osim ove dve prilike, muslimani ne priznaju niti slave bilo koji drugi dan u godini.

Naravno, postoje i druge radosne prilike za koje islam diktira odgovarajuće slavlje, kao što su svadbene proslave (valima) ili povodom rođenja deteta (aqeeqah). Međutim, ovi dani nisu navedeni kao određeni dani u godini; umesto toga, oni se slave onako kako se dešavaju tokom života muslimana.

Na jutro i Ramazanskog i Kurban bajrama, muslimani prisustvuju bajram-namazu u zajednici u džamiji, nakon čega slijedi propoved (hutbah) koja podsjeća muslimane na njihove dužnosti i odgovornosti. Nakon molitve, muslimani se pozdravljaju frazom "Bajram Mubarak Olsun" ili "Eid Mubarak" (Sretan Bajram) i razmenjuju poklone i slatkiše.

## Ramazanski bajram

Ramazanski bajram označava kraj Ramazana, koji je tokom devetog meseca lunarnog islamskog kalendara.

Ramazanski bajram je važan jer sledi jedan od najsvetijih meseci od svih: Ramazan. Ramazan je vreme za muslimane da ojačaju svoju vezu sa Allahom, recituju Kur'an i povećaju dobra dela. Na kraju Ramazana, Allah daje muslimanima dan Ramazanskog bajrama kao nagradu za uspješno okončanje posta i za povećanje bogosluženja tokom mjeseca Ramazana. Na Ramazanski bajram, muslimani zahvaljuju Allahu na prilici da prisustvuju još jednom Ramazanu, da mu se približe, da postanu bolji ljudi i da imaju još jednu šansu da budu spašeni od džehennemskog (paklenog) ognja.

{{ $page->hadith('ibnmajah:3925') }}

## Kurban Bajram

Kurban Bajram označava završetak godišnjeg perioda hadža (hodočašća u Meku), koji se održava u mjesecu Dhul-Hijjah, dvanaestom i posljednjem mjesecu lunarnog islamskog kalendara.

Proslava Kurban bajrama je u znak sećanja na odanosti proroka Ibrahima (Abrahama) Allahu i njegovu spremnost da žrtvuje svog sina, Ismaila. Na samom mestu žrtvovanja, Allah je zamenio Ismaila ovnom, koji je trebalo da bude zaklan umesto njegovog sina. Ova Allahova zapovijed bila je test spremnosti i posvećenosti proroka Ibrahima da se pokorava zapovesti Gospodara svoga, bez pitanja. Stoga, Kurban bajrma označava praznik žrtvovanja.

Jedno od najboljih djela za muslimane na dan Kurban bajrama je čin žrtvovanja, koji se sprovodi nakon Bajram-namaza. Muslimani žrtvuju životinju da bi se setili žrtve proroka Ibrahama za Allaha. Žrtvena životinja mora biti ovca, jagnje, koza, krava, bik ili kamila. Životinja mora biti dobrog zdravlja i starija od određene starosti da bi bila zaklana, na halal, islamski način. Meso žrtvovane životinje deli se između osobe koja žrtvuje, njihovih prijatelja i porodice i siromašnih i siromašnih.

Žrtvovanje životinje na Kurban bajram odražava spremnost proroka Ibrahama da žrtvuje svog sina i to je takođe čin obožavanja koji se nalazi u Kur'anu i potvrđenoj tradiciji proroka Muhameda {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

U Kur'anu, Bog je rekao:

{{ $page->verse('2:196..') }}
