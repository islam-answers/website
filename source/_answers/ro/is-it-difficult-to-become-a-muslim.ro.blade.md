---
extends: _layouts.answer
section: content
title: Este dificil să devii musulman?
date: 2024-11-25
description: A deveni musulman este foarte ușor și oricine poate face asta. Acceptarea
  islamului implică rostirea mărturiei de credință.
sources:
- href: https://islamqa.info/en/answers/11819/
  title: islamqa.info
- href: https://islamqa.info/en/answers/6703/
  title: islamqa.info
translator_id: 554943
---

Printre virtuțile islamului se numără faptul că în această religie nu există intermediari în relația dintre o persoană și Domnul său. Acceptarea acestei religii nu implică nicio ceremonie sau procedură specială care trebuie făcută în fața unei alte persoane și nici nu necesită consimțământul anumitor persoane.

A deveni musulman este foarte ușor și oricine o poate face, chiar dacă este singur în deșert sau într-o cameră încuiată. Tot ce trebuie să faci este să spui două fraze frumoase care rezumă sensul islamului: "Ash-hadu an la ilaha illa Allah, wa ash-hadu anna Muhammadan Rasulu-Allah". Aceasta înseamnă: "Mărturisesc că nu există altă divinitate (nimeni demn de adorare) în afară de Allah și mărturisesc că Muhammad este Mesagerul lui Allah".

Oricine rostește aceste două declarații de credință, cu convingere și crezând în ele, devine musulman, împărtășind toate drepturile și îndatoririle pe care le au ceilalți musulmani.

Unii oameni au o noțiune greșită că acceptarea islamului necesită un anunț în prezența unor savanți sau șeici de rang înalt sau raportarea acestui act la curțile de justiție sau la alte autorități. De asemenea, se crede în mod greșit că acceptarea islamului ar necesita un certificat eliberat de autorități.

Dorim să clarificăm faptul că întreaga chestiune este foarte ușoară și că niciuna dintre aceste condiții nu este necesară. Allah Atotputernicul este mai presus de orice înțelegere și cunoaște bine secretele tuturor inimilor. Cu toate acestea, cei care urmează să adopte islamul ca religie sunt sfătuiți să se înregistreze ca musulmani la agenția guvernamentală competentă, deoarece această procedură le poate facilita multe aspecte, inclusiv posibilitatea de a efectua Hajj (pelerinajul) și 'Umrah.

Cu toate acestea, nu este suficient să rostești această mărturie verbal (fie în privat, fie în public); ci trebuie să crezi în ea în inima ta, cu o convingere fermă și o credință de nezdruncinat. Dacă cineva este cu adevărat sincer și respectă învățăturile islamului în toată viața sa, se va considera o persoană nou-născută.
