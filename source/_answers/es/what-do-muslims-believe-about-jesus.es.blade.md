---
extends: _layouts.answer
section: content
title: ¿Qué es lo que los musulmanes creen sobre Jesús?
date: 2024-06-02
description: Los musulmanes respetan y reverencian a Jesús. Lo consideran uno de los
  más grandes mensajeros de Dios para la humanidad.
sources:
- href: https://www.islam-guide.com/es/ch3-10.htm
  title: islam-guide.com
---

Los musulmanes respetan y reverencian a Jesús (La Paz se a con él). Lo consideran uno de los más grandes mensajeros de Dios para la humanidad. El Corán confirma su nacimiento virginal. Dios purificó a su madre María. Existe un capítulo entero en el Corán llamado "Maryam" (María). El Corán describe el nacimiento de Jesús como sigue:

{{ $page->verse('3:45-47') }}

Jesús nació milagrosamente [sin padre] por orden de Dios quien creó a Adán sin padre ni madre. Dios dijo:

{{ $page->verse('3:59') }}

Durante su misión profética, Jesús hizo varios milagros. Dios nos dice que Jesús dijo:

{{ $page->verse('3:49..') }}

Los musulmanes creen que Jesús no fue crucificado (y mucho menos que murió en la cruz). Era el plan de los enemigos de Jesús el crucificarlo (y matarlo), pero Dios lo salvó y lo elevó hacia Sí. La apariencia de Jesús fue colocada sobre otra persona, y los enemigos de Jesús aprendieron a este hombre y lo crucificaron, pensando que era Jesús. Dios dijo:

{{ $page->verse('..4:157..') }}

Tanto Muhammad {{ $page->pbuh() }} como Jesús no vinieron a cambiar la doctrina básica de la creencia en un solo Dios, traída por lo profetas anteriores, sino a confirmarla y a renovarla.
