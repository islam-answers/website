---
extends: _layouts.answer
section: content
title: De ce bărbații musulmani au barbă?
date: 2024-12-31
description: Bărbații musulmani își lasă barbă pentru a urma exemplul profetului Muhammad,
  pentru a-și menține firea înnăscută și pentru a se distinge de necredincioși.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
translator_id: 554943
---

Ultimul și cel mai bun dintre Mesageri, Muhammad {{ $page->pbuh() }}, și-a lăsat barba să crească, la fel ca și califii care au venit după el, precum și tovarășii săi și liderii și oamenii de rând ai musulmanilor. Acesta este modul de viață al profeților și mesagerilor și al urmașilor lor și face parte din fitrah (predispoziția inițială, naturală) cu care Allah i-a creat pe oameni. Aisha (Allah să fie mulțumit de ea) a relatat că Trimisul lui Allah {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:261a') }}

Ceea ce se cere credinciosului, dacă Allah și Trimisul Său au poruncit ceva, este să spună: Noi auzim și ne supunem, după cum spune Allah:

{{ $page->verse('24:51') }}

Bărbații musulmani își lasă barba pentru a se supune lui Allah și Trimisului Său. Profetul {{ $page->pbuh() }} le-a ordonat musulmanilor să își lase barba să crească liber și să își taie mustața. A fost relatat de Ibn 'Umar că Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn 'Abd al-Barr a spus: "Este interzis rasul bărbii și nimeni nu face acest lucru, cu excepția bărbaților care sunt efeminați", adică a celor care imită femeile. Jabir relatează următoarele despre Profetul Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2344b') }}

Shaykh al-Islam Ibn Taymiyah (Allah să aibă milă de el) a spus:

> Coranul, Sunnah (tradiția profetului) și ijma' (consensul savant) indică faptul că trebuie să ne deosebim de necredincioși în toate aspectele și să nu îi imităm, deoarece imitarea lor în exterior ne va face să îi imităm în faptele și obiceiurile lor rele și chiar în credințe. (...)
