---
extends: _layouts.answer
section: content
title: De ce femeile musulmane poartă hijab?
date: 2024-03-25
description: Hijabul este o poruncă de la Allah care împuternicește o femeie prin
  accentuarea frumuseții sale spirituale interioare, nu a aparenței sale superficiale.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/my-hijab-reflections-by-muslim-women
  title: islamicpamphlets.com
translator_id: 554943
---

Cuvântul Hijab provine din rădăcina arabă „hajaba”, care înseamnă a ascunde sau a acoperi. Într-un context islamic, hijab se referă la codul vestimentar al femeilor musulmane care au ajuns la pubertate. Hijabul presupune acoperirea întregului corp, cu excepția feței și a mâinilor. Unele femei aleg să-și acopere fața și mâinile, iar acest lucru este denumit Burqa sau Niqab.

Pentru a îndeplini condițiile hijabului, femeilor musulmane li se cere să-și acopere cu modestie corpul cu haine care nu le dezvăluie silueta în fața bărbaților cu care nu sunt înrudite. Cu toate acestea, hijabul nu se referă doar la aparența exterioare; ci este, de asemenea, și despre un mod nobil de a vorbi, modestie și o conduită demnă.

{{ $page->verse('33:59') }}

Deși există multe beneficii ale hijabului, principalul motiv pentru care femeile musulmane poartă hijab este pentru că este o poruncă de la Dumnezeu (Allah), iar El știe ce este mai bine pentru creația Sa.

Hijabul oferă femeii libertatea de a-şi exprima frumusețea ei spirituală interioară, mai degrabă decât aspectul ei fizic, superficial. Le oferă femeilor libertatea de a fi membri activi ai societății, păstrându-și în același timp modestia.

Hijabul nu simbolizează suprimarea, oprimarea sau tăcerea. Mai degrabă, este o protecție împotriva observațiilor degradante, a avansurilor nedorite și a discriminării inechitabile. Prin hijab, femeia îşi acoperă aspectul fizic, nu mintea sau intelectul.
