---
extends: _layouts.answer
section: content
title: Che cos'è la Sunnah?
date: 2024-12-28
description: La Sunnah, di ispirazione divina, integra il Corano chiarendo e approfondendo
  le norme necessarie per la pratica religiosa islamica.
sources:
- href: https://islamqa.info/en/answers/77243/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145520/
  title: islamqa.info
translator_id: 573107
---

La Sunnah - ovvero le parole e gli atti attribuiti al Profeta Muhammad {{ $page->pbuh() }} e le azioni da lui approvate - è una delle due parti della rivelazione divina (Wahy) che furono rivelate al Messaggero di Allah {{ $page->pbuh() }}. L'altra parte della rivelazione è il Sacro Corano. Allah dice:

{{ $page->verse('53:3-4') }}

È stato narrato che il Messaggero di Allah {{ $page->pbuh() }} disse:

{{ $page->hadith('ibnmajah:12') }}

Hassaan ibn 'Atiyah ha detto:

> Jibril (l'angelo Gabriele) era solito trasmettere la sunnah al Profeta {{ $page->pbuh() }} così come era solito trasmettergli il Corano.

L'importanza della Sunnah è innanzitutto dovuta al fatto che spiega il Corano ed è un commento ad esso; inoltre aggiunge alcune regole a quelle già riportate nel Corano. Allah dice:

{{ $page->verse('..16:44') }}

Ibn 'Abd al-Barr disse:

> Il commento del Profeta {{ $page->pbuh() }} al Corano è di due tipi: 
>
> 1. Spiega cose che vengono menzionate in termini generali nel Sacro Corano, come le cinque preghiere quotidiane, i loro tempi, la prostrazione, l'inchino e tutte le altre. 
> 1. Aggiunge regole a quelle già riportate nel Corano, come il divieto di sposare contemporaneamente una donna e la sua zia paterna o materna.

Il profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:3461') }}
