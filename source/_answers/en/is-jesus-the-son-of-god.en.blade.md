---
extends: _layouts.answer
section: content
title: Is Jesus the son of God?
date: 2024-12-22
description: Jesus is considered a great prophet and messenger of God, but not God
  or the son of God; rather, he is a servant and messenger among humans.
sources:
- href: https://islamqa.info/en/answers/26301/
  title: islamqa.info
- href: https://islamqa.info/en/answers/82361/
  title: islamqa.info
---

Muslims regard Jesus as a revered prophet and messenger of God, born to the Virgin Mary through a miraculous birth. However, the core belief that distinguishes Islamic understanding from Christian doctrine is the rejection of Jesus as divine or the literal son of God.

### The oneness of God

Central to Islamic belief is Tawhid (the oneness of God), emphasizing that there is no deity besides God, and He neither begets nor is begotten. Consequently, attributing divinity to Jesus or assigning Him the title of God's son contradicts the fundamental principle of monotheism in Islam.

One of the most important principles of belief in Allah is to declare that Allah is above all attributes that imply shortcomings. One of the attributes that imply shortcomings that the Muslim must reject is the notion that Allah has a son, because that implies need and that there is a being who is similar to Him, and these are matters which Allah is far above. Allah says in the Quran:

{{ $page->verse('112:1-4') }}

The Quran explicitly addresses the misconception of Jesus' divinity, emphasizing his human status and his servitude to God. Allah says:

{{ $page->verse('5:75') }}

The fact that they both ate food means that they were in need of food for nourishment. According to scholars, this implies that Jesus and his mother had to relieve themselves after the food had been digested. Almighty Allah is far above depending on food or having to go to the restroom. Allah also says:

{{ $page->verse('4:171') }}

### Examining the validity of Christian scriptures

Muslims believe that the Gospels that are in people’s hands today, in which the Christians believe, have been tampered with and changed, and are still being tampered with from time to time, so that there is nothing left in the form in which the Gospel was originally revealed from Allah.

The Gospel which speaks the most about the belief in the trinity and the divinity of Jesus {{ $page->pbuh() }}, and which has become a reference point for the Christians in their arguments in support of this falsehood, is the Gospel of John. This Gospel is subject to doubts about its authorship even among some Christian scholars themselves - which is not the case with the other Gospels in which they believe.

Encyclopaedia Britannica mentions:

> As for the gospel of John, it is undoubtedly fabricated. Its author wanted to pitch two of the disciples against one another, namely St. John and St. Matthew.
> This writer who appears in the text claimed that he was the disciple who was loved by the Messiah, and the Church took this at face value and affirmed that the writer was the disciple John, and it put his name on the book, even though the author was not John for certain.

### What does “son of God” mean in the Bible?

The Bible in which it says that Jesus is the son of God is the same Bible in which the lineage of the Messiah ends with Adam {{ $page->pbuh() }}, and he too is described as a son of God:

> Now Jesus himself was about thirty years old when he began his ministry. He was the son, so it was thought, of Joseph, the son of Heli … the son of Seth, the son of Adam, the son of God. (Luke 3:23-38)

This is the same Bible that describes Israel (Jacob) in the same terms:

> Then say to Pharaoh, ‘This is what the LORD says: Israel is my firstborn son.' (Exodus 4:22)

The same is said of Solomon {{ $page->pbuh() }}:

> He said to me: 'Solomon your son is the one who will build my house and my courts, for I have chosen him to be my son, and I will be his father. (1 Chronicles 28:6)

Were Adam, Israel and Solomon all other sons of God, before Jesus {{ $page->pbuh() }}? Exalted be Allah far above what they say! Indeed, in the Gospel of John itself there is an explanation of what is meant by this being a son; it includes all the righteous servants of God, so there is nothing unique about Jesus or any other prophet in this regard:

> But as many as received him, to them gave he power to become the sons of God, even to them that believe on his name. (John 1:12)

Something similar appears in the Gospel of Matthew:

> Blessed are the peacemakers, for they will be called sons of God. (Matthew 5:8-9)

This usage of the word “son” in the language of the Bible is a metaphor for the righteous servant of God, without it implying anything special or unique about the way in which he is created, or describing him literally as the offspring of God. Hence John says:

> How great is the love the Father has lavished on us, that we should be called children of God! (1 John 3:1)

There remains the issue of Jesus {{ $page->pbuh() }} being described as a son of God, and what they fabricated about the Lord of the Worlds, saying that He was the father of Jesus, the Messiah {{ $page->pbuh() }}. This too is not unique in the language of the Gospels:

> Jesus said, Do not hold on to me, for I have not yet returned to the Father. Go instead to my brothers and tell them, 'I am returning to my Father and your Father, to my God and your God. (John 20:17)

In this verse, Jesus says that God is a father to them too, and that God is the God of them all. The Quran emphasises God’s absolute oneness and perfection, rejecting any association of partners or offspring with Him. The term “son of God” in Biblical language is metaphorical, signifying righteousness, not divinity. Therefore, Muslims uphold Jesus’ noble status while affirming the oneness of God as the core of their faith.
