FROM php:8.3-alpine3.21

LABEL maintainer="Muhannad Ajjan"

RUN \
    apk update && \
    apk add libzip-dev libxml2-dev yaml-dev git openssh && \
    apk add php83-xmlwriter php83-fileinfo php83-pecl-yaml && \
    apk add --no-cache --virtual .build-deps $PHPIZE_DEPS && \
    pecl install yaml && \
    docker-php-ext-install zip xml fileinfo && \
    docker-php-ext-enable yaml && \
    apk add nodejs yarn brotli && \
    rm -rf /var/cache/apk/* && \
    apk del --purge .build-deps

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

RUN yarn global add firebase-tools

WORKDIR /app
