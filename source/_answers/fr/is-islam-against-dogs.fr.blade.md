---
extends: _layouts.answer
section: content
title: L'islam est-il contre les chiens ?
date: 2024-10-18
description: L'islam enseigne à traiter les animaux avec dignité. Les chiens ne peuvent pas être gardés comme animaux de compagnie, sauf à des fins spécifiques.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 573111
---

Les animaux font partie de la création d'Allah, et nous ne les voyons pas d'un mauvais œil. Au contraire, en tant que musulmans, nous avons le devoir de les traiter avec dignité, humanité et miséricorde. Le Messager d'Allah, {{ $page->pbuh() }}, a dit :

{{ $page->hadith('bukhari:3321') }}

## Tuer les chiens

Abdallah ibn Umar a dit :

{{ $page->hadith('muslim:1570b') }}

Il est important de préciser le type de chiens mentionné dans ce récit ainsi que le contexte général. Les chiens en question ici sont des meutes de chiens sauvages qui errent dans les villes, constituant une nuisance pour la communauté et un risque sanitaire. Ils sont porteurs et propagateurs de maladies, tout comme les rats, et causent de nombreux désagréments à la population. L'abattage d'animaux dangereux ou porteurs de maladies est une pratique courante et acceptée dans le monde entier.

Sur la base de l'ensemble des preuves, les juristes islamiques ont précisé que l'ordre de tuer les chiens n'est permis que lorsqu'il s'agit d'un prédateur ou d'un chien nuisible. En revanche, un chien inoffensif, quelle que soit sa couleur, ne peut être tué.

## Pureté

La salive des chiens est considérée comme impure par la majorité des érudits musulmans et leur fourrure l'est également selon certains avis. L'un des principes fondamentaux de l'islam est la pureté à tous les niveaux, et cela fut pris en compte, tout comme le fait que les anges n'entrent pas dans les endroits où se trouvent des chiens. Cette interdiction s'explique par les paroles du Prophète, {{ $page->pbuh() }} :

{{ $page->hadith('bukhari:3322') }}

## Garder un chien comme animal de compagnie

L'islam interdit aux musulmans de garder des chiens comme animaux de compagnie. Le Messager d'Allah, {{ $page->pbuh() }}, a dit :

{{ $page->hadith('muslim:1574g') }}
