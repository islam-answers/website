---
extends: _layouts.answer
section: content
title: Zašto muslimani poste?
date: 2025-01-26
description: Primarni razlog zašto muslimani poste tokom meseca Ramazana je postizanje
  taqwe (svesti o Bogu).
sources:
- href: https://seekersguidance.org/articles/https-www-seekersguidance-org-courses-why-muslims-fast-the-higher-aims-of-fasting-explained/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/fast/why-do-we-fast/
  title: seekersguidance.org
- href: https://seekersguidance.org/articles/quran-articles/spiritual-purpose-fasting-closeness-allah/
  title: seekersguidance.org
translator_id: 584497
---

Post je jedan od najvećih ibadeta koji muslimani čine svake godine tokom meseca Ramazana. To je zaista iskren čin ibadeta za koji će Uzvišeni Allah nagraditi –

{{ $page->hadith('bukhari:5927') }}

U suri El-Baqara, Uzvišeni Allah kaže:

{{ $page->verse('2:185') }}

Osnovni razlog zašto muslimani poste tokom meseca Ramazana jasno je iz ajeta:

{{ $page->verse('2:183') }}

Osim toga, vrline posta su brojne, kao što je činjenica da:

1. To je iskupljenje za nečije grehe i nepravde.
1. To je sredstvo za suzbijanje nedozvoljenih želja.
1. Olakšava pokazivanje odanosti.

Post predstavlja mnoge izazove za ljude, od gladi i žeđi do poremećenih obrazaca spavanja i još mnogo toga. Svaki izazov je dio borbe koja nam postala obavezna kako bismo kroz njih mogli učiti, razvijati se i rasti.
Ove poteškoće ne prolaze nezapaženo od strane Allaha, a nama je rečeno da ih budemo aktivno svjesni kako bismo mogli očekivati​velikodušnu nagradu za njih od Njega. Ovo se razumije iz riječi poslanika Muhammeda {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:37') }}
