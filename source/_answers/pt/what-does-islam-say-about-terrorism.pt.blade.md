---
extends: _layouts.answer
section: content
title: O Que o Islã Diz sobre Terrorismo?
date: 2024-06-02
description: |
  O Islã, a religião da misericórdia, não permite terrorismo.
sources:
- href: https://www.islam-guide.com/pt/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

O Islã, a religião da misericórdia, não permite terrorismo. No Alcorão, Deus disse:

{{ $page->verse('60:8') }}

O Profeta {{ $page->pbuh() }} costumava proibir os soldados de matar mulheres e crianças, e ele os avisava:

{{ $page->hadith('tirmidhi:1408') }}

E ele também disse:

{{ $page->hadith('bukhari:3166') }}

O Profeta Muhammad {{ $page->pbuh() }} também proibiu a punição com o fogo.

Uma vez ele listou o assassinato como o segundo maior dos pecados, e advertiu que no Dia do Juízo,

{{ $page->hadith('bukhari:6533') }}

Os muçulmanos são encorajados a serem gentis com os animais e são proibidos de feri-los. Uma vez o Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('bukhari:3318') }}

Ele também disse que um homem deu de beber a um cão muito sedento, e Deus perdoou seus pecados por essa atitude. Perguntaram ao Profeta “Mensageiro de Deus, nós somos recompensados pelo bom tratamento dispensado aos animais?" Ele disse:

{{ $page->hadith('bukhari:2466') }}

Adicionalmente, quando tiram a vida de um animal para alimento, os muçulmanos são ordenados a fazê-lo de forma que cause o menor sofrimento e medo possíveis. O Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('tirmidhi:1409') }}

À luz destes e outros textos islâmicos, o ato de incitar o terror nos corações de civis indefesos, a destruição desenfreada de prédios e propriedades, o bombardeio e mutilação de homens, mulheres e crianças inocentes são todos atos proibidos e detestáveis de acordo com o Islã e os muçulmanos.

Os muçulmanos seguem uma religião de paz, misericórdia e perdão, e a vasta maioria não tem nada a ver com os eventos violentos que alguns associaram aos muçulmanos. Se um muçulmano cometer um ato de terrorismo, essa pessoa seria culpada de violar as leis do Islã.

No entanto, é importante distinguir entre terrorismo e resistência legítima à ocupação, uma vez que os dois são muito diferentes.
