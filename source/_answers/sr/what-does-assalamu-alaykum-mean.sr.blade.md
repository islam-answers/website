---
extends: _layouts.answer
section: content
title: Šta znači selamu alejkum?
date: 2025-01-26
description: Selamu alejkum znči “mir s vama” i pozdrav je koji se koristi među muslimanima.
  Dati selam znači bezazlenost, sigurnost i zaštitu od zla i grešaka.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
translator_id: 584497
---

Kada je islam došao, Allah je propisao da način pozdrava među muslimanima bude El-salamu alejkum i da se ovaj pozdrav koristi samo među muslimanima.

Značenje “selam” (doslovno znači “mir”) je bezopasnost, sigurnost i zaštita od zla i grešaka. Ime al-Selam je ime Allaha, dž.š., pa je značenje selama koji se traži među muslimanima: Neka se blagoslov Njegovog imena spusti na vas. Upotreba predloga 'ala u alejkumu (na tebe) ukazuje na to da je pozdrav inkluzivan.

Najpotpuniji oblik pozdrava je da se kaže “Es-Salamu `alejkum ve rahmatu-Allahi ve barakatuhu”, što znači “Neka je mir s vama i Allahova milost i Njegovi blagoslovi.

Poslanik {{ $page->pbuh() }} učinio je širenje selama delom vere.

{{ $page->hadith('bukhari:12') }}

Stoga je Poslanik {{ $page->pbuh() }} objasnio da davanje selama širi ljubav i bratstvo. Allahov Poslanik {{ $page->pbuh() }} je rekao:

{{ $page->hadith('muslim:54') }}

Poslanik {{ $page->pbuh() }} nam je naredio da uzvratimo selam i učinio to pravom i dužnošću. Rekao je:

{{ $page->hadith('muslim:2162a') }}

Jasno je da je obavezno uzvratiti selam, jer vam musliman time daje sigurnost, a vi njemu zauzvrat morate dati isto tu sigurnost. Kao da ti kaže, ja ti dajem spas i sigurnost, pa i ti njemu daj isto, da ne posumnja i ne pomisli da onaj kome je dao selam ga izdaje ili ignoriše.
