---
extends: _layouts.answer
section: content
title: Kinh Qur'an nói về điều gì?
date: 2024-04-03
description: Nguồn gốc chính của đức tin và hành động của mỗi người Islam.
sources:
- href: https://www.islam-guide.com/vi/ch3-7.htm
  title: islam-guide.com
---

Kinh Qur'an, là lời nói thiên khải cuối cùng của Thượng Đế, là nguồn gốc căn bản cho đức tin và hành động của mỗi người Islam. Kinh Qur'an giải quyết tất cả các vấn đề liên quan đến con người: như sự thông thái, học thuyết, việc thờ phụng, các giao dịch, luật pháp, ..., nhưng chủ đề cơ bản là mối quan hệ giữa Thượng Đế và những sinh linh của Ngài. Đồng thời, Kinh Qur'an còn đưa hướng dẫn và những răn dạy chi tiết dành cho một xã hội công bằng, những hành vi đúng mực của con người và một hệ thống kinh tế công bằng.

Lưu ý Kinh Qur'an được mặc khải cho Thiên Sứ Muhammad {{ $page->pbuh() }} chỉ duy nhất bằng tiếng Ả-rập. Do đó, bất cứ bản dịch nào của Kinh Qur'an, dù là tiếng Anh hay bất cứ thứ tiếng nào khác, cũng không phải là Kinh Qur'an, hay là một phiên bản của Kinh Qur'an, chính xác, đó chỉ là những bản dịch nghĩa của Kinh Qur'an. Kinh Qur'an chỉ tồn tại duy nhất bằng tiếng Ả-rập, thứ tiếng mà nó được mặc khải mà thôi.
