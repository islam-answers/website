---
extends: _layouts.answer
section: content
title: Hva sier islam om terrorisme?
date: 2024-06-19
description: Islam er en barmhjertig religion og tillater ikke terrorisme.
sources:
- href: https://www.islam-guide.com/no/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

Islam er en barmhjertig religion og tillater ikke terrorisme. I Koranen har Gud sagt:

{{ $page->verse('60:8') }}

Profeten Mohammad {{ $page->pbuh() }} forbød soldater å drepe kvinner og barn, og han gav dem følgende råd:

{{ $page->hadith('tirmidhi:1408') }}

Han sa også:

{{ $page->hadith('bukhari:3166') }}

Profeten Mohammad {{ $page->pbuh() }} har også forbudt avstraffelser med ild.

En gang klassifiserte han mord som den nest største av de store synder, og han advarte til og med om følgende på dommedag:

{{ $page->hadith('bukhari:6533') }}

Muslimer er til og med oppfordret til å være snille mot dyr, og det er forbudt å skade dem. En gang sa profeten Mohammad {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3318') }}

Han fortalte også om en mann som ga drikke til en veldig tørst hund, og Gud tilgav hans synder på grunn av denne gjerningen. Profeten {{ $page->pbuh() }} ble spurt: “Guds budbringer, blir vi belønnet for å være snille mot dyr?” Han sa:

{{ $page->hadith('bukhari:2466') }}

Når muslimer skal avlive et dyr for å spise det, så er de befalt å gjøre dette på en måte som medfører minst mulig frykt og lidelse for dyret. Profeten Mohammad {{ $page->pbuh() }} sa:

{{ $page->hadith('tirmidhi:1409') }}

I lys av disse og andre islamske tekster, så er følgende handlinger forbudte og ansett for å være avskyelige i følge islamsk lære og muslimer: spredning av terrorfrykt blant forsvarsløse sivile, store ødeleggelser av bygninger og eiendommer, samt bombing og lemlesting av uskyldige menn, kvinner og barn.

Muslimer følger en religion som er fredelig, barmhjertig og tilgivelig, og den store majoriteten har ingenting å gjøre med de voldelige hendelser som noen har forbundet med muslimer. Hvis en muslim utfører en terrorhandling, vil han gjøre seg skyldig i brudd på islamsk lov.

Det er imidlertid viktig å skille mellom terrorisme og legitim motstand mot okkupasjon, da de to er svært forskjellige.
