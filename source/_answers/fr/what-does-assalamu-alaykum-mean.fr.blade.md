---
extends: _layouts.answer
section: content
title: Que signifie Assalamu Alaykum ?
date: 2024-08-03
description: Assalamu Alaykum signifiant «que la paix soit avec vous» est une formule
  de salutation utilisée par les musulmans.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
translator_id: 586055
---

Lorsque l'islam est apparu, Allah a ordonné que la manière de saluer les musulmans soit «Al-salamu 'alaykum», et que cette salutation ne soit utilisée qu'entre musulmans.

Le sens de «Salam» (qui signifie littéralement "paix") est l'innocuité, la sécurité ou encore la protection contre le mal et les fautes. De plus, «Al-Salam» est un des 99 noms d'Allah (qu'Il soit exalté). Ainsi, la signification de la salutation «Salam» qui est obligatoire pour les musulmans, est : «Que la bénédiction de Son nom descende sur vous». L'utilisation de la préposition «'ala» dans «'alaykum (sur vous)» indique que la salutation est inclusive.

La forme de salutation la plus complète consiste à dire «As-Salamu `alaykoum wa rahmatu-Allahi wa barakatuhu", que l’on peut traduire par : «Que la paix soit sur vous et que la miséricorde d'Allah et ses bénédictions soient sur vous".

Le prophète {{ $page->pbuh() }} a fait de la diffusion du salam une partie de la foi.

{{ $page->hadith('bukhari:12') }}

C'est pourquoi le prophète {{ $page->pbuh() }} a expliqué que le fait de donner le Salam répand l'amour et la fraternité. Le Messager d'Allah {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:54') }}

Le Prophète {{ $page->pbuh() }} nous a ordonné de rendre le salam, et en a fait un droit et un devoir. Il dit :

{{ $page->hadith('muslim:2162a') }}

Il est clair qu'il est obligatoire de rendre les salam, car en faisant cela, c'est comme s'il vous disait : « Je vous donne la sécurité» ! Vous devez donc lui rendre la pareille, afin qu'il ne se méfie pas et ne pense pas que celui à qui il a donné le salut, le trahit ou l'ignore.
