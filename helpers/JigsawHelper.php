<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

class JigsawHelper
{
    /**
     * Get all published answers.
     *
     * @return Collection A collection of answer slugs grouped by the language code.
     */
    function getPublishedAnswers(): Collection
    {
        return collect(glob(__DIR__ . '/../source/_answers/*/*.md'))
            ->map(fn($path) => basename($path, '.blade.md'))
            ->groupBy(fn($slug) => explode('.', $slug)[1])
            ->map(fn($items) => $items->map(fn($slug) => strtok($slug, '.')));
    }

    function getPublishedLanguages(): Collection
    {
        return collect(glob(__DIR__ . '/../source/_answers/*'))
            ->map(fn($path) => basename($path));
    }

    function isPublished(string $slug, string $language): bool
    {
        return file_exists(__DIR__ . "/../source/_answers/{$language}/{$slug}.{$language}.blade.md");
    }

    function getContent(string $slug, string $language): string
    {
        return file_get_contents(__DIR__ . "/../source/_answers/{$language}/{$slug}.{$language}.blade.md");
    }

    function getMetadata(string $content): array
    {
        preg_match('/---(.*?)---/s', $content, $matches);
        return yaml_parse($matches[1]);
    }

    function getMarkdown(string $content): string
    {
        preg_match('/---(.*?)---/s', $content, $matches);
        return trim(str_replace($matches[0], '', $content));
    }

    function getParagraphsInAnswer(string $markdown): Collection
    {
        return collect(preg_split('/\n{2,}/', $markdown))->map(function ($paragraph) {
            return trim($paragraph);
        });
    }
}
