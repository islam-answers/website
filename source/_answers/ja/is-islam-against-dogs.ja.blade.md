---
extends: _layouts.answer
section: content
title: イスラムは犬に反対するのか？
date: 2024-12-15
description: イスラムでは動物を品位を保って優しくて道徳的に扱うべきだと教えられます。犬をペットとして飼わないが、決まった目的があれば飼うことはできます。
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 594447
---

動物はアッラーの創造物なのでムスリムは動物を軽蔑すべきではなく、むしろ品位を保って慈悲深くて、人道的に扱うべきです。アッラーの預言者ムハンマド {{ $page->pbuh() }} がこう言っています：

{{ $page->hadith('bukhari:3321') }}

## 犬を殺すこと：

ウマルの息子のアブドアッラーはこう言っています：

{{ $page->hadith('muslim:1570b') }}

しかし、この件の叙事の中でその犬の種類と、全体的な文脈を明確にしなければなりません。言及された犬は街を回っていた野生犬の群れで社会に迷惑をかけて、命や健康に危険をさらされるのです。ネズミと同じように病気を運び、移り、人々に多くの不便をもたらします。危険な動物と病気を移る動物も殺すことは世界中に受けられています。

全体的な証拠に基づいて、イスラム聖職者たちは、犬を殺す命令は捕食で有害な場合にのみ許されると述べています。一方で無害な犬は、その色にかかわらず、殺されるべきではありません。

## 清浄

大多数のイスラム学者によれば、犬の「よだれ」は不浄です。 いくつかの学者が犬の毛皮も不浄だと言っています。イスラムにおけるもっと重要な教えの一つはあらゆるレベルでの清浄だと考慮すれば、犬を飼うの禁止が合理的になるでしょう。それと、天使が犬のいる家に入らないという事実もあります。預言者ムハンマドが {{ $page->pbuh() }} こう語っています：

{{ $page->hadith('bukhari:3322') }}

## 犬をペットとして飼うこと：

イスラムは、ムスリムが犬をペットとして飼うことを禁じています。アッラーの預言者が {{ $page->pbuh() }}：

{{ $page->hadith('muslim:1574g') }}
