---
extends: _layouts.answer
section: content
title: Que signifie Bismillah ?
date: 2024-09-29
description: Bismillah signifie "au nom d'Allah". Les musulmans le prononcent avant
  de commencer une action au nom d'Allah et en recherchant son aide et ses bénédictions.
sources:
- href: https://islamqa.info/en/answers/21722/
  title: islamqa.info
- href: https://islamqa.info/en/answers/163573/
  title: islamqa.info
translator_id: 586055
---

Bismillah signifie "Au nom d'Allah". La forme complète est "Bismillah al-Rahman al-Rahim", qui se traduit par "Au nom d'Allah, le Tout Miséricordieux, le Très Miséricordieux".

Lorsque l'on dit "Bismillah" avant de commencer à faire quelque chose, cela signifie : "Je commence cette action en m'accompagnant du nom d'Allah ou en cherchant de l'aide par le nom d'Allah, cherchant ainsi la bénédiction. Allah est Dieu, le bien-aimé et l'adoré, vers lequel les cœurs se tournent avec amour, vénération et obéissance (adoration). Il est al-Rahman (le Tout Miséricordieux) dont l'attribut est une vaste miséricorde ; et al-Rahim (le Très Miséricordieux) qui fait en sorte que cette miséricorde atteigne sa création.

Ibn Jarir (qu'Allah lui fasse miséricorde) a dit :

> Allah, qu'Il soit exalté et que Son nom soit sanctifié, a enseigné à Son Prophète Muhammad {{ $page->pbuh() }} les bonnes manières en lui apprenant à mentionner Ses plus beaux noms avant toute action. Il lui a ordonné de mentionner ces attributs avant de commencer quoi que ce soit, et a fait de ce qu'Il lui a enseigné une voie à suivre pour tous les gens avant de commencer quoi que ce soit, des mots à écrire au début de leurs lettres et de leurs livres. Le sens apparent de ces mots indique exactement ce qu'ils signifient, et il n'est pas nécessaire de l'épeler.

Les musulmans disent « Bismillah » avant de manger en raison du récit rapporté par `Aicha (qu'Allah l'agrée), selon lequel le Prophète {{ $page->pbuh() }} a dit :

{{ $page->hadith('ibnmajah:3264') }}

Il y a une omission dans la phrase « Bismillah » lorsqu’elle est prononcée avant de commencer quelque chose, ce qui peut être « Je commence mon action au nom d’Allah », comme dire « Au nom d’Allah je lis », « Au nom d’Allah j’écris », « Au nom d’Allah je roule », etc. Ou encore « Mon départ est au nom d’Allah », « Ma course est au nom d’Allah », « Ma lecture est au nom d’Allah », etc. Il se peut que la bénédiction vienne du fait de prononcer le nom d’Allah en premier, et cela exprime également le fait de commencer seulement au nom d’Allah et non au nom de quelqu’un d’autre.
