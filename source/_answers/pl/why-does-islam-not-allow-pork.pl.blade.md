---
extends: _layouts.answer
section: content
title: Dlaczego islam nie zezwala na wieprzowinę?
date: 2024-07-23
description: Muzułmanie są posłuszni temu, co nakazuje im Allah i powstrzymują się
  od tego, czego On im zakazuje, niezależnie od tego, czy powód tego jest jasny, czy
  nie.
sources:
- href: https://islamqa.info/en/answers/49688/
  title: islamqa.info
translator_id: 583847
---

Podstawową zasadą dla muzułmanina jest to, że jest on posłuszny temu, co Allah mu nakazuje i powstrzymuje się od tego, czego mu zabrania, niezależnie od tego, czy powód tego jest jasny, czy nie.

Niedopuszczalne jest, by muzułmanin odrzucał jakiekolwiek postanowienie Prawa Szariatu lub wahał się, czy go przestrzegać, jeśli powód tego nie jest mu jasny. Musi on raczej zaakceptować postanowienia dotyczące halal i haram, gdy są one udowodnione w tekście, niezależnie od tego, czy rozumie powód, który za tym stoi, czy nie. Allah mówi:

{{ $page->verse('33:36') }}

### Dlaczego wieprzowina jest haram?

Wieprzowina jest haram (zakazana) w islamie zgodnie z tekstem Koranu, w którym Allah mówi:

{{ $page->verse('2:173..') }}

Muzułmaninowi nie wolno ją spożywać w żadnych okolicznościach, z wyjątkiem przypadków konieczności, gdy życie danej osoby zależy od jej spożycia, takich jak głód, gdy dana osoba obawia się, że umrze i nie może znaleźć innego rodzaju pożywienia.

W tekstach Szariatu nie ma wzmianki o konkretnym powodzie zakazu spożywania wieprzowiny, poza wersetem, w którym Allah mówi:

{{ $page->verse('..6:145..') }}

Słowo rijs (przetłumaczone tutaj jako "obrzydliwe") jest używane w odniesieniu do wszystkiego, co jest uważane za odrażające w islamie i zgodnie ze zdrową ludzką naturą (fitrah). Już sam ten powód jest wystarczający.

Istnieje ogólny powód, który jest podany w odniesieniu do zakazu haram jedzenia i napojów, i tym podobnych, który wskazuje na powód zakazu wieprzowiny. Ten ogólny powód można znaleźć w wersecie, w którym Allah mówi:

{{ $page->verse('..7:157..') }}

### Naukowe i medyczne powody zakazu spożywania wieprzowiny

Badania naukowe i medyczne dowiodły również, że świnia, wśród wszystkich innych zwierząt, jest uważana za nosiciela zarazków szkodliwych dla ludzkiego organizmu. Szczegółowe wyjaśnienie wszystkich tych szkodliwych chorób zajęłoby zbyt dużo czasu, ale w skrócie możemy je wymienić jako: choroby pasożytnicze, choroby bakteryjne, wirusy i tak dalej.

Te i inne szkodliwe skutki wskazują, że Allah zakazał spożywania wieprzowiny tylko z jednego powodu, którym jest ochrona życia i zdrowia, które są jednymi z pięciu podstawowych potrzeb chronionych przez Szariat.
