---
extends: _layouts.answer
section: content
title: Ce spune Islamul despre terorism?
date: 2024-02-12
description: Islamul, o religie a milei, nu permite terorismul.
sources:
- href: https://www.islam-guide.com/ch3-11.htm
  title: islam-guide.com
translator_id: 554943
---

Islamul, o religie a milei, nu permite terorismul. În Coran, Dumnezeu a spus:

{{ $page->verse('60:8') }}

Profetul Muhammad {{ $page->pbuh() }} obișnuia să interzică soldaților să omoare femei și copii și îi sfătuia: {...Nu trădați, nu fiți excesiv, nu ucideți un nou-născut.} Și a mai spus: { Oricine a ucis o persoană care a încheiat un tratat cu musulmanii nu va simți mirosul Paradisului, deși parfumul lui se găsește timp de patruzeci de ani.}

De asemenea, Profetul Muhammad {{ $page->pbuh() }} a interzis pedeapsa prin ardere cu focul.

El a menționat faptul că a face o crimă e al doilea dintre păcatele majore și chiar a avertizat că în Ziua Judecății, {Primele lucruri care vor fi judecate între oameni în Ziua Judecății vor fi cele ale vărsării de sânge.}

Musulmanii sunt încurajați să fie blânzi chiar și cu animalele și le este interzis să le rănească. Odată, Profetul Muhammad {{ $page->pbuh() }} a spus: {O femeie a fost pedepsită pentru că a închis o pisică până când aceasta a murit. Din această cauză, ea a fost condamnată în Iad. Cât timp a închis-o, ea nu a dat pisicii de mâncare sau de băut și nici nu a eliberat-o pentru a mânca insectele pământului.}

El a mai spus că un bărbat a dat de băut unui câine foarte însetat, așa că Dumnezeu i-a iertat păcatele pentru această acțiune. Profetul a fost întrebat: „Mesager al lui Dumnezeu, suntem răsplătiți pentru bunătatea față de animale?” El a spus: {Există o recompensă pentru bunătate față de orice animal sau om viu.}

De asemenea, atunci când sacrifică un animal pentru a se hrăni cu el, musulmanilor li se cere să facă acest lucru într-un mod care provoacă cea mai mică cantitate de frică și suferință posibilă animalului. Profetul Muhammad {{ $page->pbuh() }} a spus: {Când sacrifici un animal, fă-o în cel mai bun mod. Ar trebui să-ți ascuți cuțitul pentru a reduce suferința animalului.}

Având în vedere acestea și alte texte islamice, incitarea terorii în inimile civililor lipsiți de apărare, distrugerea totală a clădirilor și proprietăților, bombardarea și mutilarea bărbaților, femeilor și copiilor nevinovați sunt toate acte interzise și detestabile conform Islamului. Musulmanii urmează o religie a păcii, milei și iertării, iar marea majoritate nu au nimic de-a face cu evenimentele violente pe care unii le-au asociat cu musulmanii. Dacă un musulman ar comite un act de terorism, această persoană ar încălca legile Islamului.
