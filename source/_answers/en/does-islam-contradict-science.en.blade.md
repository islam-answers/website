---
extends: _layouts.answer
section: content
title: Does Islam contradict science?
date: 2024-03-23
description: The Quran contains scientific facts that have only been discovered recently
  through the advancement of technology and scientific knowledge.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
---

The Quran, the book of Islam, is the final book of revelation from God to humanity and the last in the line of revelations given to the Prophets.

Although the Quran (revealed over 1400 years ago), is not primarily a book of science, it does contain scientific facts that have only been discovered recently through the advancement of technology and scientific knowledge. Islam encourages reflection and scientific research because understanding the nature of creation enables people to further appreciate their Creator and the extent of His power and wisdom.

The Quran was revealed at a time when Science was primitive; there were no telescopes, microscopes or anything close to today's technology. People believed that the sun orbited the earth and the sky was held up by pillars at the corners of a flat earth. Against this backdrop the Quran was revealed, containing many scientific facts on topics ranging from astronomy to biology, geology to zoology.

Some of the many scientific facts found in the Quran include:

### Fact #1 - Origin of Life

{{ $page->verse('21:30') }}

Water is pointed out as the origin of all life. All living things are made of cells and we now know that cells are mostly made of water. This was discovered only after the invention of the microscope. In the deserts of Arabia, it would be inconceivable to think that someone would have guessed that all life came from water.

### Fact #2 - Human Embryonic Development

God speaks about the stages of man's embryonic development:

{{ $page->verse('23:12-14') }}

The Arabic word "alaqah" has three meanings: a leech, a suspended thing and a blood clot. "Mudghah" means a chewed substance. Embryology scientists have observed that the usage of these terms in describing the formation of the embryo is accurate and in conformity with our current scientific understanding of the development process.

Little was known about the staging and classification of human embryos until the twentieth century, which means that the descriptions of the human embryo in the Quran cannot be based on scientific knowledge from the seventh century.

### Fact #3 - Expansion of the Universe

At a time when the science of Astronomy was still primitive, the following verse in the Quran was revealed:

{{ $page->verse('51:47') }}

One of the intended meanings of the above verse is that God is expanding the universe (i.e. heavens). Other meanings are that God provides for, and has power over, the universe - which are also true.

The fact that the universe is expanding (e.g. planets are moving further away from each other) was discovered in the last century. Physicist Stephen Hawking in his book 'A Brief History of Time' writes, "The discovery that the universe is expanding was one of the great intellectual revolutions of the twentieth century."

The Quran alludes to the expansion of the universe even before the invention of the telescope!

### Fact #4 - Iron Sent Down

Iron is not natural to earth, as it came to this planet from outer space. Scientists have found that billions of years ago, the earth was struck by meteorites which were carrying iron from distant stars which had exploded.

{{ $page->verse('..57:25..') }}

God uses the words 'sent down'. The fact that iron was sent down to earth from outer space is something which could not be known by the primitive science of the seventh century.

### Fact #5 - Sky's Protection

The sky plays a crucial role in protecting the earth and its inhabitants from the lethal rays of the sun, as well as the freezing cold of space.

God asks us to consider the sky in the following verse:

{{ $page->verse('21:32') }}

The Quran points to the sky's protection as a sign of God, protective properties which were discovered by scientific research conducted in the twentieth century.

### Fact #6 - Mountains

God draws our attention to an important characteristic of mountains:

{{ $page->verse('78:6-7') }}

The Quran accurately describes the deep roots of mountains by using the word "pegs". Mount Everest, for example, has an approximate height of 9km above ground, while its root is deeper than 125km!

The fact that mountains have deep 'peg'-like roots was not known until after the development of the theory of plate tectonics in the beginning of the twentieth century. God also says in the Quran (16:15), that the mountains have a role in stabilising the earth "…so that it would not shake," which has just begun to be understood by scientists.

### Fact #7 - Sun's Orbit

In 1512, astronomer Nicholas Copernicus put forward his theory that the Sun is motionless at the centre of the solar system and the planets revolve around it. This belief was widespread amongst astronomers until the twentieth century. It is now a well established fact that the Sun is not stationary but is moving in an orbit around the centre of our Milky Way galaxy.

{{ $page->verse('21:33') }}

### Fact #8 - Internal Waves in the Ocean

It was commonly thought that waves only occur on the surface of the ocean. However, oceanographers have discovered that there are internal waves that take place below the surface which are invisible to the human eye and can only be detected by specialised equipment.

The Quran mentions:

{{ $page->verse('24:40..') }}

This description is amazing because 1400 years ago there was no specialist equipment to discover the internal waves deep inside the oceans.

### Fact #9 - Lying & Movement

There was a cruel oppressive tribal leader who lived during the time of Prophet Muhammad {{ $page->pbuh() }}. God revealed a verse to warn him:

{{ $page->verse('96:15-16') }}

God does not call this person a liar, but calls his forehead (the front part of the brain) 'lying' and 'sinful', and warns him to stop. Numerous studies have found that the front part of our brain (frontal lobe) is responsible for both lying and voluntary movement, and hence sin. These functions were discovered with medical imaging equipment which was developed in the twentieth century.

### Fact #10 - The Two Seas that do not Mix

Regarding the seas, our Creator says:

{{ $page->verse('55:19-20') }}

A physical force called surface tension prevents the waters of neighbouring seas from mixing, due to the difference in the density of these waters. It is as if a thin wall were between them. This has only very recently been discovered by oceanographers.

### Couldn't Muhammad have authored the Quran?

The Prophet Muhammad {{ $page->pbuh() }} was known in history to be illiterate; he could not read nor write and was not educated in any field that could account for the scientific accuracies in the Quran.

Some may claim that he copied it from learned people or scientists of his time. If it was copied, we would have expected to see all the incorrect scientific assumptions of the time also being copied. Rather, we find that the Quran does not contain any errors whatsoever - be they scientific or otherwise.

Some people may also claim that the Quran was changed as new scientific facts were discovered. This cannot be the case because it is a historically documented fact that the Quran is preserved in its original language - which is a miracle in itself.

### Just a coincidence?

{{ $page->verse('41:53') }}

While this answer has only focused on scientific miracles, there are many more types of miracles mentioned in the Quran: historical miracles; prophecies that have come true; linguistic and literary styles that cannot be matched; not to mention the moving effect it has on people. All these miracles cannot be due to coincidence. They clearly indicate that the Quran is from God, the Creator of all these laws of science. He is the one and same God who sent all the Prophets with the same message - to worship the one God only and to follow the teachings of His Messenger.

The Quran is a book of guidance that demonstrates that God did not create humans to simply wander aimlessly. Rather, it teaches us that we have a meaningful and higher purpose in life - to acknowledge God’s complete perfection, greatness and uniqueness, and obey Him.

It is up to each person to use their God-given intellect and reasoning abilities to contemplate and recognise God’s signs - the Quran being the most important sign. Read and discover the beauty and truth of the Quran, so that you may attain success!
