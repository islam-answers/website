---
extends: _layouts.answer
section: content
title: L'islam autorise-t-il les mariages forcés ?
date: 2024-06-23
description: Les hommes comme les femmes ont le droit de choisir leur conjoint, et
  un mariage est considéré comme nul s'il n'est pas véritablement approuvé par la
  femme.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 573111
---

Les mariages arrangés sont des pratiques culturelles prédominantes dans certains pays du monde. Bien qu'ils ne soient pas réservés aux musulmans, les mariages forcés ont été associés à tort à l'islam.

Dans l'islam, les hommes comme les femmes ont le droit de choisir ou de rejeter leur conjoint potentiel. Un mariage est considéré comme nul et invalide s'il n'est pas précédé d'une véritable approbation de la part de la femme.
