---
extends: _layouts.answer
section: content
title: Da li je u redu odgoditi prelazak na islam?
date: 2025-02-20
description: Odgađanje prelaska na islam je obeshrabreno, jer onaj ko umre sledeći
  veru koja nije islam, izgubio je život na ovom svetu (dunjaluku) i na budućem (ahiretu).
sources:
- href: https://islamqa.info/en/answers/2585/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/260971/
  title: islamweb.net
translator_id: 584497
---

Islam je prava Allahova vera i njeno prihvaćanje pomaže osobi da postigne sreću na dunjaluku i na ahiretu. Allah je zaključio božanske objave sa religijom islama; shodno tome, On ne prihvata da Njegovi robovi imaju drugu vjeru osim islama, kao što kaže u Kur'anu:

{{ $page->verse('3:85') }}

Ko umre sledeći veru koja nije islam, izgubio je život na dunjaluku i na ahiretu. Dakle, osoba je obavezna da požuri da prihvati islam jer ne zna za moguće prepreke na koje može naići, uključujući i smrt. Stoga ne bi trebalo biti odugovlačenja u ovom pogledu.

Smrt je moguća bilo kada, pa treba požuriti da uđe u islam odmah, kako bi, ako uskoro dođe njihovo vreme, sreli Allaha kao sljedbenika Njegove vere, islama, osim koje On ne prihvata nijednu drugu veru.

Treba napomenuti da je musliman dužan pridržavati se prividnih pobožnih obreda i dužnosti kao što je obavljanje namaza na vreme; međutim, neko ih može obaviti tajno, pa čak i kombinirati dvije molitve prema sunnetu Poslanika {{ $page->pbuh() }} kada se pojavi hitna potreba.

Stoga, umreti kao nemusliman nije isto kao umreti kao muslimanski grešnik. Nemusliman će večno boraviti u džehennemskoj vatri. Međutim, čak i ako musliman uđe u Džehennem zbog greha, neko će biti kažnjen za njih, ali neće trajno ostati u njemu. Štaviše, Allah bi ovome mogao oprostiti grehe, spasiti takvu osobu od džehennemske vatre i uvesti je u Džennet. Allah kaže u Kur'anu:

{{ $page->verse('4:116') }}

Na kraju, savetujemo vam da požurite da prihvatite islam i sve će biti bolje, ako Allah da. Allah kaže u Kur'anu:

{{ $page->verse('..65:2-3..') }}
