---
extends: _layouts.answer
section: content
title: Este Islamul numai pentru arabi?
date: 2024-03-13
description: Islamul nu este numai pentru arabi. Majoritatea musulmanilor nu sunt
  arabi, iar Islamul este o religie universală pentru toate popoarele.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=MSC01&articleID=MSC010003&articlePages=1
  title: spubs.com
translator_id: 554943
---

Faptul că Islamul ar fi o religie numai pentru arabi este complet fals - de fapt, doar aproximativ 15% până la 20% dintre musulmanii din lume sunt arabi. Există mai mulți musulmani indieni decât musulmani arabi și mai mulți musulmani indonezieni decât musulmani indieni! A crede că Islamul este doar o religie pentru arabi este un mit care a fost răspândit de dușmanii Islamului la începutul istoriei sale. Această presupunere greșită se bazează probabil pe faptul că majoritatea primei generații de musulmani erau arabi, Coranul este în arabă și Profetul Muhammad {{ $page->pbuh() }} era arab. Cu toate acestea, atât învățăturile Islamului, cât și istoria răspândirii sale arată că primii musulmani au făcut toate eforturile pentru a răspândi mesajul religiei lor tuturor națiunilor, raselor și popoarelor.

Mai mult decât atât, ar trebui clarificat faptul că nu toți arabii sunt musulmani și nu toți musulmanii sunt arabi. Un arab poate fi musulman, creștin, evreu, ateu sau de orice altă religie sau ideologie. De asemenea, multe țări pe care unii oameni le consideră a fi „arabe” nu sunt deloc „arabe”, cum ar fi Turcia și Iranul (Persia). Oamenii care trăiesc în aceste țări vorbesc alte limbi decât araba ca limbi materne și sunt de o moștenire etnică diferită de cea a arabilor.

Este important să ne dăm seama că încă de la începutul misiunii începute de Profetul Muhammad {{ $page->pbuh() }}, adepții săi proveneau dintr-un spectru larg de indivizi - de exemplu Bilal, sclavul african; Suhaib, romanul bizantin; Ibn Sailam, rabinul evreu; și Salman, persanul. Întrucât adevărul religios este etern și neschimbător, iar omenirea este o singură frăție universală, Islamul învață că revelațiile lui Dumnezeu Atotputernicul către omenire au fost întotdeauna consecvente, clare și universale.

Adevărul Islamului este destinat tuturor oamenilor, indiferent de rasă, naționalitate sau fundal lingvistic. Aruncând o privire asupra lumii musulmane, din Nigeria până în Bosnia și din Malaezia până în Afganistan, putem demonstra că islamul este un mesaj universal pentru întreaga omenire - ca să nu mai vorbim de faptul că un număr semnificativ de europeni și americani de toate rasele și originile etnice se convertesc la Islam.
