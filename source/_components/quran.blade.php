<blockquote class="border-green-600">
    <p class="mb-2">
        {{ $text }}
    </p>

    <div class="flex justify-start">
        <cite class="text-xs text-gray-600">
            <a href="https://quran.com/{{ $reference }}?translations={{ $answer->getString('translation_id') }}" title="{{ $answer->getString('quran') }} - {{ $reference }}" class="text-gray-600" target="_blank">
                {{ $answer->getString('quran') }}
                -
                {{ $reference }}
            </a>
        </cite>

        <div class="text-xs text-gray-600 my-0 ms-2">
            ({{ $answer->getString('quran_translation_notice') }})
        </div>
    </div>
</blockquote>
