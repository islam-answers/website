---
extends: _layouts.answer
section: content
title: Dlaczego prorok Muhammad poślubił młodą dziewczynę?
date: 2025-02-18
description: Małżeństwo Mahometa z Aiszą było normą VII wieku; dziś krytykowane według
  współczesnych standardów.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
translator_id: 584496
---

Aisza (niech Allah będzie z niej zadowolony) miała 6 lat, kiedy wyszła za mąż za Proroka Muhammada {{ $page->pbuh() }}, a miała 9 lat, kiedy małżeństwo zostało skonsumowane.

Niestety, obecnie widzimy, że wysiłki innych religii skupiają się głównie na przekręcaniu i manipulowaniu faktami historycznymi i etymologicznymi. Jednym z takich tematów jest oskarżenie o młode małżeństwo Aiszy z Prorokiem Mahometem {{ $page->pbuh() }}. Misjonarze próbują oskarżyć Proroka o molestowanie dzieci, choć w politycznie poprawnych terminach, z uwagi na fakt, że Aisza została zaręczona w wieku 6 lat, a małżeństwo zostało skonsumowane 3 lata później — w wieku 9 lat — gdy była w pełni dojrzała płciowo. Upływ czasu między zaręczynami a skonsumowaniem małżeństwa Aiszy wyraźnie pokazuje, że jej rodzice czekali, aż osiągnie dojrzałość płciową, zanim jej małżeństwo zostało skonsumowane. Ta odpowiedź ma na celu obalenie niesprawiedliwego oskarżenia pod adresem Proroka Mahometa {{ $page->pbuh() }}.

### Dojrzewanie i młode małżeństwo w kulturze semickiej

Oskarżenie o molestowanie dziecka przeczy podstawowemu faktowi, że dziewczynka staje się kobietą, gdy zaczyna się jej cykl menstruacyjny. Znaczenie menstruacji, o którym powie ci każdy, kto ma choćby najmniejszą znajomość fizjologii, polega na tym, że jest to znak, że dziewczynka przygotowuje się do zostania matką.

Kobiety osiągają dojrzałość płciową w różnym wieku, od 8 do 12 lat, w zależności od genetyki, rasy i środowiska. Istnieje niewielka różnica w wielkości chłopców i dziewcząt do dziesiątego roku życia, skok wzrostu w okresie dojrzewania zaczyna się wcześniej u dziewcząt, ale trwa dłużej u chłopców. Pierwsze oznaki dojrzewania występują około 9 lub 10 roku życia u dziewcząt, ale bliżej 12 roku życia u chłopców. Ponadto kobiety w cieplejszym środowisku osiągają dojrzałość płciową w znacznie wcześniejszym wieku niż te w zimnym środowisku. Średnia temperatura kraju lub prowincji jest tutaj uważana za najważniejszy czynnik, nie tylko w odniesieniu do menstruacji, ale w odniesieniu do całego rozwoju seksualnego w okresie dojrzewania.

Małżeństwo we wczesnych latach dojrzewania było akceptowalne w Arabii VII wieku, ponieważ było normą społeczną we wszystkich kulturach semickich od Izraelitów po Arabów i wszystkie narody pomiędzy nimi. Zgodnie z Talmudem, który Żydzi uważają za swoją „ustną Torę”, Sanhedryn 76b wyraźnie stwierdza, że lepiej jest, aby kobieta wyszła za mąż, gdy ma pierwszą miesiączkę, a w Ketuvot 6a znajdują się zasady dotyczące stosunków seksualnych z dziewczętami, które jeszcze nie miesiączkowały. Jim West, pastor baptystyczny, przestrzega następującej tradycji Izraelitów:

- Żona miała być brana z większego kręgu rodzinnego (zwykle na początku dojrzewania lub około 13 roku życia), aby utrzymać czystość linii rodzinnej. 
- Dojrzewanie zawsze było symbolem dorosłości w historii. 
- Dojrzewanie jest definiowane jako wiek lub okres, w którym dana osoba jest po raz pierwszy zdolna do rozmnażania płciowego; w innych epokach historycznych rytuał lub świętowanie tego przełomowego wydarzenia było częścią kultury.

Znani seksuolodzy R.E.L. Masters i Allan Edwards w swoim badaniu nad ekspresją seksualną Afro-Azjatów stwierdzają, co następuje:

> Obecnie w wielu rejonach Afryki Północnej i Bliskiego Wschodu dziewczynki wychodzą za mąż i idą do łóżka w wieku od pięciu do dziewięciu lat; żadna szanująca się kobieta nie pozostaje niezamężna po osiągnięciu wieku dojrzewania.

### Powody poślubienia Aiszy

Jeśli chodzi o historię jej małżeństwa, Prorok {{ $page->pbuh() }} opłakiwał śmierć swojej pierwszej żony Khadeejah, która go wspierała i stała u jego boku, i nazwał rok, w którym zmarła, Rokiem Smutku. Następnie poślubił Sawdah, która była starszą kobietą i nie była zbyt piękna; raczej poślubił ją, aby pocieszyć ją po śmierci jej męża. Cztery lata później Prorok {{ $page->pbuh() }} poślubił Aishę. Powody małżeństwa były następujące:

1. Miał sen o poślubieniu jej. Z opowieści Aiszy wynika, że Prorok {{ $page->pbuh() }} powiedział do niej:

{{ $page->hadith('bukhari:3895') }}

2. Cechy inteligencji i sprytu, które Prorok {{ $page->pbuh() }} zauważył u Aiszy już jako małego dziecka, więc chciał ją poślubić, aby była bardziej zdolna niż inni do przekazywania raportów o tym, co robił i mówił. W rzeczywistości, jak wspomniano powyżej, była punktem odniesienia dla towarzyszy Proroka {{ $page->pbuh() }} w odniesieniu do ich spraw i orzeczeń.

3. Miłość Proroka {{ $page->pbuh() }} do jej ojca Abu Bakra i prześladowania, których Abu Bakr doznał w imię powołania do Islamu, które znosił z cierpliwością. Był najsilniejszym i najbardziej szczerym z ludzi w wierze po Prorokach.

### Czy były jakieś przeciwwskazania do ich małżeństwa?

Odpowiedź brzmi: nie. Nie ma absolutnie żadnych zapisów z muzułmańskich, świeckich ani innych źródeł historycznych, które nawet w sposób dorozumiany pokazywałyby cokolwiek innego niż całkowitą radość wszystkich stron zaangażowanych w to małżeństwo. Nabia Abbott opisuje małżeństwo Aiszy z Prorokiem {{ $page->pbuh() }} w następujący sposób:

> W żadnej wersji nie ma komentarza na temat różnicy wieku między Mohammedem a Aishah ani na temat młodego wieku panny młodej, która nie mogła mieć więcej niż dziesięć lat i która wciąż była bardzo zakochana w swojej zabawie.

Nawet znany krytyk orientalistyczny, W. Montgomery Watt, powiedział o charakterze moralnym Proroka {{ $page->pbuh() }} następujące słowa:

> Z punktu widzenia czasów Mahometa, zarzuty o zdradę i zmysłowość nie mogą być podtrzymane. Jego współcześni nie uważali go za moralnie wadliwego w żaden sposób. Wręcz przeciwnie, niektóre z czynów krytykowanych przez współczesnych ludzi Zachodu pokazują, że standardy Muhammada były wyższe niż te, które obowiązywały w jego czasach.

Oprócz faktu, że nikt nie był niezadowolony z niego lub jego czynów, był on wybitnym przykładem charakteru moralnego w swoim społeczeństwie i czasach. Dlatego też ocenianie jego moralności na podstawie standardów naszego dzisiejszego społeczeństwa i kultury jest nie tylko absurdalne, ale także niesprawiedliwe.

### Małżeństwo w okresie dojrzewania dzisiaj

Współcześni Prorokowi (zarówno wrogowie, jak i przyjaciele) wyraźnie zaakceptowali małżeństwo Proroka {{ $page->pbuh() }} z Aiszą bez żadnych problemów. Dowodem na to jest brak krytyki tego małżeństwa aż do czasów współczesnych.

Przyczyną tego jest zmiana w kulturze naszych czasów, dotycząca wieku, w którym ludzie zazwyczaj zawierają związek małżeński. Ale nawet dzisiaj, w XXI wieku, wiek zgody seksualnej jest nadal dość niski w wielu miejscach. W Niemczech, Włoszech i Austrii ludzie mogą legalnie uprawiać seks w wieku 14 lat, a na Filipinach i w Angoli mogą legalnie uprawiać seks w wieku 12 lat. Kalifornia była pierwszym stanem, który zmienił wiek zgody na 14 lat, co uczynił w 1889 roku. Po Kalifornii dołączyły inne stany i również podniosły wiek zgody.

### Islam i wiek dojrzewania

Islam jasno naucza, że dorosłość zaczyna się, gdy dana osoba osiągnie wiek dojrzewania. Allah mówi w Koranie:

{{ $page->verse('24:59..') }}

Osiągnięcie dojrzałości płciowej przez kobiety następuje wraz z początkiem miesiączki, jak mówi Allah w Koranie:

{{ $page->verse('65:4..') }}

Dlatego częścią islamu jest uznanie nadejścia okresu dojrzewania za początek dorosłości. Jest to czas, w którym osoba jest już dojrzała i gotowa do odpowiedzialności osoby dorosłej. Na jakiej więc podstawie ktoś miałby krytykować małżeństwo Aiszy, skoro jej małżeństwo zostało skonsumowane po osiągnięciu przez nią dojrzałości?

Gdyby zarzut „molestowania dzieci” wysunięto przeciwko Prorokowi {{ $page->pbuh() }}, musielibyśmy również uwzględnić wszystkich Semitów, którzy zaakceptowali małżeństwo w okresie dojrzewania jako normę.

Co więcej, wiedząc, że Prorok {{ $page->pbuh() }} nie poślubił żadnej dziewicy oprócz Aiszy i że wszystkie jego pozostałe żony były wcześniej zamężne (i wiele z nich było w podeszłym wieku), obalimy pogląd rozpowszechniany przez wiele wrogich źródeł, że podstawowym motywem małżeństw Proroka było pożądanie fizyczne i przyjemność z kobiet, ponieważ gdyby taki był jego zamiar, wybierałby tylko młode kobiety.

### Wnioski

Widzieliśmy zatem, że:

- W VII wieku w Arabii semickie społeczeństwo zezwalało na małżeństwa osób w okresie dojrzewania. 
- Nie ma żadnych doniesień o sprzeciwie wobec małżeństwa Proroka z Aiszą ani ze strony jego przyjaciół, ani wrogów. 
- Nawet dzisiaj istnieją kultury, które nadal zezwalają na małżeństwa kobiet w okresie dojrzewania.

Mimo tych powszechnie znanych faktów niektórzy ludzie nadal oskarżają Proroka {{ $page->pbuh() }} o niemoralność. A jednak to on wymierzył sprawiedliwość kobietom Arabii i podniósł je do poziomu, jakiego wcześniej nie widziały w swoim społeczeństwie, czego starożytne cywilizacje nigdy nie zrobiły swoim kobietom.

Na przykład, kiedy po raz pierwszy został Prorokiem, poganie z Arabii odziedziczyli pogardę dla kobiet, która była przekazywana ich żydowskim i chrześcijańskim sąsiadom. Tak haniebne było wśród nich błogosławieństwo posiadania dziecka płci żeńskiej, że posunęli się tak daleko, że pochowali to dziecko żywcem, aby uniknąć hańby związanej z dziećmi płci żeńskiej. Allah mówi w Koranie:

{{ $page->verse('16:58-59') }}

Muhammad {{ $page->pbuh() }} nie tylko surowo zniechęcał i potępiał ten akt, ale także uczył ludzi, aby szanowali i cenili swoje córki i matki jako partnerki i źródła zbawienia dla mężczyzn w ich rodzinie. Powiedział:

{{ $page->hadith('ibnmajah:3669') }}

Zostało to również opowiedziane w hadisie od Anasa Ibn Malika:

{{ $page->hadith('muslim:2631') }}

Innymi słowy, jeśli ktoś kocha Posłańca Allaha {{ $page->pbuh() }} i pragnie być z nim w Dniu Zmartwychwstania w Niebie, to powinien być dobry dla swoich córek. Z pewnością nie jest to akt „pedofila”, jak niektórzy chcieliby, abyśmy wierzyli.
