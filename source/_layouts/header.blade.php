@php
$menuItems = [
    ['title' => 'Home', 'url' => '/', 'active' => $page->isActive('/')],
    ['title' => 'Answers', 'url' => '/answers/', 'active' => $page->isActive('/answers')],
    ['title' => 'About', 'url' => '/about/', 'active' => $page->isActive('/about')]
];
@endphp
<header class="flex items-center shadow bg-white border-b h-16 md:h-24 py-4" role="banner">
    <div class="container flex items-center justify-between max-w-8xl mx-auto px-4 lg:px-8">
        <div class="flex items-center">
            <a href="/" title="{{ $page->siteName }} home" class="inline-flex items-center">
                @include('_components.logo')

                <span class="text-lg md:text-2xl text-blue-800 font-semibold hover:text-blue-600 my-0">{{ $page->siteName }}</span>
            </a>
        </div>

        <div class="flex items-center">
            <nav class="hidden lg:flex items-center text-lg ml-8">
                @foreach($menuItems as $item)
                <a title="{{ $page->siteName }} {{ $item['title'] }}" href="{{ $item['url'] }}" class="mr-6 text-gray-700 hover:text-blue-600 {{ $item['active'] ? 'active text-blue-600' : '' }}">
                    {{ $item['title'] }}
                </a>
                @endforeach
            </nav>

            <details class="relative lg:hidden">
                <summary class="text-gray-900 list-none">
                    <span class="sr-only">Menu</span>
                    <svg class="h-6 w-6" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"/>
                    </svg>
                </summary>

                <nav class="absolute right-0 w-48 py-2 mt-2 bg-white rounded-lg shadow-xl border">
                    @foreach($menuItems as $item)
                    <a href="{{ $item['url'] }}" class="block px-4 py-2 text-gray-700 hover:text-blue-600 {{ $item['active'] ? 'active text-blue-600' : '' }}">
                        {{ $item['title'] }}
                    </a>
                    @endforeach
                </nav>
            </details>

            <a href="/join/" class="ml-6 px-4 py-2 bg-blue-600 text-white rounded hover:bg-blue-700 hover:text-white">Join us</a>
        </div>
    </div>
</header>