---
extends: _layouts.answer
section: content
title: Ce înseamnă cuvântul Mahram în Islam?
date: 2024-11-28
description: Un Mahram este o persoană cu care căsătoria este permanent interzisă,
  cum ar fi rudele apropiate de sânge, rudele adoptive sau socrii.
sources:
- href: https://islamqa.org/hanafi/daruliftaa/8495/
  title: islamqa.org (Daruliftaa.com)
- href: https://islamqa.info/en/answers/5538/
  title: islamqa.info
- href: https://islamqa.info/en/answers/130002/
  title: islamqa.info
translator_id: 554943
---

Ca principiu general, un Mahram este o persoană cu care căsătoria este permanent ilegală. Acesta este motivul pentru care "Mahram" este tradus în limba românǎ prin "rudǎ necăsătoribilǎ". Este permis ca o femeie să își dea jos hijab-ul în fața rudelor sale Mahram. De asemenea, este permisă strângerea de mână sau sărutarea unui Mahram pe cap, nas sau obraz.

Aceastǎ interzicere permanentă a căsătoriei este stabilită în trei moduri: prin rudenie (legături de sânge), relație adoptivǎ (mai precis prin alăptare) și relație prin căsătorie.

Astfel, ilegalitatea permanentă a căsătoriei este stabilită prin cele trei tipuri de relații menționate mai sus, iar un Mahram este cel cu care căsătoria este ilegală permanent. Cu alte cuvinte, o persoană devine Mahram datorită acestor trei tipuri de relații.

## Relația de rudenie/familie

Este permanent ilegal ca un bărbat să se căsătorească cu următoarele (prin urmare, el va fi considerat Mahram pentru ele):

- Mama, mama ei (bunica) și tot așa mai departe;
- Bunica paternă și tot așa mai departe;
- Fiicele, nepoatele și tot așa mai departe;
- Toate tipurile de surori (inclusiv cele vitrege);
- Mătușile materne și paterne;
- Nepoatele (fiicele fratelui sau surorii)

Allah spune în Coran:

{{ $page->verse('4:23') }}

În mod similar, rudele "Mahram" ale unei femei prin relațiile de familie sunt:

- Tatăl, tatǎl sǎu (bunicul) și tot așa mai departe;
- Bunicul matern și tot așa mai departe;
- Fiii, nepoții, și tot așa mai departe;
- Toate tipurile de frați (inclusiv cei vitregi);
- Unchi materni și paterni;
- Nepoți (fiii fratelui sau ai surorii)

Allah spune în Coran:

{{ $page->verse('..24:31..') }}

## Relația de adopție (prin alǎptare)

Oricine este un Mahram prin relația de rudenie va fi, de asemenea, considerat un Mahram prin adopție. Astfel, un tată adoptiv (soțul mamei adoptive), un frate adoptiv, un unchi adoptiv, un nepot adoptiv etc. vor fi considerați Mahram pentru o femeie, iar o persoană va fi Mahram pentru o mamă adoptivă, o soră adoptivă, o nepoată adoptivă etc.

Cu toate acestea, ar trebui să ne amintim că acest lucru se întâmplă numai atunci când alăptarea (de cǎtre o altǎ femeie decât mama) are loc în perioada prevăzută pentru aceasta, care este de doi ani. Trebuie să fim atenți atunci când stabilim cine este un Mahram prin intermediul relațiilor adoptive, deoarece acest lucru poate fi uneori complex. Trebuie să se apeleze la un savant înainte de a ajunge la o hotărâre.

## Relația de căsătorie

A treia relație prin care căsătoria devine ilegală în mod permanent și, în consecință, se stabilește relația de a fi Mahram este cea a căsătoriei.

Există patru tipuri de persoane cu care căsătoria devine ilegală permanent din cauza relației de căsătorie:

- Mama soției (soacra), bunica și așa mai departe: Căsătoria cu aceasta devine ilegală prin simpla semnare a contractului de căsătorie cu fiica, indiferent dacă căsătoria a fost consumată sau nu.
- Fiica soției (dintr-o căsătorie anterioară), nepoata și de la aceasta în jos: Căsătoria cu aceasta devine ilegală (permanent) dacă căsătoria cu mama sa a fost consumată.
- Soția fiului, nepotului și așa mai departe: Acest lucru este valabil indiferent dacă fiul a consumat căsătoria sau nu.
- Mama vitregă, bunica vitregă și așa mai departe: Adică acele femei care au fost căsătorite cu tatăl sau bunicul patern sau matern al cuiva.

În concluzie, un Mahram este cel cu care căsătoria este permanent ilegală, iar această ilegalitate/interzicere permanentă a căsătoriei se stabilește în trei moduri: relația de rudenie (familie), relația prin adopție (alǎptare) și relația prin căsătorie.
