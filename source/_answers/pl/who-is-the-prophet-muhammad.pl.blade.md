---
extends: _layouts.answer
section: content
title: Kim jest Prorok Muhammad?
date: 2024-07-14
description: Prorok Muhammad (Pokój z Nim) był doskonałym przykładem uczciwego, sprawiedliwego,
  miłosiernego, współczującego, prawdomównego i odważnego człowieka.
sources:
- href: https://www.islam-guide.com/ch3-8.htm
  title: islam-guide.com
translator_id: 583847
---

Muhammad {{ $page->pbuh() }} urodził się w Mekce w roku 570. Ponieważ jego ojciec zmarł przed jego narodzinami, a jego matka zmarła wkrótce potem, został wychowany przez swojego wuja, który pochodził z szanowanego plemienia Kurajszytów. Został wychowany jako analfabeta, nie potrafił czytać ani pisać i pozostał nim aż do śmierci. Jego ludzie, przed jego misją jako proroka, nie znali nauki i większość z nich była analfabetami. Gdy dorastał, stał się znany jako prawdomówny, uczciwy, godny zaufania, hojny i szczery. Był tak godny zaufania, że nazywano go Godnym Zaufania. Muhammad {{ $page->pbuh() }} był bardzo religijny i od dawna nie znosił dekadencji i bałwochwalstwa swojego społeczeństwa.

W wieku czterdziestu lat Muhammad {{ $page->pbuh() }} otrzymał swoje pierwsze objawienie od Boga za pośrednictwem Anioła Gabriela. Objawienia trwały przez dwadzieścia trzy lata i są znane jako Koran.

Gdy tylko zaczął recytować Koran i głosić prawdę, którą objawił mu Bóg, on i jego niewielka grupa zwolenników doświadczyli prześladowań ze strony niewierzących. Prześladowania te stały się tak zaciekłe, że w roku 622 Bóg nakazał im emigrację. Ta emigracja z Mekki do Medyny, około 260 mil na północ, wyznacza początek kalendarza muzułmańskiego.

Po kilku latach Prorok Muhammad {{ $page->pbuh() }} i jego zwolennicy mogli powrócić do Mekki, gdzie przebaczyli swoim przeciwnikom. Zanim Prorok Muhammad {{ $page->pbuh() }} zmarł w wieku sześćdziesięciu trzech lat, większa część Półwyspu Arabskiego stała się muzułmańska, a w przeciągu stulecia od jego śmierci islam rozprzestrzenił się od Hiszpanii na Zachodzie aż do Chin na Wschodzie. Jednym z powodów szybkiego i pokojowego rozprzestrzeniania się islamu była prawda i jasność jego doktryny. Islam wzywa do wiary tylko w jednego Boga, który jest jedynym godnym czci.

Prorok Muhammad {{ $page->pbuh() }} był doskonałym przykładem człowieka uczciwego, sprawiedliwego, miłosiernego, współczującego, prawdomównego i odważnego. Chociaż był człowiekiem, był daleki od wszelkich złych cech i starał się wyłącznie ze względu na Boga i Jego nagrodę w Życiu Ostatecznym. Co więcej, we wszystkich swoich czynach i poczynaniach zawsze był uważny i bał się Boga.
