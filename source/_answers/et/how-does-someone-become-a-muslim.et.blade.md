---
extends: _layouts.answer
section: content
title: Kuidas moslemiks saadakse?
date: 2024-06-08
description: Öeldes vaid veendunult, “Lää ilääha illallah, Muhammadur rasuulullah,”pöördub
  inimene islamisse ja temast saab moslem.
sources:
- href: https://www.islam-guide.com/ee/ch3-6.htm
  title: islam-guide.com
---

Öeldes vaid veendunult, “Lää ilääha illallah, Muhammadur rasuulullah,”pöördub inimene islamisse ja temast saab moslem. See ütlus tähendab: “Pole teist jumalust peale Jumala, ja Muhammed on Jumala Sõnumitooja (Prohvet).” Esimene osa: „Ei ole teist jumalust peale Jumala,” tähendab, et ei ole õigust teenida kedagi teist peale Jumala ja et Jumalal ei ole ei kaaslast ega ka poega. Et olla moslem, tuleb ka:

- Uskuda, et püha Koraan on otsene sõna Jumalalt ja Tema poolt ilmutatud.
- Uskuda, et Viimne Kohtupäev (Ülestõusmispäev) on tõeline ja saabub, nagu Jumal on Koraanis lubanud .
- Võtta islam oma religiooniks.
- Ei tohi kummardada kedagi ega midagi peale Jumala.

Prohvet Muhammad {{ $page->pbuh() }} ütles:

{{ $page->hadith('muslim:2747') }}
