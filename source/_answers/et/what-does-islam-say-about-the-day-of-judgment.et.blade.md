---
extends: _layouts.answer
section: content
title: Mida ütleb islam Viimse Kohtupäeva kohta?
date: 2024-06-08
description: Saabub päev, mil Jumal hävitab kogu unversumi ja äratab surnud üles kohtumõistmiseks.
sources:
- href: https://www.islam-guide.com/ee/ch3-5.htm
  title: islam-guide.com
---

Nagu kristlased, nii usuvad ka moslemid, et praegune elu on vaid ettevalmistus järgmiseks eksistentsiks. Praegune elu on test igale isikule eluks pärast surma. Saabub päev, mil Jumal hävitab kogu unversumi ja äratab surnud üles kohtumõistmiseks. See päev saab olema selle elu algus, mis iial ei lõpe. See päev on Viimne Kohtupäev. Sel päeval tasub Jumal kõigile inimestele nende usu ja tegude alusel. Need, kes surevad uskudes, et „Pole teist jumalust peale Jumala ja Muhammed on Jumala sõnumitooja (Prohvet)” ja on moslemid (Jumala tahtele alluvad), saavad sel päeval tasutud ja võetakse igaveseks vastu Paradiisi, nagu Jumal on öelnud:

{{ $page->verse('2:82') }}

Kuid need, kes surevad uskumata, et “Pole teist jumalust peale Jumala ja Muhammed on Jumala sõnumitooja (Prohvet)” või ei ole moslemid, kaotavad paradiisi igaveseks ja saadetakse Põrgutulle, nagu Jumal on öelnud:

{{ $page->verse('3:85') }}

Ja nagu Ta veel on öelnud:

{{ $page->verse('3:91') }}

Võidakse küsida: „Minu arvates on islam hea religioon, kuid kui ma pöördun islamisse, siis mu perekond, sõbrad ja teised inimesed kiusavad mind taga ja heidavad mu üle nalja. Kui ma ei pöördu islamisse, kas ma pääsen siis ikkagi Paradiisi ja mind päästetakse Põrgutulest?”

Vastuseks on see, mida Jumal eelmainitud värsis ütles, “Ja see, kes otsib religiooniks muud peale islami (Jumala tahtele allumise), seda ei võeta temalt vastu ja temast saab Teispoolsuses üks kaotajatest.”

Olles saatnud prohvet Muhammedi {{ $page->pbuh() }} inimesi islamisse kutsuma, ei võta Jumal neilt vastu enam ühtki teist religiooni peale islami. Jumal on meie Looja ja Alalhoidja. Ta lõi meile kõik, mis Maa peal leidub. Kõik õnnistus ja hea, mis meil on, on Temalt. Nii et kui keegi pärast kõike seda hülgab usu Jumalasse, tema prohvet Muhammedi {{ $page->pbuh() }}, või tema religiooni, islamisse, karistatakse teda selle eest teispoolsuses. Tegelikult on meie loomise peamine põhjus üksnes Jumala teenimine ja Temale kuuletumine, nagu Jumal on Pühas Koraanis öelnud (Koraan 51:56).

See elu, mida me täna elame, on väga lühike. Uskmatud arvavad Viimsel Kohtupäeval, et see elu, mida nad maa peal elasid, kestis vaid päeva või osa päevast, nagu Jumal on öelnud:

{{ $page->verse('23:112-113..') }}

Ja Ta on ka öelnud:

{{ $page->verse('23:115-116..') }}

Elu teispoolsuses on tõeline. See ei ole üksnes hingeline, vaid ka füüsiline. Me elame seal oma hingede ja kehadega.

Võrreldes seda maailma Teispoolsusega, ütles Muhammed {{ $page->pbuh() }} :

{{ $page->hadith('muslim:2858') }}

Teisisõnu, selle maailma väärtus võrreldes Teispoolsusega on nagu mõned piisad vett võrreldes merega.
