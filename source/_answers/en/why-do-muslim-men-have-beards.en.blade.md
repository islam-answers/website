---
extends: _layouts.answer
section: content
title: Why do Muslim men have beards?
date: 2024-12-29
description: Muslim men grow beards to follow Prophet Muhammad’s example, uphold their
  innate disposition and distinguish themselves from non-believers.
sources:
- href: https://www.islamweb.net/en/fatwa/81978/
  title: islamweb.net
- href: https://islamqa.info/en/answers/171299/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1189/
  title: islamqa.info
- href: https://islamqa.info/en/answers/75525/
  title: islamqa.info
---

The last and best of the Messengers, Muhammad {{ $page->pbuh() }} let his beard grow, as did the caliphs who came after him, and his companions and the leaders and common folk of the Muslims. This is the way of the Prophets and Messengers and their followers, and it is part of the fitrah (original predisposition) with which Allah created people. Aisha (may Allah be pleased with her) narrated that the Messenger of Allah {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:261a') }}

What is required of the believer, if Allah and His Messenger have enjoined something, is to say: We hear and obey, as Allah says:

{{ $page->verse('24:51') }}

Muslim men grow their beards in obedience to Allah and His Messenger. The Prophet {{ $page->pbuh() }} ordered the Muslims to let their beards grow freely and to trim their moustaches. It was narrated from Ibn ‘Umar that Prophet Muhammad {{ $page->pbuh() }} said:

{{ $page->hadith('bukhari:5892') }}

Al-Imam Ibn ‘Abd al-Barr said: “It is forbidden to shave the beard, and no one does this except men who are effeminate” i.e., those who imitate women. Jabir reports the following about Prophet Muhammad {{ $page->pbuh() }}:

{{ $page->hadith('muslim:2344b') }}

Shaykh al-Islam Ibn Taymiyah (may Allah have mercy on him) said:

> The Quran, Sunnah and ijma’ (scholarly consensus) all indicate that we must differ from the unbelievers in all aspects and not imitate them, because imitating them on the outside will make us imitate them in their bad deeds and habits, and even in beliefs. (...)
