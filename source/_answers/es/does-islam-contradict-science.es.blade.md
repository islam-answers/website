---
extends: _layouts.answer
section: content
title: ¿Contradice el Islam a la ciencia?
date: 2024-10-09
description: El Corán contiene hechos científicos que sólo se han descubierto recientemente
  gracias al avance de la tecnología y el conocimiento científico.
sources:
- href: https://islamicpamphlets.com/science-in-islam
  title: islamicpamphlets.com
translator_id: 573109
---

El Corán, el libro del Islam, es el último libro de revelación de Dios a la humanidad y el último en la línea de revelaciones dadas a los Profetas.

Aunque el Corán (revelado hace más de 1400 años) no es principalmente un libro de ciencia, contiene hechos científicos que sólo se han descubierto recientemente gracias al avance de la tecnología y el conocimiento científico. El Islam fomenta la reflexión y la investigación científica porque la comprensión de la naturaleza de la creación permite a las personas apreciar más a su Creador y el alcance de Su poder y sabiduría.

El Corán fue revelado en una época en que la ciencia era primitiva; no había telescopios, microscopios ni nada parecido a la tecnología actual. La gente creía que el Sol orbitaba alrededor de la Tierra y que el cielo estaba sostenido por pilares en las esquinas de una Tierra plana. En este contexto se reveló el Corán, que contiene muchos datos científicos sobre temas que van de la astronomía a la biología, de la geología a la zoología.

Algunos de los muchos hechos científicos que se encuentran en el Corán incluyen:

### Hecho #1 - Origen de la vida

{{ $page->verse('21:30') }}

Se señala al agua como el origen de toda la vida. Todos los seres vivos están formados por células y ahora sabemos que las células están formadas principalmente por agua. Esto sólo se descubrió tras la invención del microscopio. En los desiertos de Arabia, sería inconcebible pensar que alguien hubiera adivinado que toda la vida procedía del agua.

### Hecho #2 - Desarrollo embrionario humano

Dios habla de las etapas del desarrollo embrionario del hombre:

{{ $page->verse('23:12-14') }}

La palabra árabe "alaqah" tiene tres significados: sanguijuela, cosa suspendida y coágulo de sangre. "Mudghah" significa sustancia masticada. Los científicos especializados en embriología han observado que el uso de estos términos para describir la formación del embrión es exacto y conforme a nuestra comprensión científica actual del proceso de desarrollo.

Hasta el siglo XX se sabía poco sobre la estadificación y clasificación de los embriones humanos, lo que significa que las descripciones del embrión humano en el Corán no pueden basarse en conocimientos científicos del siglo VII.

### Hecho #3 - Expansión del Universo

En una época en que la ciencia de la astronomía era aún primitiva, se reveló el siguiente versículo del Corán:

{{ $page->verse('51:47') }}

Uno de los significados previstos del versículo anterior es que Dios está expandiendo el universo (es decir, los cielos). Otro significado es que Dios provee al universo y tiene poder sobre él, lo cual también es cierto.

El hecho de que el universo se está expandiendo (por ejemplo, los planetas se alejan unos de otros) se descubrió en el siglo pasado. El físico Stephen Hawking, en su libro "Breve historia del tiempo", escribe: "El descubrimiento de que el universo se expande fue una de las grandes revoluciones intelectuales del siglo XX".

¡El Corán alude a la expansión del universo incluso antes de la invención del telescopio!

### Hecho #4 - Hierro Enviado

El hierro no es natural de la Tierra, ya que llegó a este planeta desde el espacio exterior. Los científicos han descubierto que hace miles de millones de años la Tierra fue golpeada por meteoritos que transportaban hierro de estrellas lejanas que habían explotado.

{{ $page->verse('..57:25..') }}

Dios usa la palabra 'enviado'. El hecho de que el hierro fue enviado a la tierra desde el espacio exterior es algo que no podía ser conocido por la ciencia primitiva del siglo VII.

### Dato #5 – La protección del cielo

El cielo desempeña un papel crucial en la protección de la Tierra y sus habitantes de los rayos letales del sol, así como del frío glacial del espacio.

Dios nos pide que consideremos el cielo en el versículo siguiente:

{{ $page->verse('21:32') }}

El Corán señala la protección del cielo como signo de Dios, propiedades protectoras que fueron descubiertas por investigaciones científicas realizadas en el siglo XX.

### Dato #6 – Las montañas

Dios llama nuestra atención sobre una característica importante de las montañas:

{{ $page->verse('78:6-7') }}

El Corán describe con precisión las profundas raíces de las montañas utilizando la palabra "clavijas". El monte Everest, por ejemplo, tiene una altura aproximada de 9km sobre el suelo, ¡mientras que su raíz está a más de 125 km de profundidad!

El hecho de que las montañas tengan raíces profundas en forma de "clavija" no se conoció hasta que se desarrolló la teoría de la tectónica de placas a principios del siglo XX. Dios también dice en el Corán (16:15) que las montañas desempeñan un papel en la estabilización de la Tierra "...para que no tiemble", algo que los científicos acaban de empezar a comprender.

### Dato #7: La órbita del Sol

En 1512, el astrónomo Nicolás Copérnico propuso su teoría de que el Sol está inmóvil en el centro del sistema solar y los planetas giran a su alrededor. Esta creencia estuvo muy extendida entre los astrónomos hasta el siglo XX. Hoy es un hecho bien establecido que el Sol no está inmóvil, sino que se mueve en una órbita alrededor del centro de nuestra galaxia, la Vía Láctea.

{{ $page->verse('21:33') }}

### Hecho #8 - Ondas internas en el océano

Se creía que las olas sólo se producían en la superficie del océano. Sin embargo, los oceanógrafos han descubierto que existen ondas internas que tienen lugar bajo la superficie y que son invisibles para el ojo humano y sólo pueden detectarse con equipos especializados.

El Corán menciona:

{{ $page->verse('24:40..') }}

Esta descripción es asombrosa porque hace 1.400 años no existían equipos especializados para descubrir las ondas internas en el interior de los océanos.

### Dato #9 – Mentiras y movimiento

Había un líder tribal cruel y opresivo que vivió durante la época del Profeta Muhammad {{ $page->pbuh() }}. Dios le reveló un versículo para advertirle:

{{ $page->verse('96:15-16') }}

Dios no llama mentirosa a esta persona, sino que llama a su frente (la parte frontal del cerebro) 'mentirosa' y 'pecadora', y le advierte que deje de hacerlo. Numerosos estudios han descubierto que la parte frontal de nuestro cerebro (lóbulo frontal) es responsable tanto de la mentira como del movimiento voluntario y, por tanto, del pecado. Estas funciones se descubrieron con equipos médicos de diagnóstico por imagen que se desarrollaron en el siglo XX.

### Hecho #10 - Los dos mares que no se mezclan

Con respecto a los mares, nuestro Creador dice:

{{ $page->verse('55:19-20') }}

Una fuerza física llamada tensión superficial impide que las aguas de mares vecinos se mezclen, debido a la diferencia de densidad de estas aguas. Es como si hubiera una delgada pared entre ellas. Esto ha sido descubierto muy recientemente por los oceanógrafos.

### ¿No pudo Muhammad haber escrito el Corán?

El Profeta Muhammad {{ $page->pbuh() }} era conocido en la historia por ser analfabeto; no sabía leer ni escribir y no tenía formación en ningún campo que pudiera explicar las precisiones científicas del Corán.

Algunos afirman que lo copió de sabios o científicos de su época. Si lo hubiera copiado, habríamos esperado ver copiadas también todas las suposiciones científicas incorrectas de la época. Sin embargo, el Corán no contiene ningún error, ni científico ni de otro tipo.

Algunas personas también afirman que el Corán fue modificado a medida que se descubrieron nuevos hechos científicos. Esto no puede ser así porque es un hecho históricamente documentado que el Corán se conserva en su idioma original, lo que es un milagro en sí mismo.

### ¿Sólo una coincidencia?

{{ $page->verse('41:53') }}

Aunque esta respuesta sólo se ha centrado en los milagros científicos, hay muchos más tipos de milagros mencionados en el Corán: milagros históricos; profecías que se han hecho realidad; estilos lingüísticos y literarios inigualables; por no hablar del efecto conmovedor que tiene en la gente. Todos estos milagros no pueden deberse a la casualidad. Indican claramente que el Corán procede de Dios, el Creador de todas estas leyes de la ciencia. Él es el mismo Dios que envió a todos los Profetas con el mismo mensaje: adorar al único Dios y seguir las enseñanzas de Su Mensajero.

El Corán es un libro de guía que demuestra que Dios no creó a los seres humanos para que simplemente vagaran sin rumbo. Por el contrario, nos enseña que tenemos un propósito significativo y superior en la vida: reconocer la completa perfección, grandeza y singularidad de Dios, y obedecerle.

Depende de cada persona utilizar el intelecto y la capacidad de razonamiento que Dios le ha dado para contemplar y reconocer los signos de Dios, siendo el Corán el signo más importante. Lee y descubre la belleza y la verdad del Corán, ¡para que puedas alcanzar el éxito!
