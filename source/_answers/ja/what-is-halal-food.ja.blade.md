---
extends: _layouts.answer
section: content
title: ハラールフードとは何か？
date: 2024-08-22
description: 「ハラールフード」はイスラム教で神様によって「食べてもよい」ということを指します。その主な例外は豚肉とアルコールとみなされています。
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 587748
---

「ハラール」という言葉は神様によって「許されたもの」を意味するアラビア語から誘導されました。普通には、豚肉とアルコール以外、ほとんどの飲食物はハラールとみなされています。お肉の場合には、牛、羊、鶏などのような動物を食べるには、屠殺時に「アッラーの御名において（ビスミッラー）」と唱え、イスラーム法の決まりに適合して苦しまない方法で屠殺する。
