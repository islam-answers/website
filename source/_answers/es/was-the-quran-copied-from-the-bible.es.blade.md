---
extends: _layouts.answer
section: content
title: ¿El Corán fue copiado de la Biblia?
date: 2024-11-04
description: El Corán no fue copiado de la Biblia. El Profeta Muhammad era analfabeto, no había una Biblia en árabe y las revelaciones venían directamente de Allah.
sources:
- href: https://islamqa.org/hanafi/askimam/18140/
  title: islamqa.org (AskImam.org)
- href: https://islamqa.info/en/answers/487612/
  title: islamqa.info
translator_id: 573109
---

Allah creó todo lo que nos rodea con un propósito. El hombre también ha sido creado con un propósito y es adorar a Allah. Allah dice en el Corán:

{{ $page->verse('51:56') }}

Para guiar a la humanidad, Allah envió profetas de vez en cuando. Estos profetas recibieron la revelación de Allah y cada religión posee su propia colección de escrituras divinas, que constituyen el fundamento de sus creencias. Estas escrituras son la transcripción material de la revelación divina, directamente como en el caso del profeta Moisés (Musa), que recibió los mandamientos de Allah, o indirectamente como en el caso de Jesús (Isa) y Muhammad {{ $page->pbuh() }}, que transmitieron al pueblo la revelación que les comunicó el ángel Gabriel (Jibreel).

El Corán ordena a todos los musulmanes que crean en las escrituras que lo preceden. Sin embargo, como musulmanes creemos que la Torá (Tawrah), el Libro de los Salmos (Zaboor) y el Evangelio (Injeel) en su forma original, es decir, tal como fueron revelados, son la palabra de Allah.

## ¿Podría el profeta Muhammad haber copiado el Corán de la Biblia?

La discusión de cómo el Profeta Muhammad {{ $page->pbuh() }} pudo haber aprendido las historias de la Gente del Libro, y luego transmitirlas en el Corán - lo que significa que no fue revelación- es una discusión de varias afirmaciones en las que él {{ $page->pbuh() }} pudo haber aprendido las historias directamente de sus libros sagrados, o aprendido lo que las escrituras contenían de historias, mandamientos y prohibiciones, verbalmente de la Gente del Libro, si no le fue posible estudiar sus libros por sí mismo.

Estas reivindicaciones pueden resumirse en tres afirmaciones básicas:

1. La afirmación de que el Profeta Muhammad {{ $page->pbuh() }} no era iletrado ni analfabeto
1. La afirmación de que las escrituras cristianas estaban a disposición del Profeta {{ $page->pbuh() }} y podía citarlas o copiarlas
1. La afirmación de que La Meca era un importante centro educativo para el estudio de las escrituras

En primer lugar, el texto del Corán y la Sunnah en muchos lugares indican claramente, sin dejar lugar a dudas, que el Profeta Muhammad {{ $page->pbuh() }} era realmente iletrado y que nunca narró ninguna de las historias de la Gente del Libro antes de que le llegara la revelación, y nunca escribió nada de su puño y letra. No sabía nada de las historias antes de que comenzara su misión, y nunca habló de ellas antes de eso.

Allah, exaltado sea, dice:

{{ $page->verse('29:48') }}

Los libros de la biografía y la historia del Profeta no mencionan que el Profeta escribiera la revelación, o que él mismo escribiera sus cartas a los reyes. Más bien se menciona lo contrario; el Profeta Muhammad {{ $page->pbuh() }} tenía escribas que escribían la revelación y otras cosas. En un brillante texto de Ibn Jaldún, hay una descripción del nivel de educación en la Península Arábiga justo antes de que comenzara la misión del Profeta. Dice:

> Entre los árabes, la habilidad de escribir era más rara que los huevos de las camellas. La mayoría de la gente era analfabeta, sobre todo los habitantes del desierto, porque se trata de una habilidad que suele encontrarse entre los habitantes de las ciudades.

De ahí que los árabes no se refirieran a la persona analfabeta como analfabeta; más bien describirían a quien sabía leer y escribir como conocedor, porque saber leer y escribir era la excepción, no la norma entre la gente.

En segundo lugar, no hay pruebas, indicios, ni sugerencias de que existiera ninguna traducción árabe de la Biblia en aquella época. Al-Bujari narró de Abu Hurayrah (que Allah esté complacido con él) que dijo:

{{ $page->hadith('bukhari:7362') }}

Este hadiz indica que los judíos tenían el monopolio total del texto y su explicación. Si sus textos se conocieran en árabe, no habría habido necesidad de que los judíos leyeran el texto o lo explicaran [a los musulmanes].

Esta idea se apoya en el versículo en el que Allah, exaltado sea, dice:

{{ $page->verse('3:78') }}

Quizá el libro más importante escrito sobre este tema sea el de Bruce Metzger, profesor de Lengua y Literatura del Nuevo Testamento, The Bible in Translation (La Biblia traducida):

> Lo más probable es que la traducción árabe más antigua de la Biblia se remonte al siglo VIII.

El orientalista Thomas Patrick Hughes escribió:

> No hay pruebas de que Muhammad hubiera leído las Escrituras cristianas... Hay que señalar que no hay pruebas claras que sugieran que existiera ninguna traducción al árabe del Antiguo y Nuevo Testamento antes de la época de Muhammad.

En tercer lugar, la idea de que el Profeta adquirió la revelación de la gente es una idea antigua. Los politeístas lo acusaron de eso, y el Corán los refutó, como dice Allah, exaltado sea:

{{ $page->verse('25:5-6') }}

Además, la afirmación de que el Profeta Muhammad {{ $page->pbuh() }} adquirió conocimientos de la Gente del Libro cuando estuvo en La Meca es una afirmación que queda desmentida por el estatus académico y cultural de La Meca y la verdadera naturaleza de la conexión que esta sociedad árabe tenía con los conocimientos de la Gente del Libro.

Por lo tanto, después de probar que el Profeta Muhammad {{ $page->pbuh() }} no aprendió el conocimiento de la Gente del Libro de ellos directamente o a través de un intermediario, no nos queda otra opción que afirmar que fue revelación de Allah a Su Profeta elegido.
