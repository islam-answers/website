---
extends: _layouts.answer
section: content
title: Why did Allah send Prophet Muhammad?
date: 2025-02-05
description: Allah sent Prophet Muhammad to restore true monotheism, clarify religious
  disputes, and guide people to righteousness.
sources:
- href: https://islamqa.info/en/answers/11575/
  title: islamqa.info
- href: https://islamqa.info/en/answers/13957/
  title: islamqa.info
- href: https://islamqa.info/en/answers/10468/
  title: islamqa.info
- href: https://islamqa.info/en/answers/2114/
  title: islamqa.info
---

Prophets are the Messengers of Allah to His slaves; they convey His commands, and give them glad tidings of the delights that Allah has promised them if they obey His commands, and they warn them of the abiding punishment if they go against His prohibitions. They tell them the stories of the past nations and the punishment and vengeance that befell them in this world because they disobeyed the commands of their Lord.

These divine commands and prohibitions cannot be known through independent thinking, hence Allah has prescribed laws and enjoined commands and prohibitions, to honor mankind and protect their interests, because people may follow their desires and thus transgress the limits and abuse people and deprive them of their rights.

### Why did Allah send prophets and messengers?

By His wisdom Allah sent among mankind from time to time messengers to remind them of the commands of Allah and to warn them against falling into sin, to preach to them and to tell them the stories of those who came before them. For when people hear wondrous stories it makes their minds alert, so they understand and increase in knowledge and understand correctly. For the more people hear, the more ideas they will have; the more ideas they have, the more they will think; the more they think, the more they will know; and the more they know, the more they will do. So there is no alternative to sending messengers in order to explain the truth.

The need of the people of the earth for the messengers is not like their need for the sun, moon, wind and rain, or like a man’s need for his life, or like the eye’s need for light, or like the body’s need for food and drink. Rather it is greater than that, greater than his need for anything you could think of. The messengers (peace and blessings of Allah be upon them) are intermediaries between Allah and His creation, conveying His commands and prohibitions. They are ambassadors from Him to His slaves. The last and greatest of them, the noblest before his Lord, was Muhammad {{ $page->pbuh() }}. Allah sent him as a mercy to the worlds, guidance for those who want to draw closer to Allah, proof which left no excuse for all people.

The Prophets and Messengers were many, and no one knows their number except Allah. Among them are those of whom Allah has told us, and some of whom He has not told us. Allah says:

{{ $page->verse('4:164..') }}

### Why did Allah send Prophet Muhammad?

The Message of the Prophet {{ $page->pbuh() }} was not unique to him, but was in nature and content similar to the message brought by other Messengers before him. Allah had sent Prophets and Messengers, such as Moosa (Moses), ‘Eesa (Jesus) and others, to the Children of Israel, and great numbers had believed in them and borne witness to the truth of their Books, which were similar in general terms to the message brought by the Quran. This was eloquent testimony to the truth of the Message with which he was sent, especially as it belonged to the same type of Message to whose truth they had testified.

When Allah sent Muhammad {{ $page->pbuh() }} with the same Message as the Prophets who had come before him, the Quran came to confirm their Books and their Prophethood, and to call people to believe in them. So when the People of the Book disbelieved in him and his Book, it meant that they were disbelieving in their own Books and Messengers. As the Quran contained the same principles as their books, and confirmed them, this meant that it was the least likely to be fabricated or to have come from a source other than Allah, because all of them came from Allah, may He be exalted.

Differences and disputes arose among the Children of Israel. They introduced alterations and changes in their beliefs and laws. Thus truth was extinguished and falsehood prevailed, oppression and evil became widespread, and people needed a religion that would establish truth, destroy evil and guide people to the straight path, therefore Allah sent Muhammad {{ $page->pbuh() }} as Allah said:

{{ $page->verse('16:64') }}

Allah sent all the Prophets and Messengers to call for the worship of Allah alone, and to bring people from darkness to light. The first of these Messengers was Nuh and the last of them was Muhammad {{ $page->pbuh() }} as Allah said:

{{ $page->verse('16:36..') }}
