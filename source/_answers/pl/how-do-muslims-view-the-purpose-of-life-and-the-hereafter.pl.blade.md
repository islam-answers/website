---
extends: _layouts.answer
section: content
title: Jak muzułmanie postrzegają cel życia i zaświaty?
date: 2024-10-07
description: Islam naucza, że celem życia jest oddawanie czci Bogu i że istoty ludzkie
  będą sądzone przez Boga w zaświatach.
sources:
- href: https://spubs.com/sps/sp.cfm?subsecID=NMM01&articleID=NMM010003&articlePages=4
  title: spubs.com
translator_id: 584496
---

W Świętym Koranie Bóg naucza istoty ludzkie, że zostały stworzone w celu oddawania Mu czci i że podstawą wszelkiego prawdziwego kultu jest świadomość Boga. Ponieważ nauki islamu obejmują wszystkie aspekty życia i etyki, świadomość Boga jest zalecana we wszystkich ludzkich sprawach.

Islam wyjaśnia, że wszystkie ludzkie czyny są aktami czci, jeśli są wykonywane wyłącznie dla Boga i zgodnie z Jego Boskim Prawem. Jako taki, kult w islamie nie ogranicza się do rytuałów religijnych. Nauki islamu działają jak miłosierdzie i uzdrowienie dla ludzkiej duszy, a takie cechy jak pokora, szczerość, cierpliwość i dobroczynność są silnie wspierane. Ponadto islam potępia pychę i zadufanie w sobie, ponieważ Bóg Wszechmogący jest jedynym sędzią ludzkiej prawości.

Islamski pogląd na naturę człowieka jest również realistyczny i wyważony. Uważa się, że istoty ludzkie nie są z natury grzeszne, lecz w równym stopniu zdolne do czynienia dobra i zła. Islam naucza również, że wiara i działanie idą w parze. Bóg dał ludziom wolną wolę, a miarą ich wiary są czyny i działania. Jednak istoty ludzkie zostały również stworzone jako słabe i regularnie popadają w grzech.

Taka jest natura człowieka stworzonego przez Boga w Jego Mądrości i nie jest ona z natury „zepsuta” lub wymagająca naprawy. Dzieje się tak, ponieważ droga pokuty jest zawsze otwarta dla wszystkich istot ludzkich, a Bóg Wszechmogący kocha skruszonego grzesznika bardziej niż tego, który w ogóle nie grzeszy.

Prawdziwą równowagę w islamskim życiu zapewnia zdrowa bojaźń boża oraz szczera wiara w Jego nieskończone miłosierdzie. Życie bez strachu przed Bogiem prowadzi do grzechu i nieposłuszeństwa, podczas gdy wiara w to, że zgrzeszyliśmy tak bardzo, że Bóg nie może nam wybaczyć, prowadzi jedynie do rozpaczy. W związku z tym islam naucza, że tylko ci, którzy są w błędzie, rozpaczają z powodu Miłosierdzia swego Pana.

{{ $page->verse('39:53') }}

Ponadto Święty Koran, który został objawiony Prorokowi Muhammadowi {{ $page->pbuh() }}, zawiera wiele nauk na temat życia w zaświatach i Dnia Sądu Ostatecznego. Z tego powodu muzułmanie wierzą, że wszystkie istoty ludzkie zostaną ostatecznie osądzone przez Boga za swoje przekonania i czyny w ziemskim życiu.

Sądząc istoty ludzkie, Bóg Wszechmogący będzie zarówno Miłosierny, jak i Sprawiedliwy, a ludzie będą sądzeni tylko za to, do czego byli zdolni. Wystarczy powiedzieć, że islam naucza, że życie jest próbą i że wszyscy ludzie będą odpowiedzialni przed Bogiem. Szczera wiara w życie pozagrobowe jest kluczem do prowadzenia zrównoważonego życia i moralności. W przeciwnym razie życie jest postrzegane jako cel sam w sobie, co powoduje, że ludzie stają się bardziej samolubni, materialistyczni i niemoralni.
