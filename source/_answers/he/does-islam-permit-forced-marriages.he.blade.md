---
extends: _layouts.answer
section: content
title: האם האסלאם מתיר נישואים בכפייה?
date: 2024-10-10
description: לגברים ולנקבות יש את הזכות לבחור את בן זוגם, ונישואים נחשבים בטלים אם
  לא ניתן אישור אמיתי של אישה.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
translator_id: 593514
---

נישואים מסודרים הם פרקטיקות תרבותיות שרווחות במדינות מסוימות ברחבי העולם. למרות שאינם מוגבלים למוסלמים, נישואים בכפייה הפכו קשורים באופן שגוי לאסלאם.

באסלאם, גם לגברים וגם לנשים יש את הזכות לבחור או לדחות את בן זוגם הפוטנציאלי, ונישואים נחשבים בטלים אם לא ניתן אישור אמיתי של אישה לפני הנישואין.
