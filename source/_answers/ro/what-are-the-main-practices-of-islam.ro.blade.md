---
extends: _layouts.answer
section: content
title: Care sunt principalele practici ale Islamului?
date: 2024-03-23
description: Principalele practici ale Islamului (cei cinci piloni ai credinței) sunt
  Mărturisirea credinței, Rugăciunile, Caritatea prescrisă, Postul și Pelerinajul.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
translator_id: 554943
---

Principalele practici ale Islamului sunt cunoscute drept cei cinci piloni ai credinței.

### Prima practică: Mărturisirea credinței

Înseamnă a declara faptul că nu există niciun alt Dumnezeu demn de închinare în afară de Allah, iar Muhammad este ultimul Său mesager.

### A doua practică: rugăciunile

Rugăciunile se efectuează de cinci ori pe zi: în zori, la prânz, la mijlocul după-amiezii, după apus și noaptea.

### A treia practică: Caritatea prescrisă ("zakat")

Aceasta este o caritate anuală obligatorie oferită celor mai puțin norocoși și este calculată ca o mică parte din economiile anuale totale ale cuiva. Această sumă este de 2,5% din averea unei persoane și este plătită de cei care au exces financiar și au capacitatea așadar să ajute pe cei nevoiași.

### A patra practică: Postul în luna Ramadanului

Pe parcursul acestei luni, musulmanii trebuie să se abțină de la orice mâncare, băutură și relații sexuale cu soții/soțiile lor, din zori până la apus. Această practică promovează cumpătării, conștiința și frica lui Dumnezeu, precum și empatia față de cei săraci.

### A cincea practică: Pelerinajul.

Fiecărui musulman capabil i se cere să facă pelerinajul la Mecca cel puțin o dată în timpul vieții. Pelerinajul implică rugăciune, caritate, călătorie și supunere lui Dumnezeu, și este o experiență foarte spirituală.
