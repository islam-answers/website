---
extends: _layouts.answer
section: content
title: Was sagt der Islam zur Evolution?
date: 2024-08-11
description: Gott erschuf den ersten Menschen, Adam, in seiner vollendeten Form. Über
  andere Lebewesen als den Menschen äußern sich die islamischen Quellen jedoch nicht.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
translator_id: 587746
---

Gott erschuf den ersten Menschen, Adam, in seiner endgültigen Form – im Gegensatz zu einer allmählichen Entwicklung aus fortgeschrittenen Affen. Dies kann von der Wissenschaft weder direkt bestätigt noch widerlegt werden, da es sich um ein einzigartiges und singuläres historisches Ereignis handelte – ein Wunder.

Was andere Lebewesen als den Menschen betrifft, so schweigen die islamischen Quellen zu dieser Angelegenheit und verlangen von uns lediglich den Glauben, dass Gott sie auf die von ihm gewünschte Weise erschaffen hat, sei es durch die Evolution oder auf andere Weise.
