---
extends: _layouts.answer
section: content
title: What is Jihad?
date: 2024-03-23
description: The essence of Jihad is to struggle and sacrifice for one’s religion
  in a manner which is pleasing to God.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

The essence of Jihad is to struggle and sacrifice for one’s religion in a manner which is pleasing to God. Linguistically, it means to “struggle” and can refer to striving to do good deeds, giving charity, or fighting for the sake of God.

The most commonly known form is the military Jihad which is permitted in order to preserve the well being of society, to prevent oppression from spreading, and to promote justice.
