---
extends: _layouts.answer
section: content
title: Qu’est-ce que l’Islam ?
date: 2024-07-23
description: L’Islam signifie se soumettre, obéir aux commandements, adorer Allah seul et avoir foi en Lui sans objection.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 586055
---

Si vous vous référez aux dictionnaires de langue arabe, vous constaterez que le sens du mot islam est : la soumission, l'humilité, l'obéissance aux ordres et tenir compte des interdits sans objection, l'adoration sincère d'Allah seul, la croyance en ce qu'Il nous dit et la foi en Lui.

Le mot islam est devenu le nom de la religion apportée par le prophète Muhammad {{ $page->pbuh() }}.

### Pourquoi cette religion s'appelle Islam ?

Toutes les religions sur Terre portent différents noms : soit le nom d'un homme, soit celui d'une nation. Ainsi, le christianisme tire son nom du Christ ; le bouddhisme vient de son fondateur Bouddha ; de même que le judaïsme tire son nom d'une tribu appelée Yehudah (Juda), ce qui lui a valu le nom de judaïsme, et ainsi de suite...

Cependant l’Islam est une exception car son nom ne vient pas d’un homme ou d’une nation spécifique, mais il fait plutôt référence à la signification et à l’essence du mot Islam. Ce que ce nom indique est que l'établissement de cette religion et son rayonnement sur Terre n'ont pas été l'œuvre d'un homme en particulier, et qu'elle n'est pas réservée à une nation particulière à l'exclusion de toutes les autres. Son objectif est plutôt de diffuser les attributs qu’implique le mot Islam à tous les peuples sur Terre. Ainsi, quiconque acquiert ces valeurs, qu'il soit du passé ou du présent, est musulman, et quiconque s’en imprègnera dans le futur sera également musulman.

L'islam est la religion de tous les Prophètes. Elle est apparue au début de la prophétie, à l'époque de notre père Adam et tous les messages appelaient à l'Islam (à la soumission à Allah) en termes de croyance et de règles de base. Les croyants qui ont suivi les prophètes précédents étaient tous musulmans au sens général, et ils entreront au paradis en vertu de leur Islam.
