---
extends: _layouts.answer
section: content
title: Cine poate deveni musulman?
date: 2024-09-03
description: Orice om poate deveni musulman, indiferent de religia sa anterioară,
  vârstă, naționalitate sau etnie.
sources:
- href: https://islamqa.info/en/answers/4246
  title: islamqa.info
- href: https://islamqa.info/en/answers/373188
  title: islamqa.info
translator_id: 554943
---

Orice om poate deveni musulman, indiferent de religia sa anterioară, vârstă, naționalitate sau etnie. Pentru a deveni musulman, trebuie doar să rostești următoarele cuvinte: "Ash hadu alla ilaha illa Allah, wa ash hadu anna Mohammadan rasulAllah". Această mărturie de credință înseamnă: "Mărturisesc că nu există alt zeu demn de a fi adorat în afară de Allah și că Mohammad este Trimisul Său". Trebuie să le spui și să crezi în aceste cuvinte.

Odată ce spui această propoziție, devii automat musulman. Nu trebuie să cauți un anumit moment sau o oră, fie noaptea sau ziua, pentru a te întoarce către Allah și a începe noua viață. Oricând este momentul potrivit pentru asta.

Nu ai nevoie de permisiunea nimănui, sau de un preot care să te convertească, sau de un intermediar care să medieze pentru tine, sau de cineva care să te călăuzească spre Allah, pentru că El e cel care te călăuzește. Întoarce-te la Allah, căci El este aproape; este mai aproape de tine decât crezi sau îți poți imagina:

{{ $page->verse('2:186') }}

Allah, fie ca El să fie înălțat, spune:

{{ $page->verse('6:125..') }}
