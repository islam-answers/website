<ul>
    @php ($contributors = collect($page->l10n['team'])->sortBy('name'))
    @foreach ($contributors as $contributor)
    <li>
        <span class="text-blue-600 font-semibold">{{ $contributor['name'] }}</span>
    </li>
    @endforeach
</ul>