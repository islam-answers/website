---
extends: _layouts.answer
section: content
title: What do Muslims believe about Jesus?
date: 2024-02-12
description: Muslims respect and revere Jesus. They consider him one of the greatest
  of God’s messengers to mankind.
sources:
- href: https://www.islam-guide.com/ch3-10.htm
  title: islam-guide.com
---

Muslims respect and revere Jesus {{ $page->pbuh() }}. They consider him one of the greatest of God’s messengers to mankind. The Quran confirms his virgin birth, and a chapter of the Quran is entitled ‘Maryam’ (Mary). The Quran describes the birth of Jesus as follows:

{{ $page->verse('3:45-47') }}

Jesus was born miraculously by the command of God, the same command that had brought Adam into being with neither a father nor a mother. God has said:

{{ $page->verse('3:59') }}

During his prophetic mission, Jesus performed many miracles. God tells us that Jesus said:

{{ $page->verse('3:49..') }}

Muslims believe that Jesus was not crucified. It was the plan of Jesus’ enemies to crucify him, but God saved him and raised him up to Him. And the likeness of Jesus was put over another man. Jesus’ enemies took this man and crucified him, thinking that he was Jesus. God has said:

{{ $page->verse('..4:157..') }}

Neither Prophet Muhammad {{ $page->pbuh() }} nor Jesus came to change the basic doctrine of the belief in one God, brought by earlier prophets, but rather to confirm and renew it.
