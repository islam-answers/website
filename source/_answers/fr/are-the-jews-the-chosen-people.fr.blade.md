---
extends: _layouts.answer
section: content
title: Les Juifs sont-ils le peuple élu ?
date: 2024-12-23
description: Les Juifs ont été choisis pour leurs prophètes, mais ont perdu la faveur
  en raison du rejet, la meilleure nation devenant les disciples du prophète Mohammed.
sources:
- href: https://www.islamweb.net/en/fatwa/91584/
  title: islamweb.net
- href: https://islamqa.org/hanafi/fatwaa-dot-com/76468/
  title: islamqa.org (Fatwaa.com)
- href: https://islamqa.info/en/answers/9905/
  title: islamqa.info
- href: https://www.islamweb.net/en/fatwa/15096/
  title: islamweb.net
translator_id: 586055
---

Le Coran atteste qu'Allah a choisi les enfants d'Israël (Israël étant le prophète Ya'qoob / Jacob {{ $page->pbuh() }}) à leur époque, et qu'Il les a préférés à tous les autres peuples de leur époque. Le Coran contient de nombreux versets à ce sujet.

Cependant, ils n’ont pas été choisis en raison de leur race ou de leur couleur, mais en raison du grand nombre de prophètes qui leur ont été envoyés. Allah favorise toujours ceux qui croient en Lui et font de bonnes œuvres, quelle que soit leur couleur, leur race ou leur nationalité. Allah dit dans le Coran :

{{ $page->verse('16:97') }}

Le prophète Muhammad {{ $page->pbuh() }} a dit :

{{ $page->hadith('muslim:1842') }}

Allah dit dans le Coran :

{{ $page->verse('5:20') }}

Allah dit encore :

{{ $page->verse('44:32') }}

Cependant, ils n'ont pas été reconnaissants de ce bienfait et ont nié ce que les prophètes leur ont apporté. Ils ont donc mérité la colère et la malédiction d'Allah. Ils ont été humiliés [par Allah] partout où ils ont été rattrapés, sauf par un pacte d'Allah et un traité des gens, et Allah a fait d'eux des singes et des porcs.

Du vivant du Prophète Muhammad {{ $page->pbuh() }}, les Juifs ont toujours prétendu qu'ils étaient les élus d'Allah. Allah a révélé le verset suivant en réponse à leur affirmation :

{{ $page->verse('5:18..') }}

En conclusion, les Juifs étaient préférés aux autres peuples à l'époque de leurs prophètes, car ils leur obéissaient parfois, mais la plupart du temps ils les démentaient et les tuaient même. Allah dit dans le Coran :

{{ $page->verse('..2:87') }}

Cependant, lorsqu'Allah a envoyé le prophète Muhammad {{ $page->pbuh() }} comme dernier messager, Allah a considéré que les meilleures personnes étaient celles qui le suivaient. Allah dit dans le Coran :

{{ $page->verse('3:110') }}

Allah dit encore :

{{ $page->verse('2:143..') }}

Etant donné la situation, les Juifs de l'époque du Prophète Muhammad {{ $page->pbuh() }} n'étaient pas meilleurs avec lui qu'ils ne l'étaient avec les Prophètes précédents, qu'Allah les exalte ; au contraire, ils le traitaient de menteur, lui faisaient du mal et complotaient même pour le tuer.
