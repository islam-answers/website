---
extends: _layouts.answer
section: content
title: What are the main practices of Islam?
date: 2024-03-23
description: The main practices of Islam are referred to as the five pillars. They
  include the Testimony of Faith, Prayers, Prescribed Charity, Fasting, and Pilgrimage.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/about-islam-brief-introduction
  title: islamicpamphlets.com
---

The main practices of Islam are referred to as the five pillars.

### First Practice: The Testimony of Faith

To declare that there is no God worthy of worship except Allah, and Muhammad is His final Messenger.

### Second Practice: Prayers

To be performed five times daily: once each at dawn, noon, mid-afternoon, after sunset and at night.

### Third Practice: Prescribed Charity "Zakat"

This is an annual compulsory charity paid to those less fortunate and is calculated as a small portion of one’s total annual savings, which includes 2.5% of monetary wealth and may include other assets. It is paid by those who have excess wealth.

### Fourth Practice: Fasting the Month of Ramadan

Throughout this month, Muslims must refrain from all food, drink and sexual relations with their spouses, from dawn to sunset. It promotes self-restraint, consciousness of God and empathy to the poor.

### Fifth Practice: The Pilgrimage.

Every able Muslim is required to make the pilgrimage to Mecca in their lifetime. It involves prayer, supplication, charity and travelling, and is a very humbling and spiritual experience.
