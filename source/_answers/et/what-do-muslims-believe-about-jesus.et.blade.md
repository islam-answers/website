---
extends: _layouts.answer
section: content
title: Mida usuvad moslemid Jeesuse kohta?
date: 2024-06-08
description: Moslemid peavad Jeesusest lugu ja austavad teda. Nad peavad teda inimkonnale
  saadetud Jumala sõnumitoojatest üheks suuremaks.
sources:
- href: https://www.islam-guide.com/ee/ch3-10.htm
  title: islam-guide.com
---

Moslemid peavad Jeesusest lugu ja austavad teda {{ $page->pbuh() }}. Nad peavad teda inimkonnale saadetud Jumala sõnumitoojatest üheks suuremaks. Koraan kinnitab tema sündi neitsist ja Koraani üks peatükk kannab koguni pealkirja „Maryam” (Maarja). Koraan kirjeldab Jeesuse sündi järgnevalt:

{{ $page->verse('3:45-47') }}

Oma prohvetliku missiooni ajal tegi Jeesus palju imesid. Jumal ütleb meile, et Jeesus ütles:

{{ $page->verse('3:49..') }}

Moslemid usuvad, et Jeesust ei löödud risti. Jeesuse vaenlaste plaan oli ta risti lüüa, kuid Jumal päästis ta ja tõstis ta üles Enda juurde. Üks teine mees tehti Jeesuse sarnaseks, Jeesuse vaenlased võtsid selle mehe ja lõid ta risti, arvates et see oli Jeesus. Jumal on öelnud:

{{ $page->verse('..4:157..') }}

Ei Muhammed {{ $page->pbuh() }} ega Jeesus ei tulnud muutma põhiõpetust usust Ainujumalasse, mille olid toonud varasemad prohvetid, vaid pigem tulid nad seda kinnitama ja uuendama.
