---
extends: _layouts.answer
section: content
title: Ist der Islam gegen Hunde?
date: 2024-11-17
description: Der Islam lehrt Respekt und Fürsorge für Tiere. Hunde dürfen nicht als Haustiere gehalten werden, aber sie können für bestimmte Zwecke genutzt werden.
sources:
- href: https://islamqa.info/en/answers/69840
  title: islamqa.info
- href: https://islamqa.info/en/answers/33668
  title: islamqa.info
- href: https://seekersguidance.org/answers/halal-and-haram/killing-dogs-islamic-command/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-answers-feeds/can-you-explain-the-reason-for-killing-black-dogs/
  title: seekersguidance.org
- href: https://seekersguidance.org/answers/general-counsel/what-is-the-islamic-stance-on-having-a-dog/
  title: seekersguidance.org
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/171047
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 587746
---

Tiere sind eine Schöpfung Allahs und wir betrachten Tiere nicht als etwas Schlechtes. Vielmehr sind wir als Muslime dazu verpflichtet, Tiere würdevoll, menschlich und barmherzig zu behandeln. Allahs Gesandter {{ $page->pbuh() }} sagte:

{{ $page->hadith('bukhari:3321') }}

## Töten von Hunden

Abdallah ibn Umar sagte:

{{ $page->hadith('muslim:1570b') }}

Es ist wichtig zu klären, welche Art von Hunden in dieser Erzählung gemeint ist und in welchem allgemeinen Kontext sie steht. Die hier erwähnten Hunde sind Rudel wilder Hunde, die in Städten und Dörfern umherwandern und eine Plage für die Gemeinschaft und ein Gesundheitsrisiko darstellen. Sie übertragen und verbreiten Krankheiten, genau wie Ratten, und verursachen den Menschen viele Unannehmlichkeiten. Das Töten gefährlicher oder krankheitsübertragender Tiere ist auf der ganzen Welt üblich und akzeptiert.

Aufgrund der Gesamtheit der Beweise haben die Rechtsgelehrten darauf hingewiesen, dass der Befehl zum Töten von Hunden nur dann zulässig ist, wenn es sich um räuberische und schädliche Hunde handelt. Ein harmloser Hund, unabhängig von seiner Farbe, darf jedoch nicht getötet werden.

## Reinheit

Der Speichel von Hunden ist nach Ansicht der Mehrheit der muslimischen Gelehrten unrein, und nach Ansicht einiger auch ihr Fell. Eine der wichtigsten Lehren des Islam ist Reinheit auf allen Ebenen, daher wurde dies berücksichtigt, ebenso wie die Tatsache, dass Engel sich nicht an Orten aufhalten, an denen es Hunde gibt. Der Grund dafür sind die Worte des Propheten {{ $page->pbuh() }}:

{{ $page->hadith('bukhari:3322') }}

## Hunde als Haustiere halten

Der Islam verbietet es Muslimen, Hunde als Haustiere zu halten. Der Gesandte Allahs {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:1574g') }}
