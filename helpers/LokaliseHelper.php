<?php

namespace App\Helpers;

use Illuminate\Support\Collection;
use Lokalise\LokaliseApiClient;

class LokaliseHelper
{
    protected LokaliseApiClient $client;
    protected string $projectId;

    function __construct()
    {
        $apiToken = getenv('LOKALISE_API_TOKEN');

        $this->client = new LokaliseApiClient($apiToken);
        $this->projectId = getenv('LOKALISE_PROJECT_ID');
    }

    function getKeys(int $limit = 1000, array $query = []): Collection
    {
        $query = array_merge(['limit' => $limit], $query);

        $response = $this->client->keys->list($this->projectId, $query);

        return collect($response->body['keys']);
    }

    function getKeysByName(string $keyNames, array $query = []): Collection
    {
        return $this->getKeys(query: array_merge(['filter_keys' => $keyNames], $query));
    }

    function getTranslations(Collection $keys, array $query = []): Collection
    {
        $query = array_merge([
            'key_ids' => $keys->pluck('key_id')->all(),
            'is_reviewed' => true,
        ], $query);

        $response = $this->client->translations->list($this->projectId, $query);

        return collect($response->body['translations']);
    }

    function getLanguages(): Collection
    {
        $response = $this->client->languages->list($this->projectId);

        return collect($response->body['languages']);
    }

    function getTask(int $taskId): array
    {
        $response = $this->client->tasks->retrieve($this->projectId, $taskId);

        return $response->body['task'];
    }

    function getTasks(array $query = []): Collection
    {
        $response = $this->client->tasks->list($this->projectId, $query);

        return collect($response->body['tasks']);
    }

    function createTask(array $data): array
    {
        $response = $this->client->tasks->create($this->projectId, $data);

        return $response->body['task'];
    }

    function updateTask(int $taskId, array $data)
    {
        $response = $this->client->tasks->update($this->projectId, $taskId, $data);

        return $response->body;
    }

    function closeTask(int $taskId)
    {
        return $this->updateTask($taskId, ['close_task' => true]);
    }

    function deleteTask(int $taskId)
    {
        $response = $this->client->tasks->delete($this->projectId, $taskId);

        return $response->body;
    }

    function getUserGroups(int $teamId): Collection
    {
        $response = $this->client->teamUserGroups->list($teamId);

        return collect($response->body['user_groups']);
    }

    function getTeams(): Collection
    {
        $response = $this->client->teams->list();

        return collect($response->body['teams']);
    }

    function getTeamUsers(int $teamId): Collection
    {
        $response = $this->client->teamUsers->list($teamId);

        return collect($response->body['team_users']);
    }

    function getContributors(): Collection
    {
        $response = $this->client->contributors->list($this->projectId);

        return collect($response->body['contributors']);
    }

    function putBaseLanguageKeys(Collection $keysData): Collection
    {
        $createdAndUpdatedKeys = collect();

        $request = [
            'use_automations' => false,
            'keys' => $keysData->all(),
            'include_translations' => false,
        ];
        $response = $this->client->keys->create($this->projectId, $request);

        $createdAndUpdatedKeys->push(...$response->body['keys']);

        if (empty($response->body['errors'])) {
            return $createdAndUpdatedKeys;
        }

        $failedKeyNames = collect($response->body['errors'])
            ->filter(fn ($error) => $error['message'] === 'This key name is already taken')
            ->pluck('key_name.web')
            ->unique();

        // TODO: if there's other errors, throw an exception

        $keyObjects = $this->getKeysByName($failedKeyNames->implode(','), ['include_translations' => false]);

        // Retry by updating instead of creating
        $response = $this->client->keys->bulkUpdate($this->projectId, [
            'keys' => $keyObjects->map(function ($key) use ($keysData) {
                return array_merge(
                    [
                        'key_id' => $key['key_id'],
                    ],
                    $keysData->firstWhere('key_name', $key['key_name']['web']),
                );
            })->all(),
            'use_automations' => false,
        ]);

        $createdAndUpdatedKeys->push(...$response->body['keys']);

        return $createdAndUpdatedKeys->sort();
    }

    function updateTranslation(int $keyId, array $translation): Collection
    {
        $response = $this->client->keys->bulkUpdate($this->projectId, [
            'keys' => [
                [
                    'key_id' => $keyId,
                    'translations' => [
                        $translation,
                    ],
                ],
            ],
        ]);

        return collect($response->body['keys']);
    }
}
