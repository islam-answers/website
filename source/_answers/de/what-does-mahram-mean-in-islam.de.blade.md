---
extends: _layouts.answer
section: content
title: Was bedeutet Mahram im Islam?
date: 2024-12-08
description: Ein Mahram ist eine Person, mit der eine Ehe verboten ist, wie nahe Blutsverwandte,
  Pflegeverwandte oder Schwiegerverwandte.
sources:
- href: https://islamqa.org/hanafi/daruliftaa/8495/
  title: islamqa.org (Daruliftaa.com)
- href: https://islamqa.info/en/answers/5538/
  title: islamqa.info
- href: https://islamqa.info/en/answers/130002/
  title: islamqa.info
translator_id: 587746
---

Grundsätzlich ist ein Mahram eine Person, mit der die Ehe auf Dauer unrechtmäßig ist. Aus diesem Grund wird "Mahram" im Englischen mit "unverheiratbarer Verwandter" übersetzt. Es ist erlaubt, dass eine Frau ihren Hidschab vor ihren Mahrams ablegt. Ebenso ist es erlaubt, einem Mahram die Hand zu geben oder ihn auf den Kopf, die Nase oder die Wange zu küssen.

Dieses (dauerhafte Verbot der Heirat) wird auf drei Arten begründet: Durch Verwandtschaft (Blutsbande), Pflegeverhältnis (Stillen) und Beziehung durch Heirat.

Ein Mahram ist eine Person, mit der eine Heirat dauerhaft verboten ist, basierend auf drei Arten von Beziehungen: Blutsverwandtschaft, Pflegeverhältnissen oder durch Schwägerschaft. Diese Beziehungen schaffen eine dauerhafte Unzulässigkeit der Heirat, und eine Person wird aufgrund dieser drei Verhältnisse zu einem Mahram.

## Verwandtschaftsverhältnis der Familie/Abstammung

Es ist einem Mann dauerhaft verboten, die folgenden Personen zu heiraten (daher gilt er für sie als Mahram):

 - Mutter, Großmutter und so weiter;
 - Großmutter väterlicherseits und so weiter;
 - Töchter, Enkelinnen und so weiter;
 - Alle Arten von Schwestern (ob Voll- oder Halbschwestern),
 - Tanten mütterlicher- und väterlicherseits,
 - Nichten (Töchter des Bruders oder der Schwester),

Allah sagt im Qur'an:

{{ $page->verse('4:23') }}

Ebenso gehören zu den Mahrams einer Frau die folgenden Personen, basierend auf familiären Beziehungen:

 - Vater, Großvater und so weiter;
 - Großvater mütterlicherseits und so weiter;
 - Söhne, Enkel und so weiter;
 - Alle Arten von Brüdern (ob Voll- oder Halbbrüder),
 - Onkel mütterlicher- und väterlicherseits,
 - Neffen (Söhne des Bruders oder der Schwester),

Allah sagt im Qur'an:

{{ $page->verse('..24:31..') }}

## Pflegeverhältnis

Wer durch Abstammung ein Mahram ist, wird auch durch Pflege als Mahram angesehen. So werden ein Pflegevater (Ehemann einer Pflegemutter), ein Pflegebruder, ein Pflegeonkel, ein Pflegeneffe usw. alle als Mahram einer Frau angesehen, und man ist ein Mahram einer Pflegemutter, Pflegeschwester, Pflegenichte usw.

Man sollte jedoch bedenken, dass dies nur gilt, wenn das Stillen innerhalb der dafür vorgesehenen Zeitspanne von zwei Jahren erfolgt. Man sollte bei der Bestimmung, wer durch Pflegebeziehungen ein Mahram ist, vorsichtig sein, denn die Bestimmung kann manchmal komplex und kompliziert sein. Man muss sich an einen Gelehrten wenden, bevor man zu einem Urteil kommt.

## Verhältnis der Ehe

Die dritte Beziehung, mit der die Ehe dauerhaft rechtswidrig wird und folglich die Beziehung eines Mahram begründet wird, ist die der Ehe.

Es gibt vier Kategorien von Menschen, mit denen die Ehe aufgrund der ehelichen Beziehung dauerhaft unrechtmäßig wird:

 - Die Mutter (Schwiegermutter), Großmutter und aufwärts der eigenen Ehefrau: Eine Ehe mit ihr wird bereits durch die Eheschließung mit der Tochter rechtswidrig, unabhängig davon, ob die Ehe vollzogen wurde oder nicht.
 - Die Tochter (aus einer früheren Ehe), Enkelin und abwärts der eigenen Ehefrau: Eine Ehe mit ihr wird (dauerhaft) rechtswidrig, wenn die Ehe mit ihrer Mutter vollzogen wurde.
 - Die Ehefrau des eigenen Sohnes, Enkels und abwärts: Dies gilt unabhängig davon, ob der Sohn die Ehe vollzogen hat oder nicht.
 - Die Stiefmutter, Stiefgroßmutter und aufwärts: Gemeint sind jene Frauen, die in der Ehe des eigenen Vaters oder Großvaters väterlicher- bzw. mütterlicherseits waren.

Zusammenfassend lässt sich sagen, dass ein Mahram jemand ist, mit dem eine Ehe dauerhaft ungesetzlich ist. Und diese dauerhafte Ungesetzlichkeit/das Verbot der Eheschließung wird auf drei Arten begründet: durch die Verwandtschaft durch Abstammung, durch Pflegeelternschaft und durch Heirat.
