const mix = require("laravel-mix");
require("laravel-mix-jigsaw");

mix.disableSuccessNotifications();
mix.setPublicPath("source/assets/build");

mix
  .jigsaw({
    watch: [
      "CollectionItem/*.php",
      "l10n/*.json",
      "l10n/verses/*.json",
      "l10n/ahadith/*.json",
      "lokalise_cache/*.json",
      "config/*.php",
    ],
  })
  .postCss("source/_assets/css/main.css", "css/main.css", [
    require("postcss-import"),
    require("tailwindcss/nesting"),
    require("tailwindcss"),
  ])
  .options({ processCssUrls: false })
  .browserSync({
    server: "build_local",
    files: ["build_local/**"],
    open: false,
  })
  .sourceMaps()
  .version();
