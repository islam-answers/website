---
extends: _layouts.answer
section: content
title: Hva handler Koranen om?
date: 2024-06-15
description: Den viktigste kilden i enhver muslims tro og praksis.
sources:
- href: https://www.islam-guide.com/no/ch3-7.htm
  title: islam-guide.com
---

Koranen, de siste åpenbarte ord fra Gud, er den viktigste kilden i enhver muslims tro og praksis. Den tar for seg alle emner som angår mennesker: visdom, doktrine, tilbedelse, forretninger, lov og rett, osv, men det grunnleggende tema er forholdet mellom Gud og Hans skapninger. Koranen gir også veiledning og detaljert opplæring for å fremme et rettferdig samfunn, anstendig menneskelig atferd og et rettferdig økonomisk system.

Merk deg at Koranen ble åpenbart for Mohammad {{ $page->pbuh() }} kun på arabisk. Dette innebærer at enhver oversettelse av koranen, det være seg på norsk eller et hvilket som helst annet språk, ikke er Koranen og heller ikke en versjon av Koranen. Man kan heller si at det er oversettelser av meningen i Koranen. Koranen eksisterer kun på arabisk, som er det språket den ble åpenbart på.
