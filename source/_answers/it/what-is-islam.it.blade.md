---
extends: _layouts.answer
section: content
title: Che cos'è l'Islam?
date: 2024-08-04
description: Islam significa sottomettersi, imporsi dei limiti, obbedire ai comandi
  e conformarsi ai divieti senza obiezioni, adorare Allah soltanto e avere fede in
  Lui.
sources:
- href: https://islamqa.info/en/answers/10446/
  title: islamqa.info
- href: https://islamqa.info/en/answers/20756/
  title: islamqa.info
- href: https://islamqa.info/en/answers/172775/
  title: islamqa.info
translator_id: 573107
---

Consultando i dizionari di lingua araba, scopriamo che il significato della parola Islam è: sottomissione, imposizione di limiti a se stessi, obbedienza ai comandi e rispetto dei divieti senza alcuna obiezione, adorazione sincera unicamente di Allah, credendo a ciò che Egli ci dice e avendo fede in Lui.

La parola Islam è diventata il nome della religione divulgata e trasmessa dal profeta Mohammed {{ $page->pbuh() }}.

### Perché questa religione si chiama Islam

Tutte le religioni sono definite con termini diversi che possono derivare, ad esempio, dal nome di un singolo uomo o di un determinato popolo. Così, il Cristianesimo ha preso il nome da Cristo e il Buddismo dal suo fondatore Buddha. Allo stesso modo, il Giudaismo ha preso il nome da una tribù conosciuta come Yehudah (Giuda), per poi diffondersi col nome di Ebraismo. E così via.

Tranne l'Islam, poiché il termine non deriva da un uomo in particolare o da un determinato popolo, ma il suo nome si riferisce al significato della parola Islam. Questo nome indica pertanto che l’istituzione e la fondazione di questa religione non è stata opera di un uomo in particolare e che non è destinata ad un unico popolo, escludendo tutti gli altri. Il suo scopo è piuttosto quello di trasmettere a tutti i popoli della terra la caratteristica che è implicita nella parola Islam. Quindi, chiunque abbia acquisito questa caratteristica, nel passato o nel presente, è un musulmano, e anche colui che la acquisirà in futuro sarà un musulmano.

L'Islam è la religione di tutti i Profeti. È comparsa all'inizio della Profezia, dal tempo di nostro padre Adamo, e tutti i messaggi invitavano all'Islam (sottomissione ad Allah) per quel che riguarda la fede e le regole basilari. I credenti che hanno seguito i profeti precedenti erano tutti musulmani, in linea di massima, ed entreranno in Paradiso in virtù del loro Islam.
