---
extends: _layouts.answer
section: content
title: What does Subhanallah mean?
date: 2024-10-03
description: Commonly translated as “Glory be to Allah”, it appears in the Quran in order to negate deficiencies and flaws attributed to Allah, and affirm His perfection.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
---

The Arabic term “tasbih” usually refers to the phrase “SubhanAllah”. It is a phrase which Allah has approved for Himself, and has inspired His angels to say, and guided the most excellent of His creation to declare. “SubhanAllah” is commonly translated as “Glory be to Allah”, however this translation falls short in conveying the full meaning of the tasbih.

The tasbih is composed of two words: Subhana and Allah. Linguistically, the word “subhana” comes from the word “sabh”, which means distance, remoteness. Accordingly, Ibn 'Abbas explained the phrase as the purity and absolving of Allah above every evil or unbefitting matter. In other words, Allah is forever far away and eternally high above every wrongdoing, deficiency, shortcoming and inappropriateness.

This phrase appears in the Qur’an in order to negate inappropriate and incompatible descriptions attributed to Allah:

{{ $page->verse('23:91') }}

A single tasbih suffices to negate every flaw or lie attributed to Allah in every place and every time by any or all creation. Thus, Allah refutes numerous lies attributed to Him with a single tasbih:

{{ $page->verse('37:149-159') }}

The purpose of the negation is so that its opposite is affirmed. For example, when the Quran negates ignorance from Allah, then the Muslim negates ignorance and in turn affirms the opposite for Allah, which is all-encompassing knowledge. The tasbih is fundamentally a negation (of deficiencies, wrongdoing and false statements) and therefore, according to this principle, entails the affirmation of the opposite, which is beautiful existence, perfect conduct and utmost truth.

Sa'd bin Abu Waqqas (May Allah be pleased with him) reported:

{{ $page->hadith('muslim:2698') }}
