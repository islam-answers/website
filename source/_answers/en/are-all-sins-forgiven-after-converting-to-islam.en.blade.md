---
extends: _layouts.answer
section: content
title: Are all sins forgiven after converting to Islam?
date: 2024-12-29
description: When a disbeliever becomes Muslim, Allah forgives all that he did when
  he was a non-Muslim, and he becomes cleansed of sin.
sources:
- href: https://islamqa.info/en/answers/46505/
  title: islamqa.info
---

By His Grace and Mercy, Allah has made embracing Islam a cause to erase the sins that were committed before it. When a disbeliever becomes Muslim, Allah forgives all that he did when he was a non-Muslim, and he becomes cleansed of sin.

‘Amr ibn al-‘As (may Allah be pleased with him) said:

{{ $page->hadith('muslim:121') }}

Imam Nawawi said: “Islam destroys that which came before it” means that it erases it and wipes it out.

Shaykh Ibn `Uthaymin (may Allah have mercy on him) was asked a similar question, about someone who earned money by dealing in drugs before he became Muslim. He replied: We say to this brother whom Allah has blessed with Islam after he had earned unlawful wealth: be of good cheer, for this wealth is permissible for him and there is no sin on him in it, whether he keeps it or gives it in charity, or uses it to get married, because Allah says in His Holy Book:

{{ $page->verse('8:38') }}

This means all that has passed, in general terms, is forgiven. But any money that was taken by force from its owner must be returned to him. But money that was earned by agreements between people, even if it is unlawful, like that which is earned through Riba (interest), or by selling drugs, etc. it is permissible for him when he becomes Muslim.

Many of the non-Muslims at the time of the Prophet Muhammad {{ $page->pbuh() }} became Muslim after they had killed Muslims, but they were not punished for what they had done.
