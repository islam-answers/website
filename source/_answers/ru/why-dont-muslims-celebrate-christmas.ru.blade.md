---
extends: _layouts.answer
section: content
title: Почему мусульмане не празднуют Рождество?
date: 2024-12-23
description: Мусульмане почитают Иисуса (мир ему) как великого пророка, но отвергают
  его божественность. Рождество, укорененное в иных верованиях, не допускается в Исламе.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 592987
---

Мусульмане уважают и почитают Иисуса {{ $page->pbuh() }}, сына Марьям (Марии) и ждут его второго пришествия. Они считают его одним из величайших посланников Бога. Вера мусульманина невозможна, если он не верит во всех Посланников Аллаха, включая Иисуса {{ $page->pbuh() }}.

Но мусульмане не верят, что Иисус был Богом или Сыном Божьим. Мусульмане также не верят, что он был распят. Аллах послал Иисуса к детям Израиля, чтобы призвать их верить в Аллаха и поклоняться Ему одному. Аллах поддержал Иисуса чудесами, которые доказали, что он говорил правду.

Аллах говорит в Коране:

{{ $page->verse('19:88-92') }}

### Можно ли праздновать Рождество?

Учитывая эти теологические различия, мусульманам не разрешается выделять праздники других религий для проведения каких-либо ритуалов или обычаев. Напротив, день их праздников — это просто обычный день для мусульман, и они не должны выделять его для какой-либо деятельности, которая является частью того, что делают немусульмане в эти дни.

Христианское празднование Рождества — это традиция, которая включает в себя верования и практики, не укорененные в исламских учениях. Таким образом, мусульманам не разрешается участвовать в этих празднованиях, поскольку они не соответствуют исламскому пониманию Иисуса {{ $page->pbuh() }} или его учений.

Помимо того, что это нововведение, это также относится к подражанию неверующим в вопросах, которые свойственны только им и их религии. Пророк Мухаммед {{ $page->pbuh() }} сказал:

{{ $page->hadith('abudawud:4031') }}

Ибн Усеймин (да помилует его Аллах) сказал:

> Поздравление неверующих с Рождеством или любым другим их религиозным праздником запрещено, поскольку это подразумевает одобрение того, чему они следуют в неверии. Даже если мусульманин не принимает это неверие, ему запрещено одобрять ритуалы неверия или поздравлять кого-либо с этим. Не разрешено мусульманам также подражать им каким-либо образом, который является уникальным лишь для их праздников, будь то еда, одежда, купание, разжигание огня или воздержание от обычной работы, поклонения и так далее. И не разрешено устраивать пир или обмениваться подарками, продавать вещи, которые помогают праздновать такие праздники (...) или украшать себя, или устанавливать декорации.

В энциклопедии "Британника" говорится следующее:

> Слово Рождество происходит от древнеанглийского Cristes maesse, «Месса Христа». Не существует определенной традиции относительно даты рождения Христа. Христианские хронографы III века считали, что сотворение мира произошло в день весеннего равноденствия, который тогда считался 25 марта; следовательно, новое творение в воплощении (т. е. зачатие) и смерть Христа должны были произойти в один и тот же день, а его рождение последовало через девять месяцев в день зимнего солнцестояния, 25 декабря.

> Причина, по которой Рождество стали праздновать 25 декабря, остается неясной, но, скорее всего, заключается в том, что ранние христиане хотели, чтобы эта дата совпадала с языческим римским праздником, отмечавшим Рождество «Непобедимого Солнца» (natalis solis invicti); этот праздник отмечал зимнее солнцестояние, когда дни снова начинают удлиняться, а солнце - подниматься выше в небе. Традиционные обычаи, связанные с Рождеством, развились из нескольких источников в результате совпадения празднования рождения Христа с языческими сельскохозяйственными и солнечными обрядами в середине зимы. В древнем Риме Сатурналии (17 декабря) были временем веселья и обмена подарками. 25 декабря также считалось датой рождения иранского бога Митры, Солнца Праведности. В римский Новый год (1 января) дома украшались зеленью и огнями, а детям и бедным дарились подарки.

Таким образом, любой разумный человек может понять что, нет никаких веских оснований для отмечания Рождества. Иисус {{ $page->pbuh() }} и его истинные последователи также не праздновали Рождество или просили кого-либо отметить его, и нет никаких записей о том, чтобы кто-либо, называющий себя христианином, праздновал Рождество до нескольких сотен лет после Иисуса. Так были ли спутники Иисуса более праведными, не празднуя Рождество, или же таковы и сегодняшние люди?

Если вы хотите уважать Иисуса {{ $page->pbuh() }} также, как это делают мусульмане, не следует праздновать выдуманные события, которые были выбраны, чтобы совпасть с языческими праздниками и копировать языческие обычаи. Вы действительно думаете, что Бог или даже сам Иисус одобрили бы или осудили такое?
