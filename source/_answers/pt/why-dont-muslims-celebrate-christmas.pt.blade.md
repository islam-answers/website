---
extends: _layouts.answer
section: content
title: Por que os muçulmanos não celebram o Natal?
date: 2024-12-23
description: Os muçulmanos veneram Jesus como um grande profeta, mas rejeitam a sua
  divindade. O Natal, enraizado em crenças não islâmicas, não é permitido no Islão.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org (Qibla.com)
translator_id: 594172
---

Os muçulmanos respeitam e veneram Jesus {{ $page->pbuh() }}, o filho de Maryam (Maria) e aguardam a sua segunda vinda. Consideram-no um dos maiores mensageiros de Deus para a humanidade. A fé de um muçulmano só é válida se este acreditar em todos os mensageiros de Allah, incluindo Jesus {{ $page->pbuh() }}.

Mas os muçulmanos não acreditam que Jesus era Deus ou o Filho de Deus. Os muçulmanos também não acreditam que ele tenha sido crucificado. Allah enviou Jesus aos filhos de Israel para os convidar a acreditar apenas em Allah e a adorá-Lo. Allah apoiou Jesus com milagres que provaram que ele estava a falar a verdade.

Allah diz no Alcorão:

{{ $page->verse('19:88-92') }}

### É permitido celebrar o Natal?

Tendo em conta estas diferenças teológicas, os muçulmanos não estão autorizados a selecionar as festas de outras religiões para qualquer um destes rituais ou costumes. Pelo contrário, o dia das suas festas é apenas um dia normal para os muçulmanos, que não o devem destacar para qualquer atividade que faça parte do que os não muçulmanos fazem nesses dias.

A celebração cristã do Natal é uma tradição que incorpora crenças e práticas não enraizadas nos ensinamentos islâmicos. Como tal, não é permitido aos muçulmanos participarem nestas celebrações, uma vez que não estão de acordo com o entendimento islâmico de Jesus {{ $page->pbuh() }} ou dos seus ensinamentos.

Além de ser uma inovação, é uma imitação dos incrédulos em assuntos que são exclusivos deles e da sua religião. O Profeta Muhammad {{ $page->pbuh() }} disse:

{{ $page->hadith('abudawud:4031') }}

Ibn 'Uthaymin (que Allah tenha misericórdia dele) disse:

> Felicitar os descrentes por ocasião do Natal ou de qualquer outra das suas festas religiosas é proibido, de acordo com o consenso académico, porque implica a aprovação do que eles seguem como descrença e a sua aprovação por eles. Mesmo que não aprove essa descrença para si próprio, é proibido ao muçulmano aprovar rituais de descrença ou felicitar outra pessoa por eles. Não é permitido aos muçulmanos imitá-los em qualquer forma que seja exclusiva das suas festas, quer se trate de comida, roupas, banhos, acender fogueiras ou abster-se do trabalho habitual ou do culto, e assim por diante. E não é permitido dar um banquete ou trocar presentes ou vender coisas que os ajudem a celebrar as suas festas (...) ou adornar-se ou colocar decorações.

A Enciclopédia Britânica afirma o seguinte:

> A palavra Natal deriva do inglês antigo Cristes maesse, "Missa de Cristo". Não existe uma tradição certa sobre a data do nascimento de Cristo. Os cronógrafos cristãos do século III acreditavam que a criação do mundo tinha lugar no equinócio da primavera, então considerado como 25 de março; por conseguinte, a nova criação na encarnação (ou seja, a conceção) e a morte de Cristo devem ter ocorrido no mesmo dia, com o seu nascimento nove meses depois, no solstício de inverno, a 25 de dezembro.

> A razão pela qual o Natal passou a ser celebrado a 25 de dezembro permanece incerta, mas o mais provável é que os primeiros cristãos quisessem que a data coincidisse com a festa romana pagã que assinalava o "aniversário do sol invicto" (natalis solis invicti); esta festa celebrava o solstício de inverno, quando os dias começam novamente a alongar-se e o sol começa a subir mais alto no céu. Os costumes tradicionais ligados ao Natal desenvolveram-se, portanto, a partir de várias fontes, em resultado da coincidência da celebração do nascimento de Cristo com as observâncias agrícolas e solares pagãs do meio do inverno. No mundo romano, a Saturnália (17 de dezembro) era uma época de festa e de troca de presentes.  O dia 25 de dezembro era também considerado como a data de nascimento do deus misterioso iraniano Mitra, o Sol da Justiça. No Ano Novo romano (1 de janeiro), as casas eram decoradas com verdura e luzes e eram oferecidos presentes às crianças e aos pobres.

Portanto, como qualquer pessoa racional pode ver, não há qualquer base sólida para o Natal, nem Jesus {{ $page->pbuh() }} ou os seus verdadeiros seguidores celebraram o Natal ou pediram a alguém que o celebrasse, nem há qualquer registo de alguém que se chame cristão celebrar o Natal até várias centenas de anos depois de Jesus. Então, será que os companheiros de Jesus foram mais corretamente orientados para não celebrarem o Natal do que as pessoas de hoje?

Por isso, se quiser respeitar Jesus {{ $page->pbuh() }} como fazem os muçulmanos, não celebre um acontecimento inventado que foi escolhido para coincidir com festivais pagãos e copiar costumes pagãos. Acham mesmo que Deus, ou mesmo o próprio Jesus, aprovaria ou condenaria uma coisa destas?
