---
extends: _layouts.answer
section: content
title: Pourquoi les musulmans ne sortent-ils pas avec quelqu'un ?
old_titles:
- Pourquoi les musulmans n'ont-ils pas le droit de sortir avec quelqu'un ?
date: 2024-09-21
description: Le Coran interdit d'avoir des petites amies ou petits amis. L'islam préserve
  les relations entre les individus d'une manière qui convient à la nature humaine.
sources:
- href: https://islamqa.org/shafii/seekersguidance-shafii/241530
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107140
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/hanafi/seekersguidance-hanafi/107560
  title: islamqa.org (SeekersGuidance.org)
- href: https://islamqa.org/shafii/qibla-shafii/33045
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/askimam/17968
  title: islamqa.org (AskImam.org)
- href: https://www.islamweb.net/en/fatwa/89542
  title: islamweb.net
- href: https://www.islamweb.net/en/fatwa/82217
  title: islamweb.net
- href: https://islamqa.info/en/answers/20949
  title: islamqa.info
- href: https://islamqa.info/en/answers/33702
  title: islamqa.info
- href: https://islamqa.info/en/answers/1200
  title: islamqa.info
- href: https://islamqa.info/en/answers/21933
  title: islamqa.info
- href: https://islamqa.info/en/answers/93450
  title: islamqa.info
- href: https://islamqa.info/en/answers/1114
  title: islamqa.info
- href: https://islamqa.info/en/answers/3807
  title: islamqa.info
translator_id: 586055
---

L'islam préserve les relations entre les individus d'une manière qui convient à la nature humaine. Les contacts entre un homme et une femme sont interdits, sauf dans le cadre d'un mariage légal. S'il est nécessaire de parler au sexe opposé, cela doit se faire dans les limites de la politesse et des bonnes manières.

## Prévenir la tentation

L'islam tient à éliminer toute forme de fornication dès le départ. Il n'est pas permis à une femme ou à un homme d'établir une amitié ou une relation amoureuse avec une personne du sexe opposé par le biais de forums de discussion, d'Internet ou de tout autre moyen, car cela conduit la femme ou l'homme à la tentation. C'est la voie du diable qui entraîne la personne dans le péché, la fornication ou l'adultère. Allah dit :

{{ $page->verse('24:21..') }}

Il est interdit à la femme de parler doucement à celui qui ne lui est pas permis, comme le dit Allah :

{{ $page->verse('..33:32') }}

La rencontre intime, la mixité et l'entremêlement d'hommes et de femmes dans un même lieu, les regroupements, ainsi que le dévoilement et l'exposition des femmes aux hommes sont interdits par la loi de l'Islam (Shari'ah). Ces actes sont interdits parce qu'ils font partie des causes de la fitnah (tentation ou épreuve qui implique de mauvaises conséquences), de l'éveil des désirs et de la commission de l'indécence et des actes répréhensibles. Le prophète {{ $page->pbuh() }} a dit :

{{ $page->hadith('ahmad:1369') }}

Le Coran interdit d'avoir des petites amies ou des petits amis. Allah dit :

{{ $page->verse('..4:25..') }}

## Considérations sociales

S'engager dans une relation de type petit ami/petite amie peut nuire gravement à la réputation d'une femme, sans que cela n'affecte nécessairement le partenaire masculin. Les relations sexuelles avant le mariage sont contraires à la religion et mettent en péril l'honneur de la lignée. Il est difficile de voir clair dans le brouillard de l'amour, de la luxure ou de l'infatuation, et c'est pourquoi Allah nous ordonne à tous de ne pas avoir de relations sexuelles avant le mariage. Tant de dommages peuvent survenir après un seul acte sexuel illicite avant le mariage, par exemple des grossesses non désirées, des maladies sexuellement transmissibles, des chagrins d'amour, de la culpabilité.

Outre l'interdiction explicite de Dieu, il existe des raisons évidentes pour lesquelles les relations pré-maritales sont illégales. En voici quelques-unes :

1. La paternité est sans ambiguïté si la femme tombe enceinte. Même dans le cas d'une relation à long terme, qui peut garantir que la femme ne couche pas avec d'autres hommes afin d'établir la compatibilité avec un futur conjoint ?
1. Un enfant né hors mariage n'est pas attribué au père. La préservation de la lignée est l'un des principaux objectifs de la charia.
1. De telles relations donnent aux hommes l'avantage de « faire ce qu'ils veulent » avec les femmes, et sont exempts de toute responsabilité émotionnelle et financière envers la femme et les enfants nés de la relation.
1. Il existe une vertu innée dans la préservation de la chasteté avant le mariage qui est reconnue depuis des siècles et établie par toutes les religions.
1. Bien que nous ne puissions observer aucune différence extérieure dans nos affaires mondaines, les pratiques inappropriées ont un effet sur l'âme que nous ne pouvons pas percevoir. Plus on se livre au péché, plus notre cœur s'endurcit.
1. Avoir plusieurs partenaires augmente le risque de contracter des maladies sexuellement transmissibles et d'affecter les autres.
1. Il n’existe aucune garantie que le fait de vivre ensemble avant le mariage soit un indicateur fiable de la relation après le mariage. De nombreux couples non musulmans vivent ensemble pendant des années, pour ensuite se séparer après le mariage.
1. Il faut également se demander si l’on aimerait que ses propres fils et filles fassent la même chose avant le mariage ?

En conclusion, il est insensé que les relations à long terme et les relations sexuelles avant le mariage soient autorisées. Les considérations individuelles sont insignifiantes par rapport à la multitude de problèmes graves qui affecteraient les individus, principalement les femmes, et c'est le meilleur moyen de conduire à la décadence de la société et de sa propre âme.
