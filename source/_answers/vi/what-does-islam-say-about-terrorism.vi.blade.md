---
extends: _layouts.answer
section: content
title: Islam nói gì về Khủng bố?
date: 2024-06-26
description: Islam, tôn giáo của sự khoan dung, không cho phép hành động khủng bố.
sources:
- href: https://www.islam-guide.com/vi/ch3-11.htm
  title: islam-guide.com
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
---

Islam, tôn giáo của sự khoan dung, không cho phép hành động khủng bố. Trong Kinh Qur'an, Thượng Đế nói:

{{ $page->verse('60:8') }}

Thiên Sứ Muhammad {{ $page->pbuh() }} đã cấm binh sỹ của mình giết hại phụ nữ và trẻ em và Ông khuyên họ như sau:

{{ $page->hadith('tirmidhi:1408') }}

Và Ông cũng nói:

{{ $page->hadith('bukhari:3166') }}

Bên cạnh đó, Thiên Sứ Muhammad {{ $page->pbuh() }} đã cấm hành vi trừng phạt bằng lửa.

Đã có lúc Ông liệt kê giết người vào hàng thứ hai trong những đại tội và Ông cũng cảnh báo điều đó trong Ngày Phán quyết,

{{ $page->hadith('bukhari:6533') }}

Người Islam còn được khuyến khích đối xử tốt với động vật và bị cấm không được làm tổn thương chúng. Có lần Thiên Sứ Muhammad {{ $page->pbuh() }} nói:

{{ $page->hadith('bukhari:3318') }}

Ông cũng từng nói rằng một người đàn ông đã cho con chó bị khát uống nước, vì thế Thượng Đế đã bỏ qua mọi tội lỗi của anh ta. Người ta hỏi Thiên Sứ “Hỡi Người đưa tin của Thượng Đế, liệu chúng tôi có được thưởng vì đã đối xử tốt với động vật?” Ông liền trả lời:

{{ $page->hadith('bukhari:2466') }}

Bên cạnh đó, khi giết động vật làm thức ăn, người Islam được lệnh là phải làm sao để cho động vật ít bị hoảng sợ và tổn thương nhất có thể. Thiên Sứ Muhammad {{ $page->pbuh() }} nói:

{{ $page->hadith('tirmidhi:1409') }}

Dưới ánh sáng của những điều răn dạy này và những văn bản khác của Islam, hành động kích động sự sợ hãi trong lòng của những thường dân không có phương tiện tự vệ, và việc phá hủy hoàn toàn nhà cửa, tài sản, những hành động ném bom và gây thương tật cho những người lớn và trẻ em vô tội đều bị cấm và là những hành động ghê tởm theo Islam và người Islam.

Người Islam theo tôn giáo của hòa bình, khoan dung và độ lượng và phần đông trong số họ không liên quan đến những sự kiện bạo lực mà một số người cho rằng có liên quan đến người Islam. Nếu cá nhân người Islam nào tham dự vào hành động khủng bố, người đó sẽ phạm tội vi phạm những luật lệ của Islam.

Tuy nhiên, điều quan trọng là phải phân biệt giữa khủng bố và kháng cự hợp pháp đối với sự chiếm đóng, vì hai điều này rất khác nhau.
