---
extends: _layouts.answer
section: content
title: Was ist das Scharia-Gesetz?
date: 2024-09-22
description: Die Scharia bezieht sich auf die gesamte Religion des Islam, die Allah
  für Seine Diener gewählt hat, um sie aus den Tiefen der Dunkelheit ins Licht zu
  führen.
sources:
- href: https://islamqa.info/en/answers/210742/
  title: islamqa.info
translator_id: 587746
---

Das Wort Scharia bezieht sich auf die gesamte Religion des Islam, die Allah für seine Diener ausgewählt hat, um sie aus den Tiefen der Dunkelheit ins Licht zu führen. Es ist das, was er ihnen vorgeschrieben und ihnen Gebote und Verbote, Halal und Haram erklärt hat.

Derjenige, der der Scharia Allahs folgt und als erlaubt ansieht, was Er erlaubt hat, und als verboten ansieht, was Er verboten hat, wird den Triumph erringen.

Wer gegen die Scharia Allahs verstößt, setzt sich dem Zorn, der Wut und der Strafe Allahs aus.

Allah, erhaben sei Er, sagt:

{{ $page->verse('45:18') }}

Al-Khalil ibn Ahmad (möge Allah ihm gnädig sein) sagte:

> Das Wort Scharia (Plural: Scharai') bezeichnet das, was Allah für die Menschen in Bezug auf religiöse Angelegenheiten vorgeschrieben hat (shara‘a), sowie das, was Er ihnen hinsichtlich Gebet, Fasten, Hajj und anderen religiösen Verpflichtungen befohlen hat. Es ist das shir‘ah (der Ort im Fluss, an dem man trinken kann).

Ibn Hazm (möge Allah ihm gnädig sein) sagte:

> Die Scharia ist das, was Allah, erhaben sei Er, durch den Propheten Muhammad, {{ $page->pbuh() }}, in Bezug auf die Religion festgelegt hat, ebenso wie durch die Propheten (Friede sei mit ihnen), die vor ihm kamen.
Das Urteil des aufhebenden Textes ist als das endgültige Urteil zu betrachten.

### Sprachlicher Ursprung des Begriffs Scharia (Shari'ah)

Der sprachliche Ursprung des Begriffs „Scharia“ bezieht sich auf den Ort, an dem ein Reiter trinken kann, sowie den Punkt im Fluss, an dem man Wasser schöpfen kann. Allah, erhaben sei Er, sagt:

{{ $page->verse('42:13') }}
