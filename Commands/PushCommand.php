<?php

namespace App\Commands;

use App\Helpers\AnswersHelper;
use App\Helpers\LokaliseHelper;
use Illuminate\Support\Collection;
use Lokalise\Exceptions\LokaliseApiException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

#[AsCommand(name: "lokalise:push", description: "Push keys to Lokalise")]
class PushCommand extends Command
{
    private LokaliseHelper $lokalise;
    private AnswersHelper $answers;

    public function __construct()
    {
        $this->lokalise = new LokaliseHelper();
        $this->answers = new AnswersHelper();

        parent::__construct();
    }

    /**
     * @param array $answer
     * @param Collection $keysObjects
     * @param $lang_iso
     * @param $user_id
     * @param $task_id
     * @return array
     */
    public function generateReviewTaskData(
        array $answer,
        Collection $keysObjects,
        string $lang_iso,
        int $user_id,
        int $task_id
    ): array {
        return [
            "title" => $answer["metadata"]["title"],
            "description" => $answer["url"],
            "keys" => $keysObjects->pluck("key_id")->toArray(),
            "languages" => [
                [
                    "language_iso" => $lang_iso,
                    "users" => [$user_id],
                ],
            ],
            "source_language_iso" => "en",
            "auto_close_languages" => false,
            "auto_close_task" => false,
            "auto_close_items" => false,
            "do_lock_translations" => false,
            "task_type" => "review",
            "parent_task_id" => $task_id,
        ];
    }

    /**
     * @param array $answer
     * @param Collection $keysObjects
     * @param $lang_iso
     * @param $user_id
     * @return array
     */
    public function generateTranslationTaskData(
        array $answer,
        Collection $keysObjects,
        $lang_iso,
        $user_id
    ): array {
        return [
            "title" => $answer["metadata"]["title"],
            "description" => $answer["url"],
            "keys" => $keysObjects->pluck("key_id")->toArray(),
            "languages" => [
                [
                    "language_iso" => $lang_iso,
                    "users" => [$user_id],
                ],
            ],
            "source_language_iso" => "en",
            "auto_close_languages" => true,
            "auto_close_task" => true,
            "auto_close_items" => false,
            "do_lock_translations" => false,
            "task_type" => "translation",
        ];
    }

    function findVerses(array $answer): Collection
    {
        $jsonPath = "l10n/verses/" . $answer["slug"] . ".en.json";

        if (!file_exists($jsonPath)) {
            return collect();
        }

        $verses = json_decode(file_get_contents($jsonPath), true);
        $foundVerses = collect();
        $markdown = $answer["markdown"];

        preg_match_all(
            '/{{ \$page->verse\(\'(.*?)\'\) }}/',
            $markdown,
            $matches
        );

        collect($matches[1])->each(function ($verse) use (&$foundVerses) {
            $foundVerses->add($verse);
        });

        return $foundVerses->mapWithKeys(function ($verse) use (
            $answer,
            $verses
        ) {
            return [$verse => $verses[$verse]];
        });
    }

    function findAhadith(array $answer): Collection
    {
        $jsonPath = "l10n/ahadith/" . $answer["slug"] . ".en.json";

        if (!file_exists($jsonPath)) {
            return collect();
        }

        $ahadith = json_decode(file_get_contents($jsonPath), true);
        $foundAhadith = collect();
        $markdown = $answer["markdown"];

        preg_match_all(
            '/{{ \$page->hadith\(\'(.*?)\'\) }}/',
            $markdown,
            $matches
        );

        collect($matches[1])->each(function ($hadith) use (&$foundAhadith) {
            $foundAhadith->add($hadith);
        });

        return $foundAhadith->mapWithKeys(function ($hadith) use (
            $answer,
            $ahadith
        ) {
            return [$hadith => $ahadith[$hadith]];
        });
    }

    function splitparagraphs(string $markdown): Collection
    {
        return collect(preg_split('/\n{2,}/', $markdown))->map(function (
            $paragraph
        ) {
            return trim($paragraph);
        });
    }

    function replaceTags(string $markdown, string $slug): string
    {
        $markdown = preg_replace(
            '/{{\s*\$page->pbuh\(\s*\)\s*}}/',
            "[%key:pbuh%]",
            $markdown
        );
        $markdown = preg_replace(
            '/{{\s*\$page->verse\(\s*\'(.*?)\'\s*\)\s*}}/',
            "[%key:$slug.30.verse:\$1%]",
            $markdown
        );
        $markdown = preg_replace(
            '/{{\s*\$page->hadith\(\s*\'(.*?)\'\s*\)\s*}}/',
            "[%key:$slug.40.hadith:\$1%]",
            $markdown
        );

        return $markdown;
    }

    function keysForAnswer(array $answer): Collection
    {
        $keys = collect();

        $keys->put($answer["slug"] . ".10.title", $answer["metadata"]["title"]);
        $keys->put(
            $answer["slug"] . ".20.summary",
            $answer["metadata"]["description"]
        );

        $this->findVerses($answer)->each(function ($verse, $key) use (
            &$keys,
            $answer
        ) {
            $keys->put($answer["slug"] . ".30.verse:$key", $verse);
        });

        $this->findAhadith($answer)->each(function ($hadith, $key) use (
            &$keys,
            $answer
        ) {
            $keys->put($answer["slug"] . ".40.hadith:$key", $hadith);
        });

        $markdown = $answer["markdown"];

        $markdown = $this->replaceTags($markdown, $answer["slug"]);
        $markdown = trim($markdown);

        $this->splitparagraphs($markdown)->each(function (
            $paragraph,
            $key
        ) use (&$keys, $answer) {
            $key = str_pad($key + 1, 3, "0", STR_PAD_LEFT);
            $keys->put($answer["slug"] . ".50.paragraph:$key", $paragraph);
        });

        return $keys;
    }

    function generateKeysForLokalise(
        Collection $keys,
        array $answer,
        array $targetLanguage
    ): Collection {
        return $keys->map(function ($value, $key) use (
            $answer,
            $targetLanguage
        ) {
            $tags = [$answer["slug"]];
            $description = null;
            $charLimit = null;
            $translations = [
                [
                    "language_iso" => "en",
                    "translation" => $value,
                    "is_reviewed" => true,
                    "is_unverified" => false,
                ],
            ];

            if (str_contains($key, ".verse:")) {
                $tags[] = "Quran";
                $verseReference = substr($key, strpos($key, ":") + 1);
                $descriptionParts = ["https://quran.com/$verseReference"];

                // if it has two dots, it's a partial verse reference (e.g. ..12:34)
                if (str_contains($verseReference, "..")) {
                    // note: this needs to be the first check
                    $tags[] = "Partial Verse";
                    $descriptionParts = [
                        "https://quran.com/" .
                        str_replace("..", "", $verseReference),
                    ];
                    $descriptionParts[] =
                        "This key contains a partial verse reference. Please verify the reference range after copying the translation from Quran.";
                }

                // if it has dashes, it's a range of verses (e.g. 12:34-56)
                if (str_contains($verseReference, "-")) {
                    $tags[] = "Multiple Verses";
                    $descriptionParts[] =
                        "This key contains a verse range. Please verify the reference range after copying the translation from Quran.";
                }

                $descriptionParts[] =
                    "Please copy the translation from Quran.com and paste it here. Do not translate the verse yourself.";

                $description = implode("\n\n", $descriptionParts);
            }

            if (str_contains($key, ".hadith:")) {
                $tags[] = "Hadith";
                $hadithReference = substr($key, strpos($key, ":") + 1);
                $description = "https://sunnah.com/$hadithReference";
            }

            if (str_contains($key, ".paragraph:")) {
                $tags[] = "Content";

                if (preg_match('/^\[%key:(.*?)]$/', $value)) {
                    $tags[] = "Reference";
                    $description =
                        "Contains a reference to a verse or hadith. Please leave as is, but mark it completed. It should only contain the key.";

                    $translations[] = [
                        "language_iso" => $targetLanguage["lang_iso"],
                        "translation" => $value,
                        "is_reviewed" => false,
                        "is_unverified" => true,
                    ];
                }
            }

            if (str_ends_with($key, ".title")) {
                $tags[] = "Title";
                $description =
                    "The question title. SEO: meta title tag. Must not exceed 60 characters.";
                $charLimit = 60;
            }

            if (str_ends_with($key, ".summary")) {
                $tags[] = "Summary";
                $description =
                    "The answer summary. SEO: meta description tag. Must not exceed 160 characters.";
                $charLimit = 160;
            }

            return [
                "key_name" => $key,
                "description" => $description,
                "platforms" => ["web"],
                "tags" => $tags,
                "translations" => $translations,
                "char_limit" => $charLimit,
            ];
        });
    }

    function pickAnswer(
        Collection $answers,
        QuestionHelper $helper,
        InputInterface $input,
        OutputInterface $output
    ): array {
        $question = new ChoiceQuestion(
            "Please select the answer to push keys for",
            $answers
                ->mapWithKeys(
                    fn($answer) => [
                        $answer["slug"] => $answer["metadata"]["title"],
                    ]
                )
                ->all()
        );
        $question->setErrorMessage("Answer %s is invalid.");
        $answer = $helper->ask($input, $output, $question);

        return $answers->firstWhere("slug", $answer);
    }

    function pickLanguage(
        Collection $languages,
        QuestionHelper $helper,
        InputInterface $input,
        OutputInterface $output
    ): array {
        $question = new ChoiceQuestion(
            "Please choose the target language",
            $languages
                ->mapWithKeys(
                    fn($language) => [
                        $language["lang_iso"] => $language["lang_name"],
                    ]
                )
                ->all()
        );

        $question->setErrorMessage("Language %s is invalid.");
        $language = $helper->ask($input, $output, $question);

        return $languages->firstWhere("lang_iso", $language);
    }

    function pickUser(
        string $message,
        Collection $users,
        QuestionHelper $helper,
        InputInterface $input,
        OutputInterface $output
    ): array {
        $question = new ChoiceQuestion(
            $message,
            $users
                ->mapWithKeys(
                    fn($user) => [$user["email"] => $user["fullname"]]
                )
                ->all()
        );

        $question->setErrorMessage("User %s is invalid.");
        $user = $helper->ask($input, $output, $question);

        return $users->firstWhere("email", $user);
    }

    function pickContributor(
        string $message,
        Collection $contributors,
        QuestionHelper $helper,
        InputInterface $input,
        OutputInterface $output
    ): array {
        $question = new ChoiceQuestion(
            $message,
            $contributors
                ->mapWithKeys(
                    fn($contributor) => [
                        $contributor["email"] => sprintf(
                            "%s (%s)",
                            $contributor["fullname"],
                            collect($contributor["languages"])
                                ->filter(
                                    fn($language) => $language["is_writable"]
                                )
                                ->pluck("lang_name")
                                ->join(", ")
                        ),
                    ]
                )
                ->all()
        );

        $question->setErrorMessage("Contributor %s is invalid.");
        $contributor = $helper->ask($input, $output, $question);

        return $contributors->firstWhere("email", $contributor);
    }

    protected function configure(): void
    {
        $this->addOption(
            "lang",
            "l",
            InputOption::VALUE_OPTIONAL,
            "The language to push keys for"
        )
            ->addOption(
                "slug",
                "s",
                InputOption::VALUE_OPTIONAL,
                "The answer slug to push keys for"
            )
            ->addOption(
                "contributor",
                "c",
                InputOption::VALUE_OPTIONAL,
                "The contributor ID to assign the translation task to"
            )
            ->addOption(
                "reviewer",
                "r",
                InputOption::VALUE_OPTIONAL,
                "The reviewer ID to assign the review task to"
            )
            ->addOption(
                "non-interactive",
                null,
                InputOption::VALUE_NONE,
                "Run the command in non-interactive mode"
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $preselectedLanguage = $input->getOption("lang");
        $preselectedSlug = $input->getOption("slug");
        $preselectedContributor = $input->getOption("contributor");
        $preselectedReviewer = $input->getOption("reviewer");
        $nonInteractive = $input->getOption("non-interactive");

        if (
            $nonInteractive &&
            (!$preselectedLanguage ||
                !$preselectedSlug ||
                !$preselectedContributor ||
                !$preselectedReviewer)
        ) {
            $output->writeln(
                "<error>When running in non-interactive mode, you must provide the --lang, --slug, --contributor, and --reviewer options</error>"
            );
            return Command::FAILURE;
        }

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper("question");

        // get answer (this can be later made a multiselect)
        $answers = $this->answers->getBaseAnswers();

        if ($preselectedSlug) {
            $answer = $answers->firstWhere("slug", $preselectedSlug);
        } else {
            $answer = $this->pickAnswer($answers, $helper, $input, $output);
        }

        // get language
        $languages = $this->lokalise
            ->getLanguages()
            ->reject(fn($language) => $language["lang_iso"] === "en");

        if ($preselectedLanguage) {
            $language = $languages->firstWhere(
                "lang_iso",
                $preselectedLanguage
            );
        } else {
            $language = $this->pickLanguage(
                $languages,
                $helper,
                $input,
                $output
            );
        }

        $keys = $this->keysForAnswer($answer);
        $lokaliseKeys = $this->generateKeysForLokalise(
            $keys,
            $answer,
            $language
        );

        if (!$nonInteractive) {
            $output->writeln(
                "You are about to push " .
                    $keys->count() .
                    " keys for " .
                    $answer["slug"] .
                    " to Lokalise."
            );
            $output->writeln(
                "Please review the keys and translations before proceeding."
            );
            $output->writeln("");

            $lokaliseKeys->each(function ($key) use ($output) {
                $output->writeln($key["key_name"]);
                $output->writeln("---");
                $output->writeln(
                    collect($key["translations"])
                        ->filter(
                            fn($translation) => $translation["language_iso"] ==
                                "en"
                        )
                        ->value("translation")
                );
                $output->writeln("");
            });

            $question = new ConfirmationQuestion(
                "Do you want to proceed? (y/n)",
                false
            );
            if (!$helper->ask($input, $output, $question)) {
                $output->writeln("Aborting.");
                return Command::SUCCESS;
            }
        }

        $output->writeln("Pushing keys for " . $answer["slug"]);

        $pushedKeys = $this->lokalise->putBaseLanguageKeys($lokaliseKeys);

        $pushedKeys->each(function ($key) use ($output) {
            $output->writeln("- " . $key["key_name"]["web"]);
        });

        if ($pushedKeys->count() !== $keys->count()) {
            $output->writeln("Some keys failed to create or update. Aborting.");
            return Command::FAILURE;
        }

        $output->writeln(
            "Pushed " . $pushedKeys->count() . " keys for " . $answer["slug"]
        );
        $output->writeln("");

        // choose a contributor to create and assign a translation task for
        $contributors = $this->lokalise->getContributors();

        if ($preselectedContributor) {
            $contributor = $contributors->firstWhere(
                "user_id",
                $preselectedContributor
            );
        } else {
            $contributor = $this->pickContributor(
                "Please select the CONTRIBUTOR to create and assign a translation task for",
                $contributors->filter(
                    fn($contributor) => collect($contributor["languages"])
                        ->filter(
                            fn($contributorLanguage) => $contributorLanguage[
                                "lang_iso"
                            ] == $language["lang_iso"]
                        )
                        ->isNotEmpty()
                ),
                $helper,
                $input,
                $output
            );
        }

        $output->writeln("Creating task for " . $contributor["fullname"]);

        // can use this to separate this big method into smaller ones
        // $keysObjects = $this->lokalise->getKeysByName($keys->keys()->implode(','));

        $translationTaskData = $this->generateTranslationTaskData(
            $answer,
            $pushedKeys,
            $language["lang_iso"],
            $contributor["user_id"]
        );

        $translationTask = null;

        try {
            $translationTask = $this->lokalise->createTask(
                $translationTaskData
            );
            $output->writeln(
                "Created task for " .
                    $contributor["fullname"] .
                    " with " .
                    $pushedKeys->count() .
                    " keys"
            );
        } catch (LokaliseApiException $exception) {
            $output->writeln(
                "<error>Failed to create task for " .
                    $contributor["fullname"] .
                    "</error>"
            );
            $output->writeln("<error>" . $exception->getMessage() . "</error>");
        }

        if ($translationTask == null) {
            try {
                $translationTask = $this->lokalise
                    ->getTasks([
                        "filter_title" => $answer["metadata"]["title"],
                    ])
                    ->first(
                        fn($task) => $task["task_type"] === "translation" &&
                            collect($task["languages"])->contains(
                                fn($lang) => $lang["language_iso"] ===
                                    $language["lang_iso"] &&
                                    collect($lang["users"])->contains(
                                        fn($user) => $user["email"] ===
                                            $contributor["email"]
                                    )
                            )
                    );

                $output->writeln(
                    "Found existing task for " . $contributor["fullname"]
                );
            } catch (\Exception $exception) {
                $output->writeln(
                    "<error>Failed to find task for " .
                        $contributor["fullname"] .
                        "</error>"
                );
                $output->writeln(
                    "<error>" . $exception->getMessage() . "</error>"
                );

                $output->writeln("Aborting.");

                return Command::FAILURE;
            }
        }

        $output->writeln("");

        // choose a reviewer to create and assign a review task for
        $teams = $this->lokalise->getTeams();
        $users = $this->lokalise->getTeamUsers($teams->first()["team_id"]);

        if ($preselectedReviewer) {
            $reviewer = $users->firstWhere("user_id", $preselectedReviewer);
        } else {
            $reviewer = $this->pickUser(
                "Please select the REVIEWER to create and assign a review task for",
                $users->filter(fn($user) => $user["role"] === "owner"),
                $helper,
                $input,
                $output
            );
        }

        $output->writeln("Creating review task for " . $reviewer["fullname"]);

        $reviewTaskData = $this->generateReviewTaskData(
            $answer,
            $pushedKeys,
            $language["lang_iso"],
            $reviewer["user_id"],
            $translationTask["task_id"]
        );

        $reviewTask = null;

        try {
            $reviewTask = $this->lokalise->createTask($reviewTaskData);
            $output->writeln(
                "Created review task for " .
                    $reviewer["fullname"] .
                    " with " .
                    $pushedKeys->count() .
                    " keys"
            );
        } catch (LokaliseApiException $exception) {
            $output->writeln(
                "<error>Failed to create review task for " .
                    $reviewer["fullname"] .
                    "</error>"
            );
            $output->writeln("<error>" . $exception->getMessage() . "</error>");
        }

        if ($reviewTask == null) {
            try {
                $reviewTask = $this->lokalise
                    ->getTasks([
                        "filter_title" => $answer["metadata"]["title"],
                    ])
                    ->first(
                        fn($task) => $task["task_type"] === "review" &&
                            collect($task["languages"])->contains(
                                fn($lang) => $lang["language_iso"] ===
                                    $language["lang_iso"] &&
                                    collect($lang["users"])->contains(
                                        fn($user) => $user["email"] ===
                                            $reviewer["email"]
                                    )
                            )
                    );

                $output->writeln(
                    "Found existing review task for " . $reviewer["fullname"]
                );
            } catch (\Exception $exception) {
                $output->writeln(
                    "<error>Failed to find review task for " .
                        $reviewer["fullname"] .
                        "</error>"
                );
                $output->writeln(
                    "<error>" . $exception->getMessage() . "</error>"
                );

                $output->writeln("Aborting.");

                return Command::FAILURE;
            }
        }

        $output->writeln("<info>All good!</info>");

        return Command::SUCCESS;
    }
}
