---
extends: _layouts.answer
section: content
title: What does Islam say about evolution?
date: 2024-03-23
description: God created the first human being, Adam in his final form. As for living
  creatures other than humans, Islamic sources are silent on the matter.
sources:
- href: https://islamicpamphlets.com/introduction-to-islam
  title: islamicpamphlets.com
---

God created the first human being, Adam, in his final form - as opposed to evolving gradually from advanced apes. This cannot be directly confirmed or denied by science because this was a unique and singular historical event – a miracle.

As for living creatures other than humans, Islamic sources are silent on the matter and only require us to believe that God created them in whatever manner He wanted, using evolution or otherwise.
