---
extends: _layouts.answer
section: content
title: Este Islamul o religie ce promovează rasismul?
date: 2024-08-21
description: Islamul nu acordă atenție diferențelor de culoare, rasă sau neam.
sources:
- href: https://islamqa.info/en/answers/12391/
  title: islamqa.info
- href: https://islamqa.org/hanafi/hadithanswers/122082/
  title: islamqa.org (HadithAnswers.com)
translator_id: 554943
---

Toți oamenii sunt urmașii unui bărbat și ai unei femei, credincioși și kaafir (necredincioși), albi și negri, arabi și ne-arabi, bogați și săraci, nobili și umili.

Islamul nu acordă atenție diferențelor de culoare, rasă sau neam. Toți oamenii provin din Adam, iar Adam a fost creat din țărână. În Islam, diferențierea între oameni se bazează pe credință (Iman) și evlavie (Taqwa), prin împlinirea a ceea ce Allah a poruncit și evitarea a ceea ce Allah a interzis. Allah spune:

{{ $page->verse('49:13') }}

Profetul Muhammad {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:2564b') }}

Islamul îi consideră pe toți oamenii egali în ceea ce privește drepturile și îndatoririle lor. Oamenii sunt egali în fața legii (Shari'ah), după cum spune Allah:

{{ $page->verse('16:97') }}

Credința, sinceritatea și evlavia conduc toate la Paradis, care este dreptul celui care dobândește aceste atribute, chiar dacă este unul dintre cei mai slabi sau mai umili oameni. Allah spune:

{{ $page->verse('..65:11') }}

Kufr (necredința), aroganța și opresiunea conduc toate la Iad, chiar dacă cel care face aceste lucruri este unul dintre cei mai bogați sau mai nobili oameni. Allah spune:

{{ $page->verse('64:10') }}

Grupul de consilieri ai Profetului Muhammad {{ $page->pbuh() }} a inclus bărbați musulmani din toate triburile, rasele și culorile. Inimile lor erau pline de Tawhid (monoteism) și erau uniți de credința și evlavia lor - precum Abu Bakr din Quraysh, 'Ali ibn Abi Taalib din Bani Haashim, Bilal etiopianul, Suhayb romanul, Salman persanul, oameni bogați precum 'Uthman și oameni săraci precum 'Ammar, oameni cu mijloace și oameni săraci precum Ahl al-Suffah și alții.

Ei au crezut în Allah și s-au străduit de dragul Lui, încât Allah și Trimisul Său au fost mulțumiți de ei. Ei au fost adevărații credincioși.

{{ $page->verse('98:8') }}
