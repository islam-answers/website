---
extends: _layouts.answer
section: content
title: O que é Jihad?
date: 2024-09-22
description: A essência da Jihad é lutar e sacrificar-se pela religião de uma forma
  que agrade a Deus.
sources:
- href: https://islamicpamphlets.com/frequently-asked-questions
  title: islamicpamphlets.com
translator_id: 594172
---

A essência da Jihad é lutar e sacrificar-se pela religião de uma forma que agrade a Deus. Linguisticamente, significa "lutar" e pode referir-se ao esforço para praticar boas acções, fazer caridade ou lutar pela causa de Deus.

A forma mais conhecida é a Jihad militar, que é permitida para preservar o bem-estar da sociedade, impedir a propagação da opressão e promover a justiça.
