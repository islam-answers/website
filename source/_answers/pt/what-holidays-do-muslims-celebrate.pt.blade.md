---
extends: _layouts.answer
section: content
title: Que feriados celebram os muçulmanos?
date: 2024-12-16
description: Os muçulmanos celebram apenas dois Eids (festas), o Eid al-Fitr (no final
  do mês do Ramadan) e o Eid al-Adha, no final do Hajj (peregrinação anual).
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 594172
---

Os muçulmanos celebram apenas dois Eids (festivais): O Eid al-Fitr (no final do mês do Ramadan) e o Eid al-Adha, que assinala a conclusão do Hajj (período anual de peregrinação) no 10º dia do mês de Dhul-Hijjah.

Durante estas duas festas, os muçulmanos felicitam-se mutuamente, espalham alegria nas suas comunidades e festejam com as suas famílias alargadas. Mas, mais importante ainda, recordam as bênçãos de Allah sobre eles, celebram o Seu nome e oferecem a oração do Eid na mesquita. Para além destas duas ocasiões, os muçulmanos não reconhecem nem celebram quaisquer outros dias do ano.

É claro que há outras ocasiões de alegria para as quais o Islam dita uma celebração apropriada, como as celebrações de casamento (walima) ou por ocasião do nascimento de uma criança (aqeeqah). No entanto, estes dias não são especificados como dias específicos no ano; em vez disso, são celebrados à medida que acontecem no decurso da vida de um muçulmano.

Na manhã de Eid al-Fitr e de Eid al-Adha, os muçulmanos participam nas orações de Eid em congregação na mesquita, seguidas de um sermão (khutbah) que recorda aos muçulmanos os seus deveres e responsabilidades. Depois da oração, os muçulmanos cumprimentam-se uns aos outros com a frase "Eid Mubarak" (Abençoado Eid) e trocam presentes e doces.

## Eid al-Fitr

O Eid al-Fitr marca o fim do Ramadan, que tem lugar no nono mês do calendário islâmico lunar.

O Eid al-Fitr é importante porque se segue a um dos meses mais sagrados de todos: O Ramadan. O Ramadan é um período em que os muçulmanos reforçam os seus laços com Allah, recitam o Alcorão e aumentam as suas boas acções. No final do Ramadan, Allah concede aos muçulmanos o dia de Eid al-Fitr como recompensa por terem completado com êxito o jejum e por terem aumentado os actos de culto durante o mês do Ramadão. No Eid al-Fitr, os muçulmanos agradecem a Allah a oportunidade de testemunhar outro Ramadan, de se aproximarem d'Ele, de se tornarem melhores pessoas e de terem outra oportunidade de serem salvos do fogo do Inferno.

{{ $page->hadith('ibnmajah:3925') }}

## Eid al-Adha

O Eid al-Adha marca a conclusão do período anual do Hajj (peregrinação a Meca), que ocorre no mês de Dhul-Hijjah, o décimo segundo e último mês do calendário islâmico baseado na lua.

A celebração do Eid al-Adha tem por objetivo comemorar a devoção do profeta Ibrahim (Abraão) a Allah e a sua disponibilidade para sacrificar o seu filho Ismail (Ismael). No momento do sacrifício, Allah substituiu Ismael por um carneiro, que deveria ser abatido em vez do seu filho. Esta ordem de Allah foi um teste à vontade e ao empenhamento do profeta Ibrahim em obedecer à ordem do seu Senhor, sem questionar. Por conseguinte, Eid al-Adha significa a festa do sacrifício.

Uma das melhores acções a realizar pelos muçulmanos no Eid al-Adha é o ato de Qurbani/Uhdiya (sacrifício), que se realiza após a oração do Eid. Os muçulmanos sacrificam um animal para recordar o sacrifício do Profeta Abraão por Allah. O animal sacrificado deve ser uma ovelha, um cordeiro, uma cabra, uma vaca, um touro ou um camelo. O animal deve estar de boa saúde e ter mais de uma certa idade para poder ser abatido de forma halal, islâmica. A carne do animal sacrificado é partilhada entre o sacrificador, os seus amigos e familiares e os pobres e necessitados.

O sacrifício de um animal no Eid al-Adha reflecte a disponibilidade do profeta Abraão para sacrificar o seu próprio filho e é também um ato de culto que se encontra no Alcorão e uma tradição confirmada do profeta Muhammad {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

No Alcorão, Deus diz:

{{ $page->verse('2:196..') }}
