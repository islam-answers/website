---
extends: _layouts.answer
section: content
title: Jakie święta obchodzą muzułmanie?
date: 2024-08-29
description: Muzułmanie obchodzą tylko dwa święta (Eidy), Eid al-Fitr (na koniec miesiąca
  Ramadanu) i Eid al-Adha, na koniec Hajju (rocznej pielgrzymki).
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 584496
---

Muzułmanie obchodzą tylko dwa Eid (święta): Eid al-Fitr (na zakończenie miesiąca Ramadan) i Eid al-Adha, które oznacza zakończenie hadżdżu (okresu corocznej pielgrzymki) i przypada na 10. dzień miesiąca Dhul-Hijjah.

Podczas tych dwóch świąt muzułmanie gratulują sobie nawzajem, szerzą radość w swoich społecznościach i świętują z dalszymi rodzinami. Ale co ważniejsze, pamiętają o błogosławieństwach Allaha dla nich, celebrują Jego imię i ofiarowują modlitwę Eid w meczecie. Poza tymi dwoma okazjami, muzułmanie nie uznają ani nie świętują żadnych innych dni w roku.

Oczywiście, istnieją inne radosne okazje, dla których islam nakazuje odpowiednie świętowanie, takie jak celebracje weselne (walima) lub z okazji narodzin dziecka (aqeeqah). Jednak dni te nie są określone jako konkretne dni w roku; raczej są obchodzone w miarę, jak zdarzają się w ciągu życia muzułmanina.

W poranek zarówno Eid al-Fitr, jak i Eid al-Adha muzułmanie uczestniczą w modlitwach Eid w zgromadzeniu w meczecie, po których następuje kazanie (khutbah), które przypomina muzułmanom o ich obowiązkach i odpowiedzialności. Po modlitwie muzułmanie witają się nawzajem zwrotem „Eid Mubarak” (Błogosławiony Eid) i wymieniają się prezentami i słodyczami.

## Eid al-Fitr

Eid al-Fitr oznacza koniec Ramadanu, który odbywa się w dziewiątym miesiącu księżycowego kalendarza islamskiego.

Eid al-Fitr jest ważne, ponieważ następuje po jednym z najświętszych miesięcy: Ramadanie. Ramadan to czas, w którym muzułmanie umacniają więź z Allahem, recytują Koran i zwiększają liczbę dobrych uczynków. Pod koniec Ramadanu Allah daje muzułmanom dzień Eid al-Fitr jako nagrodę za pomyślne ukończenie postu i zwiększenie liczby aktów czci w miesiącu Ramadan. Podczas Eid al-Fitr muzułmanie dziękują Allahowi za możliwość bycia świadkami kolejnego Ramadanu, zbliżenia się do Niego, stania się lepszymi ludźmi i otrzymania kolejnej szansy na ocalenie przed Ogniem Piekielnym.

{{ $page->hadith('ibnmajah:3925') }}

## Eid al-Adha

Eid al-Adha oznacza zakończenie corocznego okresu hadżdżu (pielgrzymki do Mekki) przypadającego w miesiącu Dhul-Hijjah, dwunastym i ostatnim miesiącu muzułmańskiego kalendarza księżycowego.

Obchody Eid al-Adha mają na celu upamiętnienie oddania proroka Ibrahima (Abrahama) Allahowi i jego gotowości do poświęcenia swojego syna, Ismaela (Izmaela). W momencie poświęcenia Allah zastąpił Ismaela baranem, który miał zostać zabity zamiast jego syna. Ten rozkaz od Allaha był testem gotowości i zaangażowania proroka Ibrahima w posłuszeństwo rozkazowi jego Pana, bez zadawania pytań. Dlatego Eid al-Adha oznacza święto poświęcenia.

Jednym z najlepszych uczynków, jakie muzułmanie mogą wykonać w Eid al-Adha, jest akt Qurbani/Uhdiya (ofiara), który jest wykonywany po modlitwie Eid. Muzułmanie składają w ofierze zwierzę, aby upamiętnić ofiarę proroka Abrahama dla Allaha. Zwierzęciem ofiarnym musi być owca, jagnię, koza, krowa, byk lub wielbłąd. Zwierzę musi być zdrowe i mieć powyżej pewnego wieku, aby mogło zostać zabite w sposób halal, islamski. Mięso złożonego w ofierze zwierzęcia jest dzielone między osobę składającą ofiarę, jej przyjaciół i rodzinę oraz biednych i potrzebujących.

Ofiarowanie zwierzęcia podczas święta Eid al-Adha odzwierciedla gotowość proroka Abrahama do poświęcenia własnego syna. Jest to również akt czci zawarty w Koranie i potwierdzona tradycja proroka {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

W Koranie Bóg powiedział:

{{ $page->verse('2:196..') }}
