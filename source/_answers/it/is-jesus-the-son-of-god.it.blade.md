---
extends: _layouts.answer
section: content
title: Gesù è il figlio di Dio?
date: 2024-12-28
description: Gesù è considerato un importante profeta e messaggero di Dio, ma non
  Dio o il figlio di Dio; semmai, è un servo e un messaggero tra gli uomini.
sources:
- href: https://islamqa.info/en/answers/26301/
  title: islamqa.info
- href: https://islamqa.info/en/answers/82361/
  title: islamqa.info
translator_id: 573107
---

I musulmani considerano Gesù come un profeta e messaggero di Dio meritevole di grande rispetto, nato dalla Vergine Maria attraverso una nascita miracolosa. Tuttavia, uno dei fondamenti che distingue l'Islam dalla dottrina cristiana è il rifiuto di Gesù come divino o figlio di Dio.

### L'unicità di Dio

Elemento centrale della fede islamica è il Tawhid (l'unicità di Dio), che sottolinea che non esiste altra divinità all'infuori di Dio, e che Egli non genera né è generato. Di conseguenza, attribuire a Gesù la divinità o il titolo di figlio di Dio contraddice il principio fondamentale del monoteismo nell'Islam.

Uno dei principi più importanti della fede in Allah è dichiarare che Egli è al di sopra di tutti gli attributi che implicano mancanze. Uno degli attributi che sottintende la presenza di difetti, e che il musulmano deve assolutamente rifiutare, è il concetto che Allah abbia un figlio, perché ciò implica un bisogno e che esista un essere simile a Lui, questioni rispetto alle quali Allah è ben al di sopra. Allah dice nel Corano:

{{ $page->verse('112:1-4') }}

Il Corano affronta esplicitamente l'errata concezione della divinità di Gesù, sottolineandone la condizione umana e la servitù nei confronti di Dio. Allah dice:

{{ $page->verse('5:75') }}

Il fatto che entrambi mangiassero significa che avevano bisogno di cibo per nutrirsi. Secondo gli studiosi, questo implica che Gesù e sua madre avevano anche necessità di liberarsi dopo che il cibo era stato digerito. Allah Onnipotente è ben al di sopra della dipendenza dal cibo o del dover andare in bagno. Allah dice anche:

{{ $page->verse('4:171') }}

### Esaminare la validità delle scritture cristiane

I musulmani credono che i Vangeli che arrivano oggi in mano alle persone, in cui credono i cristiani, siano stati manomessi e modificati, e continuino ad esserlo tuttora, arrivando al punto che ormai non è rimasto nulla della forma in cui il Vangelo fu originariamente rivelato da Allah.

Il Vangelo che parla maggiormente della fede nella trinità e nella divinità di Gesù {{ $page->pbuh() }}, e che è diventato un punto di riferimento per i cristiani nelle loro argomentazioni a sostegno di questa falsità, è il Vangelo di Giovanni. Questo Vangelo ha fatto sorgere dubbi sulla sua paternità anche tra gli stessi studiosi cristiani - cosa che invece non è avvenuta per gli altri Vangeli in cui credono.

L'Enciclopedia Britannica afferma:

> Per quanto riguarda il Vangelo di Giovanni, è senza dubbio inventato. Il suo autore voleva mettere l'uno contro l'altro due discepoli, vale a dire San Giovanni e San Matteo. 
> Questo scrittore che compare nel testo sosteneva di essere il discepolo amato dal Messia. La Chiesa accettò il fatto senza riserve, affermando che lo scrittore era il discepolo Giovanni, e mise quindi il suo nome sul libro, anche se non vi era alcuna certezza che l'autore fosse Giovanni.

### Cosa significa “figlio di Dio” nella Bibbia?

La Bibbia in cui si afferma che Gesù è il figlio di Dio è la stessa Bibbia in cui la discendenza del Messia termina con Adamo {{ $page->pbuh() }}, e anche lui viene descritto come figlio di Dio:

> Ora Gesù stesso aveva circa trent'anni quando iniziò il suo ministero. Era figlio, così si pensava, di Giuseppe, figlio di Eli... figlio di Set, figlio di Adamo, figlio di Dio. (Luca 3:23-38)

Questa è la stessa Bibbia che descrive Israele (Giacobbe) con gli stessi termini:

> Allora di' al faraone: «Così dice il Signore: Israele è il mio figlio primogenito». (Esodo 4:22)

Lo stesso si dice di Salomone {{ $page->pbuh() }}:

> Egli mi disse: «Salomone, tuo figlio, costruirà la mia casa e i miei cortili, perché io l'ho scelto come figlio e io gli sarò padre» (1 Cronache 28:6).

Adamo, Israele e Salomone erano tutti gli altri figli di Dio, prima di Gesù {{ $page->pbuh() }}? Esaltato sia Allah ben al di sopra di quel che dicono! Infatti, nello stesso Vangelo di Giovanni c'è la spiegazione di ciò che si intende con la parola figlio: essa comprende tutti i giusti servitori di Dio, pertanto non vi è nulla di unico in Gesù o in qualsiasi altro profeta a questo proposito:

> Ma a quanti lo hanno accolto, egli ha dato il potere di diventare figli di Dio, a quelli che credono nel suo nome. (Giovanni 1:12)

Qualcosa di simile appare nel Vangelo di Matteo:

> Beati gli operatori di pace, perché saranno chiamati figli di Dio. (Matteo 5:8-9)

L'uso della parola “figlio” nel linguaggio biblico è una metafora del giusto servitore di Dio, senza che ciò implichi nulla di speciale o unico nel modo in cui è stato creato, o che lo descriva letteralmente come discendenza di Dio. Perciò Giovanni dice:

> Quanto è grande l'amore che il Padre ha riversato su di noi per essere chiamati figli di Dio! (1 Giovanni 3:1)

Rimane la questione di Gesù {{ $page->pbuh() }} descritto come figlio di Dio, e di ciò che hanno inventato sul Signore dei Mondi, dicendo che era il padre di Gesù, il Messia {{ $page->pbuh() }}. Anche questo non è un caso unico nel linguaggio dei Vangeli:

> Gesù disse: Non mi trattenete, perché non sono ancora tornato al Padre. Andate invece dai miei fratelli e dite loro: "Io torno al Padre mio e Padre vostro, al Dio mio e Dio vostro. (Giovanni 20:17)

In questo versetto, Gesù afferma che Dio è un padre anche per loro, e che Dio è il Dio di tutti loro. Il Corano sottolinea l'assoluta unicità e perfezione di Dio, rifiutando qualsiasi associazione di partner o prole con Lui. Il termine "figlio di Dio" nel linguaggio biblico è metaforico, significa rettitudine, non divinità. Pertanto, i musulmani confermano lo status elevato di Gesù, affermando tuttavia l'unicità di Dio come nucleo essenziale della loro fede.
