---
extends: _layouts.answer
section: content
title: ¿Quién escribió el Corán?
date: 2024-06-02
description: El Corán es la palabra literal de Dios, que Él reveló a Su profeta Muhammad,
  a través del Arcángel Gabriel.
sources:
- href: https://www.islam-guide.com/es/ch1-1.htm
  title: islam-guide.com
---

Dios respaldó a Su último profeta Muhammad {{ $page->pbuh() }}, con muchos milagros y muchas evidencias que prueban que él es un verdadero profeta enviado por Dios. Así también respaldó a Su último libro revelado, el Corán, con muchos milagros que prueban que el Corán es la literal palabra de Dios, revelada por Él, y que no fue escrito por ningún ser humano, este capítulo analiza esa evidencia.

El Corán es la palabra literal de Dios, que Él reveló a Su profeta Muhammad {{ $page->pbuh() }}, a través del Arcángel Gabriel. El Corán fue memorizado por Muhammad {{ $page->pbuh() }}, quien luego, lo dictó a sus compañeros. Ellos a su vez lo memorizaron, lo escribieron y lo repasaron con el Profeta {{ $page->pbuh() }}. Más aún, El Profeta {{ $page->pbuh() }} repasaba el Corán con el Arcángel Gabriel, una vez cada año, y dos veces el último año de su vida. Desde que fuera revelado hasta nuestros días, siempre ha habido una gran cantidad de musulmanes que han memorizado el Corán letra por letra. Algunos de ellos han sido capaces de memorizar todo el Corán a la edad de 10 años. Ni una letra del Corán ha sido cambiada en el transcurso de todos estos siglos.

El Corán que fuera revelado catorce siglos atrás, menciona hechos científicos, recientemente descubiertos o comprobados por los científicos. Esto prueba sin duda que el Corán debe ser la literal palabra de Dios y que el Corán no fue escrito por Muhammad {{ $page->pbuh() }} o por ningún otro ser humano. Esto también prueba que Muhammad {{ $page->pbuh() }} es verdaderamente un profeta de Dios. Esta más allá de la razón que alguien haya tenido conocimiento, 1400 años atrás, de estas verdades científicas descubiertas o probadas tan solo recientemente con maquinarias avanzadas y métodos científicos sofisticados.
