---
extends: _layouts.answer
section: content
title: Šta islam kaže o Sudnjem danu?
date: 2024-08-13
description: Doći će dan kada će ceo univerzum biti uništen i mrtvi će biti vaskrsnuti
  kako bi im Bog presudio.
sources:
- href: https://www.islam-guide.com/ch3-5.htm
  title: islam-guide.com
translator_id: 584497
---

Kao i hrišćani, muslimani veruju da je sadašnji život samo probna priprema za sledeću oblast postojanja. Ovaj život je test za svakog pojedinca za život posle smrti. Doći će dan kada će ceo univerzum biti uništen i mrtvi će biti vaskrsnuti za sud od Boga. Ovaj dan će biti početak života koji nikada neće završiti. Ovaj dan je Sudnji dan. Na taj dan, svi ljudi će biti nagrađeni od Boga u skladu sa svojim verovanjima i delima. Oni koji umru vjerujući da "nema drugog Boga osim Allaha, i da je Muhammed poslanik Božiji" i koji su muslimani, biće nagrađeni na taj dan i biće primljeni u Džennet (Raj) zauvek, kao što je Bog rekao:

{{ $page->verse('2:82') }}

Ali oni koji umru ne verujući da "Nema drugog Boga osim Allaha, i da je Muhammed poslanik Božiji" ili nisu muslimani, izgubiće Džennet (Raj) zauvek i biće poslani u Džehennem (Pakao), kao što je Bog rekao:

{{ $page->verse('3:85') }}

I kao što je On rekao:

{{ $page->verse('3:91') }}

Neko može da pita: "Mislim da je islam dobra religija, ali ako bih prešao na islam, moja porodica, prijatelji i drugi ljudi bi me progonili i ismevali me. Dakle, ako ne pređem na islam, da li ću ući u Džennet i biti spašen od paklene vatre?"

Odgovor je ono što je Bog rekao u prethodnom ajetu, "I ko traži drugu religiju osim islama, ona neće biti prihvaćena od njega i on će biti jedan od gubitnika na onom svetu."

Nakon što je poslao Poslanika Muhammeda {{ $page->pbuh() }} da pozove ljude na islam, Bog ne prihvata privrženost bilo kojoj drugoj religiji osim islama. Bog je naš Stvoritelj i Održavatelj. On je stvorio za nas sve što je na zemlji. Svi blagoslovi i dobre stvari koje imamo su od Njega. Dakle, posle svega ovoga, kada neko odbaci veru u Boga, Njegovog Poslanika Muhameda {{ $page->pbuh() }} ili Njegovu religiju islama, to je samo da on ili ona budu kažnjeni na Ahiretu. U stvari, glavna svrha našeg stvaranja je da obožavamo samo Boga i da Mu se pokoravamo, kao što je Bog rekao u Časnom Kur'anu (51:56).

Ovaj život koji danas živimo je veoma kratak život. Nevernici na Sudnjem danu će misliti da je život koji su živeli na zemlji bio samo jedan dan ili deo dana, kao što je Bog rekao:

{{ $page->verse('23:112-113..') }}

I On je rekao:

{{ $page->verse('23:115-116..') }}

Život na onom svijetu je veoma stvaran život. To nije samo duhovno, već i fizičko. Živećemo tamo sa našim dušama i telima.

Upoređujući ovaj svet sa Ahiretom, Poslanik Muhammed {{ $page->pbuh() }}, je rekao:

{{ $page->hadith('muslim:2858') }}

Značenje je da je vrednost ovog sveta u poređenju sa onom na budućem svetu (Ahiretu) kao nekoliko kapi vode u poređenju sa morem.
