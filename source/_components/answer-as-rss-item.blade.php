<entry>
    <id>{{ $entry->getCanonicalUrl() }}</id>
    <link type="text/html" rel="alternate" href="{{ $entry->getCanonicalUrl() }}" />
    <title>{{ $entry->title }}</title>
    <author><name>Islam Answers</name></author>
    <published>{{ date(DATE_ATOM, $entry->date) }}</published>
    <updated>{{ date(DATE_ATOM, $entry->getModifiedTime()) }}</updated>
    <summary>{{ $entry->description }}</summary>
    <content>{{ $entry->description }}</content>
</entry>
