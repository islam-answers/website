---
extends: _layouts.answer
section: content
title: Czy ubój Halal jest okrutny?
date: 2024-12-28
description: Ubój halal jest bardziej humanitarny i higieniczny niż zachodnie metody ogłuszania, które powodują ból i zatrzymują krew w mięsie.
sources:
- href: https://islamqa.org/hanafi/qibla-hanafi/35739/
  title: islamqa.org (Qibla.com)
- href: https://islamqa.org/hanafi/daruliftaa-birmingham/136275/
  title: islamqa.org (DarulIftaBirmingham.co.uk)
translator_id: 584496
---

Islamska praktyka uboju zwierząt poprzez ostre cięcie w przód szyi często spotyka się z krytyką ze strony niektórych obrońców praw zwierząt, którzy twierdzą, że jest to forma okrucieństwa wobec zwierząt – twierdzą, że jest to bolesna, nieludzka metoda zabijania zwierząt. Na Zachodzie prawo wymaga, aby przed ubojem ogłuszyć zwierzęta strzałem w głowę, rzekomo w celu pozbawienia ich przytomności i uniemożliwienia im odzyskania przytomności przed zabiciem, aby nie spowalniać ruchu linii produkcyjnej. Stosuje się ją również w celu zapobieżenia odczuwaniu bólu przez zwierzę przed jego śmiercią.

### Niemieckie badania nad bólem

Dlatego może być zaskoczeniem dla tych, którzy dokonali takich aklimatyzacji, dowiedzenie się o wynikach badania przeprowadzonego przez profesora Wilhelma Schulze i jego kolegę dr Hazima ze Szkoły Medycyny Weterynaryjnej Uniwersytetu w Hanowerze w Niemczech. Badanie - _„Próby obiektywizacji bólu i świadomości w konwencjonalnych metodach CBP (ogłuszanie pistoletem z zamkiem błyskawicznym) i rytualnych (halal, nóż) uboju owiec i cieląt”_ - stwierdza, że:

> Ubój według zasad islamu jest najbardziej humanitarną metodą uboju, a CBP, praktykowany na Zachodzie, powoduje u zwierzęcia dotkliwy ból.

W badaniu kilka elektrod zostało chirurgicznie wszczepionych w różnych punktach czaszki wszystkich zwierząt, dotykając powierzchni mózgu. Zwierzętom pozwolono na rekonwalescencję przez kilka tygodni. Niektóre zwierzęta zostały następnie ubite poprzez wykonanie szybkiego, głębokiego nacięcia ostrym nożem na szyi, przecinając żyły szyjne i tętnice szyjne, a także tchawicę i przełyk (metoda islamska). Inne zwierzęta zostały ogłuszone za pomocą CBP. Podczas eksperymentu elektroencefalograf (EEG) i elektrokardiogram (EKG) rejestrowały stan mózgu i serca wszystkich zwierząt w trakcie uboju i ogłuszania.

Wyniki przedstawiają się następująco:

##### Metoda islamska

1. Pierwsze trzy sekundy od czasu islamskiego uboju zarejestrowane na EEG nie wykazały żadnych zmian w stosunku do wykresu przed ubojem, co wskazuje, że zwierzę nie odczuwało bólu podczas lub bezpośrednio po nacięciu.
2. Przez kolejne 3 sekundy EEG zarejestrowało stan głębokiego snu — nieprzytomności. Wynika to z dużej ilości krwi wypływającej z ciała.
3. Po wspomnianych 6 sekundach EEG zarejestrowało poziom zerowy, wskazujący na brak jakiegokolwiek odczuwania bólu.
4. W momencie, gdy sygnał mózgowy (EEG) spadł do poziomu zerowego, serce nadal biło, a ciało drgało energicznie (odruchowe działanie rdzenia kręgowego), wypychając maksymalną ilość krwi z organizmu, co skutkowało higienicznym mięsem dla konsumenta.

##### Metoda zachodnia przez CBP ogłuszanie

1. Zwierzęta były najwyraźniej nieprzytomne wkrótce po ogłuszeniu.
2. EEG wykazało silny ból bezpośrednio po ogłuszeniu.
3. Serca zwierząt ogłuszonych metodą CBP przestawały bić wcześniej niż serca zwierząt ubitych zgodnie z metodą islamską, co skutkowało zatrzymaniem większej ilości krwi w mięsie. To z kolei jest niehigieniczne dla konsumenta.

### Przepisy islamskie dotyczące uboju

Jak widać z tego badania, islamski ubój zwierząt jest błogosławieństwem zarówno dla zwierzęcia, jak i dla człowieka. Aby ubój był legalny, osoba dokonująca tego działania musi podjąć szereg środków. Ma to zapewnić jak największe korzyści zarówno zwierzęciu, jak i konsumentowi. W związku z tym Prorok Muhammad {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:1955') }}

Przedmiot używany do uboju zwierzęcia powinien być ostry i użyty szybko. Szybkie przecięcie naczyń szyi odcina dopływ krwi do nerwów w mózgu odpowiedzialnych za ból. W ten sposób zwierzę nie odczuwa bólu. Ruchy i więdnięcie, które pojawiają się u zwierzęcia po wykonaniu cięcia, nie są spowodowane bólem, ale skurczem i rozluźnieniem mięśni niedokrwionych. Prorok {{ $page->pbuh() }} uczył również muzułmanów, aby nie ostrzyli ostrza noża w obecności zwierzęcia ani nie zabijali zwierzęcia w obecności innych zwierząt tego samego gatunku.

Cięcie powinno obejmować tchawicę, przełyk i dwie żyły szyjne bez przecinania rdzenia kręgowego. Metoda ta skutkuje szybkim wypływem większości krwi z ciała zwierzęcia. Jeśli rdzeń kręgowy zostanie przecięty, włókna nerwowe prowadzące do serca mogą zostać uszkodzone, co może doprowadzić do zatrzymania akcji serca i zastoju krwi w naczyniach krwionośnych. Krew musi zostać całkowicie spuszczona przed usunięciem głowy. Oczyszcza to mięso poprzez usunięcie większości krwi, która działa jako pożywka dla mikroorganizmów; mięso pozostaje również dłużej świeże w porównaniu z innymi metodami uboju.

Dlatego oskarżenia o znęcanie się nad zwierzętami słusznie należy kierować przeciwko tym, którzy nie stosują islamskiej metody uboju zwierząt, lecz wolą stosować metody, które powodują ból i cierpienie u zwierząt, a także mogą wyrządzić krzywdę osobom spożywającym mięso.
