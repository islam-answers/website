---
extends: _layouts.answer
section: content
title: De ce musulmanii nu beau alcool?
date: 2024-10-11
description: Consumul de alcool este un păcat major. Acesta încețoșează mintea și
  irosește bani. Allah a interzis tot ceea ce dăunează trupului și minții.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
translator_id: 554943
---

Islamul a venit să aducă și să sporească lucrurile bune și să îndepărteze și să reducă lucrurile dăunătoare. Tot ceea ce este benefic sau în mare parte benefic este permis (halal) și tot ceea ce este dăunător sau în mare parte dăunător este interzis (haram). Alcoolul se încadrează, fără îndoială, în a doua categorie. Allah spune:

{{ $page->verse('2:219') }}

Efectele nocive și rele ale alcoolului sunt bine cunoscute de toți oamenii. Printre efectele nocive ale alcoolului se numără cele menționate de Allah:

{{ $page->verse('5:90-91') }}

Alcoolul duce la multe lucruri dăunătoare și merită să fie numit "cheia tuturor relelor" - așa cum a fost descris de Profetul Muhammad {{ $page->pbuh() }}, care a spus:

{{ $page->hadith('ibnmajah:3371') }}

El creează dușmănie și ură între oameni, îi împiedică să își amintească de Allah și să se roage, îi îndeamnă la zina (relații sexuale ilegale) și îi poate chiar îndemna să comită incest cu fiicele, surorile sau alte rude de sex feminin. El înlătură mândria și gelozia protectoare, generează rușine și regret. Acesta conduce la dezvăluirea secretelor și la expunerea greșelilor și îi încurajează pe oameni să comită păcate și acțiuni rele.

În plus, Ibn 'Umar a povestit că Profetul {{ $page->pbuh() }} a spus:

{{ $page->hadith('muslim:2003a') }}

Profetul {{ $page->pbuh() }} a mai spus:

{{ $page->hadith('ibnmajah:3381') }}
