---
extends: _layouts.answer
section: content
title: Quem é o Profeta Muhammad?
date: 2024-06-02
description: O Profeta Muhammad foi um exemplo perfeito de um ser humano honesto,
  justo, misericordioso, compassivo, verdadeiro e corajoso.
sources:
- href: https://www.islam-guide.com/pt/ch3-8.htm
  title: islam-guide.com
---

Muhammad {{ $page->pbuh() }} nasceu em Meca no ano de 570. Uma vez que seu pai morreu antes do seu nascimento e sua mãe morreu pouco depois, ele foi educado por seu tio, que era da respeitada tribo do Coraix. Ele cresceu iletrado, incapaz de ler ou escrever, e permaneceu assim até a sua morte. Seu povo, antes de sua missão como um profeta, era ignorante da ciência e a maioria deles era iletrada. Quando ele cresceu, ele se tornou conhecido por ser confiável, honesto, generoso e sincero. Ele era tão confiável que eles o chamam de Confiável. Muhammad {{ $page->pbuh() }} era muito religioso, e há muito tempo ele detestava a decadência e idolatria de sua sociedade.

Na idade de quarenta anos, Muhammad {{ $page->pbuh() }} recebeu sua primeira revelação de Deus através do Anjo Gabriel. As revelações continuaram por vinte e três anos, e elas são conhecidas coletivamente como o Alcorão.

Assim que ele começou a recitar o Alcorão e a pregar a verdade que Deus tinha revelado a ele, ele e seu pequeno grupo de seguidores sofreram perseguições dos descrentes. A perseguição aumentou tanto que no ano de 622 Deus deu a eles a ordem para emigrar. Essa emigração de Meca para a cidade de Medina, em torno de 260 milhas ao norte, marca o começo do calendário islâmico.

Depois de vários anos, Muhammad {{ $page->pbuh() }} e seus seguidores foram capazes de retornar a Meca, onde perdoaram seus inimigos. Antes de Muhammad {{ $page->pbuh() }} morrer, na idade de sessenta e três anos, a maior parte da Península Arábica tinha se tornado muçulmana, e dentro de um século de sua morte, o Islã se espalhou da Espanha no Ocidente até tão longe no Oriente quanto a China. Entre as razões para o rápido e pacífico crescimento do Islã estava a verdade e claridade de sua doutrina. O Islã chama para a fé em um único Deus, Que é o único merecedor de adoração.

O Profeta Muhammad {{ $page->pbuh() }} foi um exemplo perfeito de um ser humano honesto, justo, misericordioso, compassivo, verdadeiro e corajoso. Embora ele fosse um homem, todas as características ruins foram removidas dele e ele se empenhou exclusivamente em nome de Deus e Sua recompensa na Outra Vida. Além disso, em todas as suas ações e transações, ele era sempre consciente e temeroso a Deus.
