---
extends: _layouts.answer
section: content
title: Cosa dice l'Islam sul giorno del giudizio?
date: 2024-05-26
description: Verrà un giorno in cui l'intero universo sarà distrutto e i morti saranno
  resuscitati per il giudizio di Dio.
sources:
- href: https://www.islam-guide.com/it/ch3-5.htm
  title: islam-guide.com
---

Come i cristiani, i musulmani credono che la vita attuale è solo un periodo di preparazione a prova per il futuro regno dell'esistenza. Questa vita è una prova per ogni individuo della vita dopo la morte. Verrà un giorno in cui l'universo intero sarà distrutto e i morti risorgeranno per l'intervento di Dio. Questo giorno sarà l'inizio di una vita che non finirà mai. Questo giorno è il giorno del giudizio. In quel giorno, tutti i popoli saranno ricompensati da Dio secondo la loro fede e le loro azioni. Coloro che moriranno credendo che “Non esiste vero dio ma Dio e Mohammed è il messaggero (profeta) di Dio”, sono musulmani, essi saranno ricompensati in quel giorno e saranno ammessi al Paradiso per sempre, come Dio disse:

{{ $page->verse('2:82') }}

Ma coloro che muoiono non credendo che “Non esiste vero dio ma Dio e Mohammed è il messaggero (profeta) di Dio” o non sono musulmani, perderanno il Paradiso per sempre e saranno inviati alle fiamme dell'Inferno, come Dio disse:

{{ $page->verse('3:85') }}

Ed Egli disse:

{{ $page->verse('3:91') }}

Si potrebbe chiedere, ‘Penso che l'Islam sia una buona religione, ma se mi convertissi all'Islam, la mia famiglia, gli amici e le altre persone mi perseguiterebbero e si prenderebbero gioco di me. Se non mi converto all'Islam, entrerò nel Paradiso e sarò salvato dal fuoco dell'Inferno?’

La risposta di Dio è nel versetto precedente, “Chi vuole una religione diversa dall'Islam, il suo culto non sarà accettato e nell'altra vita sarà tra i perdenti.”

Dopo aver inviato il profeta Mohammed {{ $page->pbuh() }} per chiamare il popolo all'Islam, Dio non accetta adesioni ad altre religioni se non all'Islam. Dio è il nostro Creatore e Sostenitore. Egli creò per noi tutto ciò che esiste sulla Terra. Tutte le benedizioni e le cose buone derivano da Lui. Così, dopo tutto ciò, quando qualcuno rifiuta la fede in Dio, il Suo profeta Mohammed {{ $page->pbuh() }}, o la Sua religione dell'Islam, sarà punito nell'aldilà. Ora, lo scopo principale della nostra creazione è di adorare solo Dio e di obbedirgli, come Dio disse nel Sacro Corano (51:56).

La vita che viviamo attualmente è molto breve. I non credenti nel giorno del giudizio penseranno che la vita vissuta sulla Terra sia solo un giorno o una parte del giorno, come Dio disse:

{{ $page->verse('23:112-113..') }}

Ed Egli disse:

{{ $page->verse('23:115-116..') }}

La vita nell'aldilà è vera vita. Non è solo spirituale, ma anche fisica. Ci vivremo con le nostre anime e i nostri corpi.

Facendo un paragone tra questo mondo e l'aldilà, il profeta Mohammed {{ $page->pbuh() }} disse:

{{ $page->hadith('muslim:2858') }}

Il significato è che, il valore del mondo paragonato con l'aldilà è come poche gocce di acqua rispetto al mare.
