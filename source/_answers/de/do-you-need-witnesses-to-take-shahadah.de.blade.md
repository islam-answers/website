---
extends: _layouts.answer
section: content
title: Brauchen Sie Zeugen, um Shahadah zu sagen?
date: 2024-07-13
description: Es ist keine Voraussetzung, öffentlich vor Zeugen bekannt zu geben, dass
  man zum Islam konvertieren will.
sources:
- href: https://islamqa.info/en/answers/49715
  title: islamqa.info
---

Um Muslim zu werden, ist es keine Voraussetzung, dies vor anderen Leuten öffentlich bekannt zu geben. Der Islam ist eine Sache zwischen dem Diener und Seinem Herrn, gepriesen und erhaben sei Er.

Wenn man bezeugen lassen will, dass man konvertiert ist, um dies mit einer persönlichen Bescheinigung festzuhalten, so spricht nichts dagegen. Jedoch darf dies nicht als eine Voraussetzung gesehen werden, damit sein Eintritt zum Islam akzeptiert wird.
