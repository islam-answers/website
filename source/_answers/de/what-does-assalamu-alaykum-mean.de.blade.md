---
extends: _layouts.answer
section: content
title: Was bedeutet Assalamu Alaykum?
date: 2024-09-22
description: Assalamu alaykum bedeutet "Friede sei mit dir" und ist ein üblicher Gruß unter Muslimen. "Salam" bedeutet Sicherheit und Schutz vor Bösem.
sources:
- href: https://islamqa.info/en/answers/4596/
  title: islamqa.info
- href: https://islamqa.info/en/answers/132956
  title: islamqa.info
translator_id: 587746
---

Als der Islam kam, schrieb Allah vor, dass die Begrüßungsform unter Muslimen „Al-salamu 'alaykum“ sein sollte und dass dieser Gruß nur unter Muslimen verwendet werden sollte.

Die Bedeutung von „Salam“ (wörtlich „Frieden“) umfasst Harmlosigkeit, Sicherheit und Schutz vor Übel und Fehlern. Der Name al-Salam ist einer der 99 Namen Allahs, erhaben sei Er. Daher bedeutet der Gruß „Salam“, der unter Muslimen verwendet wird, dass der Segen Seines Namens auf euch herabkommen möge. Die Verwendung der Präposition „‘ala“ in „‘alaykum“ (auf euch) zeigt, dass der Gruß umfassend gemeint ist.

Die vollständigste Form der Begrüßung ist "As-Salamu `alaykum wa rahmatu-Allahi wa barakatuhu", was soviel bedeutet wie "Friede sei mit dir und die Barmherzigkeit Allahs und Seine Segnungen".

Der Prophet {{ $page->pbuh() }} machte die Verbreitung des Salam zu einem Teil des Glaubens.

{{ $page->hadith('bukhari:12') }}

Deshalb hat der Prophet {{ $page->pbuh() }} erklärte, dass das Geben von Salam Liebe und Brüderlichkeit verbreitet. Der Gesandte Allahs {{ $page->pbuh() }} sagte:

{{ $page->hadith('muslim:54') }}

Der Prophet {{ $page->pbuh() }} hat uns befohlen, den Salam zu erwidern und hat es zu einem Recht und einer Pflicht gemacht. Er sagte:

{{ $page->hadith('muslim:2162a') }}

Es ist offensichtlich, dass es verpflichtend ist, den Salam zu erwidern, da ein Muslim durch das Erwidern des Salam Sicherheit gewährt und daher auch im Gegenzug Sicherheit erwarten darf. Es ist, als würde er dir sagen: „Ich gebe dir Schutz und Sicherheit, also musst du mir das Gleiche bieten.“ So wird verhindert, dass er misstrauisch wird oder denkt, dass derjenige, dem er den Salam gegeben hat, ihn betrügt oder ignoriert.
