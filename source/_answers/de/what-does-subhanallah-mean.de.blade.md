---
extends: _layouts.answer
section: content
title: Was bedeutet Subhanallah?
date: 2024-10-20
description: Oft mit „Gepriesen sei Allah“ übersetzt, betont dieser Ausdruck im Qur'an Allahs Vollkommenheit und verneint jegliche Mängel oder Fehler.
sources:
- href: https://www.daralarqam.co.uk/articles/subhanallah-meaning
  title: daralarqam.co.uk
translator_id: 587746
---

Der arabische Begriff „Tasbih“ bezieht sich gewöhnlich auf die Phrase „SubhanAllah“. Es ist ein Ausdruck, den Allah für sich selbst ausgewählt hat, seine Engel dazu inspiriert hat, ihn zu sagen, und den besten seiner Schöpfung gelehrt hat, ihn auszusprechen. „SubhanAllah“ wird oft mit „Gepriesen sei Allah“ übersetzt, jedoch gibt diese Übersetzung die volle Bedeutung des Tasbih nicht vollständig wieder.

Der Tasbih besteht aus zwei Wörtern: Subhana und Allah. Linguistisch kommt das Wort „Subhana“ vom Wort „Sabh“, das Distanz, Abgelegenheit bedeutet. Dementsprechend erklärte Ibn 'Abbas den Satz als die Reinheit und Absolution Allahs über jede böse oder unpassende Angelegenheit. Mit anderen Worten: Allah ist ewig erhaben und fern von jeglichem Unrecht, Mangel, Fehler oder Unangemessenheit

Dieser Satz erscheint im Qur'an, um unangemessene und unvereinbare Beschreibungen, die Allah zugeschrieben werden, zu widerlegen:

{{ $page->verse('23:91') }}

Ein einziges Tasbih genügt, um jeden Fehler oder jede Lüge zu widerlegen, die Allah an jedem Ort und zu jeder Zeit von einem oder allen Geschöpfen zugeschrieben wird. So widerlegt Allah zahlreiche Lügen, die Ihm zugeschrieben werden, mit einem einzigen Tasbih:

{{ $page->verse('37:149-159') }}

Der Zweck der Verneinung besteht darin, das Gegenteil zu bekräftigen. Zum Beispiel, wenn der Qur'an Allah Unwissenheit abspricht, verneint der Muslim damit Unwissenheit und bestätigt im Gegenzug das Gegenteil für Allah: umfassendes Wissen. Das Tasbih ist im Kern eine Verneinung (von Mängeln, Fehlverhalten und falschen Aussagen) und bringt daher nach diesem Prinzip die Bestätigung des Gegenteils mit sich: eine schöne Existenz, vollkommenes Handeln und höchste Wahrheit.

Sa'd bin Abu Waqqas (möge Allah mit ihm zufrieden sein) berichtete:

{{ $page->hadith('muslim:2698') }}
