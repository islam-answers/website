---
extends: _layouts.answer
section: content
title: Ce spune Islamul despre tratamentul părinților?
date: 2024-04-01
description: A arăta bunăvoință părinților este una dintre cele mai bune acțiuni.
  Porunca de a fi bun cu părinții este o învățătură regăsită foarte clar în Coran.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 554943
---

A arăta bunăvoință față de părinți este una dintre cele mai bine răsplătite acțiuni în ochii lui Dumnezeu. Porunca de a fi bun cu părinții este o învățătură regăsită foarte clar în Coran. Dumnezeu spune:

{{ $page->verse('4:36..') }}

Mențiunea slujirii față de părinți urmează imediat după slujirea față de Dumnezeu. Acest lucru se repetă în tot Coranul.

{{ $page->verse('17:23') }}

În acest verset, Dumnezeu ne poruncește să ne închinăm Lui singur și nimănui altcuiva - iar imediat apoi El ne îndreaptă atenția asupra bunului comportament cu părinții, mai ales când aceștia au îmbătrânit și depind de alții pentru adăpost și nevoile lor. Musulmanii sunt învățați să nu fie aspri cu părinții sau să-i certe, ci mai degrabă să le vorbească cu cuvinte onorabile și să le arate dragoste.

În Coran, Dumnezeu amintește oamenilor să se roage pentru părinții lor:

{{ $page->verse('17:24') }}

Dumnezeu subliniază că ascultarea părinților înseamnă ascultarea Lui, cu excepția cazului când ei ne cer să asociem alți parteneri lui Dumnezeu. El spune:

{{ $page->verse('31:14-15..') }}

Acest verset subliniază faptul că toți părinții merită un tratament bun. O atenție suplimentară trebuie acordată mamelor, datorită greutăților mai mari pe care acestea le au.

A avea grijă de părinți este considerată una dintre cele mai bune fapte:

{{ $page->hadith('muslim:85e') }}

În concluzie, în Islam, a cinsti părinții înseamnă a-i asculta, a-i respecta, a te ruga pentru ei, a coborî vocea în prezența lor, a le zâmbi, a nu arăta neplăcere față de ei, a te strădui să-i slujești, a le îndeplini dorințele și a-i consulta.
