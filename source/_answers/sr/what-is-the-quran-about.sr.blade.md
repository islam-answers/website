---
extends: _layouts.answer
section: content
title: O čemu govori Kur'an?
date: 2024-10-24
description: Primarni izvor vere i prakse svakog muslimana.
sources:
- href: https://www.islam-guide.com/ch3-7.htm
  title: islam-guide.com
translator_id: 584497
---

Kur'an, posljednja objavljena Božija reč, primarni je izvor vere i prakse svakog muslimana. Bavi se svim temama koje se tiču​ljudskih bića: mudrošću, naukom, bogoslužjem, transakcijama, zakonom itd., ali njegova osnovna tema je odnos između Boga i Njegovih stvorenja. U isto vreme, daje smernice i detaljna učenja za pravedno društvo, ispravno ljudsko ponašanje i pravedan ekonomski sistem.

Imajte na umu da je Kur'an objavljen Poslaniku Muhammedu {{ $page->pbuh() }} samo na arapskom jeziku. Dakle, svaki prevod Kur'ana, bilo na engleskom ili bilo kojem drugom jeziku, nije ni Kur'an, niti verzija Kur'ana, već je samo prevod značenja Kur'ana. Kur'an postoji samo na arapskom jeziku na kojem je objavljen.
