---
extends: _layouts.answer
section: content
title: Kes kirjutas Koraani?
date: 2024-06-08
description: Koraan on Jumala otsene sõna, mille Ta ilmutas Muhammedile läbi ingel
  Gaabrieli.
sources:
- href: https://www.islam-guide.com/ee/ch1-1.htm
  title: islam-guide.com
---

Jumal on andnud oma viimasele prohvetile Muhammedile {{ $page->pbuh() }} toetuseks palju imesid ja muud tõendamaks, et ta on Jumala enda poolt saadetud. Samuti on Jumal pannud oma viimasesse ilmutatud raamatusse, Pühasse Koraani, palju tõendeid selle kohta, et tegu on Jumala enda sõnaga, mis ei ole kirjutatud ühegi inimese poolt.

Koraan on Jumala otsene sõna, mille Ta ilmutas Muhammedile {{ $page->pbuh() }} läbi ingel Gaabrieli. Muhammed {{ $page->pbuh() }} õppis selle pähe ning luges seejärel oma kaaslastele peast ette. Kaaslased õppisid omakorda Koraani pähe ning kirjutasid üles, vaadates veel Muhammedi {{ $page->pbuh() }} eluajal kõik üksipulgi üle. Igal aastal käis ingel Gaabriel korra Muhammedi {{ $page->pbuh() }} teadmisi kontrollimas, lastes tal järjest kogu Koraani peast ette lugeda, viimasel eluaastal tuli ingel Gaabriel oma testi läbi viima koguni kaks korda. Ajast, mil Koraan ilmutati kuni tänase päevani on alati olnud rohkelt moslemeid, kes on terve Koraani täht-tähelt pähe õppinud. Paljud teavad kogu Koraani peast juba 10-aastaselt. Tänu sellele, et nii paljudel moslemitel Koraan peas on, ei ole läbi sajandite Koraanis muudetud ühtegi tähte.

Kuigi Koraan ilmutati juba 14 sajandit tagasi, sisaldab see fakte, mis on alles hiljuti teadlaste poolt avastatud või tõestatud. Paljud teadlased on Koraani lugedes imestust avaldanud, kuidas on sellised teadmised sinna üldse jõudnud. Kõigile on ju selge, et 14 sajandit tagasi elanud kirjaoskamatu Meka kaupmees ei oleks suutnud välja mõeldamõelda seda, mille kindlakstegemiseks on tänapäeva helgemad pead kasutanud arenenud tehnoloogia ja keeruliste teaduslike meetodite abi.
