---
extends: _layouts.answer
section: content
title: Welche Feiertage feiern Muslime?
date: 2024-08-29
description: 'Muslime feiern nur zwei Feste, auch Eid genannt: Eid al-Fitr (am Ende
  des Monats Ramadan) und Eid al-Adha (am Ende der jährlichen Pilgerfahrt, des Hajj).'
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 587746
---

Muslime feiern nur zwei Eide (Feste): Eid al-Fitr (am Ende des Monats Ramadan) und Eid al-Adha, das die Vollendung der Hajj (jährliche Pilgerfahrt) am 10. Tag des Monats Dhul-Hijjah markiert.

An diesen beiden Festen gratulieren Muslime einander, bringen Freude in ihre Gemeinschaften und feiern im Kreise ihrer Verwandten. Vor allem aber gedenken sie der Segnungen Allahs, preisen Seinen Namen und verrichten das Eid-Gebet in der Moschee. Außer diesen beiden Anlässen erkennen Muslime keine weiteren Feiertage im Jahr an oder feiern sie.

Natürlich gibt es auch andere erfreuliche Anlässe, die im Islam gefeiert werden, wie Hochzeiten (Walima) oder die Geburt eines Kindes (Aqeeqah). Diese Ereignisse sind jedoch keine festgelegten Feiertage im Jahr, sondern werden gefeiert, wenn sie im Leben eines Muslims eintreten.

Am Morgen von Eid al-Fitr und Eid al-Adha versammeln sich Muslime in der Moschee zum Eid-Gebet, gefolgt von einer Predigt (Khutbah), die sie an ihre Pflichten und Verantwortungen erinnert. Nach dem Gebet wünschen sich die Muslime einander „Eid Mubarak“ (Gesegnetes Fest) und tauschen Geschenke sowie Süßigkeiten aus.

## Eid al-Fitr

Eid al-Fitr kennzeichnet das Ende des Ramadan, der im neunten Monat des islamischen Mondkalenders gefeiert wird.

Eid al-Fitr ist von großer Bedeutung, da es auf einen der heiligsten Monate, den Ramadan, folgt. Ramadan ist eine Zeit, in der Muslime ihre Beziehung zu Allah stärken, den Koran rezitieren und gute Taten vermehren. Am Ende des Ramadan belohnt Allah die Muslime mit dem Festtag Eid al-Fitr für das erfolgreiche Fasten und das vermehrte Gedenken während des Monats. An Eid al-Fitr danken Muslime Allah für die Gelegenheit, einen weiteren Ramadan zu erleben, sich Ihm näher zu kommen, bessere Menschen zu werden und eine weitere Chance auf Rettung vor dem Höllenfeuer zu erhalten.

{{ $page->hadith('ibnmajah:3925') }}

## Eid al-Adha

Eid al-Adha markiert den Abschluss der jährlichen Hajj-Zeit (Pilgerfahrt nach Mekka), die im Monat Dhul-Hijjah stattfindet, dem zwölften und letzten Monat des islamischen Mondkalenders.

Das Fest von Eid al-Adha gedenkt der Hingabe des Propheten Ibrahim (Abraham) an Allah und seiner Bereitschaft, seinen Sohn Ismail (Ismael) zu opfern. Im Moment der Opfergabe ersetzte Allah Ismail durch einen Widder, der anstelle seines Sohnes geschlachtet werden sollte. Dieser Befehl Allahs war eine Prüfung für Ibrahim, um seine Bereitschaft und Treue zu zeigen, den Befehl seines Herrn ohne Frage zu befolgen. Daher bedeutet Eid al-Adha das Fest des Opfers.

Eine der besten Taten, die Muslime an Eid al-Adha verrichten können, ist das Qurbani/Uhdiya (Opfer), das nach dem Eid-Gebet durchgeführt wird. Muslime opfern ein Tier, um an das Opfer des Propheten Abraham für Allah zu erinnern. Das Opfer kann ein Schaf, Lamm, Ziege, Rind, Stier oder Kamel sein. Das Tier muss gesund und ein bestimmtes Alter erreicht haben, um halal, also auf islamische Weise geschlachtet zu werden. Das Fleisch des opfernden Tieres wird unter der opfernden Person, ihren Freunden und Familien sowie den Armen und Bedürftigen aufgeteilt.

Das Opfern eines Tieres an Eid al-Adha spiegelt die Bereitschaft des Propheten Abraham wider, seinen eigenen Sohn zu opfern, und ist ein Akt der Anbetung, der sowohl im Quran verankert als auch eine bestätigte Tradition des Propheten Muhammad, {{ $page->pbuh() }} ist.

{{ $page->hadith('bukhari:5558') }}

Im Quran hat Gott gesagt:

{{ $page->verse('2:196..') }}
