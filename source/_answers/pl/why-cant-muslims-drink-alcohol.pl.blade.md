---
extends: _layouts.answer
section: content
title: Dlaczego muzułmanie nie mogą pić alkoholu?
date: 2024-11-17
description: Picie alkoholu jest poważnym grzechem i kluczem do wszelkiego zła. Zaćmiewa
  umysł i marnuje pieniądze. Allah zakazał wszystkiego, co szkodzi ciału i umysłowi.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
translator_id: 583847
---

Islam przyszedł, aby przynieść i pomnożyć dobro, a także odeprzeć i zredukować szkodliwe rzeczy. Wszystko, co jest korzystne lub w większości korzystne, jest dozwolone (halal), a to, co jest szkodliwe lub w większości szkodliwe, jest zabronione (haram). Alkohol bez wątpienia należy do tej drugiej kategorii. Allah mówi:

{{ $page->verse('2:219') }}

Szkodliwe i złe skutki alkoholu są dobrze znane wszystkim ludziom. Wśród szkodliwych skutków alkoholu jest ten, o którym wspomniał Allah:

{{ $page->verse('5:90-91') }}

Alkohol prowadzi do wielu szkodliwych rzeczy i zasługuje na miano "klucza do wszelkiego zła" - jak określił go prorok Muhammad {{ $page->pbuh() }}, który powiedział:

{{ $page->hadith('ibnmajah:3371') }}

Tworzy wrogość i nienawiść między ludźmi, uniemożliwia im pamiętanie o Allahu i modlitwie, wzywa ich do zina [nielegalnych stosunków seksualnych], a nawet może wezwać ich do popełnienia kazirodztwa z ich córkami, siostrami lub innymi krewnymi. Zabiera dumę i opiekuńczą zazdrość, rodzi wstyd, żal i hańbę. Prowadzi to do ujawnienia tajemnic i obnażenia wad. Zachęca ludzi do popełniania grzechów i złych czynów.

Ponadto Ibn 'Umar przekazał, że Prorok {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:2003a') }}

Prorok {{ $page->pbuh() }} również powiedział:

{{ $page->hadith('ibnmajah:3381') }}
