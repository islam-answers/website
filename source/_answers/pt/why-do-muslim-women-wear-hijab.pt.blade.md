---
extends: _layouts.answer
section: content
title: Por que as mulheres muçulmanas usam o Hijab?
date: 2024-10-09
description: O Hijab é uma ordem de Allah que dá poder à mulher, realçando a sua beleza
  espiritual interior, em vez da sua aparência superficial.
sources:
- href: https://islamicpamphlets.com/misconceptions-about-islam
  title: islamicpamphlets.com
- href: https://islamicpamphlets.com/my-hijab-reflections-by-muslim-women
  title: islamicpamphlets.com
translator_id: 594172
---

A palavra Hijab deriva da raiz árabe "Hajaba", que significa esconder ou cobrir. Num contexto islâmico, Hijab refere-se ao código de vestuário exigido às mulheres muçulmanas que atingiram a puberdade. O hijab consiste em cobrir ou velar todo o corpo, com exceção do rosto e das mãos. Algumas mulheres optam também por cobrir o rosto e as mãos, o que é designado por Burqa ou Niqab.

Para observar o Hijab, as mulheres muçulmanas são obrigadas a cobrir modestamente o seu corpo com roupas que não revelem a sua figura à frente de homens que não sejam da sua família. No entanto, o Hijab não tem apenas a ver com a aparência exterior; tem também a ver com um discurso nobre, modéstia e conduta digna.

{{ $page->verse('33:59') }}

Embora existam muitos benefícios do Hijab, a principal razão pela qual as mulheres muçulmanas o usam é o facto de ser uma ordem de Allah (Deus), e Ele sabe o que é melhor para a Sua criação.

O Hijab dá poder à mulher ao realçar a sua beleza espiritual interior, em vez da sua aparência superficial. Dá às mulheres a liberdade de serem membros activos da sociedade, mantendo a sua modéstia.

O Hijab não simboliza a supressão, a opressão ou o silêncio. Pelo contrário, é uma proteção contra comentários degradantes, avanços indesejados e discriminação injusta. Por isso, da próxima vez que vir uma mulher muçulmana, saiba que ela cobre a sua aparência física, não a sua mente ou intelecto.
