---
extends: _layouts.answer
section: content
title: Que dit l'Islam sur le traitement des parents ?
date: 2024-09-13
description: La bonté envers ses parents est l'un des actes les mieux récompensés par Dieu, comme enseigné clairement dans le Coran.
sources:
- href: https://islamqa.info/en/answers/13783/
  title: islamqa.info
- href: https://islamqa.info/en/answers/112020/
  title: islamqa.info
- href: https://islamqa.info/en/answers/139144/
  title: islamqa.info
translator_id: 573111
---

Faire preuve de bonté envers ses parents est l'un des actes les mieux récompensés par Allah. Le commandement d’être bienveillant envers ses parents est un enseignement clair du Coran. Allah dit :

{{ $page->verse('4:36..') }}

La mention du devoir envers les parents suit immédiatement celle du devoir envers Dieu. Cela se répète tout au long du Coran.

{{ $page->verse('17:23') }}

Dans ce verset, Dieu nous ordonne de n'adorer que Lui et personne d'autre, puis Il nous incite à bien nous comporter avec nos parents, en particulier lorsqu'ils deviennent âgés et dépendent des autres pour s'abriter et subvenir à leurs besoins. Les musulmans apprennent à ne pas être durs avec leurs parents ni à les gronder, mais plutôt à leur parler avec des paroles aimables et à leur montrer de l'amour et de la tendresse.

Dans le Coran, Allah demande également à une personne d'implorer pour ses parents :

{{ $page->verse('17:24') }}

Allah insiste sur le fait qu'obéir à ses parents équivaut à Lui obéir, sauf lorsqu'il s'agit de Lui associer des partenaires.

{{ $page->verse('31:14-15..') }}

Ce verset souligne que tous les parents méritent d'être bien traités, avec une attention particulière accordée aux mères en raison des difficultés supplémentaires qu'elles endurent.

Prendre soin de ses parents est considéré comme l'une des meilleures actions :

{{ $page->hadith('muslim:85e') }}

En conclusion, en islam, honorer ses parents signifie leur obéir, les respecter, prier pour eux, baisser la voix en leur présence, leur sourire, ne pas montrer de mécontentement à leur égard, s'efforcer de les servir, exaucer leurs souhaits, les consulter et écouter ce qu'ils ont à dire.
