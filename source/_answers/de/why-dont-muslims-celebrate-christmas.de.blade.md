---
extends: _layouts.answer
section: content
title: Warum feiern Muslime kein Weihnachten?
date: 2024-12-25
description: Muslime ehren Jesus (Friede sei mit ihm) als Propheten, lehnen seine
  Göttlichkeit ab. Weihnachten ist wegen nicht-islamischer Wurzeln im Islam verboten.
sources:
- href: https://islamqa.info/en/answers/178136/
  title: islamqa.info
- href: https://islamqa.info/en/answers/145950/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7856/
  title: islamqa.info
- href: https://islamqa.org/hanafi/qibla-hanafi/42845/
  title: islamqa.org
translator_id: 587746
---

Muslime respektieren und verehren Jesus {{ $page->pbuh() }}, den Sohn von Mariam (Maria), und erwarten seine Wiederkunft. Sie betrachten ihn als einen der größten Gesandten Gottes an die Menschheit. Der Glaube eines Muslims ist nicht gültig, wenn er nicht an alle Gesandten Allahs glaubt, einschließlich Jesus {{ $page->pbuh() }}.

Aber Muslime glauben nicht, dass Jesus Gott oder der Sohn Gottes war. Ebenso wenig glauben Muslime, dass er gekreuzigt wurde. Allah sandte Jesus zu den Kindern Israels, um sie aufzurufen, nur an Allah zu glauben und nur Ihn anzubeten. Allah unterstützte Jesus mit Wundern, die bewiesen, dass er die Wahrheit sprach.

Allah sagt im Koran:

{{ $page->verse('19:88-92') }}

### Ist es erlaubt, Weihnachten zu feiern?

In Anbetracht dieser theologischen Unterschiede ist es den Muslimen nicht gestattet, die Feste anderer Religionen für irgendwelche Rituale oder Bräuche herauszugreifen. Vielmehr ist der Tag ihrer Feste ein ganz gewöhnlicher Tag für die Muslime, und sie sollten ihn nicht für irgendwelche Aktivitäten nutzen, die zu dem gehören, was die Nicht-Muslime an diesen Tagen tun.

Das christliche Weihnachtsfest ist eine Tradition, die Glaubenssätze und Bräuche einschließt, die nicht in den Lehren des Islam wurzeln. Daher ist es Muslimen nicht gestattet, an diesen Feierlichkeiten teilzunehmen, da sie nicht mit dem islamischen Verständnis von Jesus {{ $page->pbuh() }} oder seinen Lehren übereinstimmen.

Es handelt sich dabei nicht nur um eine Neuerung, sondern auch um die Nachahmung der Ungläubigen in Angelegenheiten, die ihnen und ihrer Religion eigen sind. Der Prophet Muhammad {{ $page->pbuh() }} sagte:

{{ $page->hadith('abudawud:4031') }}

Ibn `Uthaymin (möge Allah ihm gnädig sein) sagte:

> Den Ungläubigen zu Weihnachten oder einem ihrer anderen religiösen Feste zu gratulieren, ist nach dem Konsens der Gelehrten verboten, denn es bedeutet, dass man das billigt, was sie dem Unglauben anhängen, und dass man es für sie billigt. Auch wenn er diesen Unglauben für sich selbst nicht billigt, ist es dem Muslim verboten, Rituale des Unglaubens gutzuheißen oder jemand anderem dafür zu gratulieren. Es ist den Muslimen nicht gestattet, sie in irgendeiner Weise nachzuahmen, die ihren Festen eigen ist, sei es in Bezug auf Essen, Kleidung, Baden, Feuermachen oder das Unterlassen der üblichen Arbeit oder des Gottesdienstes usw. Und es ist nicht gestattet, ein Fest zu geben oder Geschenke auszutauschen oder Dinge zu verkaufen, die ihnen helfen, ihre Feste zu feiern (...), oder sich zu schmücken oder Dekorationen anzubringen.

In der Encyclopedia Britannica heißt es dazu:

> Das Wort Weihnachten leitet sich vom altenglischen Cristes maesse ab, „Christi Messe“. Es gibt keine sichere Überlieferung über das Datum von Christi Geburt. Christliche Chronographen des 3. Jahrhunderts glaubten, dass die Erschaffung der Welt zur Frühlingstagundnachtgleiche stattfand, die damals auf den 25. März datiert wurde. Daher müssen die Neuschöpfung in der Menschwerdung (d. h. der Empfängnis) und der Tod Christi am selben Tag stattgefunden haben, und seine Geburt erfolgte neun Monate später zur Wintersonnenwende am 25. Dezember.

> Der Grund, warum Weihnachten am 25. Dezember gefeiert wurde, ist unklar, aber höchstwahrscheinlich liegt es daran, dass die frühen Christen das Datum mit dem heidnischen römischen Fest zusammenfallen lassen wollten, das den „Geburtstag der unbesiegten Sonne“ (natalis solis invicti) markierte; dieses Fest feierte die Wintersonnenwende, wenn die Tage wieder länger werden und die Sonne höher am Himmel steht. Die traditionellen Bräuche im Zusammenhang mit Weihnachten haben sich dementsprechend aus mehreren Quellen entwickelt, als Folge des Zusammentreffens der Feier der Geburt Christi mit den heidnischen landwirtschaftlichen und solaren Feierlichkeiten zur Wintersonnenwende. In der römischen Welt waren die Saturnalien (17. Dezember) eine Zeit des Feierns und des Austauschs von Geschenken. Der 25. Dezember galt auch als Geburtsdatum des iranischen Mysteriengottes Mithra, der Sonne der Gerechtigkeit. Am römischen Neujahrstag (1. Januar) wurden die Häuser mit Grünpflanzen und Lichtern geschmückt und es wurden Geschenke an Kinder und Arme verteilt.

Wie also jeder vernünftige Mensch erkennen kann, gibt es keine vernünftige Grundlage für Weihnachten. Weder Jesus {{ $page->pbuh() }} noch seine wahren Anhänger haben Weihnachten gefeiert oder irgendjemanden aufgefordert, Weihnachten zu feiern. Auch gibt es bis mehrere hundert Jahre nach Jesus keine Aufzeichnungen darüber, dass jemand, der sich Christ nannte, Weihnachten feierte. Waren also die Gefährten Jesu rechtschaffener, als sie Weihnachten nicht feierten, oder sind es die Menschen von heute?

Wenn du also Jesus respektieren willst {{ $page->pbuh() }} wie es Muslime tun, feiert nicht irgendein erfundenes Ereignis, das ausgewählt wurde, um mit heidnischen Festen zusammenzufallen, und kopiert heidnische Bräuche. Glaubst du wirklich, dass Gott oder sogar Jesus selbst so etwas gutheißen oder verurteilen würde?
