---
extends: _layouts.answer
section: content
title: Why did prophet Muhammad marry a young girl?
date: 2024-12-05
description: Prophet Muhammad’s marriage to Aisha reflected 7th-century norms, with
  no objections then; modern criticism applies today’s standards to historical practices.
sources:
- href: https://islamqa.info/en/answers/44990/
  title: islamqa.info
- href: https://islamqa.org/hanafi/askimam/2061/
  title: islamqa.org (AskImam.org)
---

Aisha (may Allah be pleased with her) was 6 years old when she was married to Prophet Muhammad {{ $page->pbuh() }} and was 9 when the marriage was consummated.

Sadly, we currently see the efforts of other religions focused primarily on twisting and manipulating historical and etymological facts. One such topic has been the allegation of the young marriage of Aisha to Prophet Muhammad {{ $page->pbuh() }}. The missionaries try to accuse the Prophet of being a child molester, albeit in politically correct terms, due to the fact that Aisha was betrothed at the age of 6 years old and the marriage was consummated 3 years later - at 9 years old - when she was in full puberty. The lapse of time between the betrothal and consummation of Aisha clearly shows that her parents were waiting for her to reach puberty before her marriage was consummated. This answer seeks to refute the unjust allegation against Prophet Muhammad {{ $page->pbuh() }}.

### Puberty and young marriage in Semitic culture

The allegation of child molestation contradicts the basic fact that a girl becomes a woman when she begins her menstruation cycle. The significance of menstruation that anyone with the slightest familiarity with physiology will tell you is that it is a sign that the girl is being prepared to become a mother.

Women reach puberty at different ages ranging from 8-12 years old depending on genetics, race and environment. There is little difference in the size of boys and girls until the age of ten, the growth spurt at puberty starts earlier in girls but lasts longer in boys. The first signs of puberty occur around age 9 or 10 in girls, but closer to 12 in boys. Also, women in warmer environments reach puberty at a much earlier age than those in cold environments. The average temperature of the country or province is considered the most important factor here, not only with regard to menstruation but as regards the whole of sexual development at puberty.

Marriage in the early years of puberty was acceptable in 7th century Arabia, as it was the social norm in all Semitic cultures from the Israelites to the Arabs and all nations in between. According to the Talmud, which the Jews regard as their “oral Torah”, Sanhedrin 76b clearly states that it is preferable that a woman be married when she has her first menses, and in Ketuvot 6a there are rules regarding sexual intercourse with girls who have not yet menstruated. Jim West, a Baptist minister, observes the following tradition of the Israelites:

- The wife was to be taken from within the larger family circle (usually at the outset of puberty or around the age of 13) in order to maintain the purity of the family line.
- Puberty has always been a symbol of adulthood throughout history.
- Puberty is defined as the age or period at which a person is first capable of sexual reproduction, in other eras of history, a rite or celebration of this landmark event was a part of the culture.

The renowned sexologists, R.E.L. Masters and Allan Edwards, in their study of Afro-Asian sexual expression, state the following:

> Today, in many parts of North Africa and the Middle East, girls are wedded and bedded between the ages of five and nine; and no self-respecting female remains unmarried beyond the age of puberty.

### Reasons for marrying Aisha

With regard to the story of her marriage, the Prophet {{ $page->pbuh() }} had grieved over the death of his first wife Khadeejah, who had supported him and stood by his side, and he called the year in which she died The Year of Sorrow. Then he married Sawdah, who was an older woman and was not very beautiful; rather he married her to console her after her husband had died. Four years later, the Prophet {{ $page->pbuh() }} married Aisha. The reasons for the marriage were as follows:

1. He had a dream about marrying her. It is proven from the narration of Aisha that the Prophet {{ $page->pbuh() }} said to her:

{{ $page->hadith('bukhari:3895') }}

2. The characteristics of intelligence and smartness that the Prophet {{ $page->pbuh() }} had noticed in Aisha even as a small child, so he wanted to marry her so that she would be more able than others to transmit reports of what he did and said. In fact, as stated above, she was a reference point for the companions of the Prophet {{ $page->pbuh() }} with regard to their affairs and rulings.

3. The love of the Prophet {{ $page->pbuh() }} for her father Abu Bakr, and the persecution that Abu Bakr had suffered for the sake of the call to Islam, which he bore with patience. He was the strongest and most sincere of people in faith after the Prophets.

### Were there any objections to their marriage?

The answer to this is no. There are absolutely no records from Muslim, secular, or any other historical sources which even implicitly display anything other than utter joy from all parties involved over this marriage. Nabia Abbott describes the marriage of Aisha to the Prophet {{ $page->pbuh() }} as follows:

> In no version is there any comment made on the disparity of the ages between Mohammed and Aishah or on the tender age of the bride who, at the most, could not have been over ten years old and who was still much enamored with her play.

Even the well-known critical orientalist, W. Montgomery Watt, said the following about the Prophet’s {{ $page->pbuh() }} moral character:

> From the standpoint of Muhammad’s time, then, the allegations of treachery and sensuality cannot be maintained. His contemporaries did not find him morally defective in any way. On the contrary, some of the acts criticized by the modern Westerner show that Muhammad’s standards were higher than those of his time.

Aside from the fact that no one was displeased with him or his actions, he was a paramount example of moral character in his society and time. Therefore, to judge his morality based on the standards of our society and culture today is not only absurd, but also unfair.

### Marriage at puberty today

The Prophet’s contemporaries (both enemies and friends) clearly accepted the Prophet’s {{ $page->pbuh() }} marriage to Aisha without any problem. We see the evidence for this by the lack of criticism against this marriage until modern times.

What caused this is a change in the culture of our times today, regarding the age when people usually get married. But even today in the 21st century, the age of sexual consent is still quite low in many places. In Germany, Italy and Austria, people can legally have sex at age 14, and in the Philippines and Angola they can legally have sex at the age of 12 years old. California was the first state to change the age of consent to 14, which it did in 1889. After California, other states joined in and raised the age of consent too.

### Islam and the age of puberty

Islam clearly teaches that adulthood starts when a person has attained puberty. Allah says in the Quran:

{{ $page->verse('24:59..') }}

The attaining of puberty by women is with the start of menses, as Allah says in the Quran:

{{ $page->verse('65:4..') }}

Thus, it is part of Islam to acknowledge the coming of puberty as the start of adulthood. It is the time when the person has already matured and is ready for the responsibilities of an adult. So on what basis should someone criticize the marriage of Aisha, since her marriage was consummated after she reached puberty?

If  the allegation of “child molestation” were to be advanced against the Prophet {{ $page->pbuh() }}, we would also have to include all the Semitic people who accepted marriage at puberty as the norm.

Moreover, knowing that the Prophet {{ $page->pbuh() }} did not marry any virgin other than Aisha and that all his other wives had been previously married (and many of them were old), this will refute the notion spread by many hostile sources, that the basic motive behind the Prophet’s marriages was physical desire and enjoyment of women, because if that was his intention he would have chosen only those who were young.

### Conclusions

We have thus seen that:

- It was the norm of the Semitic society in 7th century Arabia to allow pubescent marriages.
- There were no reports of opposition to the Prophet’s marriage to Aisha, either from his friends or enemies.
- Even today, there are cultures who still allow pubescent marriage for their young women.

In spite of facing these well-known facts, some people still accuse the Prophet {{ $page->pbuh() }} of immorality. Yet, it was he who had brought justice to the women of Arabia and raised them to a level they had not seen before in their society, something which ancient civilizations have never done to their women.

For example, when he first became Prophet, the pagans of Arabia had inherited a disregard for women as had been passed down among their Jewish and Christian neighbors. So disgraceful was it considered among them to be blessed with a female child, that they would go so far as to bury this baby alive in order to avoid the disgrace associated with female children. Allah says in the Quran:

{{ $page->verse('16:58-59') }}

Not only did Muhammad {{ $page->pbuh() }} severely discourage and condemn this act, but he also used to teach people to respect and cherish their daughters and mothers as partners and sources of salvation for the men of their family. He said:

{{ $page->hadith('ibnmajah:3669') }}

It was also narrated in a hadith from Anas Ibn Malik that:

{{ $page->hadith('muslim:2631') }}

In other words, if one loves the Messenger of Allah {{ $page->pbuh() }} and wishes to be with him on the Day of Resurrection in Heaven, then they should be good to their daughters. This is certainly not the act of a “child molester”, as some would like us to believe.
