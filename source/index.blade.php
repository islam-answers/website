@extends('_layouts.main')

@push('head')
<script type="application/ld+json">
{
    "@context" : "https://schema.org",
    "@type" : "WebSite",
    "name" : "{{ $page->siteName }}",
    "alternateName" : "{{ $page->alternateSiteName }}",
    "url" : "{{ $page->baseUrl }}"
}
</script>
@endpush

@section('body')

<h1>Guidance through the<br class="hidden sm:inline"> harmony of life and Islam</h1>

<div class="text-2xl border-b border-blue-200 mb-6 pb-10">
    <p>
        <a href="/ask/" title="Ask a question" class="text-blue-600">Ask a question</a>

        or

        <a href="/join/" title="Join our team" class="text-blue-600">join our team</a>!
    </p>
</div>

<h2 class="text-2xl mb-4">Select your language</h2>

@include('_components.languages-list', ['compact' => false])

@stop
