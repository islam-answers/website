@php ($sources = [
    [
        'title' => 'IslamQA.info',
        'href' => 'https://islamqa.info',
    ],
    [
        'title' => 'IslamWeb.net',
        'href' => 'https://islamweb.net',
    ],
    [
        'title' => 'AskImam.org',
        'href' => 'https://askimam.org',
    ],
    [
        'title' => 'Muslim Skeptic / Daniel Haqiqatjou',
        'href' => 'https://muslimskeptic.com',
    ],
    [
        'title' => 'Mufti Menk',
        'href' => 'https://muftimenk.com',
    ],
    [
        'title' => 'IslamQA.org',
        'href' => 'https://islamqa.org',
    ],
    [
        'title' => 'Quran.com',
        'href' => 'https://quran.com',
    ],
    [
        'title' => 'Sunnah.com',
        'href' => 'https://sunnah.com',
    ],
    [
        'title' => 'IERA',
        'href' => 'https://iera.org',
    ],
    [
        'title' => 'Islam-Guide.com',
        'href' => 'https://islam-guide.com',
    ],
    [
        'title' => 'IslamicPamphlets',
        'href' => 'https://islamicpamphlets.com',
    ],
    [
        'title' => 'DarAlArqam.co.uk',
        'href' => 'https://daralarqam.co.uk',
    ],
    [
        'title' => 'OneReason.org',
        'href' => 'https://onereason.org',
    ],
    [
        'title' => 'Belal Assaad',
        'href' => 'https://www.youtube.com/@belal.assaad',
    ],
    [
        'title' => 'Dr. Eyad Qunaibi',
        'href' => 'https://www.youtube.com/@DrEyadQunaibiGlobalChannel',
    ],
    [
        'title' => 'Ali Hammuda',
        'href' => 'https://www.youtube.com/@Ali.Hammuda',
    ],
    [
        'title' => 'Abu Taymiyyah',
        'href' => 'https://www.youtube.com/@AbutaymiyyahMJ',
    ]
])

<ul>
    @foreach ($sources as $source)
    <li>
        <a href="{{ $source['href'] }}" target="_blank">{{ $source['title'] }}</a>
    </li>
    @endforeach
</ul>