---
extends: _layouts.answer
section: content
title: Quelles sont les fêtes célébrées par les musulmans ?
date: 2024-07-12
description: Les musulmans ne célèbrent que deux Aïd (fêtes), l'Aïd al-Fitr (à la
  fin du mois de Ramadan) et l'Aïd al-Adha, à la fin du Hajj (pèlerinage annuel).
sources:
- href: https://islamqa.info/en/answers/486/religious-celebrations-in-islam
  title: islamqa.info
translator_id: 573111
---

Les musulmans ne célèbrent que deux Aïd (fêtes) : L'Aïd al-Fitr (à la fin du mois de Ramadan) et l'Aïd al-Adha, qui marque l'achèvement du Hajj (pèlerinage annuel) le 10ᵉ jour du mois de Dhul-Hijjah.

Lors de ces deux fêtes, les musulmans se félicitent mutuellement, répandent la joie dans leur communauté et célèbrent avec leur famille élargie. Mais surtout, ils se souviennent des bénédictions d'Allah sur eux, célèbrent son nom et font la prière de l'Aïd à la mosquée. En dehors de ces deux occasions, les musulmans ne reconnaissent ni ne célèbrent aucun autre jour de l'année.

Bien sûr, il existe d'autres occasions joyeuses pour lesquelles l'islam dicte des célébrations appropriées, telles que les célébrations de mariage (walima) ou à l'occasion de la naissance d'un enfant (aqeeqah). Cependant, ces jours ne sont pas spécifiés comme des jours particuliers de l'année ; ils sont plutôt célébrés au fur et à mesure qu'ils se produisent dans la vie d'un musulman.

Le matin de l'Aïd al-Fitr et de l'Aïd al-Adha, les musulmans assistent aux prières de l'Aïd en congrégation à la mosquée, suivies d'un sermon (khutbah) rappelant aux musulmans leurs devoirs et responsabilités. Après la prière, les musulmans se saluent avec l'expression "Aïd Moubarak" (Aïd béni) et échangent des cadeaux et des friandises.

## Aïd al-Fitr

L'Aïd al-Fitr marque la fin du Ramadan, qui a lieu pendant le neuvième mois du calendrier islamique lunaire.

L'Aïd al-Fitr est important, car il suit l'un des mois les plus sacrés de tous : le Ramadan. Le Ramadan est une période où les musulmans renforcent leur lien avec Allah, récitent le Coran et multiplient les bonnes actions. À la fin du Ramadan, Allah offre aux musulmans le jour de l'Aïd al-Fitr comme récompense pour avoir réussi à jeûner et pour avoir accru leurs actes d'adoration durant le mois de Ramadan. Le jour de l'Aïd al-Fitr, les musulmans remercient Allah pour l'opportunité de vivre un autre Ramadan, de se rapprocher de Lui, de devenir de meilleures personnes et d'avoir une nouvelle chance d'être sauvés du Feu de l'Enfer.

{{ $page->hadith('ibnmajah:3925') }}

## Aïd al-Adha

L'Aïd al-Adha marque la fin de la période annuelle du Hajj (pèlerinage à La Mecque), qui se déroule au mois de Dhul-Hijjah, le douzième et dernier mois du calendrier islamique lunaire.

La célébration de l'Aïd al-Adha commémore la dévotion du prophète Ibrahim (Abraham) à Allah et sa volonté de sacrifier son fils Ismail (Ismaël). Au moment du sacrifice, Allah a remplacé Ismail par un bélier, qui devait être égorgé à la place de son fils. Cet ordre d'Allah était un test de la volonté et de l'engagement du prophète Ibrahim à obéir à l'ordre de son Seigneur, sans poser de questions. C'est pourquoi l'Aïd al-Adha signifie la fête du sacrifice.

L'une des meilleures actions que les musulmans peuvent accomplir le jour de l'Aïd al-Adha est l'acte de Qurbani/Uhdiya (sacrifice), qui est accompli après la prière de l'Aïd. Les musulmans sacrifient un animal en souvenir du sacrifice du prophète Abraham pour Allah. L'animal sacrifié doit être un mouton, un agneau, une chèvre, une vache, un taureau ou un chameau. L'animal doit être en bonne santé et avoir dépassé un certain âge afin d'être abattu de manière halal, c'est-à-dire islamique. La viande de l'animal sacrifié est partagée entre la personne qui fait le sacrifice, ses amis et sa famille, ainsi que les pauvres et les nécessiteux.

Le sacrifice d'un animal le jour de l'Aïd al-Adha reflète la volonté du prophète Abraham de sacrifier son propre fils. Il s'agit également d'un acte d'adoration mentionné dans le Coran et d'une tradition confirmée du prophète Muhammad, {{ $page->pbuh() }}.

{{ $page->hadith('bukhari:5558') }}

Dans le Coran, Dieu a dit :

{{ $page->verse('2:196..') }}
