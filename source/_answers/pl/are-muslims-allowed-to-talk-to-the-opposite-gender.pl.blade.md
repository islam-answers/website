---
extends: _layouts.answer
section: content
title: Czy muzułmanom wolno rozmawiać z osobami płci przeciwnej?
date: 2024-12-03
description: W islamie kontakty z osobami płci przeciwnej są dozwolone, jeśli zajdzie
  taka potrzeba, ale muszą być skromne, krótkie i należy unikać pokus.
sources:
- href: https://islamqa.info/en/answers/27304/
  title: islamqa.info
- href: https://islamqa.info/en/answers/1497/
  title: islamqa.info
- href: https://islamqa.info/en/answers/34841/
  title: islamqa.info
- href: https://islamqa.info/en/answers/21183/
  title: islamqa.info
translator_id: 584496
---

W islamie interakcje z płcią przeciwną są dozwolone z konieczności, ale muszą być skromne, krótkie i należy unikać pokus.

### Zasada prewencji (zapobiegania szkodom)

W islamie wszystko, co może doprowadzić człowieka do popadnięcia w niedozwolone (haram) rzeczy, jest również niedozwolone, nawet jeśli w zasadzie jest to pierwotnie dozwolone. To właśnie uczeni nazywają zasadą odstraszania od krzywdy. Allah mówi w Koranie:

{{ $page->verse('24:21..') }}

Rozmowa — czy to osobista, ustna czy pisemna — między mężczyzną i kobietą jest sama w sobie dozwolona, ale może być sposobem na popadnięcie w próbę (fitnę) stworzoną przez Szatana (Szajtana).

### Niebezpieczeństwa związane z interakcją z płcią przeciwną

Nie ma wątpliwości, że fitnah (pokusa) kobiet jest wielka. Prorok {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:2741') }}

Dlatego muzułmanin musi być ostrożny w stosunku do tej fitny i trzymać się z daleka od wszystkiego, co może sprawić, że stanie się jej ofiarą. Jednymi z największych przyczyn tej fitny są patrzenie na kobiety i przebywanie z nimi.

Allah mówi w Koranie:

{{ $page->verse('24:30-31') }}

Tutaj Allah nakazuje swojemu Prorokowi {{ $page->pbuh() }}, aby powiedział wierzącym mężczyznom i kobietom, by spuszczali wzrok i strzegli swojej czystości, a następnie wyjaśnia, że to jest dla nich czystsze. Strzeżenie swojej czystości i unikanie niemoralnych działań osiąga się tylko poprzez unikanie środków prowadzących do takich działań. Niewątpliwie pozwalanie sobie na wędrowanie wzrokiem i mieszanie się mężczyzn i kobiet w miejscu pracy i w innych miejscach jest jednym z największych środków prowadzących do niemoralności.

Jak często te rozmowy prowadziły do złych rezultatów, a nawet powodowały, że ludzie się zakochiwali, a niektórzy robili rzeczy jeszcze poważniejsze niż to. Szatan (Szatan) sprawia, że każdy z nich wyobraża sobie atrakcyjne cechy w drugim, co prowadzi ich do rozwijania przywiązania, które jest szkodliwe dla ich duchowego dobrobytu i spraw światowych.

### Etykieta rozmowy z płcią przeciwną

Mahram (w języku angielskim tłumaczone jako „niezamężna rodzina”) to osoba, z którą nigdy nie wolno jej poślubić ze względu na bliskie pokrewieństwo (w przypadku kobiety są to jej przodkowie, potomkowie, bracia, wujkowie i siostrzeńcy) lub ze względu na karmienie piersią, bądź też ze względu na pokrewieństwo poprzez małżeństwo (w przypadku kobiety są to przodkowie jej męża, jego ojczymowie, jego potomkowie oraz mężowie jej córek).

Rozmowa z osobą płci przeciwnej, z którą nie jest się spokrewnionym (tj. nie Mahram), powinna mieć miejsce wyłącznie w określonych celach, takich jak zadanie pytania, kupno lub sprzedaż, pytanie o głowę rodziny itd. Takie rozmowy powinny być krótkie i konkretne, bez żadnych wątpliwości co do tego, co się mówi lub jak się to mówi. Rozmowa nie powinna zbytnio odbiegać od omawianego tematu; nie należy pytać o sprawy osobiste, które nie mają związku z omawianą kwestią, takie jak wiek danej osoby, jej wzrost lub miejsce zamieszkania itd.

Islam blokuje wszystkie drogi, które mogą prowadzić do fitnah (pokusy), stąd zabrania łagodnej mowy i nie pozwala mężczyźnie być sam na sam z kobietą niebędącą Mahram. Nie powinni pozwalać, aby ich głosy były łagodne, ani używać łagodnych i delikatnych wyrażeń. powinni raczej mówić tym samym, zwykłym tonem głosu, jakim mówiliby do kogokolwiek innego. Allah, niech będzie wywyższony, mówi, zwracając się do Matek Wierzących:

{{ $page->verse('..33:32') }}

Niedopuszczalne jest również, aby mężczyzna przebywał sam na sam z kobietą, która nie jest jego Mahram, ponieważ Prorok {{ $page->pbuh() }} powiedział:

{{ $page->hadith('muslim:1341') }}

Rozmowa lub korespondencja musi zostać natychmiast przerwana, jeśli w sercu pojawi się uczucie pożądania. Należy również unikać żartów, śmiechu lub flirtowania. Inną etykietą interakcji z płcią przeciwną jest unikanie gapienia się i zawsze staranie się, aby jak najbardziej obniżyć spojrzenie. Jarir bin 'Abdullah relacjonował:

{{ $page->hadith('muslim:2159') }}

Warto również wspomnieć, że mężczyźnie wierzącemu w Allaha i Jego Posłańca {{ $page->pbuh() }} nie wolno mieć kontaktu fizycznego z osobą płci przeciwnej, która nie jest dla niego mahramem.
