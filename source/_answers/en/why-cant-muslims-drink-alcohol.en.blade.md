---
extends: _layouts.answer
section: content
title: Why can't Muslims drink alcohol?
date: 2024-10-08
description: Drinking alcohol is a major sin and is the key to all evils. It clouds the mind and wastes money. Allah has forbidden everything that harms the body and mind.
sources:
- href: https://islamqa.info/en/answers/40882/
  title: islamqa.info
- href: https://islamqa.info/en/answers/7327/
  title: islamqa.info
- href: https://islamqa.info/en/answers/38145/
  title: islamqa.info
---

Islam came to bring and increase good things, and to ward off and reduce harmful things. Whatever is beneficial or mostly beneficial is permissible (halal) and whatever is harmful or mostly harmful is forbidden (haram). Alcohol undoubtedly falls into the second category. Allah says:

{{ $page->verse('2:219') }}

The harmful and evil effects of alcohol are well known to all people. Among the harmful effects of alcohol is that which was mentioned by Allah:

{{ $page->verse('5:90-91') }}

Alcohol leads to many harmful things, and deserves to be called “the key to all evils” – as it was described by Prophet Muhammad {{ $page->pbuh() }}, who said:

{{ $page->hadith('ibnmajah:3371') }}

It creates enmity and hatred between people, prevents them from remembering Allah and praying, calls them to zina [unlawful sexual relationships], and may even call them to commit incest with their daughters, sisters or other female relatives. It takes away pride and protective jealousy, generates shame, regret and disgrace. It leads to the disclosure of secrets and exposure of faults. It encourages people to commit sins and evil actions.

Furthermore, Ibn ‘Umar narrated that the Prophet {{ $page->pbuh() }} said:

{{ $page->hadith('muslim:2003a') }}

The Prophet {{ $page->pbuh() }} also said:

{{ $page->hadith('ibnmajah:3381') }}
